# -*- mode: org -*-

* Hint: Burger

#+CAPTION: A 10x10 grid. Column labels: 1. 2, 2. 1, 3, 1. 1, 1, 1, 1. 1, 3, 1. 2, 2. 1. Row labels: 3. 1, 1. 5. 1, 1, 1, 1. 5. 1, 1. 3.
#+ATTR_LATEX: :width \textwidth
[[file:images/nonogram-09.png]]



