
#+ATTR_HTML: :class puzzle-instructions
#+NAME: content
#+BEGIN_HTML
*How to play:* Each cell in the grid of squares can either have a *X* (or blank) or be *filled in*. The filled in boxes create the pixels of an image.
Each row and each column have one or more numbers. This specifies how many pixels are in that row/column.
The number given is how many unbroken pixels will show up together.
Two numbers separated by a space, e.g., "1 1", will have /at least one empty spot/ between them.
#+END_HTML

Look for rows/columns with "0"s and x out the entire region,
do the same for rows/columns with the same # as the width/length, but fill in the entire region.

#+CAPTION: A 5x5 grid. Column labels: 1. 4. 2, 1. 4. 1. Row labels: 0. 3. 3. 1, 1. 5. The 5 row has been filled in.
#+ATTR_LATEX: :width 150px
[[file:images/example_b.png]]

Look for overlaps, continue started lines, and continue analyzing and filling things out
until you solve the puzzle. Check YouTube videos for step-by-step solutions to other Nonogram (aka Picross) puzzles.

#+CAPTION: The same puzzle as above. The 4 columns and the 2, 1 column has been filled in.
#+ATTR_LATEX: :width 150px
[[file:images/example_c.png]]

