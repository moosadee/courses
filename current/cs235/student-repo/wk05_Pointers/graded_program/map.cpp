#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
using namespace std;

const string CLEAR    = "\033[0m";
const string BLACK    = "\033[3;30;40m";
const string RED      = "\033[3;31;40m";
const string GREEN    = "\033[3;32;40m";
const string YELLOW   = "\033[3;33;40m";
const string BLUE     = "\033[3;34;40m";
const string MAGENTA  = "\033[3;35;40m";
const string CYAN     = "\033[3;36;40m";
const string WHITE    = "\033[3;37;40m";

struct Map
{
    Map() { }
    Map( string name, string desc )
        {
            this->name = name;
            this->description = desc;
            ptrNorth = nullptr;
            ptrSouth = nullptr;
            ptrWest = nullptr;
            ptrEast = nullptr;
        }
    string name;
    string description;
    Map* ptrNorth;
    Map* ptrSouth;
    Map* ptrWest;
    Map* ptrEast;
};

void Setup( vector<Map>& maps )
{
    // Maps based on Zork I :)
    maps.push_back( Map( "West of House", "You are standing in an open field west of a white house." ) );            // 0
    maps.push_back( Map( "North of House", "You are facing the north side of a white house. There is no door." ) );  // 1
    maps.push_back( Map( "South of House", "You are facing the south side of a white house. There is no door." ) );  // 2
    maps.push_back( Map( "Forest", "This is a dimly lit forest, with large trees all around." ) );                   // 3
    maps.push_back( Map( "Clearing", "There is a path winding through a dimly lit forest." ) );                      // 4
    maps.push_back( Map( "Behind House", "You are behind the white house. A small window is slightly ajar." ) );     // 5
    maps.push_back( Map( "Kitchen", "A table seems to have been used recently for the preparation of food." ) );     // 6
}

// Map neighbors:
//  4  1                      N
//  3  0  5                 W   E
//     2  6                   S
void SetNeighbors( vector<Map>& maps )
{
    // TODO: Set up neighbors based on the map above
}

void Game()
{
    vector<Map> maps;
    Setup( maps );
    SetNeighbors( maps );
    Map* currentMap = &maps[0];
    char userInput;
    int turn = 0;

    bool done = false;
    while ( !done )
    {
        turn++;
        cout << endl << endl << string( 80, '-' ) << endl << "Turn: " << turn << endl << string( 80, '-' ) << endl;
        // TODO: Display current map's name and description.

        cout << endl << GREEN << "You can move... ";
        // TODO: Display which neighbors are traversable.

        cout << endl << endl << WHITE << "What do you want to do? (N/S/E/W): ";
        cin >> userInput;
        userInput = tolower( userInput );
        cout << endl;

        // TODO: Look at user's input. If a map is in that direction (not nullptr neighbor), then move the currentMap there.
        // Otherwise, display an error.
        else if ( userInput == 'q' )
        {
            done = true;
        }
        else
        {
            cout << RED << "YOU CANNOT MOVE IN THAT DIRECTION!" << CLEAR << endl << endl;
        }
    }
}

//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! DON'T MODIFY ANYTHING BELOW THIS POINT! !!
//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
void Tester();
int main( int argCount, char* args[] )
{
    cout << fixed << setprecision( 2 );
    Game();

    return 0;
}
