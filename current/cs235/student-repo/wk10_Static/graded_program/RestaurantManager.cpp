#include "RestaurantManager.h"

#include <stdexcept>
#include <iostream>
using namespace std;

// - STUDENT CODE ----------------------------------------------------------
// TODO: Initialize the list of orders and the list of meals.

void RestaurantManager::CreateOrder( string meal_name )
{
		// TODO: Implement function
}

void RestaurantManager::PrepNextOrder()
{
		// TODO: Implement function
}

Meal RestaurantManager::ReturnMeal()
{
		// TODO: Implement function
}
// -------------------------------------------------------------------------

void RestaurantManager::Display()
{
    cout << "Restaurant manager" << endl;
    cout << "* Total orders: " << orders.size() << endl;
    cout << "* Total meals:  " << meals.size() << endl;
}

vector<string> RestaurantManager::GetIngredients( string meal_name )
{
    vector<string> foods;

    if ( meal_name == "breakfast combo" )
    {
        foods.push_back( "eggs" );
        foods.push_back( "pancakes" );
        foods.push_back( "bacon" );
        foods.push_back( "coffee" );
    }
    else if ( meal_name == "soup of the day" )
    {
        foods.push_back( "tomato soup" );
        foods.push_back( "french bread" );
    }
    else if ( meal_name == "lunch combo" )
    {
        foods.push_back( "sandwich" );
        foods.push_back( "chips" );
        foods.push_back( "soda" );
    }

    return foods;
}

