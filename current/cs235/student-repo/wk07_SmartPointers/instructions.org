#+HTML_HEAD: <style type='text/css'> body { font-family: sans-serif; } #content { width: 100%; max-width: 100%; } </style>
#+HTML_HEAD: <style type='text/css'> h3:before { content:"💾 "; } h3:after { content: ""; }  </style>
#+HTML_HEAD: <style type='text/css'> h3 { background: rgba( 0, 255, 150, 0.5 ); }  </style>
#+HTML_HEAD: <style type='text/css'> h4:before { content:"🟣 "; } h4:after { content: ""; }  </style>
#+HTML_HEAD: <style type='text/css'> code { background: rgba( 190, 111, 255, 0.2 ); padding: 2px; }  </style>
#+HTML_HEAD: <style type='text/css'> .src-terminal { background: #201826; color: #e5d3f4; font-weight: bold; } .src-terminal:before { content: "TERMINAL"; }  </style>
#+HTML_HEAD: <style type='text/css'> .src-form { background: #bddfff; color: #001528; font-weight: bold; } .src-form:before { content: "FORM"; }  </style>
#+HTML_HEAD: <style type='text/css'> .outline-1, .outline-2, .outline-3 { margin-left: 10px; padding: 10px; border: outset 2px #c0c0c0; background: rgba( 0, 255, 150, 0.1 ); margin-bottom: 10px; } </style>
#+HTML_HEAD: <style type='text/css'> .outline-3 { margin-bottom: 50px; } </style>

#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/tomorrow-night-blue.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script>
#+HTML_HEAD: <script>hljs.initHighlightingOnLoad();</script>

* Assignment and policy info

- Assignment info:
  - [[https://gitlab.com/moosadee/courses/-/blob/main/current/policies/practice-graded.org?ref_type=heads][Practice and Graded programs]]
  - [[https://gitlab.com/moosadee/courses/-/blob/main/current/policies/resubmit.org?ref_type=heads][Resubmission and regrading policy and procedure]]
  - [[https://gitlab.com/moosadee/courses/-/blob/main/current/policies/duedates.org?ref_type=heads][Due dates and available until dates]]
- How To:
  - [[https://gitlab.com/moosadee/courses/-/blob/main/reference/ide_vscode_build_and_run.org?ref_type=heads][Build and Run your program in VS Code]]
  - [[https://gitlab.com/moosadee/courses/-/blob/main/current/policies/turnin-lab.org?ref_type=heads][Turn in your lab]]
  - [[https://gitlab.com/moosadee/courses/-/blob/main/reference/vscode-git.org?ref_type=heads][Use Git and VS Code]]
- Quick reference:
  - [[https://gitlab.com/moosadee/courses/-/blob/main/current/share/read/CS200review.org?ref_type=heads][CS 200 code reference]]
  - [[https://gitlab.com/moosadee/courses/-/blob/main/reference/cpp_program_arguments.org?ref_type=heads][C++ program arguments]]
  - Debugging with [[https://gitlab.com/moosadee/courses/-/blob/main/reference/gdb.org?ref_type=heads][gdb]] / [[https://gitlab.com/moosadee/courses/-/blob/main/reference/lldb.org?ref_type=heads][lldb]]
  - [[https://gitlab.com/moosadee/courses/-/blob/main/reference/mac.org?ref_type=heads][Common issues on Mac]]
- Links:
  - [[https://moosadee.gitlab.io/courses/][R.W.'s courses homepage]]
  - [[https://gitlab.com/moosadee/courses/-/blob/main/current/cs235/student-repo/wk07_SmartPointers/instructions.org][Assignment direct link]]

# ------------------------------------------------------------------------------

* Included files:

#+BEGIN_SRC
wk07_SmartPointers
├── instructions.org
├── practice1_unique_pointers
│   └── uniqueptr.cpp
└── practice2_shared_pointers
    └── sharedptr.cpp
#+END_SRC

# ------------------------------------------------------------------------------

* Practice programs

** Practice 1 - Unique Pointer

*** Reference

- Documentation page: [[https://cplusplus.com/reference/memory/unique_ptr/][https://cplusplus.com/reference/memory/unique_ptr/]]

Creating a dynamic array:

#+BEGIN_SRC form
  // USING A VANILLA POINTER:
  string* arr = new string[SIZE];

  // USING UNIQUE POINTER:
  unique_ptr<string[]> arr = unique_ptr<string[]>( new string[SIZE] );

  // USING UNIQUE POINTER, BUT SHORTER:
  auto arr = unique_ptr<string[]>( new string[SIZE] );
#+END_SRC

Moving pointer to point to new address:

#+BEGIN_SRC form
  // USING A VANILLA POINTER:
  myPointer = otherPointer;

  // USING UNIQUE POINTER (this changes ownership of that address):
  myPointer = move( otherPointer );
#+END_SRC

Freeing dynamic array space (vanilla pointer only):

#+BEGIN_SRC form
  delete [] arr;
#+END_SRC

*** Example output

#+BEGIN_SRC terminal
UNIQUE POINTERS

Allocate space for a new arrays using `arraySize`.
arraySize: 3, itemCount: 0
Enter a new item to add, or QUIT to quit: cs134

Adding cs134 to arr at position 0 and incrementing `itemCount`...
arraySize: 3, itemCount: 1
Enter a new item to add, or QUIT to quit: cs200

Adding cs200 to arr at position 1 and incrementing `itemCount`...
arraySize: 3, itemCount: 2
Enter a new item to add, or QUIT to quit: cs235

Adding cs235 to arr at position 2 and incrementing `itemCount`...

Resize old dynamic array...
- Allocate space for a larger array
- Copy values from old array to new array
- Free the space at the old location
- Point our array to the new location

Resize new dynamic array...
- Allocate space for a larger array
- Copy values from old array to new array
- Use `move` to change ownership of the new array address to our original `arr` pointer.
- Update the `arraySize` to the `newSize`.
arraySize: 6, itemCount: 3
Enter a new item to add, or QUIT to quit: cs250

Adding cs250 to arr at position 3 and incrementing `itemCount`...
arraySize: 6, itemCount: 4
Enter a new item to add, or QUIT to quit: QUIT

Display all array elements...
oldDynArr[0] = cs134
newDynArr[0] = cs134
oldDynArr[1] = cs200
newDynArr[1] = cs200
oldDynArr[2] = cs235
newDynArr[2] = cs235
oldDynArr[3] = cs250
newDynArr[3] = cs250
(Old dynamic array only) - Free the memory!!

THE END
#+END_SRC

*** Instructions

This program starts with a dynamic array going through the steps, but you will
add a second array using the =unique_ptr= class.

1. Beneath the "Allocate space for a new arrays using `arraySize`." output,
   I'm declaring a dynamic array using a traditional pointer:
   =oldDynArr = new string[arraySize];= ... You'll do the same, but with the
   =newDynArr= unique pointer.
2. Within the while loop, in the =else= case it adds a new item =text= to each
   of the two arrays. The syntax will be the same for both.
3. Within the =if ( itemCount == arraySize )= branch, it will check if the
   arrays are full. The first scope (within ={}=) shows steps to "resize"
   the dynamic array. Afterwards, you'll do the same for the =unique_ptr=
   version of the dynamic array. Do the following:
   - Create a =newArr= dynamic array (also a =unique_ptr=). Set it up to use
     =newSize= as the size.
   - In a for loop, copy values from =newDynArr= (the old, small array) to the
     =newArr= (the new, big array).
   - After the for loop, use the =move= function to change the ownership
     of =newArr= to the =newDynArr=.

# ------------------------------------------------------------------------------

** Practice 2 - Shared Pointer

*** Reference

- Documentation page: [[https://cplusplus.com/reference/memory/shared_ptr/][https://cplusplus.com/reference/memory/shared_ptr/]]

Allocating space for a new variable:

#+BEGIN_SRC form
  // USING A VANILLA POINTER:
  string* var = new string;

  // USING SHARED POINTER:
  shared_ptr<string> var = shared_ptr<string>( new string );

  // USING SHARED POINTER, BUT SHORTER:
  auto var = shared_ptr<string>( new string );
#+END_SRC

Pointing to an existing address:

#+BEGIN_SRC form
  // USING A VANILLA POINTER:
  string* pointer = otherPointer;

  // USING SHARED POINTER:
  shared_ptr<string> pointer = shared_ptr<string>( otherPointer );

  // USING SHARED POINTER, BUT SHORTER:
  auto pointer = shared_ptr<string>( otherPointer );
#+END_SRC

Freeing dynamic variable space (vanilla pointer only):

#+BEGIN_SRC form
  delete var;
#+END_SRC

*** Example output

#+BEGIN_SRC terminal
SHARED POINTERS

Allocate space for nodes the old way...
OldNode: A
OldNode: B
OldNode: C

Allocate space for nodes the new way...
NewNode: X
NewNode: Y
NewNode: Z

Set up Node neighbors for OldNodes...

Set up Node neighbors for NewNodes...

Display OldNode list... A... B... C... 

Display NewNode list... X... Y... Z... 

(NewNode list only) - Display use counts :)
newFirst: 2
newFirst->ptrNext: 3
newFirst->ptrNext->ptrNext: 2

(OldNode list only) - Free all OldNodes!

THE END
#+END_SRC

*** Instructions

With this program we're creating a simple list data structure. Two Node types
are declared, =OldNode= uses traditional pointers, but =NewNode= uses the
=shared_ptr= pointers. I've implemented the old-style list, and you'll do the
same with the new-style list.

1. After the message "Allocate space for nodes the new way...", create 3
   =shared_ptr<NewNode>= objects - =newFirst=, =newSecond=, =newThird=.
   Initialize them to "X", "Y", and "Z". The syntax is a bit ugly:
   =auto newFirst = shared_ptr<NewNode>( new NewNode( "X" ) );=
2. After the "Set up Node neighbors for NewNodes..." message, follow the
   oldFirst/oldSecond/oldThird pointer updates above to set the neighbors;
   the syntax will be the same. (nullptr <- 1 <-> 2 <-> 3 -> nullptr)
   - NOTE: You don't have to initialize any of the pointers to nullptr;
     that happens in the Node constructors.
3. For the "Display NewNode list..." section, you'll need a "walker" pointer
   to point to wherever it's currently visiting in the list, starting
   at the first Node.
   1. Initialize it to the first node:
      =auto ptrCurrent = shared_ptr<NewNode>( newFirst );=
   2. Create a while loop, loop while =ptrCurrent= is not nullptr.
      Within the while loop:
      - Display =ptrCurrent='s data.
      - Set =ptrCurrent= equal to =ptrCurrent->ptrNext= to have it step forward.
4. Under the "Display use counts" section, use the =.use_count()= that is
   part of the =shared_ptr= class. Display each Node's use count.
