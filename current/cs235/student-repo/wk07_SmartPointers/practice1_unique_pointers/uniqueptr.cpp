7#include <memory>
#include <iostream>
#include <string>
using namespace std;

int main()
{
    cout << endl << "UNIQUE POINTERS" << endl;

    int arraySize = 3;
    int itemCount = 0;

    string* oldDynArr = nullptr;
    unique_ptr<string[]> newDynArr;

    cout << endl << "Allocate space for a new arrays using `arraySize`." << endl;
    oldDynArr = new string[arraySize];


    bool done = false;
    while ( !done )
    {
        // Display array information
        cout << "arraySize: " << arraySize
             << ", itemCount: " << itemCount << endl;

        cout << "Enter a new item to add, or QUIT to quit: ";
        string text;
        getline( cin, text );
        cout << endl;

        if ( text == "QUIT" )
        {
            done = true;
        }
        else
        {
            cout << "Adding " << text << " to arr at position "
                 << itemCount << " and incrementing `itemCount`..."
                 << endl;

            // Add the new item to the arrays
            oldDynArr[itemCount] = text;


            // Add to the item count
            itemCount++;
        }

        // Check if full, resize if so
        if ( itemCount == arraySize )
        {
            int newSize = arraySize + 3;
            
            {   // Begin scope
                cout << endl << "Resize old dynamic array..." << endl;
                cout << "- Allocate space for a larger array" << endl;
                string* newArr = new string[newSize];
                
                cout << "- Copy values from old array to new array" << endl;
                for ( int i = 0; i < arraySize; i++ )
                {
                    newArr[i] = oldDynArr[i];
                }

                cout << "- Free the space at the old location" << endl;
                delete [] oldDynArr;

                cout << "- Point our array to the new location" << endl;
                oldDynArr = newArr;
            }   // End scope

            {   // Begin scope
                cout << endl << "Resize new dynamic array..." << endl;
                cout << "- Allocate space for a larger array" << endl;


                cout << "- Copy values from old array to new array" << endl;


                cout << "- Use `move` to change ownership of the new"
                     << " array address to our original `arr` pointer." << endl;

            }   // End scope

            cout << "- Update the `arraySize` to the `newSize`." << endl;
            arraySize = newSize;
        }
    }

    cout << "Display all array elements..." << endl;
    for ( int i = 0; i < itemCount; i++ )
    {
        cout << "oldDynArr[" << i << "] = " << oldDynArr[i] << endl;
        cout << "newDynArr[" << i << "] = " << newDynArr[i] << endl;
    }

    cout << "(Old dynamic array only) - Free the memory!!" << endl;
    delete [] oldDynArr;

    cout << endl << "THE END" << endl;
    return 0;
}
