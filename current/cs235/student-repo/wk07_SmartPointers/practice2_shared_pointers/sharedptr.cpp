#include <memory>
#include <iostream>
#include <string>
using namespace std;

struct OldNode
{
    OldNode() {}
    OldNode( string d )
    {
        cout << "OldNode: " << d << endl;
        data = d;
        ptrNext = nullptr;
        ptrPrev = nullptr;
    }
    string data;
    OldNode* ptrNext;
    OldNode* ptrPrev;
};

struct NewNode
{
    NewNode() {}
    NewNode( string d )
    {
        cout << "NewNode: " << d << endl;
        data = d;
        ptrNext = nullptr;
        ptrPrev = nullptr;
    }
    string data;
    shared_ptr<NewNode> ptrNext;
    shared_ptr<NewNode> ptrPrev;
};

int main()
{
    cout << endl << "SHARED POINTERS" << endl;

    cout << endl << "Allocate space for nodes the old way..." << endl;
    OldNode* oldFirst  = new OldNode( "A" );
    OldNode* oldSecond = new OldNode( "B" );
    OldNode* oldThird  = new OldNode( "C" );

    cout << endl << "Allocate space for nodes the new way..." << endl;


    cout << endl << "Set up Node neighbors for OldNodes..." << endl;
    oldFirst->ptrNext = oldSecond;
    oldSecond->ptrPrev = oldFirst;
    oldSecond->ptrNext = oldThird;
    oldThird->ptrPrev = oldSecond;
    
    cout << endl << "Set up Node neighbors for NewNodes..." << endl;

    cout << endl;
    
    {
        cout << "Display OldNode list... ";
        OldNode* ptrCurrent = oldFirst;
        while ( ptrCurrent != nullptr )
        {
            cout << ptrCurrent->data << "... ";
            ptrCurrent = ptrCurrent->ptrNext;
        }
        cout << endl;
    }

    {
        cout << endl << "Display NewNode list... ";

    }
    cout << endl << endl;

    cout << "(NewNode list only) - Display use counts :)" << endl;
    cout << "newFirst: "

         << endl;
    cout << "newFirst->ptrNext: "

         << endl;
    cout << "newFirst->ptrNext->ptrNext: "

         << endl;
    cout << endl;

    cout << "(OldNode list only) - Free all OldNodes!" << endl;
    delete oldFirst->ptrNext->ptrNext;
    delete oldFirst->ptrNext;
    delete oldFirst;

    cout << endl << "THE END" << endl;
    return 0;
}

