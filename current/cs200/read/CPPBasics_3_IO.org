# -*- mode: org -*-

* *Interactivity and Output in C++*

Most programs feature interactivity in some way or another. This can involve moving a mouse around, clicking on things,
tapping on a touch screen, using a gamepad's joysticks and buttons, but we'll be primarily interacting with our C++
programs through keyboard input. Feedback from our C++ programs will come in the form of text output to the screen,
as we are writing terminal programs.


--------------------------------------------------------------------------------
*Program output:*

#+BEGIN_SRC
Enter number 1: 1300
Enter number 2: 37

1300 + 37 = 1337
#+END_SRC
--------------------------------------------------------------------------------

We will also delve into reading from and writing to text files (or other file formats) later on. But for now,
let's focus on the terminal/console.


#+BEGIN_hint
*Terminology*

"Terminal", "Console", "Bash", "Command Line" tend to be used interchangably.
A terminal is a text-based interface you can use to interact with your computer.
In order to start simple with introductory programming, we begin by /just/
thinking about Command Line Interfaces and writing programs that deal with text.
#+END_hint

#+ATTR_LATEX: :width 100px
[[file:images/c2_u01_terminal-hi.png]]

#+BEGIN_LATEX
\vspace{2cm}
#+END_LATEX

* *Outputting Information with =cout=*

The =cout= command (pronounced as /"c-out"/ for "console-out") is used to write information to the screen.
This can include outputting a string literal:

--------------------------------------------------------------------------------
*C++ source:*

#+BEGIN_SRC
cout << "Hello, world!" << endl;
#+END_SRC
--------------------------------------------------------------------------------

or a variable's value:

--------------------------------------------------------------------------------
*C++ source:*

#+BEGIN_SRC
cout << yourName << endl;
#+END_SRC
--------------------------------------------------------------------------------

or stringing multiple things together:

--------------------------------------------------------------------------------
*C++ source:*

#+BEGIN_SRC
cout << "Hello, " << yourName << "!" << endl;
#+END_SRC
--------------------------------------------------------------------------------

In C++, we use the *output stream operator* =<<= to string together multiple items for our output.



#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

** *Newlines with endl*

The =endl= command stands for /"end-line"/ and ensures there is a vertical space between that =cout= statement and the next one.
For example, if we write two =cout=
statements without endl like this:

--------------------------------------------------------------------------------
*C++ source:* cout statement without endl

#+BEGIN_SRC
cout << "Hello";
cout << "World";
#+END_SRC

*Program output:*

#+BEGIN_SRC
HelloWorld
#+END_SRC
--------------------------------------------------------------------------------

If we want to separate them on two different lines, we can write:

--------------------------------------------------------------------------------
*C++ source:* cout statement with endl

#+BEGIN_SRC
cout << "Hello" << endl;
cout << "World";
#+END_SRC

*Program output:*

#+BEGIN_SRC
Hello
World
#+END_SRC
--------------------------------------------------------------------------------

Remember that in C++, a statement ends with a semicolon ;, so you can split your =cout=
statement across multiple lines, as long as you're chaining items together with =<<= and only adding a =;= on the last line:

--------------------------------------------------------------------------------
*C++ source:* Displaying several string literals and variable values

#+BEGIN_SRC
cout << "Name:   " << name
     << "Age:    " << age
     << "State:  " << state << endl;
#+END_SRC
--------------------------------------------------------------------------------

#+BEGIN_LATEX
\vspace{2cm}
#+END_LATEX

* *Inputting Information with =cin=*

When we want the user to enter a value for a variable using the keyboard, we use the =cin= command
(pronounced as "c-in" or "console-in").

For variables like int and float, you will use this format to store data from the keyboard into the variable:




#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

** *Using =cin >>= for variables*

--------------------------------------------------------------------------------
*C++ source:* Reading input into one variable

#+BEGIN_SRC
cin >> VARIABLENAME;
#+END_SRC
--------------------------------------------------------------------------------

You can also chain cin statements together to read multiple values for multiple variables:

--------------------------------------------------------------------------------
*C++ source:* Inputting data into multiple variables at once (separated by spaces)

#+BEGIN_SRC
cin >> VARIABLENAME1 >> VARIABLENAME2 >> ETC;
#+END_SRC
--------------------------------------------------------------------------------

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

** *Strings and =cin >>=*

When using =cin >>= with a string variable, keep in mind that it will only read until the first whitespace character,
meaning it can't capture spaces or tabs. For example:

--------------------------------------------------------------------------------
*C++ source:* Getting one word (ends at spaces) with the input stream operator

#+BEGIN_SRC
string name;
cin >> name;
#+END_SRC
--------------------------------------------------------------------------------

If you enter "Rachel Singh", name will contain "Rachel". To capture spaces, you need to use a different function.


#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

** *Using =getline(cin, var);= for Strings*

You can use the getline function to capture an entire line of text as a string. This is useful when you want to capture spaces and multiple words. For example:

--------------------------------------------------------------------------------
*C++ source:* Getting a full line of text (including spaces) with the getline function

#+BEGIN_SRC
string name;
getline(cin, name);
#+END_SRC
--------------------------------------------------------------------------------

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

** *Mixing =cin >> var;= and =getline(cin, var);=*

If you mix =cin >> var;= and =getline(cin, var);=,
you might encounter issues with the input buffer.
To avoid this, use =cin.ignore();= before =getline(cin, var);= if you used =cin >> var;= before it.

--------------------------------------------------------------------------------
*C++ source:*

#+BEGIN_SRC
int number;
string text;

cin >> number;
cin.ignore();
getline( cin, text );
#+END_SRC
--------------------------------------------------------------------------------

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

** *Escape Sequences*

There are special characters, called escape sequences, that you can use in your =cout= statements:

| Character | Description                    |
|-----------+--------------------------------|
| =\n=      | newline (equivalent to =endl=) |
| =\t=      | tab                            |
| =\"=      | double quote                   |

Example code:

*** *Example: Using the newline character*

--------------------------------------------------------------------------------
*C++ source:* Using the newline character

#+BEGIN_SRC
cout << "\"hello\nworld\"" << endl;
#+END_SRC

*Program output:*

#+BEGIN_SRC
"hello
world"
#+END_SRC
--------------------------------------------------------------------------------

*** *Example: Using the tab character*
--------------------------------------------------------------------------------
*C++ source:* Using the tab character in outputs

#+BEGIN_SRC
cout << "A\tB\tC" << endl;
cout << "1\t2\t3" << endl;
#+END_SRC

*Program output:*

#+BEGIN_SRC
A       B       C
1       2       3
#+END_SRC
--------------------------------------------------------------------------------

*** *Example: Displaying text with double quotes in it*

--------------------------------------------------------------------------------
*C++ source:* Displaying text with double quotes in it

#+BEGIN_SRC
cout << "He said \"Hi!\" to me!" << endl;
#+END_SRC

*Program output:*

#+BEGIN_SRC
He said "Hi!" to me!
#+END_SRC
--------------------------------------------------------------------------------

#+BEGIN_LATEX
\vspace{2cm}
#+END_LATEX

* *Review questions:*

1. What is a "console" (aka "terminal")?
2. What does "=cout=" stand for?
3. What does "=cin=" stand for?
4. The "=endl=" command is used for...
5. The =\t= command is used for...
6. The =\n= command is used for...
7. The "=getline=" function is used for...
8. When do you need to have =cin.ignore();= in your code?
