#include <iostream>
#include <fstream>
#include <string>
using namespace std;

std::string Replace( std::string fulltext, std::string findme, std::string replacewith )
{
  std::string updated = fulltext;
  size_t index = updated.find( findme );

  while ( index != std::string::npos )
    {
      // Make the replacement
      updated = updated.replace( index, findme.size(), replacewith );

      // Look for the item again
      index = updated.find( findme );
    }

  return updated;
}

bool Contains( std::string haystack, std::string needle, bool caseSensitive )
{
  std::string a = haystack;
  std::string b = needle;

  if ( !caseSensitive )
    {
      for ( unsigned int i = 0; i < a.size(); i++ )
        {
          a[i] = tolower( a[i] );
        }

      for ( unsigned int i = 0; i < b.size(); i++ )
        {
          b[i] = tolower( b[i] );
        }
    }

  return ( a.find( b ) != std::string::npos );
}



int main( int argCount, char* args[] )
{
  string inpath, outpath;
  if ( argCount < 3 )
    {
      cout << args[0] << " <inputfile> <outputfile>" << endl;
      return 1;
    }

  inpath = string( args[1] );
  outpath = string( args[2] );

  ifstream input( inpath );
  ofstream output( outpath );

  string buffer;
  while ( getline( input, buffer ) )
    {
      if ( Contains( buffer, "#+BEGIN_SRC cpp :class cpp", false ) )
        {
          output << "#+BEGIN_HTML" << endl << "\\begin{lstlisting}[style=cpp]" << endl;
        }
      else if ( Contains( buffer, "#+BEGIN_SRC terminal :class terminal", false ) )
        {
          output << "#+BEGIN_HTML" << endl << "\\begin{lstlisting}[style=terminal]" << endl;
        }
      else if ( Contains( buffer, "#+BEGIN_SRC fileout :class fileout", false ) )
        {
          output << "#+BEGIN_HTML" << endl << "\\begin{lstlisting}[style=textfile]" << endl;
        }
      else if ( Contains( buffer, "#+END_SRC", false ) )
        {
          output << "\\end{lstlisting}" << endl << "#+END_HTML" << endl;
        }
      else if ( Contains( buffer, "ATTR_HTML", false ) ||
                Contains( buffer, "#+BEGIN_HTML", false ) ||
                Contains( buffer, "#+END_HTML", false ) )
        {
          // Skip
        }
      else
        {
          output << buffer << endl;
        }
    }

  return 0;
}
