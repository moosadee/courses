# -*- mode: org -*-

Switch statements are a special type of branching mechanism that *only* checks
if the value of a variable is *equal to* one of several values. Switch
statements can be useful when implementing a menu in a program, or something
else where you only have a few, finite, discrete options.

In C++, switch statements only work with primitive data types, like integers
and chars - not strings.

#+BEGIN_SRC cpp :class cpp
switch ( VARIABLE )
{
 case VALUE1:
   // Do thing
 break;

 case VALUE2:
   // Do thing
 break;

 default:
  // Default code
}
#+END_SRC

With a switch statement, each *case* is one equivalence expression. The
=default= case is executed if none of the previous cases are.

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

#+BEGIN_SRC cpp :class cpp
case VALUE1: // equivalent to: if ( VARIABLE == VALUE1 )
#+END_SRC

#+BEGIN_SRC cpp :class cpp
case VALUE2: // equivalent to: if ( VARIABLE == VALUE2 )
#+END_SRC

#+BEGIN_SRC cpp :class cpp
break: // required at the end of a case statement (unless...)
#+END_SRC

#+BEGIN_SRC cpp :class cpp
default: // equivalent to: else
#+END_SRC

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*The default case* is not required, just like how the *else* clause is not
required in an if statement.

#+BEGIN_LATEX
\vspace{2cm}
#+END_LATEX

*The =break;= statement*

The end of each case should have a =break;= statement at the end. If the
=break= is not there, then it will continue executing each subsequent case's
code until it /does/ hit a break.

This behavior is "flow-through", and it can be used as a feature if it
matches the logic you want to write.

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*Variables inside cases*

If you're *declaring a variable* within a *case* statement, you need to enclose
your case with curly braces ={ }=, otherwise you'll get compiler errors:

#+BEGIN_SRC cpp :class cpp
switch ( operation )
{
 case 'A':
   {
     float result = num1 + num2;
     cout << "Result: " << result << endl;
   }
  break;
}
#+END_SRC

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*Calculator example:*

Perhaps you are implementing a calculator, and want to get an option from the
user: (A)dd, (S)ubtract, (M)ultiply, or (D)ivide. You could store their choice
in a =char= variable called =operation= and then use the =switch= statement to
decide what kind of computation to do:

*C++ source:* Basic calculator

#+BEGIN_SRC cpp :class cpp
switch ( operation )
{
 case 'A': // if ( operation == 'A' )
  result = num1 + num2;
  break;

 case 'S': // else if ( operation == 'S' )
  result = num1 - num2;
  break;

 case 'M': // else if ( operation == 'M' )
  result = num1 * num2;
  break;

 case 'D': // else if ( operation == 'D' )
  result = num1 / num2;
  break;
}

cout << "Result: " << result << endl;
#+END_SRC

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*Flow-through example:*

Sometimes you want to check if your variable equals a value "x" or "y", and
execute the same code for each. You can use flow-through in this case. If no
=break;= is given for a specific =case=, then it will continue to execute code
for the following =case= until a =break;= is found.

#+BEGIN_SRC cpp :class cpp
char choice;
cout << "Do you want to quit? (Y/N): ";
cin >> choice;

switch ( choice )
{
 case 'Y':
 case 'y':
  done = true;
  break;

case 'N':
case 'n':
  done = false;
  break;

default:
  cout << "Unknown selection!" << endl;
}
#+END_SRC
