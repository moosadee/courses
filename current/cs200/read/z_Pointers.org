# -*- mode: org -*-

[[file:images/reading_u06_Pointers_pointer.png]]

* *Memory addresses*

When we declare a variable, what we're actually doing is telling
the computer to set aside some *memory* (in RAM) to hold
some information. Depending on what data type we declare, a
different amount of memory will need to be reserved for that variable.

| Data type | Size    |
|-----------+---------|
| boolean   | 1 byte  |
| character | 1 byte  |
| integer   | 4 bytes |
| float     | 4 bytes |
| double    | 8 bytes |

A *bit* is the smallest unit, storing just 0 or 1.
A *byte* is a set of 8 bits. With a byte, we can store
numbers from 0 to 255, for an /unsigned/ number (only 0 and
positive numbers, no negatives).

Minimum value, 0

(Decimal value = $128 \cdot 0 + 64 \cdot 0 + 32 \cdot 0 + 8 \cdot 0 + 4 \cdot 0 + 2 \cdot 0 + 1 \cdot 0$)

| place | 128 | 64 | 32 | 16 | 8 | 4 | 2 | 1 |
| value |   0 |  0 |  0 |  0 | 0 | 0 | 0 | 0 |

Maximum value, 255

(Decimal value = $128 \cdot 1 + 64 \cdot 1 + 32 \cdot 1 + 8 \cdot 1 + 4 \cdot 1 + 2 \cdot 1 + 1 \cdot 1$)

| place | 128 | 64 | 32 | 16 | 8 | 4 | 2 | 1 |
| value |   1 |  1 |  1 |  1 | 1 | 1 | 1 | 1 |

A =char= only needs one byte to store a single letter because
we represent letters with the ASCII or UTF8 codes 65 - 90 for upper-case,
and 97 - 122 for lower-case.

|  A |  B |  C |  D |  E |  F |  G |  H |  I |  J |  K |  L |  M |
| 65 | 66 | 67 | 68 | 69 | 70 | 71 | 72 | 73 | 74 | 75 | 76 | 77 |
|----+----+----+----+----+----+----+----+----+----+----+----+----|
|  N |  O |  P |  Q |  R |  S |  T |  U |  V |  W |  X |  Y |  Z |
| 78 | 79 | 80 | 81 | 82 | 83 | 84 | 85 | 86 | 87 | 88 | 89 | 90 |

|   a |   b |   c |   d |   e |   f |   g |   h |   i |   j |   k |   l |   m |
|  97 |  98 |  99 | 100 | 101 | 102 | 103 | 104 | 105 | 106 | 107 | 108 | 109 |
|-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----|
|   n |   o |   p |   q |   r |   s |   t |   u |   v |   w |   x |   y |   z |
| 110 | 111 | 112 | 113 | 114 | 115 | 116 | 117 | 118 | 119 | 120 | 121 | 122 |


Any of these numbers can be stored with 8 bits:

A = 65...

(Decimal value = $128 \cdot 0 + 64 \cdot 1 + 32 \cdot 0 + 8 \cdot 0 + 4 \cdot 0 + 2 \cdot 0 + 1 \cdot 1$)

| place | 128 | 64 | 32 | 16 | 8 | 4 | 2 | 1 |
| value |   0 |  1 |  0 |  0 | 0 | 0 | 0 | 1 |

z = 122...

(Decimal value = $128 \cdot 0 + 64 \cdot 1 + 32 \cdot 1 + 8 \cdot 1 + 4 \cdot 0 + 2 \cdot 1 + 1 \cdot 0$)

| place | 128 | 64 | 32 | 16 | 8 | 4 | 2 | 1 |
| value |   0 |  1 |  1 |  1 | 1 | 0 | 1 | 0 |


So when we *declare* a variable, the computer finds an available
space in memory and reserves the appropriate amount of bytes in memory.
For example, our *char* could be assigned the *memory address*
=0xabc008= in memory, and its value being stored would look like this:

| ...007 | ...008 | ...009 | ...00a | ...00b | ...00c | ...00d | ...00e | ...00f | ...010 |
|      ... |      0 |      1 |      1 |      1 |      1 |      0 |      1 |      0 | ...    |

(...007 and ...010 are spaces in memory taken by something else)


We can view the addresses of variables in our program by using the
*address-of* operator =&=. Note that we have used the ampersand
symbol before to declare pass-by-reference parameters, but this is
a different symbol used in a different context.

#+BEGIN_SRC cpp :class cpp
#include <iostream>
using namespace std;

int main()
{
  int number1 = 10;
  int number2 = 20;

  cout << &number1 << "\t"
       << &number2 << endl;

  return 0;
}
#+END_SRC

Running the program, it would display the memory addresses
for these two variables. Notice that they're 4 bytes apart
in memory (one is at ...70 and one is at ...74):

--------------------------------------------------------------------------------
*C++ source:*

#+BEGIN_SRC
#+END_SRC
--------------------------------------------------------------------------------

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
0x7ffd3a24cc70	0x7ffd3a24cc74
#+END_SRC

Each time we run the program, we will get different memory addresses,
since the operating system reclaims that memory when the program ends,
and gives us new memory spaces to allocate next time we run the program.

--------------------------------------------------------------------------------
*C++ source:*

#+BEGIN_SRC
#+END_SRC
--------------------------------------------------------------------------------

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
0x7ffe0e708a80	0x7ffe0e708a84
#+END_SRC


When we declare an *array* of integers of size $n$,
the program asks for $n \times 4$ bytes of memory to work with.
The two variables above didn't /have/ to be side-by-side
in memory; they just happened to be because they were declared
close together. With an array, however, *all elements of the array*
will be *contiguous* (side-by-side) in memory.


Here we have an array of integers:

#+BEGIN_SRC cpp :class cpp
int arr[3];

for ( int i = 0; i < 3; i++ )
  {
    cout << &arr[i] << "\t";
  }
#+END_SRC

And the output:

--------------------------------------------------------------------------------
*C++ source:*

#+BEGIN_SRC
#+END_SRC
--------------------------------------------------------------------------------

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
0x7ffd09a130c0	0x7ffd09a130c4	0x7ffd09a130c8
#+END_SRC

Because the elements of an array must be contiguous in memory,
we /cannot/ resize a normal array. After our array is
declared, chances are the memory addresses right after it
will be put to use elseware on the computer and will be unavailable
to our program and our array.

But, after we learn about pointers, we will learn how we can
dynamically allocate as much memory as we need at any time during
our program's execution - giving us the ability to "resize"
arrays by allocating space for a /new/ array, copying
the data over to the larger chunk of memory, and deallocating
the old chunk of memory from the smaller array.





-----------------------------------------------

* *Pointers*

** *Creating pointer variables*

We can declare special variables that store memory addresses
rather than storing an int or float or char value. These variables
are called *pointers*.

We can declare a pointer like this: =int* ptrNumber;=
Or like this: =int * ptrNumber;=
Or like this: =int *ptrNumber;=


But note that doing this declares one pointer and several integers:
=int * ptrNumber, notAPointer1, notAPointer2;=
To avoid confusion, \underline{declare multiple pointers on separate lines}.


If we declare a pointer as an =int*= type, then it will only
be able to point to the addresses of *integer variables*,
and likewise for any other data type.

#+ATTR_HTML: :class hint
#+NAME: content
#+BEGIN_HTML
*Context:* Safety with pointers!

Remember how variables in C++ store *garbage* in them
initially? The same is true with pointers - it will store
a garbage memory address. This can cause problems if
we try to work with a pointer while it's storing garbage.


To play it safe, any pointer that is not currently in use
should be initialized to =NULL= or =nullptr=.
#+END_HTML

Declaring a pointer and initializing it to =nullptr=:
#+BEGIN_SRC cpp :class cpp
  #include <iostream>
  using namespace std;

  int main()
  {
    int * ptr = nullptr;

    return 0;
  }
#+END_SRC

** *Assigning pointers to addresses*

Once we have a pointer, we can point it to the address of any
variable with a matching data type. To do this, we have to use the
*address-of* operator to access the variable's address -
this is what gets stored as the pointer's value.

- Assigning an address during declaration: =int * ptr = &somevariable;=
- Assigning an address /after/ declaration: =ptr = &somevariable;=


After assigning an address to a pointer, if we =cout=
the pointer it will display the memory address of the /pointed-to/ variable - just like if
we had used =cout= to display the /address-of/ that variable.

#+BEGIN_SRC cpp :class cpp
  // Shows the same address
  cout << ptr << endl;
  cout << &somevariable;
#+END_SRC

Here's a simple program that has an integer =var= with
a value of 10, and a pointer =ptr= that points to =var='s
address.

#+BEGIN_SRC cpp :class cpp
  #include <iostream>
  using namespace std;

  int main()
  {
    int * ptr;
    int var = 10;

    ptr = &var;

    cout << "var address: " << &var << endl;
    cout << "ptr address: " << &ptr << endl;
    cout << "var value:   " << var << endl;
    cout << "ptr value:   " << ptr << endl;

    return 0;
  }
#+END_SRC

The output would look like:

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
var address: 0x7ffc3d6028b0
ptr address: 0x7ffc3d6028b4
var value:   10
ptr value:   0x7ffc3d6028b0
#+END_SRC

Some things to note:
-   =var= stores its data at its address =0x7ffc3d6028b0=.
-   =ptr= stores its data at its address =0x7ffc3d6028b4=.
-   =var='s value is 10, since it's an integer.
-   =ptr='s value is the address of =var=, since it's a pointer-to-an-integer.


[[file:images/c2_u08_ptrmemory.png]]



** *Dereferencing pointers to get values*

Once the pointer is pointing to the address of a variable, we can
/access/ that pointed-to variable's value by /dereferencing/
our pointer. This gives us the ability to read the value stored
at that memory address, or overwrite the value stored at that memory address.

We *dereference* the pointer by prefixing the pointer's name
with a =*= - again, another symbol being reused but in a different
context.

In this code, we point =ptr= to the address of =var=.
Outputting =ptr= will give us =var='s address,
and outputting =*ptr= (ptr dereferenced) will give us
the value stored at =var='s address.

#+BEGIN_SRC cpp :class cpp
  int * ptr;
  int var = 10;

  ptr = &var;

  cout << "ptr value:        " << ptr << endl;
  cout << "ptr dereferenced: " << *ptr << endl;
#+END_SRC

Output:

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
ptr value:        0x7ffd21de775c
ptr dereferenced: 10
#+END_SRC


Then, we could also overwrite the value stored at =var='s
address by again dereferencing the pointer and writing an assignment statement:

#+BEGIN_SRC cpp :class cpp
*ptr = 20;
#+END_SRC

 When we output the value that =var= is storing, either
directly through =var= or through the =ptr=, we
can see that the value of =var= has been overwritten:

#+BEGIN_SRC cpp :class cpp
  cout << "var value:        " << var << endl;
  cout << "*ptr value:       " << *ptr << endl;
#+END_SRC

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
var value:        20
*ptr value:       20
#+END_SRC



--------------------------------------------------------------------------------

* *Pointer cheat sheet*

| *Declare a pointer*         | =int* ptrInt;=               |
|                             | =float *ptrFloat;=           |
| *Assign pointer to address* | =ptrInt = &intVar;=          |
|                             | =ptrFloat = &floatVar;=      |
| *Dereference a pointer*     | =cout << *ptrChar;=          |
|                             | =*ptrInt = 100;=             |
| *Assign to nullptr*         | =float *ptrFloat = nullptr;= |
|                             | =ptrChar = nullptr;=         |




--------------------------------------------------------------------------------
* *Invalid memory access with pointers*

Remember that when you declare a variable in C++, it will initially
store *garbage*. This is true of pointers as well.
When you declare a pointer without initializing it to =nullptr=,
it will be storing random garbage that it will try to interpret as a
memory address.
If you *dereference* a pointer that is pointing to garbage,
your program is going to run into a problem - a segmentation fault.

 A pointer pointing to garbage:
#+BEGIN_SRC cpp :class cpp
  int main()
  {
    int * bob;
    cout << *bob << endl;

    return 0;
  }
#+END_SRC

Program output:
#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Segmentation fault (core dumped)
#+END_SRC

- How do we check whether memory address is valid? ::
In short, we can't check if an address stored in a pointer is
valid. This is why we initialize our pointers to =nullptr=
when they're not in use - we know =nullptr= means
that the pointer is not pointing to anything.




----------------------------

* *Review questions:*

#+ATTR_HTML: :class action-item
#+NAME: content
#+BEGIN_HTML
1. Given the variable declaration:
   =int num = 10;=
   What kind of memory is the num variable stored in?
2. Given the variable declaration:
   =int* num = new int;=
   What kind of memory is the num variable stored in?
3. How do you *declare* a pointer?
4. How do you *access the address of* a variable?
5. How do you assign the *address of a variable* to a pointer?
6. How do you *dereference* a pointer to display the value of the address it's pointing to?
7. How do you *dereference* a pointer to store a new value in the memory block pointed to?
8. When a pointer is not currently in use, it should be initialized to...
#+END_HTML
