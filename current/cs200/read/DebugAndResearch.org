# -*- mode: org -*-

* *Syntax*


#+BEGIN_hint
*What is /syntax/?*

syntax, noun - The way in which linguistic elements (such as words) are put together to form constituents (such as phrases or clauses)

From the [[https://www.merriam-webster.com/dictionary/syntax][Merriam-Webster Dictionary]].
#+END_hint


C++, Java, C#, and other "C-like" languages follow similar syntax rules.

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*Lines of code:*

A code statement ends with a semi-colon. Statements are single
commands, like using =cout= ("console out") to display text to the screen,
assigning a value to a variable, and other simple operations.

--------------------------------------------------------------------------------
*C++ source:*

#+BEGIN_SRC
string state;               // Variable declaration
cout << "Enter state: ";    // Display text to screen
cin >> state;               // Getting input from keyboard
cout << state << endl;      // Displaying text to the screen
#+END_SRC
--------------------------------------------------------------------------------

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*Comments:*

Comments are notes left in the code by humans for humans. They can
help us remember what our code was doing later on, or help guide someone else reading
through our code. There are two ways we can write comments in C++.

If we use =//= then all text afterwards will be a comment. This is a single-line comment.
If we use =/*= then all text will be a comment, only ending once we reach a =*/=. This is a multi-line comment.

--------------------------------------------------------------------------------
*C++ source:*

#+BEGIN_SRC
string state; // Variable declaration

/*
Next we're going to search for this state
in the list of all states that match xyz criteria...
*/
#+END_SRC
--------------------------------------------------------------------------------

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*Code blocks:*

There are certain types of instructions that /contain/ additional code.
If statements, for example, contain a set of instructions to execute /only if/ the *condition* given
evalutes to =true=. Any time an instruction /contains/ additional instructions, we use
opening and closing curly braces ={ }= to contain this internal code. Note that an
if statement and other structures that /contain/ code don't end with a semicolon =;=.

--------------------------------------------------------------------------------
*C++ source:*

#+BEGIN_SRC
if ( price > 100 )
{ // Code block begin
  cout << "Too expensive..." << endl;
} // Code block end
#+END_SRC
--------------------------------------------------------------------------------

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

* *Types of errors*

*Syntax errors and logic errors*


#+ATTR_LATEX: :width 0.3\textwidth
[[file:images/c2_u01_debugging2.png]]

*Syntax errors* are errors in the language rules of your program code.
This include things like *typos*, *misspellings*, or just writing something
that isn't valid C++ code, such as forgetting a =;= at the end of some instructions.
Although they're annoying, syntax errors are probably the "best" type of error
because the program won't build while they're present. Your program won't build
if you have syntax errors.


*Logic errors* are errors in the programmer's logic. This could be something like
a wrong math formula, a bad condition on an if statement, or other things where
the programmer thinks /thing A/ is going to happen, but /thing B/ happens instead.
These don't lead to build errors, so the program can still be run, but it may result
in the program crashing, or invalid data being generated, or other problems.

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*Minimizing bugs and debugging time*


#+ATTR_LATEX: :width 0.3\textwidth
[[file:images/coding-strategy.png]]

- Write a few lines of code and build after every few lines - this will help you detect
  syntax errors early.
- DON'T write the "entire program" before ever doing a build or a test - chances are
  you've written in some syntax errors, and now you'll have a long list of errors
  to sift through!
- If you aren't sure where an error is coming from, try /commenting out/ large chunks
  of your program until it's building again. Then, you can /un-comment-out/ small
  bits at a time to figure out where the problem is.


#+BEGIN_LATEX
\vspace{2cm}
#+END_LATEX

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

* *Testing software*

** *Why test?*

#+ATTR_LATEX: :width 0.6\textwidth
[[file:images/hopeitworks.png]]

*How do you know that your program actually works?*

Sure, you may have manually tested your code, running the program and typing in test data and checking the output.
After a while, manual testing becomes a chore. Maybe you start entering "asdjhfklq" as the test data and just make sure nothing breaks while running the program.
But are you sure your program runs, doesn't crash, and gives the correct output for all reasonable cases?

A skill that is good to develop as a software developer is how to test and validate your work. You can break down parts of your code into little transactions of "inputs" and "outputs", and then develop test cases.

- *Test case:* A test case is a single test. You specify some input(s) that you will give your program, and the expected output(s) it should return.

When you run the actual program with your inputs, it will return actual output(s). Compare the actual output with the expected output to validate whether the program worked as expected.
Test cases can be built without any of the program built so far. In fact, it can be handy to write your tests ahead of time so you have a better understanding of how things are supposed to work. 


#+BEGIN_LATEX
\newpage
#+END_LATEX

** *Writing test cases*

*Example: Bank withdrawals*

In this example, the program keeps track of a bank balance and the amount the user wants to withdraw, but it shouldn't let the balance fall below 0.

#+ATTR_LATEX: :mode table :align p{1cm}|l|p{3cm}|p{2cm}
| *Test case* | *Input(s)*                    | *Expected output* | *Actual output* |
|-------------+-------------------------------+-------------------+-----------------|
|           1 | balance = 100, withdraw = 10  | balance is now 90 |                 |
|           2 | balance = 0, withdraw = 100   | can't withdraw!   |                 |
|           3 | balance = 100, withdraw = 100 | balance is now 0  |                 |

You would make a list of inputs and what should be the result for each case,
and then run your program and check each scenario - does the *actual output*
match the *expected output*?
If something doesn't match, it could mean that there's an error in a calculation somewhere,
or other logic in the program. (And yes, sometimes tests can be wrong, too.)

** *Ways to test*

*Testing* is a whole job title in software development. Usually the "developers" write
the new features and maybe add *unit tests* for their code. A dedicated QA (Quality Assurance)
or SET (Software Engineer in Test) is then responsible for /thoroughly/ testing developer work.
This includes testing in multiple ways, both the feature itself "in a vacuum", and connected to the
entire system as a whole, to make sure that nothing breaks along the way.

*** *Manual testing*

Manually testing is as it sounds - the QA has a list of *test cases* in a document or wiki
and they follow those test cases to make sure the program continues working.
Often this means navigating the program's UI from the user's perspective, going through
each step to make sure nothing breaks.



*** *Integration tests*

Integration tests test the new feature *within the entire system as a whole*, integrated with
the rest of the program. Usually companies will have multiple servers, such as a /QA server/
where the program is set up like it's ready for the real world, but only used by testers and
filled with test/mock data. This is usually where integration tests happen - we want to
make sure it works before we /push to production/ (in other words, submit the new feature
to the live website or app that the actual users are using.)


*** *Unit tests*

Often the developers who write features are required to also write unit tests for those features.
Unit tests are a way to test "in a vacuum", one /unit/ at a time. They're handy for making sure
that each individual component of a feature doesn't break. Unit tests are automated - you can write
something like

--------------------------------------------------------------------------------
*C++ source:*

#+BEGIN_SRC
// Inputs for this test case
int input_balance = 100;
int input_withdraw = 10;

// What the expected result (balance) is
int expected_output = 90;

// Call the function that handles it with the
// test case inputs, its output is our "actual output".
int actual_output = Withdraw( input_balance, expected_output );

if ( actual_output == expected_output )
{
  // If the actual output matches
  // the expected output - PASS!
  cout << "Test passes!" << endl;
}
else
{
  // The actual output didn't match, so test FAILS.
  cout << "TEST FAILS!" << endl;
}
#+END_SRC
--------------------------------------------------------------------------------

Again, for reference, the first test case is written out as:


#+ATTR_LATEX: :mode table :align p{1cm}|l|p{3cm}|p{2cm}
| *Test case* | *Input(s)*                   | *Expected output* |
|-------------+------------------------------+-------------------|
|           1 | balance = 100, withdraw = 10 | balance is now 90 |


#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

** *Test driven development*

In the book *Test-Driven Development* by Kent Beck, he highlights a way
of approaching software development where you write your tests for a new feature
/first/, before writing any actual code to implement that feature.
By writing tests *first*, it helps developers think through the actual
requirements in a quantifiable way. You can read more about it on the
Wikipedia page - https://en.wikipedia.org/wiki/Test-driven_development .

I think it's an interesting way to approach design and development,
though from my experience no company I've ever worked for has used
this test-first approach. I do find this a good way to go about designing
solutions during job interview questions! (Plus it gives you stalling time
as you ask your interviewer questions about the requirements,
gives you more time to think through your design, and gives you a
way to verify your work afterwards. ;)


#+BEGIN_LATEX
\newpage
#+END_LATEX

* *Doing research as a programmer*

For developers it's a pretty common experience for people to feel like
they spend a lot of their time searching for solutions and scouring
/Stack Overflow/ posts for solutions to their troubles. In the professional
world you will be *learning your company's codebase*, as well as probably
learning *third party APIs* from other companies as well. While you may
be proficient in the language you work in, /learning other peoples' code and products/
will always be part of it. So, research is part of development.

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

** *Internal wikis and documentation*

#+ATTR_LATEX: :width \textwidth
[[file:images/reading_DebugAndResearch-NoRequirements.png]]

Companies often have some kind of internal Wiki (or Confluence, or Sharepoint)
where employees can post documentation and notes to share with others.
However, developers don't usually do much documentation on their code or
code they've explored, so usually a company's internal documentation for
their own code is pretty bad. (In my experience only the auto-generated API
docs built from code comments are okay.)

#+BEGIN_LATEX
\newpage
#+END_LATEX

** *Search engines (and AI?)*

Searching on a search engine can come up with tutorials, blogs, API docs,
message board posts, and other resources for reference. Finding help,
documentation, examples, and posts where someone else had the same problem
is a common way of working through problems, as well as asking coworkers,
sketching out the problem on paper, and testing out different approaches.

As Large Language Models ("AI") becomes more popular, it may be tempting
to try to ask it all of your questions. However, there's no /intelligence/
in this LLM-based AI, it's not thinking critically about what you've said,
and it can frequently get things wrong but in a confident manner.
I would suggest that you use it as a starting point, somewhere to get
some ideas, rather than utilizing it as actual *documentation*.

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX


** *Documentation resources*

#+CAPTION: A screenshot of the documentation page for Canvas' API, listing an action, inputs, and other information.
#+ATTR_LATEX: :width 0.9\textwidth
[[file:images/reading_DebugAndResearch-CanvasAPI.png]]

When working with third party libraries and APIs that are provided by another business
or organization they will generally have decent documentation explaining the items
you can use. Going over API documentation is a big part of software development. :)
So having the patience to read through docs to find what you need is important.

If you want to see some examples of API documentation, here are a few.
You can also search "[thing] API docs" to find documentation for a lot of things,
from meal delivery apps to catalogs of venomous frogs in Australia.

- Canvas API: https://canvas.instructure.com/doc/api/all_resources.html
- Spotify API: https://developer.spotify.com/documentation/web-api
- Reddit API: https://www.reddit.com/dev/api/

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

** *Stack Overflow and message boards*

As you do web searches for programming problems (for me, usually build error messages -
/what does it mean?/ :)) often you will see message boards with archived
posts from people with similar problems. These are pretty common to reference,
though can be limited in how helpful they are. Sometimes the poster's problem
matches yours, sometimes nobody responded, sometimes you can't quite find
what you need. But still a common resource.

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

** *Language reference resources*

#+CAPTION: An image of the cover of the book "JavaScript Pocket Reference", published by O'Reilly.
#+ATTR_LATEX: :width 0.3\textwidth
[[file:images/reading_DebugAndResearch-JSRefBook.jpg]]

It's good to keep a reference book about the language you're using as well.
These can usually be college style textbooks about the language, or maybe
a quick reference. There are also books that help you gain a deeper understanding
of the language, frameworks, and tools you use.

There are also often online video lessons that can teach you about
technologies as well. These are usually through websites like Pluralsight
or maybe Coursera or Udemy.

The software/web/mobile development landscape is always changing so as a
developer you will often need to keep up with updates that your
company makes to frameworks and technologies.

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX
