# -*- mode: org -*-

* Introduction
Computers are all about *binary* - 1's and 0's, on and off, true and false.
A large part of writing logic with our programs is all about asking "yes/no"
questions. If "criteria" is true, then execute this code... if it's false,
execute that code. In order to better understand how branching works in our
programs, it's important to learn about *boolean logic*.

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

"Boolean" refers to something that is either *true* or *false*. This can be the
values "true" and "false" itself, or it can be an expression that evaluates to
true or false. For example:

- if the user clicked "save", then...
- if the account balance is less than 0, then...
- if the "done" variable is set to true, then...

#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

* True/false questions in programming

| *Question*                           | *Represented as C++ code* |
|--------------------------------------+---------------------------|
| Is /x/ equal to /y/?                 | x == y                    |
| Is /x/ not equal to /y/?             | x != y                    |
| Is /x/ greater than /y/?             | x > y                     |
| Is /x/ greater than or equal to /y/? | x >= y                    |
| Is /x/ less than /y/?                | x < y                     |
| Is /x/ less than or equal to /y/?    | x <= y                    |
| Is /x/ true?                         | x                         |
| Is /x/ false?                        | !x                        |

*x* and *y* can be swapped out with different types of data. We can check if
two strings of text are equal, or if two numbers are equal. With the bottom
two examples, "is x?" and "is not x?" applies for if *x* is a
*boolean variable* - a variable that is storing a value of either true or false.

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX


#+ATTR_LATEX: :width 0.3\textwidth
[[file:images/reading_u03_CPPBasics_cat.png]]

#+BEGIN_SRC cpp
if ( i_have_not_been_fed && it_is_6_am ) {
  MakeLotsOfNoise();
}
#+END_SRC

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

* Compound operations with logic operators

We can also create *compound boolean expressions* from separate boolean
expressions using *logic operators*. Logic operators are AND (&&), OR (||),
and NOT (!).

- Is /x == y/ and /x == z/?
  - In C++ code: =x == y && x == z=
  - True if both sub-expressions are true (x==y and x==z).
  - False if one or both sub-expressions are false.


*Example:*

#+BEGIN_SRC cpp :class cpp
  if ( wantsBeer && isAtLeast21 && likesBeer ) {
      GiveBeer();
  }
#+END_SRC


#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX


- Is /x == y/ or /x == z/?
  - In C++ code: =x == y || x == z=
  - True if at least one sub-expression is true.
  - False if both sub-expressions are false.


*Example:*

#+BEGIN_SRC cpp :class cpp
  if ( isBaby || isSenior || isVet )
  {
      GiveDiscount();
  }
#+END_SRC

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX


- Is /x/ false?
  - In C++ code: =!x=
  - True if the sub-expression is false.
  - False is the sub-expression is true.


*Example:*

#+BEGIN_SRC cpp :class cpp
  // Just checking documentSaved is true
  if ( documentSaved ) {
      Quit();
  }

  // Checking if documentSaved is false
  if ( !documentSaved ) {
      Save();
      Quit();
  }
#+END_SRC

#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

* Boolean variables and relational operations

We can have *boolean variables* that store *true* or *false*, and we can then
do tests on those values...

#+BEGIN_SRC cpp :class cpp
  bool quit = false;

  // ( pretend there's more code here )

  if ( quit ) // If quit is true
  {
      return 0; // exit
  }
#+END_SRC

But we can also create *boolean expressions* by utilizing those
*relational operators* to compare two values together.

#+BEGIN_SRC cpp :class cpp
  float balance;
  cout << "Enter your bank balance: ";
  cin >> balance;

  if ( balance < 0 ) // balance < 0 will return true or false
  {
      cout << "OVERDRAWN!" << endl;
  }
#+END_SRC

These *relational operators* can also work on *strings*, where =<= and
=>= will check alphabetical order (sort of).

#+BEGIN_SRC cpp :class cpp
  string student1, student2;

  cout << "Enter student 1 name: ";
  getline( cin, student1 );

  cout << "Enter student 2 name: ";
  getline( cin, student2 );

  if ( student1 < student2 )
  {
      cout << "Student 1 goes first" << endl;
  }
  else
  {
      cout << "Student 2 goes first" << endl;
  }
#+END_SRC

Note that when using =<= and =>= with strings or chars, it
will compare them based on alphabetical order - /if/ they're both the
same case (both uppercase or both lowercase). The code 65 represents ='A'=,
but the code 97 represents ='a'=, so items will be "sorted" based on
these codes.

#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX


* *Truth tables*

We can use *truth tables* to help us visualize the logic that we're working with,
and to validate if our assumptions are true. Truth tables are for when we have
an expression with more than one variable (usually) and want to see the result
of using ANDs, ORs, and NOTs to combine them.



#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX


** *Truth table for NOT:*

On the top-left of the truth table we will have the boolean variables or expressions
written out, and we will write down all its possible states. With just one
variable $p$, we will only have two states: when it's true, or when it's false.


On the right-hand side (I put after the double lines) will be the result of the
expression - in this case, "not-p" $!p$. The not operation simply takes the
value and flips it to the opposite: true $\to$ false, and false $\to$ true.

#+ATTR_LATEX: :align center
| =p= | =!p= |
|-----+------|
| T   | F    |
| F   | T    |

#+BEGIN_LATEX
\vspace{2cm}
#+END_LATEX

** *Truth table for AND:*
For a truth table that deals with two variables, $p$ and $q$, the total
amount of states will be 4:

1. $p$ is true and $q$ is true.
2. $p$ is true and $q$ is false.
3. $p$ is false and $q$ is true.
4. $p$ is false and $q$ is false.

#+ATTR_LATEX: :align center
| =p= | =q= |   | =p && q= |
|-----+-----+---+----------|
| T   | T   |   | T        |
| T   | F   |   | F        |
| F   | T   |   | F        |
| F   | F   |   | F        |


This is just a generic truth table. We can replace
the values with boolean expressions in C++ to check our logic.

#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

** *Truth table for OR:*
This also deals with 2 variables, $p$ and $q$, so 4 total states. If
/at least one/ of the two variables are *true*, then the entire expression is
*true*. An expression with an "or" is only *false* when
*all sub-expressions are false*.

#+ATTR_LATEX: :align center
| =p= | =q= | =p ‖ q= |
|-----+-----+---------|
| T   | T   | T       |
| T   | F   | T       |
| F   | T   | T       |
| F   | F   | F      |


#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

** *Example - AND:*

We will only quit the program if the game is saved and the user wants to quit:

#+ATTR_LATEX: :align center
| =game_saved= | =want_to_quit= | =game_saved && want_to_quit= |
|--------------+----------------+------------------------------|
| T            | T              | T                            |
| T            | F              | F                            |
| F            | T              | F                            |
| F            | F              | F                            |


The states are:


1. *T:* *=gameSaved= is true and =wantToQuit= is true:* Quit the game.
2. *F:* *=gameSaved= is true and =wantToQuit= is false:* The user doesn't
    want to quit; don't quit.
3. *F:* *=gameSaved= is false and =wantToQuit= is true:* The user wants to
    quit but we haven't saved yet; don't quit.
4. *F:* *=gameSaved= is false and =wantToQuit= is false:* The user doesn't
   want to quit and the game hasn't been saved; don't quit.


#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX


** *Example - OR:*

If the store has cakes OR ice cream, then suggest it to the user that
wants dessert.

#+ATTR_LATEX: :align center
| =hasCake= | =hasIcecream= | =hasCake= ‖ =hasIceCream= |
|-----------+---------------+---------------------------|
| T         | T             | T                         |
| T         | F             | T                         |
| F         | T             | T                         |
| F         | F             | F                         |

The states are:


1. *T:* *=hasCake= is true and =hasIcecream=* is true: The store has desserts;
   suggest it to the user.
2. *T:* *=hasCake= is true and =hasIcecream=* is false: The store has cake but
    no ice cream; suggest it to the user.
3. *T:* *=hasCake= is false and =hasIcecream=* is true: The store has no cake
    but it has ice cream; suggest it to the user.
4. *F:* *=hasCake= is false and =hasIcecream=* is false: The store doesn't
   have cake and it doesn't have ice cream; don't suggest it to the user.

#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

* *DeMorgan's Laws*

Finally, we need to cover DeMorgan's Laws, which tell us what the /opposite/
of an expression means.


For example, if we ask the program *"is a $>$ 20?"* and the result is *false*,
then what does this imply? If $a > 20$ is false, then $a \leq 20$ is true...
notice that the opposite of "greater than" is "less than OR equal to"!

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

** *Opposite of =a && b=*

#+ATTR_LATEX: :align center
| =a= | =b= | =a && b= | =!( a && b )= | =!a ‖ !b= |
|-----+-----+----------+---------------+-----------|
| T   | T   | T        | F             | F         |
| T   | F   | F        | T             | T         |
| F   | T   | F        | T             | T         |
| F   | F   | F        | T             | T         |

If we're asking "is $a$ true and is $b$ true?" together, and the result is
*false*, that means either:


1. $a$ is false and $b$ is true, or
2. $a$ is true and $b$ is false, or
3. $a$ is false and $b$ is false.

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

These are the states where the result of =a && b= is false in the truth table.

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

In other words,


#+BEGIN_center
=!( a && b )= $\equiv$ =!a ‖ !b=
#+END_center

If $a$ is false, or $b$ is false, or both are false, then the result of
=a && b= is false.

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

** *Opposite of =a ‖ b=:*

#+ATTR_LATEX: :align center
| =a= | =b= | =a ‖ b= | =!( a ‖ b )= | =!a && !b= |
|-----+-----+---------+--------------+------------|
| T   | T   | T       | F            | F          |
| T   | F   | T       | F            | F          |
| F   | T   | T       | F            | F          |
| F   | F   | F       | T            | T          |

If we're asking "is $a$ true or $b$ true?", if the result of that is *false*
that means only one thing:

1. Both $a$ and $b$ were false.


This is the only state where the result of =a || b= is false.

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

In other words,

#+BEGIN_center
=!( a ‖ b )= $\equiv$ =!a && !b=
#+END_center

=a ‖ b= is false only if $a$ is false AND $b$ is false.

#+BEGIN_LATEX
\vspace{2cm}
#+END_LATEX

** *Summary*

In order to be able to write *if statements* or *while loops*, you need to
understand how these boolean expressions work. If you're not familiar with
these concepts, they can lead to you writing *logic errors* in your programs,
leading to behavior that you didn't want.
