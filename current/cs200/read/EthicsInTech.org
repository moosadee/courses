# -*- mode: org -*-

Software is made by people and it is used by people. We have a responsibility to design and test
software to avoid doing harm to people. While designing software it can be difficult to think of
how people who aren't like us will be affected, which is why we should have our work reviewed
by others with different backgrounds and experiences to try to catch issues ahead of time.

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

Some examples of problematic designs are:

- A social media platform giving you reminders of what happened a year ago,
  when a user happened to experience a tragedy at that time.
- Writing scripts to "validate" peoples' names, which mark certain names as invalid,
  whether because too short / has a space / has punctuation, and so on.
  (Names generally shouldn't be validated! Also, not everyone has just 2 names!)
- A medical machine whose software hasn't been properly tested to prevent invalid inputs,
  causing over-radiation of cancer patients, resulting in numerous deaths.
- A video platform being designed so that videos that keep eyeballs the longest generate
  the most profit, leading to exploitative video creation tactics (content farms,
  content designed to exploit childrens' attention spans) with no motivation to
  curb these practices aside from pressure from users of the platform.


And as we use more and more algorithms to crunch data and try to make decisions,
the topic of algorithmic bias also comes up - what should we allow a machine to make
decisions on, and what /should it not/ have the ability to control?
How do we hold a machine accountable for decisions it makes, when its whole algorithm
is a black-box, even to its developers?

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

Here are a few items I want you to look at for the discussion board post:


- Data scientist Cathy O’Neil on the cold destructiveness of big data:

  [[https://qz.com/819245/data-scientist-cathy-oneil-on-the-cold-destructiveness-of-big-data][qz.com/819245/data-scientist-cathy-oneil-on-the-cold-destructiveness-of-big-data]]

- How tech's lack of diversity affects its products: An interview with Sara Wachter-Boettcher (Podcast/transcript)

  [[https://uxpod.com/episodes/how-techs-lack-of-diversity-affects-its-products-an-interview-with-sara-watchter-boettcher.html][uxpod.com/episodes/how-techs-lack-of-diversity-affects-its-products-an-interview-with-sara-watchter-boettcher.html]]

- How I'm fighting bias in algorithms | Joy Buolamwini:

  https://www.youtube.com/watch?v=UG_X_7g63rY

- Facebook showed this ad to 95% women. Is that a problem?:

  https://www.youtube.com/watch?v=2wVPyiyukQc
