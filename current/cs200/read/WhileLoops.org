# -*- mode: org -*-


* While statements

A while statement is another type of loop that operates on a CONDITION, similar
to a for loop. For loops are better for counting, and while loops are better
for conditions.

A while loop takes this form:

#+BEGIN_SRC cpp :class cpp
cout << "Before loop" << endl;

while ( CONDITION )
{
  // Execute this code
  cout << "Repeats!" << endl;
}

cout << "After loop" << endl;
#+END_SRC

Any code within a while loop gets executed while the CONDITION is TRUE. Once
the condition is no longer true, then the while loop ends and the program
continues.

Remember that we can build *conditional* statements with the greater-than,
less-than, greater-than-or-equal-to, less-than-or-equal-to, equal-to, and
not-equal-to operators.

~

*Warning!:* Because a while loop will keep going until the condition is false,
it is possible to write a program where the condition /never becomes false/,
resulting in an *infinite loop*!

~

* Relational operators

The Relational Operators are operators where you compare two values or
variables. Their names are:

1. is /a/ equal to /b/?	/a == b/
2. is /a/ not equal to /b/?	/a != b/
3. is /a/ less than /b/? /a < b/
4. is /a/ less than or equal to /b/? /a <= b/
5. is /a/ greater than /b/?	/a > b/
6. is /a/ greater than or equal to /b/? /a >= b/

All of these are basically "yes/no" questions, and can be used to build
conditions for if statements and while loops.

~

* Infinite loops

An infinite loop error can occur if you're using a while loop and nothing inside
the loop makes the CONDITION evaluate to FALSE.

For example, if you run this code, it will keep printing out numbers forever:

#+BEGIN_SRC cpp :class cpp
#include <iostream>
using namespace std;

int main()
{
  int iterations = 0;
  bool running = true;

  while ( running )
  {
    iterations += 1;
    cout << iterations << endl;
  } // end of while loop


  // never gets to this point
  cout << "THE END" << endl;
  return 0;
}
#+END_SRC


(You can run this program here: https://replit.com/@rsingh13/Infinite-Loop-1#main.cpp)

There needs to be enough logic inside your while loop to effect the CONDITION.
In the example above, we have a *running* variable, but nothing INSIDE the while
loop ever sets it to *False*. Because it's always true, the loop always
keeps running.

~

* Logical opposites

If we're asking

/(x > y)?/

If it's *true*, then yes, /x > y/. If it's *false*, however, then /x <= y/ is
true (NOT /x < y/!). Make sure to keep these in mind...

1. The OPPOSITE of /x > y/ is /x <= y/.
2. The OPPOSITE of /x < y/ is /x >= y/.
3. The OPPOSITE of /x == y/ is /x != y/.


~

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

* Uses of while loops

** Counting up

The following while loop will start at 1, display the number to the screen,
and each loop around it will go up by 1 until it gets past 9.

#+BEGIN_SRC cpp :class cpp
int num = 1;
while ( num < 10 )
{
  cout << num << "\t";
  num++; // add 1 to num
}
#+END_SRC

*Program output:*

#+BEGIN_SRC
1	2	3	4	5	6	7	8	9
#+END_SRC

#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

** Keep the program running

In some cases, you'll have a main menu and want to return the user back to that
menu after each operation until they choose to quit. You could implement this
with a basic boolean variable:

#+BEGIN_SRC cpp :class cpp
bool done = false;
while ( !done )
{
  cout << "Option: ";
  cin >> option;
  if ( option == "QUIT" )
  {
    done = true;
  }
}
cout << "Bye" << endl;
#+END_SRC

...OR...

#+BEGIN_SRC cpp :class cpp
bool running = true;
while ( running )
{
  cout << "Option: ";
  cin >> option;
  if ( option == "QUIT" )
  {
    running = false;
  }
}
cout << "Bye" << endl;
#+END_SRC

#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

** Validating user input

Sometimes you want to make sure what the user entered is valid before continuing
on. If you just used an if statement, it would only check the user's input
/once/, allowing them to enter something invalid the second time. Use a while
loop to make sure that the program doesn't move on until it has valid data.

#+BEGIN_SRC cpp :class cpp
cout << "Enter a number between 1 and 10: ";
cin >> num;

while ( num < 1 || num > 10 )	// out of bounds!
{
  cout << "Invalid number! Try again: ";
  cin >> num;
}

cout << "Thank you" << endl;
#+END_SRC

*Program output:*

#+BEGIN_SRC
Enter a number between 1 and 10: 100
Invalid number! Try again: -400
Invalid number! Try again: -5
Invalid number! Try again: 5
Thank you
#+END_SRC

#+BEGIN_LATEX
\vspace{2cm}
#+END_LATEX

* *Do... while loops*

A do...while loop is just like a while loop, except the condition goes at the
end of the code block, and the code within the code block is *always executed
at least one time*.

#+BEGIN_SRC cpp :class cpp
do
{
  // Do this at least once
} while ( CONDITION );
#+END_SRC

For example, you might want to always get user input, but if they enter
something invalid you'll repeat that step until they enter something valid.

#+BEGIN_SRC cpp :class cpp
do
{
  cout << "Enter a choice: ";
  cin >> choice;
} while ( choice > 0 );
#+END_SRC

#+BEGIN_LATEX
\vspace{2cm}
#+END_LATEX

* *Special commands*

** *continue*

Sometimes you might want to stop the current *iteration* of the loop, but you
don't want to leave the entire loop. In this case, you can use =continue;= to
skip the rest of the current iteration and move on to the next.

#+BEGIN_SRC cpp :class cpp
int counter = 10;
while ( counter > 0 )
{
  counter--;
  if ( counter % 2 == 0 ) // is an even number?
  {
    continue;	// skip the rest
  }
  cout << counter << " odd number" << endl;
}
#+END_SRC

*Program output:*

#+BEGIN_SRC
9 odd number
7 odd number
5 odd number
3 odd number
1 odd number
#+END_SRC

#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

** *break*

In other cases, maybe you want to leave a loop before its condition has become
false. You can use a =break;= statement to force a loop to quit.

#+BEGIN_SRC cpp :class cpp
while ( true )	// Infinite loop :o
{
  cout << "Enter QUIT to quit: ";
  cin >> userInput;
  if ( userInput == "QUIT" )
  {
    break;	// stop looping
  }
}
#+END_SRC

(Note: This example is considered bad design!)
