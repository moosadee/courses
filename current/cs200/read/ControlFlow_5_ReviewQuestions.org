# -*- mode: org -*-

------------------------------------------------------
* *Review questions:*

#+ATTR_HTML: :class action-item
#+NAME: content
#+BEGIN_HTML
1. A boolean expression can be...
2. What do each of these expressions represent?
   1. =b=
   2. =!b=
   3. =a == b=
   4. =a > b=
   5. =a = b=
3. What are each of the following operators?
   1. =&&=
   2. =||=
   3. =!=
4. When does the expression =a && b= evaluate to =true=? =false=?
5. When does the expression when =a || b= evaluate to =true=? =false=?
6. An if statement's code block will be executed when the if statement's condition evaluates to...
7. A while loop's code block will continue executing while the while loop's condition evaluates to...
#+END_HTML
