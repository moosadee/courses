// In VS Code, go to File > Open Folder, and open "graded_program1" to begin.
// To build, go to View > Terminal, and type:
// g++ apartment.cpp -o Apart.exe
// To run the program, use:
// ./Apart.exe

#include <iostream>   // Console input and output
#include <string>     // String data type
using namespace std;  // Using the STanDard C++ library

/** apartment program
    argument 1: room 1 name
    argument 2: room 1 width
    argument 3: room 1 length
    argument 3: room 2 name
    argument 4: room 2 width
    argument 5: room 2 length
*/
int main( int argCount, char* args[] )
{
    cout << endl <<"-- APARTMENT PROGRAM -- " << endl;
    // Check to see if program was ran with enough arguments
    if ( argCount < 6 ) { cout << "Not enough arguments! Expected: " << args[0] << " ROOM1NAME ROOM1WIDTH ROOM1LENGTH ROOM2NAME ROOM2WIDTH ROOM2LENGTH" << endl; return 1; }

    // Converting arguments to variables
    string room1_name   = string( args[1] );
    float  room1_width  = stof( args[2] );
    float  room1_length = stof( args[3] );
    float  room1_area   = 0;

    string room2_name   = string( args[4] );
    float  room2_width  = stof( args[5] );
    float  room2_length = stof( args[6] );
    float  room2_area   = 0;

    float  total_area   = 0;


    // - STUDENT CODE ----------------------------------------------------------
    // -------------------------------------------------------------------------

    cout << endl << "-- GOODBYE! --" << endl;

    return 0;
}
