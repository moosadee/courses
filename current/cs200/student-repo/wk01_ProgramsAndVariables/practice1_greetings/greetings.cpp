#include <iostream>   // Console input and output
#include <string>     // String data type
using namespace std;  // Using the STanDard C++ library

/** greetings program
    argument 1: course name
    argument 2: season
    argument 3: year
 */
int main( int argCount, char* args[] )
{
  cout << endl << "-- GREETINGS PROGRAM -- " << endl;
  // Check to see if program was ran with enough arguments
  if ( argCount < 4 ) { cout << "Not enough arguments! Expected: " << args[0] << " COURSE SEASON YEAR" << endl; return 1; }

  // Converting arguments to variables
  string course_name = string( args[1] );
  string season = string( args[2] );
  int year = stoi( args[3] );


  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Use `cout` to display the course name:

  // TODO: Use `cout` to display the season and year:
  // -------------------------------------------------------------------------

  cout << endl << "-- GOODBYE! --" << endl;

  return 0;
}
