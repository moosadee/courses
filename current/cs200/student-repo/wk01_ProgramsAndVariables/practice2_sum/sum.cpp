#include <iostream>   // Console input and output
#include <string>     // String data type
using namespace std;  // Using the STanDard C++ library

/** greetings program
    argument 1: number 1
    argument 2: number 2
*/
int main( int argCount, char* args[] )
{
    cout << endl << "-- SUM PROGRAM -- " << endl;
    // Check to see if program was ran with enough arguments
    if ( argCount < 3 ) { cout << "Not enough arguments! Expected: " << args[0] << " NUMBER1 NUMBER2" << endl; return 1; }

    // Converting arguments to variables
    float number1 = stof( args[1] );
    float number2 = stof( args[2] );
    float result;
  
    // - STUDENT CODE ----------------------------------------------------------
    // TODO: Calcluate the sum of `number1` and `number2` and store the result in `result`.
    // -------------------------------------------------------------------------

    cout << number1 << " + " << number2 << " = " << result << endl;

    cout << endl << "-- GOODBYE! --" << endl;
  
    return 0;
}
