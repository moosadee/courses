#+HTML_HEAD: <style type='text/css'> body { font-family: sans-serif; } #content { width: 100%; max-width: 100%; } </style>
#+HTML_HEAD: <style type='text/css'> h3:before { content:"💾 "; } h3:after { content: ""; }  </style>
#+HTML_HEAD: <style type='text/css'> h3 { background: rgba( 0, 255, 150, 0.5 ); }  </style>
#+HTML_HEAD: <style type='text/css'> h4:before { content:"🟣 "; } h4:after { content: ""; }  </style>
#+HTML_HEAD: <style type='text/css'> code { background: rgba( 190, 111, 255, 0.2 ); padding: 2px; }  </style>
#+HTML_HEAD: <style type='text/css'> .src-terminal { background: #201826; color: #e5d3f4; font-weight: bold; } .src-terminal:before { content: "TERMINAL"; }  </style>
#+HTML_HEAD: <style type='text/css'> .src-form { background: #bddfff; color: #001528; font-weight: bold; } .src-form:before { content: "FORM"; }  </style>
#+HTML_HEAD: <style type='text/css'> .outline-1, .outline-2, .outline-3 { margin-left: 10px; padding: 10px; border: outset 2px #c0c0c0; background: rgba( 0, 255, 150, 0.1 ); margin-bottom: 10px; } </style>
#+HTML_HEAD: <style type='text/css'> .outline-3 { margin-bottom: 50px; } </style>

#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/tomorrow-night-blue.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script>
#+HTML_HEAD: <script>hljs.initHighlightingOnLoad();</script>

* Assignment and policy info

- Assignment info:
  - [[https://gitlab.com/moosadee/courses/-/blob/main/current/policies/practice-graded.org?ref_type=heads][Practice and Graded programs]]
  - [[https://gitlab.com/moosadee/courses/-/blob/main/current/policies/resubmit.org?ref_type=heads][Resubmission and regrading policy and procedure]]
  - [[https://gitlab.com/moosadee/courses/-/blob/main/current/policies/duedates.org?ref_type=heads][Due dates and available until dates]]
- How To:
  - [[https://gitlab.com/moosadee/courses/-/blob/main/reference/ide_vscode_build_and_run.org?ref_type=heads][Build and Run your program in VS Code]]
  - [[https://gitlab.com/moosadee/courses/-/blob/main/current/policies/turnin-lab.org?ref_type=heads][Turn in your lab]]
  - [[https://gitlab.com/moosadee/courses/-/blob/main/reference/vscode-git.org?ref_type=heads][Use Git and VS Code]]
- Quick reference:
  - [[https://gitlab.com/moosadee/courses/-/blob/main/current/share/read/CS200review.org?ref_type=heads][CS 200 code reference]]
  - [[https://gitlab.com/moosadee/courses/-/blob/main/reference/cpp_program_arguments.org?ref_type=heads][C++ program arguments]]
  - Debugging with [[https://gitlab.com/moosadee/courses/-/blob/main/reference/gdb.org?ref_type=heads][gdb]] / [[https://gitlab.com/moosadee/courses/-/blob/main/reference/lldb.org?ref_type=heads][lldb]]
  - [[https://gitlab.com/moosadee/courses/-/blob/main/reference/mac.org?ref_type=heads][Common issues on Mac]]
- Links:
  - [[https://moosadee.gitlab.io/courses/][R.W.'s courses homepage]]
  - [[https://gitlab.com/moosadee/courses/-/blob/main/current/cs200/student-repo/wk01_ProgramsAndVariables/instructions.org][Assignment direct link]]

--------------------------------------------------------------------------------

* Included files:

*Getting started:*

1. This lab is located in your *repository folder* under *=wk01_ProgramsAndVariables=*.
1. In VS Code use "Open Folder" to open the specific subprogram you wish to work with (e.g., =practice1_greetings=, =practice2_sum=, etc.)
2. BUILD: =g++ *.cpp= for programs with just a cpp file.
3. RUN: =./a.out= (Linux/Mac) or =./a.exe= (Windows).

#+BEGIN_SRC
wk01_ProgramsAndVariables
├── graded_program1
│   ├── apartment.cpp
│   └── tester.cpp
├── graded_program2
│   ├── tester.cpp
│   └── trip.cpp
├── instructions.org
├── practice1_greetings
│   └── greetings.cpp
├── practice2_sum
│   └── sum.cpp
└── practice3_order
    └── order.cpp
#+END_SRC

--------------------------------------------------------------------------------

* Practice programs

** Practice 1 - Greetings

#+BEGIN_SRC bash
  $ ./Greet.exe

  -- GREETINGS PROGRAM --
  Not enough arguments! Expected: ./Greet.exe COURSE SEASON YEAR

  
  $ ./Greet.exe CS200 Spring 2025

  -- GREETINGS PROGRAM --
  Welcome to CS200, Semester: Spring 2025

  -- GOODBYE! --
#+END_SRC

When launching this program the user needs to enter the COURSECODE (e.g. "CS20" or "CS235", etc.), SEASON (e.g., "Spring" or "Fall", etc.), and YEAR (e.g. "2025" or "2026", etc.)

In the STUDENT CODE region, do the following:
1. Use a =cout= statement to display the message "Welcome to".
2. Use a =cout= statement to display the value of the variable =course_name=.
3. Use a =cout= statement to display the message "Semester:".
4. Use a =cout= statement to display the =season= and =year= variables.

--------------------------------------------------------------------------------

** Practice 2 - Sum

#+BEGIN_SRC bash
  $ ./Sum.exe

  -- SUM PROGRAM --
  Not enough arguments! Expected: ./Sum.exe NUMBER1 NUMBER2

  
  $ ./Sum.exe 3 5

  -- SUM PROGRAM --
  3 + 5 = 8

  -- GOODBYE! --
#+END_SRC

When launching this program the user needs to provide two numbers. This program will then compute the sum of those two numbers and display the result on the screen.

Within the STUDENT CODE region, write an *assignment statement*. Assign the result of =number1 + number2= to the variable =result=.

--------------------------------------------------------------------------------

** Practice 3 - Order

#+BEGIN_SRC bash
  $ ./Order.exe

  -- ORDER PROGRAM --
  Not enough arguments! Expected: ./Order.exe FOOD PRICE AMOUNT


  $ ./Order.exe burrito 2.99 3

  -- ORDER PROGRAM -- 
  Food: burrito
  Price, each: $2.99
  Amount ordered: 3
  Total price: $8.97

  -- GOODBYE! --
#+END_SRC

When launching this program the user needs to provide three arguments: 1. the name of a food (one word only), 2. the price of that food, and 3. the quantity of that food to be ordered.

Within the STUDENT CODE region, calculate the total cost of the purchase, which will be the =amount= times the =price=. Store the result in the =total_cost= variable.

Afterwards, use =cout= statements to display each of the following items:
- A label "Food:", followed by the variable =food=.
- A label "Price, each: $", followed by the variable =price=.
- A label "Amount ordered:", followed by the variable =amount=.

--------------------------------------------------------------------------------

* Graded programs

** Graded 1 - Apartment

#+BEGIN_SRC bash
  $ ./a.out
  -- APARTMENT PROGRAM --
  Not enough arguments! Expected: ./a.out ROOM1NAME ROOM1WIDTH ROOM1LENGTH ROOM2NAME ROOM2WIDTH ROOM2LENGTH

  
  $ ./Apart.exe Bathroom 10 12 Kitchen 20 25

  -- APARTMENT PROGRAM --
  ROOM 1: Bathroom
  Dimensions: 10 x 12
  Area: 120 sqft

  ROOM 2: Kitchen
  Dimensions: 20 x 25
  Area: 500 sqft

  Total apartment sqft: 620

  -- GOODBYE! --
#+END_SRC

In this program the user will pass in a *name, width, and length* for two rooms (so 6 arguments total).
The program will calculate each room's *area*, display its *dimensions and area*, and also calculate the total area for both rooms together.

*** Instructions

The arguments are saved in the various room variables in the starter code.

The program will display both rooms' names and dimensions (as "WIDTH X LENGTH") and calculate the area, displaying it with "sqft" at the end.

See the example output for what the program output should look like. (Doesn't have to match exactly.)

--------------------------------------------------------------------------------

** Graded 2 - Trip

#+BEGIN_SRC bash
  $  Not enough arguments! Expected: ./Trip.exe TRAVEL_DIST MILES_PER_GALLON GALLONS_PER_TANK GAS_PRICE_PER_GALLON


  $ ./Trip.exe 248 33 10.8 2.83

  -- TRIP CALCULATOR --
  Distance to travel: 248.00
  Miles per gallon: 33.00
  Gallons per tank: 10.80
  Price per gallon: $2.83
  Total gallons needed: 7.52
  Total price: $21.27
  Total tanks of gas: 0.70

  -- GOODBYE! --
#+END_SRC

In this program the user will pass in the following arguments:
1. Distance (in miles) between two locations
2. Miles per gallon for their car
3. Gallons per tank for their car
4. Gas price per gallon

The program will then calculate how much gas will be needed to travel that distance and how much it will cost.


*** Instructions
