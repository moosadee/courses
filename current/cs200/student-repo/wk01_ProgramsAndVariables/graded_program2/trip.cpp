// In VS Code, go to File > Open Folder, and open "graded_program2" to begin.
// To build, go to View > Terminal, and type:
// g++ trip.cpp -o Trip.exe
// To run the program, use:
// ./Trip.exe

#include <iostream>
#include <iomanip>
using namespace std;

/** trip program
    argument 1: distance in miles between locations (e.g., kc -> stl is 248 miles)
    argument 2: miles per gallon (e.g., 33)
    argument 3: gallons per tank (e.g., 10.8)
    argument 4: gas price per gallon (e.g., 2.83)
*/
int main( int argCount, char* args[] )
{
    cout << endl << "-- TRIP CALCULATOR -- " << endl;
    // Check to see if program was ran with enough arguments
    if ( argCount < 5 ) { cout << "Not enough arguments! Expected: " << args[0] << " TRAVEL_DIST MILES_PER_GALLON GALLONS_PER_TANK GAS_PRICE_PER_GALLON" << endl; return 1; }
    cout << fixed << setprecision( 2 );

    // Converting arguments to variables
    float distance_to_travel    = stof( args[1] );
    float miles_per_gallon      = stof( args[2] );
    float gallons_per_tank      = stof( args[3] );
    float price_per_gallon      = stof( args[4] );

    cout << "Distance to travel: " << distance_to_travel << endl;
    cout << "Miles per gallon: " << miles_per_gallon << endl;
    cout << "Gallons per tank: " << gallons_per_tank << endl;
    cout << "Price per gallon: $" << price_per_gallon << endl;
  
    // - STUDENT CODE ----------------------------------------------------------
    // -------------------------------------------------------------------------

    cout << endl << "-- GOODBYE! --" << endl;  
}
