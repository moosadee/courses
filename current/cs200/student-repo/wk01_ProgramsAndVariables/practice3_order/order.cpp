#include <iostream>   // Console input and output
#include <iomanip>    // Special output formatting
#include <string>     // String data type
using namespace std;  // Using the STanDard C++ library

/** greetings program
    argument 1: food name
    argument 2: price
    argument 3: quantity to order
 */
int main( int argCount, char* args[] )
{
  cout << endl << "-- ORDER PROGRAM -- " << endl;
  // Check to see if program was ran with enough arguments
  if ( argCount < 4 ) { cout << "Not enough arguments! Expected: " << args[0] << " FOOD PRICE AMOUNT" << endl; return 1; }
  cout << fixed << setprecision( 2 ); // Set up for USD formatting

  // Converting arguments to variables
  string food = string( args[1] );
  float price = stof( args[2] );
  int amount = stoi( args[3] );

  float total_cost = 0;
  
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Calculate the total price of the order by multiplying `price` and `amount`.
  // TODO: Use `cout` to display the name of the food
  // TODO: Use `cout` to display the price of each item
  // TODO: Use `cout` to display the total amount of this item being ordered
  // -------------------------------------------------------------------------
  cout << "Total price: $" << total_cost << endl;
  
  cout << endl << "-- GOODBYE! --" << endl;
  
  return 0;
}
