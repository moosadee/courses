#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

struct BusStop
{
    int id;
    string name;
    float lat;
    float lon;
};

int main()
{
    cout << endl << "-- BUS STOP PROGRAM -- " << endl;
    cout << fixed << setprecision( 2 );

    // TODO: Create BusStop variables

    cout << left
         << setw( 10 ) << "ID"
         << setw( 15 ) << "LATITUDE"
         << setw( 15 ) << "LONGITUDE"
         << setw( 50 ) << "STOP NAME"
         << endl << string( 80, '-' ) << endl;
    
    cout << endl << "-- GOODBYE! --" << endl;
}
