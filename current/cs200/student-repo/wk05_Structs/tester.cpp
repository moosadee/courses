#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <string>
#include <map>
using namespace std;

const string CLEAR = "\033[0m"; const string RED = "\033[0;31m";
const string GREEN = "\033[0;32m"; const string YELLOW = "\033[0;33m";

bool Contains( std::string haystack, std::string needle, bool caseSensitive )
{
    string a = haystack; string b = needle;
    if ( !caseSensitive )
    {
        for ( unsigned int i = 0; i < a.size(); i++ ) { a[i] = tolower( a[i] ); }
        for ( unsigned int i = 0; i < b.size(); i++ ) { b[i] = tolower( b[i] ); }
    }
    return ( a.find( b ) != std::string::npos );
}

struct TestCase
{
    string assignmentName;
    string preEcho;
    string programName;
    string outputName;
    string outputName2;
    string arguments;
    map<string, bool> expectedOutputs;
};

void RunTest( TestCase& tc )
{
    // Run program
    string command = "";
    if ( tc.preEcho != "" )
    {
        command += "echo \"" + tc.preEcho + "\" | ";
    }
    command +="./" + tc.programName + " "
        + tc.arguments + " > " + tc.outputName;
    system( command.c_str() );

    cout << endl << "- PROGRAM: " << tc.assignmentName << endl;

    // Check output
    ifstream input( tc.outputName );
    string line, output="";
    while ( getline( input, line ) )
    {
        output += line + "\t";
    }
    output += "\n\n";
    input.close();
    input.open( tc.outputName2 );
    while ( getline( input, line ) )
    {
        output += line + "\t";
    }
    output += "\n";
    cout << "  - OUTPUT: ";
    if ( output.size() > 80 )
    {
        cout << output.substr( 0, 70 ) << YELLOW << " (... TRUNCATED ...)" << CLEAR;
    }
    else
    {
        cout << output;
    }
    cout << endl;
    for ( auto& expOut : tc.expectedOutputs )
    {
        if ( Contains( output, expOut.first, false ) )
        {
            expOut.second = true;
        }
    }

    // Test results
    for ( auto& expOut : tc.expectedOutputs )
    {
        string result = ( expOut.second )
            ? ( GREEN + "  - [PASS] Found output" )
            : ( RED   + "  - [FAIL] DIDN'T FIND OUTPUT" );
        cout << result << " \"" << expOut.first << "\""
             << " in program " << tc.assignmentName
             << CLEAR << endl;
    }

    // Cleanup
    command = "rm " + tc.outputName;
    system( command.c_str() );
}

std::string ToLower( const std::string& val )
{
    std::string str = "";
    for ( unsigned int i = 0; i < val.size(); i++ )
    {
        str += tolower( val[i] );
    }
    return str;
}

int main()
{
    // Practice 1
    {
        system( "g++ practice1_*/*.cpp -o program1.exe" );
        vector<TestCase> testCases(1);
        testCases[0].assignmentName = "practice1";
        testCases[0].programName = "program1.exe";
        testCases[0].outputName = "program1-out.txt";
        testCases[0].arguments = "";
        testCases[0].preEcho = "";
        testCases[0].expectedOutputs[ "510192" ] = false;
        testCases[0].expectedOutputs[ "25053" ] = false;
        testCases[0].expectedOutputs[ "23515" ] = false;
        testCases[0].expectedOutputs[ "38.95" ] = false;
        testCases[0].expectedOutputs[ "39.02" ] = false;
        testCases[0].expectedOutputs[ "39.06" ] = false;
        testCases[0].expectedOutputs[ "-95.26" ] = false;
        testCases[0].expectedOutputs[ "-94.57" ] = false;
        testCases[0].expectedOutputs[ "-94.54" ] = false;
        testCases[0].expectedOutputs[ "KU LAWRENCE" ] = false;
        testCases[0].expectedOutputs[ "ON TROOST" ] = false;
        testCases[0].expectedOutputs[ "ON 35TH" ] = false;
        
        for ( auto& test : testCases ) { RunTest( test ); }
    }

    // Practice 2

    {
        system( "g++ practice2_*/*.cpp -o program2.exe" );
        vector<TestCase> testCases(2);
        testCases[0].assignmentName = "practice2";
        testCases[0].programName = "program2.exe";
        testCases[0].outputName = "program2-out.txt";
        testCases[0].outputName2 = "";
        testCases[0].arguments = "5";
        testCases[0].preEcho = "";
        testCases[0].expectedOutputs[ "5.00 cups of peanut butter" ] = false;
        testCases[0].expectedOutputs[ "2.50 cups of sugar" ] = false;
        testCases[0].expectedOutputs[ "5.00 total of egg" ] = false;
        
        testCases[1].assignmentName = "practice2";
        testCases[1].programName = "program2.exe";
        testCases[1].outputName = "program2-out.txt";
        testCases[1].outputName2 = "";
        testCases[1].arguments = "0.5";
        testCases[1].preEcho = "";
        testCases[1].expectedOutputs[ "0.50 cups of peanut butter" ] = false;
        testCases[1].expectedOutputs[ "0.25 cups of sugar" ] = false;
        testCases[1].expectedOutputs[ "0.50 total of egg" ] = false;
        
        for ( auto& test : testCases ) { RunTest( test ); }
    }

    // Graded program
    {
        cout << endl << "- PROGRAM: graded_program" << endl;
        cout << YELLOW << "Program has to be manually graded" << CLEAR << endl;
        /*
        system( "g++ graded_program/*.cpp -o graded.exe" );
        
        vector<TestCase> testCases(2);
    
        testCases[0].assignmentName = "graded_program";
        testCases[0].programName = "bank.exe";
        testCases[0].outputName = "graded-out.txt";
        testCases[0].outputName2 = "";
        testCases[0].arguments = "";
        testCases[0].preEcho = "1 2 3 4";
        testCases[0].expectedOutputs[ "checking" ] = false;
        testCases[0].expectedOutputs[ "100100" ] = false;
        testCases[0].expectedOutputs[ "2255" ] = false;

        testCases[1].assignmentName = "graded_program1";
        testCases[1].programName = "graded_program1/bank.exe";
        testCases[1].outputName = "graded_program1/graded-out.txt";
        testCases[1].outputName2 = "graded_program1/accountTest.txt";
        testCases[1].arguments = "graded_program1/accountTest.txt";
        testCases[1].preEcho = "50";
        testCases[1].expectedOutputs[ "checking" ] = false;
        testCases[1].expectedOutputs[ "100100" ] = false;
        testCases[1].expectedOutputs[ "2305" ] = false;
        
        for ( auto& test : testCases ) { RunTest( test ); }

        */
        //system( "rm graded.exe" );
    }

    return 0;
}
