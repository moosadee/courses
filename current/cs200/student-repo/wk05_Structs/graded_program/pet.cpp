#include <iostream>
#include <cstdlib>
#include <string>
#include <ctime>
using namespace std;

const string CLEAR    = "\033[0m";
const string BLACK    = "\033[3;30;40m";
const string RED      = "\033[3;31;40m";
const string GREEN    = "\033[3;32;40m";
const string YELLOW   = "\033[3;33;40m";
const string BLUE     = "\033[3;34;40m";
const string MAGENTA  = "\033[3;35;40m";
const string CYAN     = "\033[3;36;40m";
const string WHITE    = "\033[3;37;40m";

struct Pet
{
    string name;
    int health;
    int hunger;
    int happiness;
};

int main()
{
    cout << endl << "-- VIRTUAL PET -- " << endl;

    srand( time( NULL ) ); // Seed the random number generator

    Pet myPet = {
        "Petty McPetFace",  // name
        100,                // health
        0,                  // hunger
        100                 // happiness
    };
    int choice;
    int random;

    bool running = true;
    while ( running )
    {
        cout << CYAN << string( 80, '-' ) << endl;
        // TODO: Display the pet's name, health, hunger, and happiness

        // Choice menu
        cout << "OPTIONS:" << endl;
        cout << "1. Play" << endl;
        cout << "2. Feed" << endl;
        cout << "3. Medicine" << endl;
        cout << endl << "CHOICE: ";
        cin >> choice;

        // Handle choice
        if ( choice == 1 )
        {
            random = rand() % 10 + 1;
            // TODO: Add the `random` value to the pet's happiness.
            // Also display a message to the screen.
        }
        else if ( choice == 2 )
        {
            random = rand() % 10 + 1;
            // TODO: Subtract the `random` value from pet's `hunger`.
            // Also display a message to the screen.
        }
        else if ( choice == 3 )
        {
            random = rand() % 10 + 1;
            // TODO: Add the `random` value to pet's `health`.
            // Also display a message to the screen.
        }
        else
        {
            cout << "Unknown selection!" << endl;
        }

        // Stat updates
        myPet.hunger += rand() % 5;
        myPet.health -= rand() % 5;

        // Warnings
        // TODO: If the pet's health is below 50, display a warning.
        // Also subtract a random amount from its happiness.
        // TODO: If the pet's hunger is above 50, display a warning.
        // Also subtract a random amount from health and happiness.

        // Upper/lower bound stats
        if ( myPet.health < 0 )           { myPet.health = 0; }
        else if ( myPet.health > 100 )    { myPet.health = 100; }
        if ( myPet.hunger < 0 )           { myPet.hunger = 0; }
        else if ( myPet.hunger > 100 )    { myPet.hunger = 100; }
        if ( myPet.happiness < 0 )        { myPet.happiness = 0; }
        else if ( myPet.happiness > 100 ) { myPet.happiness = 100; }

        // Check for fail state
        if ( myPet.health == 0 )
        {
            cout << RED << myPet.name << " HAS PASSED OUT! :(" << endl;
            running = false;
        }
    }

    cout << endl << "-- GOODBYE! --" << endl;
    return 0;
}
