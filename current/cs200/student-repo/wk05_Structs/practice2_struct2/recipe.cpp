#include <iostream>
#include <iomanip>
using namespace std;

struct Ingredient
{
    string name;
    float amount;
    string unit;
};

struct Recipe
{
    string name;

    Ingredient ing1;
    Ingredient ing2;
    Ingredient ing3;
};

int main( int argCount, char* args[] )
{
    cout << endl << "-- COOKIE PROGRAM -- " << endl;
    // Check to see if program was ran with enough arguments
    if ( argCount < 2 ) { cout << "Not enough arguments!"
        << "Expected: " << args[0] << " batches" << endl; return 1; }

    float batches = stof( args[1] );
    cout << fixed << setprecision( 2 );

    // TODO: Declare a Recipe variable, then set up its member variables.

    // TODO: Display the recipe's name and all of its ingredients.

    cout << endl << "-- GOODBYE! --" << endl;  
}
