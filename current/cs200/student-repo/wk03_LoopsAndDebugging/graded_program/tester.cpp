#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <string>
#include <map>
using namespace std;

struct TestCase
{
    string command;
    map<string, bool> expectedOutputs;
};

std::string ToLower( const std::string& val )
{
    std::string str = "";
    for ( unsigned int i = 0; i < val.size(); i++ )
    {
        str += tolower( val[i] );
    }
    return str;
}

int main()
{
    const string CLEAR    = "\033[0m"; const string RED      = "\033[0;31m"; const string GREEN    = "\033[0;32m"; const string YELLOW   = "\033[0;33m";

    vector<string> sourceFiles = { "mars.cpp" };
    string command = "";
    vector<TestCase> testCases(2);

    testCases[0].command = "./a.out 10 10 1 > result.txt";
    testCases[0].expectedOutputs[ "DAY 1" ] = false;
    testCases[0].expectedOutputs[ "DAY 2" ] = false;
    testCases[0].expectedOutputs[ "DAY 3" ] = false;
    testCases[0].expectedOutputs[ "DAY 4" ] = false;
    testCases[0].expectedOutputs[ "FOOD: 10" ] = false;
    testCases[0].expectedOutputs[ "FOOD: 7" ] = false;
    testCases[0].expectedOutputs[ "FOOD: 4" ] = false;
    testCases[0].expectedOutputs[ "FOOD: 1" ] = false;
    testCases[0].expectedOutputs[ "OXYGEN: 10" ] = false;
    testCases[0].expectedOutputs[ "OXYGEN: 9" ] = false;
    testCases[0].expectedOutputs[ "OXYGEN: 8" ] = false;
    testCases[0].expectedOutputs[ "OXYGEN: 7" ] = false;
    testCases[0].expectedOutputs[ "RAN OUT OF FOOD" ] = false;

    testCases[1].command = "./a.out 100 25 7 > result.txt";
    testCases[1].expectedOutputs[ "DAY 1" ] = false;
    testCases[1].expectedOutputs[ "DAY 2" ] = false;
    testCases[1].expectedOutputs[ "DAY 3" ] = false;
    testCases[1].expectedOutputs[ "DAY 4" ] = false;
    testCases[1].expectedOutputs[ "FOOD: 100" ] = false;
    testCases[1].expectedOutputs[ "FOOD: 79" ] = false;
    testCases[1].expectedOutputs[ "FOOD: 58" ] = false;
    testCases[1].expectedOutputs[ "FOOD: 37" ] = false;
    testCases[1].expectedOutputs[ "OXYGEN: 25" ] = false;
    testCases[1].expectedOutputs[ "OXYGEN: 18" ] = false;
    testCases[1].expectedOutputs[ "OXYGEN: 11" ] = false;
    testCases[1].expectedOutputs[ "OXYGEN: 4" ] = false;
    testCases[1].expectedOutputs[ "RAN OUT OF OXYGEN" ] = false;

    cout << "- BUILDING PROGRAM -------------------------------" << endl;
    command = "g++ ";
    for ( auto& src : sourceFiles ) { command += src + " "; }
    cout << command << endl;
    system( command.c_str() );

    cout << "- TEST PROGRAM -----------------------------------" << endl;
    // Remove previous output file
    system( "rm result.txt" );

    for ( auto& test : testCases )
    {
        system( test.command.c_str() );
        cout << string( 80, '-' ) << endl << "TEST: " << test.command << endl;

        ifstream input( "result.txt" );
        string outline;
        cout << string( 80, '-' ) << endl << "Program output:" << endl;
        while ( getline( input, outline ) )
        {
            // Check if this output has any of the required items
            for ( auto& expected : test.expectedOutputs )
            {
                outline = ToLower( outline );
                string expectedStr = ToLower( expected.first );
                if ( outline.find( expectedStr ) != string::npos )
                {
                    expected.second = true;
                }
            }
            cout << outline << endl;
        }
        cout << endl;

        cout << "TEST CASES..." << endl;
        // Were all results found?
        bool allResultsFound = true;
        for ( auto& expected : test.expectedOutputs )
        {
            if ( expected.second == true )
            {
                cout << GREEN << "[PASS] Output found: \"" << expected.first << "\"" << endl;
            }
            else
            {
                allResultsFound = false;
                cout << RED << "[FAIL] Output not found: \"" << expected.first << "\"" << endl;
            }
        }

        if ( allResultsFound )
        {
            cout << GREEN << "[PASS] " << test.command << endl;
        }
        cout << CLEAR;
    }

    return 0;
}
