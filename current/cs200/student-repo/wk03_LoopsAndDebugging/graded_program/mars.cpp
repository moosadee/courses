#include <iostream>
using namespace std;

/** mars sim program
    argument 1: food
    argument 2: oxygen
    argument 3: people
*/
int main( int argCount, char* args[] )
{
    cout << endl << "-- MARS SIMULATOR PROGRAM -- " << endl;
    // Check to see if program was ran with enough arguments
    if ( argCount < 4 ) { cout << "Not enough arguments! Expected: " << args[0] << " FOOD OXYGENTANKS PEOPLE" << endl; return 1; }

    // Converting arguments to variables
    int food = stoi( args[1] );
    int oxygen = stoi( args[2] );
    int people = stoi( args[3] );

    int days = 0;
    bool viable = true;

    cout << endl << "EXPERIMENT ENDED AFTER " << days << " DAYS" << endl;

    cout << endl << "-- GOODBYE! --" << endl;
}
