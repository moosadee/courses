#include <iostream>
#include <vector>
#include <string>
using namespace std;

int main( int argCount, char* args[] )
{
    if ( argCount != 3 )
    {
        cout << endl << "Expected form: " << args[0] << " start end" << endl;
        return 1;
    }

    int start = stoi( args[1] );
    int end = stoi( args[2] );

    int i = start;
    cout << "Displaying numbers from " << start << " to " << end << "..." << endl;
    while ( i < end )
    {
        cout << i << "\t";
        i++;
    }
    cout << endl;

    return 0;
}
