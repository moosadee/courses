#include <iostream>
using namespace std;

int main( int argCount, char* args[] )
{
    if ( argCount < 3 )
    {
        cout << "NOT ENOUGH ARGUMENTS! Expected form: " << endl;
        cout << args[0] << " low high" << endl;
        return 1;
    }

    int low = stoi( args[1] );
    int high = stoi( args[2] );
    int counter;

    cout << endl << "LOOPING WITH WHILE LOOP:" << endl;
    // TODO: Use a while loop to display numbers from low to high, going up by 2 each time.
    cout << endl;

    cout << endl << "LOOPING WITH FOR LOOP:" << endl;
    // TODO: Use a for loop to display numbers from low to high, going up by 2 each time.
    cout << endl;

    return 0;
}
