#include <iostream>
using namespace std;

int main()
{
    int min = 1;
    int max = 10;
    int choice;

    cout << "min: " << min << ", max: " << max << endl;
    cout << "Enter a number within the range: ";
    cin >> choice;

    // TODO: Add a while loop. While the user's input is invalid, then:
    // TODO: 1. Display an error. Ask them to re-enter their selection.
    // TODO: 2. Use the cin command to store their response in the choice variable.

    cout << "You entered: " << choice << endl;

    return 0;
}
