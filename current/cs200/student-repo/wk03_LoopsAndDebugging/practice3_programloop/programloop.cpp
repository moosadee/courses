#include <iostream>
using namespace std;

int main()
{
    int choice;
    bool running = true;

    // TODO: Surround the following menu in a while loop that keeps running while `running` is true.
        // Display MENU
        cout << "-- MAIN MENU --" << endl;
        cout << "1. Display my favorite book" << endl;
        cout << "2. Display my favorite movie" << endl;
        cout << "3. Display my favorite game" << endl;
        cout << "4. Exit program" << endl << endl;

        // GET USER SELECTION
        cout << "SELECTION: ";
        cin >> choice;
        cout << "You chose " << choice << endl;

        // PERFORM OPERATION
        switch( choice )
        {
        case 1:
            cout << "Masters of Doom" << endl;
            break;

        case 2:
            cout << "The Lost Skeleton of Cadavra" << endl;
            break;

        case 3:
            cout << "Divinity Original Sin 2" << endl;
            break;

        case 4:                                                       // SOLUTION
            running = false;                                          // SOLUTION
            break;                                                    // SOLUTION

        default:
            cout << "UNKNOWN COMMAND!" << endl;
        }

        cout << endl;
    // TODO: Put end of while loop here (closing curly brace })


    // End of program
    cout << "Goodbye." << endl;

    return 0;
}
