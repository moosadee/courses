#include <iostream>
#include <iomanip>
using namespace std;

int main( int argCount, char* args[] )
{
    if ( argCount < 2 ) { cout << "Not enough arguments! Give: low high" << endl; return 1; }

    cout << fixed << setprecision( 2 );

    int low = stoi( args[1] );
    int high = stoi( args[2] );

    int sum = 0;
    int counter = low;
    // TODO: Keep looping while the counter is less than the high value.
    // TODO: Within the loop, display sum + counter  ...
    // TODO: Then add counter onto sum.
    // TODO: Then display the sum again.
    // TODO: Then increment the counter by 1.

    cout << endl << "The result is: " << sum << "!" << endl;

    return 0;
}
