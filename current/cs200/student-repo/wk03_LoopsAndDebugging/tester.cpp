#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <string>
#include <map>
using namespace std;

const string CLEAR = "\033[0m"; const string RED = "\033[0;31m";
const string GREEN = "\033[0;32m"; const string YELLOW = "\033[0;33m";

bool Contains( std::string haystack, std::string needle, bool caseSensitive )
{
    string a = haystack; string b = needle;
    if ( !caseSensitive )
    {
        for ( unsigned int i = 0; i < a.size(); i++ ) { a[i] = tolower( a[i] ); }
        for ( unsigned int i = 0; i < b.size(); i++ ) { b[i] = tolower( b[i] ); }
    }
    return ( a.find( b ) != std::string::npos );
}

struct TestCase
{
    string assignmentName;
    string preEcho;
    string programName;
    string outputName;
    string arguments;
    map<string, bool> expectedOutputs;
};

void RunTest( TestCase& tc )
{
    // Run program
    string command = "";
    if ( tc.preEcho != "" )
    {
        command += "echo \"" + tc.preEcho + "\" | ";
    }
    command +="./" + tc.programName + " "
        + tc.arguments + " > " + tc.outputName;
    system( command.c_str() );

    cout << endl << "- PROGRAM: " << tc.assignmentName << endl;

    // Check output
    ifstream input( tc.outputName );
    string line, output="";
    while ( getline( input, line ) )
    {
        output += line + "\t";
    }
    cout << "  - OUTPUT: " << output << endl;
    for ( auto& expOut : tc.expectedOutputs )
    {
        if ( Contains( output, expOut.first, false ) )
        {
            expOut.second = true;
        }
    }

    // Test results
    for ( auto& expOut : tc.expectedOutputs )
    {
        string result = ( expOut.second )
            ? ( GREEN + "  - [PASS] Found output" )
            : ( RED   + "  - [FAIL] DIDN'T FIND OUTPUT" );
        cout << result << " \"" << expOut.first << "\""
             << " in program " << tc.assignmentName
             << CLEAR << endl;
    }

    // Cleanup
    command = "rm " + tc.outputName;
    system( command.c_str() );
}

std::string ToLower( const std::string& val )
{
    std::string str = "";
    for ( unsigned int i = 0; i < val.size(); i++ )
    {
        str += tolower( val[i] );
    }
    return str;
}

int main()
{
    // Practice 1
    {
        cout << YELLOW << "DEBUG TEXT FILE:" << CLEAR << endl;
        ifstream input( "practice1_debug/debug-questions.txt" );
        string buffer;
        while ( getline( input, buffer ) )
        {
            cout << buffer << endl;
        }
    }

    // Practice 2
    {
        system( "g++ practice2_*/*.cpp -o program2.exe" );

        vector<TestCase> testCases(2);
        testCases[0].assignmentName = "practice2";
        testCases[0].programName = "program2.exe";
        testCases[0].outputName = "program2-out.txt";
        testCases[0].arguments = "2 10";
        testCases[0].preEcho = "";
        testCases[0].expectedOutputs[ "2 4 6 8 10" ] = false;
        
        testCases[1].assignmentName = "practice2";
        testCases[1].programName = "program2.exe";
        testCases[1].outputName = "program2-out.txt";
        testCases[1].arguments = "10 20";
        testCases[1].preEcho = "";
        testCases[1].expectedOutputs[ "10 12 14 16 18 20" ] = false;

        for ( auto& test : testCases ) { RunTest( test ); }

        //system( "rm program2.exe" );
    }

    // Practice 3
    {
        cout << endl << "- PROGRAM: practice3" << endl;
        cout << YELLOW << "Practice 3 has to be manually graded" << CLEAR << endl;
    }

    // Practice 4
    {
        system( "g++ practice4_*/*.cpp -o program4.exe" );
        vector<TestCase> testCases(1);
        testCases[0].assignmentName = "practice4";
        testCases[0].programName = "program4.exe";
        testCases[0].outputName = "program4-out.txt";
        testCases[0].arguments = "";
        testCases[0].preEcho = "-5 50 5";
        testCases[0].expectedOutputs[ "Invalid input" ] = false;

        RunTest( testCases[0] );
        //system( "rm program4.exe" );
    }

    // Practice 5
    {
        system( "g++ practice5_*/*.cpp -o program5.exe" );

        vector<TestCase> testCases(5);
        testCases[0].assignmentName = "practice5";
        testCases[0].programName = "program5.exe";
        testCases[0].outputName = "program5-out.txt";
        testCases[0].arguments = "5 8";
        testCases[0].preEcho = "";
        testCases[0].expectedOutputs[ "0+5=5" ] = false;
        testCases[0].expectedOutputs[ "5+6=11" ] = false;
        testCases[0].expectedOutputs[ "11+7=18" ] = false;
        testCases[0].expectedOutputs[ "18+8=26" ] = false;
        testCases[0].expectedOutputs[ "The result is" ] = false;
        
        testCases[1].assignmentName = "practice5";
        testCases[1].programName = "program5.exe";
        testCases[1].outputName = "program5-out.txt";
        testCases[1].arguments = "1 5";
        testCases[1].preEcho = "";
        testCases[1].expectedOutputs[ "0+1=1" ] = false;
        testCases[1].expectedOutputs[ "1+2=3" ] = false;
        testCases[1].expectedOutputs[ "3+3=6" ] = false;
        testCases[1].expectedOutputs[ "6+4=10" ] = false;
        testCases[1].expectedOutputs[ "10+5=15" ] = false;
        testCases[1].expectedOutputs[ "The result is" ] = false;
        

        for ( auto& test : testCases ) { RunTest( test ); }

        //system( "rm program5.exe" );
    }

    // Graded program
    {
        system( "g++ graded_program/mars.cpp -o graded.exe" );
        
        vector<TestCase> testCases(2);

        testCases[0].assignmentName = "graded_program";
        testCases[0].programName = "graded.exe";
        testCases[0].outputName = "graded-out.txt";
        testCases[0].arguments = "10 10 1";
        testCases[0].preEcho = "";
        testCases[0].expectedOutputs[ "DAY 1" ] = false;
        testCases[0].expectedOutputs[ "DAY 2" ] = false;
        testCases[0].expectedOutputs[ "DAY 3" ] = false;
        testCases[0].expectedOutputs[ "DAY 4" ] = false;
        testCases[0].expectedOutputs[ "FOOD: 10" ] = false;
        testCases[0].expectedOutputs[ "FOOD: 7" ] = false;
        testCases[0].expectedOutputs[ "FOOD: 4" ] = false;
        testCases[0].expectedOutputs[ "FOOD: 1" ] = false;
        testCases[0].expectedOutputs[ "OXYGEN: 10" ] = false;
        testCases[0].expectedOutputs[ "OXYGEN: 9" ] = false;
        testCases[0].expectedOutputs[ "OXYGEN: 8" ] = false;
        testCases[0].expectedOutputs[ "OXYGEN: 7" ] = false;
        testCases[0].expectedOutputs[ "RAN OUT OF FOOD" ] = false;

        testCases[1].assignmentName = "graded_program";
        testCases[1].programName = "graded.exe";
        testCases[1].outputName = "graded-out.txt";
        testCases[1].arguments = "100 25 7";
        testCases[1].preEcho = "";
        testCases[1].expectedOutputs[ "DAY 1" ] = false;
        testCases[1].expectedOutputs[ "DAY 2" ] = false;
        testCases[1].expectedOutputs[ "DAY 3" ] = false;
        testCases[1].expectedOutputs[ "DAY 4" ] = false;
        testCases[1].expectedOutputs[ "FOOD: 100" ] = false;
        testCases[1].expectedOutputs[ "FOOD: 79" ] = false;
        testCases[1].expectedOutputs[ "FOOD: 58" ] = false;
        testCases[1].expectedOutputs[ "FOOD: 37" ] = false;
        testCases[1].expectedOutputs[ "OXYGEN: 25" ] = false;
        testCases[1].expectedOutputs[ "OXYGEN: 18" ] = false;
        testCases[1].expectedOutputs[ "OXYGEN: 11" ] = false;
        testCases[1].expectedOutputs[ "OXYGEN: 4" ] = false;
        testCases[1].expectedOutputs[ "RAN OUT OF OXYGEN" ] = false;

        for ( auto& test : testCases ) { RunTest( test ); }

        //system( "rm graded.exe" );
    }

    return 0;
}
