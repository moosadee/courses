#+HTML_HEAD: <style type='text/css'> body { font-family: sans-serif; } #content { width: 100%; max-width: 100%; } </style>
#+HTML_HEAD: <style type='text/css'> h3:before { content:"💾 "; } h3:after { content: ""; }  </style>
#+HTML_HEAD: <style type='text/css'> h3 { background: rgba( 0, 255, 150, 0.5 ); }  </style>
#+HTML_HEAD: <style type='text/css'> h4:before { content:"🟣 "; } h4:after { content: ""; }  </style>
#+HTML_HEAD: <style type='text/css'> code { background: rgba( 190, 111, 255, 0.2 ); padding: 2px; }  </style>
#+HTML_HEAD: <style type='text/css'> .src-terminal { background: #201826; color: #e5d3f4; font-weight: bold; } .src-terminal:before { content: "TERMINAL"; }  </style>
#+HTML_HEAD: <style type='text/css'> .src-form { background: #bddfff; color: #001528; font-weight: bold; } .src-form:before { content: "FORM"; }  </style>
#+HTML_HEAD: <style type='text/css'> .outline-1, .outline-2, .outline-3 { margin-left: 10px; padding: 10px; border: outset 2px #c0c0c0; background: rgba( 0, 255, 150, 0.1 ); margin-bottom: 10px; } </style>
#+HTML_HEAD: <style type='text/css'> .outline-3 { margin-bottom: 50px; } </style>

#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/tomorrow-night-blue.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script>
#+HTML_HEAD: <script>hljs.initHighlightingOnLoad();</script>

* Assignment and policy info

- Assignment info:
  - [[https://gitlab.com/moosadee/courses/-/blob/main/current/policies/practice-graded.org?ref_type=heads][Practice and Graded programs]]
  - [[https://gitlab.com/moosadee/courses/-/blob/main/current/policies/resubmit.org?ref_type=heads][Resubmission and regrading policy and procedure]]
  - [[https://gitlab.com/moosadee/courses/-/blob/main/current/policies/duedates.org?ref_type=heads][Due dates and available until dates]]
- How To:
  - [[https://gitlab.com/moosadee/courses/-/blob/main/reference/ide_vscode_build_and_run.org?ref_type=heads][Build and Run your program in VS Code]]
  - [[https://gitlab.com/moosadee/courses/-/blob/main/current/policies/turnin-lab.org?ref_type=heads][Turn in your lab]]
  - [[https://gitlab.com/moosadee/courses/-/blob/main/reference/vscode-git.org?ref_type=heads][Use Git and VS Code]]
- Quick reference:
  - [[https://gitlab.com/moosadee/courses/-/blob/main/current/share/read/CS200review.org?ref_type=heads][CS 200 code reference]]
  - [[https://gitlab.com/moosadee/courses/-/blob/main/reference/cpp_program_arguments.org?ref_type=heads][C++ program arguments]]
  - Debugging with [[https://gitlab.com/moosadee/courses/-/blob/main/reference/gdb.org?ref_type=heads][gdb]] / [[https://gitlab.com/moosadee/courses/-/blob/main/reference/lldb.org?ref_type=heads][lldb]]
  - [[https://gitlab.com/moosadee/courses/-/blob/main/reference/mac.org?ref_type=heads][Common issues on Mac]]
- Links:
  - [[https://moosadee.gitlab.io/courses/][R.W.'s courses homepage]]
  - [[https://gitlab.com/moosadee/courses/-/blob/main/current/cs200/student-repo/wk09_Classes/instructions.org][Assignment direct link]]

# ------------------------------------------------------------------------------

* Included files:

#+BEGIN_SRC
wk09_Classes
├── graded_program
│   ├── Fraction.cpp
│   ├── Fraction.h
│   ├── program.cpp
│   ├── Tests.cpp
│   └── Tests.h
├── instructions.html
├── instructions.org
├── practice1_pet
│   ├── main.cpp
│   ├── Pet.cpp
│   └── Pet.h
├── practice2_ingredient
│   ├── Ingredient.cpp
│   ├── Ingredient.h
│   └── main.cpp
└── tester.cpp
#+END_SRC

# ------------------------------------------------------------------------------

* Practice programs

# ------------------------------------------------------------------------------
** Practice 1 - Pet

*** Reference

Declaring a variable whose type is a class:

#+BEGIN_SRC form
  CLASSNAME VARNAME;
#+END_SRC

Calling a function that belongs to the class, via the object:

#+BEGIN_SRC form
  VARNAME.FUNCNAME( ARGUMENTS );
#+END_SRC

*** Example output

#+BEGIN_SRC terminal
-- MY PET --
NAME:   Luna
AGE:    5
ANIMAL: Cat
BREED:  Gremlin
#+END_SRC

*** Instructions

In this program the =Pet= class has already been created. Look at the Pet.h and
Pet.cpp files for reference.

You'll be modifying the main.cpp file. Within this file, do the following:

1. Declare a new Pet variable.
2. Use the SetName, SetAge, SetAnimal, and SetBreed functions to set up the pet.
3. Call the pet's Display() function.

# ------------------------------------------------------------------------------

** Practice 2 - Ingredient

*** Example output

#+BEGIN_SRC terminal
BREAD RECIPE

Ingredients
* 4.5 Cups of Flour
* 1 Tablespoons of Sugar
* 2.25 Teaspoons of Instant Yeast
* 1.66 Cups of Water
* 2.5 Teaspoons of Table Salt
#+END_SRC

*** Instructions

Within main.cpp the program is currently using separate variables to store
the ingredients for a recipe.

At the top, replace each ingredient with an =Ingredient= object. Use its
Setup function in order to set up its member variables.

Down below, replace the =cout= statements with a call to each Ingredient's
Display function.

# ------------------------------------------------------------------------------

* Graded programs

# ------------------------------------------------------------------------------

** Graded program 1

*** Reference

Convert an int to a string:

#+BEGIN_SRC form
  string str = to_string( myInteger );
#+END_SRC

Convert an int to a float:

#+BEGIN_SRC form
  float fl = float( myInteger );
#+END_SRC

Class declaration (.h file):

#+BEGIN_SRC form
  class CLASSNAME
  {
  public:
    RETURN FUNCNAME( PARAMETERS );
  
  private:
    TYPE VARNAME;
  }; // DON'T FORGET THE SEMICOLON!
#+END_SRC

Class function definition (.cpp file):

#+BEGIN_SRC form
  RETURN CLASSNAME::FUNCNAME( PARAMETERS )
  {
  }
#+END_SRC

UML reference:
- Top section: Class name
- Middle section: Member variables
- Bottom section: Member functions
- Items are written with types SECOND, so =name : string= translates into C++ as
  =string name=.
- =+= symbol: Public accessibility
- =-= symbol: Private accessibility

*** Example output

#+BEGIN_SRC terminal
$ ./a.out 1 2 x 2 3
1/2 x 2/3 = 2/6

$ ./a.out 1 2 + 2 3
3/6 + 4/6 = 7/6

$ ./a.out 1 2 / 2 3
1/2 / 2/3 = 3/4

$ ./a.out 1 2 - 2 3
3/6 - 4/6 = -1/6
#+END_SRC

*** Instructions

**** *Fraction.h*

Create the declaration for the Fraction class according to this UML diagram:

#+ATTR_HTML: :class uml
| *Fraction*                          |
|-------------------------------------|
| - =m_num= : int                     |
| - =m_den= : int                     |
|-------------------------------------|
| + SetNum( newNum : int ) : void     |
| + SetDenom( newDenom : int ) : void |
| + GetNum() : int                    |
| + GetDenom() : int                  |
| + GetDecimal() : float              |
| + GetString() : string              |
| - CommonDemon( int denom ) : void   |

*NOTE:* Make sure to add this line of code to the bottom of the
class declaration:

#+BEGIN_SRC cpp
friend void Test_Fraction();
#+END_SRC

This will make it so that the unit tests work properly with the Fraction class.

~

**** *Fraction.cpp*

Within Fraction.cpp, implement each of the following functions:

***** *SetNum*

#+ATTR_HTML: :class funcspecs
| Return: void               |
| Parameters: newNum, an int |

Assign the value of the numerator member variable to the value passed in as
the =newNum= parameter.

***** *SetDenom*

#+ATTR_HTML: :class funcspecs
| Return: void                 |
| Parameters: newDenom, an int |

ERROR CHECK: Check to see if =newDenom= is an invalid value (i.e., 0). If so,
display an error message and =return;= to leave the function early without
changing anything.

Assign the value of the denominator member variable to the value passed in as
the =newDenom= parameter.

***** *GetNum*

#+ATTR_HTML: :class funcspecs
| Return: int      |
| Parameters: none |

Return the value of the numerator member variable.

***** *GetDenom*

#+ATTR_HTML: :class funcspecs
| Return: int      |
| Parameters: none |

Return the value of the denominator member variable.

***** *GetDecimal*

#+ATTR_HTML: :class funcspecs
| Return: float    |
| Parameters: none |

Return the result of the numerator divided by the denominator. Note that you
will need to convert at least one of these items into a float in order for
the math to preserve the remainder.

***** *GetString*

#+ATTR_HTML: :class funcspecs
| Return: string   |
| Parameters: none |

Return a string that shows "numerator/denominator". You can use the =to_string=
function to convert integers to strings.

***** *CommonDenom*

#+ATTR_HTML: :class funcspecs
| Return: void              |
| Parameters: denom, an int |

For this function, multiply the numerator member variable by the =denom=
parameter and update the numerator member.

Multiply the denominator member variable by the =denom= parameter and update
the denominator variable.

~

**** *main.cpp*

Once you've implemented the Fraction class, uncomment out the code in the lower
part of =main()= and test out the program.
