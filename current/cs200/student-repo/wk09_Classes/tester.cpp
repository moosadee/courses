#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <string>
#include <map>
using namespace std;

const string CLEAR = "\033[0m"; const string RED = "\033[0;31m";
const string GREEN = "\033[0;32m"; const string YELLOW = "\033[0;33m";

bool Contains( std::string haystack, std::string needle, bool caseSensitive )
{
    string a = haystack; string b = needle;
    if ( !caseSensitive )
    {
        for ( unsigned int i = 0; i < a.size(); i++ ) { a[i] = tolower( a[i] ); }
        for ( unsigned int i = 0; i < b.size(); i++ ) { b[i] = tolower( b[i] ); }
    }
    return ( a.find( b ) != std::string::npos );
}

struct TestCase
{
    string assignmentName;
    string preEcho;
    string programName;
    string outputName;
    string arguments;
    map<string, bool> expectedOutputs;
};

void RunTest( TestCase& tc )
{
    // Run program
    string command = "";
    if ( tc.preEcho != "" )
    {
        command += "echo \"" + tc.preEcho + "\" | ";
    }
    command +="./" + tc.programName + " "
        + tc.arguments + " > " + tc.outputName;
    system( command.c_str() );

    cout << endl << "- PROGRAM: " << tc.assignmentName << endl;

    // Check output
    ifstream input( tc.outputName );
    string line, output="";
    while ( getline( input, line ) )
    {
        output += line + "\t";
    }
    cout << "  - OUTPUT: " << output << endl;
    for ( auto& expOut : tc.expectedOutputs )
    {
        if ( Contains( output, expOut.first, false ) )
        {
            expOut.second = true;
        }
    }

    // Test results
    for ( auto& expOut : tc.expectedOutputs )
    {
        string result = ( expOut.second )
            ? ( GREEN + "  - [PASS] Found output" )
            : ( RED   + "  - [FAIL] DIDN'T FIND OUTPUT" );
        cout << result << " \"" << expOut.first << "\""
             << " in program " << tc.assignmentName
             << CLEAR << endl;
    }

    // Cleanup
    command = "rm " + tc.outputName;
    system( command.c_str() );
}

int main()
{
    // Practice 1
    {
        cout << YELLOW << "practice1 must be checked manually!" << CLEAR << endl;
    }

    // Practice 2
    {
        cout << YELLOW << "practice2 must be checked manually!" << CLEAR << endl;
    }

    // Graded program
    {
        system( "g++ graded_program/*.h graded_program/*.cpp -o graded.exe" );
        cout << "- PROGRAM: graded" << endl;
        system( "./graded.exe test" );
        system( "rm graded.exe" );
    }

    return 0;
}
