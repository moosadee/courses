#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

#include "Tests.h"
#include "Fraction.h"

int main( int argCount, char* args[] )
{
    // ARGUMENT CHECK:
    if ( argCount == 2 && string( args[1] ) == "test" )
    {
        Test_Fraction();
        return 0;
    }

    if ( argCount < 6 )
    {
        cout << "NOT ENOUGH ARGUMENTS!" << endl;
        cout << "Expected: " << args[0]
             << " num1 denom1 operation num2 denom2" << endl;
        cout << "Operations: + - x /" << endl;
        return 1;
    }

    int num1 = stoi( args[1] );
    int denom1 = stoi( args[2] );
    char op = args[3][0];
    int num2 = stoi( args[4] );
    int denom2 = stoi( args[5] );

    if ( denom1 == 0 || denom2 == 0 )
    {
        cerr << "ERROR: Can't have 0 as a denominator!" << endl;
        return 2;
    }

    // TODO: Uncomment out this region once you create the Fraction class.
    /*
    Fraction frac1;
    frac1.SetNum( num1 );
    frac1.SetDenom( denom1 );

    Fraction frac2;
    frac2.SetNum( num2 );
    frac2.SetDenom( denom2 );

    Fraction result;

    switch( op )
    {
    case '+':
        frac1.CommonDenominatorize( denom2 );
        frac2.CommonDenominatorize( denom1 );
        result.SetNum( frac1.GetNum() + frac2.GetNum() );
        result.SetDenom( frac1.GetDenom() );
        break;

    case '-':
        frac1.CommonDenominatorize( denom2 );
        frac2.CommonDenominatorize( denom1 );
        result.SetNum( frac1.GetNum() - frac2.GetNum() );
        result.SetDenom( frac1.GetDenom() );
        break;

    case 'x':
        result.SetNum( frac1.GetNum() * frac2.GetNum() );
        result.SetDenom( frac1.GetDenom() * frac2.GetDenom() );
        break;

    case '/':
        result.SetNum( frac1.GetNum() * frac2.GetDenom() );
        result.SetDenom( frac1.GetDenom() * frac2.GetNum() );
        break;

    default:
        cerr << "UNKNOWN OPERATION!" << endl;
        return 3;
    }

    cout << frac1.GetString()
         << " " << op << " "
         << frac2.GetString()
         << " = " << result.GetString() << endl;
    */

    return 0;
}
