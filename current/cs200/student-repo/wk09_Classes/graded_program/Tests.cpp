#include "Tests.h"

#include "Fraction.h"

#include <iostream>
#include <string>
using namespace std;

void Test_Fraction()
{
    const string GRN = "\033[0;32m";
    const string RED = "\033[0;31m";
    const string BOLD = "\033[37;45m";
    const string CLR = "\033[0m";
    int totalTests = 0, totalPass = 0, totalFail = 0;

    { // TEST BEGIN
        totalTests++;
        string testName = "Call SetNum, double check m_num...";
        Fraction frac;
        frac.SetNum( 2 );
        auto expOut = 2;
        auto actOut = frac.m_num;
        
        if ( actOut != expOut )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* Expected result: " << expOut  << endl;
            cout << "* Actual result:   " << actOut << CLR << endl;
            totalFail++;
        }
        else
        {
            cout << GRN << "[PASS] " << testName << CLR << endl;
            totalPass++;
        }
    }

    { // TEST BEGIN
        totalTests++;
        string testName = "Call SetNum, double check m_num...";
        Fraction frac;
        frac.SetNum( -5 );
        auto expOut = -5;
        auto actOut = frac.m_num;
        
        if ( actOut != expOut )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* Expected result: " << expOut  << endl;
            cout << "* Actual result:   " << actOut << CLR << endl;
            totalFail++;
        }
        else
        {
            cout << GRN << "[PASS] " << testName << CLR << endl;
            totalPass++;
        }

        { // TEST BEGIN
            totalTests++;
            string testName = "Call SetDenom, double check m_den...";
            Fraction frac;
            frac.SetDenom( 2 );
            auto expOut = 2;
            auto actOut = frac.m_den;
        
            if ( actOut != expOut )
            {
                cout << RED << "[FAIL] " << testName << endl;
                cout << "* Expected result: " << expOut  << endl;
                cout << "* Actual result:   " << actOut << CLR << endl;
                totalFail++;
            }
            else
            {
                cout << GRN << "[PASS] " << testName << CLR << endl;
                totalPass++;
            }
        }

        { // TEST BEGIN
            totalTests++;
            string testName = "Call SetDenom, double check m_den...";
            Fraction frac;
            frac.SetDenom( -5 );
            auto expOut = -5;
            auto actOut = frac.m_den;
        
            if ( actOut != expOut )
            {
                cout << RED << "[FAIL] " << testName << endl;
                cout << "* Expected result: " << expOut  << endl;
                cout << "* Actual result:   " << actOut << CLR << endl;
                totalFail++;
            }
            else
            {
                cout << GRN << "[PASS] " << testName << CLR << endl;
                totalPass++;
            }
        }

        { // TEST BEGIN
            totalTests++;
            string testName = "Call SetDenom with 0, don't update m_den...";
            Fraction frac;
            frac.m_den = 10;
            frac.SetDenom( 0 );
            auto expOut = 10;
            auto actOut = frac.m_den;
        
            if ( actOut != expOut )
            {
                cout << RED << "[FAIL] " << testName << endl;
                cout << "* Expected result: " << expOut  << endl;
                cout << "* Actual result:   " << actOut << CLR << endl;
                totalFail++;
            }
            else
            {
                cout << GRN << "[PASS] " << testName << CLR << endl;
                totalPass++;
            }
        }

        { // TEST BEGIN
            totalTests++;
            string testName = "Set m_num, call GetNum, check result...";
            Fraction frac;
            frac.m_num = 7;
            auto expOut = 7;
            auto actOut = frac.GetNum();
        
            if ( actOut != expOut )
            {
                cout << RED << "[FAIL] " << testName << endl;
                cout << "* Expected result: " << expOut  << endl;
                cout << "* Actual result:   " << actOut << CLR << endl;
                totalFail++;
            }
            else
            {
                cout << GRN << "[PASS] " << testName << CLR << endl;
                totalPass++;
            }
        }

        { // TEST BEGIN
            totalTests++;
            string testName = "Set m_num, call GetNum, check result...";
            Fraction frac;
            frac.m_num = 3;
            auto expOut = 3;
            auto actOut = frac.GetNum();
        
            if ( actOut != expOut )
            {
                cout << RED << "[FAIL] " << testName << endl;
                cout << "* Expected result: " << expOut  << endl;
                cout << "* Actual result:   " << actOut << CLR << endl;
                totalFail++;
            }
            else
            {
                cout << GRN << "[PASS] " << testName << CLR << endl;
                totalPass++;
            }
        }

        { // TEST BEGIN
            totalTests++;
            string testName = "Set m_den, call GetDenom, check result...";
            Fraction frac;
            frac.m_den = 7;
            auto expOut = 7;
            auto actOut = frac.GetDenom();
        
            if ( actOut != expOut )
            {
                cout << RED << "[FAIL] " << testName << endl;
                cout << "* Expected result: " << expOut  << endl;
                cout << "* Actual result:   " << actOut << CLR << endl;
                totalFail++;
            }
            else
            {
                cout << GRN << "[PASS] " << testName << CLR << endl;
                totalPass++;
            }
        }

        { // TEST BEGIN
            totalTests++;
            string testName = "Set m_den, call GetDenom, check result...";
            Fraction frac;
            frac.m_den = 9;
            auto expOut = 9;
            auto actOut = frac.GetDenom();
        
            if ( actOut != expOut )
            {
                cout << RED << "[FAIL] " << testName << endl;
                cout << "* Expected result: " << expOut  << endl;
                cout << "* Actual result:   " << actOut << CLR << endl;
                totalFail++;
            }
            else
            {
                cout << GRN << "[PASS] " << testName << CLR << endl;
                totalPass++;
            }
        }

        { // TEST BEGIN
            totalTests++;
            string testName = "Set m_num and m_den, call GetDecimal, check result...";
            Fraction frac;
            frac.m_num = 2;
            frac.m_den = 4;
            auto expOut = 0.5;
            auto actOut = frac.GetDecimal();
        
            if ( actOut != expOut )
            {
                cout << RED << "[FAIL] " << testName << endl;
                cout << "* Expected result: " << expOut  << endl;
                cout << "* Actual result:   " << actOut << CLR << endl;
                totalFail++;
            }
            else
            {
                cout << GRN << "[PASS] " << testName << CLR << endl;
                totalPass++;
            }
        }

        { // TEST BEGIN
            totalTests++;
            string testName = "Set m_num and m_den, call GetDecimal, check result...";
            Fraction frac;
            frac.m_num = 6;
            frac.m_den = 3;
            auto expOut = 2.0;
            auto actOut = frac.GetDecimal();
        
            if ( actOut != expOut )
            {
                cout << RED << "[FAIL] " << testName << endl;
                cout << "* Expected result: " << expOut  << endl;
                cout << "* Actual result:   " << actOut << CLR << endl;
                totalFail++;
            }
            else
            {
                cout << GRN << "[PASS] " << testName << CLR << endl;
                totalPass++;
            }
        }

        { // TEST BEGIN
            totalTests++;
            string testName = "Set m_num and m_den, call GetString, check result...";
            Fraction frac;
            frac.m_num = 2;
            frac.m_den = 4;
            auto expOut = "2/4";
            auto actOut = frac.GetString();
        
            if ( actOut != expOut )
            {
                cout << RED << "[FAIL] " << testName << endl;
                cout << "* Expected result: " << expOut  << endl;
                cout << "* Actual result:   " << actOut << CLR << endl;
                totalFail++;
            }
            else
            {
                cout << GRN << "[PASS] " << testName << CLR << endl;
                totalPass++;
            }
        }

        { // TEST BEGIN
            totalTests++;
            string testName = "Set m_num and m_den, call GetString, check result...";
            Fraction frac;
            frac.m_num = 1;
            frac.m_den = 3;
            auto expOut = "1/3";
            auto actOut = frac.GetString();
        
            if ( actOut != expOut )
            {
                cout << RED << "[FAIL] " << testName << endl;
                cout << "* Expected result: " << expOut  << endl;
                cout << "* Actual result:   " << actOut << CLR << endl;
                totalFail++;
            }
            else
            {
                cout << GRN << "[PASS] " << testName << CLR << endl;
                totalPass++;
            }
        }

        { // TEST BEGIN
            totalTests++;
            string testName = "Set m_num=1 and m_den=2, call CommonDenom(4), check result...";
            Fraction frac;
            frac.m_num = 1;
            frac.m_den = 2;
            frac.CommonDenom( 4 );
            auto expOut_num = 4;
            auto actOut_num = frac.m_num;
            auto expOut_den = 8;
            auto actOut_den = frac.m_den;
        
            if ( actOut_num != expOut_num )
            {
                cout << RED << "[FAIL] " << testName << endl;
                cout << "* Expected m_num: " << expOut_num << endl;
                cout << "* Actual m_num:   " << actOut_num << CLR << endl;
                totalFail++;
            }
            else if ( actOut_den != expOut_den )
            {
                cout << RED << "[FAIL] " << testName << endl;
                cout << "* Expected m_den: " << expOut_den << endl;
                cout << "* Actual m_den:   " << actOut_den << CLR << endl;
                totalFail++;                
            }
            else
            {
                cout << GRN << "[PASS] " << testName << CLR << endl;
                totalPass++;
            }
        }

        { // TEST BEGIN
            totalTests++;
            string testName = "Set m_num=2 and m_den=3, call CommonDenom(5), check result...";
            Fraction frac;
            frac.m_num = 2;
            frac.m_den = 3;
            frac.CommonDenom( 5 );
            auto expOut_num = 10;
            auto actOut_num = frac.m_num;
            auto expOut_den = 15;
            auto actOut_den = frac.m_den;
        
            if ( actOut_num != expOut_num )
            {
                cout << RED << "[FAIL] " << testName << endl;
                cout << "* Expected m_num: " << expOut_num << endl;
                cout << "* Actual m_num:   " << actOut_num << CLR << endl;
                totalFail++;
            }
            else if ( actOut_den != expOut_den )
            {
                cout << RED << "[FAIL] " << testName << endl;
                cout << "* Expected m_den: " << expOut_den << endl;
                cout << "* Actual m_den:   " << actOut_den << CLR << endl;
                totalFail++;                
            }
            else
            {
                cout << GRN << "[PASS] " << testName << CLR << endl;
                totalPass++;
            }
        }
    }

    string results = "FINISHED (" + to_string( totalTests )
        + " TESTS, " + to_string( totalPass )
        + " PASS, " + to_string( totalFail ) + " FAIL)";
    cout << BOLD << results << string( 80-results.size(), '-' )<< CLR << endl;
}
