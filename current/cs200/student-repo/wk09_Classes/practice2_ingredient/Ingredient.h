#ifndef _INGREDIENT
#define _INGREDIENT

#include <string>
using namespace std;

class Ingredient
{
public:
    void Setup( string new_name, string new_unit, float new_amount );
    void Display();

private:
    // Member variables
    string name = "unset";
    string unit = "unset";
    float amount = 0;
};

#endif
