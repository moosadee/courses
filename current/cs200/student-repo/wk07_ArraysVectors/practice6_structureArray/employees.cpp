#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
using namespace std;

struct Employee
{
    int year;
    string name;
    string title;
    float wage;
};

int main()
{
    cout << endl << "-- SALARY PROGRAM -- " << endl;
    cout << left;

    vector<Employee> employees = {
        { 2023, "Droo Crown", "President", 391567 },
        { 2023, "Barry Cirrus", "VP/Academic Affairs", 202022 },
        { 2023, "Flan Lute", "Associate Professor", 141140 },
        { 2023, "Zebra Welder", "Assistant Dean", 115973 },
        { 2023, "Berra Veabre", "Associate Professor", 103906 },
        { 2023, "Arrdub Singin", "Associate Professor", 85835 },
        { 2023, "Mauren Mimble", "Associate Professor", 81470 }
    };

    cout
        << setw( 10 ) << "YEAR"
        << setw( 20 ) << "NAME"
        << setw( 30 ) << "POSITION"
        << setw( 20 ) << "SALARY"
        << endl;
    cout << string( 80, '-' ) << endl;

    cout << endl << "-- GOODBYE! --" << endl;  
    return 0;
}
