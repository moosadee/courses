// PROGRAM: Practice using the STL vector
#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
using namespace std;

struct Product
{
    string name;
    float price;
};

int main()
{
    cout << endl << "-- TACO PLACE -- " << endl;
    cout << fixed << setprecision(2);

    // Set up the data
    vector<Product> product_list(3);

    product_list[0].name = "Bean Burrito";
    product_list[0].price = 1.99;

    product_list[1].name = "Crunchy Taco";
    product_list[1].price = 1.79;

    product_list[2].name = "Baja Blast";
    product_list[2].price = 1.29;

    // Display the data
    cout << "MENU" << endl;
    for ( size_t i = 0; i < product_list.size(); i++ )
    {
        cout << i << ". " << product_list[i].name
             << " ($" << product_list[i].price << ")" << endl;
    }

    int index;

    cout << endl << "What do you want to eat? ";
    cin >> index;

    cout << endl << "You chose:" << endl;
  
    cout << endl << "Product: ";
    // 1. TODO: Display the product at `index`'s name.

    cout << endl << "Cost: $";
    // 2. TODO: Display the product at `index`'s price.

    cout << endl << "-- GOODBYE! --" << endl;  
    return 0;
}
