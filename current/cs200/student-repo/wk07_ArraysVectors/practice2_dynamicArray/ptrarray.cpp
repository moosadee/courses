// PROGRAM: Practice creating a dynamic array using a pointer
#include <iostream>
#include <array>
#include <string>
using namespace std;

int main()
{
    cout << endl << "-- MY CLASSES v2 -- " << endl;
    size_t total_classes;

    cout << "How many classes do you have? ";
    cin >> total_classes;
    cin.ignore();

    // 1. TODO: Create a new dynamic array of strings, use `total_classes`
    // as the size.

    cout << endl << "Getting input:" << endl;
    // 2. TODO: Loop over all the indices of the array. Within the for loop,
    // display "Enter class #" and the value of `i`. Afterwards,
    // get the user's input and store it in the array at position `[i]`.

    cout << endl << "Resulting array:" << endl;

    for ( size_t i = 0; i < total_classes; i++ )
    {
        // 3. TODO: Display the index `i` and the element `ARRAYNAME[i]`.
    }

    // 4. TODO: Free the array's memory

    cout << endl << "-- GOODBYE! --" << endl;  
    return 0;
}
