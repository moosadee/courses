#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int main( int argCount, char* args[] )
{
    cout << endl << "-- IMAGE DRAWER -- " << endl;
    if ( argCount < 2 ) // Check for required arguments
    {
        cout << "Not enough arguments! Expected: "
             << args[0] << " FILENAME" << endl; // Give instructions
        return 1; // Exit early
    }

    ifstream input;
    input.open( string( args[1] ) );
    if ( input.fail() )
    {
        cout << "ERROR: Could not find file!" << endl;
        return 2; // Exit early
    }

    const int HEIGHT = 5;
    const int WIDTH = 5;
    // TODO: Declare a 2D array of chars named `image` with [HEIGHT][WIDTH]

    // Read all the text into the array
    int x = 0, y = 0;
    while ( input >> image[y][x] )
    {
        x++;
        if ( x >= WIDTH )
        {
            x = 0;
            y++;
        }
    }

    // Display the image
    // TODO: Create a for loop, starting from y = 0 while y < HEIGHT.
    // Within that for loop, put another: starting from x = 0 while x < WIDTH.
    // Within the inner loop, `cout` the value of `image` at [y][x].
    // After the inner loop but before the end of the outer loop, cout endl.

    cout << endl << "-- GOODBYE! --" << endl;
    return 0;
}
