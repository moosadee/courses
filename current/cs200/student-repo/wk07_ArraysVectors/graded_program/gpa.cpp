#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
#include <vector>     // Library that contains `vector`
#include <iomanip>    // Library for formatting; `setprecision`
using namespace std;  // Using the C++ STanDard libraries

int main()
{
    cout << "GPA CALCULATOR" << endl << endl;

    vector<float> course_grades;
    float this_grade;
    char choice;

    cout << "A = 4.0 \t B = 3.0 \t C = 2.0"
         << "\t D = 1.0 \t F = 0.0" << endl << endl;

    // GRADE ENTRY -------------------------------------------------------------
    while ( choice != 'N' )
    {
        cout << "Enter a grade: ";
        cin >> this_grade;

        // TODO: Push `this_grade` into the `grades` vector.

        cout << "Enter another? (Y/N): ";
        cin >> choice;
        choice = toupper( choice );
        cout << endl;
    }

    // CALCULATE GPA -----------------------------------------------------------
    float total = 0;
    float result_gpa = 0;

    // TODO: Use a for loop to iterate over all the grades
    // in the `course_grades` vector.
    // Within the loop, add each grade element to the `total`.

    // TODO: Calculate the average (total divided by
    // the amount of elements, use vector's size function.)

    cout << "RESULT: " << result_gpa << endl;

    return 0;
}
