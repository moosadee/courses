#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <string>
#include <map>
using namespace std;

struct TestCase
{
    string command;
    map<string, bool> expectedOutputs;
};

int main()
{
    const string CLEAR    = "\033[0m"; const string RED      = "\033[0;31m"; const string GREEN    = "\033[0;32m"; const string YELLOW   = "\033[0;33m";

    vector<string> sourceFiles = { "shop.cpp" };
    string command = "";
    vector<TestCase> testCases(3);

    testCases[0].command = "./a.out 100 25 25 25 > result.txt";
    testCases[0].expectedOutputs[ "You have enough money!" ] = false;
    testCases[0].expectedOutputs[ "$25" ] = false;

    testCases[1].command = "./a.out 50 35 25 15 > result.txt";
    testCases[1].expectedOutputs[ "You don't have enough money!" ] = false;
    testCases[1].expectedOutputs[ "$-25" ] = false;

    testCases[2].command = "./a.out 30 5 10 15 > result.txt";
    testCases[2].expectedOutputs[ "You have exactly enough money!" ] = false;

    cout << "- BUILDING PROGRAM -------------------------------" << endl;
    command = "g++ ";
    for ( auto& src : sourceFiles ) { command += src + " "; }
    cout << command << endl;
    system( command.c_str() );

    cout << "- TEST PROGRAM -----------------------------------" << endl;
    // Remove previous output file
    system( "rm result.txt" );

    for ( auto& test : testCases )
    {
	system( test.command.c_str() );
	cout << string( 80, '-' ) << endl << "TEST: " << test.command << endl;

	ifstream input( "result.txt" );
	string outline;
	cout << string( 80, '-' ) << endl << "Program output:" << endl;
	while ( getline( input, outline ) )
	{
	    // Check if this output has any of the required items
	    for ( auto& expected : test.expectedOutputs )
	    {
		if ( outline.find( expected.first ) != string::npos )
		{
		    expected.second = true;
		}
	    }
	    cout << outline << endl;
	}
	cout << endl;

	cout << "TEST CASES..." << endl;
	// Were all results found?
	bool allResultsFound = true;
	for ( auto& expected : test.expectedOutputs )
	{
	    if ( expected.second == true )
	    {
		cout << GREEN << "[PASS] Output found: \"" << expected.first << "\"" << endl;
	    }
	    else
	    {
		allResultsFound = false;
		cout << RED << "[FAIL] Output not found: \"" << expected.first << "\"" << endl;
	    }
	}

	if ( allResultsFound )
	{
	    cout << GREEN << "[PASS] " << test.command << endl;
	}
	cout << CLEAR;
    }

    return 0;
}
