#include <iostream>
#include <iomanip>
using namespace std;

/** shop program
    argument 1: how much money you have
    argument 2: price 1
    argument 3: price 2
    argument 4: price 3
*/
int main( int argCount, char* args[] )
{
    cout << "-- SHOP CALCULATOR -- " << endl;
    // Check to see if program was ran with enough arguments
    if ( argCount < 5 ) { cout << "Not enough arguments! Expected: " << args[0] << " CURRENT_MONEY PRICE1 PRICE2 PRICE3" << endl; return 1; }
    cout << fixed << setprecision( 2 );

    // Converting arguments to variables
    float current_money  = stof( args[1] );
    float price1         = stof( args[2] );
    float price2         = stof( args[3] );
    float price3         = stof( args[4] );

    cout << "Current money:  $" << currentMoney << endl;
    cout << "Cost of item 1: $" << price1 << endl;
    cout << "Cost of item 2: $" << price2 << endl;
    cout << "Cost of item 3: $" << price3 << endl;

    // - STUDENT CODE ----------------------------------------------------------
    // -------------------------------------------------------------------------

    cout << "-- GOODBYE! --" << endl;
}
