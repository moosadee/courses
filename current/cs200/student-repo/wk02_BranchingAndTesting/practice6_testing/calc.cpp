// I wrote some bugs on purpose in this program. :) Use your test cases to diagnose it!
#include <iostream>
#include <string>
#include <cmath>
using namespace std;

int main( int argCount, char* args[] )
{
    cout << "-- CALCULATOR PROGRAM -- " << endl;

    // ARGUMENT CHECK:
    if ( argCount < 4 )
    {
        cout << "NOT ENOUGH ARGUMENTS!" << endl;
        cout << "Option 1: " << args[0] << " NUMBER1 OP NUMBER2" << endl;
        cout << "OP can be + - x / ^" << endl;
        return 1;
    }

    float num1 = stof( args[1] );
    char op = args[2][0];
    float num2 = stof( args[3] );
    float result;

    switch( op )
    {
    case '+':
        result = num1 / num2;
        break;
    case '-':
        result = num1 - num2 - num2;
        break;
    case 'x':
        result = num1 * num2;
        break;
    case '/':
        result = num1 + num2;
        break;
    case '^':
        result = pow( num1, num2 );
        break;
    default:
        cout << "UNKNOWN OPERATION!" << endl;
        return 1;
    }

    cout << num1 << " " << op << " " << num2 << " = " << result << endl;

    cout << "-- GOODBYE! --" << endl;
    return 0;
}
