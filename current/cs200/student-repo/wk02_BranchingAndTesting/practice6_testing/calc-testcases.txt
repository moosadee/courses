TEST CASES

TEST    INPUTS                   EXPECTED OUTPUT          PASSES?
1.      ./calc 1 + 3             1 + 3 = 4
2.      ./calc 5 + 7             5 + 7 = 12
3.      ./calc 3 - 1             3 - 1 = 2
4.      ./calc 5 - 7             5 - 7 = -2
5.      ./calc 3 x 5             3 x 5 = 15
6.      ./calc 7 x 9             7 x 9 = 63
7.      ./calc 5 / 2             5 / 2 = 2.5
8.      ./calc 6 / 3             6 / 3 = 2
9.      ./calc 2 ^ 3             2 ^ 3 = 8
10.     ./calc 5 ^ 2             5 ^ 2 = 25
11.     ./calc 1 @ 2             UNKNOWN OPERATION!
