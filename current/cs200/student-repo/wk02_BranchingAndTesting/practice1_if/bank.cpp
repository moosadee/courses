#include <iostream>
#include <iomanip>
using namespace std;

int main( int argCount, char* args[] )
{
    cout << "-- BANK BALANCE PROGRAM -- " << endl;
    if ( argCount < 3 ) { cout << "NOT ENOUGH ARGUMENTS! Expected: withdraw balance" << endl; return 1; }

    float withdraw = stof( args[1] );
    float balance = stof( args[2] );
    float remaining = 0;

    cout << fixed << setprecision( 2 ); // USD formatting

    // - STUDENT CODE ----------------------------------------------------------
    // -------------------------------------------------------------------------

    cout << endl << "-- GOODBYE! --" << endl;
    return 0;
}
