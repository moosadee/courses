TEST CASES

TEST    INPUTS                       EXPECTED OUTPUT                PASSES?
1.      ./bank.exe 50 100            Balance: $50
2.      ./bank.exe 25 10             Balance: $-15 (OVERDRAWN)
