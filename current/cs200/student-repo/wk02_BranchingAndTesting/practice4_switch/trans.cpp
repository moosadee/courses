#include <iostream>
#include <string>
using namespace std;

int main(int argCount, char *args[])
{
    cout << "-- TRANSLATION PROGRAM -- " << endl;
    if (argCount < 2) {
        cout << "NOT ENOUGH ARGUMENTS! Expected: language" << endl;
        cout << "e = esperanto, s = spanish, m = mandarin, h = hindi" << endl;
        return 1;
    }

    char language = args[1][0];

    string english = "cat";
    string translated = "";
    string languageName = "";

    // - STUDENT CODE ----------------------------------------------------------
    // -------------------------------------------------------------------------

    cout << "English: " << english << ", " << languageName << ": " << translated << endl;

    cout << "-- GOODBYE! --" << endl;
    return 0;
}
