#include <iostream>   // Console input and output
#include <string>     // String data type
using namespace std;  // Using the STanDard C++ library

int main( int argCount, char* args[] )
{
    cout <<"-- APARTMENT PROGRAM v2 -- " << endl;
    // Check to see if program was ran with enough arguments
    if ( argCount < 6 ) { cout << "Not enough arguments! Expected: " << args[0] << " ROOM1NAME ROOM1WIDTH ROOM1LENGTH ROOM2NAME ROOM2WIDTH ROOM2LENGTH" << endl; return 1; }

    // Converting arguments to variables
    string room1_name   = string( args[1] );
    float  room1_width  = stof( args[2] );
    float  room1_length = stof( args[3] );
    float  room1_area   = 0;

    string room2_name   = string( args[4] );
    float  room2_width  = stof( args[5] );
    float  room2_length = stof( args[6] );
    float  room2_area   = 0;

    float total_area = 0;

    // - STUDENT CODE ----------------------------------------------------------
    // TODO: Copy the rooms area calculation and cout statements from your week 1 apartment.cpp program.
    // TODO: Use an if statement to figure out if one room or the other is bigger, or if they're the same size.
    // -------------------------------------------------------------------------

    cout << "-- GOODBYE! --" << endl;
    return 0;
}
