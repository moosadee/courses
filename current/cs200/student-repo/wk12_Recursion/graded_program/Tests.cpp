#include "Tests.h"

#include <iostream>
#include <vector>
#include <string>
using namespace std;

void Test_GetFirstUppercase()
{
    const string GRN = "\033[0;32m"; const string RED = "\033[0;31m"; const string BOLD = "\033[37;45m"; const string CLR = "\033[0m";
    int totalTests = 0, totalPass = 0, totalFail = 0;

    { // TEST BEGIN
				totalTests++;
				string input = "the quick brown FOX";
				char expectOut = 'F';
				char actualOut = GetFirstUppercase( input );
				string testName = "GetFirstUppercase( \"the quick brown FOX\" ) should be 'F'";

				if ( actualOut == expectOut )
				{
						cout << GRN << "[PASS] " << testName << endl;
						totalPass++;
				}
				else
				{
						cout << RED << "[FAIL] " << testName << endl;
						cout << "* Expected result: " << expectOut << endl;
						cout << "* Actual result:   " << actualOut << endl;
						totalFail++;
				}
				cout << CLR;
    } // TEST END

    { // TEST BEGIN
				totalTests++;
				string input = "there are no capital letters";
				char expectOut = ' ';
				char actualOut = GetFirstUppercase( input );
				string testName = "GetFirstUppercase( \"there are no capital letters\" ) should be ' '";

				if ( actualOut == expectOut )
				{
						cout << GRN << "[PASS] " << testName << endl;
						totalPass++;
				}
				else
				{
						cout << RED << "[FAIL] " << testName << endl;
						cout << "* Expected result: " << expectOut << endl;
						cout << "* Actual result:   " << actualOut << endl;
						totalFail++;
				}
				cout << CLR;
    } // TEST END

    string results = "FINISHED (" + to_string( totalTests ) + " TESTS, " + to_string( totalPass ) + " PASS, " + to_string( totalFail ) + " FAIL)";
    cout << BOLD << results << string( 80-results.size(), '-' )<< CLR << endl;
}
