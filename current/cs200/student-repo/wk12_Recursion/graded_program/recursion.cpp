#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
using namespace std;

#include "Tests.h"

//! Helper function to figure out if letter is upper-case
bool IsUppercase( char letter )
{
    return ( letter != ' ' && toupper( letter ) == letter );
}

char GetFirstUppercase( string text, unsigned int pos /* = 0 */ ) // Definition
{
    for ( size_t i = pos; i < text.size(); i++ )
    {
				if ( IsUppercase( text[i] ) ) { return text[i]; }
    }
    return ' '; // Not found
		// - STUDENT CODE ----------------------------------------------------------
    // TODO: Replace the above iterative implementation with a RECURSIVE implementation.
    //       Remove the code above once done.

		// -------------------------------------------------------------------------
}

// ------------------------- //
// -- DO NOT CHANGE MAIN! -- //
// ------------------------- //
int main( int argCount, char* args[] )
{
    // ARGUMENT CHECK:
    if ( argCount == 2 && string( args[1] ) == "test" )
    {
				Test_GetFirstUppercase();
				return 0;
    }
    else if ( argCount < 3 )
    {
				cout << "NOT ENOUGH ARGUMENTS!" << endl;
				cout << "Option 1: " << args[0] << " text phrase blah blah blah" << endl;
				cout << "Option 2: " << args[0] << " test" << endl;
				return 1;
    }

    string text = "";
    for ( int i = 1; i < argCount; i++ )
    {
				text += args[i];
    }

    cout << "Search text \"" << text << "\" for first upper-case letter:" << endl;
    char result = GetFirstUppercase( text );
    cout << "Result: " << result << endl;

    return 0;
}
