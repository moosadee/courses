#ifndef _TESTS
#define _TESTS

#include <string>
using namespace std;

bool IsUppercase( char letter );
char GetFirstUppercase( string text, unsigned int pos = 0 ); // Declaration with default arg
void Test_GetFirstUppercase();

#endif
