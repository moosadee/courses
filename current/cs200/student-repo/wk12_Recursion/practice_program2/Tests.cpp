#include "Tests.h"

#include <iostream>
#include <vector>
#include <string>
using namespace std;

void Test_FindMin()
{
		const string GRN = "\033[0;32m"; const string RED = "\033[0;31m"; const string BOLD = "\033[37;45m"; const string CLR = "\033[0m";
		int totalTests = 0, totalPass = 0, totalFail = 0;

		{ // TEST BEGIN
				totalTests++;
				vector<char> input = { 'k', 'i', 't', 't', 'e', 'n' };
			  char expectOut = 'e';
				char actualOut = FindMin( input, 0 );
				string testName = "FindMin( { 'k', 'i', 't', 't', 'e', 'n' }, 0 ) should be 'e'";

				if ( actualOut == expectOut )
				{
						cout << GRN << "[PASS] " << testName << endl;
						totalPass++;
				}
				else
				{
						cout << RED << "[FAIL] " << testName << endl;
						cout << "* Expected result: " << expectOut << endl;
						cout << "* Actual result:   " << actualOut << endl;
						totalFail++;
				}
				cout << CLR;
		} // TEST END

		{ // TEST BEGIN
				totalTests++;
				vector<char> input = { 'C', 'H', 'I', 'A' };
			  char expectOut = 'A';
				char actualOut = FindMin( input, 0 );
				string testName = "FindMin( { 'C', 'H', 'I', 'A' }, 0 ) should be 'A'";

				if ( actualOut == expectOut )
				{
						cout << GRN << "[PASS] " << testName << endl;
						totalPass++;
				}
				else
				{
						cout << RED << "[FAIL] " << testName << endl;
						cout << "* Expected result: " << expectOut << endl;
						cout << "* Actual result:   " << actualOut << endl;
						totalFail++;
				}
				cout << CLR;
		} // TEST END

		string results = "FINISHED (" + to_string( totalTests ) + " TESTS, " + to_string( totalPass ) + " PASS, " + to_string( totalFail ) + " FAIL)";
		cout << BOLD << results << string( 80-results.size(), '-' )<< CLR << endl;
}
