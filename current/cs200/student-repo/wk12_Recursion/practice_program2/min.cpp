#include <iostream>
#include <vector>
#include <string>
using namespace std;

#include "Tests.h"

char FindMin( vector<char> data, int startIndex )
{
		int min_index = startIndex;
		for ( size_t i = 1; i < data.size(); i++ )
    {
				if ( data[i] < data[min_index] )
        {
						min_index = i;
        }
    }
		return data[min_index];
		// - STUDENT CODE ----------------------------------------------------------
    // TODO: Replace the above iterative implementation with a RECURSIVE implementation.
    //       Remove the code above once done.

		// -------------------------------------------------------------------------
}

// ------------------------- //
// -- DO NOT CHANGE MAIN! -- //
// ------------------------- //
int main( int argCount, char* args[] )
{
		// ARGUMENT CHECK:
		if ( argCount == 2 && string( args[1] ) == "test" ) { Test_FindMin(); return 0; }
		else if ( argCount < 2 )
		{
				cout << "NOT ENOUGH ARGUMENTS!" << endl;
				cout << "Option 1: " << args[0] << " l e t t e r s" << endl;
				cout << "Option 2: " << args[0] << " test" << endl;
				return 1;
		}

		// Program code:
		vector<char> letters;

		cout << "List: ";
		for ( int i = 1; i < argCount; i++ )
		{
				if ( i != 1 ) { cout << ", "; }
				cout << args[i][0];
				letters.push_back( args[i][0] );
		}
		cout << endl;

		char result = FindMin( letters, 0 );
		cout << "Result: " << result << endl;

		return 0;
}
