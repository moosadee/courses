#include "Tests.h"

#include <iostream>
#include <string>
using namespace std;

void Test_Alphabet()
{
		const string GRN = "\033[0;32m"; const string RED = "\033[0;31m"; const string BOLD = "\033[37;45m"; const string CLR = "\033[0m";
		int totalTests = 0, totalPass = 0, totalFail = 0;

		{ // TEST BEGIN
				char input1 = 'a';
				char input2 = 'd';
				string expectOut = "abcd";
				string actualOut = Alphabet( input1, input2 );
				string testName = "Alphabet( 'a', 'd' ) should be \"" + expectOut + "\"";

				if ( actualOut == expectOut )
				{
						cout << GRN << "[PASS] " << testName << endl;
						totalPass++;
				}
				else
				{
						cout << RED << "[FAIL] " << testName << endl;
						cout << "* Expected result: " << expectOut << endl;
						cout << "* Actual result:   " << actualOut << endl;
						totalFail++;
				}
				cout << CLR;
		} // TEST END

		{ // TEST BEGIN
				char input1 = 'M';
				char input2 = 'P';
				string expectOut = "MNOP";
				string actualOut = Alphabet( input1, input2 );
				string testName = "Alphabet( 'M', 'P' ) should be \"" + expectOut + "\"";

				if ( actualOut == expectOut )
				{
						cout << GRN << "[PASS] " << testName << endl;
						totalPass++;
				}
				else
				{
						cout << RED << "[FAIL] " << testName << endl;
						cout << "* Expected result: " << expectOut << endl;
						cout << "* Actual result:   " << actualOut << endl;
						totalFail++;
				}
				cout << CLR;
		} // TEST END

		string results = "FINISHED (" + to_string( totalTests ) + " TESTS, " + to_string( totalPass ) + " PASS, " + to_string( totalFail ) + " FAIL)";
		cout << BOLD << results << string( 80-results.size(), '-' )<< CLR << endl;
}
