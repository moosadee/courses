#include <iostream>
#include <string>
using namespace std;

#include "Tests.h"

string Alphabet( char start, char end )
{
		string str = "";
		for ( start = start; start <= end; start++ )
		{
				str += start;
		}
		return str;
		// - STUDENT CODE ----------------------------------------------------------
    // TODO: Replace the above iterative implementation with a RECURSIVE implementation.
    //       Remove the code above once done.

		// -------------------------------------------------------------------------
}

// ------------------------- //
// -- DO NOT CHANGE MAIN! -- //
// ------------------------- //
int main( int argCount, char* args[] )
{
		// ARGUMENT CHECK:
		if ( argCount == 2 && string( args[1] ) == "test" )
		{
				Test_Alphabet();
				return 0;
		}
		else if ( argCount < 3 )
		{
				cout << "NOT ENOUGH ARGUMENTS!" << endl;
				cout << "Option 1: " << args[0] << " LETTERSTART LETTEREND" << endl;
				cout << "Option 2: " << args[0] << " test" << endl;
				return 1;
		}

		char startLetter = args[1][0];
		char endLetter = args[2][0];

		cout << "Start at '" << startLetter << "' end at '" << endLetter << "'" << endl;
		string result = Alphabet( startLetter, endLetter );
		cout << "Result: " << result << endl;

		return 0;
}
