#include <iostream>
#include <fstream>
using namespace std;

// Plant info is from grownative.org/native-plant-database
int main( int argCount, char* args[] )
{
    cout << endl <<"-- TO DO LIST --" << endl;

    int counter = 1;
    string todo = "";

    ofstream outfile; // Creating an output file
    outfile.open( "todo.txt" );

    bool running = true;
    while ( running )
    {
        cout << "Enter the next item on your TODO list, or QUIT to end: " << endl;
        cout << ">> ";
        // TODO: Use the `getline` function to read input from `cin` into the `todo` variable.

        if ( todo == "QUIT" )
        {
            // Quit program
            running = false;
        }
        else
        {
            // Add it to the to-do list
            // TODO: Output the `counter` and `todo` variables to the `outfile`.
            counter++;
        }
    }

    cout << "List written to todo.txt" << endl;

    outfile.close();

    cout << endl << "-- GOODBYE! --" << endl;
}
