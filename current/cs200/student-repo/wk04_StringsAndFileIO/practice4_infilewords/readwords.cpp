#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

/** read words program
		argument 1: filename
*/
int main( int argCount, char* args[] )
{
		cout << endl << "-- READ WORDS PROGRAM -- " << endl;
		// Check to see if program was ran with enough arguments
		if ( argCount < 2 ) { cout << "Not enough arguments! Expected: " << args[0] << " FILENAME" << endl; return 1; }
		cout << fixed << setprecision( 2 );

		// Converting arguments to variables
		string filename = string( args[1] );

		string word;

		ifstream infile; // Creating an input file

		// TODO: Use the infile's open function to open the `filename`.

		// TODO: If infile's `fail()` function returns `true` then display an error message and return 1.

		// TODO: Read one word from `infile` into `word` using the >> operator.
		cout << "READ: " << word << endl;

		// TODO: Read one word from `infile` into `word` using the >> operator.
		cout << "READ: " << word << endl;

		// TODO: Read one word from `infile` into `word` using the >> operator.
		cout << "READ: " << word << endl;

		cout << endl << "-- GOODBYE! --" << endl;
}
