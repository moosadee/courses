#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

/** read whole file program
		argument 1: filename
*/
int main( int argCount, char* args[] )
{
		cout << endl << "-- READ WHOLE FILE PROGRAM -- " << endl;
		// Check to see if program was ran with enough arguments
		if ( argCount < 2 ) { cout << "Not enough arguments! Expected: " << args[0] << " FILENAME" << endl; return 1; }
		cout << fixed << setprecision( 2 );

		// Converting arguments to variables
		string filename = string( args[1] );

		string line;

		ifstream infile;
		infile.open( filename );

		if ( infile.fail() )
		{
				cout << "ERROR: Could not find \"" << filename << "\"!" << endl;
				return 1;
		}

		int line_count = 0;
		// TODO: Use a while loop, read in from `infile` to `line` using the getline function.
		// Within the while loop, add 1 to line count. Display the line count and the `line` variable.

		cout << endl << "-- GOODBYE! --" << endl;
}
