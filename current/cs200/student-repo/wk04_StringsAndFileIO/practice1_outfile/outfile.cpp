#include <iostream>
#include <fstream>
using namespace std;

// Plant info is from grownative.org/native-plant-database
int main( int argCount, char* args[] )
{
    cout << endl <<"-- NATIVE FLOWERS v1 --" << endl;
    // Check to see if program was ran with enough arguments
    if ( argCount < 2 )
    {
        cout << "Not enough arguments! Expected: " << args[0] << " FLOWERNAME" << endl;
        cout << "Flowers are coneflower, aster, phlox" << endl;
        return 1;
    }

    // Converting arguments to variables
    string flowerName = string( args[1] );

    ofstream outfile; // Creating an output file

    // TODO: Use the outfile's open function to open "flower-info.txt".

    // TODO: Change these from `cout` to `outfile`
    if ( flowerName == "coneflower" )
    {
        cout << "PURPLE CONEFLOWER" << endl;
        cout << "SUN:    Full" << endl;
        cout << "SOIL:   Dry/moderate" << endl;
        cout << "NATURE: Butterfly, pollinators" << endl;
        cout << "HEIGHT: 24 - 36 inches" << endl;
        cout << "SPREAD: 12 - 16 inches" << endl;
    }
    else if ( flowerName == "aster" )
    {
        cout << "AROMATIC ASTER" << endl;
        cout << "SUN:    Full" << endl;
        cout << "SOIL:   Dry/moderate" << endl;
        cout << "NATURE: Butterfly, pollinators" << endl;
        cout << "HEIGHT: 12 - 30 inches" << endl;
        cout << "SPREAD: 16 - 20 inches" << endl;
    }
    else if ( flowerName == "phlox" )
    {
        cout << "DOWNY PHLOX" << endl;
        cout << "SUN:    Full/medium" << endl;
        cout << "SOIL:   Dry" << endl;
        cout << "NATURE: Butterfly, moth" << endl;
        cout << "HEIGHT: 12 - 18 inches" << endl;
        cout << "SPREAD: 12 - 16 inches" << endl;
    }

    cout << "Plant information saved to flower-info.txt" << endl;

    // TODO: Close outfile with its close() function.

    cout << endl << "-- GOODBYE! --" << endl;
}
