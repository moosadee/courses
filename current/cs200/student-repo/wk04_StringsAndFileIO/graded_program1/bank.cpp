#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

/** bank balance program
    argument 1: save data file
*/
int main( int argCount, char* args[] )
{
    cout << endl << "-- ATM PROGRAM -- " << endl;
    // Check to see if program was ran with enough arguments
    if ( argCount < 2 ) { cout << "Not enough arguments! Expected: " << args[0] << " SAVEFILE" << endl; return 1; }
    cout << fixed << setprecision( 2 );

    // Converting arguments to variables
    string filename = string( args[1] );

    string account_name      = "";
    int    account_number    = 0;
    float  account_balance   = 0;
    float  deposit           = 0;
    bool   new_account       = false;

    ifstream infile;
    ofstream outfile;

    // STARTUP: LOAD DATA FILE -------------------------------------------------
    infile.open( filename );

    // TODO: Set the `new_account` boolean to true if `infile.fail()` happens

    if ( new_account )
    {
        cout << "No save file found! Creating new account..." << endl;

        cout << "Enter account type (checking/savings): ";
        // TODO: Get user's input and store in `account_name`.

        cout << "Enter account number: ";
        // TODO: Get user's input and store in `account_number`.

        cout << "Enter starting balance: ";
        // TODO: Get user's input and store in `account_balance`.
    }
    else
    {
        cout << "Loading account..." << endl;

        // TODO: Get info from the file, store in `account_name`.
        // TODO: Get info from the file, store in `account_number`.
        // TODO: Get info from the file, store in `account_balance`.

    }
    infile.close();

    cout << endl << "-- WELCOME TO BAINC BANK --" << endl;
    cout << "YOUR ACCOUNT..." << account_name << endl;
    cout << "ACCOUNT #......" << account_number << endl;
    cout << "BALANCE $......" << account_balance << endl;
    cout << endl;

    cout << "Enter amount of money to deposit to account: $";
    // TODO: Have user enter their deposit amount, store in `deposit` variable.

    account_balance += deposit;

    cout << "You now have a balance of $" << account_balance << endl;
    cout << endl << "Thanks for banking with BAINC! Come again soon!" << endl;

    // CLOSING PROGRAM: SAVE DATA FILE -----------------------------------------
    outfile.open( filename );

    // TODO: Output the `account_name` to the file.
    // TODO: Output the `account_number` to the file.
    // TODO: Output the `account_balance` to the file.



    outfile.close();

    cout << endl << "Account updates saved to \"" << filename << "\"" << endl;

    cout << endl << "-- GOODBYE! --" << endl;
}
