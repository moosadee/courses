#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <string>
#include <map>
using namespace std;

struct TestCase
{
    string command;
    map<string, bool> expectedOutputs;
};

int main()
{
    const string CLEAR    = "\033[0m"; const string RED      = "\033[0;31m"; const string GREEN    = "\033[0;32m"; const string YELLOW   = "\033[0;33m";

    vector<string> sourceFiles = { "apartment.cpp" };
    string command = "";
    vector<TestCase> testCases(3);

    testCases[0].command = "./a.out bedroom 3 5 kitchen 7 11 > result.txt";
    testCases[0].expectedOutputs[ "bedroom" ] = false;
    testCases[0].expectedOutputs[ "3 x 5" ] = false;
    testCases[0].expectedOutputs[ "15 sqft" ] = false;
    testCases[0].expectedOutputs[ "kitchen" ] = false;
    testCases[0].expectedOutputs[ "7 x 11" ] = false;
    testCases[0].expectedOutputs[ "77 sqft" ] = false;
    testCases[0].expectedOutputs[ "ROOM 1 is smaller than ROOM 2" ] = false;

    testCases[1].command = "./a.out kitchen 11 5 patio 7 3 > result.txt";
    testCases[1].expectedOutputs[ "kitchen" ] = false;
    testCases[1].expectedOutputs[ "11 x 5" ] = false;
    testCases[1].expectedOutputs[ "55 sqft" ] = false;
    testCases[1].expectedOutputs[ "patio" ] = false;
    testCases[1].expectedOutputs[ "7 x 3" ] = false;
    testCases[1].expectedOutputs[ "21 sqft" ] = false;
    testCases[1].expectedOutputs[ "ROOM 1 is bigger than ROOM 2" ] = false;

    testCases[2].command = "./a.out bathroom 10 2 laundry 5 4 > result.txt";
    testCases[2].expectedOutputs[ "bathroom" ] = false;
    testCases[2].expectedOutputs[ "10 x 2" ] = false;
    testCases[2].expectedOutputs[ "20 sqft" ] = false;
    testCases[2].expectedOutputs[ "laundry" ] = false;
    testCases[2].expectedOutputs[ "5 x 4" ] = false;
    testCases[2].expectedOutputs[ "20 sqft" ] = false;
    testCases[2].expectedOutputs[ "ROOM 1 and ROOM 2 have the same area" ] = false;

    cout << "- BUILDING PROGRAM -------------------------------" << endl;
    command = "g++ ";
    for ( auto& src : sourceFiles ) { command += src + " "; }
    cout << command << endl;
    system( command.c_str() );

    cout << "- TEST PROGRAM -----------------------------------" << endl;
    // Remove previous output file
    system( "rm result.txt" );

    for ( auto& test : testCases )
    {
	system( test.command.c_str() );
	cout << string( 80, '-' ) << endl << "TEST: " << test.command << endl;

	ifstream input( "result.txt" );
	string outline;
	cout << string( 80, '-' ) << endl << "Program output:" << endl;
	while ( getline( input, outline ) )
	{
	    // Check if this output has any of the required items
	    for ( auto& expected : test.expectedOutputs )
	    {
		if ( outline.find( expected.first ) != string::npos )
		{
		    expected.second = true;
		}
	    }
	    cout << outline << endl;
	}
	cout << endl;

	cout << "TEST CASES..." << endl;
	// Were all results found?
	bool allResultsFound = true;
	for ( auto& expected : test.expectedOutputs )
	{
	    if ( expected.second == true )
	    {
		cout << GREEN << "[PASS] Output found: \"" << expected.first << "\"" << endl;
	    }
	    else
	    {
		allResultsFound = false;
		cout << RED << "[FAIL] Output not found: \"" << expected.first << "\"" << endl;
	    }
	}

	if ( allResultsFound )
	{
	    cout << GREEN << "[PASS] " << test.command << endl;
	}
	cout << CLEAR;
    }

    return 0;
}
