#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <string>
#include <map>
using namespace std;

const string CLEAR = "\033[0m"; const string RED = "\033[0;31m";
const string GREEN = "\033[0;32m"; const string YELLOW = "\033[0;33m";

bool Contains( std::string haystack, std::string needle, bool caseSensitive )
{
    string a = haystack; string b = needle;
    if ( !caseSensitive )
    {
        for ( unsigned int i = 0; i < a.size(); i++ ) { a[i] = tolower( a[i] ); }
        for ( unsigned int i = 0; i < b.size(); i++ ) { b[i] = tolower( b[i] ); }
    }
    return ( a.find( b ) != std::string::npos );
}

struct TestCase
{
    string assignmentName;
    string preEcho;
    string programName;
    string outputName;
    string outputName2;
    string arguments;
    map<string, bool> expectedOutputs;
};

void RunTest( TestCase& tc )
{
    // Run program
    string command = "";
    if ( tc.preEcho != "" )
    {
        command += "echo \"" + tc.preEcho + "\" | ";
    }
    command +="./" + tc.programName + " "
        + tc.arguments + " > " + tc.outputName;
    system( command.c_str() );

    cout << endl << "- PROGRAM: " << tc.assignmentName << endl;

    // Check output
    ifstream input( tc.outputName );
    string line, output="";
    while ( getline( input, line ) )
    {
        output += line + "\t";
    }
    output += "\n\n";
    input.close();
    input.open( tc.outputName2 );
    while ( getline( input, line ) )
    {
        output += line + "\t";
    }
    output += "\n";
    cout << "  - OUTPUT: ";
    if ( output.size() > 80 )
    {
        cout << output.substr( 0, 70 ) << YELLOW << " (... TRUNCATED ...)" << CLEAR;
    }
    else
    {
        cout << output;
    }
    cout << endl;
    for ( auto& expOut : tc.expectedOutputs )
    {
        if ( Contains( output, expOut.first, false ) )
        {
            expOut.second = true;
        }
    }

    // Test results
    for ( auto& expOut : tc.expectedOutputs )
    {
        string result = ( expOut.second )
            ? ( GREEN + "  - [PASS] Found output" )
            : ( RED   + "  - [FAIL] DIDN'T FIND OUTPUT" );
        cout << result << " \"" << expOut.first << "\""
             << " in program " << tc.assignmentName
             << CLEAR << endl;
    }

    // Cleanup
    command = "rm " + tc.outputName;
    system( command.c_str() );
}

std::string ToLower( const std::string& val )
{
    std::string str = "";
    for ( unsigned int i = 0; i < val.size(); i++ )
    {
        str += tolower( val[i] );
    }
    return str;
}

int main()
{
    // Practice 1
    {
        system( "g++ practice1_*/*.cpp -o practice1_outfile/program1.exe" );
        vector<TestCase> testCases(3);
        testCases[0].assignmentName = "practice1";
        testCases[0].programName = "practice1_outfile/program1.exe";
        testCases[0].outputName = "program1-out.txt";
        testCases[0].arguments = "phlox";
        testCases[0].preEcho = "";
        testCases[0].expectedOutputs[ "DOWNY PHLOX" ] = false;
        
        testCases[1].assignmentName = "practice1";
        testCases[1].programName = "practice1_outfile/program1.exe";
        testCases[1].outputName = "program1-out.txt";
        testCases[1].arguments = "coneflower";
        testCases[1].preEcho = "";
        testCases[1].expectedOutputs[ "PURPLE CONEFLOWER" ] = false;
        
        testCases[2].assignmentName = "practice1";
        testCases[2].programName = "practice1_outfile/program1.exe";
        testCases[2].outputName = "program1-out.txt";
        testCases[2].arguments = "aster";
        testCases[2].preEcho = "";
        testCases[2].expectedOutputs[ "AROMATIC ASTER" ] = false;

        for ( auto& test : testCases ) { RunTest( test ); }
    }

    // Practice 2

    {
        system( "g++ practice2_*/*.cpp -o practice2_cinwords/program2.exe" );
        vector<TestCase> testCases(3);
        testCases[0].assignmentName = "practice2";
        testCases[0].programName = "practice2_cinwords/program2.exe";
        testCases[0].outputName = "practice2_cinwords/program2-out.txt";
        testCases[0].outputName2 = "practice2_cinwords/flower-info.txt";
        testCases[0].arguments = "";
        testCases[0].preEcho = "1";
        testCases[0].expectedOutputs[ "BLAZING STAR" ] = false;
        
        testCases[1].assignmentName = "practice2";
        testCases[1].programName = "practice2_cinwords/program2.exe";
        testCases[1].outputName = "practice2_cinwords/program2-out.txt";
        testCases[1].outputName2 = "practice2_cinwords/flower-info.txt";
        testCases[1].arguments = "";
        testCases[1].preEcho = "2";
        testCases[1].expectedOutputs[ "BEE BALM" ] = false;
        
        testCases[2].assignmentName = "practice2";
        testCases[2].programName = "practice2_cinwords/program2.exe";
        testCases[2].outputName = "practice2_cinwords/program2-out.txt";
        testCases[2].outputName2 = "practice2_cinwords/flower-info.txt";
        testCases[2].arguments = "";
        testCases[2].preEcho = "3";
        testCases[2].expectedOutputs[ "BUTTERFLY MILKWEED" ] = false;

        for ( auto& test : testCases ) { RunTest( test ); }
    }

    // Practice 3
    {
        cout << endl << "- PROGRAM: practice3" << endl;
        cout << YELLOW << "Practice 3 has to be manually graded" << CLEAR << endl;
    }

    // Practice 4
    {
        system( "g++ practice4_*/*.cpp -o practice4_infilewords/program4.exe" );
        vector<TestCase> testCases(1);
        testCases[0].assignmentName = "practice4";
        testCases[0].programName = "practice4_infilewords/program4.exe";
        testCases[0].outputName = "practice4_infilewords/program4-out.txt";
        testCases[0].arguments = "practice4_infilewords/alice.txt";
        testCases[0].preEcho = "";
        testCases[0].expectedOutputs[ "Alice" ] = false;
        testCases[0].expectedOutputs[ "was" ] = false;
        testCases[0].expectedOutputs[ "beginning" ] = false;
    
        RunTest( testCases[0] );
    }

    // Practice 5
    {
        system( "g++ practice5_*/*.cpp -o practice5_infilelines/program5.exe" );
        vector<TestCase> testCases(1);
        testCases[0].assignmentName = "practice5";
        testCases[0].programName = "practice5_infilelines/program5.exe";
        testCases[0].outputName = "practice5_infilelines/program5-out.txt";
        testCases[0].arguments = "practice5_infilelines/alice.txt";
        testCases[0].preEcho = "";
        testCases[0].expectedOutputs[ "Alice was beginning" ] = false;
        testCases[0].expectedOutputs[ "bank, and of having" ] = false;
        testCases[0].expectedOutputs[ "the book her sister" ] = false;
    
        RunTest( testCases[0] );
    }

    // Practice 6
    {
        system( "g++ practice6_*/*.cpp -o practice6_readentirefile/program6.exe" );
        vector<TestCase> testCases(1);
        testCases[0].assignmentName = "practice6";
        testCases[0].programName = "practice6_readentirefile/program6.exe";
        testCases[0].outputName = "practice6_readentirefile/program6-out.txt";
        testCases[0].arguments = "practice6_readentirefile/alice.txt";
        testCases[0].preEcho = "";
        testCases[0].expectedOutputs[ "Alice was beginning" ] = false;
        testCases[0].expectedOutputs[ "bank, and of having" ] = false;
        testCases[0].expectedOutputs[ "the book her sister" ] = false;
        testCases[0].expectedOutputs[ "conversations in it" ] = false;
        testCases[0].expectedOutputs[ "without pictures" ] = false;
    
        RunTest( testCases[0] );
    }
    
    // Graded program 1
    {
        system( "g++ graded_program1/bank.cpp -o graded_program1/bank.exe" );
        system( "rm graded_program1/accountTest.txt" );
        
        vector<TestCase> testCases(2);
    
        testCases[0].assignmentName = "graded_program1";
        testCases[0].programName = "graded_program1/bank.exe";
        testCases[0].outputName = "graded_program1/graded-out.txt";
        testCases[0].outputName2 = "graded_program1/accountTest.txt";
        testCases[0].arguments = "graded_program1/accountTest.txt";
        testCases[0].preEcho = "checking 100100 2222 33";
        testCases[0].expectedOutputs[ "checking" ] = false;
        testCases[0].expectedOutputs[ "100100" ] = false;
        testCases[0].expectedOutputs[ "2255" ] = false;

        testCases[1].assignmentName = "graded_program1";
        testCases[1].programName = "graded_program1/bank.exe";
        testCases[1].outputName = "graded_program1/graded-out.txt";
        testCases[1].outputName2 = "graded_program1/accountTest.txt";
        testCases[1].arguments = "graded_program1/accountTest.txt";
        testCases[1].preEcho = "50";
        testCases[1].expectedOutputs[ "checking" ] = false;
        testCases[1].expectedOutputs[ "100100" ] = false;
        testCases[1].expectedOutputs[ "2305" ] = false;
        
        for ( auto& test : testCases ) { RunTest( test ); }
    
        //system( "rm graded.exe" );
    }
  
    // Graded program 2
    {
        cout << endl << "- PROGRAM: graded_program2" << endl;
        cout << YELLOW << "Program has to be manually graded" << CLEAR << endl;
        /*
        system( "g++ graded_program2/*.cpp -o strings.exe" );
        
        vector<TestCase> testCases(1);
    
        testCases[0].assignmentName = "graded_program2";
        testCases[0].programName = "strings.exe";
        testCases[0].outputName = "graded2-out.txt";
        testCases[0].outputName2 = "";
        testCases[0].arguments = "";
        testCases[0].preEcho = "1 0";
        testCases[0].expectedOutputs[ "checking" ] = false;
        testCases[0].expectedOutputs[ "100100" ] = false;
        testCases[0].expectedOutputs[ "2255" ] = false;
        
        for ( auto& test : testCases ) { RunTest( test ); }
    
        //system( "rm graded.exe" );
        */
    }

    return 0;
}
