#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

/** read lines program
		argument 1: filename
*/
int main( int argCount, char* args[] )
{
		cout << endl << "-- READ LINES PROGRAM -- " << endl;
		// Check to see if program was ran with enough arguments
		if ( argCount < 2 ) { cout << "Not enough arguments! Expected: " << args[0] << " FILENAME" << endl; return 1; }
		cout << fixed << setprecision( 2 );

		// Converting arguments to variables
		string filename = string( args[1] );

		string line;

		ifstream infile; // Creating an input file

		// TODO: Use the infile's open function to open the `filename`.

		// TODO: If infile's `fail()` function returns `true` then display an error message and return 1.

		// TODO: Read one LINE from `infile` into `line` using the getline function.
		cout << "READ: " << line << endl;

		// TODO: Read one LINE from `infile` into `line` using the getline function.
		cout << "READ: " << line << endl;

		// TODO: Read one LINE from `infile` into `line` using the getline function.
		cout << "READ: " << line << endl;

		cout << endl << "-- GOODBYE! --" << endl;
}
