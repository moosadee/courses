#include <iostream>
#include <fstream>
using namespace std;

// Plant info is from grownative.org/native-plant-database
int main( int argCount, char* args[] )
{
		cout << endl <<"-- NATIVE FLOWERS v2 --" << endl;

		cout << "SELECT A FLOWER:" << endl;
		cout << "1. Eastern Blazing Star" << endl;
		cout << "2. Bee Balm" << endl;
		cout << "3. Butterfly Milkweed" << endl;

		int choice = 1;

		cout << endl << "CHOICE: ";
		// TODO: Use a `cin` statement to get input from the keyboard, store in the `choice` variable.

		ofstream outfile; // Creating an output file

		// TODO: Use the outfile's open function to open "flower-info.txt".

		// TODO: Change these from `cout` to `outfile`
		if ( choice == 1 )
		{
				cout << "EASTERN BLAZING STAR" << endl;
				cout << "SUN:    Full/medium" << endl;
				cout << "SOIL:   Dry/moderate" << endl;
				cout << "NATURE: Butterfly, hummingbird, pollinators" << endl;
				cout << "HEIGHT: 30 - 48 inches" << endl;
				cout << "SPREAD: 10 - 18 inches" << endl;
		}
		else if ( choice == 2 )
		{
				cout << "BEE BALM" << endl;
				cout << "SUN:    Full/medium" << endl;
				cout << "SOIL:   Dry/moderate" << endl;
				cout << "NATURE: Butterfly, hummingbird" << endl;
				cout << "HEIGHT: 18 - 24 inches" << endl;
				cout << "SPREAD: 16 - 24 inches" << endl;
		}
		else if ( choice == 3 )
		{
				cout << "BUTTERFLY MILKWEED" << endl;
				cout << "SUN:    Full" << endl;
				cout << "SOIL:   Dry/moderate" << endl;
				cout << "NATURE: Butterfly, hummingbird, pollinators" << endl;
				cout << "HEIGHT: 18 - 24 inches" << endl;
				cout << "SPREAD: 18 - 24 inches" << endl;
		}

		cout << "Plant information saved to flower-info.txt" << endl;

		// TODO: Close outfile with its close() function.

		cout << endl << "-- GOODBYE! --" << endl;
}
