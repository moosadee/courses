#include <iostream>
#include <string>
using namespace std;

struct Course
{
    string code;
    Course* ptrPrev;
    Course* ptrNext;
};

int main()
{
    cout << endl << "-- POINTERS TO COURSES -- " << endl;

    Course course1, course2, course3, course4;
    course1.code = "CS 134";
    course2.code = "CS 200";
    course3.code = "CS 235";
    course4.code = "CS 250";

    // TODO: Set each course's `ptrPrev` and `ptrNext`.

    Course* ptrCurrent = &course1; // Point to the first course
    while ( ptrCurrent != nullptr ) // Keep looping until we run out of courses
    {
        cout << "Current course: "
             << ptrCurrent->code << endl;
        // TODO: Set `ptrCurrent` equal to its own `ptrNext`.
    }

    cout << endl << "-- GOODBYE! --" << endl;
    return 0;
}
