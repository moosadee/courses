#ifndef _TESTS
#define _TESTS

#include <vector>
#include <string>
using namespace std;

string VectorToString( const vector<string>& vec );
string VectorToString( const vector<size_t>& vec );
void LoadData( vector<string>& data, string filename );
vector<size_t> LinearSearch_PartialMatch( const vector<string>& data, string findme );
void Test_LinearSearch();

#endif
