#include <iostream>
#include <fstream>
#include <vector>
#include <string>
using namespace std;

#include "Tests.h"

vector<size_t> LinearSearch_PartialMatch( const vector<string>& data, string findme )
{
    vector<size_t> indices;
    // TODO: Implement linear search; store matches in the
    //       `indices` vector instead of returning the index right away.

    return indices; // Return all matching indices
}

// !! YOU DON'T NEED TO CHANGE ANYTHING BELOW !!

int main( int argCount, char* args[] )
{
    // ARGUMENT CHECK:
    if ( argCount == 2 && string( args[1] ) == "test" )
    {
        Test_LinearSearch();
        return 0;
    }
    else if ( argCount < 3 )
    {
        cout << "NOT ENOUGH ARGUMENTS!" << endl;
        cout << "Option 1: " << args[0] << " FILENAME SEARCHTERM" << endl;
        cout << "Option 2: " << args[0] << " test" << endl;
        return 1;
    }

    // Converting arguments to variables
    string filename = string( args[1] );
    string findme   = string( args[2] );

    vector<string> data;
    LoadData( data, filename );

    vector<size_t> found_indices = LinearSearch_PartialMatch( data, findme );

    cout << "Found results: " << found_indices.size() << endl;

    for ( auto& index : found_indices )
    {
        cout << "INDEX: " << index << ", DATA: " << data[index] << endl;
    }
}
