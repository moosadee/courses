#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <string>
#include <map>
using namespace std;

const string CLEAR = "\033[0m"; const string RED = "\033[0;31m";
const string GREEN = "\033[0;32m"; const string YELLOW = "\033[0;33m";

bool Contains( std::string haystack, std::string needle, bool caseSensitive )
{
    string a = haystack; string b = needle;
    if ( !caseSensitive )
    {
        for ( unsigned int i = 0; i < a.size(); i++ ) { a[i] = tolower( a[i] ); }
        for ( unsigned int i = 0; i < b.size(); i++ ) { b[i] = tolower( b[i] ); }
    }
    return ( a.find( b ) != std::string::npos );
}

struct TestCase
{
    string assignmentName;
    string preEcho;
    string programName;
    string outputName;
    string arguments;
    map<string, bool> expectedOutputs;
};

void RunTest( TestCase& tc )
{
    // Run program
    string command = "";
    if ( tc.preEcho != "" )
    {
        command += "echo \"" + tc.preEcho + "\" | ";
    }
    command +="./" + tc.programName + " "
        + tc.arguments + " > " + tc.outputName;
    system( command.c_str() );

    cout << endl << "- PROGRAM: " << tc.assignmentName << endl;

    // Check output
    ifstream input( tc.outputName );
    string line, output="";
    while ( getline( input, line ) )
    {
        output += line + "\t";
    }
    cout << "  - OUTPUT: " << output << endl;
    for ( auto& expOut : tc.expectedOutputs )
    {
        if ( Contains( output, expOut.first, false ) )
        {
            expOut.second = true;
        }
    }

    // Test results
    for ( auto& expOut : tc.expectedOutputs )
    {
        string result = ( expOut.second )
            ? ( GREEN + "  - [PASS] Found output" )
            : ( RED   + "  - [FAIL] DIDN'T FIND OUTPUT" );
        cout << result << " \"" << expOut.first << "\""
             << " in program " << tc.assignmentName
             << CLEAR << endl;
    }

    // Cleanup
    command = "rm " + tc.outputName;
    system( command.c_str() );
}

int main()
{
    // Practice 1
    {
        system( "g++ practice1_*/program1.cpp -o program1.exe" );

        vector<TestCase> testCases(1);
        testCases[0].assignmentName = "practice1";
        testCases[0].programName = "program1.exe";
        testCases[0].outputName = "program1-out.txt";
        testCases[0].arguments = "";
        testCases[0].preEcho = "";
        testCases[0].expectedOutputs[ "1. Add item" ] = false;
        testCases[0].expectedOutputs[ "2. Checkout" ] = false;

        RunTest( testCases[0] );

        system( "rm program1.exe" );
    }

    // Practice 2
    {
        system( "g++ practice2_*/program2.cpp -o program2.exe" );

        vector<TestCase> testCases(2);
        testCases[0].assignmentName = "practice2";
        testCases[0].programName = "program2.exe";
        testCases[0].outputName = "program2-out.txt";
        testCases[0].arguments = "";
        testCases[0].preEcho = "123.45";
        testCases[0].expectedOutputs[ "$123.45" ] = false;

        testCases[1].assignmentName = "practice2";
        testCases[1].programName = "program2.exe";
        testCases[1].outputName = "program2-out.txt";
        testCases[1].arguments = "";
        testCases[1].preEcho = "10.20";
        testCases[1].expectedOutputs[ "$10.20" ] = false;

        for ( auto& testCase : testCases ) { RunTest( testCase ); }

        system( "rm program2.exe" );
    }

    // Practice 3
    {
        system( "g++ practice3_*/program3.cpp -o program3.exe" );

        vector<TestCase> testCases(1);
        testCases[0].assignmentName = "practice3";
        testCases[0].programName = "program3.exe";
        testCases[0].outputName = "program3-out.txt";
        testCases[0].arguments = "";
        testCases[0].preEcho = "";
        testCases[0].expectedOutputs[ "Tax rate is:" ] = false;
        testCases[0].expectedOutputs[ "9.61" ] = false;

        RunTest( testCases[0] );

        system( "rm program3.exe" );
    }

    // Practice 4
    {
        system( "g++ practice4_*/program4.cpp -o program4.exe" );

        vector<TestCase> testCases(2);
        testCases[0].assignmentName = "practice4";
        testCases[0].programName = "program4.exe";
        testCases[0].outputName = "program4-out.txt";
        testCases[0].arguments = "";
        testCases[0].preEcho = "100 9";
        testCases[0].expectedOutputs[ "$109" ] = false;

        testCases[1].assignmentName = "practice4";
        testCases[1].programName = "program4.exe";
        testCases[1].outputName = "program4-out.txt";
        testCases[1].arguments = "";
        testCases[1].preEcho = "250 10";
        testCases[1].expectedOutputs[ "$275" ] = false;

        for ( auto& testCase : testCases ) { RunTest( testCase ); }

        system( "rm program4.exe" );
    }

    // Practice 5
    {
        system( "g++ practice5_*/*.h practice5_*/*.cpp -o program5.exe" );
        cout << "- PROGRAM: practice5" << endl;
        system( "./program5.exe test" );
        system( "rm program5.exe" );
    }

    // Practice 6
    {
        system( "g++ practice6_*/*.h practice6_*/*.cpp -o program6.exe" );
        cout << "- PROGRAM: practice6" << endl;
        system( "./program6.exe test" );
        system( "rm program6.exe" );
    }

    // Graded program
    {
        system( "g++ graded_program/*.h graded_program/*.cpp -o graded.exe" );
        cout << "- PROGRAM: graded program" << endl;
        system( "./graded.exe test" );
        system( "rm graded.exe" );
    }

    return 0;
}
