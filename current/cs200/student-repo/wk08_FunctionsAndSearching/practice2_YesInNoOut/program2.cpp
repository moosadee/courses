#include <iostream>
#include <iomanip>
using namespace std;

/**
   DisplayFormattedUSD function
   YES input parameters / NO output return
   @param price    The price to display to the screen
   Set up cout to have fixed and setprecision(2) formatting.
   Then, use cout to display the `price` parameter.
*/
// TODO: Implement DisplayFormattedUSD function

int main()
{
    float user_money;
    cout << "Enter an amount of money: ";
    cin >> user_money;

    // TODO: Call DisplayFormattedUSD

    return 0;
}
