#ifndef _TESTS
#define _TESTS

#include <vector>
#include <string>
using namespace std;

string VectorToString( const vector<string>& vec );
string VectorToString( const vector<size_t>& vec );
void LoadData( vector<string>& data, string filename );
size_t BinarySearch_GetFirstFoundIndex( const vector<string>& data, string findme );
void Test_BinarySearch();

#endif
