#include <iostream>
#include <fstream>
#include <limits>
#include <vector>
#include <string>
using namespace std;

#include "Tests.h"

size_t BinarySearch_GetFirstFoundIndex(
    const vector<string>& data, string findme )
{
    // TODO: Implement binary search

    // Not found
    return numeric_limits<size_t>::max(); // return an invalid index
}

// !! YOU DON'T NEED TO CHANGE ANYTHING BELOW !!

int main( int argCount, char* args[] )
{
    // ARGUMENT CHECK:
    if ( argCount == 2 && string( args[1] ) == "test" )
    {
        Test_BinarySearch();
        return 0;
    }
    else if ( argCount < 3 )
    {
        cout << "NOT ENOUGH ARGUMENTS!" << endl;
        cout << "Option 1: " << args[0] << " FILENAME SEARCHTERM" << endl;
        cout << "Option 2: " << args[0] << " test" << endl;
        return 1;
    }

    // Converting arguments to variables
    string filename = string( args[1] );
    string findme   = string( args[2] );

    vector<string> data;
    LoadData( data, filename );

    size_t first_found_index = BinarySearch_GetFirstFoundIndex( data, findme );

    if ( first_found_index == numeric_limits<size_t>::max() )
    {
        cout << "Did not find a match!" << endl;
    }
    else
    {
        cout << "Found index: " << first_found_index
             << ", DATA: " << data[first_found_index] << endl;
    }
}
