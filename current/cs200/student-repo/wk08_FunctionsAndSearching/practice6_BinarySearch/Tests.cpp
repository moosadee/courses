#include "Tests.h"

#include <iostream>
#include <fstream>
#include <limits>
#include <vector>
#include <string>
using namespace std;

string VectorToString( const vector<string>& vec )
{
    string str = "{ ";
    for ( size_t i = 0; i < vec.size(); i++ )
    {
        if ( i != 0 ) { str += ", "; }
        str += to_string( i ) + " = ";
        str += "\"" + vec[i] + "\"";
    }
    str += " }";
    return str;
}

string VectorToString( const vector<size_t>& vec )
{
    string str = "{ ";
    for ( size_t i = 0; i < vec.size(); i++ )
    {
        if ( i != 0 ) { str += ", "; }
        str += to_string( i ) + " = ";
        str += to_string( vec[i] );
    }
    str += " }";
    return str;
}

void Test_BinarySearch()
{
    const string GRN = "\033[0;32m";
    const string RED = "\033[0;31m";
    const string BOLD = "\033[37;45m";
    const string CLR = "\033[0m";
    int totalTests = 0, totalPass = 0, totalFail = 0;

    { // TEST BEGIN
        totalTests++;
        vector<string> input_data
            = { "aardvark", "bat", "cat", "dog", "elephant" };
        string input_findme = "dog";
        size_t expect = 3;
        size_t actual = BinarySearch_GetFirstFoundIndex(
            input_data, input_findme );
        string testName = "BinarySearch_GetFirstFoundIndex( "
            + VectorToString( input_data ) + ", \""
            + input_findme + "\" ) should result in " + to_string( expect );

        if ( actual != expect )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* Expected result: " << expect << endl;
            cout << "* Actual result:   " << actual << endl;
            totalFail++;
        }
        else
        {
            cout << GRN << "[PASS] " << testName << endl;
            totalPass++;
        }

        cout << CLR;
    } // TEST END

    { // TEST BEGIN
        totalTests++;
        vector<string> input_data
            = { "aardvark", "bat", "cat", "dog", "elephant" };
        string input_findme = "fox";
        size_t expect = numeric_limits<size_t>::max();
        size_t actual = BinarySearch_GetFirstFoundIndex(
            input_data, input_findme );
        string testName = "BinarySearch_GetFirstFoundIndex( "
            + VectorToString( input_data ) + ", \""
            + input_findme + "\" ) should result in " + to_string( expect );

        if ( actual != expect )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* Expected result: " << expect << endl;
            cout << "* Actual result:   " << actual << endl;
            totalFail++;
        }
        else
        {
            cout << GRN << "[PASS] " << testName << endl;
            totalPass++;
        }

        cout << CLR;
    } // TEST END

    string results = "FINISHED (" + to_string( totalTests )
        + " TESTS, " + to_string( totalPass )
        + " PASS, " + to_string( totalFail ) + " FAIL)";
    cout << BOLD << results << string( 80-results.size(), '-' )<< CLR << endl;
}

void LoadData( vector<string>& data, string filename )
{
    ifstream infile( filename );
    string line;
    if ( infile.fail() )
    {
        cout << "ERROR: Could not find file \"" << filename << "\"" << endl;
        return;
    }

    while ( getline( infile, line ) )
    {
        data.push_back( line );
    }

    cout << data.size() << " data items loaded" << endl << endl;
}
