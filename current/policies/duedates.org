* Assignment due dates
- Assignments are given a *due date* and an *available until* date, which can be viewed on Canvas.
- The *due date* is when you should aim to get the assignment in by.
- The *available until* date is the last available date/time you can submit the assignment before it closes.
- After the *due date* has passed, a -0.5% per day penalty is applied to the maximum possible score for the assignment.
