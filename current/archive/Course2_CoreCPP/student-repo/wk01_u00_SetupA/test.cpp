#include <iostream> // Library to write text to the screen (console)
#include <string>   // Library to use string data types
using namespace std; // Using the C++ STanDard library

int main() // Program starting function
{ // Code block begin

  // cout : Console Out, allows us to display text and variables to the screen (console)
  // <<   : The "output stream operator", points towards the console out
  // endl : "END-Line", ends the line so the next cout starts on a new line.
  // Text strings must go in double quotes.

  cout << "Hi, my name is... ";
  
  
  cout << endl;

  cout << "This semester, I'm taking... ";
  

  cout << endl;

  cout << "Goodbye!" << endl;

  return 0; // End of program, 0 errors.
} // Code block end
