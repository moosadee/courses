# -*- mode: org -*-
#+OPTIONS: html-postamble:nil

#+LATEX_HEADER: \hypersetup{colorlinks=true,linkcolor=blue}

* Course information

| College                   | Johnson County Community College                               |
| Division                  | CSIT (Computer Science/Information Technology)                 |
| Instructor                | R.W. Singh (they/them)                                         |
| Semester                  | Spring 2025 (1/21/2025 - 5/19/2025)                            |
| Course                    | CS 235: Object Oriented Programming using C++ (4 credit hours) |
| Online Section            | Section 300 / CRN 11922 / Online                               |
| HyFlex Section            | Section 400 / CRN 11923 / HyFlex                               |
| Schedule (HyFlex Section) | Tuesdays/Thursdays, 11:00 am - 12:15 pm                        |
| Room (HyFlex Section)     | Regnier center, room 353                                       |
| Office hours              | Mondays, 3:30 - 5:30 pm; Tuesdays 9:30 - 10:30 am;             |
|                           | Tuesdays 4:30 - 5:30 pm; Thursdays 9:30 - 10:30am              |

- Course description :: This course continues developing problem solving techniques by focusing on object-oriented styles using C++ abstract data types. Basic data structures such as queues, stacks, trees, dictionaries, their associated operations, and their array and pointer implementations will be studied. Topics also include recursion, templates, fundamental algorithm analysis, searching, sorting, hashing, object-oriented concepts and large program organization. Students will write programs using the concepts covered in the lecture. 3 hrs. lecture, 2 hrs. lab by arrangement/wk.
  Catalog link:  [[https://catalog.jccc.edu/coursedescriptions/cs/#CS_250]]
- Prerequisites :: CS 235 or (CS 200 and CS 210 or CS 236 or CS 255 or CIS 240 or MATH 242).
- Drop deadlines :: To view the deadline dates for dropping this course, please refer to the schedule on the JCCC website under Admissions>Enrollment Dates> Dropping Credit Classes. After the 100% refund date, you will be financially responsible for the tuition charges; for details, search on Student Financial Responsibility on the JCCC web page. Changing your schedule may reduce eligibility for financial aid and other third party funding. Courses not dropped will be graded. For questions about dropping courses, contact the Student Success Center at 913-469-3803.
  - Academic Calendar: [[https://www.jccc.edu/modules/calendar/academic-calendar.html]]
  - *First week attendance and Auto-withdraws:* Attendance for the first week of classes at JCCC are recorded and students marked as NOT IN ATTENDANCE get auto-withdrawn from the course. Please pay attention to course announcements / emails from the instructor for instructions on how to make sure you are marked as IN ATTENDANCE.
  - To learn more about reinstatement, please visit: https://www.jccc.edu/admissions/enrollment/reinstatement.html
  - *Faculty-Initiated Withdrawal:* The instructor may opt to withdraw you from the class as a result of extended lack of contact and coursework being done; see Attendance policies section for more.

** Instructor information

- Name: R.W. Singh (aka "Moose")
- Pronouns: they/them
- Office: RC 348 H
- Email: rsingh13@jccc.edu (Canvas Inbox messages preferred)
- Office phone: (913) 799-3671

*** Class communication

- *Please prefer Canvas Inbox* - My direct @ jccc work email is full of other work related emails, I will see your email /fastest/ if you email me via Canvas.
- *Reply speed* - I will attempt to reply within 1 business day of receiving your message.
- *Course announcements* - I will periodically post Announcements on Canvas, which may have assignment fixes, course news, etc. Please make sure to keep an eye on it. 

** Course delivery

*** Online section

Section 300 is setup as an *Online* course. This means the following:
- There is no scheduled class time each week; everything is online asynchronous.
- Attendance is counted by assignments completed for the assigned week.
- Video lectures and archives of the /other/ section's class will be provided via Yuja.
- Exams are also given online, through Canvas.

To see more about JCCC course delivery options, please visit: https://www.jccc.edu/student-resources/course-delivery-methods.html

*** HyFlex section

Section 400 is set up as a *HyFlex* course. This means the following:
- Courses have a scheduled "in-class" time each week.
- Students can choose to attend class in one of three ways:
  1. In person in the classroom during class time.
  2. Remotely via Zoom during class time.
  3. Watch the archived Zoom lecture /after/ class time. *If you do not attend the class section, I expect that you will watch the archived class video afterward so that you stay up-to-date on course news.*
- A Zoom link will be available for each class session. Please see the Canvas page, under "Zoom", for the link.
- Class sessions will be recorded and you can view them afterwards. They will be posted to the Canvas main page once available.

To see more about JCCC course delivery options, please visit: https://www.jccc.edu/student-resources/course-delivery-methods.html

  # ----------------------------------------------------------

** Student drop-in times (office hours)

Office hours for *Spring 2025* are:

- Mondays, 3:30 - 5:30 pm
- Tuesdays, 9:30 - 10:30 am
- Tuesdays, 4:30 - 5:30 pm
- Thursdays, 9:30 - 10:30 am

I will be available on campus or via Zoom. Zoom link will be posted on Canvas under "office hours". Office hours are time for you to drop in whenever and ask questions as needed.

# ----------------------------------------------------------

** Course supplies

1) *Textbook:* Rachel's CS 200 course notes (https://moosadee.gitlab.io/courses/)
   - I will link to each reading item on Canvas.
2) *Zoom:* Needed for remote attendance / office hours
3) *Tools:* See first week assignments for setup instructions.
   - WINDOWS:  g++ (MinGW), make, git, gdb
   - LINUX: g++, make, git, gdb
   - MAC: brew, g++, make, git, gdb
   - VS Code ([[https://code.visualstudio.com/]]) or VS Codmium ([[https://vscodium.com/]]).
4) *Accounts:* See first week assignments for setup instructions.
   - GitLab (https://gitlab.com/) for code storage
5) *Optional:* Things that might be handy
   - Dark Reader plugin (Firefox/Chrome/Safari/Edge) to turn light-mode webpages into dark-mode. (https://darkreader.org/)

** Recommended experience

*Computer skills* - You should have a base level knowledge of using a computer, including:
- Navigating your Operating System, including:
  - Installing software
  - Running software
  - Locating saved files on your computer
  - Writing text documents, exporting to PDF
  - Taking screenshots
  - Editing .txt and other plaintext files
- Navigating the internet:
  - Navigating websites, using links
  - Sending emails
  - Uploading attachments

*Learning skills* - Learning to program takes a lot of reading, and you will be building up your problem solving skills. You should be able to exercise the following skills:
- Breaking down problems - Looking at a problem in small pieces and tackling them one part at a time.
- Organizing your notes so you can use them for reference while coding.
- Reading an entire part of an assignment before starting - these aren't step-by-step to-do lists.
- Learning how to ask a question - Where are you stuck, what are you stuck on, what have you tried?
- Recognizing when additional learning resources are needed and seeking them out - such as utilizing JCCC's Academic Achievement Center tutors.
- Managing your time to give yourself enough time to tackle challenges, rather than waiting until the last minute.

*How to ask questions* - When asking questions about a programming assignment via email, please include the following information so I can answer your question:
1. Be sure to let me know WHICH ASSIGNMENT IT IS, the specific assignment name, so I can find it.
2. Include a SCREENSHOT of what's going wrong.
3. What have you tried so far?


------

* Course policies

** Grading breakdown

Assessment types are given a certain weight in the overall class.
Breakdown percentages are based off the course catalog requirements
([[https://catalog.jccc.edu/coursedescriptions/cs/#CS_250]])

*Final letter grade:* JCCC uses whole letter grades for final course grades: F, D, C, B, and A. The way I break down what your receive at the end of the semester is as follows:

| Total score            | Letter grade |
|------------------------+--------------|
| 89.5% <= grade <= 100% | A            |
| 79.5% <= grade < 89.5% | B            |
| 69.5% <= grade < 79.5% | C            |
| 59.5% <= grade < 69.5% | D            |
| 0% <= grade < 59.5%    | F            |

*Grading style:* All assignments begin at 0% at the start of the semester. As you work through course content, your grade will grow and will not go down. Toward the end of the semester the score you have reflects your final grade in the course, so you will be working towards the grade you want.

*Assignment types:*

- *Exams* (40% of grade)
- *Project* (20% of grade) - A programming project that you will work on throughout the semester.
- *Labs* (20% of grade) - Weekly programming labs to practice new topics.
- *Concept intros* (10% of grade) - Weekly reading and review quiz material.
- *Exercises* ()
  - *Tech Literacy* (5% of grade) - Discussion boards to expand learning of tech topics.
  - *Status updates* (5% of grade) - Weekly updates on how you're doing with the course topics.

** Due dates, late assignments, re-submissions

- *Due dates* are set as a guide for when you /should/ have your assignments in by.
  - I do not count off points for "late" assignments. Just get the assignment in by the "available until" date.
- *End dates/available until dates* are a hard-stop for when an assignment can be turned in. Assignments cannot be turned in after this date.
- *Resubmissions* to some assignments are permitted:
  - *Concept Introductions* are auto-graded and you can resubmit them as much as you'd like, with the highest score being saved.
  - *Labs* are manually graded but you can turn in fixes after receiving feedback from the instructor.


** Attendance

*First week of class:*
JCCC requires us to take attendance during the first week of the semester. Students are required to attend class (if there is a scheduled class session) this first week. If there are scheduling conflicts during the first week of class, please reach out to the instructor to let them know.
JCCC auto-drops students marked as not in attendance during the first week of class, but students can be reinstated.
See https://www.jccc.edu/admissions/enrollment/reinstatement.html for more details.


*HyFlex classes:* The following three scenarios count as student attendance for my classes:
1. Attending class in person during the class times, or
2. Attending class remotely via Zoom during class times, or
3. Watching the recorded Zoom class afterwards. *If you do not attend the class session I will expect you to watch the archived class video so that you keep up-to-date on class news and topics.*

*Online classes:* Attendance is counted as completion of assignments for a given week.


*Faculty-Initiated Withdrawal:* Following the Administrative Drop for Non-Attendance period of each semester (see Section I.A above), a faculty member may choose to withdraw a student whose absences have exceeded the attendance guidelines stated in the course syllabus. There is no reimbursement or forgiveness of tuition and fees for a Faculty-Initiated Withdrawal. Students should not assume that a faculty member will initiate this optional process, and it remains the ultimate responsibility of the student to withdraw and accept all financial and academic consequences as a result of the withdrawal.

Faculty initiated withdrawal may be taken after the faculty member has notified the student through the Excessive Absence Alert procedure that excessive absence has potentially placed the student in academic jeopardy. The withdrawal will be recorded in the student’s record in accordance with the published drop deadlines and the Grading System Policy. The student may also be withdrawn from other scheduled courses if the withdrawn course is a required course. The last date each semester for a faculty-initiated withdrawal shall be the same last date allowed for a student-initiated withdrawal.

The Excessive Absence Alert shall consist of a written notice from the faculty member to the student advising the student that the student’s excessive absence has placed the student in academic jeopardy. The notice shall further state that the student may be withdrawn from the class as per course syllabus guidelines if satisfactory arrangements for the student’s regular class attendance cannot be made with the faculty member. Such written notice shall be provided to the student via email to the student’s College-provided email account and shall constitute adequate notice to the student. Students are responsible for monitoring their College-provided email accounts.

See JCCC's Student Attendance Operating Procedure 314.01:
[[https://www.jccc.edu/about/leadership-governance/policies/students/academic/procedure-attendance.html]]

- I may initiate student withdrawal if student fails to submit a month work of course assignments.
- You should notify me of extended absenses (not able to do coursework) before your absense. Upon returning, you should make an effort to work on the late work weekly and work to catch up on course content.


#+BEGIN_LATEX
\newpage
#+END_LATEX

** Tentative schedule

This schedule is *recommended*, but the modules are available whenever you meet the prerequisites for any of them.

#+ATTR_LATEX: :mode table :align c | p{1.2cm} | p{5cm}
| Week # | Monday | Recommended topics                             |
|--------+--------+------------------------------------------------|
|      1 | Jan 20 | Welcome, setup, CS 200 review                  |
|--------+--------+------------------------------------------------|
|      2 | Jan 27 | Testing, debugging, templates, friends         |
|--------+--------+------------------------------------------------|
|      3 | Feb 3  | Software development, Project v1               |
|--------+--------+------------------------------------------------|
|      4 | Feb 10 | Standard Template Library, Exceptions          |
|--------+--------+------------------------------------------------|
|      5 | Feb 17 | Intermediate class features, pointers          |
|--------+--------+------------------------------------------------|
|      6 | Feb 24 | Polymorphism, Static members                   |
|--------+--------+------------------------------------------------|
|      7 | Mar 3  | Smart pointers, Project v2                     |
|--------+--------+------------------------------------------------|
|      8 | Mar 10 | Algorithm efficiency                           |
|--------+--------+------------------------------------------------|
|        |        | Mar 17 - Mar 23 - Spring Break                 |
|--------+--------+------------------------------------------------|
|      9 | Mar 24 | Test-driven Development, Project v3            |
|--------+--------+------------------------------------------------|
|     10 | Mar 31 | Recursion                                      |
|--------+--------+------------------------------------------------|
|     11 | Apr 7  | Design patterns, Project v4                    |
|--------+--------+------------------------------------------------|
|     12 | Apr 14 | Overloading functions, constructors, operators |
|--------+--------+------------------------------------------------|
|     13 | Apr 21 | Anonymous functions                            |
|--------+--------+------------------------------------------------|
|     14 | Apr 28 | Linking libraries                              |
|--------+--------+------------------------------------------------|
|     15 | May 5  | Catch-up / project day                         |
|--------+--------+------------------------------------------------|
|     16 | May 12 | Finals week (See JCCC finals schedule)         |

#+BEGIN_LATEX
\newpage
#+END_LATEX

** Class format

Class sessions are flexible and can be changed to suit student requests.
By default, class sessions are usually used for:
- Working through example code
- An overview of the assignments for the week
- In-class working time

Generally, I do not lecture during class times; there are video lectures
and reading assignments that students can complete independently.
Class times for this course are better used for students to get hands-on
experience with the new topics while having the instructor available
to answer questions and make clarifications.

*Grade scoring:*

# [[file:images/score.png]]


- All assignment scores begin at *0%* at the start of the semester, so your grade begins at 0% at the start.
- As you progress through the course and finish more course content, your grade will continue rising. Ideally, if you get 100% on all assignments in the course, you'll end with 100% as your final grade.
- Most assignments can be resubmitted after grading to improve your score afterwards. (e.g., if I notice bugs in your lab, I'll make comments on why it happens, and you can go back and fix it.)
- Assignments don't close until the *last day of class - July 25th*. See the Academic Calendar (https://www.jccc.edu/modules/calendar/academic-calendar.html) if needed.



** Academic honesty

The assignments the instructor writes for this course are meant to help the student learn new topics,
starting easy and increasing the challenge over time. If a student does not do their own work then they
miss out on the lessons and strategy learned from going from step A to step B to step C. The instructor
is always willing to help you work through assignments, so ideally the student shouldn't feel the need
to turn to third party sources for help.

*Generally, for R.W. Singh's courses:*

- OK things:
  - Asking the instructor for help, hints, or clarification, on any assignment.
  - Posting to the discussion board with questions (except with tests - please email me for those). (If you're unsure if you can post a question to the discussion board, you can go ahead and post it. If there's a problem I'll remove/edit the message and just let you know.)
  - Searching online for general knowledge questions (e.g. "C++ if statements", error messages).
  - Working with a tutor through the assignments, as long as they're not doing the work for you.
  - Use your IDE (replit, visual studio, code::blocks) to test out things before answering questions.
  - Brainstorming with classmates, sharing general information ("This is how I do input validation").

- Not OK Things:
  - Sharing your code files with other students, or asking other students for their code files.
  - Asking a tutor, peer, family member, friend, AI, etc. to do your assignments for you.
  - Searching for specific solutions to assignments online/elseware.
  - Basically, any work/research you aren't doing on your own, that means you're not learning the topics.
  - Don't give your code files to other students, even if it is "to verify my work!"
  - Don't copy solutions off other parts of the internet; assignments get modified a little bit each semester.



If you have any further questions, please contact the instructor.

Each instructor is different, so make sure you don't assume that what is OK with one instructor is OK with another.


** Student success tips

- *I need to achieve a certain grade for my financial aid or student visa. What do I need to plan on?*
  - If you need to get a certain grade, such as an A for this course, to maintain your financial aid or student visa, then you need to set your mindset for this course immediately. You should prioritize working on assignments early and getting them in ahead of time so that you have the maximum amount of time to ask questions and get help. You should not be panicking at the end of the semester because you have a grade less than what you need. From week 1, make sure you're committed to staying on top of things. 
- *How do I contact the instructor?*
  - The best way to contact the instructor is via Canvas' email system. You can also email the instructor at rsingh13@jccc.edu, however, emails are more likely to be lost in the main inbox, since that's where all the instructor's work-related email goes. You can also attend Zoom office hours to ask questions. 
- *What are some suggestions for approaching studying and assignments for this course?*
  - Each week is generally designed with this "path" in mind:
    - Watch lecture videos, read assigned reading.
    - Work on Concept Introduction assignment(s).
    - Work on Exercise assignment.
  - Those are the core topics for the class. The Tech Literacy assignments can be done a bit more casually, and the Topic Mastery (exams) don't have to be done right away - do the exams once you feel comfortable with the topic. 
- *Where do I find feedback on my work?*
  - Canvas should send you an email when there is feedback on your work, but you can also locate assignment feedback by going to your Grades view on Canvas, locating the assignment, and clicking on the speech balloon icon to open up comments. These will be important to look over during the semester, especially if you want to resubmit an assignment for a better grade. 
- *How do I find a tutor?*
  - JCCC's Academic Achievement Center

    (https://www.jccc.edu/student-resources/academic-resource-center/academic-achievement-center/)

    provides tutoring services for our area. Make sure to look for the expert tutor service and you can learn more about getting a tutor. 
- *How do I keep track of assignments and due dates so I don't forget something?*
  - Canvas has a CALENDAR view, but it might also be useful to utilize something like Google Calendar, which can text and email you reminders, or even keeping a paper day planner that you check every day. 



** Accommodations and life help


- *How do I get accommodations? - Access Services*

   https://www.jccc.edu/student-resources/access-services/

   Access Services provides students with disabilities equal opportunity and access. Some of the accommodations and services include testing accommodations, note-taking assistance, sign language interpreting services, audiobooks/alternative text and assistive technology.


- *What if I'm having trouble making ends meet in my personal life? - Student Basic Needs Center*

  https://www.jccc.edu/student-resources/basic-needs-center/

  Check website for schedule and location. The JCCC Student Assistance Fund is to help students facing a sudden and unforeseen emergency that has affected their ability to attend class or otherwise meet the academic obligations of a JCCC student. If you are experiencing food or housing insecurity, or other hardships, stop by COM 319 and visit with our helpful staff.


- *Is there someone I can talk to for my degree plan? - Academic Advising*

  https://www.jccc.edu/student-resources/academic-counseling/

  JCCC has advisors to help you with:
  - Choose or change your major and stay on track for graduation.
  - Ensure a smooth transfer process to a 4-year institution.
  - Discover resources and tools available to help build your schedule, complete enrollment and receive help with coursework each semester.
  - Learn how to get involved in Student Senate, clubs and orgs, athletics, study abroad, service learning, honors and other leadership programs.
  - If there’s a hold on your account due to test scores, academic probation or suspension, you are required to meet with a counselor.

- *Is there someone I can talk to for emotional support? - Personal Counseling*

  https://www.jccc.edu/student-resources/personal-counseling/

  JCCC counselors provide a safe and confidential environment to talk about personal concerns. We advocate for students and assist with personal issues and make referrals to appropriate agencies when needed.


- *How do I get a tutor? - The Academic Achievement Center*

  https://www.jccc.edu/student-resources/academic-resource-center/academic-achievement-center/

  The AAC is open for Zoom meetings and appointments. See the website for their schedule. Meet with a Learning Specialist for help with classes and study skills, a Reading Specialist to improve understanding of your academic reading, or a tutor to help you with specific courses and college study skills. You can sign up for workshops to get off to a Smart Start in your semester or analyze your exam scores!


- *How can I report ethical concerns? - Ethics Report Line*

  https://www.jccc.edu/about/leadership-governance/administration/audit-advisory/ethics-line/

  You can report instances of discrimination and other ethical issues to JCCC via the EthicsPoint line.


- *What other student resources are there? - Student Resources Directory*

  https://www.jccc.edu/student-resources/


-----

* Additional information

** ADA compliance / disabilities
JCCC provides a range of services to allow persons with disabilities to participate in educational programs and activities. If you are a student with a disability and if you are in need of accommodations or services, it is your responsibility to contact Access Services and make a formal request. To schedule an appointment with an Access Advisor or for additional information, you can contact Access Services at (913) 469-3521 or accessservices@jccc.edu. Access Services is located on the 2nd floor of the Student Center (SC202)

** Attendance standards of JCCC
Educational research demonstrates that students who regularly attend and participate in all scheduled classes are more likely to succeed in college. Punctual and regular attendance at all scheduled classes, for the duration of the course, is regarded as integral to all courses and is expected of all students. Each JCCC faculty member will include attendance guidelines in the course syllabus that are applicable to that course, and students are responsible for knowing and adhering to those guidelines. Students are expected to regularly attend classes in accordance with the attendance standards implemented by JCCC faculty.

The student is responsible for all course content and assignments missed due to absence. Excessive absences and authorized absences are handled in accordance with the Student Attendance Operating Procedure.

** Academic Dishonesty
No student shall attempt, engage in, or aid and abet behavior that, in the judgment of the faculty member for a particular class, is construed as academic dishonesty. This includes, but is not limited to, cheating, plagiarism or other forms of academic dishonesty.

Examples of academic dishonesty and cheating include, but are not limited to, unauthorized acquisition of tests or other academic materials and/or distribution of these materials, unauthorized sharing of answers during an exam, use of unauthorized notes or study materials during an exam, altering an exam and resubmitting it for re-grading, having another student take an exam for you or submit assignments in your name, participating in unauthorized collaboration on coursework to be graded, providing false data for a research paper, using electronic equipment to transmit information to a third party to seek answers, or creating/citing false or fictitious references for a term paper. Submitting the same paper for multiple classes may also be considered cheating if not authorized by the faculty member.

Examples of plagiarism include, but are not limited to, any attempt to take credit for work that is not your own, such as using direct quotes from an author without using quotation marks or indentation in the paper, paraphrasing work that is not your own without giving credit to the original source of the idea, or failing to properly cite all sources in the body of your work. This includes use of complete or partial papers from internet paper mills or other sources of non-original work without attribution.

A faculty member may further define academic dishonesty, cheating or plagiarism in the course syllabus.

** College Wellness and Safety
College Wellness and Safety (https://www.jccc.edu/media-resources/wellness-safety/)

# https://www.jccc.edu/media-resources/covid-19/

- Stay home when you're sick
- Wash hands frequently
- Cover your mouth when coughing or sneezing
- Clean surfaces
- Facial coverings are available and welcomed but not required
- Wear yoru name badge or carry your JCCC photo id while on campus

**  College emergency response plan
https://www.jccc.edu/student-resources/police-safety/police-department/college-emergency-response-plan/

** Student code of conduct policy
http://www.jccc.edu/about/leadership-governance/policies/students/student-code-of-conduct/student-code-conduct.html

** Student handbook
http://www.jccc.edu/student-resources/student-handbook.html

** Campus safety
Information regarding student safety can be found at http://www.jccc.edu/student-resources/police-safety/. Classroom and campus safety are of paramount importance at Johnson County Community College and are the shared responsibility of the entire campus population. Please review the following:

- *Report emergencies:* to Campus Police (available 24 hours a day)
  - In person at the Midwest Trust Center (MTC 115)
  - Call 913-469-2500 (direct line) – Tip: program in your cell phone
  - Phone app - download JCCC Guardian (the free campus safety app: www.jccc.edu/guardian) - instant panic button and texting capability to Campus Police
  - Anonymous reports to KOPS-Watch -

    https://secure.ethicspoint.com/domain/en/report_company.asp?clientid=25868 or 888-258-3230

- *Be Alert:*
  - You are an extra set of eyes and ears to help maintain campus safety
  - Trust your instincts
  - Report suspicious or unusual behavior/circumstances to Campus Police (see above)

- *Be Prepared:*
  - Identify the red/white stripe Building Emergency Response posters throughout campus and online that show egress routes, shelter, and equipment
  - View A.L.I.C.E. training (armed intruder response training - Alert, Lockdown, Inform, Counter and/or Evacuate)
    - Student training video: https://www.youtube.com/watch?v=kMcT4-nWSq0
  - Familiarize yourself with the College Emergency Response Plan:
    (jccc.edu/student-resources/police-safety/college-emergency-response-plan/)

- *During an emergency:* Notifications/Alerts (emergencies and inclement weather) are sent to all employees and students using email and text messaging
  - students are automatically enrolled, see JCCC Alert - Emergency Notification:
    (jccc.edu/student-resources/police-safety/jccc-alert.html)

- *Weapons policy:* Effective July 1, 2017, concealed carry handguns are permitted in JCCC buildings subject to the restrictions set forth in the Weapons Policy. Handgun safety training is encouraged of all who choose to conceal carry. Suspected violations should be reported to JCCC Police Department 913-469-2500 or if an emergency, you can also call 911.




-----

* Course catalog info

[[https://catalog.jccc.edu/coursedescriptions/cs/#CS_235]]

*Objectives:*

    Develop C++ programs using a disciplined object-oriented approach to software development.
    Create C++ classes using the concepts of encapsulation and data abstraction.
    Create programs that integrate advanced programming topics.
    Develop new classes by inheriting existing classes.
    Implement polymorphism to produce dynamic, run-time applications.
    Employ commonly accepted programming standards for code and documentation.


*Content Outline and Competencies:*
#+BEGIN_SRC
I.  Software Development
  A. Identify C++ features needed to solve problems in C++.
  B. Define the problem and identify the classes.
  C. Develop a solution.
  D. Code the solution.
  E. Test the solution.
II.  Encapsulation and Data Abstraction
  A. Apply the C++ syntax to define classes.
  B. Use the string class and the vector template classes.
  C. Create copy constructors.
  D. Create friend functions.
  E. Create overloaded operators including the >>, << and = operators.
  F. Implement class composition.
  G. Describe and create constructors and destructors.
  H. Describe use of the “this” pointer.
  I. Implement classes that contain arrays of objects.
  J. Compare static and instance data members in a class.
  K. Compare private and public access specifiers.
III. Advanced Programming Topics
  A. Establish pointers to manage dynamic memory.
    1. Use new and delete operators.
    2. Declare dynamic memory arrays.
    3. Define and use pointers to objects.
    4. Define objects containing pointers.
  B. Create recursive functions.
  C. Organize projects into multiple files.
  D. Handle exceptions caused by unusual or error conditions during program execution.
  E.  Describe and use the C++ I/O system.
  F.  Create templates to develop data independent classes.
IV. Inheritance
  A. Explain how inheritance can be used to develop new classes.
  B. Explain the “is-a” and the “has-a” relationships between classes.
  C. Describe and use constructors and destructors for inherited classes.
  D. Define the base class.
  E. Use inherited classes.
  F. Create overloaded, inherited functions.
V. Polymorphism
  A. Explain how polymorphism is used to solve programming problems.
  B. Create virtual functions.
  C. Create abstract classes using pure virtual functions.
  D.  Explain static binding and dynamic binding.
VI. Code Standards
  A. Create descriptive identifiers according to language naming conventions.
  B. Write structured and readable code.
  C. Create documentation.
#+END_SRC
