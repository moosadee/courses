* How do we test?

#+ATTR_LATEX: :width 0.5\textwidth
[[file:../../../images/errorinmain.png]]

How do you know that your program actually works? Especially as it gets more complex?

Software development skills mean more than just writing code. It also includes knowing how to test your code, to prove that it is working as intended.

For the most basic tests, we investigate a portion of code and ask, "Given some inputs, what are the expected outputs?" and "If I run the program with these *inputs*, what are the *actual outputs*?", and finally, "Does my *actual output* match my *expected output*?"

~

* Writing tests first?

#+ATTR_LATEX: :width 0.5\textwidth
[[file:../../../images/tests.png]]

Notice that you don't actually need to know /what/ is in the /function/ before you write tests for it. Knowing what the program is /supposed to do/, you can figure out a set of inputs and outputs that it ought to have.

Often, it is very useful to write your *test cases* prior to actually writing any code. This helps you solidify what exactly he program is supposed to do. Plus, if you write your test cases afterwards, it is a bit like reading your own essay - your brain will skip over errors because it /knows what you meant/, and doesn't actually read the actual words (or code).

~

* Multiple test cases

#+ATTR_LATEX: :width 0.5\textwidth
[[file:../../../images/itworks.png]]

It isn't good enough to write one single test case. Often, you will want to have enough test cases to check for as many possibilities as possible - though we can't write an infinite amount. So sometimes, it's best to just write tests for reasonable outcomes, including potential errors.

When we eventually write code to do our testing for us, we can have the program keep an "ear out" for an error happening - and, sometimes we want that error to occur. Such as if the user enters a negative deposit amount, we would want the program to notice and send an error code or error message. We could also write our tests to make sure the error occurs, rather than allowing the user to deposit (or withdraw) a "negative" amount of money!

~

* Example 1

#+ATTR_LATEX: :width 0.5\textwidth
[[file:../../../images/coworker-maryam.png]]

Let's say your coworker Maryam is writing code for a new feature and you're writing the tests. You both have the requirements, so you can both work at the same time.

For the program, it takes in a list of grades (4.0 being 'A', 3.0 being 'B', 2.0 being 'C', 1.0 being 'D', and 0.0 being 'F') and calculates a grade point average. (Averages are calculated as the *sum of all the grades* divided by *the amount of grades*.)

~

* Example 2

#+ATTR_LATEX: :width 0.5\textwidth
[[file:../../../images/coworker-andre.png]]

Now your coworker André is working on another part of the program, and you're writing the tests.

The program takes in the price of a given textbook title, and an amount of that textbook to purchase, and returns the total cost of purchasing those textbooks.

~

* Reminder

#+ATTR_LATEX: :width 0.5\textwidth
[[file:../../../images/reminder.png]]

Takeaways:

- A single test case consists of inputs and their expected output(s).
- Running the program with the test's inputs will give us the actual output(s).
- A test will pass if the actual output matches the expected output.
- A test will fail if the actual output does not match the expected output.
- Having multiple test cases help you verify that your code works properly.
