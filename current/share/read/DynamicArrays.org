# -*- mode: org -*-

* Program memory

[[file:../../../images/topics/program-memory-space.png]]

Programs have a memory space where different types of data is stored.

- *Text* is program instructions.
- *Data* is where global and static variables are stored.
- *Heap* space is where dynamically allocated variables/arrays are stored.
- *Stack* space is where local variables, parameter variables, are stored.

There is a limit to stack space. When you have something like a logic error in a recursive function and it keeps allocating space for variables eventually you'll get a Stack Overflow, running out of stack space.

Dynamically allocated variables and arrays are stored on the Heap.

#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

* Dynamic array

We can use Dynamic Arrays to allocate space for an array at run-time, without having to know or hard-code the array size in our code. To do this, we need to allocate memory on the heap via a *pointer*.

In C++ we use the *new* and *delete* keywords to allocate and deallocate memory. Because we have to manually manage this memory, it's important to remember to *delete* anything that has been allocated via *new*.

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*Creating a dynamic array*

We don't have to know the size of the array at compile-time, so we can do things

#+BEGIN_SRC cpp :class cpp
int size;
cout << "Enter size: ";
cin >> size;
string* products = new string[size];
#+END_SRC

like ask the user to enter a size, or otherwise base its size off a variable.

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*Setting elements of the array*

We can access elements of the dynamic array with the subscript operator, as before. However, we won't know the size of the array unless we use the =size= variable, so it'd be better to use a for loop to assign values instead of this example:

#+BEGIN_SRC cpp :class cpp
products[0] = "Pencil";
products[1] = "Eraser";
products[2] = "Pencil case";
products[3] = "Pencil sharpener";
products[4] = "Ruler";
#+END_SRC

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*Iterating over the array*

Accessing elements of the array and iterating over the array is done the same way as with a traditional array.

#+BEGIN_SRC cpp :class cpp
for ( unsigned int i = 0; i < size; i++ )
{
  cout << i << ". "; // Display index
  cout << products[i] << endl; // Display element at that index
}
#+END_SRC

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*Freeing the memory when done*

Before your pointer loses scope we need to make sure to free the space that we allocated:

#+BEGIN_SRC cpp :class cpp
delete [] products;
#+END_SRC

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

** "Resizing" the array

When memory is allocated for an array we must know the entire size at once
because all of the array's elements are /contiguous in memory/. Because of this,
we don't technically "resize" a dynamic array, instead we allocate more space /elseware/
and copy the data over to the new array. Here are the steps:

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*One: Allocate space for a new, bigger array*

#+BEGIN_SRC cpp :class cpp
string* newArray = new string[ size + 10 ];
#+END_SRC

*Two: Copy the data from the old array to the new array*

#+BEGIN_SRC cpp :class cpp
for ( unsigned int i = 0; i < size; i++ )
{
  newArray[i] = products[i];
}
#+END_SRC

*Three: Free the space at the old address*

#+BEGIN_SRC cpp :class cpp
delete [] products;
#+END_SRC

*Four: Update the main array pointer to the new address*

#+BEGIN_SRC cpp :class cpp
products = newArray; // Point to same address
#+END_SRC

*Five: Update your size variable*

#+BEGIN_SRC cpp :class cpp
size = size + 10;
#+END_SRC

-------------------------------------------------------------------------

#+BEGIN_LATEX
\vspace{2cm}
#+END_LATEX

* Review questions:

1. How do you allocate memory for an array via a pointer?
2. How do you deallocate memory for a dynamic array?
3. What are the steps to "resize" a dynamic array?
