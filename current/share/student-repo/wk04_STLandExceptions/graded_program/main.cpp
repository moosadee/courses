#include <iostream>
using namespace std;

#include "FixedArrayTester.h"
#include "FixedArray.h"

int main( int argCount, char* args[] )
{
    if ( argCount == 2 && args[1] == string( "test" ) )
    {
        FixedArrayTester tester;
        tester.RunAll();
        return 0;
    }

    // Run program
    FixedArray<string> courses;
    courses.PushBack( "CS 134" );
    courses.PushBack( "CS 200" );
    courses.PushBack( "CS 235" );
    courses.PushBack( "CS 250" );

    cout << "Display array:" << endl;
    courses.Display();

    cout << endl;
    cout << "Back:  " << courses.GetBack() << endl;
    cout << "Front: " << courses.GetFront() << endl;
    cout << "At 2:  " << courses.GetAt( 2 ) << endl;
    cout << "At 10: " << courses.GetAt( 10 ) << endl; // CRASHES PROGRAM
    cout << "Size: " << courses.Size() << endl;
    cout << "Location of \"CS 210\": " << courses.Search( "CS 210" ) << endl; // CRASHES PROGRAM
    cout << endl;
    cout << "Display array again:" << endl;
    courses.PopBack();
    courses.Display();

    return 0;
}
