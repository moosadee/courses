#ifndef _FIXED_ARRAY_HPP
#define _FIXED_ARRAY_HPP

#include <iostream>
#include <stdexcept>                                                                                // NEW
using namespace std;

template <typename T>
//! A data structure that wraps a fixed array
class FixedArray
{
public:
    FixedArray();

    void PushBack( T newItem );
    void PopBack();
    T& GetBack();
    T& GetFront();
    T& GetAt( size_t index );

    size_t Search( T item ) const;
    size_t Size() const;
    bool IsEmpty() const;
    bool IsFull() const;
    void Display() const;
    void Clear();

private:
    T m_array[10];
    const size_t ARRAY_SIZE;
    size_t m_itemCount;

    friend class FixedArrayTester;
};

template <typename T>
FixedArray<T>::FixedArray()
    : ARRAY_SIZE( 10 )
{
    // TODO: Implement
}

template <typename T>
void FixedArray<T>::Clear()
{
}

template <typename T>
size_t FixedArray<T>::Size() const
{
    return 0; // TODO: REMOVE THIS LINE!                                                            // STARTER CODE
}

template <typename T>
bool FixedArray<T>::IsFull() const
{
    return false; // TODO: REMOVE THIS LINE!                                                        // STARTER CODE
}

template <typename T>
bool FixedArray<T>::IsEmpty() const
{
    return false; // TODO: REMOVE THIS LINE!                                                       // STARTER CODE
}

template <typename T>
void FixedArray<T>::PushBack( T newItem )
{
    // ERROR CHECK
    if ( IsFull() )
    {
        // TODO: Throw a `length_error` exception                                                   // STARTER CODE
    }
    // COPY/PASTE IMPLEMENTATION FROM WEEK 2 graded_program...
}

template <typename T>
void FixedArray<T>::PopBack()
{
    // ERROR CHECK
    if ( IsEmpty() )
    {
        // TODO: Throw a `runtime_error` exception                                                  // STARTER CODE
    }
    // COPY/PASTE IMPLEMENTATION FROM WEEK 2 graded_program...
}

template <typename T>
T& FixedArray<T>::GetBack()
{
    // ERROR CHECK
    if ( IsEmpty() )
    {
        // TODO: Throw a `runtime_error` exception                                                  // STARTER CODE
    }
    // COPY/PASTE IMPLEMENTATION FROM WEEK 2 graded_program...
}

template <typename T>
T& FixedArray<T>::GetFront()
{
    // ERROR CHECK
    if ( IsEmpty() )
    {
        // TODO: Throw a `runtime_error` exception                                                  // STARTER CODE
    }
    return m_array[0];
}

template <typename T>
T& FixedArray<T>::GetAt( size_t index )
{
    // ERROR CHECKS
    if ( IsEmpty() )
    {
        // TODO: Throw a `runtime_error` exception                                                  // STARTER CODE
    }
    else if ( index < 0 || index >= m_itemCount )
    {
        // TODO: Throw a `out_of_range` exception                                                   // STARTER CODE
    }
    // COPY/PASTE IMPLEMENTATION FROM WEEK 2 graded_program...
}

template <typename T>
size_t FixedArray<T>::Search( T item ) const
{
    // COPY/PASTE IMPLEMENTATION FROM WEEK 2 graded_program...
}

template <typename T>
void FixedArray<T>::Display() const
{
    for ( size_t i = 0; i < m_itemCount; i++ )
    {
        cout << i << ". " << m_array[i] << endl;
    }
}

#endif
