// !! Don't need to change this file !!
#ifndef _FIXED_ARRAY_TESTER
#define _FIXED_ARRAY_TESTER

#include "FixedArray.h"

const string CLEAR    = "\033[0m";
const string RED      = "\033[3;31m";
const string GREEN    = "\033[3;32m";

class FixedArrayTester
{
public:
    void RunAll();
    void Test_FixedArray_Constructor();
    void Test_FixedArray_PushBack();
    void Test_FixedArray_PopBack();
    void Test_FixedArray_GetBack();
    void Test_FixedArray_GetFront();
    void Test_FixedArray_GetAt();
    void Test_FixedArray_IsFull();
    void Test_FixedArray_IsEmpty();
    void Test_FixedArray_Clear();
    void Test_FixedArray_Size();
    void Test_FixedArray_Search();
};
#endif

