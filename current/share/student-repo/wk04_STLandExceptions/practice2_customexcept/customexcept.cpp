#include <stdexcept>
#include <iostream>
#include <string>
#include <array>
using namespace std;

// TODO: Implement a `NotEnoughFriendsException` custom exception                                   // STARTER CODE

// TODO: Implement a `NotEnoughPizzaException` custom exception                                     // STARTER CODE

int SlicesPerPerson(int friends, int pizzaSlices)
{
    // TODO: If friends is 0, throw a `NotEnoughFriendsException` exception                         // STARTER CODE
    // TODO: If pizza slices is 0, throw a `NotEnoughPizzaException` exception                      // STARTER CODE
    return float(pizzaSlices) / friends;
}

int main()
{
    std::cout << std::string( 80, '-' ) << std::endl;
    std::cout << "PIZZA PARTY" << std::endl;

    int friendCount, pizzaSliceCount;
    std::cout << "How many pizza slices at pizza party? ";
    std::cin >> pizzaSliceCount;

    std::cout << "How many friends at party? ";
    std::cin >> friendCount;

    std::cout << endl;

    int slices = -1;
    // TODO: Surround the risky function call with try/catch blocks.                                // STARTER CODE
		slices = SlicesPerPerson(friendCount, pizzaSliceCount);                                         // STARTER CODE

    return 0;
}






