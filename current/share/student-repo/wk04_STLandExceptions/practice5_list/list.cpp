#include <iostream>
#include <string>
#include <list>
using namespace std;


int main()
{
    cout << "LIST PROGRAM" << endl;

    cout << endl << "Declare a list of strings..." << endl;                                         // STARTER CODE

    bool done = false;
    string input, position;
    while ( !done )
    {
        cout << "Enter a new course, or STOP to finish: ";
        getline( cin, input );
        if ( input == "STOP" )
        {
            break;
        }

        cout << "Insert at (F) FRONT or (B) BACK? ";
        getline( cin, position );
        if ( position == "F" )
        {
            cout << endl << "Pushing new item to front of list..." << endl;                          // STARTER CODE
        }
        else if ( position == "B" )
        {
            cout << endl << "Pushing new item to back of list..." << endl;                           // STARTER CODE
        }
    }

    cout << endl << "Iterating over the list to display each element's  value..." << endl;           // STARTER CODE

    cout << endl << "THE END" << endl;
    return 0;
}
