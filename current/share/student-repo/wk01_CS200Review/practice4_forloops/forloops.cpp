// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
using namespace std;  // Using the C++ STanDard libraries

// - PROGRAM CODE -----------------------------------------------------------//
int main( int argCount, char* args[] )
{
    int lowNumber;
    int highNumber;
    int step;
    int counter;

    if ( argCount == 4 )
    {
                                lowNumber = stof( args[1] );
                                highNumber = stof( args[2] );
                                step = stof( args[3] );
    }
    else
    {
                                cerr << "Expected form: " << args[0] << " LOW HIGH STEP" << endl;
                                return 1;
    }

    // STUDENT CODE -----------------------------------------------------------
    cout << endl << "LOW TO HIGH: ";
    // TODO: Implement low to high loop


    cout << endl << "HIGH TO LOW: ";
                // TODO: Implement high to low loop

    // ------------------------------------------------------------------------

    // Return 0 means quit program with no errors, in this context.
    cout << endl << endl;
    return 0;
}

/* INSTRUCTIONS:
         Create a for loop that goes from LOW to HIGH, sing the STEP amount given.
         Then, go from HIGH to LOW, going down by the STEP given.

         BUILD AND RUN:
         g++ forloops.cpp
         ./a.out OR ./a.exe

         EXAMPLE OUTPUT:
         $ ./a.out 2 12 3

         LOW TO HIGH: 2 5 8 11
         HIGH TO LOW: 12 9 6 3
 *
 */
