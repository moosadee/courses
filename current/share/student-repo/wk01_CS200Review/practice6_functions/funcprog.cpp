#include <iostream>
#include <string>
using namespace std;

/**
   CalculatePricePlusTax function
   NO input parameters / YES output return
   @param   original_price    The original price of an item (before tax)
   @param   tax_percent       The tax rate, in % form
   @return  float             The new price after tax is applied
   Calculates the price plus the additional price to due tax and returns the value.
*/
// STUDENT CODE -----------------------------------------------------------
// TODO: Define the CalculatePricePlusTax function here

// ------------------------------------------------------------------------

int main( int argCount, char* args[] )
{
    float price, tax, updatedPrice;

    if ( argCount == 3 )
    {
        price = stof( args[1] );
        tax = stof( args[2] );
    }
    else
    {
        cerr << "Expected form: " << args[0] << " PRICE TAXPERCENT" << endl;
        return 1;
    }

    // STUDENT CODE -----------------------------------------------------------
    // TODO: Call the CalculatePricePlusTax function and store its return result in a variable

    // ------------------------------------------------------------------------

    cout << "Original price: $" << price << endl;
    cout << "Sales tax:      %" << tax << endl;
    cout << "Updated price:  $" << updatedPrice << endl;

    return 0;
}

/* INSTRUCTIONS:
   Above main() define the CalculatePricePlusTax function according to the
   input parameters and return specified in the comment above.

   Within main(), call the CalculatePricePlusTax function, passing in
   the `price` and `tax`, and storing the result in `updatedPrice`.`

   BUILD AND RUN:
   g++ funcprog.cpp
   ./a.out OR ./a.exe

   EXAMPLE OUTPUT:
   $ ./a.out 100 9.5
   Original price: $100
   Sales tax:      %9.5
   Updated price:  $109.5
*/
