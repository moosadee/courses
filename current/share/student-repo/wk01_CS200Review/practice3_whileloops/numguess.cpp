#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
#include <cstdlib>    // Needed for rand
#include <ctime>      // Needed for time
using namespace std;  // Using the C++ STanDard libraries


int main()
{
    srand( time( NULL ) ); // Seed the random number generator.
    int secretNumber = rand() % 10 + 1; // Random number between 1 and 10.
    int playerGuess = 0;
    int totalGuesses = 0;

    // STUDENT CODE -----------------------------------------------------------

    // ------------------------------------------------------------------------

    cout << endl << "YOU WIN!" << endl;

    return 0;
}

/* INSTRUCTIONS:
   Create a while loop that will continue running while the user has not yet
   guessed the secret number. Within the loop, ask the user for their
   next guess and use a cin statement to get their selection. Also add
   to the total guesses each time.

   BUILD AND RUN:
   g++ numguess.cpp

   ./a.out OR ./a.exe

   EXAMPLE OUTPUT:
   $ ./a.out
   TRY 1 - Guess the secret number...: 6
   TRY 2 - Guess the secret number...: 4
   TRY 3 - Guess the secret number...: 2
   TRY 4 - Guess the secret number...: 3
   TRY 6 - Guess the secret number...: 1
*/
