#include <iostream>
#include <string>
#include <fstream>
using namespace std;

int main( int argCount, char* args[] )
{
    string filepath = "";
    string buffer = "";
    string loadedText = "";
    string userText = "";
    ifstream input;
    ofstream output;

    if ( argCount == 2 )
    {
        filepath = string( args[1] );
    }
    else
    {
        cerr << "Expected form: " << args[0] << " FILE" << endl;
        return 1;
    }

    // STUDENT CODE -----------------------------------------------------------
                
    // ------------------------------------------------------------------------

    return 0;
}

/* INSTRUCTIONS:
   Use the `input` ifstream object and open the `filepath` given.
   If this fails (input.fail()) then display an error and exit with
   an error code.

   While there is text in the file load it in the buffer:
   while ( getline( input, buffer ) )
   within the loop, add the `buffer` to the `loadedText`, plus a new line.
   Use cout to display the currently loaded line of text.
   After the while loop, close the `input` file.

   Next, ask the user for a new line of text to add.
   Get their input and store it in the `userText`. Add this to
   the end of the `loadedText` as well.

   Then, use the `output` and open the `filepath` again.
   Output the updated `loadedText` to the output file,
   then close the output file.

   BUILD AND RUN:
   g++ saveload.cpp
   ./a.out OR ./a.exe

   EXAMPLE PROGRAM OUTPUT:
   $ ./a.out file1.txt
   LOADED: The Fox and the Grapes
   LOADED:
   LOADED: A FAMISHED FOX saw some clusters of ripe black grapes hanging from
   LOADED: a trellised vine. She resorted to all her tricks to get at them, but
   LOADED: wearied herself in vain, for she could not reach them. At last she
   LOADED: turned away, hiding her disappointment and saying: "The Grapes are sour,
   LOADED: and not ripe as I thought."

   Enter a new line to add: Then the Fox went to sleep.
   file1.txt updated.


   EXAMPLE FILE OUTPUT:
   The Fox and the Grapes

   A FAMISHED FOX saw some clusters of ripe black grapes hanging from
   a trellised vine. She resorted to all her tricks to get at them, but
   wearied herself in vain, for she could not reach them. At last she
   turned away, hiding her disappointment and saying: "The Grapes are sour,
   and not ripe as I thought."
   Then the Fox went to sleep.

*/
