#include "Ingredient.h"

#include <string>
#include <iostream>
using namespace std;

/**
   @param  newName   A new value to store as the ingredient's name
*/
void Ingredient::SetName( string newName )
{
    if ( newName == "" ) { return; }
    m_name = newName;
}

/**
   @param  newUnit   A new value to store as the ingredient's unit
*/
void Ingredient::SetUnit( string newUnit )
{
    if ( newUnit == "" ) { return; }
    m_unit = newUnit;
}

/**
   @param  newAmount  A new value to store as the ingredient's amount
*/
void Ingredient::SetAmount( float newAmount )
{
    if ( newAmount <= 0 ) { return; }
    m_amount = newAmount;
}

/**
   @return  The value of the corresponding member variable (for ingredient's name)
*/
string Ingredient::GetName() const
{
    return m_name;
}

/**
   @return  The value of the corresponding member variable (for ingredient's unit)
*/
string Ingredient::GetUnit() const
{
    return m_unit;
}

/**
   @param  batches  The amount of batches to alter the ingredient amount by
   @return  The value of the corresponding member variable (for ingredient's amount)
*/
float Ingredient::GetAmount( float batches ) const
{
    return m_amount * batches;
}

/**
   @param  newName   A new value to store as the ingredient's name
   @param  newUnit   A new value to store as the ingredient's unit
   @param  newAmount  A new value to store as the ingredient's amount
   Sets up the ingredient's member variables
*/
void Ingredient::Setup( string newName, string newUnit, float newAmount )
{
    SetName( newName );
    SetUnit( newUnit );
    SetAmount( newAmount );
}

/**
   Default constructor - Set name and unit to "UNKNOWN" and amount to 0.
*/
Ingredient::Ingredient()
{
    Setup( "UNKONWN", "UNKNOWN", 0 );
}

/**
   @param  newName   A new value to store as the ingredient's name
   @param  newUnit   A new value to store as the ingredient's unit
   @param  newAmount  A new value to store as the ingredient's amount
   Parameterized constructor
   Sets up the ingredient's member variables
*/
Ingredient::Ingredient( string newName, string newUnit, float newAmount )
{
    Setup( newName, newUnit, newAmount );
}

/**
   @param  batches  The amount of batches to alter the ingredient amount by
   Displays the ingredient's amount to use (modified by batches), the unit of measurement, and the name of the ingredient
*/
void Ingredient::Display( float batches )
{
    cout << "* " << GetAmount( batches )
         << " " << GetUnit()
         << " of " << GetName() << endl;
}

/**
   Function to test the Ingredient class
*/
void Test_Ingredient()
{
    const std::string GRN = "\033[0;32m"; const std::string RED = "\033[0;31m"; const std::string CLR = "\033[0m";

    {
        Ingredient ing;
        string input_newName = "NAME";
        ing.SetName( input_newName );
        string expect_out = "NAME";
        string actual_out = ing.m_name;

        if ( actual_out == expect_out ) { cout << GRN << "[PASS] "; }
        else                            { cout << RED << "[FAIL] "; }
        
        cout << "Call SetName( \"" << input_newName << "\" ); check member variable" << endl;
        cout << " * Expected m_name: " << expect_out << endl;
        cout << " * Actual   m_name: " << actual_out << endl;
        cout << CLR << endl;
    }

    {
        Ingredient ing;
        string input_newName = "cheese";
        ing.SetName( input_newName );
        string expect_out = "cheese";
        string actual_out = ing.m_name;

        if ( actual_out == expect_out ) { cout << GRN << "[PASS] "; }
        else                            { cout << RED << "[FAIL] "; }
        
        cout << "Call SetName( \"" << input_newName << "\" ); check member variable" << endl;
        cout << " * Expected m_name: " << expect_out << endl;
        cout << " * Actual   m_name: " << actual_out << endl;
        cout << CLR << endl;
    }
    
    {
        Ingredient ing;
        string input_newUnit = "UNIT";
        ing.SetUnit( input_newUnit );
        string expect_out = "UNIT";
        string actual_out = ing.m_unit;

        if ( actual_out == expect_out ) { cout << GRN << "[PASS] "; }
        else                            { cout << RED << "[FAIL] "; }
        
        cout << "Call SetUnit( \"" << input_newUnit << "\" ); check member variable" << endl;
        cout << " * Expected m_unit: " << expect_out << endl;
        cout << " * Actual   m_unit: " << actual_out << endl;
        cout << CLR << endl;
    }
    
    {
        Ingredient ing;
        string input_newUnit = "cups";
        ing.SetUnit( input_newUnit );
        string expect_out = "cups";
        string actual_out = ing.m_unit;

        if ( actual_out == expect_out ) { cout << GRN << "[PASS] "; }
        else                            { cout << RED << "[FAIL] "; }
        
        cout << "Call SetUnit( \"" << input_newUnit << "\" ); check member variable" << endl;
        cout << " * Expected m_unit: " << expect_out << endl;
        cout << " * Actual   m_unit: " << actual_out << endl;
        cout << CLR << endl;
    }
    
    {
        Ingredient ing;
        float input_newAmount = 12.34;
        ing.SetAmount( input_newAmount );
        float expect_out = 12.34;
        float actual_out = ing.m_amount;

        if ( actual_out == expect_out ) { cout << GRN << "[PASS] "; }
        else                            { cout << RED << "[FAIL] "; }
        
        cout << "Call SetAmount( " << input_newAmount << " ); check member variable" << endl;
        cout << " * Expected m_amount: " << expect_out << endl;
        cout << " * Actual   m_amount: " << actual_out << endl;
        cout << CLR << endl;
    }
    
    {
        Ingredient ing;
        float input_newAmount = 13.37;
        ing.SetAmount( input_newAmount );
        float expect_out = 13.37;
        float actual_out = ing.m_amount;

        if ( actual_out == expect_out ) { cout << GRN << "[PASS] "; }
        else                            { cout << RED << "[FAIL] "; }
        
        cout << "Call SetAmount( " << input_newAmount << " ); check member variable" << endl;
        cout << " * Expected m_amount: " << expect_out << endl;
        cout << " * Actual   m_amount: " << actual_out << endl;
        cout << CLR << endl;
    }

    {
        Ingredient ing;
        ing.m_amount = 5;
        float input_newAmount = -10;
        ing.SetAmount( input_newAmount );
        float expect_out = 5;
        float actual_out = ing.m_amount;

        if ( actual_out == expect_out ) { cout << GRN << "[PASS] "; }
        else                            { cout << RED << "[FAIL] "; }
        
        cout << "Call SetAmount( " << input_newAmount << " ); check for NO UPDATE" << endl;
        cout << " * Expected m_amount: " << expect_out << endl;
        cout << " * Actual   m_amount: " << actual_out << endl;
        cout << CLR << endl;
    }

    {
        Ingredient ing;
        ing.m_name = "egg";
        string expect_out = "egg";
        string actual_out = ing.GetName();

        if ( actual_out == expect_out ) { cout << GRN << "[PASS] "; }
        else                            { cout << RED << "[FAIL] "; }

        cout << "Set m_name; call GetName()" << endl;
        cout << " * Expected GetName(): " << expect_out << endl;
        cout << " * Actual   GetName(): " << actual_out << endl;
        cout << CLR << endl;
    }

    {
        Ingredient ing;
        ing.m_unit = "tsp";
        string expect_out = "tsp";
        string actual_out = ing.GetUnit();

        if ( actual_out == expect_out ) { cout << GRN << "[PASS] "; }
        else                            { cout << RED << "[FAIL] "; }

        cout << "Set m_unit; call GetUnit()" << endl;
        cout << " * Expected GetUnit(): " << expect_out << endl;
        cout << " * Actual   GetUnit(): " << actual_out << endl;
        cout << CLR << endl;
    }
    
    {
        Ingredient ing;
        ing.m_unit = "cup";
        string expect_out = "cup";
        string actual_out = ing.GetUnit();

        if ( actual_out == expect_out ) { cout << GRN << "[PASS] "; }
        else                            { cout << RED << "[FAIL] "; }

        cout << "Set m_unit; call GetUnit()" << endl;
        cout << " * Expected GetUnit(): " << expect_out << endl;
        cout << " * Actual   GetUnit(): " << actual_out << endl;
        cout << CLR << endl;
    }
    
    {
        Ingredient ing;
        ing.m_amount = 10.25;
        float input_batches = 1;
        float expect_out = 10.25;
        float actual_out = ing.GetAmount( input_batches );

        if ( actual_out == expect_out ) { cout << GRN << "[PASS] "; }
        else                            { cout << RED << "[FAIL] "; }

        cout << "Set m_amount; call GetAmount( " <<  input_batches << " )" << endl;
        cout << " * Expected GetAmount(): " << expect_out << endl;
        cout << " * Actual   GetAmount(): " << actual_out << endl;
        cout << CLR << endl;
    }
    
    {
        Ingredient ing;
        ing.m_amount = 5.00;
        float input_batches = 1.25;
        float expect_out = 6.25;
        float actual_out = ing.GetAmount( input_batches );

        if ( actual_out == expect_out ) { cout << GRN << "[PASS] "; }
        else                            { cout << RED << "[FAIL] "; }

        cout << "Set m_amount; call GetAmount( " <<  input_batches << " )" << endl;
        cout << " * Expected GetAmount(): " << expect_out << endl;
        cout << " * Actual   GetAmount(): " << actual_out << endl;
        cout << CLR << endl;
    }

    {
        Ingredient ing;
        string input_newName = "cheese";
        string input_newUnit = "cups";
        float input_newAmount = 5;
        ing.Setup( input_newName, input_newUnit, input_newAmount );
        string expect_out1 = "cheese";
        string actual_out1 = ing.m_name;
        string expect_out2 = "cups";
        string actual_out2 = ing.m_unit;
        float expect_out3 = 5;
        float actual_out3 = ing.m_amount;

        if ( actual_out1 == expect_out1 && actual_out2 == expect_out2 && actual_out3 == expect_out3 )
        { cout << GRN << "[PASS] "; }
        else                            { cout << RED << "[FAIL] "; }
        
        cout << "Call Setup( \"" << input_newName << "\", \"" << input_newUnit << "\", " << input_newAmount << " ); check member variables" << endl;
        cout << " * Expected m_name:   " << expect_out1 << endl;
        cout << " * Expected m_unit:   " << expect_out2 << endl;
        cout << " * Expected m_amount: " << expect_out3 << endl;
        cout << " * Actual   m_name:   " << actual_out1 << endl;
        cout << " * Actual   m_unit:   " << actual_out2 << endl;
        cout << " * Actual   m_amount: " << actual_out3 << endl;
        cout << CLR << endl;
    }

    {
        Ingredient ing;
        ing.m_name = "salt";
        ing.m_unit = "tsp";
        ing.m_amount = 0.25;
        string input_newName = "";
        string input_newUnit = "";
        float input_newAmount = 0;
        ing.Setup( input_newName, input_newUnit, input_newAmount );
        string expect_out1 = "salt";
        string actual_out1 = ing.m_name;
        string expect_out2 = "tsp";
        string actual_out2 = ing.m_unit;
        float expect_out3 = 0.25;
        float actual_out3 = ing.m_amount;

        if ( actual_out1 == expect_out1 && actual_out2 == expect_out2 && actual_out3 == expect_out3 )
        { cout << GRN << "[PASS] "; }
        else                            { cout << RED << "[FAIL] "; }
        
        cout << "Call Setup( \"" << input_newName << "\", \"" << input_newUnit << "\", " << input_newAmount << " ); check INVALID INPUT IGNORED" << endl;
        cout << " * Expected m_name:   " << expect_out1 << endl;
        cout << " * Expected m_unit:   " << expect_out2 << endl;
        cout << " * Expected m_amount: " << expect_out3 << endl;
        cout << " * Actual   m_name:   " << actual_out1 << endl;
        cout << " * Actual   m_unit:   " << actual_out2 << endl;
        cout << " * Actual   m_amount: " << actual_out3 << endl;
        cout << CLR << endl;
    }

    {
        string input_newName = "tomato sauce";
        string input_newUnit = "tbsp";
        float input_newAmount = 2;
        Ingredient ing( input_newName, input_newUnit, input_newAmount );
        string expect_out1 = "tomato sauce";
        string actual_out1 = ing.m_name;
        string expect_out2 = "tbsp";
        string actual_out2 = ing.m_unit;
        float expect_out3 = 2;
        float actual_out3 = ing.m_amount;

        if ( actual_out1 == expect_out1 && actual_out2 == expect_out2 && actual_out3 == expect_out3 )
        { cout << GRN << "[PASS] "; }
        else                            { cout << RED << "[FAIL] "; }
        
        cout << "Call Ingredient ing( \"" << input_newName << "\", \"" << input_newUnit << "\", " << input_newAmount << " ); check member variables" << endl;
        cout << " * Expected m_name:   " << expect_out1 << endl;
        cout << " * Expected m_unit:   " << expect_out2 << endl;
        cout << " * Expected m_amount: " << expect_out3 << endl;
        cout << " * Actual   m_name:   " << actual_out1 << endl;
        cout << " * Actual   m_unit:   " << actual_out2 << endl;
        cout << " * Actual   m_amount: " << actual_out3 << endl;
        cout << CLR << endl;
    }
}
