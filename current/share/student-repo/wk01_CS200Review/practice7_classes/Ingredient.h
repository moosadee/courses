#ifndef _INGREDIENT
#define _INGREDIENT

#include <string>
using namespace std;

// - CLASS DECLARATION -----------------------------------------------------//
class Ingredient
{
public:
    // Member functions (methods)
    Ingredient(); // Default constructor
    Ingredient( string newName, string newUnit, float newAmount ); // Parameterized constructor
    void Setup( string newName, string newUnit, float newAmount );

    void SetName( string newName );
    void SetUnit( string newUnit );
    void SetAmount( float newAmount );

    string GetName() const;
    string GetUnit() const;
    float GetAmount( float batches ) const;

    void Display( float batches );

private:
    // Member variables
    string m_name;
    string m_unit;
    float m_amount;

    friend void Test_Ingredient();
};

void Test_Ingredient();

#endif
