// PROGRAM: Practice using class
// - INCLUDES ---------------------------------------------------------------//
#include <iostream>
#include <string>
#include <vector>
using namespace std;

#include "Ingredient.h"

// - PROGRAM CODE -----------------------------------------------------------//
int main( int argCount, char* args[] )
{
    float batches = 0.0;

    if ( argCount == 2 )
    {
        batches = stof( args[1] );
    }
    else
    {
        cerr << "Expected form: " << args[0] << " BATCHES" << endl;
        return 1;
    }

    cout << "BREAD RECIPE (" << batches << " batches):" << endl;

    // STUDENT CODE -----------------------------------------------------------
    // TODO: Replace these with a vector of Ingredient objects:
    string ingredient1_name = "Flour";
    string ingredient1_unit = "Cups";
    float  ingredient1_amount = 4.5;

    string ingredient2_name = "Sugar";
    string ingredient2_unit = "Tablespoons";
    float  ingredient2_amount = 1;

    string ingredient3_name = "Instant Yeast";
    string ingredient3_unit = "Teaspoons";
    float  ingredient3_amount = 2.25;

    string ingredient4_name = "Water";
    string ingredient4_unit = "Cups";
    float  ingredient4_amount 1.66;

    string ingredient5_name = "Table Salt";
    string ingredient5_unit = "Teaspoons";
    float  ingredient5_amount 2.5;

    // TODO: Call Display on each of the ingredients
    cout << "Ingredients" << endl;
    cout << "* " << ingredient1_amount << " " << ingredient1_unit << " of " << ingredient1_name << endl;
    cout << "* " << ingredient2_amount << " " << ingredient2_unit << " of " << ingredient2_name << endl;
    cout << "* " << ingredient3_amount << " " << ingredient3_unit << " of " << ingredient3_name << endl;
    cout << "* " << ingredient4_amount << " " << ingredient4_unit << " of " << ingredient4_name << endl;
    cout << "* " << ingredient5_amount << " " << ingredient5_unit << " of " << ingredient5_name << endl;
    // ------------------------------------------------------------------------

    return 0;
}
