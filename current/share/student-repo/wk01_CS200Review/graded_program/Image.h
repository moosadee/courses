#ifndef _IMAGE
#define _IMAGE

#include <string>
#include <vector>
using namespace std;

const string CLEAR    = "\033[0m";
const string BLACK    = "\033[3;30;40m";
const string RED      = "\033[3;31;41m";
const string GREEN    = "\033[3;32;42m";
const string YELLOW   = "\033[3;33;43m";
const string BLUE     = "\033[3;34;44m";
const string MAGENTA  = "\033[3;35;45m";
const string CYAN     = "\033[3;36;46m";
const string WHITE    = "\033[3;37;47m";

class Image
{
public:
    bool Load( string new_filename );
    void Draw();
    string GetFilename() const;
    void DrawPixel( char code ) const;

private:
    // TODO: Add a vector of strings named `pixelrows`

    // TODO: Add a string named `filename`
};

#endif
