#include <cstdlib>
#include <iostream>
using namespace std;

#include "Image.h"

int main( int argCount, char* args[] )
{
    string filename;

    system( "pwd" );
    system( "ls" );

    if ( argCount == 2 && string( args[1] ) == "test" )
    {
        Image image;
        image.Load( "image1.txt" );
        image.Draw();
        image.Load( "image2.txt" );
        image.Draw();
        image.Load( "image3.txt" );
        image.Draw();
        return 0;
    }
    else if ( argCount == 2 )
    {
        filename = string( args[1] );
    }
    else
    {
        cerr << "Expected form: " << args[0] << " FILENAME" << endl;
        return 1;
    }

    // STUDENT CODE -----------------------------------------------------------
    // TODO: Declare a variable named `image` whose type is `Image`.

    // TODO: Call image's Load function passing in the filename. Store its return result in a bool `success` variable.

    // TODO: If `success` is false, then display an error and exit the program.

    // TODO: Call the image's Draw function.

    // ------------------------------------------------------------------------

    return 0;
}
