#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
using namespace std;  // Using the C++ STanDard libraries

int main( int argCount, char* args[] )
{
    int charge;
    string graphic = "NOTSET";

    if ( argCount == 2 )
    {
                                charge = stoi( args[1] );
    }
    else
    {
                                cerr << "Expected form: " << args[0] << " BATTERYPERCENT" << endl;
                                return 1;
    }

    // STUDENT CODE -----------------------------------------------------------

    // ------------------------------------------------------------------------

    cout << graphic << endl;

    return 0;
}

/* INSTRUCTIONS:
         Utilize branching to decide which battery image to draw.
   90% and above: [****];  75% and above: [***-];  50% and above: [**--];
   25% and above: [*---];  below 25%: [----]

         BUILD AND RUN:
         g++ battery.cpp

         ./a.out OR ./a.exe

         EXAMPLE OUTPUT:
         $ ./a.out 50
         [**__]

         $ ./a.out 95
         [****]
 */
