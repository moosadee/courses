// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout`, `cin` commands
#include <string>     // Library that contains `string` types
using namespace std;  // Using the C++ STanDard libraries

void Test_Program();

// - PROGRAM CODE -----------------------------------------------------------//
int main( int argCount, char* args[] )
{
    // Variable declarations
          string name = "NOTSET";
                string semester = "NOTSET";
                int year = 2024;

                // Check if there are enough input arguments
    if ( argCount == 4 )
    {
                                name = string( args[1] );
                                semester = string( args[2] );
                                year = stoi( args[3] );
    }
                else
                {
                                cerr << "Expected form: " << args[0] << " NAME SEMESTER YEAR" << endl;
                                return 1;
                }

    // STUDENT CODE -----------------------------------------------------------
                
    // ------------------------------------------------------------------------

    // Return 0 means quit program with no errors, in this context.
    return 0;
}

/*
        INSTRUCTIONS: Use cout to display "Welcome to SEMESTER YEAR, NAME!",
        replacing SEMESTER, YEAR, and NAME with the corresponding variables.

        Afterwards, use cin and getline statements to get the user to enter
        a new year, name, and semester. Display the same welcome message
        again afterwards.

        Build the program with:
        g++ welcome.cpp

        Run the program on Windows with:
        ./a.exe

        Or on Mac/Linux with:
        ./a.out

        EXAMPLE OUTPUT:
        $ ./a.out RW Spring 2025

        Welcome to Spring 2025 semester, RW!

        Enter the year: 3035
        Enter a new name: Borkulon
        Enter the semester: Hivespring

        Welcome to Hivespring 3035 semester, Borkulon!
*/
