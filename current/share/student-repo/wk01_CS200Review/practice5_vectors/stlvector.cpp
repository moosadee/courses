#include <iostream>
#include <vector>
#include <string>
using namespace std;

// REFERENCE: std::vector documentation
// https://cplusplus.com/reference/vector/vector/

int main()
{
    // Initializing a vector with values:
    vector<string> courses = {
        "CS 134", "ENGL 121", "MATH 171", "WEB 110",
        "CIS 204", "CS 200", "IT 141", "COMS 120",
        "CIS 242", "CIS 260", "CS 235", "WEB 114",
        "CIS 264", "CIS 275", "CS 250" };
    string newCourse = "";


    // STUDENT CODE -----------------------------------------------------------

    // ------------------------------------------------------------------------

    return 0;
}

/* INSTRUCTIONS:
   Ask the user to enter the name of a new course to add. Store their
   response in the `newCourse` string variable, then push the new
   course to to `courses` vector.
   Afterwards, use a for loop to display each element's index and
   value.

   BUILD AND RUN:
   g++ stlvector.cpp
   ./a.out OR ./a.exe

   EXAMPLE OUTPUT:
   $ ./a.out

   Enter the code and number of a new course to add (e.g., ASL 120): ENG 101

   ALL COURSES:
   Course #0: CS 134
   Course #1: ENGL 121
   Course #2: MATH 171
   Course #3: WEB 110
   Course #4: CIS 204
   Course #5: CS 200
   Course #6: IT 141
   Course #7: COMS 120
   Course #8: CIS 242
   Course #9: CIS 260
   Course #10: CS 235
   Course #11: WEB 114
   Course #12: CIS 264
   Course #13: CIS 275
   Course #14: CS 250
   Course #15: ENG 101
*/
