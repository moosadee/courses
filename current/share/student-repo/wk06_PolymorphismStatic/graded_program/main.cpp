#include <iostream>
#include <iomanip>
#include <string>

#include "QuestionManager.h"
#include "Tester.h"

int main( int argCount, char* args[] )
{
    // Allow user to run program with "test" argument to run tests
    if ( argCount == 2 && string( args[1] ) == "test" )
    {
        Test();
        return 0;
    }
    else if ( argCount < 2 )
    {
        cout << "EXPECTED RUN:" << endl;
        cout << "1. " << args[0] << " test" << endl;
        cout << "2. " << args[0] << " run" << endl;
        return 1;
    }

    // TODO: Add questions via the question manager

    // TODO: Run the quiz
    

    return 0;
}

