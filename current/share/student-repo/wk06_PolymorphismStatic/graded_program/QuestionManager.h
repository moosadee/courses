#ifndef _QUESTION_MANAGER
#define _QUESTION_MANAGER

#include "QuestionFamily.h"

#include <vector>
using namespace std;

class QuestionManager
{
public:
    static void AddQuestion( IQuestion* newQuestion );
    static void RunQuiz();
    
private:
    static vector<IQuestion*> questions;
};

#endif
