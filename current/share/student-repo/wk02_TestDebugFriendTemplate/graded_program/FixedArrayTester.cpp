#include "FixedArrayTester.h"

#include <iostream>
using namespace std;

void FixedArrayTester::RunAll()
{
    cout << endl << "FixedArrayTester - RunAll" << endl;
    Test_FixedArray_Constructor();
    Test_FixedArray_Size();
    Test_FixedArray_Clear();
    Test_FixedArray_IsEmpty();
    Test_FixedArray_IsFull();
    Test_FixedArray_PopBack();
    Test_FixedArray_GetBack();
    Test_FixedArray_GetFront();
    Test_FixedArray_GetAt();
    Test_FixedArray_PushBack();
    Test_FixedArray_Search();
    cout << endl;
}

void FixedArrayTester::Test_FixedArray_Constructor()
{
    // TODO: Implement test                                                                         // STARTER CODE
}

void FixedArrayTester::Test_FixedArray_Size()
{
    { // Begin test scope
        cout << "Set m_itemCount to 5, Size() should return 5...";
        FixedArray<int> testArray;
        testArray.m_itemCount = 5;

        int expectOut = 5;
        int actualOut = testArray.Size();

        if ( actualOut == expectOut )
        {
            cout << GREEN << " PASS" << endl;
        }
        else
        {
            cout << RED << " FAIL... Was " << actualOut << " instead!" << endl;
        }
        cout << CLEAR;
    } // End test scope

    // TODO: Implement another test                                                                 // STARTER CODE
}

void FixedArrayTester::Test_FixedArray_Clear()
{
    // TODO: Implement test                                                                         // STARTER CODE
}

void FixedArrayTester::Test_FixedArray_IsEmpty()
{
    { // Begin test scope
        cout << "Set m_itemCount to 5, IsEmpty() should return false...";
        FixedArray<int> testArray;
        testArray.m_itemCount = 5;

        bool expectOut = false;
        bool actualOut = testArray.IsEmpty();

        if ( actualOut == expectOut )
        {
            cout << GREEN << " PASS" << endl;
        }
        else
        {
            cout << RED << " FAIL... Was " << actualOut << " instead!" << endl;
        }
        cout << CLEAR;
    } // End test scope

    // TODO: Implement another test                                                                 // STARTER CODE
}

void FixedArrayTester::Test_FixedArray_IsFull()
{
    // TODO: Implement tests                                                                        // STARTER CODE
}

void FixedArrayTester::Test_FixedArray_PopBack()
{
    { // Begin test scope
        cout << "Set m_itemCount to 2, call PopBack(). m_itemCount should now be 1...";
        FixedArray<int> testArray;
        testArray.m_itemCount = 2;

        testArray.PopBack();

        int expectOut = 1;
        int actualOut = testArray.m_itemCount;

        if ( actualOut == expectOut )
        {
            cout << GREEN << " PASS" << endl;
        }
        else
        {
            cout << RED << " FAIL... Was " << actualOut << " instead!" << endl;
        }
        cout << CLEAR;
    } // End test scope

    // TODO: Implement tests                                                                        // STARTER CODE
}

void FixedArrayTester::Test_FixedArray_GetBack()
{
    { // Begin test scope
        cout << "Put 10, 20, 30 in array. Call GetBack(). Should return 30...";
        FixedArray<int> testArray;
        testArray.m_itemCount = 3;
        testArray.m_array[0] = 10;
        testArray.m_array[1] = 20;
        testArray.m_array[2] = 30;

        int expectOut = 30;
        int actualOut = testArray.GetBack();

        if ( actualOut == expectOut )
        {
            cout << GREEN << " PASS" << endl;
        }
        else
        {
            cout << RED << " FAIL... Was " << actualOut << " instead!" << endl;
        }
        cout << CLEAR;
    } // End test scope

    // TODO: Implement more tests                                                                   // STARTER CODE
}

void FixedArrayTester::Test_FixedArray_GetFront()
{
    { // Begin test scope
        cout << "Put 10, 20, 30 in array. Call GetFront(). Should return 10...";
        FixedArray<int> testArray;
        testArray.m_itemCount = 3;
        testArray.m_array[0] = 10;
        testArray.m_array[1] = 20;
        testArray.m_array[2] = 30;

        int expectOut = 10;
        int actualOut = testArray.GetFront();

        if ( actualOut == expectOut )
        {
            cout << GREEN << " PASS" << endl;
        }
        else
        {
            cout << RED << " FAIL... Was " << actualOut << " instead!" << endl;
        }
        cout << CLEAR;
    } // End test scope

    // TODO: Implement more tests                                                                   // STARTER CODE
}

void FixedArrayTester::Test_FixedArray_GetAt()
{
    { // Begin test scope
        cout << "Put 10, 20, 30 in array. Call GetAt(1). Should return 20...";
        FixedArray<int> testArray;
        testArray.m_itemCount = 3;
        testArray.m_array[0] = 10;
        testArray.m_array[1] = 20;
        testArray.m_array[2] = 30;

        int expectOut = 20;
        int actualOut = testArray.GetAt(1);

        if ( actualOut == expectOut )
        {
            cout << GREEN << " PASS" << endl;
        }
        else
        {
            cout << RED << " FAIL... Was " << actualOut << " instead!" << endl;
        }
        cout << CLEAR;
    } // End test scope

    // TODO: Implement more tests                                                                   // STARTER CODE
}

void FixedArrayTester::Test_FixedArray_PushBack()
{
    { // Begin test scope
        cout << "Create empty array. Call PushBack(A). itemCount should be 1, m_array[0] should be A.";
        FixedArray<string> testArray;
        testArray.PushBack( "A" );

        int expect_itemCount = 1;
        int actual_itemCount = testArray.m_itemCount;

        string expect_array0 = "A";
        string actual_array0 = testArray.m_array[0];

        if ( actual_itemCount != expect_itemCount )
        {
            cout << RED << " FAIL... itemCount was " << actual_itemCount << " instead!" << endl;
        }
        else if ( actual_array0 != expect_array0 )
        {
            cout << RED << " FAIL... m_array[0] was " << actual_array0 << " instead!" << endl;        
        }
        else
        {
            cout << GREEN << " PASS" << endl;
        }
        cout << CLEAR;
    } // End test scope

    // TODO: Implement more tests                                                                   // STARTER CODE
}

void FixedArrayTester::Test_FixedArray_Search()
{
    { // Begin test scope
        cout << "Add A B C to array. Search for B. Should return 1.";
        FixedArray<string> testArray;
        testArray.m_itemCount = 3;
        testArray.m_array[0] = "A";
        testArray.m_array[1] = "B";
        testArray.m_array[2] = "C";

        int expectOut = 1;
        int actualOut = testArray.Search("B");

        if ( actualOut == expectOut )
        {
            cout << GREEN << " PASS" << endl;
        }
        else
        {
            cout << RED << " FAIL... Was " << actualOut << " instead!" << endl;
        }
        cout << CLEAR;
    } // End test scope

    // TODO: Implement more tests                                                                   // STARTER CODE
}

