#ifndef _FIXED_ARRAY_HPP
#define _FIXED_ARRAY_HPP

#include <iostream>
using namespace std;

template <typename T>
//! A data structure that wraps a fixed array
class FixedArray
{
public:
    FixedArray();

    void PushBack( T newItem );
    void PopBack();
    T& GetBack();
    T& GetFront();
    T& GetAt( size_t index );

    size_t Search( T item ) const;
    size_t Size() const;
    bool IsEmpty() const;
    bool IsFull() const;
    void Display() const;
    void Clear();

private:
    T m_array[10];
    const size_t ARRAY_SIZE;
    size_t m_itemCount;

    friend class FixedArrayTester;
};

template <typename T>
FixedArray<T>::FixedArray()
: ARRAY_SIZE( 10 )
{
    // TODO: Implement
}

template <typename T>
void FixedArray<T>::Clear()
{
    // TODO: Implement me!
}

template <typename T>
size_t FixedArray<T>::Size() const
{
    return 0; // TODO: REMOVE THIS LINE!                                                              // STARTER CODE
    // TODO: Implement me!
}

template <typename T>
bool FixedArray<T>::IsFull() const
{
    return false; // TODO: REMOVE THIS LINE!                                                        // STARTER CODE
    // TODO: Implement me!
}

template <typename T>
bool FixedArray<T>::IsEmpty() const
{
    return false; // TODO: REMOVE THIS LINE!                                                       // STARTER CODE
    // TODO: Implement me!
}

template <typename T>
void FixedArray<T>::PushBack( T newItem )
{
    // TODO: Implement me!
}

template <typename T>
void FixedArray<T>::PopBack()
{
    // TODO: Implement me!
}

template <typename T>
T& FixedArray<T>::GetBack()
{
    return m_array[0]; // REMOVE ME
}

template <typename T>
T& FixedArray<T>::GetFront()
{
    return m_array[0]; // REMOVE ME                                             // SOLUTION
}

template <typename T>
T& FixedArray<T>::GetAt( size_t index )
{
    return m_array[0]; // REMOVE ME
}

template <typename T>
size_t FixedArray<T>::Search( T item ) const
{
    return 0; // REMOVE ME
}

template <typename T>
void FixedArray<T>::Display() const
{
    for ( size_t i = 0; i < m_itemCount; i++ )
    {
        cout << i << ". " << m_array[i] << endl;
    }
}

#endif
