#+HTML_HEAD: <style type='text/css'> body { font-family: sans-serif; } #content { width: 100%; max-width: 100%; } </style>
#+HTML_HEAD: <style type='text/css'> h3:before { content:"💾 "; } h3:after { content: ""; }  </style>
#+HTML_HEAD: <style type='text/css'> h3 { background: rgba( 0, 255, 150, 0.5 ); }  </style>
#+HTML_HEAD: <style type='text/css'> h4:before { content:"🟣 "; } h4:after { content: ""; }  </style>
#+HTML_HEAD: <style type='text/css'> code { background: rgba( 190, 111, 255, 0.2 ); padding: 2px; }  </style>
#+HTML_HEAD: <style type='text/css'> .src-terminal { background: #201826; color: #e5d3f4; font-weight: bold; } .src-terminal:before { content: "TERMINAL"; }  </style>
#+HTML_HEAD: <style type='text/css'> .src-form { background: #bddfff; color: #001528; font-weight: bold; } .src-form:before { content: "FORM"; }  </style>
#+HTML_HEAD: <style type='text/css'> .outline-1, .outline-2, .outline-3 { margin-left: 10px; padding: 10px; border: outset 2px #c0c0c0; background: rgba( 0, 255, 150, 0.1 ); margin-bottom: 10px; } </style>
#+HTML_HEAD: <style type='text/css'> .outline-3 { margin-bottom: 50px; } </style>

#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/tomorrow-night-blue.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script>
#+HTML_HEAD: <script>hljs.initHighlightingOnLoad();</script>

* Assignment and policy info

- Assignment info:
  - [[https://gitlab.com/moosadee/courses/-/blob/main/current/policies/practice-graded.org?ref_type=heads][Practice and Graded programs]]
  - [[https://gitlab.com/moosadee/courses/-/blob/main/current/policies/resubmit.org?ref_type=heads][Resubmission and regrading policy and procedure]]
  - [[https://gitlab.com/moosadee/courses/-/blob/main/current/policies/duedates.org?ref_type=heads][Due dates and available until dates]]
- How To:
  - [[https://gitlab.com/moosadee/courses/-/blob/main/reference/ide_vscode_build_and_run.org?ref_type=heads][Build and Run your program in VS Code]]
  - [[https://gitlab.com/moosadee/courses/-/blob/main/current/policies/turnin-lab.org?ref_type=heads][Turn in your lab]]
  - [[https://gitlab.com/moosadee/courses/-/blob/main/reference/vscode-git.org?ref_type=heads][Use Git and VS Code]]
- Quick reference:
  - [[https://gitlab.com/moosadee/courses/-/blob/main/current/share/read/CS200review.org?ref_type=heads][CS 200 code reference]]
  - [[https://gitlab.com/moosadee/courses/-/blob/main/reference/cpp_program_arguments.org?ref_type=heads][C++ program arguments]]
  - Debugging with [[https://gitlab.com/moosadee/courses/-/blob/main/reference/gdb.org?ref_type=heads][gdb]] / [[https://gitlab.com/moosadee/courses/-/blob/main/reference/lldb.org?ref_type=heads][lldb]]
  - [[https://gitlab.com/moosadee/courses/-/blob/main/reference/mac.org?ref_type=heads][Common issues on Mac]]
- Links:
  - [[https://moosadee.gitlab.io/courses/][R.W.'s courses homepage]]
  - [[https://gitlab.com/moosadee/courses/-/blob/main/current/share/student-repo/wk02_TestDebugFriendTemplate/instructions.org][Assignment direct link]]

--------------------------------------------------------------------------------

* Included files:

#+BEGIN_SRC
wk02_TestDebugFriendTemplate
├── graded_program
│   ├── FixedArray.h
│   ├── FixedArrayTester.cpp
│   ├── FixedArrayTester.h
│   └── main.cpp
├── instructions.org
├── practice1_testing
│   ├── Functions.cpp
│   ├── Functions.h
│   └── main.cpp
├── practice2_debugging
│   ├── crash.cpp
│   ├── debug-questions.txt
│   └── logicerror.cpp
├── practice3_friends
│   └── main.cpp
└── practice4_templates
    ├── Functions.h
    ├── Products.cpp
    ├── Products.h
    └── main.cpp
#+END_SRC

--------------------------------------------------------------------------------

* Practice programs

Within your repository folder in this week's folder you'll see a set of practice programs to work on.
Follow along with the instructions in this document.

** Practice 1 - Testing

Within the *Functions.h* file you'll see the following functions declared, as well as a "Test" function for each:
- =bool IsOverdrawn( float balance );=
- =float AdjustIngredient( float original, float batches );=
- =bool IsValidInput( int choice, int min, int max );=
- =float Average( vector<float> arr );=
- =int Factorial( int n );=

The functions themselves aren't implemented, and each of the test functions are incomplete. Only having /one test/ generally isn't enough to check all reasonable outcomes for each of these functions.
For example, if you had a "add X and Y" tester, only checking "2 + 3 = 5?" isn't enough, because someone might have programmed the function to just have =return 5;=.

*Before fixing the functions themselves, implement the tests.* This is part of *Test Driven Development:* tests come first, to help you verify that the to-be-implemented functionality works,
and once those tests are up and running (and probably failing), then you build out the function and use the tests to verify your work.

~

*** IsOverdrawn:

=bool IsOverdrawn( float balance )=

- Parameter: =float balance=, a bank account balance.
- Return: =bool=, true if overdrawn, false otherwise.

Here we need three test cases to get complete coverage: One for overdrawn, but also checking a positive value and zero (zero isn't overdrawn, just empty).
One test is given:

#+BEGIN_SRC cpp :class cpp
    {
        float input_balance = 10;
        bool  exp_out = false;
        bool  act_out = IsOverdrawn( input_balance );
        cout << "Test: IsOverdrawn(" << input_balance << ") = " << exp_out << "? ";
        if ( act_out == exp_out ) { cout << GREEN << " PASS" << endl; }
        else                      { cout << RED << " FAIL; got " << act_out << " instead!" << endl; }
        cout << CLEAR;
    }
#+END_SRC

Use this as reference and implement two additional tests...

| Input           | Expected output       |
|-----------------+-----------------------|
| 0               | false (not overdrawn) |
| NEGATIVE NUMBER | true (overdrawn)      |

*Build and run your program and run the tests. Some will fail:*

#+BEGIN_SRC bash
  $ g++ *.h *.cpp
  $ ./a.out test

  Test: IsOverdrawn(10) = 0?  PASS
  Test: IsOverdrawn(0) = 0?  PASS
  Test: IsOverdrawn(-5) = 1?  FAIL; got 0 instead!
#+END_SRC

*Finally, fix up the =IsOverdrawn= function to have the correct functionality, then build and run tests again:*

#+BEGIN_SRC bash
  $ g++ *.h *.cpp
  $ ./a.out test

  Test: IsOverdrawn(10) = 0?  PASS
  Test: IsOverdrawn(0) = 0?  PASS
  Test: IsOverdrawn(-5) = 1?  PASS
#+END_SRC

*You can also run the command directly in the command line:*

#+BEGIN_SRC bash
  $ ./a.out IsOverdrawn 10
  IsOverdrawn(10) = false

  $ ./a.out IsOverdrawn 0
  IsOverdrawn(0) = false

  $ ./a.out IsOverdrawn -5
  IsOverdrawn(-5) = true
#+END_SRC

~

*** AdjustIngredient

=float AdjustIngredient( float original, float batches )=

- Parameter: =float original=, the original amount for the ingredient, and =float batches=, the adjusted amount of batches to bake.
- Return: =float=, the adjusted amount.

Again the first test is given, you just need to implement at least one more.

| Input - original | Input - batches | Expected output |
|------------------+-----------------+-----------------|
| 5                | 1.5             | 7.5             |
| ?                | ?               | ?               |

Make your test to use different input values and check the output value. The calculation is /original * batches/, punch your numbers into a calculator and the number value is your result. Put this hard-coded number as your test expected output.

*Implement the test, then the function, and run the automated tests and also test manually using the AdjustIngredient command.*

#+BEGIN_SRC bash
  $ ./a.out test
  (...)
  Test: AdjustIngredient(5, 2) = 10?  PASS
  Test: AdjustIngredient(7, 0.5) = 3.5?  PASS
  (...)

  $ ./a.out AdjustIngredient 5 1.5
  AdjustIngredient(5, 1.5) = 7.5

  $ ./a.out AdjustIngredient 7 0.5
  AdjustIngredient(7, 0.5) = 3.5
#+END_SRC

~

*** IsValidInput

=bool IsValidInput( int choice, int min, int max )=

- Parameter: =int choice= the user's choice. =int min= the minimum valid value (inclusive). =int max= the maximum valid value (inclusive).
- Return: =true= if choice is within min and max, or =false= otherwise.

| Input - choice | Input - min | Input - max | Expected output |
|----------------+-------------+-------------+-----------------|
|              5 |           1 |           5 | true            |
|              1 |           2 |           6 | false           |

With tests like these, you should make sure to have enough test cases to make sure that the "is equal to" case is also met... so if the minimum is 1 and the choice is 1, make sure that returns true.

(There's a difference between /1 <= x <= 5/ and /1 < x < 5/ !)

- NOTE: To test two separate boolean expressions together, use AND =&&= or OR =||=. Each sub-expression should be complete.
  - DOESN'T WORK: if ( 1 <= x <= 5 )
  - DOESN'T WORK: if ( x < 1 || > 5 )
  - OK: if ( x < 1 || x > 5 )         ...(Not the solution)

#+BEGIN_SRC bash
  $ ./a.out test
  (...)
  Test: IsValidInput(5, 1, 5) = 1?  PASS
  Test: IsValidInput(1, 1, 5) = 1?  PASS
  Test: IsValidInput(1, 2, 6) = 0?  PASS
  Test: IsValidInput(7, 2, 6) = 0?  PASS
  (...)

  $ ./a.out IsValidInput 5 1 10
  IsValidInput(5, 1, 10) = true

  $ ./a.out IsValidInput 10 1 5
  IsValidInput(10, 1, 5) = false
#+END_SRC

~

*** Average

=float Average( vector<float> arr )=

- Parameter: =vector<float> arr=, a dynamic array of floats.
- Return: the average - all elements of the =arr= summed together, then divided by =arr.size()=.

| Input - arr   | Expected output |
|---------------+-----------------|
| 1.5, 2.5, 3.5 |             2.5 |
| 5, 3, 6, 7    |            5.25 |

For a good test, not only use different values for the array elements, but also use different /amounts/ of elements. This way we can ensure that the programmer (pretending it's someone else :) didn't hard-code the array size into their implementation!

#+BEGIN_SRC bash
  $ ./a.out test
  (...)
  Test: Average({ 1.5, 2.5, 3.5 }) = 2.5?  PASS
  Test: Average({ 5, 3, 6, 7 }) = 5.25?  PASS
  (...)

  $ ./a.out Average 1 3 5 7
  Average({ 1, 3, 5, 7 }) = 4

  $ ./a.out Average 3.0 4.0 3.5 2.5
  Average({ 3, 4, 3.5, 2.5 }) = 3.25
#+END_SRC

~

*** Factorial

=int Factorial( int n )=

- Parameter: =int n=, the n value.
- Return: The result of /n!/

| Input - n | Expected output |
|-----------+-----------------|
|         0 |               1 |
|         1 |               1 |
|         2 |               2 |
|         3 |               6 |
|         4 |              24 |

Besides having two tests for something like /3!/ and /4!/, make sure to also check for special cases, like /0!/ being 1!

#+BEGIN_SRC bash
  $ ./a.out test
  (...)
  Test: Factorial(3) = 6?  PASS
  Test: Factorial(5) = 120?  PASS
  Test: Factorial(0) = 1?  PASS
  Test: Factorial(1) = 1?  PASS
  (...)

  $ ./a.out Factorial 5
  Factorial(5) = 120

  $ ./a.out Factorial 7
  Factorial(7) = 5040
#+END_SRC

~

*** Example output: No arguments given:

#+BEGIN_SRC bash
$ ./a.out

Expected form:
./a.out test - run all tests
./a.out IsOverdrawn amount - check if amount given is overdrawn
./a.out AdjustIngredient original batches - calculate adjusted ingredient amount
./a.out IsValidInput choice min max - check if choice is within the valid range
./a.out Factorial n - calculate n!
./a.out Average num1 num2 num3 num4... - Calculate the average of all numbers given
#+END_SRC

*** Example output: Running the automated tests (starter code):

#+BEGIN_SRC bash
  $ ./a.out test

  Test: IsOverdrawn(10) = 0?  PASS
  Test: AdjustIngredient(5, 2) = 10?  FAIL; got 0 instead!
  Test: IsValidInput(5, 1, 5) = 1?  FAIL; got 0 instead!
  Test: Average({ 1.5, 2.5, 3.5 }) = 2.5?  FAIL; got 0 instead!
  Test: Factorial(3) = 6?  FAIL; got 0 instead!
#+END_SRC

*** Example output: Running the automated tests (functions fixed, tests implemented):

Your tests might have different test values!

#+BEGIN_SRC bash
$ ./a.out test

Test: IsOverdrawn(10) = 0?  PASS
Test: IsOverdrawn(0) = 0?  PASS
Test: IsOverdrawn(-5) = 1?  PASS
Test: AdjustIngredient(5, 2) = 10?  PASS
Test: AdjustIngredient(7, 0.5) = 3.5?  PASS
Test: IsValidInput(5, 1, 5) = 1?  PASS
Test: IsValidInput(1, 1, 5) = 1?  PASS
Test: IsValidInput(1, 2, 6) = 0?  PASS
Test: IsValidInput(7, 2, 6) = 0?  PASS
Test: Average({ 1.5, 2.5, 3.5 }) = 2.5?  PASS
Test: Average({ 5, 3, 6, 7 }) = 5.25?  PASS
Test: Factorial(3) = 6?  PASS
Test: Factorial(5) = 120?  PASS
Test: Factorial(0) = 1?  PASS
Test: Factorial(1) = 1?  PASS
#+END_SRC

--------------------------------------------------------------------------------

** Practice 2 - Debugging

Within the =practice2_debugging= folder there are two separate programs. We will see how to use the =gdb= (GNU Debugger) in order to locate errors.
(You can also view the reference page here: [[https://gitlab.com/moosadee/courses/-/blob/main/reference/gdb.org?ref_type=heads]].)

Fill out the =debug-questions.txt= file as you go. You do not need to update the source code files!

~

*** Using the backtrace with crash.cpp

- Build the crash.cpp file using =-g= to enable debug symbols:
  #+BEGIN_SRC bash
  g++ -g crash.cpp -o crash.exe
  #+END_SRC

- Windows/Linux: Then load it into gdb:
  #+BEGIN_SRC bash
  gdb crash.exe
  #+END_SRC
- Mac/Linux: Or load it into lldb:
  #+BEGIN_SRC bash
  lldb crash.exe
  #+END_SRC

- Use the =run= command to begin the program execution. It will go until it crashes.
  - GDB view:
    #+BEGIN_SRC bash
  Dereference pointers! What could POSSIBLY go wrong...?
  0. A
  1. B
  2. C

  Program received signal SIGSEGV, Segmentation fault.
    #+END_SRC
  - LLDB view:
    #+BEGIN_SRC bash
      Process 76841 stopped
    * thread #1, name = 'crash.exe', stop reason = signal SIGSEGV: invalid address (fault address: 0x8)
    frame #0: 0x00007ffff7ebd4c4 libstdc++.so.6`std::basic_ostream<char, std::char_traits<char> >& std::operator<<<char, std::char_traits<char>, std::allocator<char> >(std::basic_ostream<char, std::char_traits<char> >&, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const&) + 4
libstdc++.so.6`std::operator<<<char, std::char_traits<char>, std::allocator<char> >:
->  0x7ffff7ebd4c4 <+4>:  movq   0x8(%rsi), %rdx
    0x7ffff7ebd4c8 <+8>:  movq   (%rsi), %rsi
    0x7ffff7ebd4cb <+11>: jmp    0x7ffff7e0e9f0            ; ___lldb_unnamed_symbol7215 + 9984

libstdc++.so.6`std::getline<char, std::char_traits<char>, std::allocator<char> >:
    0x7ffff7ebd4d0 <+0>:  endbr64 
    #+END_SRC

- Use the =bt= command to view the backtrace. Paste the backtrace into the =debug-questions.txt= document. Identify which function was being executed when the crash happened, as well as which file and line number.
  - GDB Backtrace:
    #+BEGIN_SRC bash
      #0  0x00007ffff7ebd4c4 in std::basic_ostream<char, std::char_traits<char> >& std::operator<< <char, std::char_traits<char>, std::allocator<char> >(std::basic_ostream<char, std::char_traits<char> >&, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const&) ()
      from /lib/x86_64-linux-gnu/libstdc++.so.6
      #1  0x000055555555654f in DisplayValue (ptr=0x0) at crash.cpp:8
      #2  0x00005555555565c7 in DisplayAll (ptrs=std::vector of length 4, capacity 4 = {...}) at crash.cpp:16
      #3  0x0000555555556790 in main () at crash.cpp:25
    #+END_SRC
  - LLDB Backtrace:
    #+BEGIN_SRC bash
      frame #1: 0x000055555555654f crash.exe`DisplayValue(ptr=<parent failed to evaluate: parent is NULL>) at crash.cpp:8:14
      frame #2: 0x00005555555565c7 crash.exe`DisplayAll(ptrs=size=1) at crash.cpp:16:21
      frame #3: 0x0000555555556790 crash.exe`main at crash.cpp:25:15
      frame #4: 0x00007ffff7b4ed90 libc.so.6`__libc_start_call_main(main=(crash.exe`main at crash.cpp:21:1), argc=1, argv=0x00007fffffffdee8) at libc_start_call_main.h:58:16
      frame #5: 0x00007ffff7b4ee40 libc.so.6`__libc_start_main_impl(main=(crash.exe`main at crash.cpp:21:1), argc=1, argv=0x00007fffffffdee8, init=0x00007ffff7ffd040, fini=<unavailable>, rtld_fini=<unavailable>, stack_end=0x00007fffffffded8) at libc-start.c:392:3
      frame #6: 0x0000555555556465 crash.exe`_start + 37
    #+END_SRC

~
*** Using breakpoints in logicerror.cpp

Build the logicerror.cpp program with debug symbols:

#+BEGIN_SRC bash
  g++ -g logicerror.cpp -o logic.exe
#+END_SRC

- Windows/Linux: Then load it into gdb:
  #+BEGIN_SRC bash
  gdb logic.exe
  #+END_SRC
- Mac/Linux: Or load it into lldb:
  #+BEGIN_SRC bash
  lldb logic.exe
  #+END_SRC

- Set up a breakpoint to start at the =main()= function.
  - GDB: =break main=
  - LLDB: =breakpoint set --name main=
- Begin running the program. For this program you have to require an argument, like a zip code: 66047.
  - GDB: =run 66047=
  - LLDB: =run 66047=

- Use =n= to step to the next line of code, and =s= to step INTO a function call (do this at =string city = GetCity( zipcode );=)

- You can also use =print VARNAME= to view the current value of a variable.

- GDB stepthrough:
  #+BEGIN_SRC bash
    (gdb) start 66044
    Temporary breakpoint 1 at 0x26d7: file logicerror.cpp, line 27.
    Starting program: /(...)/practice2_debugging/logic.exe 66044
    [Thread debugging using libthread_db enabled]
    Using host libthread_db library "/lib/x86_64-linux-gnu/libthread_db.so.1".

    Temporary breakpoint 1, main (argCount=2, args=0x7fffffffdb48) at logicerror.cpp:27
    27      {
	(gdb) n
	28          if ( argCount != 2 )
	(gdb) n
	34          int zipcode = stoi( args[1] );
	(gdb) n
	35          string city = GetCity( zipcode );
	(gdb) s
	GetCity[abi:cxx11](int) (zipcode=66044) at logicerror.cpp:7
	7       {
	    (gdb) n
	    8           if ( zipcode == 66002 )
	    (gdb) n
	    12          else if ( zipcode == 66044 || zipcode == 66045 || zipcode == 66046 || zipcode == 66077 )
	    (gdb) n
	    14              return "Lawrence";
	    (gdb) n
	    24      }
	(gdb) n
	main (argCount=2, args=0x7fffffffdb48) at logicerror.cpp:36
	36          cout << endl
    (gdb) n

    37               << "Zipcode: " << zipcode
    (gdb) n
    38               << ", city: " << city << endl << endl;
    (gdb) print zipcode
    $1 = 66044
    (gdb) print city
    $2 = "Lawrence"
    (gdb) n
    Zipcode: 66044, city: Lawrence

    40          return 0;
    (gdb) n
    41      }
  #+END_SRC

- LLDB Stepthrough:
  #+BEGIN_SRC bash
	Process 77292 stopped
	,* thread #1, name = 'logic.exe', stop reason = step over
	frame #0: 0x00005555555567cc logic.exe`main(argCount=2, args=0x00007fffffffdee8) at logicerror.cpp:36:13
	33  	
	34  	    int zipcode = stoi( args[1] );
	35  	    string city = GetCity( zipcode );
	-> 36  	    cout << endl
        37     	         << "Zipcode: " << zipcode
        38  		 	         << ", city: " << city << endl << endl;
  #+END_SRC

After stepping through the program once, start it again with the zipcode 66047. This should return "Lawrence", but it doesn't.
Step through the program to locate what instead gets returned, and identify which line of code has the logic error.
Type your answers in the =debug-questions.txt= file.

--------------------------------------------------------------------------------

** Practice 3 - Friends

This one is small - at the top of main.cpp there is a =Pet= class declared with a constructor function to set values of the object's /private member variables/. Besides that, there are no *accessor* functions to retrieve the private member data.

Within the class declaration, declare that a new function (which we will define in a moment) is a =friend=:

#+BEGIN_SRC cpp :class cpp
  friend void Display( const Pet& pet );
#+END_SRC

Below the function declaration, use this same function header to define the function. This function is a *friend* of the Pet class, so it will have access to the *Pet* class member variables.

Within the Display function, write a =cout= statement that displays the pet's name, animal, personality, and age.

*** Example output

#+BEGIN_SRC bash
  $ a.exe
  Kabe the Cat (Sleepy), 11 years old.
  Luna the Cat (Troublemaker), 8 years old.
  Daisy the Dog (Grumpy), 14 years old.
#+END_SRC

--------------------------------------------------------------------------------

** Practice 4 - Templates

Within the Products.h file there are three structs declared: =FoodProduct=, =SongProduct=, and =BookProduct=. They have some variables in common, but other variables that are not shared:

|             | name | price | quantity | calories | minutes | author | year |
|-------------+------+-------+----------+----------+---------+--------+------|
| FoodProduct | y    | y     | y        | y        |         |        |      |
| SongProduct | y    | y     | y        |          | y       |        |      |
| BookProduct | y    | y     | y        |          |         | y      | y    |

Each struct also contains *Constructors*, a =Setup= function, and a =Display= function.

Within *Functions.h* you'll implement two templated functions:

~

*** float TotalValue( vector<T>& arr )

This function will iterate through all of the elements of =arr= and add the worth of each item (=price=) multiplied by the amount of that item in stock (=quantity=). At the end of the function return the sum.

~

*** void Display( const vector<T>& arr )

Within this function, use a loop to iterate over all of the elements of =arr= and call each item's =Display()= function.

~

*** Updates to =main()=

Within main, do the following:

1. Create vectors and initialize data:
   - Create a vector of FoodProduct, a vector of SongProduct, and a vector of BookProduct.
   - Initialize each one with at least 2 products each.
2. Call the =TotalValue= function on each of your 3 vectors, displaying the result for each one.
3. After the table header given later in =main()=, call the =Display= function on each of the three vectors.

~

*** Example output

Once done your output should look something like this:

#+BEGIN_SRC bash
  $ a.exe
  Total food value: $61.75
  Total song value: $16.43
  Total book value: $175.68

  NAME                PRICE     QUANTITY  *CALORIES *MINUTES  *AUTHOR   *YEAR
  --------------------------------------------------------------------------------
  burrito             2.49      10        200       -         -         -
  burger              4.99      5         400       -         -         -
  banana              1.19      10        10        -         -         -
  wonderwall          1.99      2         -         3         -         -
  freebird            2.49      5         -         9         -         -
  xenogenesis         15.99     5         -         -         butler    1987
  sabriel             7.99      7         -         -         nix       1995
  frankenstein        1.99      20        -         -         shelley   1818
#+END_SRC

--------------------------------------------------------------------------------

* Graded programs
** Graded program - Fixed Array structure
*** Getting started
The graded program contains the following:

- *FixedArray.h* - Where the templated fixed array structure is created.
- *FixedArrayTester.h* - A tester class and its functions are declared here.
- *FixedArrayTester.cpp* - The tester functions are defined here.
- *main.cpp* - Program starting point.

You can build the program with debug symbols like this:

#+BEGIN_SRC bash
  g++ -g *.h *.cpp -o arr.exe
#+END_SRC

When you run the program, you can either run with the tester:

#+BEGIN_SRC bash
  ./arr.exe test
  FixedArrayTester - RunAll
  (etc)
#+END_SRC

Or don't include any arguments to get the basic program to run:

#+BEGIN_SRC bash
  ./arr.exe
  Display array:
  0.
  1.
  2.
  3.
  4.
  5.
  6.
  7.
  8.
  9.
  10. Segmentation fault
#+END_SRC

At the moment it crashes. Once you implement the core functionality, /well written code/ will avoid crashes... however, I've hidden two errors within =main()=. When you find the erroneous code, comment out that line.

The best way to approach this assignment is to:
1. Read through the function behaviors (in this document).
2. Implement the *tests* in *FixedArrayTester.cpp*. Build and run the tests to check that most of them fail.
3. Implement the *functions* in *FixedArray.h*. Build and run the tests to check that their implementation is correct.
4. Use gdb to run the main program, step through to find what lines of code are causing the program to crash. Comment out the bad lines.


- [[https://jccc.yuja.com/v/cs235-week2lab-fixedarray][🎥 Introduction to Graded Program (video)]]

~

*** FixedArray functionality

- *FixedArray() - constructor* ::
  - *Inputs:* None
  - *Expected result:*
    - =ARRAY_SIZE= should be initialized to 10.
    - =m_itemCount= should be initialized to 0.

  - *Test case:*
    | Action               | Expected result         |
    |----------------------+-------------------------|
    | FixedArray<int> test | test =m_itemCount= is 0 |
    |                      | test =ARRAY_SIZE= is 10 |

Only one test is required for this function since there is no input and every call to the constructor will result in the same thing.

~

- *=size_t= Size() const;* ::
  - *Inputs:* The member variable =m_itemCount= may have different values during the lifetime of FixedArray.
  - *Expected result:*
    - The Size function should return whatever the current amount of items in the array is - this is represented with =m_itemCount=.

  - *Example tests cases:*
    | State          | Expected result   |
    |----------------+-------------------|
    | itemCount is 5 | Size() returns 5  |
    | itemCount is 2 | Size() returns 2  |

~

- *void Clear();* ::
  - *Inputs:* None
  - *Expected result:*
    - After call, the array is considered "empty"; we use lazy deletion by setting =m_itemCount= to 0, which ensures that elements will be erased next time we insert new items.

  - *Example tests cases:*
    | State          | Function call | Expected result |
    |----------------+---------------+-----------------|
    | itemCount is 5 | Clear()       | itemCount is 0  |
    | itemCount is 2 | Clear()       | itemCount is 0  |

~

- *bool IsEmpty() const;* ::
  - *Inputs:* None
  - *Expected result:* Returns =true= if the array is empty, or =false= otherwise.

  - *Example tests cases:*
    | State          | Expected result         |
    |----------------+-------------------------|
    | itemCount is 5 | IsEmpty() returns false |
    | itemCount is 0 | IsEmpty() returns true  |

~

- *bool IsFull() const;* ::
  - *Inputs:* None
  - *Expected result:* Returns =true= if the array is full, or =false= otherwise.

  - *Example tests cases:*
    | State                  | Expected result        |
    |------------------------+------------------------|
    | itemCount is 0         | IsFull() returns false |
    | itemCount is 5         | IsFull() returns false |
    | itemCount is ARRAYSIZE | IsFull() returns true  |

~

- *void PushBack( T newItem )* ::
  - *Inputs:* A new item to store in the array.
  - *Expected result:* If there is space in the array, the new item will be added at the next available space.

  - *Example tests cases:*
    | State            | Function call   | Expected result                                                |
    |------------------+-----------------+----------------------------------------------------------------|
    | array is empty   | PushBack( "A" ) | array[0] is "A", itemCount is 1                                |
    | array has 1 item | PushBack( "B" ) | array[1] is "B", itemCount is 2                                |
    | array is full    | PushBack( "X" ) | *TEMPORARY:* cout error and return early; (later: exceptions!) |

~

- *void PopBack()* ::
  - *Inputs:* None
  - *Expected result:* The item at the "end" of the list is lazy deleted.

  - *Example tests cases:*
    | State             | Function call | Expected result                                                |
    |-------------------+---------------+----------------------------------------------------------------|
    | array has 2 items | PopBack()     | itemCount is 1                                                 |
    | array has 1 item  | PopBack()     | itemCount is 0                                                 |
    | array is empty    | PopBack()     | *TEMPORARY:* cout error and return early; (later: exceptions!) |

~

- *T& GetBack()* ::
  - *Inputs:* None
  - *Expected result:* The item at the "end" of the list is returned.

  - *Example tests cases:*
    | State             | Function call | Expected result                                                            |
    |-------------------+---------------+----------------------------------------------------------------------------|
    | array has 3 items | GetBack()     | arr[2] is returned                                                         |
    | array has 2 items | GetBack()     | arr[1] is returned                                                         |
    | array is empty    | GetBack()     | *TEMPORARY:* cout error, return =m_array[0]= for now. (later: exceptions!) |

~

- *T& GetFront()* ::
  - *Inputs:* None
  - *Expected result:* Returns the item at the "front" of the list.

  - *Example tests cases:*
    | State             | Function call | Expected result                                                            |
    |-------------------+---------------+----------------------------------------------------------------------------|
    | array has 3 items | GetFront()    | arr[0] is returned                                                         |
    | array has 2 items | GetFront()    | arr[0] is returned                                                         |
    | array is empty    | GetBack()     | *TEMPORARY:* cout error, return =m_array[0]= for now. (later: exceptions!) |

~

- *T& GetAt( =size_t= index )* ::
  - *Inputs:* The index of an item in the array.
  - *Expected result:* The item at that index.

  - *Example tests cases:*
    | State             | Function call | Expected result                                                            |
    |-------------------+---------------+----------------------------------------------------------------------------|
    | array has 3 items | GetAt( 1 )    | arr[1] is returned                                                         |
    | array has 4 items | GetAt( 5 )    | *TEMPORARY:* cout error, return =m_array[0]= for now. (later: exceptions!) |
    | array is empty    | GetBack()     | *TEMPORARY:* cout error, return =m_array[0]= for now. (later: exceptions!) |

~

- *=size_t= Search( T item ) const* ::
  - *Inputs:* An item to look for in the array.
  - *Expected result:* Returns the index where the item was found.

  - *Example tests cases:*
    | State                        | Function call | Expected result                                                 |
    |------------------------------+---------------+-----------------------------------------------------------------|
    | array has "A", "B", "C"      | Search( "B" ) | 1 is returned                                                   |
    | array has "W", "X", "Y", "Z" | Search( "Z" ) | 3 is returned                                                   |
    | array has "C", "A", "T"      | Search( "S" ) | *TEMPORARY:* cout error, return 0 for now. (later: exceptions!) |


~

*** Implementing the tests

You should begin by implementing the tests within *FixedArrayTester.cpp*. This will help you understand the functionality of the FixedArray, and give you a way to verify your work.

Keep an eye on the FixedArray class declaration so you know what functions and variables are part of it. Its private member variables are:

#+BEGIN_SRC cpp :class cpp
    T m_array[10];
    const size_t ARRAY_SIZE;
    size_t m_itemCount;
#+END_SRC

And I'll step through the functionality and suggested test cases below.


~

*** Implementing the FixedArray functionality

Now implement the functions in *FixedArray.h*. Remember that this is a templated class, so all of its declarations /and/ definitions must go in the .h file.

~

*** Discovering the errors

You can use the =gdb= program to look at the *backtrace* of functions called and line number where a crash occurs, and use *breakpoints* to pause the program execution and investigate program flow and variable values.

(You can also view the reference page here: [[https://gitlab.com/moosadee/courses/-/blob/main/reference/gdb.org?ref_type=heads]].)

Once you find the lines of code in =main()= causing the errors, comment those lines out.

**** gdb quick reference:

- From the terminal:
  - =g++ -g FILE.cpp -o PROGRAMNAME= - Build a program with debug symbols.
  - =gdb PROGRAMNAME ARG1 ARG2 ARG3= - Run the program through gdb, including any arguments.
- Within gdb:
  - =run= - Run a program normally
  - =start= - Start the program, pausing at the first executed line.
  - =print VARNAME= - Prints out the value of a variable in scope at the breakpoint.
  - =next= or =n= - Move to the next line of code.
  - =step= or =s= - Step INTO a function call.
  - =bt= - View the backtrace of functions called.
  - =list= will show you the program code from within gdb.
  - =file ./NEWPROGRAM= - Load in a new program's symbols (while in gdb).

~

*** Output

Once you've implemented the tests and the FixedArray functionality, all tests should pass.
At minimum, I've provided a few tests, so here's a look at them passing:

#+BEGIN_SRC bash
  $ .\a.exe test

  FixedArrayTester - RunAll
  Set m_itemCount to 5, Size() should return 5... PASS
  Set m_itemCount to 5, IsEmpty() should return false... PASS
  Set m_itemCount to 2, call PopBack(). m_itemCount should now be 1... PASS
  Put 10, 20, 30 in array. Call GetBack(). Should return 30... PASS
  Put 10, 20, 30 in array. Call GetFront(). Should return 10... PASS
  Put 10, 20, 30 in array. Call GetAt(1). Should return 20... PASS
  Create empty array. Call PushBack(A). itemCount should be 1, m_array[0] should be A. PASS
  Add A B C to array. Search for B. Should return 1. PASS
#+END_SRC

But you should also implement additional tests to check more cases. And assuming your input/output logic is right, the tests /should/ pass. :)

~

After the tests pass, run the main program itself.

#+BEGIN_SRC bash
  $ g++ -g *.h *.cpp -o arr.exe
  $ gdb arr.exe
#+END_SRC

Utilize the debugger to find the lines of code in =main()= that are causing the program to crash.

I'm not going to show you the full working program because that would spoil the bugs ;)

#+BEGIN_SRC bash
    $ .\a.exe
    Display array:
    0. CS 134
    1. CS 200
    2. CS 235
    3. CS 250

    Back:  CS 250
    (and more...)
#+END_SRC

Once you've commented out the bugs, you should be good to backup your changes to the GitLab server and turn in your work!
