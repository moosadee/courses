#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <string>
using namespace std;

#include "Functions.h"
#include "Products.h"

int main()
{
    cout << left << fixed << setprecision( 2 ); // Sets up formatting

    // TODO: Create a vector of FoodProduct, initialize with at least 2 FoodProducts
    // TODO: Create a vector of SongProduct, initialize with at least 2 SongProduct
    // TODO: Create a vector of BookProduct, initialize with at least 2 BookProduct
    

    // TODO: Call TotalValue for foods, display the result.
    // TODO: Call TotalValue for songs, display the result.
    // TODO: Call TotalValue for books, display the result.


    // Table header
    cout << endl;
    cout
        << setw( 20 ) << "NAME"
        << setw( 10 ) << "PRICE"
        << setw( 10 ) << "QUANTITY"
        << setw( 10 ) << "*CALORIES"
        << setw( 10 ) << "*MINUTES"
        << setw( 10 ) << "*AUTHOR"
        << setw( 10 ) << "*YEAR"
        << endl << string( 80, '-' ) << endl;

    // TODO: Call Display on foods.
    // TODO: Call Display on songs.
    // TODO: Call Display on books.

    
    return 0;
}
