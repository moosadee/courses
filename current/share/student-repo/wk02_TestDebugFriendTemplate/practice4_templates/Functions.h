#ifndef _UTILITIES
#define _UTILITIES
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
using namespace std;

// TODO: Implement TotalValue templated function
template <typename T>
float TotalValue( vector<T>& arr )
{
    float sum = 0;
    for ( size_t i = 0; i < arr.size(); i++ )                         // SOLUTION
    {                                                                 // SOLUTION
        sum += arr[i].quantity * arr[i].price;                        // SOLUTION
    }                                                                 // SOLUTION
    return sum;
}

// TODO: Implement Display templated function
template <typename T>
void Display( const vector<T>& arr )
{
    for ( const T item : arr )                                        // SOLUTION
    {                                                                 // SOLUTION
        item.Display();                                                       // SOLUTION
    }                                                                 // SOLUTION
}

#endif
