// !! Don't need to change this file !!
#include "Products.h"

#include <iostream>
#include <iomanip>
using namespace std;

FoodProduct::FoodProduct()
{
    name = "UNSET";
}

FoodProduct::FoodProduct( string name, float price, int calories, int quantity )
{
    Setup( name, price, calories, quantity );
}

void FoodProduct::Setup( string name, float price, int calories, int quantity )
{
    this->name = name;
    this->price = price;
    this->calories = calories;
    this->quantity = quantity;
}

void FoodProduct::Display() const
{
    cout
        << setw( 20 ) << name
        << setw( 10 ) << price
        << setw( 10 ) << quantity
        << setw( 10 ) << calories
        << setw( 10 ) << "-"
        << setw( 10 ) << "-"
        << setw( 10 ) << "-"
        << endl;
}

SongProduct::SongProduct()
{
    name = "UNSET";
}

SongProduct::SongProduct( string name, float price, int minutes, int quantity )
{
    Setup( name, price, minutes, quantity );
}

void SongProduct::Setup( string name, float price, int minutes, int quantity )
{
    this->name = name;
    this->price = price;
    this->minutes = minutes;
    this->quantity = quantity;
}

void SongProduct::Display() const
{
    cout
        << setw( 20 ) << name
        << setw( 10 ) << price
        << setw( 10 ) << quantity
        << setw( 10 ) << "-"
        << setw( 10 ) << minutes
        << setw( 10 ) << "-"
        << setw( 10 ) << "-"
        << endl;
}

BookProduct::BookProduct()
{
    name = "UNSET";
}

BookProduct::BookProduct( string name, string author, float price, int year, int quantity )
{
    Setup( name, author, price, year, quantity );
}

void BookProduct::Setup( string name, string author, float price, int year, int quantity )
{
    this->name = name;
    this->author = author;
    this->price = price;
    this->year = year;
    this->quantity = quantity;
}

void BookProduct::Display() const
{
    cout
        << setw( 20 ) << name
        << setw( 10 ) << price
        << setw( 10 ) << quantity
        << setw( 10 ) << "-"
        << setw( 10 ) << "-"
        << setw( 10 ) << author
        << setw( 10 ) << year
        << endl;
}
    
