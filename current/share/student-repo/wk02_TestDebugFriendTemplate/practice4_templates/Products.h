// !! Don't need to change this file !!
#ifndef _PRODUCTS
#define _PRODUCTS

#include <string>
using namespace std;

struct FoodProduct
{
    FoodProduct();
    FoodProduct( string name, float price, int calories, int quantity );
    void Setup( string name, float price, int calories, int quantity );
    void Display() const;
    
    string name;
    float price;
    int calories;
    int quantity;
};

struct SongProduct
{
    SongProduct();
    SongProduct( string name, float price, int minutes, int quantity );
    void Setup( string name, float price, int minutes, int quantity );
    void Display() const;
    
    string name;
    float price;
    int minutes;
    int quantity;
};

struct BookProduct
{
    BookProduct();
    BookProduct( string name, string author, float price, int year, int quantity );
    void Setup( string name, string author, float price, int year, int quantity );
    void Display() const;
    
    string name;
    string author;
    float price;
    int year;
    int quantity;
};

#endif
