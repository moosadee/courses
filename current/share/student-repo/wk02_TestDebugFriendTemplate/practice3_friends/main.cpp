#include <iostream>
#include <string>
using namespace std;

class Pet
{
public:
    Pet( string name, string animal, string personality, int age )
    {
	m_name = name;
	m_animal = animal;
	m_personality = personality;
	m_age = age;
    }
    
private:
    string m_name;
    string m_animal;
    string m_personality;
    int m_age;

    // TODO: Identify the Display function as a friend function
};

// TODO: Implement Display function

int main()
{
    Pet pet1( "Kabe", "Cat", "Sleepy", 11 );
    Pet pet2( "Luna", "Cat", "Troublemaker", 8 );
    Pet pet3( "Daisy", "Dog", "Grumpy", 14 );

    //Display( pet1 );
    //Display( pet2 );
    //Display( pet3 );
    
    return 0;
}
