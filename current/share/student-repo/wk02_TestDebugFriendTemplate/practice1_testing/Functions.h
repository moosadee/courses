// !! Don't need to change this file !!
#ifndef _FUNCTIONS
#define _FUNCTIONS

#include <vector>
#include <string>
using namespace std;

const string CLEAR    = "\033[0m";
const string RED      = "\033[3;31m";
const string GREEN    = "\033[3;32m";

bool IsOverdrawn( float balance );
void Test_IsOverdrawn();

float AdjustIngredient( float original, float batches );
void Test_AdjustIngredient();

bool IsValidInput( int choice, int min, int max );
void Test_IsValidInput();

float Average( vector<float> arr );
void Test_Average();

int Factorial( int n );
void Test_Factorial();

// Helper function
string ToLower( string original );

template <typename T>
ostream& operator<<( ostream& out, const vector<T>& vec )
{
    out << string( "{ " );
    for ( size_t i = 0; i < vec.size(); i++ )
    {
        if ( i != 0 ) { out << string( ", " ); }
        out << vec[i];
    }
    out << string( " }" );
    return out;
}

#endif
