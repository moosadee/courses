// !! Don't need to change this file !!
#include <iostream>
using namespace std;

#include "Functions.h"

int main( int argCount, char* args[] )
{
    cout << endl;
    string command = "";
    if ( argCount > 1 )
    {
        command = ToLower( string( args[1] ) );
    }

    if ( argCount == 2 && command == "test" )
    {
        // Run tests
        Test_IsOverdrawn();
        Test_AdjustIngredient();
        Test_IsValidInput();
        Test_Average();
        Test_Factorial();
    }
    else if ( argCount == 3 && command == "isoverdrawn" )
    {
        float amount = stof( args[2] );
        string result = ( IsOverdrawn( amount ) ) ? "true" : "false";
        cout << "IsOverdrawn(" << amount << ") = " << result << endl;
    }
    else if ( argCount == 4 && command == "adjustingredient" )
    {
        float original = stof( args[2] );
        float batches = stof( args[3] );
        float result = AdjustIngredient( original, batches );
        cout << "AdjustIngredient(" << original << ", " << batches << ") = " << result << endl;
    }
    else if ( argCount == 5 && command == "isvalidinput" )
    {
        int choice = stoi( args[2] );
        int min = stoi( args[3] );
        int max = stoi( args[4] );
        string result = ( IsValidInput( choice, min, max ) ) ? "true" : "false";
        cout << "IsValidInput(" << choice << ", " << min << ", " << max << ") = " << result << endl;
    }
    else if ( argCount == 3 && command == "factorial" )
    {
        int n = stoi( args[2] );
        int result = Factorial( n );
        cout << "Factorial(" << n << ") = " << result << endl;
    }
    else if ( argCount > 2 && command == "average" )
    {
        vector<float> values;
        for ( int i = 2; i < argCount; i++ )
        {
            values.push_back( stof( args[i] ) );
        }
        float result = Average( values );
        cout << "Average(" << values << ") = " << result << endl;
    }
    else
    {
        cout << "Expected form:" << endl;
        cout << args[0] << " test - run all tests" << endl;
        cout << args[0] << " IsOverdrawn amount - check if amount given is overdrawn" << endl;
        cout << args[0] << " AdjustIngredient original batches - calculate adjusted ingredient amount" << endl;
        cout << args[0] << " IsValidInput choice min max - check if choice is within the valid range" << endl;
        cout << args[0] << " Factorial n - calculate n!" << endl;
        cout << args[0] << " Average num1 num2 num3 num4... - Calculate the average of all numbers given" << endl;
        cout << endl;
        return 1;
    }

    cout << endl;

    return 0;
}
