#include "Functions.h"

#include <iostream>
using namespace std;

/**
   @param balance the bank account current balance
   @return true if overdrawn, false otherwise
   (Overdrawn if balance is NEGATIVE.)
*/
bool IsOverdrawn( float balance )
{
    return false; // TODO: REMOVE THIS LINE!                                                        // STARTER CODE
}

void Test_IsOverdrawn()
{
    {                                                                                               // STARTER CODE
        float input_balance = 10;
        bool  exp_out = false;
        bool  act_out = IsOverdrawn( input_balance );
        cout << "Test: IsOverdrawn(" << input_balance << ") = " << exp_out << "? ";
        if ( act_out == exp_out ) { cout << GREEN << " PASS" << endl; }
        else                      { cout << RED << " FAIL; got " << act_out << " instead!" << endl; }
        cout << CLEAR;
    }
}

/**
   @param original the original ingredient quantity
   @param batches a decimal ratio for amount of batches (e.g., 2 for double batches, 0.5 for half batches)
   @return adjusted amount of ingredient.
*/
float AdjustIngredient( float original, float batches )
{
    return 0; // TODO: REMOVE THIS LINE!                                                            // STARTER CODE
}

void Test_AdjustIngredient()
{
    {                                                                                               // STARTER CODE
        float input_original = 5;
        float input_batches = 2;
        float exp_out = 10;
        float act_out = AdjustIngredient( input_original, input_batches );
        cout << "Test: AdjustIngredient(" << input_original << ", " << input_batches << ") = " << exp_out << "? ";
        if ( act_out == exp_out ) { cout << GREEN << " PASS" << endl; }
        else                      { cout << RED << " FAIL; got " << act_out << " instead!" << endl; }
        cout << CLEAR;
    }
}

/**
   @param choice the user's selection
   @param min    the minimum valid selection
   @param max    the maximum valid selection
   @return true if choice is within the valid range, or false otherwise
   Note: min and max are INCLUSIVE.
*/
bool IsValidInput( int choice, int min, int max )
{
    return false; // TODO: REMOVE THIS LINE!                                                        // STARTER CODE
}

void Test_IsValidInput()
{
    {                                                                                               // STARTER CODE
        int input_choice = 5;
        int input_min = 1;
        int input_max = 5;
        bool  exp_out = true;
        bool  act_out = IsValidInput( input_choice, input_min, input_max );
        cout << "Test: IsValidInput(" << input_choice << ", " << input_min << ", " << input_max << ") = " << exp_out << "? ";
        if ( act_out == exp_out ) { cout << GREEN << " PASS" << endl; }
        else                      { cout << RED << " FAIL; got " << act_out << " instead!" << endl; }
        cout << CLEAR;
    }
}

/**
   @param arr an array of floats to sum together.
   @return the sum of all elements of the arr.
*/
float Average( vector<float> arr )
{
    float sum = 0;
    return 0; // TODO: REMOVE THIS LINE!                                                            // STARTER CODE
}

void Test_Average()
{
    {                                                                                               // STARTER CODE
        vector<float> input_arr = { 1.5, 2.5, 3.5 };
        float exp_out = 2.5;
        float act_out = Average( input_arr );
        cout << "Test: Average(" << input_arr << ") = " << exp_out << "? ";
        if ( act_out == exp_out ) { cout << GREEN << " PASS" << endl; }
        else                      { cout << RED << " FAIL; got " << act_out << " instead!" << endl; }
        cout << CLEAR;
    }
}

/**
   @param n the n! value to compute.
   @return the value of n!.
*/
int Factorial( int n )
{
    int product = 1;
    return product;
}

void Test_Factorial()
{
    {                                                                                               // STARTER CODE
        int input_n = 3;
        int exp_out = 3*2*1;
        int act_out = Factorial( input_n );
        cout << "Test: Factorial(" << input_n << ") = " << exp_out << "? ";
        if ( act_out == exp_out ) { cout << GREEN << " PASS" << endl; }
        else                      { cout << RED << " FAIL; got " << act_out << " instead!" << endl; }
        cout << CLEAR;
    }
}




// Helper function
string ToLower( string original )
{
    std::string str = "";
    for ( unsigned int i = 0; i < original.size(); i++ )
    {
        str += tolower( original[i] );
    }
    return str;
}
