#include <iostream>
#include <vector>
#include <string>
using namespace std;

void DisplayValue( string* ptr )
{
    cout << *ptr << endl;
}

void DisplayAll( vector<string*> ptrs )
{
    for ( size_t i = 0; i < ptrs.size(); i++ )
    {
        cout << i << ". ";
        DisplayValue( ptrs[i] );
    }
}

int main()
{
    cout << "Dereference pointers! What could POSSIBLY go wrong...?" << endl;
    string valid1 = "A", valid2 = "B", valid3 = "C";
    vector<string*> pointers = { &valid1, &valid2, &valid3, nullptr }; // The last item is invalid
    DisplayAll( pointers );

    return 0;
}
