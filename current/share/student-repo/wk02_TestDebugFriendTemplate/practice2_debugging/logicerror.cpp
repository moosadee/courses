#include <iostream>
#include <vector>
#include <string>
using namespace std;

string GetCity( int zipcode )
{
    if ( zipcode == 66002 )
    {
        return "Atchison";
    }
    else if ( zipcode == 66044 || zipcode == 66045 || zipcode == 66046 || zipcode == 66077 )
    {
        return "Lawrence";
    }
    else if ( zipcode == 66061 || zipcode == 66062 || zipcode == 66063 )
    {
        return "Olathe";
    }
    else
    {
        return "UNKNOWN";
    }
}

int main( int argCount, char* args[] )
{
    if ( argCount != 2 )
    {
        cout << endl << "Expected form: " << args[0] << " zipcode" << endl;
        return 1;
    }

    int zipcode = stoi( args[1] );
    string city = GetCity( zipcode );
    cout << endl
         << "Zipcode: " << zipcode
         << ", city: " << city << endl << endl;

    return 0;
}
