# -*- mode: org -*-

[[file:images/reading_u12_IntroDataStructures_structures.png]]

* What are data structures?


A *data structure* is an object (a class, a struct - some type of *structured* thing) that holds *data*. In particular, the entire job of a data structure object is to store data and provide an interface for *adding, removing,* and *accessing* that data.

*"Don't all the classes we write hold data? How is this different from other objects I've defined?"*

In the past, you may have written classes to represent objects, like perhaps a book. A book could have member variables like its title, isbn, and year published, and some methods that help us interface with a book.

#+ATTR_HTML: :class uml
#+ATTR_LATEX: :mode table :align | l |
|--------------------|
| Book               |
|--------------------|
| - title : string   |
| - isbn : string    |
| - year : int       |
|--------------------|
| + Setup() : void   |
| + Display() : void |
|--------------------|

However - this is not a data structure. We /could/, however, use a data structure to /store/ a list of books.


A data structure will store a series of some sort of data. Often, it will use an *array* or a *linked list* as a base structure and functionality will be built on top.

Ideally, the user doesn't care about /how/ the data structure works, they just care that they can *add, remove,* and *access* data they want to store.

Ideally, when we are writing a data structure, it should be:

-   Generic, so you could store *any data type* in it.
-   Reusable, so that the data structure can be used in many different programs.
-   Robust, offering exception handling to prevent the program from crashing when something goes wrong with it.
-   Encapsulated, handling the inner-workings of dealing with the data, without the user (or other programmers working outside the data structure) having to write special code to perform certain operations.


#+ATTR_HTML: :class uml
#+ATTR_LATEX: :mode table :align | l |
|-------------------------------------------|
| SmartBookArray                            |
|-------------------------------------------|
| - bookArray : =Book[]=                    |
| - arraySize : =int=                       |
| - bookCount : =int=                       |
|-------------------------------------------|
| + AddBook( newBook : Book ) : =void=      |
| + RemoveBook( index : int ) : =void=      |
| + RemoveBook( title : string ) : =void=   |
| + GetBook( index : int ) : =Book&=        |
| + FindBookIndex( title : string ) : =int= |
| + DisplayAll() : =void=                   |
| + Size() : =int=                          |
| + IsEmpty() : =bool=                      |
|-------------------------------------------|

The way I try to conceptualize the work I'm doing on a data structure is to pretend that I'm a developer that is going to create and sell a C++ library of data structures that /other developers at other companies/ can use in their own, completely separate, software projects. If I'm selling my data structures package to other businesses, my code should be dependable, stable, efficient, and relatively easy to use.

#+ATTR_LATEX: :width 0.8\textwidth
[[file:images/reading_u12_IntroDataStructures_structures_ad.png]]

~

* What is algorithm analysis?

Algorithm Analysis is the process of figuring out how *efficient* a function is and how it scales over time, given more and more data to operate on.

#+ATTR_LATEX: :width 0.5\textwidth
[[file:images/reading_u12_IntroDataStructures_structures_complexity.png]]

This is another important part of dealing with data structures, as different structures will offer different trade offs when it comes to the efficiency of *data access* functions, *data searching* functions, *data adding* functions, and *data removal* functions. Every operation takes a little bit of processing time, and as you iterate over data, that processing time is multiplied by the amount of times you go through a loop.

- Example: Scalability ::
Let's say we have a sorting algorithm that, for every $n$ records, it takes $n^2$ program units to find an object. If we had 100 records, then it would take $100^2 = 10,000$ time-units to sort the set of data.

What the time-units are could vary - an older machine might take longer to execute one instruction, and a newer computer might process an instruction much more quickly, but we think of algorithm complexity in this sort of generic form.

#+ATTR_LATEX: :width 0.8\textwidth
[[file:images/reading_u12_IntroDataStructures_structures_ad2.png]]


- Example: Efficiency of an array-based structure ::

#+ATTR_LATEX: :mode table :align | l | c | c | c | c | c |
| value: | kansas | missouri | arkansas | ohio | oklahoma |
| index: |      0 |        1 |        2 |    3 |        4 |


In an array-based structure, we have a series of *elements* in a row, and each element is accessible via its *index* (its position in the array). Arrays allow for random-access, so *accessing* element #2 is instant:

#+BEGIN_SRC cpp :class cpp
cout << arr[2];
#+END_SRC

No matter how many elements there are ($n$), we can access the element at index 2 without doing any looping. We state that this is $O(1)$ ("Big-O of 1") time complexity for an *access* operation on an *array*.

However, if we were *searching* through the unsorted array for an item, we would have to start at the beginning and look at each item, one at a time, until we either found what we're looking for, or hit the end of the array:

#+BEGIN_SRC cpp :class cpp
for ( int i = 0; i < ARR_SIZE; i++ )
{
    if ( arr[i] == searchTerm )
    {
            return i; // found at this position
    }
}
return -1;      // not found
#+END_SRC

For *search*, the /worst-case scenario/ is having to look at /all elements of the array/ to ensure what we're looking for isn't there. Given $n$ items in the array, we have to iterate through the loop $n$ times. This would end up being $O(n)$ ("Big-O of $n$") time complexity for a *search* operation on an *array*.

We can build our data structures on top of an array, but there is also a type of structure called a *linked* structure, which offers its own pros and cons to go with it. We will learn more about algorithm analysis and types of structures later on.

* The point of a Data Structures class

Of course, plenty of data structures have already been written and are available out there for you to use. C++ even has the *Standard Template Library* full of structures already built and optimized!

| vector | http://www.cplusplus.com/reference/vector/vector/ |
| list   | http://www.cplusplus.com/reference/list/list/     |
| map    | http://www.cplusplus.com/reference/map/map/       |
| stack  | https://cplusplus.com/reference/stack/stack/      |
| queue  | https://cplusplus.com/reference/queue/queue/      |

*"So why are we learning to write data structures if they've already been written for us?"*

While you generally /won't/ be rolling your own linked list for projects or on the job, it /is/ important to know how the inner-workings of these structures operate. Knowing how each structure works, its tradeoffs in efficiency, and how it structures its data will help you *choose* what structures to use when faced with a *design decision* for your software.

#+ATTR_LATEX: :width 0.6\textwidth
[[file:images/reading_u12_IntroDataStructures_structures_which-structure.png]]



































