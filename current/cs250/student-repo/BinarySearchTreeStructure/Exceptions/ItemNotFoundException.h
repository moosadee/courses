#ifndef _ITEM_NOT_FOUND_EXCEPTION
#define _ITEM_NOT_FOUND_EXCEPTION

// C++ Library includes
#include <stdexcept>
#include <string>

namespace Exception
{

//! EXCEPTION for item not found in a structure
    class ItemNotFoundException : public std::runtime_error
    {
    public:
        ItemNotFoundException( std::string functionName, std::string message )
            : std::runtime_error( "[" + functionName + "] " + message  ) { ; }
    };

} // End of namespace

#endif
