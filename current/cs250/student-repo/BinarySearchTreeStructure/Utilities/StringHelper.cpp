#include "StringHelper.h"

std::string StringToLower( const std::string& val )
{
    std::string str = "";
    for ( unsigned int i = 0; i < val.size(); i++ )
    {
        str += tolower( val[i] );
    }
    return str;
}
