#include "BinarySearchTreeTester.h"
#include "BinarySearchTree.h"
#include "../../Utilities/Style.h"

#include <iostream>
#include <sstream>
using namespace std;

void BinarySearchTreeTester::RunAll()
{
    DisplayHeader( "BinarySearchTreeTester - RunAll", 0 );
    Test_BinarySearchTree_NodeConstructor();
    Test_BinarySearchTree_Constructor();
    Test_BinarySearchTree_GetMinKey();
    Test_BinarySearchTree_GetMaxKey();
    Test_BinarySearchTree_GetCount();
    Test_BinarySearchTree_GetHeight();
    Test_BinarySearchTree_GetInOrder();
    Test_BinarySearchTree_GetPreOrder();
    Test_BinarySearchTree_GetPostOrder();
    Test_BinarySearchTree_Push();
    Test_BinarySearchTree_Contains();
    Test_BinarySearchTree_FindNode();
    Test_BinarySearchTree_PopNode();
    Test_BinarySearchTree_PopRoot();
    Test_BinarySearchTree_GetSuccessor();
    Test_BinarySearchTree_ShiftNodes();
}

void BinarySearchTreeTester::Test_BinarySearchTree_NodeConstructor()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "BinarySearchTreeNode Constructor";
    ostringstream oss;

    {
        string testName = "New Node check";
        BinarySearchTreeNode<int, string> testNode;
        oss << testNode.ptrLeft;
        totalTests++; testsPassed += Test_DoTheyMatch( oss.str(), string( "0" ), testName, "node.ptrLeft" );

        oss.str( "" );
        oss.clear();
        oss << testNode.ptrRight;
        totalTests++; testsPassed += Test_DoTheyMatch( oss.str(), string( "0" ), testName, "node.ptrRight" );
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void BinarySearchTreeTester::Test_BinarySearchTree_Constructor()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "BinarySearchTree Constructor";
    ostringstream oss;

    {
        string testName = "New Tree check";
        BinarySearchTree<int, string> tree;
        oss << tree.m_ptrRoot;
        totalTests++; testsPassed += Test_DoTheyMatch( oss.str(), string( "0" ), testName, "tree.m_ptrRoot" );
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void BinarySearchTreeTester::Test_BinarySearchTree_Push()
{
    DisplayHeader( __func__, 1 );
    string testName = "";
    int testsPassed = 0, totalTests = 0;
    string functionName = "BinarySearchTree::Push";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check PREREQ RecursivePush implemented" ); totalTests++;
        BinarySearchTree<int, string> tree;
        try                                                    { tree.RecursivePush( 1, "A", tree.m_ptrRoot ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        BinarySearchTree<int, string> tree;
        try                                                    { tree.Push( 1, "A" ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Push one item to tree" );
        BinarySearchTree<int, string> tree;
        tree.Push( 5, "apple" );
        string exp = "not nullptr";
        string act = ( tree.m_ptrRoot == nullptr ) ? "nullptr" : "not nullptr";

        totalTests++; testsPassed += Test_DoTheyMatch( exp, act, testName, "tree.m_ptrRoot" );
        totalTests++; testsPassed += Test_DoTheyMatch( 1, tree.m_nodeCount, testName, "tree.m_nodeCount" );
        if ( tree.m_ptrRoot == nullptr ) { cout << YELLOW << "tree.m_ptrRoot is nullptr; LEAVING TESTS EARLY." << CLEAR << endl; return; }
        totalTests++; testsPassed += Test_DoTheyMatch( 5,                 tree.m_ptrRoot->key,  testName, "tree.m_ptrRoot->key  (5)" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "apple" ), tree.m_ptrRoot->data, testName, "tree.m_ptrRoot->data (apple)" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Push: 5, 2, 1, 3, 7" );
        BinarySearchTree<int, string> tree;
        tree.Push( 5, "apple" );
        tree.Push( 2, "bread" );
        tree.Push( 1, "cheese" );
        tree.Push( 3, "dango" );
        tree.Push( 7, "egg" );
        string exp = "not nullptr";
        string act = ( tree.m_ptrRoot == nullptr ) ? "nullptr" : "not nullptr";

        totalTests++; testsPassed += Test_DoTheyMatch( exp, act, testName, "tree.m_ptrRoot" );
        totalTests++; testsPassed += Test_DoTheyMatch( 5, tree.m_nodeCount, testName, "tree.m_nodeCount" );
        if ( tree.m_ptrRoot == nullptr ) { cout << YELLOW << "tree.m_ptrRoot is nullptr; LEAVING TESTS EARLY." << CLEAR << endl; return; }

        totalTests++; testsPassed += Test_DoTheyMatch( 5,                  tree.m_ptrRoot->key,  testName, "tree.m_ptrRoot->key  (5)" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "apple" ),  tree.m_ptrRoot->data, testName, "tree.m_ptrRoot->data (apple)" );

        totalTests++; testsPassed += Test_DoTheyMatch( 2,                  tree.m_ptrRoot->ptrLeft->key,  testName, "tree.m_ptrRoot->ptrLeft->key  (2)" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "bread" ),  tree.m_ptrRoot->ptrLeft->data, testName, "tree.m_ptrRoot->ptrLeft->data (bread)" );

        totalTests++; testsPassed += Test_DoTheyMatch( 1,                  tree.m_ptrRoot->ptrLeft->ptrLeft->key,  testName, "tree.m_ptrRoot->ptrLeft->ptrLeft->key  (1)" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "cheese" ), tree.m_ptrRoot->ptrLeft->ptrLeft->data, testName, "tree.m_ptrRoot->ptrLeft->ptrLeft->data (cheese)" );

        totalTests++; testsPassed += Test_DoTheyMatch( 3,                  tree.m_ptrRoot->ptrLeft->ptrRight->key,  testName, "tree.m_ptrRoot->ptrLeft->ptrRight->key  (3)" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "dango" ),  tree.m_ptrRoot->ptrLeft->ptrRight->data, testName, "tree.m_ptrRoot->ptrLeft->ptrRight->data (dango)" );

        totalTests++; testsPassed += Test_DoTheyMatch( 7,                  tree.m_ptrRoot->ptrRight->key,  testName, "tree.m_ptrRoot->ptrRight->key  (7)" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "egg" ),  tree.m_ptrRoot->ptrRight->data, testName, "tree.m_ptrRoot->ptrRight->data (egg)" );

        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void BinarySearchTreeTester::Test_BinarySearchTree_Contains()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "BinarySearchTree::Contains";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check PREREQ RecursiveContains implemented" ); totalTests++;
        BinarySearchTree<int, string> tree;
        try                                                    { tree.RecursiveContains( 10, tree.m_ptrRoot ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        BinarySearchTree<int, string> tree;
        try                                                    { tree.Contains( 10 ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Create tree, check Contains()" );
        BinarySearchTree<int, string> tree;
        tree.m_ptrRoot = new BinarySearchTreeNode<int, string>;                                            //              5
        tree.m_ptrRoot->key = 5; tree.m_ptrRoot->data = "five";                                            //            (   )
                                                                                                           //           3     7
        tree.m_ptrRoot->ptrLeft = new BinarySearchTreeNode<int, string>;                                   //          ( )
        tree.m_ptrRoot->ptrLeft->key = 3; tree.m_ptrRoot->ptrLeft->data = "three";                         //         2   4

        tree.m_ptrRoot->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrRight->key = 7; tree.m_ptrRoot->ptrRight->data = "seven";

        tree.m_ptrRoot->ptrLeft->ptrLeft = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrLeft->ptrLeft->key = 2; tree.m_ptrRoot->ptrLeft->ptrLeft->data = "two";

        tree.m_ptrRoot->ptrLeft->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrLeft->ptrRight->key = 4; tree.m_ptrRoot->ptrLeft->ptrRight->data = "four";

        tree.m_nodeCount = 5;
        totalTests++; testsPassed += Test_DoTheyMatch( true,  tree.Contains( 2 ), testName, "tree.Contains( 2 ) = true" );
        totalTests++; testsPassed += Test_DoTheyMatch( false, tree.Contains( 8 ), testName, "tree.Contains( 8 ) = false" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void BinarySearchTreeTester::Test_BinarySearchTree_FindNode()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "BinarySearchTree::FindNode";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check PREREQ RecursiveFindNode implemented" ); totalTests++;
        BinarySearchTree<int, string> tree;
        try                                                    { tree.RecursiveFindNode( 10, tree.m_ptrRoot ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        BinarySearchTree<int, string> tree;
        try                                                    { tree.FindNode( 10 ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Create tree, check FindNode()" );
        BinarySearchTree<int, string> tree;
        tree.m_ptrRoot = new BinarySearchTreeNode<int, string>;                                            //              5
        tree.m_ptrRoot->key = 5; tree.m_ptrRoot->data = "five";                                            //            (   )
                                                                                                           //           3     7
        tree.m_ptrRoot->ptrLeft = new BinarySearchTreeNode<int, string>;                                   //          ( )
        tree.m_ptrRoot->ptrLeft->key = 3; tree.m_ptrRoot->ptrLeft->data = "three";                         //         2   4

        tree.m_ptrRoot->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrRight->key = 7; tree.m_ptrRoot->ptrRight->data = "seven";

        tree.m_ptrRoot->ptrLeft->ptrLeft = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrLeft->ptrLeft->key = 2; tree.m_ptrRoot->ptrLeft->ptrLeft->data = "two";

        tree.m_ptrRoot->ptrLeft->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrLeft->ptrRight->key = 4; tree.m_ptrRoot->ptrLeft->ptrRight->data = "four";

        tree.m_nodeCount = 5;

        BinarySearchTreeNode<int, string>* ptrNode = tree.FindNode( 7 );
        string exp = "not nullptr";
        string act = ( ptrNode == nullptr ) ? "nullptr" : "not nullptr";
        totalTests++; testsPassed += Test_DoTheyMatch( exp, act, testName, "tree.FindNode( 7 ) - not nullptr" );

        if ( ptrNode == nullptr )
        {
            cout << YELLOW << "NODE IS NULLPTR; LEAVING TESTS EARLY." << CLEAR << endl; return;
        }

        totalTests++; testsPassed += Test_DoTheyMatch( string( "seven" ),  ptrNode->data, testName, "tree.FindNode( 7 )->data = seven" );
        totalTests++; testsPassed += Test_DoTheyMatch( 7,                  ptrNode->key,  testName, "tree.FindNode( 7 )->key = 7" );

        ptrNode = tree.FindNode( 8 );
        exp = "nullptr";
        act = ( ptrNode == nullptr ) ? "nullptr" : "not nullptr";
        totalTests++; testsPassed += Test_DoTheyMatch( exp, act, testName, "tree.FindNode( 8 )" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void BinarySearchTreeTester::Test_BinarySearchTree_GetInOrder()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "BinarySearchTree::GetInOrder";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check PREREQ RecursiveGetInOrder implemented" ); totalTests++;
        BinarySearchTree<int, string> tree; stringstream output;
        try                                                    { tree.RecursiveGetInOrder( tree.m_ptrRoot, output ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        BinarySearchTree<int, string> tree;
        try                                                    { tree.GetInOrder(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Create tree, check GetInOrder()" );
        BinarySearchTree<int, string> tree;
        tree.m_ptrRoot = new BinarySearchTreeNode<int, string>;                                            //              5
        tree.m_ptrRoot->key = 5; tree.m_ptrRoot->data = "five";                                            //            (   )
                                                                                                           //           3     7
        tree.m_ptrRoot->ptrLeft = new BinarySearchTreeNode<int, string>;                                   //          ( )
        tree.m_ptrRoot->ptrLeft->key = 3; tree.m_ptrRoot->ptrLeft->data = "three";                         //         2   4

        tree.m_ptrRoot->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrRight->key = 7; tree.m_ptrRoot->ptrRight->data = "seven";

        tree.m_ptrRoot->ptrLeft->ptrLeft = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrLeft->ptrLeft->key = 2; tree.m_ptrRoot->ptrLeft->ptrLeft->data = "two";

        tree.m_ptrRoot->ptrLeft->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrLeft->ptrRight->key = 4; tree.m_ptrRoot->ptrLeft->ptrRight->data = "four";

        tree.m_nodeCount = 5;
        totalTests++; testsPassed += Test_DoTheyMatch( string( "2 3 4 5 7 " ), tree.GetInOrder(), testName, "tree.GetInOrder()" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Create tree, check GetInOrder()" );
        BinarySearchTree<int, string> tree;
        tree.m_ptrRoot = new BinarySearchTreeNode<int, string>;                                            //              5
        tree.m_ptrRoot->key = 5; tree.m_ptrRoot->data = "five";                                            //            (   )
                                                                                                           //           3     7
        tree.m_ptrRoot->ptrLeft = new BinarySearchTreeNode<int, string>;                                   //            )     )
        tree.m_ptrRoot->ptrLeft->key = 3; tree.m_ptrRoot->ptrLeft->data = "three";                         //             4     8
                                                                                                           //                    )
        tree.m_ptrRoot->ptrRight = new BinarySearchTreeNode<int, string>;                                  //                     9
        tree.m_ptrRoot->ptrRight->key = 7; tree.m_ptrRoot->ptrRight->data = "seven";

        tree.m_ptrRoot->ptrLeft->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrLeft->ptrRight->key = 4; tree.m_ptrRoot->ptrLeft->ptrRight->data = "four";

        tree.m_ptrRoot->ptrRight->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrRight->ptrRight->key = 8; tree.m_ptrRoot->ptrRight->ptrRight->data = "eight";

        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight->key = 9; tree.m_ptrRoot->ptrRight->ptrRight->ptrRight->data = "nine";

        tree.m_nodeCount = 6;
        totalTests++; testsPassed += Test_DoTheyMatch( string( "3 4 5 7 8 9 "  ), tree.GetInOrder(), testName, "tree.GetInOrder()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void BinarySearchTreeTester::Test_BinarySearchTree_GetPreOrder()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "BinarySearchTree::GetPreOrder";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check PREREQ RecursiveGetPreOrder implemented" ); totalTests++;
        BinarySearchTree<int, string> tree; stringstream output;
        try                                                    { tree.RecursiveGetPreOrder( tree.m_ptrRoot, output ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        BinarySearchTree<int, string> tree;
        try                                                    { tree.GetPreOrder(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Create tree, check GetPreOrder()" );
        BinarySearchTree<int, string> tree;
        tree.m_ptrRoot = new BinarySearchTreeNode<int, string>;                                            //              5
        tree.m_ptrRoot->key = 5; tree.m_ptrRoot->data = "five";                                            //            (   )
                                                                                                           //           3     7
        tree.m_ptrRoot->ptrLeft = new BinarySearchTreeNode<int, string>;                                   //          ( )
        tree.m_ptrRoot->ptrLeft->key = 3; tree.m_ptrRoot->ptrLeft->data = "three";                         //         2   4

        tree.m_ptrRoot->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrRight->key = 7; tree.m_ptrRoot->ptrRight->data = "seven";

        tree.m_ptrRoot->ptrLeft->ptrLeft = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrLeft->ptrLeft->key = 2; tree.m_ptrRoot->ptrLeft->ptrLeft->data = "two";

        tree.m_ptrRoot->ptrLeft->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrLeft->ptrRight->key = 4; tree.m_ptrRoot->ptrLeft->ptrRight->data = "four";

        tree.m_nodeCount = 5;
        totalTests++; testsPassed += Test_DoTheyMatch( string( "5 3 2 4 7 " ), tree.GetPreOrder(), testName, "tree.GetPreOrder()" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Create tree, check GetPreOrder()" );
        BinarySearchTree<int, string> tree;
        tree.m_ptrRoot = new BinarySearchTreeNode<int, string>;                                            //              5
        tree.m_ptrRoot->key = 5; tree.m_ptrRoot->data = "five";                                            //            (   )
                                                                                                           //           3     7
        tree.m_ptrRoot->ptrLeft = new BinarySearchTreeNode<int, string>;                                   //            )     )
        tree.m_ptrRoot->ptrLeft->key = 3; tree.m_ptrRoot->ptrLeft->data = "three";                         //             4     8
                                                                                                           //                    )
        tree.m_ptrRoot->ptrRight = new BinarySearchTreeNode<int, string>;                                  //                     9
        tree.m_ptrRoot->ptrRight->key = 7; tree.m_ptrRoot->ptrRight->data = "seven";

        tree.m_ptrRoot->ptrLeft->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrLeft->ptrRight->key = 4; tree.m_ptrRoot->ptrLeft->ptrRight->data = "four";

        tree.m_ptrRoot->ptrRight->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrRight->ptrRight->key = 8; tree.m_ptrRoot->ptrRight->ptrRight->data = "eight";

        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight->key = 9; tree.m_ptrRoot->ptrRight->ptrRight->ptrRight->data = "nine";

        tree.m_nodeCount = 6;
        totalTests++; testsPassed += Test_DoTheyMatch( string( "5 3 4 7 8 9 "  ), tree.GetPreOrder(), testName, "tree.GetPreOrder()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void BinarySearchTreeTester::Test_BinarySearchTree_GetPostOrder()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "BinarySearchTree::GetPostOrder";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check PREREQ RecursiveGetPostOrder implemented" ); totalTests++;
        BinarySearchTree<int, string> tree; stringstream output;
        try                                                    { tree.RecursiveGetPostOrder( tree.m_ptrRoot, output ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        BinarySearchTree<int, string> tree;
        try                                                    { tree.GetPostOrder(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Create tree, check GetPostOrder()" );
        BinarySearchTree<int, string> tree;
        tree.m_ptrRoot = new BinarySearchTreeNode<int, string>;                                            //              5
        tree.m_ptrRoot->key = 5; tree.m_ptrRoot->data = "five";                                            //            (   )
                                                                                                           //           3     7
        tree.m_ptrRoot->ptrLeft = new BinarySearchTreeNode<int, string>;                                   //          ( )
        tree.m_ptrRoot->ptrLeft->key = 3; tree.m_ptrRoot->ptrLeft->data = "three";                         //         2   4

        tree.m_ptrRoot->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrRight->key = 7; tree.m_ptrRoot->ptrRight->data = "seven";

        tree.m_ptrRoot->ptrLeft->ptrLeft = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrLeft->ptrLeft->key = 2; tree.m_ptrRoot->ptrLeft->ptrLeft->data = "two";

        tree.m_ptrRoot->ptrLeft->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrLeft->ptrRight->key = 4; tree.m_ptrRoot->ptrLeft->ptrRight->data = "four";

        tree.m_nodeCount = 5;
        totalTests++; testsPassed += Test_DoTheyMatch( string( "2 4 3 7 5 " ), tree.GetPostOrder(), testName, "tree.GetPostOrder()" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Create tree, check GetPostOrder()" );
        BinarySearchTree<int, string> tree;
        tree.m_ptrRoot = new BinarySearchTreeNode<int, string>;                                            //              5
        tree.m_ptrRoot->key = 5; tree.m_ptrRoot->data = "five";                                            //            (   )
                                                                                                           //           3     7
        tree.m_ptrRoot->ptrLeft = new BinarySearchTreeNode<int, string>;                                   //            )     )
        tree.m_ptrRoot->ptrLeft->key = 3; tree.m_ptrRoot->ptrLeft->data = "three";                         //             4     8
                                                                                                           //                    )
        tree.m_ptrRoot->ptrRight = new BinarySearchTreeNode<int, string>;                                  //                     9
        tree.m_ptrRoot->ptrRight->key = 7; tree.m_ptrRoot->ptrRight->data = "seven";

        tree.m_ptrRoot->ptrLeft->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrLeft->ptrRight->key = 4; tree.m_ptrRoot->ptrLeft->ptrRight->data = "four";

        tree.m_ptrRoot->ptrRight->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrRight->ptrRight->key = 8; tree.m_ptrRoot->ptrRight->ptrRight->data = "eight";

        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight->key = 9; tree.m_ptrRoot->ptrRight->ptrRight->ptrRight->data = "nine";

        tree.m_nodeCount = 6;
        totalTests++; testsPassed += Test_DoTheyMatch( string( "4 3 9 8 7 5 "  ), tree.GetPostOrder(), testName, "tree.GetPostOrder()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void BinarySearchTreeTester::Test_BinarySearchTree_GetMinKey()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "BinarySearchTree::GetMinKey";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check PREREQ RecursiveGetMin implemented" ); totalTests++;
        BinarySearchTree<int, string> tree;
        try                                                    { tree.RecursiveGetMin( tree.m_ptrRoot ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        BinarySearchTree<int, string> tree;
        try                                                    { tree.GetMinKey(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Create tree, check GetMinKey()" );
        BinarySearchTree<int, string> tree;
        tree.m_ptrRoot = new BinarySearchTreeNode<int, string>;                                            //              5
        tree.m_ptrRoot->key = 5; tree.m_ptrRoot->data = "five";                                            //            (   )
                                                                                                           //           3     7
        tree.m_ptrRoot->ptrLeft = new BinarySearchTreeNode<int, string>;                                   //          ( )
        tree.m_ptrRoot->ptrLeft->key = 3; tree.m_ptrRoot->ptrLeft->data = "three";                         //         2   4

        tree.m_ptrRoot->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrRight->key = 7; tree.m_ptrRoot->ptrRight->data = "seven";

        tree.m_ptrRoot->ptrLeft->ptrLeft = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrLeft->ptrLeft->key = 2; tree.m_ptrRoot->ptrLeft->ptrLeft->data = "two";

        tree.m_ptrRoot->ptrLeft->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrLeft->ptrRight->key = 4; tree.m_ptrRoot->ptrLeft->ptrRight->data = "four";

        tree.m_nodeCount = 5;
        int minKey = -1;
        totalTests++;
        try
        {
            minKey = tree.GetMinKey();
            testsPassed += Test_DoTheyMatch( 2, tree.GetMinKey(), testName, "tree.GetMinKey()" );
        }
        catch( ... )
        {
            Test_FailDisplayExpectedActual( testName, "Exception thrown?", false, true );
            cout << YELLOW << "EXCEPTION THROWN; LEAVING TESTS EARLY." << CLEAR << endl; return;
        }
        Test_End();
    }

    {
        string testName = Test_Begin( "Create tree, check GetMinKey()" );
        BinarySearchTree<int, string> tree;
        tree.m_ptrRoot = new BinarySearchTreeNode<int, string>;                                            //              5
        tree.m_ptrRoot->key = 5; tree.m_ptrRoot->data = "five";                                            //            (   )
                                                                                                           //           3     7
        tree.m_ptrRoot->ptrLeft = new BinarySearchTreeNode<int, string>;                                   //            )     )
        tree.m_ptrRoot->ptrLeft->key = 3; tree.m_ptrRoot->ptrLeft->data = "three";                         //             4     8
                                                                                                           //                    )
        tree.m_ptrRoot->ptrRight = new BinarySearchTreeNode<int, string>;                                  //                     9
        tree.m_ptrRoot->ptrRight->key = 7; tree.m_ptrRoot->ptrRight->data = "seven";

        tree.m_ptrRoot->ptrLeft->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrLeft->ptrRight->key = 4; tree.m_ptrRoot->ptrLeft->ptrRight->data = "four";

        tree.m_ptrRoot->ptrRight->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrRight->ptrRight->key = 8; tree.m_ptrRoot->ptrRight->ptrRight->data = "eight";

        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight->key = 9; tree.m_ptrRoot->ptrRight->ptrRight->ptrRight->data = "nine";

        tree.m_nodeCount = 6;
        totalTests++; testsPassed += Test_DoTheyMatch( 3, tree.GetMinKey(), testName, "tree.GetMinKey()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void BinarySearchTreeTester::Test_BinarySearchTree_GetMaxKey()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "BinarySearchTree::GetMaxKey";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check PREREQ RecursiveGetMax implemented" ); totalTests++;
        BinarySearchTree<int, string> tree;
        try                                                    { tree.RecursiveGetMax( tree.m_ptrRoot ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        BinarySearchTree<int, string> tree;
        try                                                    { tree.GetMaxKey(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Create tree, check GetMaxKey()" );
        BinarySearchTree<int, string> tree;
        tree.m_ptrRoot = new BinarySearchTreeNode<int, string>;                                            //              5
        tree.m_ptrRoot->key = 5; tree.m_ptrRoot->data = "five";                                            //            (   )
                                                                                                           //           3     7
        tree.m_ptrRoot->ptrLeft = new BinarySearchTreeNode<int, string>;                                   //          ( )
        tree.m_ptrRoot->ptrLeft->key = 3; tree.m_ptrRoot->ptrLeft->data = "three";                         //         2   4

        tree.m_ptrRoot->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrRight->key = 7; tree.m_ptrRoot->ptrRight->data = "seven";

        tree.m_ptrRoot->ptrLeft->ptrLeft = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrLeft->ptrLeft->key = 2; tree.m_ptrRoot->ptrLeft->ptrLeft->data = "two";

        tree.m_ptrRoot->ptrLeft->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrLeft->ptrRight->key = 4; tree.m_ptrRoot->ptrLeft->ptrRight->data = "four";

        tree.m_nodeCount = 5;
        int maxKey = -1;
        totalTests++;
        try
        {
            maxKey = tree.GetMaxKey();
            testsPassed += Test_DoTheyMatch( 7, maxKey, testName, "tree.GetMaxKey()" );
        }
        catch( ... )
        {
            Test_FailDisplayExpectedActual( testName, "Exception thrown?", false, true );
            cout << YELLOW << "EXCEPTION THROWN; LEAVING TESTS EARLY." << CLEAR << endl; return;
        }
        Test_End();
    }

    {
        string testName = Test_Begin( "Create tree, check GetMaxKey()" );
        BinarySearchTree<int, string> tree;
        tree.m_ptrRoot = new BinarySearchTreeNode<int, string>;                                            //              5
        tree.m_ptrRoot->key = 5; tree.m_ptrRoot->data = "five";                                            //            (   )
                                                                                                           //           3     7
        tree.m_ptrRoot->ptrLeft = new BinarySearchTreeNode<int, string>;                                   //          ( )     )
        tree.m_ptrRoot->ptrLeft->key = 3; tree.m_ptrRoot->ptrLeft->data = "three";                         //         2   4     8
                                                                                                           //                    )
        tree.m_ptrRoot->ptrRight = new BinarySearchTreeNode<int, string>;                                  //                     9
        tree.m_ptrRoot->ptrRight->key = 7; tree.m_ptrRoot->ptrRight->data = "seven";

        tree.m_ptrRoot->ptrLeft->ptrLeft = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrLeft->ptrLeft->key = 2; tree.m_ptrRoot->ptrLeft->ptrLeft->data = "two";

        tree.m_ptrRoot->ptrLeft->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrLeft->ptrRight->key = 4; tree.m_ptrRoot->ptrLeft->ptrRight->data = "four";

        tree.m_ptrRoot->ptrRight->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrRight->ptrRight->key = 8; tree.m_ptrRoot->ptrRight->ptrRight->data = "eight";

        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight->key = 9; tree.m_ptrRoot->ptrRight->ptrRight->ptrRight->data = "nine";

        tree.m_nodeCount = 7;
        totalTests++; testsPassed += Test_DoTheyMatch( 9, tree.GetMaxKey(), testName, "tree.GetMaxKey()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void BinarySearchTreeTester::Test_BinarySearchTree_GetCount()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "BinarySearchTree::GetCount";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        BinarySearchTree<int, string> tree;
        try                                                    { tree.GetCount(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Set size, check GetCount()" );
        BinarySearchTree<int, string> tree;
        tree.m_nodeCount = 5;
        totalTests++; testsPassed += Test_DoTheyMatch( 5, tree.GetCount(), testName, "tree.GetCount()" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Set size, check GetCount()" );
        BinarySearchTree<int, string> tree;
        tree.m_nodeCount = 10;
        totalTests++; testsPassed += Test_DoTheyMatch( 10, tree.GetCount(), testName, "tree.GetCount()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void BinarySearchTreeTester::Test_BinarySearchTree_GetHeight()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "BinarySearchTree::GetHeight";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check PREREQ RecursiveGetHeight implemented" ); totalTests++;
        BinarySearchTree<int, string> tree;
        try                                                    { tree.RecursiveGetHeight( tree.m_ptrRoot ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        BinarySearchTree<int, string> tree;
        try                                                    { tree.GetHeight(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Create tree, check GetHeight()" );
        BinarySearchTree<int, string> tree;
        tree.m_ptrRoot = new BinarySearchTreeNode<int, string>;                                            //              5
        tree.m_ptrRoot->key = 5; tree.m_ptrRoot->data = "five";                                            //            (   )
                                                                                                           //           3     7
        tree.m_ptrRoot->ptrLeft = new BinarySearchTreeNode<int, string>;                                   //          ( )
        tree.m_ptrRoot->ptrLeft->key = 3; tree.m_ptrRoot->ptrLeft->data = "three";                         //         2   4

        tree.m_ptrRoot->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrRight->key = 7; tree.m_ptrRoot->ptrRight->data = "seven";

        tree.m_ptrRoot->ptrLeft->ptrLeft = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrLeft->ptrLeft->key = 2; tree.m_ptrRoot->ptrLeft->ptrLeft->data = "two";

        tree.m_ptrRoot->ptrLeft->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrLeft->ptrRight->key = 4; tree.m_ptrRoot->ptrLeft->ptrRight->data = "four";

        tree.m_nodeCount = 5;
        totalTests++; testsPassed += Test_DoTheyMatch( 2, tree.GetHeight(), testName, "tree.GetHeight()" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Create tree, check GetHeight()" );
        BinarySearchTree<int, string> tree;
        tree.m_ptrRoot = new BinarySearchTreeNode<int, string>;                                            //              5
        tree.m_ptrRoot->key = 5; tree.m_ptrRoot->data = "five";                                            //            (   )
                                                                                                           //           3     7
        tree.m_ptrRoot->ptrLeft = new BinarySearchTreeNode<int, string>;                                   //          ( )     )
        tree.m_ptrRoot->ptrLeft->key = 3; tree.m_ptrRoot->ptrLeft->data = "three";                         //         2   4     8
                                                                                                           //                    )
        tree.m_ptrRoot->ptrRight = new BinarySearchTreeNode<int, string>;                                  //                     9
        tree.m_ptrRoot->ptrRight->key = 7; tree.m_ptrRoot->ptrRight->data = "seven";

        tree.m_ptrRoot->ptrLeft->ptrLeft = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrLeft->ptrLeft->key = 2; tree.m_ptrRoot->ptrLeft->ptrLeft->data = "two";

        tree.m_ptrRoot->ptrLeft->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrLeft->ptrRight->key = 4; tree.m_ptrRoot->ptrLeft->ptrRight->data = "four";

        tree.m_ptrRoot->ptrRight->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrRight->ptrRight->key = 8; tree.m_ptrRoot->ptrRight->ptrRight->data = "eight";

        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight->key = 9; tree.m_ptrRoot->ptrRight->ptrRight->ptrRight->data = "nine";

        tree.m_nodeCount = 7;
        totalTests++; testsPassed += Test_DoTheyMatch( 3, tree.GetHeight(), testName, "tree.GetHeight()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void BinarySearchTreeTester::Test_BinarySearchTree_PopRoot()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "BinarySearchTree::PopRoot";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check PREREQ PopNode implemented" ); totalTests++;
        BinarySearchTree<int, string> tree;
        try                                                    { tree.PopNode( tree.m_ptrRoot ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        BinarySearchTree<int, string> tree;
        try                                                    { tree.PopRoot(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    // PopNode will basically test this functionality out anyway. Just want to confirm that this PopRoot function is implemented.

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void BinarySearchTreeTester::Test_BinarySearchTree_PopNode()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "BinarySearchTree::PopNode";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check PREREQ ShiftNodes implemented" ); totalTests++;
        BinarySearchTree<int, string> tree;
        try                                                    { tree.ShiftNodes( tree.m_ptrRoot, tree.m_ptrRoot ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Check PREREQ GetSuccessor implemented" ); totalTests++;
        BinarySearchTree<int, string> tree;
        try                                                    { tree.GetSuccessor( tree.m_ptrRoot ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        BinarySearchTree<int, string> tree;
        try                                                    { tree.PopNode( tree.m_ptrRoot ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Create tree, check PopNode()" );
        BinarySearchTree<int, string> tree;                                                                // BEFORE POP ROOT:
        tree.m_ptrRoot = new BinarySearchTreeNode<int, string>;                                            //              5
        tree.m_ptrRoot->key = 5; tree.m_ptrRoot->data = "five";                                            //            (   )
                                                                                                           //           3     7
        tree.m_ptrRoot->ptrLeft = new BinarySearchTreeNode<int, string>;                                   //          ( )     )
        tree.m_ptrRoot->ptrLeft->key = 3; tree.m_ptrRoot->ptrLeft->data = "three";                         //         2   4     8
        tree.m_ptrRoot->ptrRight = new BinarySearchTreeNode<int, string>;                                  //                    )
        tree.m_ptrRoot->ptrRight->key = 7; tree.m_ptrRoot->ptrRight->data = "seven";                       //                     9
        tree.m_ptrRoot->ptrLeft->ptrLeft = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrLeft->ptrLeft->key = 2; tree.m_ptrRoot->ptrLeft->ptrLeft->data = "two";         // AFTER POP ROOT:
        tree.m_ptrRoot->ptrLeft->ptrRight = new BinarySearchTreeNode<int, string>;                         //              7
        tree.m_ptrRoot->ptrLeft->ptrRight->key = 4; tree.m_ptrRoot->ptrLeft->ptrRight->data = "four";      //            (   )
        tree.m_ptrRoot->ptrRight->ptrRight = new BinarySearchTreeNode<int, string>;                        //            3    8
        tree.m_ptrRoot->ptrRight->ptrRight->key = 8; tree.m_ptrRoot->ptrRight->ptrRight->data = "eight";   //           ( )    )
        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight = new BinarySearchTreeNode<int, string>;              //          2   4    9
        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight->key = 9;                                             //
        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight->data = "nine";                                       //

        // Don't forget to set parents!!!
        tree.m_ptrRoot->ptrLeft->ptrParent = tree.m_ptrRoot;                                               // 3
        tree.m_ptrRoot->ptrRight->ptrParent = tree.m_ptrRoot;                                              // 7
        tree.m_ptrRoot->ptrLeft->ptrLeft->ptrParent = tree.m_ptrRoot->ptrLeft;                             // 2
        tree.m_ptrRoot->ptrLeft->ptrRight->ptrParent = tree.m_ptrRoot->ptrLeft;                            // 4
        tree.m_ptrRoot->ptrRight->ptrRight->ptrParent = tree.m_ptrRoot->ptrRight;                          // 8
        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight->ptrParent = tree.m_ptrRoot->ptrRight->ptrRight;      // 9

        tree.m_nodeCount = 7;

        try
        {
            tree.PopNode( tree.m_ptrRoot );
        }
        catch( ... )
        {
            cout << YELLOW << "EXCEPTION THROWN; LEAVING TESTS EARLY." << CLEAR << endl; return;
        }

        totalTests++; testsPassed += Test_DoTheyMatch( 7, tree.m_ptrRoot->key,           testName, "tree.m_ptrRoot->key = 7" );
        totalTests++; testsPassed += Test_DoTheyMatch( 3, tree.m_ptrRoot->ptrLeft->key,  testName, "tree.m_ptrRoot->ptrLeft->key = 3" );
        totalTests++; testsPassed += Test_DoTheyMatch( 8, tree.m_ptrRoot->ptrRight->key, testName, "tree.m_ptrRoot->ptrRight->key = 8" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Create tree, check PopNode()" );
        BinarySearchTree<int, string> tree;                                                                // BEFORE POP ROOT:
        tree.m_ptrRoot = new BinarySearchTreeNode<int, string>;                                            //              5
        tree.m_ptrRoot->key = 5; tree.m_ptrRoot->data = "five";                                            //            (   )
                                                                                                           //           3     7
        tree.m_ptrRoot->ptrLeft = new BinarySearchTreeNode<int, string>;                                   //          ( )     )
        tree.m_ptrRoot->ptrLeft->key = 3; tree.m_ptrRoot->ptrLeft->data = "three";                         //         2   4     8
        tree.m_ptrRoot->ptrRight = new BinarySearchTreeNode<int, string>;                                  //                    )
        tree.m_ptrRoot->ptrRight->key = 7; tree.m_ptrRoot->ptrRight->data = "seven";                       //                     9
        tree.m_ptrRoot->ptrLeft->ptrLeft = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrLeft->ptrLeft->key = 2; tree.m_ptrRoot->ptrLeft->ptrLeft->data = "two";         // AFTER POP ROOT (TWICE):
        tree.m_ptrRoot->ptrLeft->ptrRight = new BinarySearchTreeNode<int, string>;                         //              8
        tree.m_ptrRoot->ptrLeft->ptrRight->key = 4; tree.m_ptrRoot->ptrLeft->ptrRight->data = "four";      //            (   )
        tree.m_ptrRoot->ptrRight->ptrRight = new BinarySearchTreeNode<int, string>;                        //            3    9
        tree.m_ptrRoot->ptrRight->ptrRight->key = 8; tree.m_ptrRoot->ptrRight->ptrRight->data = "eight";   //           ( )
        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight = new BinarySearchTreeNode<int, string>;              //          2   4
        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight->key = 9;                                             //
        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight->data = "nine";                                       //

        // Don't forget to set parents!!!
        tree.m_ptrRoot->ptrLeft->ptrParent = tree.m_ptrRoot;                                               // 3
        tree.m_ptrRoot->ptrRight->ptrParent = tree.m_ptrRoot;                                              // 7
        tree.m_ptrRoot->ptrLeft->ptrLeft->ptrParent = tree.m_ptrRoot->ptrLeft;                             // 2
        tree.m_ptrRoot->ptrLeft->ptrRight->ptrParent = tree.m_ptrRoot->ptrLeft;                            // 4
        tree.m_ptrRoot->ptrRight->ptrRight->ptrParent = tree.m_ptrRoot->ptrRight;                          // 8
        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight->ptrParent = tree.m_ptrRoot->ptrRight->ptrRight;      // 9

        tree.m_nodeCount = 7;

        tree.PopNode( tree.m_ptrRoot );
        tree.PopNode( tree.m_ptrRoot );

        totalTests++; testsPassed += Test_DoTheyMatch( 8, tree.m_ptrRoot->key,           testName, "tree.m_ptrRoot->key = 8" );
        totalTests++; testsPassed += Test_DoTheyMatch( 3, tree.m_ptrRoot->ptrLeft->key,  testName, "tree.m_ptrRoot->ptrLeft->key = 3" );
        totalTests++; testsPassed += Test_DoTheyMatch( 9, tree.m_ptrRoot->ptrRight->key, testName, "tree.m_ptrRoot->ptrRight->key = 9" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void BinarySearchTreeTester::Test_BinarySearchTree_GetSuccessor()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "BinarySearchTree::GetSuccessor";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check PREREQ RecursiveGetMin implemented" ); totalTests++;
        BinarySearchTree<int, string> tree;
        try                                                    { tree.RecursiveGetMin( tree.m_ptrRoot ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        BinarySearchTree<int, string> tree;
        try                                                    { tree.GetSuccessor( tree.m_ptrRoot ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Find successor" );
        BinarySearchTree<int, string> tree;
        tree.m_ptrRoot = new BinarySearchTreeNode<int, string>;                                            //              5
        tree.m_ptrRoot->key = 5; tree.m_ptrRoot->data = "five";                                            //            (   )
                                                                                                           //           3     7
        tree.m_ptrRoot->ptrLeft = new BinarySearchTreeNode<int, string>;                                   //          ( )     )
        tree.m_ptrRoot->ptrLeft->key = 3; tree.m_ptrRoot->ptrLeft->data = "three";                         //         2   4     8
                                                                                                           //                    )
        tree.m_ptrRoot->ptrRight = new BinarySearchTreeNode<int, string>;                                  //                     9
        tree.m_ptrRoot->ptrRight->key = 7; tree.m_ptrRoot->ptrRight->data = "seven";

        tree.m_ptrRoot->ptrLeft->ptrLeft = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrLeft->ptrLeft->key = 2; tree.m_ptrRoot->ptrLeft->ptrLeft->data = "two";

        tree.m_ptrRoot->ptrLeft->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrLeft->ptrRight->key = 4; tree.m_ptrRoot->ptrLeft->ptrRight->data = "four";

        tree.m_ptrRoot->ptrRight->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrRight->ptrRight->key = 8; tree.m_ptrRoot->ptrRight->ptrRight->data = "eight";

        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight = new BinarySearchTreeNode<int, string>;
        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight->key = 9; tree.m_ptrRoot->ptrRight->ptrRight->ptrRight->data = "nine";

        // Don't forget to set parents!!!
        tree.m_ptrRoot->ptrLeft->ptrParent = tree.m_ptrRoot;                                               // 3
        tree.m_ptrRoot->ptrRight->ptrParent = tree.m_ptrRoot;                                              // 7
        tree.m_ptrRoot->ptrLeft->ptrLeft->ptrParent = tree.m_ptrRoot->ptrLeft;                             // 2
        tree.m_ptrRoot->ptrLeft->ptrRight->ptrParent = tree.m_ptrRoot->ptrLeft;                            // 4
        tree.m_ptrRoot->ptrRight->ptrRight->ptrParent = tree.m_ptrRoot->ptrRight;                          // 8
        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight->ptrParent = tree.m_ptrRoot->ptrRight->ptrRight;      // 9
        tree.m_nodeCount = 7;

        BinarySearchTreeNode<int, string>* node1 = nullptr;
        BinarySearchTreeNode<int, string>* node2 = nullptr;
        BinarySearchTreeNode<int, string>* node3 = nullptr;

        try
        {
            node1 = tree.GetSuccessor( tree.m_ptrRoot );
            node2 = tree.GetSuccessor( tree.m_ptrRoot->ptrLeft );
            node3 = tree.GetSuccessor( tree.m_ptrRoot->ptrRight );
        }
        catch( ... )
        {
            cout << YELLOW << "EXCEPTION THROWN; LEAVING TESTS EARLY." << CLEAR << endl; return;
        }

        if ( node1 == nullptr || node2 == nullptr || node3 == nullptr )
        {
            cout << YELLOW << "NODE IS NULL; LEAVING TESTS EARLY." << CLEAR << endl; return;
        }

        totalTests++; testsPassed += Test_DoTheyMatch( 7, node1->key, testName, "tree.GetSuccessor( tree.m_ptrRoot )->key" );
        totalTests++; testsPassed += Test_DoTheyMatch( 4, node2->key, testName, "tree.GetSuccessor( tree.m_ptrRoot->ptrLeft )->key" );
        totalTests++; testsPassed += Test_DoTheyMatch( 8, node3->key, testName, "tree.GetSuccessor( tree.m_ptrRoot->ptrRight )->key" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void BinarySearchTreeTester::Test_BinarySearchTree_ShiftNodes()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "BinarySearchTree::ShiftNodes";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        BinarySearchTree<int, string> tree;
        try                                                    { tree.ShiftNodes( tree.m_ptrRoot, tree.m_ptrRoot ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Create tree, check ShiftNodes( 7, 8 )" );
        BinarySearchTree<int, string> tree;                                                                // BEFORE SHIFT:
        tree.m_ptrRoot = new BinarySearchTreeNode<int, string>;                                            //              5
        tree.m_ptrRoot->key = 5; tree.m_ptrRoot->data = "five";                                            //            (   )
                                                                                                           //           3     7
        tree.m_ptrRoot->ptrLeft = new BinarySearchTreeNode<int, string>;                                   //          ( )     )
        tree.m_ptrRoot->ptrLeft->key = 3; tree.m_ptrRoot->ptrLeft->data = "three";                         //         2   4     8
        tree.m_ptrRoot->ptrRight = new BinarySearchTreeNode<int, string>;                                  //                    )
        tree.m_ptrRoot->ptrRight->key = 7; tree.m_ptrRoot->ptrRight->data = "seven";                       //                     9
        tree.m_ptrRoot->ptrLeft->ptrLeft = new BinarySearchTreeNode<int, string>;                          //
        tree.m_ptrRoot->ptrLeft->ptrLeft->key = 2; tree.m_ptrRoot->ptrLeft->ptrLeft->data = "two";         // AFTER SHIFT:
        tree.m_ptrRoot->ptrLeft->ptrRight = new BinarySearchTreeNode<int, string>;                         //              5
        tree.m_ptrRoot->ptrLeft->ptrRight->key = 4; tree.m_ptrRoot->ptrLeft->ptrRight->data = "four";      //            (   )
        tree.m_ptrRoot->ptrRight->ptrRight = new BinarySearchTreeNode<int, string>;                        //           3     8
        tree.m_ptrRoot->ptrRight->ptrRight->key = 8; tree.m_ptrRoot->ptrRight->ptrRight->data = "eight";   //          ( )     )
        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight = new BinarySearchTreeNode<int, string>;              //         2   4     9
        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight->key = 9;                                             //
        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight->data = "nine";                                       //

        // Don't forget to set parents!!!
        tree.m_ptrRoot->ptrLeft->ptrParent = tree.m_ptrRoot;                                               // 3
        tree.m_ptrRoot->ptrRight->ptrParent = tree.m_ptrRoot;                                              // 7
        tree.m_ptrRoot->ptrLeft->ptrLeft->ptrParent = tree.m_ptrRoot->ptrLeft;                             // 2
        tree.m_ptrRoot->ptrLeft->ptrRight->ptrParent = tree.m_ptrRoot->ptrLeft;                            // 4
        tree.m_ptrRoot->ptrRight->ptrRight->ptrParent = tree.m_ptrRoot->ptrRight;                          // 8
        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight->ptrParent = tree.m_ptrRoot->ptrRight->ptrRight;      // 9

        tree.m_nodeCount = 7;

        /*
        Usage:
        if ( ptrCurrent->ptrLeft == nullptr )
        {
            ShiftNodes( ptrCurrent, ptrCurrent->ptrRight );
        }
        */
        tree.ShiftNodes( tree.m_ptrRoot->ptrRight, tree.m_ptrRoot->ptrRight->ptrRight );

        totalTests++; testsPassed += Test_DoTheyMatch( 8, tree.m_ptrRoot->ptrRight->key,          testName, "m_ptrRoot->ptrRight->key" );

        Test_End();
    }

    {
        string testName = Test_Begin( "Create tree, check ShiftNodes( 2, 3 )" );
        BinarySearchTree<int, string> tree;                                                                // BEFORE SHIFT:
        tree.m_ptrRoot = new BinarySearchTreeNode<int, string>;                                            //              5
        tree.m_ptrRoot->key = 5; tree.m_ptrRoot->data = "five";                                            //            (   )
                                                                                                           //           3     7
        tree.m_ptrRoot->ptrLeft = new BinarySearchTreeNode<int, string>;                                   //          (       )
        tree.m_ptrRoot->ptrLeft->key = 3; tree.m_ptrRoot->ptrLeft->data = "three";                         //         2         8
        tree.m_ptrRoot->ptrRight = new BinarySearchTreeNode<int, string>;                                  //                    )
        tree.m_ptrRoot->ptrRight->key = 7; tree.m_ptrRoot->ptrRight->data = "seven";                       //                     9
        tree.m_ptrRoot->ptrLeft->ptrLeft = new BinarySearchTreeNode<int, string>;                          //
        tree.m_ptrRoot->ptrLeft->ptrLeft->key = 2; tree.m_ptrRoot->ptrLeft->ptrLeft->data = "two";         // AFTER SHIFT:
                                                                                                           //              5
                                                                                                           //            (   )
        tree.m_ptrRoot->ptrRight->ptrRight = new BinarySearchTreeNode<int, string>;                        //           2     7
        tree.m_ptrRoot->ptrRight->ptrRight->key = 8; tree.m_ptrRoot->ptrRight->ptrRight->data = "eight";   //                  )
        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight = new BinarySearchTreeNode<int, string>;              //                   8
        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight->key = 9;                                             //                    )
        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight->data = "nine";                                       //                     9

        // Don't forget to set parents!!!
        tree.m_ptrRoot->ptrLeft->ptrParent = tree.m_ptrRoot;                                               // 3
        tree.m_ptrRoot->ptrRight->ptrParent = tree.m_ptrRoot;                                              // 7
        tree.m_ptrRoot->ptrLeft->ptrLeft->ptrParent = tree.m_ptrRoot->ptrLeft;                             // 2
        tree.m_ptrRoot->ptrRight->ptrRight->ptrParent = tree.m_ptrRoot->ptrRight;                          // 8
        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight->ptrParent = tree.m_ptrRoot->ptrRight->ptrRight;      // 9

        tree.m_nodeCount = 7;

        /*
        Usage:
        else if ( ptrCurrent->ptrRight == nullptr )
        {
            ShiftNodes( ptrCurrent, ptrCurrent->ptrLeft );
        }
        */
        tree.ShiftNodes( tree.m_ptrRoot->ptrLeft, tree.m_ptrRoot->ptrLeft->ptrLeft );

        totalTests++; testsPassed += Test_DoTheyMatch( 2, tree.m_ptrRoot->ptrLeft->key,          testName, "m_ptrRoot->ptrRight->key" );

        Test_End();
    }

    {
        string testName = Test_Begin( "Create tree, check ShiftNodes( 3, 4 )" );
        BinarySearchTree<int, string> tree;                                                                // BEFORE SHIFT:
        tree.m_ptrRoot = new BinarySearchTreeNode<int, string>;                                            //              5
        tree.m_ptrRoot->key = 5; tree.m_ptrRoot->data = "five";                                            //            (   )
                                                                                                           //           3     7
        tree.m_ptrRoot->ptrLeft = new BinarySearchTreeNode<int, string>;                                   //          ( )     )
        tree.m_ptrRoot->ptrLeft->key = 3; tree.m_ptrRoot->ptrLeft->data = "three";                         //         2   4     8
        tree.m_ptrRoot->ptrRight = new BinarySearchTreeNode<int, string>;                                  //                    )
        tree.m_ptrRoot->ptrRight->key = 7; tree.m_ptrRoot->ptrRight->data = "seven";                       //                     9
        tree.m_ptrRoot->ptrLeft->ptrLeft = new BinarySearchTreeNode<int, string>;                          //
        tree.m_ptrRoot->ptrLeft->ptrLeft->key = 2; tree.m_ptrRoot->ptrLeft->ptrLeft->data = "two";         // AFTER SHIFT:
        tree.m_ptrRoot->ptrLeft->ptrRight = new BinarySearchTreeNode<int, string>;                         //              5
        tree.m_ptrRoot->ptrLeft->ptrRight->key = 4; tree.m_ptrRoot->ptrLeft->ptrRight->data = "four";      //            (   )
        tree.m_ptrRoot->ptrRight->ptrRight = new BinarySearchTreeNode<int, string>;                        //           4     7
        tree.m_ptrRoot->ptrRight->ptrRight->key = 8; tree.m_ptrRoot->ptrRight->ptrRight->data = "eight";   //          (       )
        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight = new BinarySearchTreeNode<int, string>;              //         2         8
        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight->key = 9;                                             //                    )
        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight->data = "nine";                                       //                     9

        // Don't forget to set parents!!!
        tree.m_ptrRoot->ptrLeft->ptrParent = tree.m_ptrRoot;                                               // 3
        tree.m_ptrRoot->ptrRight->ptrParent = tree.m_ptrRoot;                                              // 7
        tree.m_ptrRoot->ptrLeft->ptrLeft->ptrParent = tree.m_ptrRoot->ptrLeft;                             // 2
        tree.m_ptrRoot->ptrLeft->ptrRight->ptrParent = tree.m_ptrRoot->ptrLeft;                            // 4
        tree.m_ptrRoot->ptrRight->ptrRight->ptrParent = tree.m_ptrRoot->ptrRight;                          // 8
        tree.m_ptrRoot->ptrRight->ptrRight->ptrRight->ptrParent = tree.m_ptrRoot->ptrRight->ptrRight;      // 9

        tree.m_nodeCount = 7;

        /*
        Usage:
        if      ( ptrCurrent->ptrLeft == nullptr )  // FALSE
        else if ( ptrCurrent->ptrRight == nullptr ) // FALSE
        else                                        // two children
        {
            BinarySearchTreeNode<TK, TD>* ptrSuccessor = GetSuccessor( ptrCurrent );
            if ( ptrSuccessor->ptrParent != ptrCurrent )
            {
                ShiftNodes( ptrSuccessor, ptrSuccessor->ptrRight );
                ptrSuccessor->ptrRight = ptrCurrent->ptrRight;
                ptrSuccessor->ptrRight->ptrParent = ptrSuccessor;
            }

            ShiftNodes( ptrCurrent, ptrSuccessor );
            ptrSuccessor->ptrLeft = ptrCurrent->ptrLeft;
            ptrSuccessor->ptrLeft->ptrParent = ptrSuccessor;
        }
        */
        tree.ShiftNodes( tree.m_ptrRoot->ptrLeft, tree.m_ptrRoot->ptrLeft->ptrRight ); // Successor seems to get minimum value on right side?

        totalTests++; testsPassed += Test_DoTheyMatch( 4, tree.m_ptrRoot->ptrLeft->key,          testName, "m_ptrRoot->ptrLeft->key" );

        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}


