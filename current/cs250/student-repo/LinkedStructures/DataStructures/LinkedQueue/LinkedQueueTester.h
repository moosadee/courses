#ifndef _LINKED_QUEUE_TESTER
#define _LINKED_QUEUE_TESTER

class LinkedQueueTester
{
public:
    void RunAll();
    void Test_LinkedQueue_Push();
    void Test_LinkedQueue_Pop();
    void Test_LinkedQueue_Front();
    void Test_LinkedQueue_Size();
    void Test_LinkedQueue_IsEmpty();
};

#endif
