#include "LinkedQueueTester.h"
#include "LinkedQueue.h"
#include "../../Utilities/Style.h"

void LinkedQueueTester::RunAll()
{
    DisplayHeader( "LinkedQueueTester - RunAll", 0 );
    Test_LinkedQueue_Front();
    Test_LinkedQueue_IsEmpty();
    Test_LinkedQueue_Pop();
    Test_LinkedQueue_Push();
    Test_LinkedQueue_Size();
}

void LinkedQueueTester::Test_LinkedQueue_Push()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "LinkedQueue::Push";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ IsEmpty implemented" ); totalTests++;
        LinkedQueue<string> testStructure;
        try                                                    { testStructure.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Check if PREREQ Size implemented" ); totalTests++;
        LinkedQueue<string> testStructure;
        try                                                    { testStructure.Size(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        LinkedQueue<string> testStructure;
        try                                                    { testStructure.Push( "A" ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Push 1 item" );
        LinkedQueue<string> testStructure;
        testStructure.Push( "cheese" );
        string exp1 = "not nullptr", exp2 = "not nullptr";
        totalTests++; testsPassed += Test_DoTheyMatch( 1,                  testStructure.m_list.m_itemCount, testName, "testStructure.m_list.m_itemCount" );
        if ( testStructure.m_list.m_itemCount == 0 ) { cout << YELLOW << "testStructure.m_list.m_itemCount IS 0; LEAVING TESTS EARLY." << CLEAR << endl; return; }
        totalTests++; testsPassed += Test_DoTheyMatch( string( "cheese" ), testStructure.m_list.GetAt( 0 ), testName, "testStructure.m_list.GetAt( 0 )" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Push 2 items, check order" );
        LinkedQueue<string> testStructure;
        testStructure.Push( "A" );
        testStructure.Push( "B" );
        totalTests++; testsPassed += Test_DoTheyMatch( 2,             testStructure.m_list.m_itemCount, testName, "testStructure.m_list.m_itemCount" );
        if ( testStructure.m_list.m_itemCount == 0 ) { cout << YELLOW << "testStructure.m_list.m_itemCount IS 0; LEAVING TESTS EARLY." << CLEAR << endl; return; }
        totalTests++; testsPassed += Test_DoTheyMatch( string( "A" ), testStructure.m_list.GetAt( 0 ), testName, "testStructure.m_list.GetAt( 0 )" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "B" ), testStructure.m_list.GetAt( 1 ), testName, "testStructure.m_list.GetAt( 1 )" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void LinkedQueueTester::Test_LinkedQueue_Pop()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "LinkedQueue::Pop";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ IsEmpty implemented" ); totalTests++;
        LinkedQueue<string> testStructure;
        try                                                    { testStructure.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Check if PREREQ Size implemented" ); totalTests++;
        LinkedQueue<string> testStructure;
        try                                                    { testStructure.Size(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        LinkedQueue<string> testStructure;
        try                                                    { testStructure.Pop(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Pop from front of list" );
        LinkedQueue<string> testStructure;
        testStructure.m_list.PushBack( "A" );
        testStructure.m_list.PushBack( "B" );
        testStructure.m_list.PushBack( "C" );

        testStructure.Pop();

        totalTests++; testsPassed += Test_DoTheyMatch( 2,             testStructure.Size(), testName, "testStructure.Size()" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "B" ), testStructure.m_list.m_ptrFirst->m_data, testName, "testStructure.m_list.m_ptrFirst->m_data" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "C" ), testStructure.m_list.m_ptrLast->m_data,  testName, "testStructure.m_list.m_ptrLast->m_data" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Can't pop from empty list" );
        LinkedQueue<string> testStructure;
        string exceptionThrown = "";
        string expectedException = "Exception::StructureEmptyException";
        try                                                      { testStructure.Pop(); }
        catch ( const Exception::StructureEmptyException& ex )   { exceptionThrown = "Exception::StructureEmptyException"; }
        catch( ... )                                             { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "list.Pop()" );

        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void LinkedQueueTester::Test_LinkedQueue_Front()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "LinkedQueue::Front";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ IsEmpty implemented" ); totalTests++;
        LinkedQueue<string> testStructure;
        try                                                    { testStructure.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        LinkedQueue<string> testStructure;
        try                                                    { testStructure.Front(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Get Front" );
        LinkedQueue<string> testStructure;
        testStructure.m_list.PushBack( "A" );
        testStructure.m_list.PushBack( "B" );
        testStructure.m_list.PushBack( "C" );

        string expOut = "A";
        string actOut = testStructure.Front();

        totalTests++; testsPassed += Test_DoTheyMatch( expOut, actOut, testName, "testStructure.Front()" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Can't get (Front) from empty list" );
        LinkedQueue<string> testStructure;
        string exceptionThrown = "";
        string expectedException = "Exception::StructureEmptyException";
        try                                                      { testStructure.Front(); }
        catch ( const Exception::StructureEmptyException& ex )   { exceptionThrown = "Exception::StructureEmptyException"; }
        catch( ... )                                             { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "list.Front()" );

        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void LinkedQueueTester::Test_LinkedQueue_Size()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "LinkedQueue::Size";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        LinkedQueue<string> testStructure;
        try                                                    { testStructure.Size(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Size of empty structure?" );
        LinkedQueue<string> testStructure;
        totalTests++; testsPassed += Test_DoTheyMatch( 0, testStructure.Size(), testName, "testStructure.Size()" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Push 2 items, check Size" );
        LinkedQueue<string> testStructure;
        testStructure.m_list.PushBack( "A" );
        testStructure.m_list.PushBack( "B" );
        totalTests++; testsPassed += Test_DoTheyMatch( 2, testStructure.Size(), testName, "testStructure.Size()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void LinkedQueueTester::Test_LinkedQueue_IsEmpty()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "LinkedQueue::IsEmpty";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        LinkedQueue<string> testStructure;
        try                                                    { testStructure.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "IsEmpty() when structure is empty" );
        LinkedQueue<string> testStructure;
        totalTests++; testsPassed += Test_DoTheyMatch( true, testStructure.IsEmpty(), testName, "testStructure.IsEmpty()" );
        Test_End();
    }

    {
        string testName = Test_Begin( "IsEmpty() when structure not empty" );
        LinkedQueue<string> testStructure;
        testStructure.m_list.PushBack( "A" );
        testStructure.m_list.PushBack( "B" );
        totalTests++; testsPassed += Test_DoTheyMatch( false, testStructure.IsEmpty(), testName, "testStructure.IsEmpty()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

