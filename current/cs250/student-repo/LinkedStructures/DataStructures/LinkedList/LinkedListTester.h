#ifndef _LINKED_LIST_TESTER
#define _LINKED_LIST_TESTER

class LinkedListTester
{
public:
    void RunAll();
    void Test_LinkedList_NodeConstructor();
    void Test_LinkedList_Constructor();
    void Test_LinkedList_GetFront();
    void Test_LinkedList_GetBack();
    void Test_LinkedList_GetAt();
    void Test_LinkedList_IsEmpty();
    void Test_LinkedList_Size();
    void Test_LinkedList_PushFront();
    void Test_LinkedList_PushBack();
    void Test_LinkedList_PushAt();
    void Test_LinkedList_PopFront();
    void Test_LinkedList_PopBack();
    void Test_LinkedList_PopAt();
    void Test_LinkedList_Clear();
};

#endif
