#ifndef LINKED_LIST_HPP
#define LINKED_LIST_HPP

// C++ Library includes
#include <iostream>
#include <string>
#include <stdexcept>
using namespace std;

// Project includes
#include "LinkedListNode.h"
#include "../../Exceptions/NotImplementedException.h"
#include "../../Exceptions/InvalidIndexException.h"
#include "../../Exceptions/NullptrException.h"
#include "../../Exceptions/NotImplementedException.h"
#include "../../Exceptions/StructureFullException.h"
#include "../../Exceptions/StructureEmptyException.h"
#include "../../Exceptions/InvalidIndexException.h"
#include "../../Exceptions/ItemNotFoundException.h"
#include "../../Exceptions/NullptrException.h"

template <typename T>
class LinkedList
{
private:
    //! A pointer to the first item in the list
    DoublyLinkedListNode<T>* m_ptrFirst;
    //! A pointer to the last item in the list
    DoublyLinkedListNode<T>* m_ptrLast;
    //! The amount of items stored in the list
    int m_itemCount;

public:
    LinkedList();
    virtual ~LinkedList();

    //! Add a new item to the front of the list
    virtual void PushFront( T newData );
    //! Add a new item to the back of the list
    virtual void PushBack( T newData );
    //! Insert an item at some index in the list
    virtual void PushAt( T newItem, int index );

    //! Remove the front-most item
    virtual void PopFront();
    //! Remove the last item
    virtual void PopBack();
    //! Remove an item in the middle
    virtual void PopAt( int index );

    //! Get the data of the front-most item
    virtual T& GetFront();
    //! Get the data of the back-most item
    virtual T& GetBack();
    //! Subscript operator to get an item at an arbitrary index
    virtual T& GetAt( int index );

    //! Returns the amount of items currently stored in the array.
    virtual int Size() const;

    //! Check if the array is currently empty.
    virtual bool IsEmpty() const;

    //! Clear all items out of the list
    virtual void Clear();

    friend class LinkedListTester;
    friend class LinkedQueueTester;
    friend class LinkedStackTester;
};

template <typename T>
LinkedList<T>::LinkedList()
{
    m_ptrFirst = nullptr;
    m_ptrLast = nullptr;
    m_itemCount = 0;
}

template <typename T>
LinkedList<T>::~LinkedList()
{
    Clear();
}

template <typename T>
void LinkedList<T>::Clear()
{
    while ( m_ptrFirst != nullptr )
    {
        if ( m_itemCount == 1 )
        {
            delete m_ptrFirst;
            m_ptrFirst = nullptr;
            m_ptrLast = nullptr;
        }
        else
        {
            m_ptrFirst = m_ptrFirst->m_ptrNext;
            delete m_ptrFirst->m_ptrPrev;
            m_ptrFirst->m_ptrPrev = nullptr;
        }

        m_itemCount--;
    }
}

template <typename T>
void LinkedList<T>::PushFront( T newData )
{
    throw Exception::NotImplementedException(
        "LinkedList<T>::PushFront" ); // Erase me!
}

template <typename T>
void LinkedList<T>::PushBack( T newData )
{
    throw Exception::NotImplementedException(
        "LinkedList<T>::PushBack" ); // Erase me!
}

template <typename T>
void LinkedList<T>::PushAt( T newItem, int index )
{
    throw Exception::NotImplementedException(
        "LinkedList<T>::PushAt" ); // Erase me!
}

template <typename T>
void LinkedList<T>::PopFront()
{
    throw Exception::NotImplementedException(
        "LinkedList<T>::PopFront" ); // Erase me!
}

template <typename T>
void LinkedList<T>::PopBack()
{
    throw Exception::NotImplementedException(
        "LinkedList<T>::PopBack" ); // Erase me!
}

template <typename T>
void LinkedList<T>::PopAt( int index )
{
    throw Exception::NotImplementedException(
        "LinkedList<T>::PopAt" ); // Erase me!
}

template <typename T>
T& LinkedList<T>::GetFront()
{
    throw Exception::NotImplementedException(
        "LinkedList<T>::GetFront" ); // Erase me!
}

template <typename T>
T& LinkedList<T>::GetBack()
{
    throw Exception::NotImplementedException(
        "LinkedList<T>::GetBack" ); // Erase me!
}

template <typename T>
T& LinkedList<T>::GetAt( int index )
{
    throw Exception::NotImplementedException(
        "LinkedList<T>::GetAt" ); // Erase me!
}

template <typename T>
bool LinkedList<T>::IsEmpty() const
{
    throw Exception::NotImplementedException(
        "LinkedList<T>::IsEmpty" ); // Erase me!
}

template <typename T>
int LinkedList<T>::Size() const
{
    throw Exception::NotImplementedException(
        "LinkedList<T>::Size" ); // Erase me!
}

#endif
