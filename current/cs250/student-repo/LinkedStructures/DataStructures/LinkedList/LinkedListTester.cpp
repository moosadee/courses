#include "LinkedListTester.h"
#include "LinkedListNode.h"
#include "LinkedList.h"
#include "../../Utilities/Style.h"

#include <sstream>

void LinkedListTester::RunAll()
{
    DisplayHeader( "LinkedListTester - RunAll", 0 );
    Test_LinkedList_NodeConstructor();
    Test_LinkedList_Constructor();
    Test_LinkedList_IsEmpty();
    Test_LinkedList_Size();
    Test_LinkedList_Clear();
    Test_LinkedList_PushFront();
    Test_LinkedList_PushBack();
    Test_LinkedList_PushAt();
    Test_LinkedList_PopFront();
    Test_LinkedList_PopBack();
    Test_LinkedList_PopAt();
    Test_LinkedList_GetFront();
    Test_LinkedList_GetBack();
    Test_LinkedList_GetAt();
}

void LinkedListTester::Test_LinkedList_NodeConstructor()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "LinkedListNode Constructor";
    ostringstream oss;

    {
        string testName = "New Node check";
        DoublyLinkedListNode<int> testNode;
        oss << testNode.m_ptrNext;
        totalTests++; testsPassed += Test_DoTheyMatch( oss.str(), string( "0" ), testName, "node.m_ptrNext" );

        oss.str( "" );
        oss.clear();
        oss << testNode.m_ptrPrev;
        totalTests++; testsPassed += Test_DoTheyMatch( oss.str(), string( "0" ), testName, "node.m_ptrPrev" );
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void LinkedListTester::Test_LinkedList_Constructor()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "LinkedList Constructor";
    ostringstream oss;

    {
        string testName = "New LinkedList check";
        LinkedList<int> testList;
        oss << testList.m_ptrFirst;
        totalTests++; testsPassed += Test_DoTheyMatch( oss.str(), string( "0" ), testName, "node.m_ptrFirst" );

        oss.str( "" );
        oss.clear();
        oss << testList.m_ptrLast;
        totalTests++; testsPassed += Test_DoTheyMatch( oss.str(), string( "0" ), testName, "node.m_ptrLast" );
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void LinkedListTester::Test_LinkedList_GetFront()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "LinkedList::GetFront";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ IsEmpty implemented" ); totalTests++;
        LinkedList<string> testList;
        try                                                    { testList.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        LinkedList<string> testList;
        try                                                    { testList.GetFront(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Get from middle of list" );
        LinkedList<string> testList;
        DoublyLinkedListNode<string>* nodeA = new DoublyLinkedListNode<string>; nodeA->m_data = "A";
        DoublyLinkedListNode<string>* nodeB = new DoublyLinkedListNode<string>; nodeB->m_data = "B";
        DoublyLinkedListNode<string>* nodeC = new DoublyLinkedListNode<string>; nodeC->m_data = "C";
        DoublyLinkedListNode<string>* nodeD = new DoublyLinkedListNode<string>; nodeD->m_data = "D";

        testList.m_ptrFirst = nodeA;
        testList.m_ptrFirst->m_ptrNext = nodeB;
        testList.m_ptrFirst->m_ptrNext->m_ptrNext = nodeC;
        testList.m_ptrFirst->m_ptrNext->m_ptrNext->m_ptrNext = nodeD;
        testList.m_ptrLast = nodeD;
        testList.m_ptrLast->m_ptrPrev = nodeC;
        testList.m_ptrLast->m_ptrPrev->m_ptrPrev = nodeB;
        testList.m_ptrLast->m_ptrPrev->m_ptrPrev->m_ptrPrev = nodeA;
        testList.m_itemCount = 4;

        totalTests++; testsPassed += Test_DoTheyMatch( string( "A" ), testList.GetFront(), testName, "testList.GetFront()" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Can't get from empty list" );
        LinkedList<string> testList;
        string exceptionThrown = "";
        string expectedException = "Exception::StructureEmptyException";
        try                                                      { testList.GetFront(); }
        catch ( const Exception::StructureEmptyException& ex )   { exceptionThrown = "Exception::StructureEmptyException"; }
        catch( ... )                                             { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "list.GetFront()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void LinkedListTester::Test_LinkedList_GetBack()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "LinkedList::GetBack";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ IsEmpty implemented" ); totalTests++;
        LinkedList<string> testList;
        try                                                    { testList.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        LinkedList<string> testList;
        try                                                    { testList.GetBack(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Get from middle of list" );
        LinkedList<string> testList;
        DoublyLinkedListNode<string>* nodeA = new DoublyLinkedListNode<string>; nodeA->m_data = "A";
        DoublyLinkedListNode<string>* nodeB = new DoublyLinkedListNode<string>; nodeB->m_data = "B";
        DoublyLinkedListNode<string>* nodeC = new DoublyLinkedListNode<string>; nodeC->m_data = "C";
        DoublyLinkedListNode<string>* nodeD = new DoublyLinkedListNode<string>; nodeD->m_data = "D";

        testList.m_ptrFirst = nodeA;
        testList.m_ptrFirst->m_ptrNext = nodeB;
        testList.m_ptrFirst->m_ptrNext->m_ptrNext = nodeC;
        testList.m_ptrFirst->m_ptrNext->m_ptrNext->m_ptrNext = nodeD;
        testList.m_ptrLast = nodeD;
        testList.m_ptrLast->m_ptrPrev = nodeC;
        testList.m_ptrLast->m_ptrPrev->m_ptrPrev = nodeB;
        testList.m_ptrLast->m_ptrPrev->m_ptrPrev->m_ptrPrev = nodeA;
        testList.m_itemCount = 4;

        totalTests++; testsPassed += Test_DoTheyMatch( string( "D" ), testList.GetBack(), testName, "testList.GetBack()" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Can't get from empty list" );
        LinkedList<string> testList;
        string exceptionThrown = "";
        string expectedException = "Exception::StructureEmptyException";
        try                                                      { testList.GetBack(); }
        catch ( const Exception::StructureEmptyException& ex )   { exceptionThrown = "Exception::StructureEmptyException"; }
        catch( ... )                                             { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "list.GetBack()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void LinkedListTester::Test_LinkedList_GetAt()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "LinkedList::GetAt";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ IsEmpty implemented" ); totalTests++;
        LinkedList<string> testList;
        try                                                    { testList.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        LinkedList<string> testList;
        try                                                    { testList.GetAt( 2 ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Get from middle of list" );
        LinkedList<string> testList;
        DoublyLinkedListNode<string>* nodeA = new DoublyLinkedListNode<string>; nodeA->m_data = "A";
        DoublyLinkedListNode<string>* nodeB = new DoublyLinkedListNode<string>; nodeB->m_data = "B";
        DoublyLinkedListNode<string>* nodeC = new DoublyLinkedListNode<string>; nodeC->m_data = "C";
        DoublyLinkedListNode<string>* nodeD = new DoublyLinkedListNode<string>; nodeD->m_data = "D";

        testList.m_ptrFirst = nodeA;
        testList.m_ptrFirst->m_ptrNext = nodeB;
        testList.m_ptrFirst->m_ptrNext->m_ptrNext = nodeC;
        testList.m_ptrFirst->m_ptrNext->m_ptrNext->m_ptrNext = nodeD;
        testList.m_ptrLast = nodeD;
        testList.m_ptrLast->m_ptrPrev = nodeC;
        testList.m_ptrLast->m_ptrPrev->m_ptrPrev = nodeB;
        testList.m_ptrLast->m_ptrPrev->m_ptrPrev->m_ptrPrev = nodeA;
        testList.m_itemCount = 4;

        totalTests++; testsPassed += Test_DoTheyMatch( string( "A" ), testList.GetAt( 0 ), testName, "testList.GetAt(0)" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "B" ), testList.GetAt( 1 ), testName, "testList.GetAt(1)" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "C" ), testList.GetAt( 2 ), testName, "testList.GetAt(2)" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "D" ), testList.GetAt( 3 ), testName, "testList.GetAt(3)" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Can't get from empty list" );
        LinkedList<string> testList;
        string exceptionThrown = "";
        string expectedException = "Exception::StructureEmptyException";
        try                                                      { testList.GetAt( 0 ); }
        catch ( const Exception::StructureEmptyException& ex )   { exceptionThrown = "Exception::StructureEmptyException"; }
        catch( ... )                                             { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "list.GetAt( 0 )" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Negative index marked invalid?" );
        LinkedList<string> testList;
        testList.m_itemCount = 5;
        string exceptionThrown = "";
        string expectedException = "Exception::InvalidIndexException";
        try                                                    { testList.GetAt( -5 ); }
        catch ( const Exception::InvalidIndexException& ex )   { exceptionThrown = "Exception::InvalidIndexException"; }
        catch( ... )                                           { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "list.GetAt( -5 )" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Out-of-bounds index marked invalid?" );
        LinkedList<string> testList;
        testList.m_itemCount = 5;
        string exceptionThrown = "";
        string expectedException = "Exception::InvalidIndexException";
        try                                                    { testList.GetAt( 6 ); }
        catch ( const Exception::InvalidIndexException& ex )   { exceptionThrown = "Exception::InvalidIndexException"; }
        catch( ... )                                           { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "list.GetAt( 6 )" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void LinkedListTester::Test_LinkedList_IsEmpty()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "LinkedList::IsEmpty";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        LinkedList<string> testList;
        try                                                    { testList.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "IsEmpty when empty?" );
        LinkedList<string> testList;
        totalTests++; testsPassed += Test_DoTheyMatch( true, testList.IsEmpty(), testName, "list.IsEmpty()" );
        Test_End();
    }

    {
        string testName = Test_Begin( "IsEmpty when not empty?" );
        LinkedList<string> testList;
        testList.m_itemCount = 1;
        totalTests++; testsPassed += Test_DoTheyMatch( false, testList.IsEmpty(), testName, "list.IsEmpty()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void LinkedListTester::Test_LinkedList_Size()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "LinkedList::Size";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        LinkedList<string> testList;
        try                                                    { testList.Size(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Size?" );
        LinkedList<string> testList;
        totalTests++; testsPassed += Test_DoTheyMatch( 0, testList.Size(), testName, "list.Size()" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Size?" );
        LinkedList<string> testList;
        testList.m_itemCount = 5;
        totalTests++; testsPassed += Test_DoTheyMatch( 5, testList.Size(), testName, "list.Size()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void LinkedListTester::Test_LinkedList_PushFront()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "LinkedList::PushFront";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ IsEmpty implemented" ); totalTests++;
        LinkedList<string> testList;
        try                                                    { testList.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        LinkedList<string> testList;
        try                                                    { testList.PushFront( "A" ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Push 1 item" );
        LinkedList<string> testList;
        testList.PushFront( "cheese" );
        string exp1 = "not nullptr", exp2 = "not nullptr";
        string act1 = ( testList.m_ptrFirst == nullptr ) ? "nullptr" : "not nullptr";
        string act2 = ( testList.m_ptrLast == nullptr  ) ? "nullptr" : "not nullptr";
        totalTests++; testsPassed += Test_DoTheyMatch( act1, exp1, testName, "testList.m_ptrFirst" );
        totalTests++; testsPassed += Test_DoTheyMatch( act2, exp2, testName, "testList.m_ptrLast" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "cheese" ), testList.m_ptrFirst->m_data, testName, "testList.m_ptrFirst->m_data" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "cheese" ), testList.m_ptrLast->m_data, testName, "testList.m_ptrLast->m_data" );
        totalTests++; testsPassed += Test_DoTheyMatch( 1, testList.m_itemCount, testName, "testList.m_itemCount" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Push 2 items" );
        LinkedList<string> testList;
        testList.PushFront( "A" );
        testList.PushFront( "B" );
        string exp1 = "not nullptr", exp2 = "not nullptr";
        string act1 = ( testList.m_ptrFirst == nullptr ) ? "nullptr" : "not nullptr";
        string act2 = ( testList.m_ptrLast == nullptr  ) ? "nullptr" : "not nullptr";
        totalTests++; testsPassed += Test_DoTheyMatch( act1, exp1, testName, "testList.m_ptrFirst" );
        totalTests++; testsPassed += Test_DoTheyMatch( act2, exp2, testName, "testList.m_ptrLast" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "B" ), testList.m_ptrFirst->m_data, testName, "testList.m_ptrFirst->m_data" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "A" ), testList.m_ptrLast->m_data, testName, "testList.m_ptrLast->m_data" );
        totalTests++; testsPassed += Test_DoTheyMatch( 2, testList.m_itemCount, testName, "testList.m_itemCount" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void LinkedListTester::Test_LinkedList_PushBack()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "LinkedList::PushBack";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ IsEmpty implemented" ); totalTests++;
        LinkedList<string> testList;
        try                                                    { testList.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        LinkedList<string> testList;
        try                                                    { testList.PushBack( "A" ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Push 1 item" );
        LinkedList<string> testList;
        testList.PushBack( "cheese" );
        string exp1 = "not nullptr", exp2 = "not nullptr";
        string act1 = ( testList.m_ptrFirst == nullptr ) ? "nullptr" : "not nullptr";
        string act2 = ( testList.m_ptrLast == nullptr  ) ? "nullptr" : "not nullptr";
        totalTests++; testsPassed += Test_DoTheyMatch( act1, exp1, testName, "testList.m_ptrFirst" );
        totalTests++; testsPassed += Test_DoTheyMatch( act2, exp2, testName, "testList.m_ptrLast" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "cheese" ), testList.m_ptrFirst->m_data, testName, "testList.m_ptrFirst->m_data" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "cheese" ), testList.m_ptrLast->m_data, testName, "testList.m_ptrLast->m_data" );
        totalTests++; testsPassed += Test_DoTheyMatch( 1, testList.m_itemCount, testName, "testList.m_itemCount" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Push 2 items, check order" );
        LinkedList<string> testList;
        testList.PushBack( "A" );
        testList.PushBack( "B" );
        string exp1 = "not nullptr", exp2 = "not nullptr";
        string act1 = ( testList.m_ptrFirst == nullptr ) ? "nullptr" : "not nullptr";
        string act2 = ( testList.m_ptrLast == nullptr  ) ? "nullptr" : "not nullptr";
        totalTests++; testsPassed += Test_DoTheyMatch( act1, exp1, testName, "testList.m_ptrFirst" );
        totalTests++; testsPassed += Test_DoTheyMatch( act2, exp2, testName, "testList.m_ptrLast" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "A" ), testList.m_ptrFirst->m_data, testName, "testList.m_ptrFirst->m_data" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "B" ), testList.m_ptrLast->m_data, testName, "testList.m_ptrLast->m_data" );
        totalTests++; testsPassed += Test_DoTheyMatch( 2, testList.m_itemCount, testName, "testList.m_itemCount" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void LinkedListTester::Test_LinkedList_PushAt()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "LinkedList::PushAt";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ IsEmpty implemented" ); totalTests++;
        LinkedList<string> testList;
        try                                                    { testList.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Check if PREREQ PushFront implemented" ); totalTests++;
        LinkedList<string> testList;
        try                                                    { testList.PushFront( "A" ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Check if PREREQ PushBack implemented" ); totalTests++;
        LinkedList<string> testList;
        try                                                    { testList.PushBack( "A" ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        LinkedList<string> testList;
        try                                                    { testList.PushAt( "A", 2 ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }


    {
        string testName = Test_Begin( "Push to middle of two items" );
        LinkedList<string> testList;
        testList.m_itemCount = 2;
        testList.m_ptrFirst = new DoublyLinkedListNode<string>;
        testList.m_ptrLast = new DoublyLinkedListNode<string>;

        testList.m_ptrFirst->m_data = "A";
        testList.m_ptrLast->m_data = "B";
        testList.m_ptrFirst->m_ptrNext = testList.m_ptrLast;
        testList.m_ptrLast->m_ptrPrev = testList.m_ptrFirst;

        testList.PushAt( "Z", 1 );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "A" ), testList.m_ptrFirst->m_data, testName, "testList.m_ptrFirstm_data" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "Z" ), testList.m_ptrFirst->m_ptrNext->m_data, testName, "testList.m_ptrFirst->m_ptrNext->m_data" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "B" ), testList.m_ptrLast->m_data, testName, "testList.m_ptrLast->m_data" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "Z" ), testList.m_ptrLast->m_ptrPrev->m_data, testName, "testList.m_ptrLast->m_ptrPrev->m_data" );
        totalTests++; testsPassed += Test_DoTheyMatch( 3, testList.m_itemCount, testName, "testList.m_itemCount" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Negative index marked invalid?" );
        LinkedList<string> testList;
        testList.m_itemCount = 5;
        string exceptionThrown = "";
        string expectedException = "Exception::InvalidIndexException";
        try                                                    { testList.PushAt( "A", -5 ); }
        catch ( const Exception::InvalidIndexException& ex )   { exceptionThrown = "Exception::InvalidIndexException"; }
        catch( ... )                                           { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "list.PushAt( \"A\", -5 )" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Out-of-bounds index marked invalid?" );
        LinkedList<string> testList;
        testList.m_itemCount = 5;
        string exceptionThrown = "";
        string expectedException = "Exception::InvalidIndexException";
        try                                                    { testList.PushAt( "A", 6 ); }
        catch ( const Exception::InvalidIndexException& ex )   { exceptionThrown = "Exception::InvalidIndexException"; }
        catch( ... )                                           { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "list.PushAt( \"A\", 6 )" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void LinkedListTester::Test_LinkedList_PopFront()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "LinkedList::PopFront";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ IsEmpty implemented" ); totalTests++;
        LinkedList<string> testList;
        try                                                    { testList.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        LinkedList<string> testList;
        try                                                    { testList.PopFront(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Pop from front of list" );
        LinkedList<string> testList;
        DoublyLinkedListNode<string>* nodeA = new DoublyLinkedListNode<string>; nodeA->m_data = "A";
        DoublyLinkedListNode<string>* nodeB = new DoublyLinkedListNode<string>; nodeB->m_data = "B";
        DoublyLinkedListNode<string>* nodeC = new DoublyLinkedListNode<string>; nodeC->m_data = "C";
        DoublyLinkedListNode<string>* nodeD = new DoublyLinkedListNode<string>; nodeD->m_data = "D";

        testList.m_ptrFirst = nodeA;
        testList.m_ptrFirst->m_ptrNext = nodeB;
        testList.m_ptrFirst->m_ptrNext->m_ptrNext = nodeC;
        testList.m_ptrFirst->m_ptrNext->m_ptrNext->m_ptrNext = nodeD;
        testList.m_ptrLast = nodeD;
        testList.m_ptrLast->m_ptrPrev = nodeC;
        testList.m_ptrLast->m_ptrPrev->m_ptrPrev = nodeB;
        testList.m_ptrLast->m_ptrPrev->m_ptrPrev->m_ptrPrev = nodeA;
        testList.m_itemCount = 4;

        testList.PopFront();

        totalTests++; testsPassed += Test_DoTheyMatch( 3,             testList.m_itemCount, testName, "testList.m_itemCount" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "B" ), testList.m_ptrFirst->m_data, testName, "testList.m_ptrFirst->m_data" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "C" ), testList.m_ptrFirst->m_ptrNext->m_data, testName, "testList.m_ptrFirst->m_ptrNext->m_data" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "D" ), testList.m_ptrLast->m_data, testName, "testList.m_ptrLast->m_data" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Pop last item from list" );
        LinkedList<string> testList;
        testList.m_ptrFirst = new DoublyLinkedListNode<string>;
        testList.m_ptrFirst->m_data = "LAST";
        testList.m_ptrLast = testList.m_ptrFirst;
        testList.m_itemCount = 1;
        testList.PopFront();
        totalTests++; testsPassed += Test_DoTheyMatch( 0, testList.m_itemCount, testName, "testList.m_itemCount" );
        string exp1 = "nullptr", exp2 = "nullptr";
        string act1 = ( testList.m_ptrFirst == nullptr ) ? "nullptr" : "not nullptr";
        string act2 = ( testList.m_ptrLast == nullptr  ) ? "nullptr" : "not nullptr";
        totalTests++; testsPassed += Test_DoTheyMatch( act1, exp1, testName, "testList.m_ptrFirst" );
        totalTests++; testsPassed += Test_DoTheyMatch( act2, exp2, testName, "testList.m_ptrLast" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Can't pop from empty list" );
        LinkedList<string> testList;
        string exceptionThrown = "";
        string expectedException = "Exception::StructureEmptyException";
        try                                                      { testList.PopFront(); }
        catch ( const Exception::StructureEmptyException& ex )   { exceptionThrown = "Exception::StructureEmptyException"; }
        catch( ... )                                             { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "list.PopFront()" );

        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void LinkedListTester::Test_LinkedList_PopBack()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "LinkedList::PopBack";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ IsEmpty implemented" ); totalTests++;
        LinkedList<string> testList;
        try                                                    { testList.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        LinkedList<string> testList;
        try                                                    { testList.PopBack(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Pop from back of list" );
        LinkedList<string> testList;
        DoublyLinkedListNode<string>* nodeA = new DoublyLinkedListNode<string>; nodeA->m_data = "A";
        DoublyLinkedListNode<string>* nodeB = new DoublyLinkedListNode<string>; nodeB->m_data = "B";
        DoublyLinkedListNode<string>* nodeC = new DoublyLinkedListNode<string>; nodeC->m_data = "C";
        DoublyLinkedListNode<string>* nodeD = new DoublyLinkedListNode<string>; nodeD->m_data = "D";

        testList.m_ptrFirst = nodeA;
        testList.m_ptrFirst->m_ptrNext = nodeB;
        testList.m_ptrFirst->m_ptrNext->m_ptrNext = nodeC;
        testList.m_ptrFirst->m_ptrNext->m_ptrNext->m_ptrNext = nodeD;
        testList.m_ptrLast = nodeD;
        testList.m_ptrLast->m_ptrPrev = nodeC;
        testList.m_ptrLast->m_ptrPrev->m_ptrPrev = nodeB;
        testList.m_ptrLast->m_ptrPrev->m_ptrPrev->m_ptrPrev = nodeA;
        testList.m_itemCount = 4;

        testList.PopBack();

        totalTests++; testsPassed += Test_DoTheyMatch( 3,             testList.m_itemCount, testName, "testList.m_itemCount" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "A" ), testList.m_ptrFirst->m_data, testName, "testList.m_ptrFirst->m_data" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "B" ), testList.m_ptrFirst->m_ptrNext->m_data, testName, "testList.m_ptrFirst->m_ptrNext->m_data" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "C" ), testList.m_ptrLast->m_data, testName, "testList.m_ptrLast->m_data" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Pop last item from list" );
        LinkedList<string> testList;
        testList.m_ptrFirst = new DoublyLinkedListNode<string>;
        testList.m_ptrFirst->m_data = "LAST";
        testList.m_ptrLast = testList.m_ptrFirst;
        testList.m_itemCount = 1;
        testList.PopBack();
        totalTests++; testsPassed += Test_DoTheyMatch( 0, testList.m_itemCount, testName, "testList.m_itemCount" );
        string exp1 = "nullptr", exp2 = "nullptr";
        string act1 = ( testList.m_ptrFirst == nullptr ) ? "nullptr" : "not nullptr";
        string act2 = ( testList.m_ptrLast == nullptr  ) ? "nullptr" : "not nullptr";
        totalTests++; testsPassed += Test_DoTheyMatch( act1, exp1, testName, "testList.m_ptrFirst" );
        totalTests++; testsPassed += Test_DoTheyMatch( act2, exp2, testName, "testList.m_ptrLast" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Can't pop from empty list" );
        LinkedList<string> testList;
        string exceptionThrown = "";
        string expectedException = "Exception::StructureEmptyException";
        try                                                      { testList.PopBack(); }
        catch ( const Exception::StructureEmptyException& ex )   { exceptionThrown = "Exception::StructureEmptyException"; }
        catch( ... )                                             { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "list.PopBack()" );

        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void LinkedListTester::Test_LinkedList_PopAt()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "LinkedList::PopAt";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ IsEmpty implemented" ); totalTests++;
        LinkedList<string> testList;
        try                                                    { testList.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        LinkedList<string> testList;
        try                                                    { testList.PopAt( 2 ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Pop from middle of list" );
        LinkedList<string> testList;
        DoublyLinkedListNode<string>* nodeA = new DoublyLinkedListNode<string>; nodeA->m_data = "A";
        DoublyLinkedListNode<string>* nodeB = new DoublyLinkedListNode<string>; nodeB->m_data = "B";
        DoublyLinkedListNode<string>* nodeC = new DoublyLinkedListNode<string>; nodeC->m_data = "C";
        DoublyLinkedListNode<string>* nodeD = new DoublyLinkedListNode<string>; nodeD->m_data = "D";

        testList.m_ptrFirst = nodeA;
        testList.m_ptrFirst->m_ptrNext = nodeB;
        testList.m_ptrFirst->m_ptrNext->m_ptrNext = nodeC;
        testList.m_ptrFirst->m_ptrNext->m_ptrNext->m_ptrNext = nodeD;
        testList.m_ptrLast = nodeD;
        testList.m_ptrLast->m_ptrPrev = nodeC;
        testList.m_ptrLast->m_ptrPrev->m_ptrPrev = nodeB;
        testList.m_ptrLast->m_ptrPrev->m_ptrPrev->m_ptrPrev = nodeA;
        testList.m_itemCount = 4;

        testList.PopAt( 1 );

        totalTests++; testsPassed += Test_DoTheyMatch( 3,             testList.m_itemCount, testName, "testList.m_itemCount" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "A" ), testList.m_ptrFirst->m_data, testName, "testList.m_ptrFirst->m_data" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "C" ), testList.m_ptrFirst->m_ptrNext->m_data, testName, "testList.m_ptrFirst->m_ptrNext->m_data" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "D" ), testList.m_ptrLast->m_data, testName, "testList.m_ptrLast->m_data" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Can't pop from empty list" );
        LinkedList<string> testList;
        string exceptionThrown = "";
        string expectedException = "Exception::StructureEmptyException";
        try                                                    { testList.PopAt( 0 ); }
        catch ( const Exception::StructureEmptyException& ex )   { exceptionThrown = "Exception::StructureEmptyException"; }
        catch( ... )                                           { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "list.PopAt( 0 )" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Negative index marked invalid?" );
        LinkedList<string> testList;
        testList.m_itemCount = 5;
        string exceptionThrown = "";
        string expectedException = "Exception::InvalidIndexException";
        try                                                    { testList.PopAt( -5 ); }
        catch ( const Exception::InvalidIndexException& ex )   { exceptionThrown = "Exception::InvalidIndexException"; }
        catch( ... )                                           { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "list.PopAt( -5 )" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Out-of-bounds index marked invalid?" );
        LinkedList<string> testList;
        testList.m_itemCount = 5;
        string exceptionThrown = "";
        string expectedException = "Exception::InvalidIndexException";
        try                                                    { testList.PopAt( 6 ); }
        catch ( const Exception::InvalidIndexException& ex )   { exceptionThrown = "Exception::InvalidIndexException"; }
        catch( ... )                                           { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "list.PopAt( 6 )" );

        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void LinkedListTester::Test_LinkedList_Clear()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "LinkedList::Clear";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        LinkedList<string> testList;
        try                                                    { testList.Clear(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Check cleanup after Clear()" );
        ostringstream oss;
        LinkedList<int> testList;
        testList.m_ptrFirst = new DoublyLinkedListNode<int>;
        testList.m_ptrLast = testList.m_ptrFirst;
        testList.m_ptrFirst->m_data = 20;
        testList.m_itemCount = 1;

        testList.Clear();

        totalTests++; testsPassed += Test_DoTheyMatch( 0, testList.m_itemCount, testName, "list.m_itemCount" );

        oss << testList.m_ptrFirst;
        totalTests++; testsPassed += Test_DoTheyMatch( oss.str(), string( "0" ), testName, "list.m_ptrFirst" );

        oss.str( "" );
        oss.clear();

        oss << testList.m_ptrLast;
        totalTests++; testsPassed += Test_DoTheyMatch( oss.str(), string( "0" ), testName, "list.m_ptrLast" );

        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

