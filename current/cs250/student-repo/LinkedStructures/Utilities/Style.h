#ifndef _STYLE
#define _STYLE

#include <iostream>
#include <string>
#include <vector>
using namespace std;

const string CLEAR    = "\033[0m";
const string BLACK    = "\033[3;30m";
const string RED      = "\033[0;31m";
const string GREEN    = "\033[0;32m";
const string YELLOW   = "\033[0;33m";
const string BLUE     = "\033[0;34m";
const string MAGENTA  = "\033[0;35m";
const string CYAN     = "\033[0;36m";
const string WHITE    = "\033[0;37m";

void DisplayHeader( string text, int level = 0 );
string GetChoice( vector<string> options, int level = 0 );
void Test_CheckPrereqImplemented();
void Test_PassDisplay( string testName, string label );
string Test_Begin( string testName );
void Test_End();

template <typename T>
void DisplayVector( const std::vector<T> vec, bool showIndex )
{
    std::cout << "{ ";

    for ( size_t i = 0; i < vec.size(); i++ )
    {
        if ( i > 0 ) { std::cout << ", "; }
        if ( showIndex ) { std::cout << i << "="; }
        std::cout << vec[i];
    }

    std::cout << " }";
}

template <typename T>
void DisplayArray( const T arr[], size_t length, bool showIndex )
{
    std::cout << "{ ";

    for ( size_t i = 0; i < length; i++ )
    {
        if ( i > 0 ) { std::cout << ", "; }
        if ( showIndex ) { std::cout << i << "="; }
        std::cout << arr[i];
    }

    std::cout << " }";
}

template <typename T>
void Test_FailDisplayExpectedActual( string testName, string label, T expect, T actual )
{
    cout << RED << " [FAIL] " << testName << endl;
    cout << " - EXPECTED " << label << ": " << expect << endl;
    cout << " - ACTUAL   " << label << ": " << actual << endl;
    cout << endl;
}

template <typename T>
int Test_DoTheyMatch( T expect, T actual, string testName, string label )
{
    if ( actual == expect )
    {
        Test_PassDisplay( testName, label );
        return 1;
    }
    else
    {
        Test_FailDisplayExpectedActual( testName, label, expect, actual );
        return 0;
    }
}


#endif
