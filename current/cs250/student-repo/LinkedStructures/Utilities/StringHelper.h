#ifndef _STRINGHELPER
#define _STRINGHELPER

#include <sstream>
#include <string>

std::string StringToLower( const std::string& val );

template <typename T>
std::string ToString( const T& value )
{
    std::stringstream ss;
    ss << value;
    return ss.str();
}

#endif
