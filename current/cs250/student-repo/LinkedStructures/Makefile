COMPILE_OBJ = g++ -std=c++17 -c -g
OBJ_PATH = temp_obj/
DEBUG_EXE = debug.exe
OBJECTS = $(OBJ_PATH)Utilities/StringHelper.o $(OBJ_PATH)Utilities/Style.o $(OBJ_PATH)DataStructures/LinkedList/LinkedListTester.o $(OBJ_PATH)DataStructures/LinkedQueue/LinkedQueueTester.o $(OBJ_PATH)DataStructures/LinkedStack/LinkedStackTester.o $(OBJ_PATH)main.o 
SRCFILES = Utilities/*.h Utilities/*.cpp Exceptions/*.h DataStructures/LinkedList/*.h DataStructures/LinkedList/*.cpp DataStructures/LinkedQueue/*.h DataStructures/LinkedQueue/*.cpp DataStructures/LinkedStack/*.h DataStructures/LinkedStack/*.cpp main.cpp
RED = "\033[0;31m"
GREEN = "\033[0;32m"
CLEAR = "\033[0m"

ifeq ($(OS),Windows_NT)
	TESTDIRECTORY=if exist
	MAKEDIRECTORY=mkdir
	REMOVEDIRECTORY=rd /s /q
	REMOVEFLAGS=
	REMOVEFILE=del
	FIXPATH = $(subst /,\,$1)
else
	TESTDIRECTORY=echo
	MAKEDIRECTORY=mkdir -p
	REMOVEDIRECTORY=rm -r
	REMOVEFLAGS=
	REMOVEFILE=rm
	FIXPATH = $1
endif

default: debug
	@echo "See also other make commands:"
	@echo "- make help"
	@echo "- make debug"
	@echo "- make fallback"
	@echo "- make clean"
	@echo ""
	@echo $(GREEN)
	@echo "DONE! I hope. That should have made \"$(DEBUG_EXE)\"!"
	@echo $(CLEAR)
	@echo "You can run the program in one of these ways:"
	@echo "  ./$(DEBUG_EXE) test                Run the unit tests"
	@echo "  gdb $(DEBUG_EXE)                   Run the program in the debugger"

help:
	@echo "MAKE... \n"
	@echo "DEBUG BUILD: type \"make debug\""
	@echo ""
	@echo "If that doesn't work, try \"make fallback\"."
	@echo ""
	@echo "FRESH BUILD: type \"make clean\" to rebuild everything."
	@echo "        TIP: Delete the specific files in \"temp_obj\" folder in"
	@echo "             order to rebuild just certain files!"
	@echo ""
	@echo "Once build is done you can run the program in one of these ways:"
	@echo "  ./$(DEBUG_EXE) test                Run the unit tests"
	@echo "  gdb $(DEBUG_EXE)                   Run the program in the debugger"
	@echo ""

clean:
	@echo "DETECTED OS: $(OS)"
	$(REMOVEDIRECTORY) $(call FIXPATH,$(OBJ_PATH))
	$(REMOVEFILE) *.exe

fallback:
	@echo "Please wait, this will take a while... (Use CTRL+C to cancel)"
	@echo "..."
	g++ -g $(SRCFILES)  -std=c++17 -o debug.exe
	@echo ""
	@echo $(GREEN)
	@echo "DONE! I hope. That should have made \"$(DEBUG_EXE)\"!"
	@echo $(CLEAR)
	@echo "Once build is done you can run the program in one of these ways:"
	@echo "  ./$(DEBUG_EXE) test                Run the unit tests"
	@echo "  gdb $(DEBUG_EXE)                   Run program in the debugger"

debug: debug.exe

debug.exe: debug_exe
	@echo "Building \"debug.exe\"..."
	g++ -o debug.exe $(OBJECTS)

debug_exe: debug_setup debug_objects

debug_setup:
	@echo ""
	@echo "DETECTED OS: $(OS)"
	@echo ""
	@echo "Check $(OBJ_PATH) and remove if exists..."
	$(TESTDIRECTORY) $(call FIXPATH,$(OBJ_PATH)) $(REMOVEDIRECTORY) $(call FIXPATH,$(OBJ_PATH)) $(REMOVEFLAGS)
	@echo ""
	@echo "Make object directories..."
	$(MAKEDIRECTORY) $(call FIXPATH,$(OBJ_PATH))
	$(MAKEDIRECTORY) $(call FIXPATH,$(OBJ_PATH)Utilities)
	$(MAKEDIRECTORY) $(call FIXPATH,$(OBJ_PATH)DataStructures)
	$(MAKEDIRECTORY) $(call FIXPATH,$(OBJ_PATH)DataStructures/LinkedQueue)
	$(MAKEDIRECTORY) $(call FIXPATH,$(OBJ_PATH)DataStructures/LinkedStack)
	$(MAKEDIRECTORY) $(call FIXPATH,$(OBJ_PATH)DataStructures/LinkedList)

debug_objects: $(OBJECTS)

$(OBJ_PATH)main.o:
	$(COMPILE_OBJ) main.cpp -o $(OBJ_PATH)main.o

$(OBJ_PATH)Utilities/StringHelper.o:
	$(COMPILE_OBJ) Utilities/StringHelper.cpp -o $(OBJ_PATH)Utilities/StringHelper.o

$(OBJ_PATH)Utilities/Style.o:
	$(COMPILE_OBJ) Utilities/Style.cpp -o $(OBJ_PATH)Utilities/Style.o

$(OBJ_PATH)DataStructures/LinkedList/LinkedListTester.o:
	$(COMPILE_OBJ) DataStructures/LinkedList/LinkedListTester.cpp -o $(OBJ_PATH)DataStructures/LinkedList/LinkedListTester.o

$(OBJ_PATH)DataStructures/LinkedQueue/LinkedQueueTester.o:
	$(COMPILE_OBJ) DataStructures/LinkedQueue/LinkedQueueTester.cpp -o $(OBJ_PATH)DataStructures/LinkedQueue/LinkedQueueTester.o

$(OBJ_PATH)DataStructures/LinkedStack/LinkedStackTester.o:
	$(COMPILE_OBJ) DataStructures/LinkedStack/LinkedStackTester.cpp -o $(OBJ_PATH)DataStructures/LinkedStack/LinkedStackTester.o

.PHONY: clean all debug_objects debug_setup debug_exe debug fallback help default
