#include <iostream>
#include <vector>
#include <string>
using namespace std;

// Include data structures tests
#include "DataStructures/SmartTable/SmartTableTester.h"
#include "DataStructures/HashTable/HashTableTester.h"

// Utilities
#include "Utilities/StringHelper.h"

int main( int argCount, char* args[] )
{
    std::vector<std::string> structures;
    if ( argCount < 2 )
    {
        cout << "Expected: " << args[0] << " datastructure1 datastructure2 ..." << endl;
        cout << "  This will run the unit tests for those structures" << endl;
        cout << "  Options: hashtable smarttable all" << endl;
        return 1;
    }
    else if ( argCount == 2 && string( args[1] ) == "all" )
    {
        structures.push_back( "hashtable" );
        structures.push_back( "smarttable" );
    }
    else if ( argCount == 2 && string( args[1] ) == "test" )
    {
        structures.push_back( "hashtable" );
    }
    else
    {
        for ( int i = 1; i < argCount; i++ )
        {
            std::string lower = StringToLower( std::string( args[i] ) );
            structures.push_back( args[i] );
        }
    }

    // Run tests for all structures given
    for ( string ds : structures )
    {
        if      ( ds == "hashtable" )   { HashTableTester tester; tester.RunAll(); }
        else if ( ds == "smarttable" )  { SmartTableTester tester; tester.RunAll(); }
    }
}
