#ifndef _HASH_TABLE
#define _HASH_TABLE

#include "HashItem.h"
#include "../SmartTable/SmartTable.h"
#include "../../Exceptions/StructureFullException.h"

//! An enumeration to define what kind of collision method the hash table is using.
enum class CollisionMethod
{
    LINEAR,
    QUADRATIC,
    DOUBLE_HASH
};

template <typename T>
//! A key/value based data structure
class HashTable
{
public:
    HashTable( int size );
    HashTable();
    ~HashTable();

    void SetCollisionMethod( CollisionMethod method );
    void Push(size_t key, T data );
    T& Get( int key );
    int Size();

private:
    int Hash1( int key );
    int LinearProbe( int originalIndex, int collisionCount );
    int QuadraticProbe( int originalIndex, int collisionCount );
    int Hash2( int key, int collisionCount );
    
    CollisionMethod m_method;
    SmartTable<HashItem<T>> m_table;

    friend class HashTableTester;
};

template <typename T>
HashTable<T>::HashTable() // Done
{
    m_method = CollisionMethod::LINEAR;
    m_table.AllocateMemory( 5 );
}

template <typename T>
HashTable<T>::HashTable( int size ) // Done
{
    m_method = CollisionMethod::LINEAR;
    m_table.AllocateMemory( size );
}

template <typename T>
HashTable<T>::~HashTable() // Done
{
    // Nothing to do
}

template <typename T>
int HashTable<T>::Size() // Done
{
    return m_table.Size();
}

template <typename T>
void HashTable<T>::SetCollisionMethod( CollisionMethod method ) // Done
{
    m_method = method;
}

template <typename T>
void HashTable<T>::Push(size_t key, T data)
{
    throw Exception::NotImplementedException( "HashTable<T>::Push" ); // TODO: Erase me!                      // STARTER CODE
}

template <typename T>
T& HashTable<T>::Get( int key )
{
    throw Exception::NotImplementedException( "HashTable<T>::Get" ); // TODO: Erase me!                       // STARTER CODE
}

template <typename T>
int HashTable<T>::Hash1( int key )
{
    throw Exception::NotImplementedException( "HashTable<T>::Hash1" ); // TODO: Erase me!                     // STARTER CODE
}

template <typename T>
int HashTable<T>::LinearProbe( int originalIndex, int collisionCount )
{
    throw Exception::NotImplementedException( "HashTable<T>::LinearProbe" ); // TODO: Erase me!               // STARTER CODE
}

template <typename T>
int HashTable<T>::QuadraticProbe( int originalIndex, int collisionCount)
{
    throw Exception::NotImplementedException( "HashTable<T>::QuadraticProbe" ); // TODO: Erase me!            // STARTER CODE
}

template <typename T>
int HashTable<T>::Hash2( int key, int collisionCount )
{
    throw Exception::NotImplementedException( "HashTable<T>::Hash2" ); // TODO: Erase me!                     // STARTER CODE
}

#endif
