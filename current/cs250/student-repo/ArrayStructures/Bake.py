# RUN WITH "python3 Bake.py" or "python3 Bake.py clean"
import os
import glob
import shutil
import sys
import datetime
import json

currentPath = os.path.dirname( os.path.realpath( __file__ ) )
objectPath = "temp_obj/"
timeFormat = "%Y%m%d%H%M"
programName = "debug.exe"
objectBuildCommand = "g++ -std=c++17 -c -g"

sourceFiles = [
        "Utilities/StringHelper.cpp", "Utilities/Style.cpp",
        "DataStructures/ArrayQueue/ArrayQueueTester.cpp",
        "DataStructures/ArrayStack/ArrayStackTester.cpp",
        "DataStructures/FixedArray/FixedArrayTester.cpp",
        "DataStructures/DynamicArray/DynamicArrayTester.cpp",
        "main.cpp"
]

folders = [
        objectPath,
        os.path.join( objectPath, "Utilities" ),
        os.path.join( objectPath, "DataStructures" ),
        os.path.join( objectPath, "DataStructures/ArrayQueue" ),
        os.path.join( objectPath, "DataStructures/ArrayStack" ),
        os.path.join( objectPath, "DataStructures/FixedArray" ),
        os.path.join( objectPath, "DataStructures/DynamicArray" )
]

def BuildObjectWithTimestamp( src ):
        sourceModified = datetime.datetime.fromtimestamp( os.path.getmtime( src ) ).strftime( timeFormat )
        obj = src.replace( ".cpp", ".o" )
        obj = os.path.join( objectPath, obj )
        obj = obj.replace( ".o", "_" + sourceModified + ".o" )

        # Build the object file
        command = objectBuildCommand + " " + src + " -o " + obj
        print( "   - ", command )
        os.system( command )

        return obj

def CleanMake():
        print( "CleanMake()" )
        objectList = []

        # Remove old obj files
        print( " - Check for and remove old object files..." )
        if ( os.path.exists( objectPath ) ):
                shutil.rmtree( objectPath )
        print( " - Check for and remove old \"" + programName + "\"..." )
        if ( os.path.exists( programName ) ):
                os.remove( programName )

        # Create necessary folders
        print( " - Create folders for object files..." )
        for fold in folders:
                print( "   - " + fold )
                os.makedirs( fold )

        # Build program
        print( " - Build object files..." )
        for src in sourceFiles:
                obj = BuildObjectWithTimestamp( src )
                objectList.append( obj )

        print( " - Assemble " + programName + "..." )
        command = "g++ -o " + programName + " "
        for obj in objectList:
                command += obj + " "
        print( "   - Command: ", command )
        os.system( command )

        print()
        print( "Use \"./" + programName + "\" to run ")
        print()


def FastMake():
        print( "FastMake()" )
        rebuild = []
        objFiles = glob.glob( "**/*.o", recursive=True )

        print( " - Check for missing object files..." )
        for src in sourceFiles:
                objPartial = src.replace( ".cpp", "*.o" )
                objPartial = os.path.join( objectPath, objPartial )
                globMatch = glob.glob( objPartial )

                if ( len( globMatch ) == 0 ):
                        print( "   - ", src, "'s obj file is missing" )
                        rebuild.append( src )

        print( " - Check age of source files..." )
        for obj in objFiles:
                path = objectPath.replace( "/", "\\" )
                cppFile = obj.replace( path, "" )
                cppFile = cppFile[:-15] + ".cpp"

                # C++ file modify time
                sourceModified = datetime.datetime.fromtimestamp( os.path.getmtime( cppFile ) ).strftime( timeFormat )

                # Object file modify time
                objModified = obj[-14:].replace( ".o", "" )

                if ( sourceModified != objModified ):
                        print( cppFile, "is stale" )
                        rebuild.append( cppFile )
                        # Delete the old obj file
                        os.remove( obj )

        if ( len( rebuild ) == 0 ):
                print()
                print( "No stale or missing files,", programName, "should be up-to-date!" )
                print()
                return
                        
        # Rebuild stale files
        print( " - Rebuild stale files..." )
        objectList = []
        for stale in rebuild:
                obj = BuildObjectWithTimestamp( stale )
                objectList.append( obj )

                
        objFiles = glob.glob( "**/*.o", recursive=True )
        
        print( " - Assemble " + programName + "..." )
        command = "g++ -o " + programName + " "
        for obj in objFiles:
                command += obj + " "
        print( "   - Command: ", command )
        os.system( command )

        print()
        print( "Use \"./" + programName + "\" to run ")
        print()




argCount = len( sys.argv )

if ( argCount == 1 ):
        print( "Run options:" )
        print( " - " + sys.argv[0] + " clean" )
        print( " - " + sys.argv[0] + " fast (DEFAULT)" )
        print( "" )
        FastMake()
        exit(0)


if ( sys.argv[1] == "clean" ):
        CleanMake()

elif ( sys.argv[1] == "fast" ):
        FastMake()
