#ifndef _NOT_IMPLEMENTED_EXCEPTION
#define _NOT_IMPLEMENTED_EXCEPTION

// C++ Library includes
#include <stdexcept>
#include <string>

namespace Exception
{

//! EXCEPTION for when a function has not yet been implemented
    class NotImplementedException : public std::runtime_error
    {
    public:
        NotImplementedException( std::string functionName )
            : std::runtime_error( functionName + " is not yet implemented! Make sure to remove the \"throw Exception::NotImplementedException\" when working on a function." ) { ; }
    };

} // End of namespace

#endif
