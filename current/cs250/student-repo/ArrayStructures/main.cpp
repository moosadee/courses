#include <iostream>
#include <vector>
#include <string>
using namespace std;

// Include data structures tests
#include "DataStructures/FixedArray/FixedArrayTester.h"
#include "DataStructures/DynamicArray/DynamicArrayTester.h"
#include "DataStructures/ArrayStack/ArrayStackTester.h"
#include "DataStructures/ArrayQueue/ArrayQueueTester.h"

// Utilities
#include "Utilities/StringHelper.h"

int main( int argCount, char* args[] ) // test
{
    std::vector<std::string> structures;
    if ( argCount < 2 )
    {
        cout << "Expected: "
             << args[0] << " datastructure1 datastructure2 ..." << endl;
        cout << "  This will run the unit tests for those structures" << endl;
        cout << "  Options: fixedarray dynamicarray "
             << " arrayqueue arraystack all" << endl;
        return 1;
    }
    else if ( argCount == 2 && string( args[1] ) == "all" )
    {
        structures.push_back( "fixedarray" );
        structures.push_back( "dynamicarray" );
        structures.push_back( "arrayqueue" );
        structures.push_back( "arraystack" );
    }
    else if ( argCount == 2 && string( args[1] ) == "test" )
    {
        structures.push_back( "fixedarray" );
        structures.push_back( "dynamicarray" );
        structures.push_back( "arrayqueue" );
        structures.push_back( "arraystack" );
    }
    else
    {
        for ( int i = 1; i < argCount; i++ )
        {
            std::string lower = StringToLower( std::string( args[i] ) );
            structures.push_back( args[i] );
        }
    }

    // Run tests for all structures given
    for ( string ds : structures )
    {
        if      ( ds == "fixedarray" )
        {
            FixedArrayTester tester; tester.RunAll();
        }
        else if ( ds == "dynamicarray" )
        {
            DynamicArrayTester tester; tester.RunAll();
        }
        else if ( ds == "arrayqueue" )
        {
            ArrayQueueTester tester; tester.RunAll();
        }
        else if ( ds == "arraystack" )
        {
            ArrayStackTester tester; tester.RunAll();
        }
    } // test lol
}
