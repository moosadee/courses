#ifndef _ARRAY_QUEUE_TESTER
#define _ARRAY_QUEUE_TESTER

class ArrayQueueTester
{
public:
    void RunAll();
    void Test_ArrayQueue_Push();
    void Test_ArrayQueue_Pop();
    void Test_ArrayQueue_Front();
    void Test_ArrayQueue_Size();
    void Test_ArrayQueue_IsEmpty();
};

#endif
