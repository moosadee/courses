#ifndef ARRAY_QUEUE_HPP
#define ARRAY_QUEUE_HPP

// Project includes
#include "../DynamicArray/DynamicArray.h"
#include "../../Exceptions/NotImplementedException.h"
#include "../../Exceptions/StructureFullException.h"
#include "../../Exceptions/InvalidIndexException.h"
#include "../../Exceptions/NullptrException.h"

template <typename T>
//! A first-in-first-out (FIFO) queue structure built on top of an array
class ArrayQueue
{
public:
    void Push(const T& newData );
    void Pop();
    T& Front();
    int Size();
    bool IsEmpty();

private:
    DynamicArray<T> m_vector;

    friend class ArrayQueueTester;
};

//! Push a new item into the queue from the back
template <typename T>
void ArrayQueue<T>::Push( const T& newData )
{
    throw Exception::NotImplementedException( "ArrayQueue::Push is not implemented" );                                                              // STARTER
}

//! Remove an item in the queue from the front
template <typename T>
void ArrayQueue<T>::Pop()
{
    throw Exception::NotImplementedException( "ArrayQueue::Pop is not implemented" );                                                               // STARTER
}

//! Access the data at the front of the queue
template <typename T>
T& ArrayQueue<T>::Front()
{
    throw Exception::NotImplementedException( "ArrayQueue::Front is not implemented" );                                                             // STARTER
}

//! Get the amount of items in the queue
template <typename T>
int ArrayQueue<T>::Size()
{
    throw Exception::NotImplementedException( "ArrayQueue::Size is not implemented" );                                                              // STARTER
}

//! Return whether the queue is empty
template <typename T>
bool ArrayQueue<T>::IsEmpty()
{
    throw Exception::NotImplementedException( "ArrayQueue::IsEmpty is not implemented" );                                                           // STARTER
}

#endif
