#include "FixedArrayTester.h"
#include "../../Utilities/Style.h"

void FixedArrayTester::RunAll()
{
    DisplayHeader( "FixedArrayTester - RunAll", 0 );
    Test_FixedArray_Constructor();
    Test_FixedArray_ShiftLeft();
    Test_FixedArray_ShiftRight();
    Test_FixedArray_PopBack();
    Test_FixedArray_PopFront();
    Test_FixedArray_PopAt();
    Test_FixedArray_GetBack();
    Test_FixedArray_GetFront();
    Test_FixedArray_GetAt();
    Test_FixedArray_IsFull();
    Test_FixedArray_IsEmpty();
    Test_FixedArray_PushBack();
    Test_FixedArray_PushFront();
    Test_FixedArray_PushAt();
    Test_FixedArray_Search();
}

void FixedArrayTester::Test_FixedArray_Constructor()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "FixedArray Constructor";

    {
        string testName = "FixedArray obj; check values after constructor";
        FixedArray<int> obj;
        totalTests++; testsPassed += Test_DoTheyMatch( size_t( 100 ), obj.ARRAY_SIZE,  testName, "obj.ARRAY_SIZE" );
        totalTests++; testsPassed += Test_DoTheyMatch( size_t( 0 ),   obj.m_itemCount, testName, "obj.m_itemCount" );
        Test_End();
    }
}

void FixedArrayTester::Test_FixedArray_ShiftLeft()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "FixedArray::ShiftLeft";
    bool prereqImplemented = true;
    {
        string testName = "Check " + functionName + " is implemented"; totalTests++;
        FixedArray<char> obj;
	obj.m_itemCount = 5;
        string exceptionMessage = "";
        try                                                    { obj.ShiftLeft( 2 ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Populate array, call ShiftLeft, check positions";
        FixedArray<std::string> original;
        original.m_itemCount = 4;
        original.m_array[0] = "a"; original.m_array[1] = "b"; original.m_array[2] = "c"; original.m_array[3] = "d";
        vector<string> expectOut = { "a", "c", "d" };
        FixedArray<string> actualOut = original;
        actualOut.ShiftLeft( 1 );

        totalTests++; testsPassed += Test_DoTheyMatch( actualOut.m_itemCount, original.m_itemCount, testName, "testList.m_itemCount" );
        for ( size_t i = 0; i < expectOut.size(); i++ )
        {
            totalTests++; testsPassed += Test_DoTheyMatch( expectOut[i], actualOut.m_array[i], testName, "arr.m_array[" + to_string( i ) + "]" );
        }

        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void FixedArrayTester::Test_FixedArray_ShiftRight()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "FixedArray::ShiftRight";
    bool prereqImplemented = true;
    {
        string testName = "Check " + functionName + " is implemented";
        FixedArray<char> obj;
	obj.m_itemCount = 5;
        string exceptionMessage = "";
        try                                                    { obj.ShiftRight( 2 ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Populate array, call ShiftRight, check positions"; totalTests++;
        FixedArray<std::string> original;
        original.m_itemCount = 4;
        original.m_array[0] = "a"; original.m_array[1] = "b"; original.m_array[2] = "c"; original.m_array[3] = "d";
        vector<string> expectOut = { "a", "b", "b", "c", "d" };
        FixedArray<string> actualOut = original;
        actualOut.ShiftRight( 1 );

        totalTests++; testsPassed += Test_DoTheyMatch( actualOut.m_itemCount, original.m_itemCount, testName, "testList.m_itemCount" );
        for ( size_t i = 0; i < expectOut.size(); i++ )
        {
            totalTests++; testsPassed += Test_DoTheyMatch( expectOut[i], actualOut.m_array[i], testName, "arr.m_array[" + to_string( i ) + "]" );
        }

        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void FixedArrayTester::Test_FixedArray_PopBack()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "FixedArray::PopBack";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ IsEmpty implemented" ); totalTests++;
        FixedArray<char> obj;
        try                                                    { obj.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Check " + functionName + " is implemented"; totalTests++;
        FixedArray<char> obj;
        string exceptionMessage = "";
        try                                                    { obj.PopBack(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Set up array, use PopBack, check";
        FixedArray<std::string> arr;
        arr.m_array[0] = "a"; arr.m_array[1] = "b"; arr.m_array[2] = "c";
        arr.m_itemCount = 3;

        arr.PopBack();

        totalTests++; testsPassed += Test_DoTheyMatch( size_t( 2 ),   arr.m_itemCount, testName, "arr.m_itemCount" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "a" ), arr.m_array[0],  testName, "arr.m_array[0]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "b" ), arr.m_array[1],  testName, "arr.m_array[1]" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Can't pop from empty list" );
        FixedArray<std::string> arr; arr.m_itemCount = 0;
        string exceptionThrown = "";
        string expectedException = "Exception::StructureEmptyException";
        try                                                      { arr.PopBack(); }
        catch ( const Exception::StructureEmptyException& ex )   { exceptionThrown = "Exception::StructureEmptyException"; }
        catch( ... )                                             { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "arr.PopBack()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void FixedArrayTester::Test_FixedArray_PopFront()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "FixedArray::PopFront";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ IsEmpty implemented" ); totalTests++;
        FixedArray<char> obj;
        try                                                    { obj.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Check " + functionName + " is implemented"; totalTests++;
        FixedArray<char> obj;
        string exceptionMessage = "";
        try                                                    { obj.PopFront(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Set up array, use PopFront, check";
        FixedArray<std::string> arr;
        arr.m_array[0] = "a"; arr.m_array[1] = "b"; arr.m_array[2] = "c";
        arr.m_itemCount = 3;

        arr.PopFront();

        totalTests++; testsPassed += Test_DoTheyMatch( size_t( 2 ),   arr.m_itemCount, testName, "arr.m_itemCount" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "b" ), arr.m_array[0],  testName, "arr.m_array[0]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "c" ), arr.m_array[1],  testName, "arr.m_array[1]" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Can't pop from empty list" );
        FixedArray<std::string> arr; arr.m_itemCount = 0;
        string exceptionThrown = "";
        string expectedException = "Exception::StructureEmptyException";
        try                                                      { arr.PopFront(); }
        catch ( const Exception::StructureEmptyException& ex )   { exceptionThrown = "Exception::StructureEmptyException"; }
        catch( ... )                                             { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "arr.PopFront()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void FixedArrayTester::Test_FixedArray_PopAt()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "FixedArray::PopAt";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ IsEmpty implemented" ); totalTests++;
        FixedArray<char> obj;
        try                                                    { obj.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Check " + functionName + " is implemented"; totalTests++;
        FixedArray<char> obj;
        string exceptionMessage = "";
        try                                                    { obj.PopAt( 1 ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Set up array, use PopAt( 1 ), check";
        FixedArray<std::string> arr;
        arr.m_itemCount = 3;
        arr.m_array[0] = "a"; arr.m_array[1] = "b"; arr.m_array[2] = "c";

        arr.PopAt( 1 );

        totalTests++; testsPassed += Test_DoTheyMatch( size_t( 2 ),   arr.m_itemCount, testName, "arr.m_itemCount" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "a" ), arr.m_array[0],  testName, "arr.m_array[0]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "c" ), arr.m_array[1],  testName, "arr.m_array[1]" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Can't pop from empty list" );
        FixedArray<std::string> arr; arr.m_itemCount = 0;
        string exceptionThrown = "";
        string expectedException = "Exception::StructureEmptyException";
        try                                                      { arr.PopAt( 1 ); }
        catch ( const Exception::StructureEmptyException& ex )   { exceptionThrown = "Exception::StructureEmptyException"; }
        catch( ... )                                             { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "arr.PopAt( 1 )" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Negative index marked invalid?" );
        FixedArray<std::string> arr; arr.m_itemCount = 5;
        string exceptionThrown = "";
        string expectedException = "Exception::InvalidIndexException";
        try                                                    { arr.PopAt( -5 ); }
        catch ( const Exception::InvalidIndexException& ex )   { exceptionThrown = "Exception::InvalidIndexException"; }
        catch( ... )                                           { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "arr.PopAt( -5 )" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Out-of-bounds index marked invalid?" );
        FixedArray<std::string> arr; arr.m_itemCount = 5;
        string exceptionThrown = "";
        string expectedException = "Exception::InvalidIndexException";
        try                                                    { arr.PopAt( 6 ); }
        catch ( const Exception::InvalidIndexException& ex )   { exceptionThrown = "Exception::InvalidIndexException"; }
        catch( ... )                                           { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "arr.PopAt( 6 )" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void FixedArrayTester::Test_FixedArray_GetBack()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "FixedArray::GetBack";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ IsEmpty implemented" ); totalTests++;
        FixedArray<char> obj;
        try                                                    { obj.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Check " + functionName + " is implemented"; totalTests++;
        FixedArray<char> obj;
        string exceptionMessage = "";
        try                                                    { obj.GetBack(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Set up array, use GetBack(), check";
        FixedArray<std::string> arr;
        arr.m_itemCount = 3;
        arr.m_array[0] = "a"; arr.m_array[1] = "b"; arr.m_array[2] = "c";
        totalTests++; testsPassed += Test_DoTheyMatch( string( "c" ), arr.GetBack(),  testName, "arr.GetBack()" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Can't get from empty list" );
        FixedArray<std::string> arr; arr.m_itemCount = 0;
        string exceptionThrown = "";
        string expectedException = "Exception::StructureEmptyException";
        try                                                      { arr.GetBack(); }
        catch ( const Exception::StructureEmptyException& ex )   { exceptionThrown = "Exception::StructureEmptyException"; }
        catch( ... )                                             { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "arr.GetBack()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void FixedArrayTester::Test_FixedArray_GetFront()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "FixedArray::GetFront";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ IsEmpty implemented" ); totalTests++;
        FixedArray<char> obj;
        try                                                    { obj.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Check " + functionName + " is implemented"; totalTests++;
        FixedArray<char> obj;
        string exceptionMessage = "";
        try                                                    { obj.GetFront(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Set up array, use GetFront(), check";
        FixedArray<std::string> arr;
        arr.m_itemCount = 3;
        arr.m_array[0] = "a"; arr.m_array[1] = "b"; arr.m_array[2] = "c";
        totalTests++; testsPassed += Test_DoTheyMatch( string( "a" ), arr.GetFront(),  testName, "arr.GetFront()" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Can't get from empty list" );
        FixedArray<std::string> arr; arr.m_itemCount = 0;
        string exceptionThrown = "";
        string expectedException = "Exception::StructureEmptyException";
        try                                                      { arr.GetFront(); }
        catch ( const Exception::StructureEmptyException& ex )   { exceptionThrown = "Exception::StructureEmptyException"; }
        catch( ... )                                             { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "arr.GetBack()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void FixedArrayTester::Test_FixedArray_GetAt()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "FixedArray::GetAt";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ IsEmpty implemented" ); totalTests++;
        FixedArray<char> obj;
        try                                                    { obj.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Check " + functionName + " is implemented"; totalTests++;
        FixedArray<char> obj;
        string exceptionMessage = "";
        try                                                    { obj.GetAt( 1 ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Set up array, use GetAt( 1 ), check";
        FixedArray<std::string> arr;
        arr.m_itemCount = 3;
        arr.m_array[0] = "a"; arr.m_array[1] = "b"; arr.m_array[2] = "c";
        totalTests++; testsPassed += Test_DoTheyMatch( string( "b" ), arr.GetAt( 1 ),  testName, "arr.GetAt( 1 )" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Can't get from empty list" );
        FixedArray<std::string> arr; arr.m_itemCount = 0;
        string exceptionThrown = "";
        string expectedException = "Exception::StructureEmptyException";
        try                                                      { arr.GetAt( 1 ); }
        catch ( const Exception::StructureEmptyException& ex )   { exceptionThrown = "Exception::StructureEmptyException"; }
        catch( ... )                                             { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "arr.GetAt( 1 )" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Negative index marked invalid?" );
        FixedArray<std::string> arr; arr.m_itemCount = 5;
        string exceptionThrown = "";
        string expectedException = "Exception::InvalidIndexException";
        try                                                    { arr.GetAt( -5 ); }
        catch ( const Exception::InvalidIndexException& ex )   { exceptionThrown = "Exception::InvalidIndexException"; }
        catch( ... )                                           { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "arr.GetAt( -5 )" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Out-of-bounds index marked invalid?" );
        FixedArray<std::string> arr; arr.m_itemCount = 5;
        string exceptionThrown = "";
        string expectedException = "Exception::InvalidIndexException";
        try                                                    { arr.GetAt( 6 ); }
        catch ( const Exception::InvalidIndexException& ex )   { exceptionThrown = "Exception::InvalidIndexException"; }
        catch( ... )                                           { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "arr.GetAt( 6 )" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void FixedArrayTester::Test_FixedArray_IsFull()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "FixedArray::IsFull";
    bool prereqImplemented = true;
    {
        string testName = "Check " + functionName + " is implemented"; totalTests++;
        FixedArray<char> obj;
        string exceptionMessage = "";
        try                                                    { obj.IsFull(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Check IsFull returns true if array is full.";
        FixedArray<int> arr;
        arr.m_itemCount = arr.ARRAY_SIZE;
        totalTests++; testsPassed += Test_DoTheyMatch( true, arr.IsFull(), testName, "arr.IsFull()" );
        Test_End();
    }

    {
        string testName = "Check IsFull returns true if array is NOT full.";
        FixedArray<int> arr;
        arr.m_itemCount = arr.ARRAY_SIZE - 1;
        totalTests++; testsPassed += Test_DoTheyMatch( false, arr.IsFull(), testName, "arr.IsFull()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void FixedArrayTester::Test_FixedArray_IsEmpty()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "FixedArray::IsEmpty";
    bool prereqImplemented = true;
    {
        string testName = "Check " + functionName + " is implemented"; totalTests++;
        FixedArray<char> obj;
        string exceptionMessage = "";
        try                                                    { obj.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Check IsEmpty returns true if array is empty.";
        FixedArray<int> arr;
        arr.m_itemCount = 0;
        totalTests++; testsPassed += Test_DoTheyMatch( true, arr.IsEmpty(), testName, "arr.IsEmpty()" );
        Test_End();
    }

    {
        string testName = "Check IsEmpty returns true if array is NOT empty.";
        FixedArray<int> arr;
        arr.m_itemCount = 5;
        totalTests++; testsPassed += Test_DoTheyMatch( false, arr.IsEmpty(), testName, "arr.IsEmpty()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void FixedArrayTester::Test_FixedArray_PushBack()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "FixedArray::PushBack";
    bool prereqImplemented = true;
    {
        string testName = "Check " + functionName + " is implemented"; totalTests++;
        FixedArray<char> obj;
        string exceptionMessage = "";
        try                                                    { obj.PushBack( 'A' ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "PushBack('A')";
        FixedArray<char> arr;

        string exceptionThrown = "";
        try             { arr.PushBack( 'A' ); }
        catch ( ... )   { exceptionThrown = "Unknown exception"; }

        totalTests++; testsPassed += Test_DoTheyMatch( string( "" ), exceptionThrown, testName, "arr.PushBack( 'A' )" );
        totalTests++; testsPassed += Test_DoTheyMatch( 'A',          arr.m_array[0],  testName, "arr.m_array[0]" );
        totalTests++; testsPassed += Test_DoTheyMatch( size_t( 1 ),  arr.m_itemCount, testName, "arr.m_itemCounts]" );

        Test_End();
    }

    {
        string testName = "PushBack('A') PushBack('B')";
        FixedArray<char> arr;

        string exceptionThrown = "";
        try             { arr.PushBack( 'A' ); arr.PushBack( 'B' ); }
        catch ( ... )   { exceptionThrown = "Unknown exception"; }

        totalTests++; testsPassed += Test_DoTheyMatch( string( "" ), exceptionThrown, testName, "arr.PushBack( 'A' )" );
        totalTests++; testsPassed += Test_DoTheyMatch( 'A',          arr.m_array[0],  testName, "arr.m_array[0]" );
        totalTests++; testsPassed += Test_DoTheyMatch( 'B',          arr.m_array[1],  testName, "arr.m_array[1]" );
        totalTests++; testsPassed += Test_DoTheyMatch( size_t( 2 ),  arr.m_itemCount, testName, "arr.m_itemCounts]" );

        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void FixedArrayTester::Test_FixedArray_PushFront()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "FixedArray::PushFront";
    bool prereqImplemented = true;
    {
        string testName = "Check " + functionName + " is implemented"; totalTests++;
        FixedArray<char> obj;
        string exceptionMessage = "";
        try                                                    { obj.PushFront( 'A' ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "PushFront('A')";
        FixedArray<char> arr;

        string exceptionThrown = "";
        try             { arr.PushFront( 'A' ); }
        catch ( ... )   { exceptionThrown = "Unknown exception"; }

        totalTests++; testsPassed += Test_DoTheyMatch( string( "" ), exceptionThrown, testName, "arr.PushBack( 'A' )" );
        totalTests++; testsPassed += Test_DoTheyMatch( 'A',          arr.m_array[0],  testName, "arr.m_array[0]" );
        totalTests++; testsPassed += Test_DoTheyMatch( size_t( 1 ),  arr.m_itemCount, testName, "arr.m_itemCounts]" );

        Test_End();
    }

    {
        string testName = "PushFront('A') PushFront('B')";
        FixedArray<char> arr;

        string exceptionThrown = "";
        try             { arr.PushFront( 'A' ); arr.PushFront( 'B' ); }
        catch ( ... )   { exceptionThrown = "Unknown exception"; }

        totalTests++; testsPassed += Test_DoTheyMatch( string( "" ), exceptionThrown, testName, "arr.PushBack( 'A' )" );
        totalTests++; testsPassed += Test_DoTheyMatch( 'B',          arr.m_array[0],  testName, "arr.m_array[0]" );
        totalTests++; testsPassed += Test_DoTheyMatch( 'A',          arr.m_array[1],  testName, "arr.m_array[1]" );
        totalTests++; testsPassed += Test_DoTheyMatch( size_t( 2 ),  arr.m_itemCount, testName, "arr.m_itemCounts]" );

        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void FixedArrayTester::Test_FixedArray_PushAt()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "FixedArray::PushAt";
    bool prereqImplemented = true;
    {
        string testName = "Check " + functionName + " is implemented"; totalTests++;
        FixedArray<char> obj;
        string exceptionMessage = "";
        try                                                    { obj.PushAt( 1, 'A' ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "PushAt( 1, 'Z' )";
        FixedArray<char> arr;
        arr.m_itemCount = 3;
        arr.m_array[0] = 'A'; arr.m_array[1] = 'B'; arr.m_array[2] = 'C';

        string exceptionThrown = "";
        try             { arr.PushAt( 1, 'Z' ); }
        catch ( ... )   { exceptionThrown = "Unknown exception"; }

        totalTests++; testsPassed += Test_DoTheyMatch( string( "" ), exceptionThrown, testName, "arr.PushAt( 1, 'Z' )" );
        totalTests++; testsPassed += Test_DoTheyMatch( 'A',          arr.m_array[0],  testName, "arr.m_array[0]" );
        totalTests++; testsPassed += Test_DoTheyMatch( 'Z',          arr.m_array[1],  testName, "arr.m_array[1]" );
        totalTests++; testsPassed += Test_DoTheyMatch( 'B',          arr.m_array[2],  testName, "arr.m_array[2]" );
        totalTests++; testsPassed += Test_DoTheyMatch( 'C',          arr.m_array[3],  testName, "arr.m_array[3]" );
        totalTests++; testsPassed += Test_DoTheyMatch( size_t( 4 ),  arr.m_itemCount, testName, "arr.m_itemCounts]" );

        Test_End();
    }

    {
        string testName = Test_Begin( "Negative index marked invalid?" );
        FixedArray<char> arr; arr.m_itemCount = 5;
        string exceptionThrown = "";
        string expectedException = "Exception::InvalidIndexException";
        try                                                    { arr.PushAt( -5, 'A' ); }
        catch ( const Exception::InvalidIndexException& ex )   { exceptionThrown = "Exception::InvalidIndexException"; }
        catch( ... )                                           { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "arr.PushAt( -5, 'A' )" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Out-of-bounds index marked invalid?" );
        FixedArray<char> arr; arr.m_itemCount = 5;
        string exceptionThrown = "";
        string expectedException = "Exception::InvalidIndexException";
        try                                                    { arr.PushAt( 6, 'A' ); }
        catch ( const Exception::InvalidIndexException& ex )   { exceptionThrown = "Exception::InvalidIndexException"; }
        catch( ... )                                           { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "arr.PushAt( 6, 'A' )" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void FixedArrayTester::Test_FixedArray_Search()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "FixedArray::Search";
    bool prereqImplemented = true;
    {
        string testName = "Check " + functionName + " is implemented"; totalTests++;
        FixedArray<char> obj;
        string exceptionMessage = "";
        try                                                    { obj.Search( 'a' ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Search for item in array";
        FixedArray<char> arr;
        arr.m_itemCount = 3;
        arr.m_array[0] = 'C'; arr.m_array[1] = 'A'; arr.m_array[2] = 'T';

        string exceptionThrown = "";
        int result = -1;
        try             { result = arr.Search( 'A' ); }
        catch ( ... )   { exceptionThrown = "Unknown exception"; }

        totalTests++; testsPassed += Test_DoTheyMatch( 1, result, testName, "arr.Search( 'A' )" );

        Test_End();
    }

    {
        string testName = "Search for item in NOT array";
        FixedArray<char> arr;
        arr.m_itemCount = 3;
        arr.m_array[0] = 'C'; arr.m_array[1] = 'A'; arr.m_array[2] = 'T';

        string exceptionThrown = "";
        int result = -1;
        try                                                     { result = arr.Search( 'Z' ); }
        catch ( const Exception::ItemNotFoundException& ex )    { exceptionThrown = "ItemNotFoundException"; }
        catch ( ... )                                           { exceptionThrown = "Unknown exception"; }

        totalTests++; testsPassed += Test_DoTheyMatch( string( "ItemNotFoundException" ), exceptionThrown, testName, "arr.Search( 'Z' )" );

        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

