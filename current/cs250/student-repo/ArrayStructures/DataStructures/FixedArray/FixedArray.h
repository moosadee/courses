#ifndef _SMART_FIXED_ARRAY_HPP
#define _SMART_FIXED_ARRAY_HPP

/* We'll work on this together during class as an introduction */

// C++ Library includes
#include <iostream>

// Project includes
#include "../../Exceptions/ItemNotFoundException.h"
#include "../../Exceptions/NotImplementedException.h"
#include "../../Exceptions/StructureFullException.h"
#include "../../Exceptions/StructureEmptyException.h"
#include "../../Exceptions/InvalidIndexException.h"
#include "../../Utilities/StringHelper.h"

template <typename T>
//! A data structure that wraps a fixed array
class FixedArray
{
public:
    FixedArray();
    ~FixedArray();

    void PushBack( T newItem );
    void PushFront( T newItem );
    void PushAt( size_t index, T newItem );

    void PopBack();
    void PopFront();
    void PopAt( size_t index );

    T& GetBack();
    T& GetFront();
    T& GetAt( size_t index );

    size_t Search( T item ) const;

    size_t Size() const;
    bool IsEmpty() const;
    bool IsFull() const;

    void Clear();

private:
    T m_array[100];
    const size_t ARRAY_SIZE;
    size_t m_itemCount;

    /* Private member methods */
    void ShiftLeft( size_t index );
    void ShiftRight( size_t index );

    friend class FixedArrayTester;
};

//! Constructor
template <typename T>
FixedArray<T>::FixedArray()
    : ARRAY_SIZE( 100 )
{
    Clear();
    m_itemCount = 0;
}

//! Destructor
template <typename T>
FixedArray<T>::~FixedArray()
{
    Clear();
}

//! Clear the data.
template <typename T>
void FixedArray<T>::Clear()
{
    m_itemCount = 0;
}

//! Returns the amount of items currently stored in the array.
template <typename T>
size_t FixedArray<T>::Size() const
{
    throw Exception::NotImplementedException( "FixedArray<T>::Size" ); // Erase this once you work on this function                                 // STARTER
}

//! Check if the array is currently full.
template <typename T>
bool FixedArray<T>::IsFull() const
{
    throw Exception::NotImplementedException( "FixedArray<T>::IsFull" ); // Erase this once you work on this function
}

//! Check if the array is currently empty.
template <typename T>
bool FixedArray<T>::IsEmpty() const
{
    throw Exception::NotImplementedException( "FixedArray<T>::IsEmpty" ); // Erase this once you work on this function
}

template <typename T>
void FixedArray<T>::ShiftLeft( size_t index )
{
    throw Exception::NotImplementedException( "FixedArray<T>::ShiftLeft" ); // Erase this once you work on this function                            // STARTER
}

template <typename T>
void FixedArray<T>::ShiftRight( size_t index )
{
    throw Exception::NotImplementedException( "FixedArray<T>::ShiftRight" ); // Erase this once you work on this function                           // STARTER
}

//! Insert an item to the END of the array.
template <typename T>
void FixedArray<T>::PushBack( T newItem )
{
    throw Exception::NotImplementedException( "FixedArray<T>::PushBack" ); // Erase this once you work on this function                             // STARTER
}

//! Insert an item to the BEGINNING of the array.
template <typename T>
void FixedArray<T>::PushFront( T newItem )
{
    throw Exception::NotImplementedException( "FixedArray<T>::PushFront" ); // Erase this once you work on this function                            // STARTER
}

//! Insert an item at some index in the array.
template <typename T>
void FixedArray<T>::PushAt( size_t index, T newItem )
{
    throw Exception::NotImplementedException( "FixedArray<T>::PushAt" ); // Erase this once you work on this function                               // STARTER
}

//! Remove the LAST item in the array.
template <typename T>
void FixedArray<T>::PopBack()
{
    throw Exception::NotImplementedException( "FixedArray<T>::PopBack" ); // Erase this once you work on this function                              // STARTER
}

//! Remove the FRONT item in the array. Shift everything to the left.
template <typename T>
void FixedArray<T>::PopFront()
{
    throw Exception::NotImplementedException( "FixedArray<T>::PopFront" ); // Erase this once you work on this function                             // STARTER
}

//! Remove an item in the middle of the array. Close up the gap.
template <typename T>
void FixedArray<T>::PopAt( size_t index )
{
    throw Exception::NotImplementedException( "FixedArray<T>::PopAt" ); // Erase this once you work on this function                                // STARTER
}

//! Get the LAST item in the array.
template <typename T>
T& FixedArray<T>::GetBack()
{
    throw Exception::NotImplementedException( "FixedArray<T>::GetBack" ); // Erase this once you work on this function                              // STARTER
}

//! Get the FIRST item in the array.
template <typename T>
T& FixedArray<T>::GetFront()
{
    throw Exception::NotImplementedException( "FixedArray<T>::GetFront" ); // Erase this once you work on this function                             // STARTER
}

//! Get an item in the array at some index.
template <typename T>
T& FixedArray<T>::GetAt( size_t index )
{
    throw Exception::NotImplementedException( "FixedArray<T>::GetAt" ); // Erase this once you work on this function                                // STARTER
}

//! Search for an item by its value, return the index of its position.
template <typename T>
size_t FixedArray<T>::Search( T item ) const
{
    throw Exception::NotImplementedException( "FixedArray<T>::Search" ); // Erase this once you work on this function                               // STARTER
}

#endif
