#ifndef _DYNAMIC_ARRAY_TESTER
#define _DYNAMIC_ARRAY_TESTER

#include "DynamicArray.h"

class DynamicArrayTester
{
public:
    void RunAll();
    void Test_DynamicArray_Constructor();
    void Test_DynamicArray_ShiftLeft();
    void Test_DynamicArray_ShiftRight();
    void Test_DynamicArray_PopBack();
    void Test_DynamicArray_PopFront();
    void Test_DynamicArray_PopAt();
    void Test_DynamicArray_GetBack();
    void Test_DynamicArray_GetFront();
    void Test_DynamicArray_GetAt();
    void Test_DynamicArray_AllocateMemory();
    void Test_DynamicArray_Resize();
    void Test_DynamicArray_IsFull();
    void Test_DynamicArray_IsEmpty();
    void Test_DynamicArray_PushBack();
    void Test_DynamicArray_PushFront();
    void Test_DynamicArray_PushAt();
    void Test_DynamicArray_Search();
};

#endif
