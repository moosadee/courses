#ifndef DYNAMIC_ARRAY
#define DYNAMIC_ARRAY

// C++ Library includes
#include <iostream>
#include <memory>
using namespace std;

// Project includes
#include "../../Exceptions/NotImplementedException.h"
#include "../../Exceptions/StructureFullException.h"
#include "../../Exceptions/StructureEmptyException.h"
#include "../../Exceptions/InvalidIndexException.h"
#include "../../Exceptions/ItemNotFoundException.h"
#include "../../Exceptions/NullptrException.h"

template <typename T>
//! A data structure that wraps a dynamic array
class DynamicArray
{
public:
    DynamicArray();
    ~DynamicArray();

    void PushBack( T newItem );
    void PushFront( T newItem );
    void PushAt( size_t index, T newItem );

    void PopBack();
    void PopFront();
    void PopAt( int index );

    T& GetBack();
    T& GetFront();
    T& GetAt( int index );

    int Search( T item ) const;

    int Size() const;
    bool IsEmpty() const;
    void Clear();

private:
    //! The pointer used for the dynamic array
    T* m_array;

    //! The current size of the array
    size_t m_arraySize;

    //! The current amount of items inserted into the array
    size_t m_itemCount;

    void ShiftLeft( int index );
    void ShiftRight( int index );

    void AllocateMemory( int size );
    void Resize( int newSize );

    bool IsFull() const;

    friend class DynamicArrayTester;
    friend class ArrayQueueTester;
    friend class ArrayStackTester;
};

//! Constructor
template <typename T>
DynamicArray<T>::DynamicArray()
{
    m_array = nullptr;
    Clear();
}

//! Destructor
template <typename T>
DynamicArray<T>::~DynamicArray()
{
    Clear();
}

//! Deallocates memory for the array and resets the member variables.
template <typename T>
void DynamicArray<T>::Clear()
{
}

//! Returns the amount of items currently stored in the array.
template <typename T>
int DynamicArray<T>::Size() const
{
    throw Exception::NotImplementedException( "DynamicArray<T>::Size" ); // Erase this once you work on this function                               // STARTER
}

//! Check if the array is currently full.
template <typename T>
bool DynamicArray<T>::IsFull() const
{
    throw Exception::NotImplementedException( "DynamicArray<T>::IsFull" ); // Erase this once you work on this function                             // STARTER
}

//! Check if the array is currently empty.
template <typename T>
bool DynamicArray<T>::IsEmpty() const
{
    throw Exception::NotImplementedException( "DynamicArray<T>::IsEmpty" ); // Erase this once you work on this function                            // STARTER
}

//! Allocate memory for the dynamic array.
template <typename T>
void DynamicArray<T>::AllocateMemory( int size )
{
    throw Exception::NotImplementedException( "DynamicArray<T>::AllocateMemory" ); // Erase this once you work on this function                     // STARTER
}

//! Resize the dynamic array.
template <typename T>
void DynamicArray<T>::Resize( int newSize )
{
    throw Exception::NotImplementedException( "DynamicArray<T>::Resize" ); // Erase this once you work on this function                             // STARTER
}

//! Move all items past the given index to the left.
template <typename T>
void DynamicArray<T>::ShiftLeft( int index )
{
    throw Exception::NotImplementedException( "DynamicArray<T>::ShiftLeft" ); // Erase this once you work on this function                          // STARTER
}

//! Move all items past the given index to the right.
template <typename T>
void DynamicArray<T>::ShiftRight( int index )
{
    throw Exception::NotImplementedException( "DynamicArray<T>::ShiftRight" ); // Erase this once you work on this function                         // STARTER
}

//! Insert an item to the END of the array.
template <typename T>
void DynamicArray<T>::PushBack( T newItem )
{
    throw Exception::NotImplementedException( "DynamicArray<T>::PushBack" ); // Erase this once you work on this function                           // STARTER
}

//! Insert an item to the BEGINNING of the array.
template <typename T>
void DynamicArray<T>::PushFront( T newItem )
{
    throw Exception::NotImplementedException( "DynamicArray<T>::PushFront" ); // Erase this once you work on this function                          // STARTER
}

//! Insert an item at some index in the array.
template <typename T>
void DynamicArray<T>::PushAt( size_t index, T newItem )
{
    throw Exception::NotImplementedException( "DynamicArray<T>::PushAt" ); // Erase this once you work on this function                             // STARTER
}

//! Remove the LAST item in the array.
template <typename T>
void DynamicArray<T>::PopBack()
{
    throw Exception::NotImplementedException( "DynamicArray<T>::PopBack" ); // Erase this once you work on this function                            // STARTER
}

//! Remove the FRONT item in the array. Shift everything to the left.
template <typename T>
void DynamicArray<T>::PopFront()
{
    throw Exception::NotImplementedException( "DynamicArray<T>::PopFront" ); // Erase this once you work on this function                           // STARTER
}

//! Remove an item in the middle of the array. Close up the gap.
template <typename T>
void DynamicArray<T>::PopAt( int index )
{
    throw Exception::NotImplementedException( "DynamicArray<T>::PopAt" ); // Erase this once you work on this function                              // STARTER
}

//! Get the LAST item in the array.
template <typename T>
T& DynamicArray<T>::GetBack()
{
    throw Exception::NotImplementedException( "DynamicArray<T>::GetBack" ); // Erase this once you work on this function                            // STARTER
}

//! Get the FIRST item in the array.
template <typename T>
T& DynamicArray<T>::GetFront()
{
    throw Exception::NotImplementedException( "DynamicArray<T>::GetFront" ); // Erase this once you work on this function                           // STARTER
}

//! Get an item in the array at some index.
template <typename T>
T& DynamicArray<T>::GetAt( int index )
{
    throw Exception::NotImplementedException( "DynamicArray<T>::GetAt" ); // Erase this once you work on this function                              // STARTER
}

//! Search for an item by its value, return the index of its position.
template <typename T>
int DynamicArray<T>::Search( T item ) const
{
    throw Exception::NotImplementedException( "DynamicArray<T>::Search" ); // Erase this once you work on this function                             // STARTER
}

#endif
