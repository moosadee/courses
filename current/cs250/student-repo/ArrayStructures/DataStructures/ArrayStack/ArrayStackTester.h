#ifndef _ARRAY_STACK_TESTER
#define _ARRAY_STACK_TESTER

class ArrayStackTester
{
public:
    void RunAll();
    void Test_ArrayStack_Push();
    void Test_ArrayStack_Pop();
    void Test_ArrayStack_Top();
    void Test_ArrayStack_Size();
    void Test_ArrayStack_IsEmpty();
};

#endif
