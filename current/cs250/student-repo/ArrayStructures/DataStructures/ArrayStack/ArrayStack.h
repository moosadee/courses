#ifndef ARRAY_STACK_HPP
#define ARRAY_STACK_HPP

#include "../DynamicArray/DynamicArray.h"
#include "../../Exceptions/NotImplementedException.h"
#include "../../Exceptions/StructureFullException.h"
#include "../../Exceptions/InvalidIndexException.h"
#include "../../Exceptions/NullptrException.h"

template <typename T>
//! A last-in-first-out (LIFO) stack structure built on top of an array
class ArrayStack
{
public:
    void Push(const T& newData );
    void Pop();
    T& Top();
    int Size();
    bool IsEmpty();

private:
    DynamicArray<T> m_vector;

    friend class ArrayStackTester;
};

//! Push a new item into the stack from the top
template <typename T>
void ArrayStack<T>::Push( const T& newData )
{
    throw Exception::NotImplementedException( "ArrayStack::Push is not implemented" );                                                              // STARTER
}

//! Remove an item from the stack via the top
template <typename T>
void ArrayStack<T>::Pop()
{
    throw Exception::NotImplementedException( "ArrayStack::Pop is not implemented" );                                                               // STARTER
}

//! Access the data at the top of the stack
template <typename T>
T& ArrayStack<T>::Top()
{
    throw Exception::NotImplementedException( "ArrayStack::Front is not implemented" );                                                             // STARTER
}

//! Get the amount of items in the stack
template <typename T>
int ArrayStack<T>::Size()
{
    throw Exception::NotImplementedException( "ArrayStack::Size is not implemented" );                                                              // STARTER
}

//! Return whether the stack is empty
template <typename T>
bool ArrayStack<T>::IsEmpty()
{
//    throw Exception::NotImplementedException( "ArrayStack::IsEmpty is not implemented" );                                                           // STARTER
    return m_vector.IsEmpty();                                                                                                                        // SOLUTION
}

#endif
