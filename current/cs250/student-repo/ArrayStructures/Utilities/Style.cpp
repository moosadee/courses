#include "Style.h"

#include <iostream>
using namespace std;

/**
   @param     text     The header text to display
   @param     level    The menu level (e.g., main menu = 0, submenu = 1, subsubmenu = 2...)
*/
void DisplayHeader( string text, int level /* = 0 */ )
{
    string title = "- " + text + " -";
    cout << string( title.size(), '-' ) << endl;
    cout << title << endl;
    cout << string( 80-level, '-' ) << endl;
}

/**
   @param     options    A list of options the user can select from
   @return    string     A value from the list of options
   Displays the list of options in a nice numbered list. Gets the users input,
   validates it, and returns a valid selection out of the list of options.
*/
string GetChoice( vector<string> options, int level /* = 0 */ )
{
    for ( size_t i = 0; i < options.size(); i++ )
    {
        cout << string( level, ' ' ) << "[" << i << "] " << options[i] << endl;
    }

    int index;
    cout << string( level, ' ' ) << "Enter choice #: ";
    cin >> index;

    while ( index < 0 || index >= options.size() )
    {
        cout << endl << string( level, ' ' ) << "** Invalid selection! Try again: ";
        cin >> index;
    }

    return options[index];
}

void Test_CheckPrereqImplemented()
{
}

void Test_PassDisplay( string testName, string label )
{
    cout << GREEN << "[PASS] " << testName << "/" << label << endl;
}

string Test_Begin( string testName )
{
//    cout << testName << ":" << endl;
    return testName;
}

void Test_End()
{
    cout << CLEAR;
}

