#include <iostream>
#include <vector>
#include <string>
#include <map>
using namespace std;

#include "CsvParser.h"

struct Record
{
    string PARKTYPE;
    int OBJECTID;
    string PARKNAME;
    float BOOKACRES;
    string REGION;
};

void LoadRecords( vector<Record>& loadedRecords, string filename )
{
    CsvDocument doc;
    doc = CsvParser::Parse( filename );

    Record record;
    for ( size_t r = 0; r < doc.rows.size(); r++ )
    {
        for ( size_t c = 0; c < doc.rows[r].size(); c++ )
        {
            // String fields
            if      ( doc.header[c] == "PARKTYPE" )  { record.PARKTYPE = doc.rows[r][c]; }
            else if ( doc.header[c] == "PARKNAME" )  { record.PARKNAME = doc.rows[r][c]; }
            else if ( doc.header[c] == "REGION" )    { record.REGION = doc.rows[r][c]; }
            // Int field
            else if ( doc.header[c] == "OBJECTID" )  { record.OBJECTID = stoi( doc.rows[r][c] ); }
            // Float field
            else if ( doc.header[c] == "BOOKACRES" ) { record.BOOKACRES = stof( doc.rows[r][c] ); }
        }
        loadedRecords.push_back( record ); // Don't forget this line! :)
    }
}

void Report1( vector<Record>& records )
{
    map<string, int> regionCounts;

    for ( auto& rec : records )
    {
        if ( rec.REGION == "" ) { rec.REGION = "UNSPECIFIED"; }
        if ( regionCounts.find( rec.REGION ) != regionCounts.end() ) // IF THIS FIELD VALUE WAS FOUND...
        {
            regionCounts[ rec.REGION ]++;
        }
        else // IF IT ISN'T ALREADY SAVED, START A NEW RECORD WITH A COUNT OF 1.
        {
            regionCounts[ rec.REGION ] = 1;
        }
    }

    cout << endl << "REPORT: TOTAL PARKS PER REGION" << endl << endl;
    for ( auto& data : regionCounts )
    {
        cout << "* " << data.first << ": " << data.second << " park(s)" << endl;
    }
}

void Report2( vector<Record>& records )
{
    map<int, int> sizeCounts;

    for ( auto& rec : records )
    {
        int key = 0;
        // Map a region size...
        if      ( rec.BOOKACRES >= 1500 ) { key = 1500; }
        else if ( rec.BOOKACRES >= 1000 ) { key = 1000; }
        else if ( rec.BOOKACRES >= 750  ) { key = 750; }
        else if ( rec.BOOKACRES >= 500  ) { key = 500; }
        else if ( rec.BOOKACRES >= 250  ) { key = 250; }
        else if ( rec.BOOKACRES >= 100  ) { key = 100; }
        else if ( rec.BOOKACRES >= 75   ) { key = 75; }
        else if ( rec.BOOKACRES >= 50   ) { key = 50; }
        else if ( rec.BOOKACRES >= 25   ) { key = 25; }
        else if ( rec.BOOKACRES >= 10   ) { key = 10; }
        else if ( rec.BOOKACRES >= 5    ) { key = 5; }
        else if ( rec.BOOKACRES >= 2    ) { key = 2; }
        else if ( rec.BOOKACRES >= 1    ) { key = 1; }

        if ( sizeCounts.find( key ) != sizeCounts.end() ) // IF THIS FIELD VALUE WAS FOUND...
        {
            sizeCounts[ key ]++;
        }
        else // IF IT ISN'T ALREADY SAVED, START A NEW RECORD WITH A COUNT OF 1.
        {
            sizeCounts[ key ] = 1;
        }
    }

    cout << endl << "REPORT: PARK AREA BREAKDOWN" << endl << endl;
    for ( auto& data : sizeCounts )
    {
        cout << "* " << data.first << ": " << data.second << " park(s)" << endl;
    }
}

int main()
{
    cout << "PROJECT EXAMPLE" << endl;
    vector<Record> records;
    LoadRecords( records, "Kansas City Missouri Parks and Boulevards Map.csv" );
    cout << records.size() << " record(s) loaded" << endl;

    Report1( records );
    Report2( records );


    return 0;
}
