# -*- mode: org -*-

#+TITLE: CS 200: Concepts of Programming using C++ (Spring 2025 version)
#+AUTHOR: Rachel Wil Sha Singh

#+OPTIONS: toc:2
# (only include two levels in TOC)
# #+OPTIONS: num:nil

#+LaTeX_CLASS_OPTIONS: [a4paper,twoside]

# #+LATEX_HEADER: \usepackage{../style/rworgmode}
# #+LATEX_HEADER: \usepackage[utf8]{inputenc}
# #+LATEX_HEADER: \usepackage{graphicx}
# #+LATEX_HEADER: \hypersetup{colorlinks=true,linkcolor=blue}
# #+LATEX_HEADER: \begin{titlepage}

# #+LATEX_HEADER: \sffamily{ \textbf{  {\fontsize{2cm}{2cm}\selectfont ~\\ Core ~\\~\\ Computer Science ~\\~\\ Concepts} } }
# #+LATEX_HEADER: ~\\ ~\\ ~\\ ~\\
# #+LATEX_HEADER: \includegraphics[width=\textwidth]{../images/topics/corecompsci.png}
# #+LATEX_HEADER: ~\\ ~\\ ~\\ ~\\
# #+LATEX_HEADER: \sffamily{ \textbf{  {\fontsize{1cm}{1cm}\selectfont Spring 2025 } } }
# #+LATEX_HEADER: \end{titlepage}

# -----------------------------------------------------------------------------

--------------------------------------------------------------------------------
- *I will be updating this book with semester content as the semester goes on.* By the end of this semester, you will be able to download this PDF as an archive of the class topics.
- Rachel Wil Sha Singh's Core C++ Course © 2025 by Rachel Wil Sha Singh is licensed under CC BY 4.0. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
- Digital textbooks: https://moosadee.gitlab.io/courses/
- These course documents are written in *emacs orgmode* and the files can
  be found here: https://gitlab.com/moosadee/courses
- Dedicated to a better world, and those who work to try to create one.

# -----------------------------------------------------------------------------

#+BEGIN_LATEX
\newpage
#+END_LATEX

------------------------------------------------------------------------------
* Week 0: Welcome, computer context

** Introductions!
#+INCLUDE: "share/read/introductions.org"

#+BEGIN_LATEX
\newpage
#+END_LATEX

** Ethics and Academic Honesty
#+INCLUDE: "share/read/ethics_academic_honesty.org"

#+BEGIN_LATEX
\newpage
#+END_LATEX
** Study skills and course success overview
#+INCLUDE: "share/read/study_skills.org"

#+BEGIN_LATEX
\newpage
#+END_LATEX




------------------------------------------------------------------------------
* Week 1: Setup, main(), variables

** Intro: Welcome to my course!
#+INCLUDE: "cs200/read/Welcome_YouBelong.org"

** Intro: Development tools
#+INCLUDE: "cs200/read/DevelopmentTools.org"

** Intro: Source Control and git
#+INCLUDE: "share/read/git_simple.org"

#+BEGIN_LATEX
\newpage
#+END_LATEX

** Reference: Using Git and VS Code
#+INCLUDE: "../reference/vscode-git.org"

#+BEGIN_LATEX
\newpage
#+END_LATEX

** Intro: main() - Structure of a C++ program
#+INCLUDE: "cs200/read/CPPBasics_1_main.org"

#+BEGIN_LATEX
\newpage
#+END_LATEX

** Intro: Variables and data types
#+INCLUDE: "cs200/read/CPPBasics_2_Variables.org"

#+BEGIN_LATEX
\newpage
#+END_LATEX

** Intro: cout - Console output
#+INCLUDE: "cs200/read/CPPBasics_3_cout.org"

#+BEGIN_LATEX
\newpage
#+END_LATEX

** Intro: Program arguments
#+INCLUDE: "../reference/cpp_program_arguments.org"

#+BEGIN_LATEX
\newpage
#+END_LATEX

** Lab: Variables and output

#+BEGIN_LATEX
\newpage
#+END_LATEX


------------------------------------------------------------------------------

* Week 2: Branching and testing

** Intro: Boolean logic
#+INCLUDE: "cs200/read/BooleanExpressions.org"

#+BEGIN_LATEX
\newpage
#+END_LATEX

** Intro: If, else if, else statements
#+INCLUDE: "cs200/read/IfStatements.org"

#+BEGIN_LATEX
\newpage
#+END_LATEX

** Intro: Switch statements
#+INCLUDE: "cs200/read/SwitchStatements.org"

#+BEGIN_LATEX
\newpage
#+END_LATEX

** Intro: Testing
#+INCLUDE: "share/read/testing.org"

#+BEGIN_LATEX
\newpage
#+END_LATEX

** Lab: Branching and testing
#+INCLUDE: "cs200/student-repo/wk02_BranchingAndTesting/instructions.org"

------------------------------------------------------------------------------

* Week 3: Looping and debugging

** Intro: While loops
#+INCLUDE: "cs200/read/WhileLoops.org"

#+BEGIN_LATEX
\newpage
#+END_LATEX

** Intro: For loops
#+INCLUDE: "cs200/read/ForLoops.org"

#+BEGIN_LATEX
\newpage
#+END_LATEX

** Intro: General debugging
#+INCLUDE: "share/read/debugging.org"

#+BEGIN_LATEX
\newpage
#+END_LATEX

** Intro: gdb debugger (Windows/Linux)
#+INCLUDE: "../reference/gdb.org"
** Intro: lldb debugger (Mac/Linux)
#+INCLUDE: "../reference/lldb.org"

 #+BEGIN_LATEX
 \newpage
 #+END_LATEX

** Lab: Looping and debugging
#+INCLUDE: "cs200/student-repo/wk03_LoopsAndDebugging/instructions.org"

#+BEGIN_LATEX
\newpage
#+END_LATEX

------------------------------------------------------------------------------
* Week 4: Strings, file streams, and console input
** Intro: Strings
#+INCLUDE: "cs200/read/Strings.org"

#+BEGIN_LATEX
\newpage
#+END_LATEX

** Intro: cin - Console input
#+INCLUDE: "cs200/read/CPPBasics_3_cin.org"

#+BEGIN_LATEX
\newpage
#+END_LATEX

** Intro: ofstream and ifstream - File input and output
#+INCLUDE: "cs200/read/FileIO.org"

#+BEGIN_LATEX
\newpage
#+END_LATEX

** Lab: Strings and File I/O
#+INCLUDE: "cs200/student-repo/wk04_StringsAndFileIO/instructions.org"

#+BEGIN_LATEX
\newpage
#+END_LATEX

------------------------------------------------------------------------------
* Week 5: Structs
** Intro: Structs
#+INCLUDE: "cs200/read/Structs.org"

#+BEGIN_LATEX
\newpage
#+END_LATEX


** Lab: Structs
#+INCLUDE: "cs200/student-repo/wk05_Structs/instructions.org"

#+BEGIN_LATEX
\newpage
#+END_LATEX


------------------------------------------------------------------------------
* Week 6: Pointers
** Intro: Pointers and memory
#+INCLUDE: "share/read/MemoryAndPointers1.org"

#+BEGIN_LATEX
\newpage
#+END_LATEX


** Lab: Pointers
#+INCLUDE: "cs200/student-repo/wk06_Pointers/instructions.org"

#+BEGIN_LATEX
\newpage
#+END_LATEX



------------------------------------------------------------------------------
* Week 7: Arrays and Vectors
** Intro: Arrays and storing lists of data
#+INCLUDE: "cs200/read/TraditionalArrays.org"

#+BEGIN_LATEX
\newpage
#+END_LATEX

** Intro: STL Array and STL Vector
#+INCLUDE: "cs200/read/STLArraysVectors.org"

#+BEGIN_LATEX
\newpage
#+END_LATEX

** Intro: Dynamic arrays
#+INCLUDE: "share/read/DynamicArrays.org"

#+BEGIN_LATEX
\newpage
#+END_LATEX


** Lab: Arrays and vectors
#+INCLUDE: "cs200/student-repo/wk07_ArraysVectors/instructions.org"

#+BEGIN_LATEX
\newpage
#+END_LATEX


------------------------------------------------------------------------------
* Mastery Check information
#+INCLUDE: "policies/masterycheck-cs200.org"

#+BEGIN_LATEX
\newpage
#+END_LATEX

* Semester project information
#+INCLUDE: "cs200/project/semester-project.org"

#+BEGIN_LATEX
\newpage
#+END_LATEX

* Common general issues
#+INCLUDE: "../reference/common_errors.org"

* Common mac-related issues
#+INCLUDE: "../reference/mac.org"

------------------------------------------------------------------------------
* Syllabus
#+INCLUDE: "syllabus_cs200.org"
