# -*- mode: org -*-

#+TITLE: CS 250: Basic Data Structures using C++ (Fall 2024 version)
#+AUTHOR: Rachel Wil Sha Singh

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="style/rworgmode.css" />
#+HTML_HEAD:  <link rel="icon" type="image/png" href="images/favicon-cs235.png">

# #+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/tomorrow-night-blue.min.css">
# #+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script>
# #+HTML_HEAD: <script>hljs.initHighlightingOnLoad();</script>

#+OPTIONS: toc:2
# (only include two levels in TOC)
# #+OPTIONS: num:nil

#+LaTeX_CLASS_OPTIONS: [a4paper,twoside]
#+LATEX_HEADER: \usepackage{style/rworgmode}
#+LATEX_HEADER: \usepackage[utf8]{inputenc}
#+LATEX_HEADER: \usepackage{graphicx}
#+LATEX_HEADER: \hypersetup{colorlinks=true,linkcolor=blue}
#+LATEX_HEADER: \begin{titlepage}

#+LATEX_HEADER: \sffamily{ \textbf{  {\fontsize{2cm}{2cm}\selectfont ~\\ Basic ~\\~\\ Data Structures ~\\~\\ Using C++} } }
#+LATEX_HEADER: ~\\ ~\\ ~\\ ~\\
#+LATEX_HEADER: \includegraphics[width=\textwidth]{images/datastructures.png}
#+LATEX_HEADER: ~\\ ~\\ ~\\ ~\\
#+LATEX_HEADER: \sffamily{ \textbf{  {\fontsize{1cm}{1cm}\selectfont Fall 2024 } } }
#+LATEX_HEADER: \end{titlepage}


# -----------------------------------------------------------------------------

#+ATTR_LATEX: :width 100px
[[file:../images/semester-fall.png]]

Rachel Wil Sha Singh's Data Structures using C++ Course © 2024 by Rachel Wil Sha Singh is licensed under CC BY 4.0. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/

- Digital textbooks: https://moosadee.gitlab.io/courses/
- These course documents are written in *emacs orgmode* and the files can
  be found here: https://gitlab.com/moosadee/courses
- Dedicated to a better world, and those who work to try to create one.








#+INCLUDE: "../widgets/new-page.org"
* Navigating this course
** Recommended Fall 2024 schedule

#+INCLUDE: "Course4_DataStructures/tentative-schedule.org"







# --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"
* Reference

** C++ quick reference
#+INCLUDE: "../reference/quick_reference_cpp.org"

# --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"
** Basic computer skills
#+INCLUDE: "../reference/computer_skills.org"

# --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"
** Common C++ bugs and issues
#+INCLUDE: "../reference/common_issues_cpp.org"

# --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"
** Code and UI style guide
#+INCLUDE: "../reference/style_guide.org"

# --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"
** Using IDEs
#+INCLUDE: "../reference/ides_steps.org"

# --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"
** Using Git
#+INCLUDE: "../reference/git.org"

# --------------------------------------------------------------------------------------------------

#+INCLUDE: "../widgets/new-page.org"
* Syllabus

#+INCLUDE: "syllabus_cs250.org"

# --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"
* Reading
** Unit 00: Welcome and setup

*** Welcome!
#+INCLUDE: "shared/reading/reading_Welcome.org"

*** This course about Data Structures
#+INCLUDE: "Course4_DataStructures/reading/reading_AboutDataStructuresCourse.org"

*** Navigating the course
#+INCLUDE: "shared/reading/reading_NavigatingCourse.org"


#+INCLUDE: "../widgets/new-page.org"
*** Tools setup
#+INCLUDE: "shared/reading/reading_ToolsSetup.org"

#+INCLUDE: "../widgets/new-page.org"
** Unit 01: Tech Literacy: Exploring computers

*** Exploring computers and software
#+INCLUDE: "shared/reading/reading_ExploringComputersAndSoftware.org"

*** Software development lifecycle and tools
#+INCLUDE: "shared/reading/reading_SoftwareDevelopment.org"

#+INCLUDE: "../widgets/new-page.org"
** Unit 02: C++: CS 200 review
#+INCLUDE: "shared/reading/reading_CPPBasicsReview.org"
#+INCLUDE: "shared/reading/reading_CPPClasses.org"

#+INCLUDE: "../widgets/new-page.org"
** Unit 03: Tech Literacy: Exploring data
#+INCLUDE: "Course4_DataStructures/reading/reading_ExploringData.org"


#+INCLUDE: "../widgets/new-page.org"
** Unit 04: Tech Literacy: Debugging, testing, and researching
#+INCLUDE: "shared/reading/reading_DebugTest.org"

#+INCLUDE: "../widgets/new-page.org"
** Unit 05: Tech Literacy: Algorithm efficiency
#+INCLUDE: "shared/reading/reading_AlgorithmEfficiency.org"

#+INCLUDE: "../widgets/new-page.org"
** Unit 06: C++: Pointers
#+INCLUDE: "shared/reading/reading_Pointers.org"

#+INCLUDE: "../widgets/new-page.org"
** Unit 05: Tech Literacy: Debugging, testing, and researching (WIP)
Work in progress
# #+INCLUDE: "shared/reading/reading_.org"

#+INCLUDE: "../widgets/new-page.org"
** Unit 06: Tech Literacy: Algorithm efficiency
#+INCLUDE: "shared/reading/reading_AlgorithmEfficiency.org"

#+INCLUDE: "../widgets/new-page.org"
** Unit 07: C++: Standard Template Library
#+INCLUDE: "shared/reading/reading_STL.org"

#+INCLUDE: "../widgets/new-page.org"
** Unit 08: C++: Recursion
#+INCLUDE: "shared/reading/reading_Recursion.org"

#+INCLUDE: "../widgets/new-page.org"
** Unit 09: Tech Literacy: Building your portfolio and resume
#+INCLUDE: "shared/reading/reading_BuildingPortfolio.org"

#+INCLUDE: "../widgets/new-page.org"
** Unit 10: C++: Templates
#+INCLUDE: "shared/reading/reading_Templates.org"

#+INCLUDE: "../widgets/new-page.org"
** Unit 11: C++: Exceptions
#+INCLUDE: "shared/reading/reading_Exceptions.org"

#+INCLUDE: "../widgets/new-page.org"
** Unit 12: C++: Searching and sorting
#+INCLUDE: "shared/reading/reading_SearchSort.org"

#+INCLUDE: "../widgets/new-page.org"
** Unit 13: Tech Literacy: Intro to Data Structures
#+INCLUDE: "Course4_DataStructures/reading/reading_IntroDataStructures.org"

#+INCLUDE: "../widgets/new-page.org"
** Unit 14: C++: Array structures
#+INCLUDE: "Course4_DataStructures/reading/reading_SmartArrays.org"

#+INCLUDE: "../widgets/new-page.org"
** Unit 15: C++ Hash Table structure
#+INCLUDE: "Course4_DataStructures/reading/reading_HashTable.org"

#+INCLUDE: "../widgets/new-page.org"
** Unit 16: C++: Linked List structure
#+INCLUDE: "Course4_DataStructures/reading/reading_LinkedList.org"

#+INCLUDE: "../widgets/new-page.org"
** Unit 17: C++: Stack and Queue structures
#+INCLUDE: "Course4_DataStructures/reading/reading_StackAndQueue.org"

#+INCLUDE: "../widgets/new-page.org"
** Unit 18: Tech Literacy: Intro to Trees
#+INCLUDE: "Course4_DataStructures/reading/reading_IntroToTrees.org"

#+INCLUDE: "../widgets/new-page.org"
** Unit 19: C++: Binary Search Tree structure
#+INCLUDE: "Course4_DataStructures/reading/reading_BinarySearchTree.org"

# --------------------------------------------------------------------------------------------------



# --------------------------------------------------------------------------------------------------
#+INCLUDE: "../widgets/new-page.org"
* Review questions
** Mastery check 1 review
*** Source control
#+INCLUDE: "shared/notes/GitSourceControl.org"

*** Unit 02: CS 200 review
#+INCLUDE: "shared/notes/CS200Review.org"

*** Unit 04: Debugging, Testing, and Research
#+INCLUDE: "shared/notes/DebugTest.org"

*** Unit 05: Tech Literacy: Algorithm efficiency
#+INCLUDE: "shared/notes/AlgorithmEfficiency.org"

*** Unit 06: Pointers
#+INCLUDE: "shared/notes/Pointers.org"


#+INCLUDE: "../widgets/new-page.org"
** Mastery check 2 review

*** Unit 07: Standard Template Library
#+INCLUDE: "shared/notes/STL.org"

# *** Unit 08: Recursion
# +INCLUDE: "shared/notes/.org"

*** Unit 10: Templates
#+INCLUDE: "shared/notes/Templates.org"

*** Unit 11: Exceptions
#+INCLUDE: "shared/notes/Exceptions.org"


#+INCLUDE: "../widgets/new-page.org"
** Mastery check 3 review

*** Unit 13: Intro to Data Structures
Work in progress

*** Unit 14: Array structures
#+INCLUDE: "Course4_DataStructures/notes/Arrays.org"

*** Unit 15: Hash Table structure
#+INCLUDE: "Course4_DataStructures/notes/HashTables.org"


#+INCLUDE: "../widgets/new-page.org"
** Mastery check 4 review

*** Unit 16: Linked List structure
#+INCLUDE: "Course4_DataStructures/notes/LinkedList.org"

*** Unit 17: Stack and Queue structures
#+INCLUDE: "Course4_DataStructures/notes/StackQueue.org"


#+INCLUDE: "../widgets/new-page.org"
** Mastery check 5 review

*** Unit 18: Intro to Trees
Work in progress

*** Unit 19: Binary Search Tree structure
#+INCLUDE: "Course4_DataStructures/notes/BinarySearchTrees.org"

# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------- # --------------------------------------------------------------------------------------------------
# #+INCLUDE: "../widgets/new-page.org"
# * In-class presentations
# ** Unit 00: Welcome and setup
# #+INCLUDE: "Course4_DataStructures/slides/slides_Welcome.org"
# 
# ** Unit 00 Lab guide: Setting up your system and tools
# #+INCLUDE: "shared//slides/slides_Setup.org"


#+INCLUDE: "../widgets/new-page.org"
* Class session summaries
** Monday, August 26th, 2024

- Syllabus / Course policies overview
  - Course textbook link https://moosadee.gitlab.io/courses/ has this book (HI!), link to archived course videos, archived example code.
  - The course is 3 hours long but it is whatever you all want to do. Usually I'll start off with some example code and answering questions,
    then I suggest that you work on labs for the class time so you can easily ask me questions if you get stuck.
  - You can attend in person, remote via Zoom, or you can watch the lectures after the fact; no points assigned to attendance.
  - Course is self-paced; *due dates* are recommended. The *end dates* are hard-stops on assignments. Get any late work in by then.
    - Work turned in after the due date is marked "Late" in Canvas but I don't take off points.
    - Once an assignment closes I won't open it back up. (Assignments begin closing in November.)
    - Several units are available up until a *milestone*. A milestone contains an exam and a project. Completing these will unlock the next set of units.
      - If you score 60% or higher on the exam the next units will unlock. If you score less than 60% I'll open the exam back up for you but we should meet up first.
      - Exam must be completed within 6 hours of when you start it (it will not pause if you exit the exam to resume later). Exam does NOT take that long to complete,
        I just want you to complete it the same day you start it.
  - After you turn in a lab and I grade it you can go back and fix things up. I'll add comments if there is something wrong.
  - All assignments begin at 0%, and your grade will go up from there as you complete assignments. Your grade won't go down (unless I find instances of plagiarism and remove points).
  - You can use whatever IDE you want, but I'd recommend Visual Studio or Code::Blocks for Windows, VS Code for Mac users.
- Unit 00: Tools setup
  - Follow along with the Unit 00 section under "Reading" in this book.
  - Create a GitLab account (*Setup: What is your GitLab username?* assignment on Canvas).
  - Install Git
  - Install an IDE
  - Clone your repository
  - Make a basic project, edit a code file, build and run
  - Add/commit/push your changes to server
  - Create merge conflict and resolve
  - *Lab: Setting up your system & tools* assignment turn-in.
- Unit 01: Exploring computers - Read thru on your own
- Unit 02: CS 200 Review
  - To get 100% on a lab, only the *graded program(s)* need to be done.
  - Turning in the *practice* programs will give you additional extra credit points. The practice programs are there
    for if you're unfamiliar with a topic and want to practice the concepts in an easier form before doing the graded assignment.
- NEXT WEEK, Monday, Sept 2nd is Labor Day - *No class*

