# -*- mode: org -*-

* *Design and polymorphism*

So much of the design tricks and features we utilize in C++ and other
object-oriented programming languages all stem from the concept of
"do not repeat yourself". If you're writing the same
set of code in multiple places, there is a chance that we could
design the program so that we only need to write that code once.


#+ATTR_LATEX: :width 0.5\textwidth
[[file:images/reading_u17_Polymorphism_family.png]]

Polymorphism is a way that we can utilize pointers and something called
*vtables* to have a family of classes (related by inheritance)
and be able to write one set of code to handle interfacing with
/all of those family members/.
We have a family tree of classes, and we can write our program to
treat all the objects as the *parent class*, but the program will
decide which set of functions to call at run time.

--------------------------------------------------------------------------------
*C++ source:* Using Polymorphism to treat ChildA and ChildB objects as their Parent object

#+BEGIN_SRC
Parent* myPtr = nullptr;
if      ( type == 1 ) { myPtr = new ChildA; }
else if ( type == 2 ) { myPtr = new ChildB; }

myPtr->Display();
delete myPtr;
#+END_SRC
--------------------------------------------------------------------------------

#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

** *Example: Quizzer and multiple question types*

Let's say we are writing a quiz program and
there are different types of questions: True/false questions,
multiple choice, and fill-in-the-blank. They all have a common
question string, but how they store their answers is different...

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*Class diagrams:*

#+ATTR_LATEX: :mode table :align | l | c | l |
|--------------------------+---+-------------------------|
| *Question*               |   | *TrueFalseQuestion*     |
|--------------------------+---+-------------------------|
| # =m_question= : string  |   | # =m_question= : string |
|                          |   | # =m_answer= : bool     |
|                          |   |                         |
|--------------------------+---+-------------------------|
| + bool AskQuestion()     |   | + bool AskQuestion()    |
| + void DisplayQuestion() |   |                         |
|--------------------------+---+-------------------------|


#+ATTR_LATEX: :mode table :align | l | c | l |
|---------------------------+---+-------------------------|
| *MultipleChoiceQuestion*  |   | *FillInQuestion*        |
|---------------------------+---+-------------------------|
| # =m_question= : string   |   | # =m_question= : string |
| # =m_options= : string[4] |   | # =m_answer= : string   |
| # =m_correct= : int       |   |                         |
|---------------------------+---+-------------------------|
| + bool AskQuestion()      |   | + bool AskQuestion()    |
| + void ListAllAnswers()   |   |                         |
|---------------------------+---+-------------------------|

How would you store a series of inter-mixed quiz questions in a program?
Without polymorphism, you might think to just have separate vectors or arrays
for all the questions:

--------------------------------------------------------------------------------
*C++ source:* Not using Polymorphism - Having to create /separate/ vectors for each Question type

#+BEGIN_SRC
vector<TrueFalseQuestion>       tfQuestions;
vector<MultipleChoiceQuestion>  mcQuestions;
vector<FillInQuestion>          fiQuestions;
#+END_SRC
--------------------------------------------------------------------------------

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

Utilizing polymorphism in C++, we could simply store an array of
pointers of the parent type:

--------------------------------------------------------------------------------
*C++ source:* Creating a vector of parent class pointers

#+BEGIN_SRC
vector<Question*> questions;
#+END_SRC

And then initialize the question as the type we want during creation:

*C++ source:* Storing different Question types in one vector

#+BEGIN_SRC
questions.push_back(new TrueFalseQuestion);
questions.push_back(new MultipleChoiceQuestion);
questions.push_back(new FillInQuestion);
#+END_SRC
--------------------------------------------------------------------------------

Since we are using the `new` keyword here, we would
also need to make sure to `delete` these items at the end
of the program:

--------------------------------------------------------------------------------
*C++ source:* Iterating over all the Question objects in the vector

#+BEGIN_SRC
for (auto& question : questions)
{
    delete question;
}
#+END_SRC
--------------------------------------------------------------------------------

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

- Other design considerations ::

When we're working with polymorphism in this way, we need to be able
to treat each child as its parent, from a "calling functions"
perspective. Each child can have its own unique member functions and variables,
but when we're making calls to functions via a pointer to the parent type,
the parent only knows about functions that it, itself, has.

Let's say that the `Question` class has a `DisplayQuestion()`
function. Since all its children use `m_question` in the same way
and inherit this function, it will be fine to call it via the pointer.

--------------------------------------------------------------------------------
*C++ source:* Calling a common function (DisplayQuestion) that is part of the Question family tree

#+BEGIN_SRC
ptrQuestion->DisplayQuestion(); // ok
#+END_SRC
--------------------------------------------------------------------------------

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

But with a function that belongs to a child - not the parent's interface -
we wouldn't be able to call that function via the pointer without casting.

--------------------------------------------------------------------------------
*C++ source:* Calling a function that doesn't belong to the Question family - just one of the subclasses

#+BEGIN_SRC
ptrQuestion->ListAllAnswers();  // not ok

(static_cast<MultipleChoiceQuestion*>(ptrQuestion))
   ->ListAllAnswers(); // ok
#+END_SRC
--------------------------------------------------------------------------------

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

You could, however, still call that =ListAllAnswers= function
from within =MultipleChoiceQuestion='s =DisplayQuestion= function,
and that would still work fine...

--------------------------------------------------------------------------------
*C++ source:* Calling the specialized function from within the general function

#+BEGIN_SRC
bool MultipleChoiceQuestion::AskQuestion()
{
    DisplayQuestion();
    ListAllAnswers();
    // etc.
}
#+END_SRC
--------------------------------------------------------------------------------

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

Still fuzzy? That's OK, this is just an overview; we're going to step
into how all this works more in-depth next.

#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

* *Reviewing classes and pointers*

** *Review: Class inheritance and function overriding*

#+ATTR_LATEX: :width 0.4\textwidth
[[file:images/reading_u17_Polymorphism_questioninherit.png]]

Some things to remember about inheritance with classes:

- Any public or protected members (functions and variables) are inherited by the child class.
  (e.g., =m_question=, =DisplayQuestion()=, and =AskQuestion()=).
- A child class can override the a parent's function by declaring and defining a function with the same signature.
  (e.g., =AskQuestion()=).
- If the child class doesn't override a parent's function, then when that function is called via the child object
  it will call the parent's version of that function. (e.g., =DisplayQuestion()=).

#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

** *Review: Pointers to class objects*

You can declare a pointer to point to the address of an existing object,
or use the pointer to allocate memory for one or more new instances
of that class...

- Pointer to existing address: =myPtr = &existingQuestion;=
- Pointer to allocate memory: =myPtr = new Question;=

Then, to access a member of that object via the pointer, we use the =->= operator,
which is equivalent to dereferencing the pointer and then accessing a member:

- Arrow operator: =myPtr->DisplayQuestion();=
- Dereference and access: =(*myPtr).DisplayQuestion();=


#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX


* *Which version of the method is called?*

Let's say we have several objects already declared:

--------------------------------------------------------------------------------
*C++ source:* Declaring multiple Question family objects

#+BEGIN_SRC
Question q1, q2;
MultipleChoiceQuestion mc1;
#+END_SRC
--------------------------------------------------------------------------------

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

We could create a =Question*= ptr that points to
=q1= or =q2= or even =mc1=...

--------------------------------------------------------------------------------
*C++ source:* Parent class pointer, pointing to different classes...

#+BEGIN_SRC
Question* ptr;
ptr = &q1;  // ok
ptr = &q2;  // ok
ptr = &mc1; // ok?
#+END_SRC
--------------------------------------------------------------------------------

#+ATTR_LATEX: :width 0.4\textwidth
[[file:images/reading_u17_Polymorphism_questioninherit.png]]

And, any functions that the =Question= class and
the =MultipleChoiceQuestion= class could be called
from this pointer...

--------------------------------------------------------------------------------
*C++ source:* Calling a function that belongs to the parent class...

#+BEGIN_SRC
ptr->DisplayQuestion();
#+END_SRC
--------------------------------------------------------------------------------

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

This is fine for any member methods *not overridden* by the child class.
But, which version of the function is called if we used an *overridden* method?

--------------------------------------------------------------------------------
*C++ source:* Trying to call a function that has different versions for each class in the family...

#+BEGIN_SRC
ptr->AskQuestion();
#+END_SRC
--------------------------------------------------------------------------------

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

#+ATTR_LATEX: :width \textwidth
[[file:images/reading_u17_Polymorphism_whichone.png]]


#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

** *No virtual methods - Which =AskQuestion()= is called?*

Let's say our class declarations look like this:


#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

--------------------------------------------------------------------------------
*Terminal:*
*File output:*
*Program output:*
*C++ source:* Question parent class declaration

#+BEGIN_SRC
class Question
{
    public:
    bool AskQuestion();
    // etc.
};
#+END_SRC
--------------------------------------------------------------------------------

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

--------------------------------------------------------------------------------
*Terminal:*
*File output:*
*Program output:*
*C++ source:* MultipleChoiceQuestion declaration

#+BEGIN_SRC
class MultipleChoiceQuestion : public Question
{
    public:
    bool AskQuestion();
    // etc.
};
#+END_SRC
--------------------------------------------------------------------------------

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

Here are the outputs we could have from using pointers in different ways:

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

--------------------------------------------------------------------------------
*A. =Question*= pointer, =Question='s =AskQuestion()= is called:**C++ source:*

#+BEGIN_SRC
Question* ptr = new Question;
bool result = ptr->AskQuestion();
#+END_SRC


*B. =MultipleChoiceQuestion*= pointer, =MultipleChoiceQuestion='s =AskQuestion()= is called:*

#+BEGIN_SRC
MultipleChoiceQuestion* ptr = new MultipleChoiceQuestion;
bool result = ptr->AskQuestion();
#+END_SRC


*C. =Question*= pointer, =Question='s =AskQuestion()= is called:*

#+BEGIN_SRC
Question* ptr = new MultipleChoiceQuestion;
bool result = ptr->AskQuestion();
#+END_SRC
--------------------------------------------------------------------------------

"Well, how is that useful at all? The function called matches the
pointer data type!" - true, but we're missing one piece that allows us to
call *any child's version of the method* from a pointer of the parent type...



#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

** *Virtual methods - Which =AskQuestion()= is called?*


Instead, let's mark our method with the *virtual* keyword:

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

--------------------------------------------------------------------------------
*C++ source:* Question class with =virtual= function

#+BEGIN_SRC
class Question
{
    public:
    virtual bool AskQuestion();
    // etc.
};
#+END_SRC

*C++ source:* MultipleChoiceQuestion class with =virtual= function

#+BEGIN_SRC
class MultipleChoiceQuestion : public Question
{
    public:
    virtual bool AskQuestion();
    // etc.
};
#+END_SRC
--------------------------------------------------------------------------------

Here are the outputs we could have from using pointers in different ways:


--------------------------------------------------------------------------------
*A. =Question*= pointer, =Question='s =AskQuestion()= is called:*

#+BEGIN_SRC
Question* ptr = new Question;
bool result = ptr->AskQuestion();
#+END_SRC

*B. =MultipleChoiceQuestion*= pointer, =MultipleChoiceQuestion='s =AskQuestion()= is called:*

#+BEGIN_SRC
MultipleChoiceQuestion* ptr = new MultipleChoiceQuestion;
bool result = ptr->AskQuestion();
#+END_SRC

*C. =Question*= pointer, =MultipleChoiceQuestion='s =AskQuestion()= is called:*

#+BEGIN_SRC
Question* ptr = new MultipleChoiceQuestion;
bool result = ptr->AskQuestion();
#+END_SRC

--------------------------------------------------------------------------------

With this, we can now store a list of *Question** objects,
and each question can be a different child class, but we can
write one set of code to interact with each one of them.

#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

* *Virtual methods, late binding, and the Virtual Table*

By using the *virtual* keyword, something happens with our functions -
it allows the pointer-to-the-parent class to figure out /which version/
of the method to actually call, instead of just defaulting to the parent class' version.
But how does this work?

*The =virtual= keyword* tells the compiler that the function
called will be figured out later. By marking a function as *virtual*,
it then is added to something called a *virtual table* - or *vtable*.

The /vtable/ stores special /pointers to functions/. If a class
contains /at least one virtual function/, then it will have its own vtable.

[[file:images/reading_u17_Polymorphism_vtables1.png]]

With the *Question* class, it isn't inheriting any methods from anywhere
else so the vtable reflects the same methods it has. But, we also have
the child class that inherits *DisplayQuestion()* and overrides *AskQuestion()*.

[[file:images/reading_u17_Polymorphism_vtables2.png]]

Because of these *vtables*, we can then have our pointers reference
this vtable when figuring out which version of a method to call.
Doing this is called *late binding* or *dynamic binding*.


#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

** *When should we use =virtual=?*

- Destructors should always be virtual. ::

If you're working with inheritance.
By making your destructor *virtual* for each class in the family,
you are ensuring that the *correct destructor* will be called when
the object is destroyed or goes out of scope. If you don't make it virtual
and utilize polymorphism, the correct destructor may not be called
(i.e., =Question='s instead of =MultipleChoiceQuestion='s).

- Constructors cannot be marked =virtual= ::

When the object is instantiated (e.g., =ptr = new MultipleChoiceQuestion;=)
that class' constructor will be called already.

- Not every function needs to be virtual. ::

It's all about design. Though generally, if you always want the parent's
version of a method to be called, you wouldn't override that method in
the child class anyway.


#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

** *Designing interfaces with pure virtual functions and abstract classes*

Polymorphism works best if you're designing a family of classes around
some sort of *interface* that they will all share. In the C# language,
there is an interface type that is available to you, but that's not here
in C++, so we implement it via classes.

- What is an Interface? ::

When we're designing a class to be an interface, the idea is that the
user (or other programmers) will just see a set of functions it will
interface with - none of the behind-the-scenes, how-it-works stuff.

Most of the devices we use have some sort of *interface*,
hiding the more complicated specifics of how it actually works within a case.
For example, a calculator has a simple interface of buttons, but if you opened
it up you would be able to see its hardware and how everything is hooked up.

We use the same idea with writing software, where we expose some interface
(in the form of the class' *public methods*) as how the "user"
interacts with our class.

#+ATTR_LATEX: :width 0.5\textwidth
[[file:images/reading_u17_Polymorphism_questionmanager.png]]

A common design practice is to write the first *base (parent) class*
to be a specification of this sort of *interface* that all its children
will adhere to, and to ensure that each child class *must follow the interface*
by using something that the compiler will enforce itself: pure virtual functions.



When working with our Quiz program idea, our *base class* is *Question*,
which would define the interface for all other types of Questions.
Generally, our base interface class *would never be instantiated* -
it is not complete in and of itself (i.e., a Question with no types of Answers) -
but is merely used to outline a common interface for its family members.


Here is a blank diagram with just the member variables defined, but not yet
any functionality, so that we can begin to step through thinking about an interface:

#+ATTR_LATEX: :width \textwidth
[[file:images/reading_u17_Polymorphism_questionfamily.png]]

Thinking in terms of implementing a program that could *edit questions*
(such as the teacher's view of the quiz), as well as that could
*ask questions* (such as the student's view), we can try to think of
what kind of functionality we would need from a question...


-    Setup the question, answer(s)
-    Display the question to the user
-    Get the user's answer
-    Check if the user's answer was correct


But, the specifics of how each of these question types stores the correct
answer (and what data type it is) and validates it differ between each of them...

|               | *User answer* | *Stored answer*   | *Validate*             |
| *True/false*  | bool          | bool answer       | *Userinput == answer?* |
| *MultiChoice* | int           | string options[4] | *Userinput == answer?* |
| *FillIn*      | int           | string answer     | *Userinput == answer?* |


We could design our Questions so that they have functionality that interacts
with the user directly (e.g., a bool function that asks the user to enter
their response and returns true if they got it right and false if not)
rather than writing functions around returning the actual answer (which would
be more difficult because they have different data types).


-    Set up question
-    Run question


- Declarations: ::
We can set up a simple interface for our Questions with these functions.
They've been marked as =virtual=, which allows us to use polymorphism,
and they've also been marked with == 0= at the end, marking them as
*pure virtual* - this tells the compiler that child classes *must*
implement their own version of these methods. A function that contains
pure virtual methods is called an *abstract class*.

--------------------------------------------------------------------------------
*C++ source:* Question interface with pure virtual functions

#+BEGIN_SRC
class Question
{
  public:
  virtual void Setup() = 0;
  virtual bool Run() = 0;

  protected:
  string m_question;
};
#+END_SRC
--------------------------------------------------------------------------------

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX


Now our child classes can inherit from =Question=. They will be
required to override =Setup()= and =Run()=, and we can also
have additional functions as needed for that implementation:

--------------------------------------------------------------------------------
*C++ source:* Inheriting from the Question interface class

#+BEGIN_SRC
class MultipleChoiceQuestion : public Question
{
  public:
  virtual void Setup();
  virtual bool Run();
  void ListAllAnswers();

  protected:
  string m_options[4];
  int m_answer;
};
#+END_SRC
--------------------------------------------------------------------------------

- Definitions: ::
Each class will have its own implementation of these interface functions,
but since they're part of an interface, when we build a program around
these classes later we can call all of them the same way.


#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

--------------------------------------------------------------------------------
*C++ source:* Question - Defining the Setup function

#+BEGIN_SRC
void Question::Setup() {
    cout << "Enter question: ";
    getline( cin, m_question );
}
#+END_SRC

*C++ source:* TrueFalseQuestion - Overwriting the Setup function calling the parent class' Setup function

#+BEGIN_SRC
void TrueFalseQuestion::Setup() {
    Question::Setup();
    cout << "Enter answer (0 = false, 1 = true): ";
    cin >> m_answer;
}
#+END_SRC

*C++ source:* MultipleChoiceQuestion

#+BEGIN_SRC
void MultipleChoiceQuestion::Setup() {
  Question::Setup();

  for ( int i = 0; i < 4; i++ )
    {
      cout << "Enter option " << i << ": ";
      getline( cin, m_options[i] );
    }

  cout << "Which index is correct? ";
  cin >> m_answer;
}
#+END_SRC

*C++ source:* FillInQuestion

#+BEGIN_SRC
void FillInQuestion::Setup() {
    Question::Setup();
    cout << "Enter answer text: ";
    getline( cin, m_answer );
}
#+END_SRC
--------------------------------------------------------------------------------


- Function calls: ::
Now, no matter what /kind/ of question subclass we're using,
we can utilize the same interface - and the same code.

--------------------------------------------------------------------------------
*C++ source:* Using Polymorphism to create the question and calling the common Setup and Run functions

#+BEGIN_SRC
// Create the pointer
Question* ptr = nullptr;

// Allocate memory
if ( choice == "true-false" )
{
    ptr = new TrueFalseQuestion();
}
else if ( choice == "multiple-choice" )
{
    ptr = new MultipleChoiceQuestion();
}
else if ( choice == "fill-in" )
{
    ptr = new FillInQuestion();
}

// Set up the question
ptr->Setup();

// Run the question
ptr->Run();

// Free the memory
delete ptr;
#+END_SRC
--------------------------------------------------------------------------------

And, utilizing this interface, we could then store a =vector<Question*>=
and set up each question as any question subclass without any duplicate code.


#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

* *Example usage: Game objects*

Let's say we have created a family tree of game objects, starting at the most basic object that
has an $(x, y)$ coordinate and dimensions:

#+ATTR_LATEX: :mode table :align | l |
|------------------------------|
| *GameObject*                 |
|------------------------------|
| + =GameObject()=             |
| + =Setup( ... )= : void      |
| + =SetTexture( ... )= : void |
| + =GetName()= : string       |
| + =Update()= : void          |
| + =Draw()= : void            |
| (etc)                        |
|------------------------------|
| # =position= : Vector2f      |
| # =name= : string            |
| # =sprite= : Sprite          |
| # =tx_coord= : IntRect       |
|------------------------------|

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

Then we might have something like an unanimated item in the world,
but maybe it has physics so it needs the ability to update,
and maybe it has some properties you wouldn't see on a character,
like the ability to pick it up, or heal a character.

#+ATTR_LATEX: :mode table :align | l |
|-----------------------------------|
| *Item* (inherits from GameObject) |
|-----------------------------------|
| + =Setup( ... )= : void           |
| (etc)                             |
|-----------------------------------|
| # =can_pick_up= : bool            |
| # =heal_amount= : int             |
|-----------------------------------|

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

But then our player and NPC characters also have animated sprites
and the ability to move with keyboard input or rudimentary AI:

#+ATTR_LATEX: :mode table :align | l |
|----------------------------------------|
| *Character* (inherits from GameObject) |
|----------------------------------------|
| + =Setup( ... )= : void                |
| + =SetSpeed( float speed )= : void     |
| + =SetDirection( int dir )= : void     |
| + =Move( int dir )= : void             |
| + =Animate()= : void                   |
| (etc)                                  |
|----------------------------------------|
| # =speed= : float                      |
| # =direction= : int                    |
|----------------------------------------|

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

If we didn't use polymorphism, we would have to store all objects in their own vector:

--------------------------------------------------------------------------------
*C++ source:* Example of storing objects separately

#+BEGIN_SRC
vector<Character> m_npcList;
vector<Item> m_pickups;
vector<GameObject> m_decor;
#+END_SRC
--------------------------------------------------------------------------------

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

But utilizing polymorphism, we can store one vector of =GameObject*= objects initialized on the heap,
and any common functionality they have (Update, Draw, etc.) could be accessed via that pointer.

--------------------------------------------------------------------------------
*C++ source:* Using Polymorphism for the game entities

#+BEGIN_SRC
// Our storage
vector<GameObject*> m_entities;

// Creating a new item (elseware in program)
GameObject* newItem = new Item;

// Adding it to the list
m_entities.push_back( newItem );

// Accessing it later
for ( auto& entity : entities )
{
  entity->Update();
}
#+END_SRC
--------------------------------------------------------------------------------
