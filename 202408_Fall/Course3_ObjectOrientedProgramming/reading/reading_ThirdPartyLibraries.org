# -*- mode: org -*-

* *Setting up third party libraries*

** *Quick checklists*
*** *Setting up a library in Visual Studio*

1. Download library from website.
2. Extract library to your hard-drive.
3. Create a new *Empty project* in Visual Studio.
4. Create a new source file (*main.cpp*) in Visual Studio.
5. Right-click on your project and go to *Properties*.
6. Make sure your *Configuration* is set to *Release* and your *Platform* is set to *x64*.
7. Under *C/C++ > General*, add SFML's *include* path to the *Additional Include Directories* property.
8. Under *Linker > General*, add SFML's *lib* path to the *Additional Library Directories* property.
9. Under *Linker > Input*, click the "v" dropdown on *Additional Dependencies* and click *<Edit...>*. Add your related .lib files (e.g., sfml-graphics.lib, sfml-window.lib, sfml-system.lib).
10. Click *OK*.
11. Move the .dll files (sfml-graphics-2.dll, sfml-window-2.dll, sfml-system-2.dll) from the SFML/bin folder to your project directory.
12. Done with configuration.

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*** *Setting up a library in Code::Blocks (Windows)*

1. Download library from website.
2. Extract library to your hard-drive.
3. Create a new *Empty project* in Code::Blocks.
4. Create a new empty file (*main.cpp*) in Code::Blocks.
5. Right-click on your project and go to *Build options...*
6. Make sure you've selected *Release* instead of Debug on the left-hand side.
7. Under *Search directories > Compiler*, add SFML's *include* path.
8. Under *Search directories > Linker*, add SFML's *lib* path.
9. Under *Linker settings* and *Link libraries:*, add the libraries: sfml-system, sfml-graphics, sfml-window.
10. Click *OK*.
11. Move the .dll files (sfml-graphics-2.dll, sfml-window-2.dll, sfml-system-2.dll) from the SFML/bin folder to your project directory.
12. Done with configuration.





#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

** *Setting up third-party libraries in your IDE*

*** *Downloading and extracting the library*

For these instructions we will be working with the SFML (Simple Fast Media Library) library, but setting up most libraries will work about the same.

**** *Getting a library in Windows*

*Downloading:* Go to the library webpage (https://www.sfml-dev.org/download.php) and select "SFML", latest stable version (as of writing this is 2.6.1).

From the downloads page, there are various options for a Windows download.

#+ATTR_LATEX: :width \textwidth
#+CAPTION: An image of the SFML downloads page, showing different versions that can be downloaded for Windows.
[[file:images/third_party_libraries_sfmlpage.png]]

If you're working with *Visual Studio*, then download the latest version, even if the year doesn't match. You'll probably want the *64-bit* verison,
since most computers these days are 64-bit.

If you're working with *Code::Blocks*, you will probably want the *64-bit SEH* option ("GCC 13.1.0 MinGW (SEH) - 64-bit"), but you can double-check
which version you need by going to your CodeBlocks install directory (such as C:\Program Files\CodeBlocks\MinGW\bin\) and looking for a file called
"libgcc"... based on this version it will tell you which version of SFML you need to download:

#+ATTR_LATEX: :width 0.7\textwidth
[[file:images/reading_u19_ThirdPartyLibraries_sfmlpage_libgcc.png]]

In *Windows* I like to have a "LIBRARIES" folder on my root directory, such as "C:\LIBRARIES\". So, all extracted, my default path for SFML would be:

=C:\LIBRARIES\SFML-2.6.1=


#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

**** *Getting a library in Linux*

You can download many libraries from your distro's *package manager*, such as synaptic for Debian-based systems.
Do a search for "libsfml" and download the *libsfml-dev* package. (Most libraries are "libXYZ-dev" as well.)

#+ATTR_LATEX: :width \textwidth
[[file:images/reading_u19_ThirdPartyLibraries_synaptic.png]]

The library will be automatically installed on your machine, with the relevant paths for configuration being.


#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

**** *Keeping track of important paths*

You'll need to know where your library files are being kept while we're configuring the project,
so make sure to take note of the paths. Note that if you're in Windows, you might have unzipped your
library to a different directory than me, and will need to replace "C:\LIBRARIES\" with wherever you
put your SFML folder.

- *.dll location:* When distributing your .exe for Windows, you'll want to include the relevant .dll files, held here.
  - Windows: =C:\LIBRARIES\SFML-2.6.1\bin\=


#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX


- *Library files:* SFML's C++ code is compiled into libraries, available here.
  - Windows: =C:\LIBRARIES\SFML-2.6.1\lib\=
  - Linux (and Mac?): =/usr/lib/x86_64-linux-gnu/=


#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX


- *.hpp files:* This shows the IDE what function and class declarations are available.
  - Windows: =C:\LIBRARIES\SFML-2.6.1\include\=
  - Linux (and Mac?): =/usr/include/=




#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

** *Setting up the project*

You can also refer to the SFML Tutorials page on how to set up Visual Studio, Code::Blocks,
and other platforms.

- *How to configure in Visual Studio:* https://www.sfml-dev.org/tutorials/2.6/start-vc.php
- *How to confingure in Code::Blocks (MinGW):* https://www.sfml-dev.org/tutorials/2.6/start-cb.php

The following write-ups are step-by-step guides with screenshots to show you how to configure
Visual Studio and Code::Blocks for SFML...







#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

*** *Visual Studio*

*1. Create a project:* Create an Empty C++ Project in Visual Studio.

#+ATTR_LATEX: :width \textwidth
[[file:images/reading_u19_ThirdPartyLibraries_vs1_2023.png]]

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*2.* Set your *Project name* and *Location*, then click *Create*.

#+ATTR_LATEX: :width \textwidth
[[file:images/reading_u19_ThirdPartyLibraries_vs2_2023.png]]

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*3. Create a source file:* Create a new .cpp file for your project by going to your *Solution Explorer*,
right-clicking on the project, going to *Add*, then selecting *New Item...*.

#+ATTR_LATEX: :width 0.8\textwidth
[[file:images/reading_u19_ThirdPartyLibraries_vs3_2023.png]]


#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*4.* Name your new file "main.cpp" and click *Add*.

[[file:images/reading_u19_ThirdPartyLibraries_vs4_2023.png]]

In your main.cpp, it can be useful to add some starter code to make sure your project
will build properly with the library linked within. Here is a very stripped-down SFML program:

--------------------------------------------------------------------------------
*C++ source:* Starter code

#+BEGIN_SRC
#include <SFML/Graphics.hpp>

int main()
{
  sf::RenderWindow window(sf::VideoMode(200, 200), "Window");
  return 0;
}
#+END_SRC
--------------------------------------------------------------------------------

At first this will generate build errors, but once we have the library set up properly, it should compile.





#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*5. Go to the properties:* Right-click on your project in the *Solution Explorer* and select *Properties*.

#+ATTR_LATEX: :width 0.6\textwidth
[[file:images/reading_u19_ThirdPartyLibraries_vs5_2023.png]]

Make sure at the top of this screen, to set the *Configuration* to Release and *Platform* to x64.

#+ATTR_LATEX: :width \textwidth
[[file:images/reading_u19_ThirdPartyLibraries_vs6_2023.png]]

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*6. Set up the include directory:* On the left-hand side, open the *C/C++* category and the *General* sub-category.
In the *Additional Include Directories* property, paste the path to your *SFML* directory's *include* folder, such as:

=C:\LIBRARIES\SFML-2.6.1\include=

#+ATTR_LATEX: :width \textwidth
[[file:images/reading_u19_ThirdPartyLibraries_vs7_2023.png]]

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*7. Set up the lib directory:* Now under the *Linker* category and the *General* sub-category,
add the path for the *SFML* directory's *lib* folder to the *Additional Library Directories* property, such as:

=C:\LIBRARIES\SFML-2.6.1\lib=

#+ATTR_LATEX: :width \textwidth
[[file:images/reading_u19_ThirdPartyLibraries_vs8_2023.png]]

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*8. Set up dependencies:* Under the *Linker* category and the *Input* sub-category,
click the "v" button next to *Additional Dependencies*. Click the *<Edit...>* button.

#+ATTR_LATEX: :width \textwidth
[[file:images/reading_u19_ThirdPartyLibraries_vs9_2023.png]]

In the pop-up box, add the following in the top text box:


#+BEGIN_SRC
sfml-graphics.lib
sfml-window.lib
sfml-system.lib
#+END_SRC

#+ATTR_LATEX: :width 0.6\textwidth
[[file:images/reading_u19_ThirdPartyLibraries_vs10_2023.png]]

Click OK when done with setting up these properties.

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*9. Build the program:* In the dropdown near the play button, select *Release* as our build type,
and make sure *x64* is selected as our platform.

#+ATTR_LATEX: :width 0.6\textwidth
[[file:images/reading_u19_ThirdPartyLibraries_vs11_2023.png]]

Now when you build there shouldn't be any errors:

#+ATTR_LATEX: :width \textwidth
[[file:images/reading_u19_ThirdPartyLibraries_vs12_2023.png]]

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*10. Adding the .dll files to the project:*

If we try to run the program right now, it will complain about missing dll files:

#+ATTR_LATEX: :width 0.8\textwidth
[[file:images/reading_u19_ThirdPartyLibraries_vs13_2023.png]]

We need to copy the dll files from the SFML library to our project now.

Right-click on the project and select *Open Folder in File Explorer*.

#+ATTR_LATEX: :width 0.6\textwidth
[[file:images/reading_u19_ThirdPartyLibraries_vs14_2023.png]]

In the SFML directory's *bin* folder (e.g., =C:\LIBRARIES\SFML-2.6.1\bin\=), copy the
relevant library files (sfml-graphics-2.dll, sfml-system-2.dll, sfml-window-2.dll).

#+ATTR_LATEX: :width \textwidth
[[file:images/reading_u19_ThirdPartyLibraries_vs15_2023.png]]

Paste these files into your project directory:

[[file:images/reading_u19_ThirdPartyLibraries_vs16_2023.png]]

Your program should now start, open, and close without any error messages. Currently the program
doesn't really do anything because there's no game loop within. Reference the
SFML setup page (https://www.sfml-dev.org/tutorials/2.6/start-vc.php) for some test code you can use
to verify that everything works properly.



*My Visual Studio setup video:* https://www.youtube.com/watch?v=4BoB5H2Kvzo


#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

*** *Code::Blocks (Windows/Linux)*

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*1. Create a project:* Create a new empty project in Code::Blocks.

#+ATTR_LATEX: :width 0.8\textwidth
[[file:images/reading_u19_ThirdPartyLibraries_cb1_2023.png]]

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*2.* Set the *Project title* and *Folder to create project in*, then click *Next >*. Use the default options,
then click *Finish* to create the project.

#+ATTR_LATEX: :width 0.8\textwidth
[[file:images/reading_u19_ThirdPartyLibraries_cb2_2023.png]]

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*3. Create a source file:* Go to *File > New > Empty file* and create a new file named "main.cpp".

#+ATTR_LATEX: :width 0.8\textwidth
[[file:images/reading_u19_ThirdPartyLibraries_cb3_2023.png]]

In your main.cpp, it can be useful to add some starter code to make sure your project
will build properly with the library linked within. Here is a very stripped-down SFML program:

--------------------------------------------------------------------------------
*C++ source:* Starter code

#+BEGIN_SRC
#include <SFML/Graphics.hpp>

int main()
{
  sf::RenderWindow window(sf::VideoMode(200, 200), "Window");
  return 0;
}
#+END_SRC
--------------------------------------------------------------------------------

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*4. Go to the project options:* Right-click on your project in the *Projects* view and select *Build options...*.

#+ATTR_LATEX: :width 0.7\textwidth
[[file:images/reading_u19_ThirdPartyLibraries_cb4_2023.png]]

On the left-hand side, make sure to select the *Release* configuration, not the Debug.

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*5. Link to the include directory:* Under the *Search directories* tab, select the *Compiler* sub-tab.
From here, click the *Add* button and paste in the path to your *SFML* directory's *include* folder, such as:

=C:\LIBRARIES\SFML-2.6.1\include=

#+ATTR_LATEX: :width \textwidth
[[file:images/reading_u19_ThirdPartyLibraries_cb6_2023.png]]


#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*6. Link to the lib directory:* Under the *Search directories* tab, select the *Linker* sub-tab.
From here, click the *Add* button and paste in the path to your *SFML* directory's *lib* folder, such as:

=C:\LIBRARIES\SFML-2.6.1\lib=

#+ATTR_LATEX: :width \textwidth
[[file:images/reading_u19_ThirdPartyLibraries_cb7_2023.png]]

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*7. Set up linked libraries:* Under the *Linker settings* tab...

For *WINDOWS*, under the *Link libraries:* section,
click the *Add* button. You'll do this three times, adding the three libraries: sfml-graphics, sfml-window, and sfml-system.

#+ATTR_LATEX: :width \textwidth
[[file:images/reading_u19_ThirdPartyLibraries_cb8_2023.png]]




#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

For *LINUX*, under the *Other linker options:* section
add these flags: =-lsfml-graphics=, =-lsfml-window=, =-lsfml-system=):

#+ATTR_LATEX: :width \textwidth
[[file:images/reading_u19_ThirdPartyLibraries_cb8linux_2023.png]]


Click *OK* once finished.


#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*8. Build the program:* In the dropdown near the build buttons, make sure to change it from building the "Debug" version
to building the *"Release"* version.

#+ATTR_LATEX: :width 0.4\textwidth
[[file:images/reading_u19_ThirdPartyLibraries_cb9_2023.png]]

Now when you build there shouldn't be any errors:

#+ATTR_LATEX: :width \textwidth
[[file:images/reading_u19_ThirdPartyLibraries_cb10_2023.png]]

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*9. (WINDOWS) Adding the .dll files to the project:*

If we try to run the program right now, it will complain about missing dll files:

#+ATTR_LATEX: :width 0.8\textwidth
[[file:images/reading_u19_ThirdPartyLibraries_cb11_2023.png]]

We need to copy the dll files from the SFML library to our project now.

In your file explorer, navigate one window to your SFML directory and one to your project directory.

In SFML's *bin* directory, highlight the dll files and copy them: sfml-graphics-2.dll, sfml-system-2.dll, and sfml-window-2.dll.

#+ATTR_LATEX: :width \textwidth
[[file:images/reading_u19_ThirdPartyLibraries_cb12_2023.png]]

Paste these files into your project directory:

[[file:images/reading_u19_ThirdPartyLibraries_cb13_2023.png]]

Your program should now start, open, and close without any error messages. Currently the program
doesn't really do anything because there's no game loop within. Reference the
SFML setup page (https://www.sfml-dev.org/tutorials/2.6/start-cb.php) for some test code you can use
to verify that everything works properly.


#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

If you use the example code from the SFML webpage guide, after you build and run you should see
a green circle rendered to our window:

#+ATTR_LATEX: :width 0.4\textwidth
[[file:images/reading_u19_ThirdPartyLibraries_running_2023.png]]

