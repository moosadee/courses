# -*- mode: org -*-

Write down example code for overloading the different types of operators we covered:

1. Arithmetic operators
2. Relational operators
3. Assignment operator
4. Subscript operator
5. Stream operators
