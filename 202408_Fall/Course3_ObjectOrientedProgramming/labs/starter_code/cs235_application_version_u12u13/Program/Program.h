#ifndef _PROGRAM
#define _PROGRAM

#include <vector>
#include <memory>

#include "../Filesystem/File.h"

class Program
{
public:
    Program();
    ~Program();

    void Setup();
    void Teardown();
    void Run();

    void Menu_Main();
    void Menu_CreateDocument();
    void Menu_ViewDocuments();
private:
    void PressEnterToContinue();
    bool IsInvalidIndex( int index );
    int GetValidChoice( int min, int max );

    int screen_width;
    int screen_height;
    string save_path;

    std::vector<unique_ptr<File> > documents;
};

int main();

#endif
