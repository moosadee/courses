#include "File.h"

#include <iostream>
#include <fstream>
using namespace std;

/*
File's PRIVATE MEMBER VARIABLES:
string path;
string name;
string ext;
vector<string> contents;
};
*/

/** DEFAULT CONSTRUCTOR
Initializes member variables with default values (doesn't matter what)
*/
File::File()
{
    // TODO: Implement me!
}

/** PARAMETERIZED CONSTRUCTOR 1
@param      path        The location of the folder to save the data.
@param      name        The name to give the file we're going to write (e.g., "<name>.txt")

This function initializes our output file with the `name` data given.
(Hint: Call the OpenFile function.)
*/
File::File( string path, string name )
{
    // TODO: Implement me!
}

/** PARAMETERIZED CONSTRUCTOR 2
@param      path        The location of the folder to save the data.
@param      name        The name to give the file we're going to write (e.g., "<name>.txt")
@param      ext         The extention signifying the type of file we're writing (e.g., .html, .txt, etc.)

This function initializes our output file with the `name` and `ext` data given.
(Hint: Call the OpenFile function.)
*/
File::File( string path, string name, string ext )
{
    // TODO: Implement me!
}

/** COPY CONSTRUCTOR
@param      other       A second file, which we're going to copy the name, extention, and contents of.
                        Note that "this" File will need " (copy)" appended to the end of its name.

This function initializes our output file using the `name` from the `other` file,
adding " (copy)" to the end of the filename, and the other file's `ext`.
(Hint: We can access the `other` File's data with `other.name` and `other.ext`)
(Hint: Call the OpenFile function.)
Afterwards, copy the `contents` from the `other` File to `this` File.
*/
File::File( const File& other )
{
    // TODO: Implement me!
}

/** OPEN FILE FUNCTION
@param      path        The location of the folder to save the data.
@param      name        The name to give the file we're going to write (e.g., "<name>.txt")
@param      ext         The extention signifying the type of file we're writing (e.g., .html, .txt, etc.)
                        Note: Defaults to "txt".

Within this function set the member variables to the parameter values passed in (e.g., this->path = path).
*/
void File::OpenFile( string path, string name, string ext /* ="txt" */)
{
    // TODO: Implement me!
}


/** WRITE TO FILE FUNCTION
@param      line        The line of text to write out to the output file.

Push the `line` of text given into the `this->contents` structure.
*/
void File::Write( string line )
{
}

/** DISPLAY FILE
Display the file's information, including `this->path`, `this->name`, `this->ext`,
and all of its contents, iterating over `this->contents` to display everything.
*/
void File::Display() const
{
    cout << "FILE: " << this->path << this->name << "." << this->ext << endl;
    for ( auto& line : this->contents )
    {
        cout << line << endl;
    }
}

/**
Return the full filepath. To get this, concatenate the path, name, and extention: PATH NAME . EXT
*/
std::string File::GetFullname() const
{
    return this->path + this->name + "." + this->ext;
}

/**
Return the contents of the `this->contents` structure.
*/
std::vector<std::string> File::GetContents() const
{
    return this->contents;
}

