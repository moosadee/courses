#include "FileTester.h"

#include <iostream>
#include <iomanip>
#include <string>

// !! The tests have already been implemented !!

FileTester::FileTester()
    : RED( "\033[0;31m" ), GRN( "\033[0;32m" ), BOLD( "\033[0;35m" ), CLR( "\033[0m" )
{
    std::cout << std::left;
}

void FileTester::Run()
{
    Test_Constructors();
    Test_OpenFile();
    Test_Write();
}

void FileTester::Test_Constructors()
{
    std::string test_set_name = "** FileTester Test_Constructors";
    std::cout << std::endl << test_set_name << std::string( 80 - test_set_name.size(), '*' ) << std::endl;

    {
        std::string test_name = "Check parameterized ctor with 1 arg";
        std::string input1 = "z_test_FILENAME";
        std::string exp_name = input1;
        std::string exp_ext = "txt";
        std::string exp_path = "../Data/";

        File file( exp_path, input1 );

        bool all_pass = (
                            ( file.name == exp_name ) &&
                            ( file.ext == exp_ext ) &&
                            ( file.path == exp_path )
                        );

        if ( all_pass )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << "* Actual output doesn't match expected!" << std::endl;
            std::cout << "* File file( \"" + input1 + "\" );" << std::endl;
            std::cout << "* Expected file.name:  \"" << exp_name << "\"" << std::endl;
            std::cout << "* Actual   file.name:  \"" << file.name << "\"" << std::endl;
            std::cout << "* Expected file.ext:   \"" << exp_ext << "\"" << std::endl;
            std::cout << "* Actual   file.ext:   \"" << file.ext << "\"" << std::endl;
            std::cout << "* Expected file.path:  \"" << exp_path << "\"" << std::endl;
            std::cout << "* Actual   file.path:  \"" << file.path << "\"" << std::endl;
        }

    }
    std::cout << CLR;

    {
        std::string test_name = "Check parameterized ctor with 1 arg";
        std::string input1 = "z_test_ILEFAY_AMENAY";
        std::string exp_name = input1;
        std::string exp_ext = "txt";
        std::string exp_path = "../Data/";

        File file( exp_path, input1 );

        bool all_pass = (
                            ( file.name == exp_name ) &&
                            ( file.ext == exp_ext ) &&
                            ( file.path == exp_path )
                        );

        if ( all_pass )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << "* Actual output doesn't match expected!" << std::endl;
            std::cout << "* File file( \"" + input1 + "\" );" << std::endl;
            std::cout << "* Expected file.name:  \"" << exp_name << "\"" << std::endl;
            std::cout << "* Actual   file.name:  \"" << file.name << "\"" << std::endl;
            std::cout << "* Expected file.ext:   \"" << exp_ext << "\"" << std::endl;
            std::cout << "* Actual   file.ext:   \"" << file.ext << "\"" << std::endl;
            std::cout << "* Expected file.path:  \"" << exp_path << "\"" << std::endl;
            std::cout << "* Actual   file.path:  \"" << file.path << "\"" << std::endl;
        }

    }
    std::cout << CLR;

    {
        std::string test_name = "Check parameterized ctor with 2 args";
        std::string input1 = "z_test_FILENAME";
        std::string input2 = "html";
        std::string exp_name = input1;
        std::string exp_ext = input2;
        std::string exp_path = "../Data/";

        File file( exp_path, input1, input2 );

        bool all_pass = (
                            ( file.name == exp_name ) &&
                            ( file.ext == exp_ext ) &&
                            ( file.path == exp_path )
                        );

        if ( all_pass )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << "* Actual output doesn't match expected!" << std::endl;
            std::cout << "* File file( \"" + input1 + "\", \"" + input2 + "\" );" << std::endl;
            std::cout << "* Expected file.name:  \"" << exp_name << "\"" << std::endl;
            std::cout << "* Actual   file.name:  \"" << file.name << "\"" << std::endl;
            std::cout << "* Expected file.ext:   \"" << exp_ext << "\"" << std::endl;
            std::cout << "* Actual   file.ext:   \"" << file.ext << "\"" << std::endl;
            std::cout << "* Expected file.path:  \"" << exp_path << "\"" << std::endl;
            std::cout << "* Actual   file.path:  \"" << file.path << "\"" << std::endl;
        }

    }
    std::cout << CLR;

    {
        std::string test_name = "Check parameterized ctor with 2 args";
        std::string input1 = "z_test_VIRUS";
        std::string input2 = "~th";
        std::string exp_name = input1;
        std::string exp_ext = input2;
        std::string exp_path = "../Data/";

        File file( exp_path, input1, input2 );

        bool all_pass = (
                            ( file.name == exp_name ) &&
                            ( file.ext == exp_ext ) &&
                            ( file.path == exp_path )
                        );

        if ( all_pass )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << "* Actual output doesn't match expected!" << std::endl;
            std::cout << "* File file( \"" + input1 + "\", \"" + input2 + "\" );" << std::endl;
            std::cout << "* Expected file.name: \"" << exp_name << "\"" << std::endl;
            std::cout << "* Actual   file.name: \"" << file.name << "\"" << std::endl;
            std::cout << "* Expected file.ext:  \"" << exp_ext << "\"" << std::endl;
            std::cout << "* Actual   file.ext:  \"" << file.ext << "\"" << std::endl;
            std::cout << "* Expected file.path:  \"" << exp_path << "\"" << std::endl;
            std::cout << "* Actual   file.path:  \"" << file.path << "\"" << std::endl;
        }

    }
    std::cout << CLR;


    {
        std::string test_name = "Check copy ctor";
        File file1;
        file1.name = "z_test_cheese";
        file1.ext  = "pizza";
        file1.contents.push_back( "A" );
        file1.contents.push_back( "B" );

        File file2( file1 );

        bool all_pass = (
                            ( file2.name == file1.name + " (copy)" ) &&
                            ( file2.ext == file2.ext ) &&
                            ( file2.contents.size() == file1.contents.size() ) &&
                            ( file2.contents[0] == file1.contents[0] ) &&
                            ( file2.contents[1] == file1.contents[1] )
                        );

        if ( all_pass )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << "* Actual output doesn't match expected!" << std::endl;
            std::cout << "* File file2( file1 );" << std::endl;
            std::cout << "* Expected file.name: \"" << file1.name + " (copy)" << "\"" << std::endl;
            std::cout << "* Actual   file.name: \"" << file2.name << "\"" << std::endl;
            std::cout << "* Expected file.ext:  \"" << file1.ext << "\"" << std::endl;
            std::cout << "* Actual   file.ext:  \"" << file2.ext << "\"" << std::endl;
            std::cout << "* Expected file.contents: ";
            for ( auto& x : file1.contents )
            {
                cout << x << " ";
            }
            cout << endl;
            std::cout << "* Actual   file.contents: ";
            for ( auto& x : file2.contents )
            {
                cout << x << " ";
            }
            cout << endl;
        }

    }
    std::cout << CLR;
}

void FileTester::Test_OpenFile()
{
    std::string test_set_name = "** FileTester Test_OpenFile";
    std::cout << std::endl << test_set_name << std::string( 80 - test_set_name.size(), '*' ) << std::endl;

    {
        std::string test_name = "Test that file is created with correct name/ext (1 arg)";
        std::string input1 = "z_test_testfile";
        std::string exp_name = input1;
        std::string exp_ext = "txt";
        std::string exp_path = "../Data/";

        File file;
        file.OpenFile( exp_path, input1 );

        bool all_pass = (
                            ( file.name == exp_name ) &&
                            ( file.ext  == exp_ext ) &&
                            ( file.path == exp_path )
                        );

        if ( all_pass )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL]" << test_name << std::endl;
            std::cout << "* Expected file.name: \""  << exp_name << "\"" << std::endl;
            std::cout << "* Actual   file.name: \""  << file.name << "\"" << std::endl;
            std::cout << "* Expected file.ext:  \""  << exp_ext  << "\"" << std::endl;
            std::cout << "* Actual   file.ext:  \""  << file.ext << "\"" << std::endl;
            std::cout << "* Expected file.path:  \"" << exp_path << "\"" << std::endl;
            std::cout << "* Actual   file.path:  \"" << file.path << "\"" << std::endl;
        }

    }
    std::cout << CLR;
    std::cout << "(NOTE: Test doesn't check that file created has CORRECT NAME.)" << std::endl;

    {
        std::string test_name = "Test that file is created with correct name/ext (2 args)";
        std::string input1 = "z_test_testtable";
        std::string input2 = "csv";
        std::string exp_name = input1;
        std::string exp_ext = input2;
        std::string exp_path = "../Data/";

        File file;
        file.OpenFile( exp_path, input1, input2 );

        bool all_pass = (
                            ( file.name == exp_name ) &&
                            ( file.ext  == exp_ext ) &&
                            ( file.path == exp_path )
                        );

        if ( all_pass )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL]" << test_name << std::endl;
            std::cout << "* Expected file.name:  \"" << exp_name << "\"" << std::endl;
            std::cout << "* Actual   file.name:  \"" << file.name << "\"" << std::endl;
            std::cout << "* Expected file.ext:   \"" << exp_ext  << "\"" << std::endl;
            std::cout << "* Actual   file.ext:   \"" << file.ext << "\"" << std::endl;
            std::cout << "* Expected file.path:  \"" << exp_path << "\"" << std::endl;
            std::cout << "* Actual   file.path:  \"" << file.path << "\"" << std::endl;
            std::cout << "* Expected file.output.is_open(): true" << std::endl;
        }

    }
    std::cout << CLR;
    std::cout << "(NOTE: Test doesn't check that file created has CORRECT NAME.)" << std::endl;

}

void FileTester::Test_Write()
{
    std::string test_set_name = "** FileTester Test_Write";
    std::cout << std::endl << test_set_name << std::string( 80 - test_set_name.size(), '*' ) << std::endl;


    {
        std::string test_name = "Check that text is written and stored";
        std::string path = "../Data/";
        std::string input1 = "z_test_testfilename";
        std::string input2 = "txt";
        std::string input3 = "line 1 2 3";
        std::string input4 = "another line";

        File file;
        file.OpenFile( path, input1, input2 );
        file.Write( input3 );
        file.Write( input4 );

        bool all_pass = false;

        std::string act_out1 = "", act_out2 = "";
        std::string message = "";

        try
        {
            act_out1 = file.contents.at(0);
            act_out2 = file.contents.at(1);
        }
        catch (...)
        {
        }

        all_pass = (
                       ( act_out1 == input3 ) &&
                       ( act_out2 == input4 )
                   );

        if ( all_pass )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            if ( message != "" )
            {
                std::cout << "* " << message << std::endl;
            }
            std::cout << "* Expected line 1: " << input3 << std::endl;
            std::cout << "* Actual   line 1: " << act_out1 << std::endl;
            std::cout << "* Expected line 2: " << input4 << std::endl;
            std::cout << "* Actual   line 2: " << act_out2 << std::endl;
        }

    }
    std::cout << CLR;

    {
        std::string test_name = "Check that text is written and stored";
        std::string path = "../Data/";
        std::string input1 = "z_test_cheese";
        std::string input2 = "html";
        std::string input3 = "<strong>THIS IS SOME TEXT</strong>";
        std::string input4 = "<em>HELLO</em>";

        File file;
        file.OpenFile( path, input1, input2 );
        file.Write( input3 );
        file.Write( input4 );

        bool all_pass = false;

        std::string act_out1 = "", act_out2 = "";
        std::string message = "";

        try
        {
            act_out1 = file.contents.at(0);
            act_out2 = file.contents.at(1);
        }
        catch (...)
        {
        }

        all_pass = (
                       ( act_out1 == input3 ) &&
                       ( act_out2 == input4 )
                   );

        if ( all_pass )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            if ( message != "" )
            {
                std::cout << "* " << message << std::endl;
            }
            std::cout << "* Expected line 1: " << input3 << std::endl;
            std::cout << "* Actual   line 1: " << act_out1 << std::endl;
            std::cout << "* Expected line 2: " << input4 << std::endl;
            std::cout << "* Actual   line 2: " << act_out2 << std::endl;
        }

    }
    std::cout << CLR;
}
