#include "Product.h"

#include <vector>
using namespace std;

class ProductManager
{
 public:
  static void AddProduct( Product new_product );
  static void AddProduct( string name, float price, int year );
  static void Display();

 private:
  static vector<Product> products;
};
