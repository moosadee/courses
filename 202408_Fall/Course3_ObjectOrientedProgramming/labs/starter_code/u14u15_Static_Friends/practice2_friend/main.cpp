#include <iostream>
#include <iomanip>
#include <vector>
using namespace std;

#include "Product.h"

// TODO: In Product.h, use this function's header to declare it as a friend.
void Display( const vector<Product>& products )
{
  cout << left << fixed << setprecision(2);
  cout << setw( 5 ) << "ID"
       << setw( 20 ) << "NAME"
       << setw( 10 ) << "PRICE"
       << setw( 10 ) << "YEAR"
       << endl;
  cout << string( 80, '-' ) << endl;

  for ( size_t i = 0; i < products.size(); i++ )
  {
    cout << setw( 5 ) << i
         << setw( 20 ) << products[i].name
         << setw( 10 ) << products[i].price
         << setw( 10 ) << products[i].year
         << endl;
  }
}

int main()
{
  cout << endl << "LAUNCH PRICES" << endl << endl;
  vector<Product> products;
  products.push_back( Product( "PlayStation", 299, 1995 ) );
  products.push_back( Product( "PlayStation 2", 299, 2000 ) );
  products.push_back( Product( "PlayStation 3", 499, 2006 ) );
  products.push_back( Product( "PlayStation 4", 399, 2013 ) );
  products.push_back( Product( "PlayStation 5", 499, 2020 ) );

  Display( products );

  cout << endl << endl;
  return 0;
}
