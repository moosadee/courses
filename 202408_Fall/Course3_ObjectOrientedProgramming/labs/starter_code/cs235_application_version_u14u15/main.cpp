#include "Filesystem/FileTester.h"
#include "Accounts/AccountManagerTester.h"
#include "Accounts/AccountTester.h"
#include "Program/Program.h"

// !! Don't update main !!
int main( int argCount, char* args[] )
{
    int action = 2;

    if ( argCount > 1 )
    {
        if ( args[2] == string( "test" ) )    { action = 1; }
        else                                  { action = 2; }
    }

    switch( action )
    {
        case 1:
        {
            FileTester ftester;
            ftester.Run();

            AccountManagerTester amtester;
            amtester.Run();

            AccountTester atester;
            atester.Run();
        }
        break;
        case 2:
        {
            Program program;
            program.Setup();
            program.Run();
            program.Teardown();
        }
        break;
    }

    return 0;
}
