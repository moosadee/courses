#ifndef _ACCOUNT_TESTER
#define _ACCOUNT_TESTER

#include "Account.h"

// !! The tests have already been implemented !!

class AccountTester
{
public:
    AccountTester();
    void Run();
    void Test_Constructors();
    void Test_SetUsername();
    void Test_SetPasswordHash();
    void Test_GetId();
    void Test_GetUsername();
    void Test_CorrectPassword();

private:
    const std::string RED;
    const std::string GRN;
    const std::string BOLD;
    const std::string CLR;

    std::string BoolToStr( bool val )
    {
        return ( val ) ? "true" : "false";
    }
};

#endif
