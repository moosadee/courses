#include "AccountManagerTester.h"
#include "../Exceptions/NotImplementedException.h"

#include <iostream>
#include <iomanip>
#include <string>

AccountManagerTester::AccountManagerTester()
    : RED( "\033[0;31m" ), GRN( "\033[0;32m" ), BOLD( "\033[0;35m" ), CLR( "\033[0m" )
{
    std::cout << std::left;
}

void AccountManagerTester::Run()
{
    Test_CreateAccount();
    Test_GetIndexOfAccount();
    Test_GetAccount();
    Test_DeleteAccount();
}

void AccountManagerTester::Test_CreateAccount()
{
    std::string test_set_name = "** AccountManagerTester Test_CreateAccount";
    std::cout << std::endl << test_set_name << std::string( 80 - test_set_name.size(), '*' ) << std::endl;

    { std::string test_name = "Call CreateAccount, check if implemented";
        bool expected_exception = false;
        bool actual_exception = false;

        try
        {
            AccountManager::CreateAccount( "test", 123 );
        }
        catch( const Exception::NotImplementedException& ex )
        {
            actual_exception = true;
        }

        if ( actual_exception == expected_exception )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << "* Exception::NotImplementedException still being thrown! Make sure to remove!" << std::endl;
            std::cout << std::endl;
            std::cout << CLR;
            return;
        }
    } std::cout << CLR;

    { std::string test_name = "Call CreateAccount, check that account is added";
        AccountManager::Clear();
        AccountManager::CreateAccount( "adiosToreador", 123 );

        size_t expected_size = 1;
        size_t actual_size = AccountManager::accounts.size();

        std::string expected_username = "adiosToreador";
        std::string actual_username = ( AccountManager::accounts.size() > 0 ) ? AccountManager::accounts[0].username : "";

        size_t expected_password_hash = 123;
        size_t actual_password_hash = ( AccountManager::accounts.size() > 0 ) ? AccountManager::accounts[0].password_hash : 0;

        if (
            actual_size == expected_size &&
            actual_username == expected_username &&
            actual_password_hash == expected_password_hash
        )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << std::setw( 40 ) << "VARIABLE"                   << std::setw( 20 ) << "EXPECTED"               << std::setw( 20 ) << "ACTUAL" << std::endl << std::string( 80, '-' ) << std::endl;
            std::cout << std::setw( 40 ) << "accounts.size()"            << std::setw( 20 ) << expected_size            << std::setw( 20 ) << actual_size << std::endl;
            std::cout << std::setw( 40 ) << "accounts[0].username"       << std::setw( 20 ) << expected_username        << std::setw( 20 ) << actual_username << std::endl;
            std::cout << std::setw( 40 ) << "accounts[0].password_hash"  << std::setw( 20 ) << expected_password_hash   << std::setw( 20 ) << actual_password_hash << std::endl;
            std::cout << std::endl;
        }
    } std::cout << CLR;

    { std::string test_name = "Call CreateAccount twice, check that account is added";
        AccountManager::Clear();
        AccountManager::CreateAccount( "twinArmageddons", 222 );
        AccountManager::CreateAccount( "adiosToreador", 123 );

        size_t expected_size = 2;
        size_t actual_size = AccountManager::accounts.size();

        std::string expected_username = "twinArmageddons";
        std::string actual_username = ( AccountManager::accounts.size() > 0 ) ? AccountManager::accounts[0].username : "";

        size_t expected_password_hash = 222;
        size_t actual_password_hash = ( AccountManager::accounts.size() > 0 ) ? AccountManager::accounts[0].password_hash : 0;

        if (
            actual_size == expected_size &&
            actual_username == expected_username &&
            actual_password_hash == expected_password_hash
        )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << std::setw( 40 ) << "VARIABLE"                   << std::setw( 20 ) << "EXPECTED"               << std::setw( 20 ) << "ACTUAL" << std::endl << std::string( 80, '-' ) << std::endl;
            std::cout << std::setw( 40 ) << "accounts.size()"            << std::setw( 20 ) << expected_size            << std::setw( 20 ) << actual_size << std::endl;
            std::cout << std::setw( 40 ) << "accounts[0].username"       << std::setw( 20 ) << expected_username        << std::setw( 20 ) << actual_username << std::endl;
            std::cout << std::setw( 40 ) << "accounts[0].password_hash"  << std::setw( 20 ) << expected_password_hash   << std::setw( 20 ) << actual_password_hash << std::endl;
            std::cout << std::endl;
        }
    } std::cout << CLR;

    std::cout << CLR;
}

void AccountManagerTester::Test_GetIndexOfAccount()
{
    std::string test_set_name = "** AccountManagerTester Test_GetIndexOfAccount";
    std::cout << std::endl << test_set_name << std::string( 80 - test_set_name.size(), '*' ) << std::endl;

    { std::string test_name = "Call GetIndexOfAccount, check if implemented";
        bool expected_exception = false;
        bool actual_exception = false;

        try
        {
            AccountManager::GetIndexOfAccount( 100 );
        }
        catch( const Exception::NotImplementedException& ex )
        {
            actual_exception = true;
        }
        catch( const std::out_of_range& ex )
        {
            // This is fine.
        }

        if ( actual_exception == expected_exception )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << "* Exception::NotImplementedException still being thrown! Make sure to remove!" << std::endl;
            std::cout << std::endl;
            std::cout << CLR;
            return;
        }
    } std::cout << CLR;

    { std::string test_name = "Add account, check getIndexOfAccount returns correct index";
        AccountManager::Clear();
        AccountManager::accounts.push_back( Account( 100, "gardenGnostic", 0 ) );
        AccountManager::accounts.push_back( Account( 200, "gutsyGumshoe", 1 ) );
        AccountManager::accounts.push_back( Account( 300, "arachnidsGrip", 2 ) );

        bool expected_exception = false;
        bool actual_exception = false;

        size_t expected_result = 1;
        size_t actual_result;

        try
        {
            actual_result = AccountManager::GetIndexOfAccount( 200 );
        }
        catch( const std::out_of_range& ex )
        {
            actual_exception = true;
        }

        if (
            actual_result == expected_result &&
            actual_exception == expected_exception
        )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << std::setw( 40 ) << "VARIABLE"                   << std::setw( 20 ) << "EXPECTED"                       << std::setw( 20 ) << "ACTUAL" << std::endl << std::string( 80, '-' ) << std::endl;
            std::cout << std::setw( 40 ) << "exception thrown"           << std::setw( 20 ) << BoolToStr( expected_exception )  << std::setw( 20 ) << BoolToStr( actual_exception ) << std::endl;
            std::cout << std::setw( 40 ) << "returned index"             << std::setw( 20 ) << expected_result                  << std::setw( 20 ) << actual_result << std::endl;
            std::cout << std::endl;
        }
    } std::cout << CLR;

    { std::string test_name = "Add account, check getIndexOfAccount returns correct index";
        AccountManager::Clear();
        AccountManager::accounts.push_back( Account( 100, "gardenGnostic", 0 ) );
        AccountManager::accounts.push_back( Account( 200, "gutsyGumshoe", 1 ) );
        AccountManager::accounts.push_back( Account( 300, "arachnidsGrip", 2 ) );

        bool expected_exception = false;
        bool actual_exception = false;

        size_t expected_result = 2;
        size_t actual_result;

        try
        {
            actual_result = AccountManager::GetIndexOfAccount( 300 );
        }
        catch( const std::out_of_range& ex )
        {
            actual_exception = true;
        }

        if (
            actual_result == expected_result &&
            actual_exception == expected_exception
        )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << std::setw( 40 ) << "VARIABLE"                   << std::setw( 20 ) << "EXPECTED"                       << std::setw( 20 ) << "ACTUAL" << std::endl << std::string( 80, '-' ) << std::endl;
            std::cout << std::setw( 40 ) << "exception thrown"           << std::setw( 20 ) << BoolToStr( expected_exception )  << std::setw( 20 ) << BoolToStr( actual_exception ) << std::endl;
            std::cout << std::setw( 40 ) << "returned index"             << std::setw( 20 ) << expected_result                  << std::setw( 20 ) << actual_result << std::endl;
            std::cout << std::endl;
        }
    } std::cout << CLR;

    std::cout << CLR;
}

void AccountManagerTester::Test_GetAccount()
{
    std::string test_set_name = "** AccountManagerTester Test_GetAccount";
    std::cout << std::endl << test_set_name << std::string( 80 - test_set_name.size(), '*' ) << std::endl;

    { std::string test_name = "PREREQUISITE: Call GetIndexOfAccount, check if implemented";
        bool expected_exception = false;
        bool actual_exception = false;

        try
        {
            AccountManager::GetIndexOfAccount( 100 );
        }
        catch( const Exception::NotImplementedException& ex )
        {
            actual_exception = true;
        }
        catch( const std::out_of_range& ex )
        {
            // This is fine.
        }
        catch( ... )
        {
            // This is fine.
        }

        if ( actual_exception == expected_exception )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << "* Exception::NotImplementedException still being thrown! Make sure to remove!" << std::endl;
            std::cout << std::endl;
            std::cout << CLR;
            return;
        }
    } std::cout << CLR;

    { std::string test_name = "Call GetAccount, check if implemented";
        bool expected_exception = false;
        bool actual_exception = false;

        try
        {
            AccountManager::GetAccount( 100 );
        }
        catch( const Exception::NotImplementedException& ex )
        {
            actual_exception = true;
        }
        catch( const std::out_of_range& ex )
        {
            // This is fine.
        }
        catch( ... )
        {
            // This is fine.
        }

        if ( actual_exception == expected_exception )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << "* Exception::NotImplementedException still being thrown! Make sure to remove!" << std::endl;
            std::cout << std::endl;
            std::cout << CLR;
            return;
        }
    } std::cout << CLR;

    { std::string test_name = "Add account, check GetAccount returns item";
        AccountManager::Clear();
        AccountManager::accounts.push_back( Account( 100, "gardenGnostic", 0 ) );
        AccountManager::accounts.push_back( Account( 200, "gutsyGumshoe", 1 ) );
        AccountManager::accounts.push_back( Account( 300, "arachnidsGrip", 2 ) );

        bool expected_exception = false;
        bool actual_exception = false;

        Account expected_result = Account( 200, "gutsyGumshoe", 1 );
        Account actual_result;

        try
        {
            actual_result = AccountManager::GetAccount( 200 );
        }
        catch( const std::out_of_range& ex )
        {
            actual_exception = true;
        }

        if (
            actual_result.username      == expected_result.username &&
            actual_result.password_hash == expected_result.password_hash &&
            actual_result.id            == expected_result.id
        )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << std::setw( 40 ) << "VARIABLE"                   << std::setw( 20 ) << "EXPECTED"                       << std::setw( 20 ) << "ACTUAL" << std::endl << std::string( 80, '-' ) << std::endl;
            std::cout << std::setw( 40 ) << "exception thrown"           << std::setw( 20 ) << BoolToStr( expected_exception )  << std::setw( 20 ) << BoolToStr( actual_exception ) << std::endl;
            std::cout << std::setw( 40 ) << "returned username"          << std::setw( 20 ) << expected_result.username         << std::setw( 20 ) << actual_result.username << std::endl;
            std::cout << std::setw( 40 ) << "returned password_hash"     << std::setw( 20 ) << expected_result.password_hash    << std::setw( 20 ) << actual_result.password_hash << std::endl;
            std::cout << std::setw( 40 ) << "returned id"                << std::setw( 20 ) << expected_result.id               << std::setw( 20 ) << actual_result.id << std::endl;
            std::cout << std::endl;
        }
    } std::cout << CLR;

    { std::string test_name = "Add account, check GetAccount returns item";
        AccountManager::Clear();
        AccountManager::accounts.push_back( Account( 100, "gardenGnostic", 0 ) );
        AccountManager::accounts.push_back( Account( 200, "gutsyGumshoe", 1 ) );
        AccountManager::accounts.push_back( Account( 300, "arachnidsGrip", 2 ) );

        bool expected_exception = false;
        bool actual_exception = false;

        Account expected_result = Account( 300, "arachnidsGrip", 2 );
        Account actual_result;
        try
        {
            actual_result = AccountManager::GetAccount( 300 );
        }
        catch( const std::out_of_range& ex )
        {
            actual_exception = true;
        }

        if (
            actual_result.username      == expected_result.username &&
            actual_result.password_hash == expected_result.password_hash &&
            actual_result.id            == expected_result.id
        )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << std::setw( 40 ) << "VARIABLE"                   << std::setw( 20 ) << "EXPECTED"                       << std::setw( 20 ) << "ACTUAL" << std::endl << std::string( 80, '-' ) << std::endl;
            std::cout << std::setw( 40 ) << "exception thrown"           << std::setw( 20 ) << BoolToStr( expected_exception )  << std::setw( 20 ) << BoolToStr( actual_exception ) << std::endl;
            std::cout << std::setw( 40 ) << "returned username"          << std::setw( 20 ) << expected_result.username         << std::setw( 20 ) << actual_result.username << std::endl;
            std::cout << std::setw( 40 ) << "returned password_hash"     << std::setw( 20 ) << expected_result.password_hash    << std::setw( 20 ) << actual_result.password_hash << std::endl;
            std::cout << std::setw( 40 ) << "returned id"                << std::setw( 20 ) << expected_result.id               << std::setw( 20 ) << actual_result.id << std::endl;
            std::cout << std::endl;
        }
    } std::cout << CLR;

    std::cout << CLR;
}

void AccountManagerTester::Test_DeleteAccount()
{
    std::string test_set_name = "** AccountManagerTester Test_DeleteAccount";
    std::cout << std::endl << test_set_name << std::string( 80 - test_set_name.size(), '*' ) << std::endl;

    { std::string test_name = "PREREQUISITE: Call GetIndexOfAccount, check if implemented";
        bool expected_exception = false;
        bool actual_exception = false;

        try
        {
            AccountManager::GetIndexOfAccount( 100 );
        }
        catch( const Exception::NotImplementedException& ex )
        {
            actual_exception = true;
        }
        catch( const std::out_of_range& ex )
        {
            // This is fine.
        }
        catch( ... )
        {
            // This is fine.
        }

        if ( actual_exception == expected_exception )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << "* Exception::NotImplementedException still being thrown! Make sure to remove!" << std::endl;
            std::cout << std::endl;
            std::cout << CLR;
            return;
        }
    } std::cout << CLR;

    { std::string test_name = "Call DeleteAccount, check if implemented";
        bool expected_exception = false;
        bool actual_exception = false;

        try
        {
            AccountManager::DeleteAccount( 100 );
        }
        catch( const Exception::NotImplementedException& ex )
        {
            actual_exception = true;
        }
        catch( ... )
        {
        }

        if ( actual_exception == expected_exception )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << "* Exception::NotImplementedException still being thrown! Make sure to remove!" << std::endl;
            std::cout << std::endl;
            std::cout << CLR;
            return;
        }
    } std::cout << CLR;

    { std::string test_name = "Add account, check DeleteAccount removes it";
        AccountManager::Clear();
        AccountManager::accounts.push_back( Account( 100, "gardenGnostic", 0 ) );
        AccountManager::accounts.push_back( Account( 200, "gutsyGumshoe", 1 ) );
        AccountManager::accounts.push_back( Account( 300, "arachnidsGrip", 2 ) );

        bool expected_exception = false;
        bool actual_exception = false;

        bool expected_deleted_found = false;
        bool actual_deleted_found = false;

        try
        {
            AccountManager::DeleteAccount( 200 );
        }
        catch( const std::out_of_range& ex )
        {
            actual_exception = true;
        }

        size_t expected_size = 2;
        size_t actual_size = AccountManager::accounts.size();

        // Look through all the elements
        for ( size_t i = 0; i < AccountManager::accounts.size(); i++ )
        {
            if ( AccountManager::accounts[i].id == 200 )
            {
                actual_deleted_found = true;
            }
        }

        if (
            actual_size == expected_size &&
            actual_deleted_found == expected_deleted_found
        )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << std::setw( 40 ) << "VARIABLE"                   << std::setw( 20 ) << "EXPECTED"                           << std::setw( 20 ) << "ACTUAL" << std::endl << std::string( 80, '-' ) << std::endl;
            std::cout << std::setw( 40 ) << "exception thrown"           << std::setw( 20 ) << BoolToStr( expected_exception )      << std::setw( 20 ) << BoolToStr( actual_exception ) << std::endl;
            std::cout << std::setw( 40 ) << "accounts.size()"            << std::setw( 20 ) << expected_size                        << std::setw( 20 ) << actual_size << std::endl;
            std::cout << std::setw( 40 ) << "Found ID in accounts?"      << std::setw( 20 ) << BoolToStr( expected_deleted_found )  << std::setw( 20 ) << BoolToStr( actual_deleted_found ) << std::endl;
            std::cout << "All accounts:" << std::endl;
            for ( size_t i = 0; i < AccountManager::accounts.size(); i++ )
            {
                std::cout << "INDEX: " << i << ", ID: " << AccountManager::accounts[i].id << ", USERNAME: " << AccountManager::accounts[i].username << std::endl;
            }

            std::cout << std::endl;
        }
    } std::cout << CLR;

    { std::string test_name = "Add account, check DeleteAccount removes it";
        AccountManager::Clear();
        AccountManager::accounts.push_back( Account( 100, "gardenGnostic", 0 ) );
        AccountManager::accounts.push_back( Account( 200, "gutsyGumshoe", 1 ) );
        AccountManager::accounts.push_back( Account( 300, "arachnidsGrip", 2 ) );

        bool expected_exception = false;
        bool actual_exception = false;

        bool expected_deleted_found = false;
        bool actual_deleted_found = false;

        try
        {
            AccountManager::DeleteAccount( 100 );
        }
        catch( const std::out_of_range& ex )
        {
            actual_exception = true;
        }

        size_t expected_size = 2;
        size_t actual_size = AccountManager::accounts.size();

        // Look through all the elements
        for ( size_t i = 0; i < AccountManager::accounts.size(); i++ )
        {
            if ( AccountManager::accounts[i].id == 100 )
            {
                actual_deleted_found = true;
            }
        }

        if (
            actual_size == expected_size &&
            actual_deleted_found == expected_deleted_found
        )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << std::setw( 40 ) << "VARIABLE"                   << std::setw( 20 ) << "EXPECTED"                           << std::setw( 20 ) << "ACTUAL" << std::endl << std::string( 80, '-' ) << std::endl;
            std::cout << std::setw( 40 ) << "exception thrown"           << std::setw( 20 ) << BoolToStr( expected_exception )      << std::setw( 20 ) << BoolToStr( actual_exception ) << std::endl;
            std::cout << std::setw( 40 ) << "accounts.size()"            << std::setw( 20 ) << expected_size                        << std::setw( 20 ) << actual_size << std::endl;
            std::cout << std::setw( 40 ) << "Found ID in accounts?"      << std::setw( 20 ) << BoolToStr( expected_deleted_found )  << std::setw( 20 ) << BoolToStr( actual_deleted_found ) << std::endl;
            std::cout << "All accounts:" << std::endl;
            for ( size_t i = 0; i < AccountManager::accounts.size(); i++ )
            {
                std::cout << "INDEX: " << i << ", ID: " << AccountManager::accounts[i].id << ", USERNAME: " << AccountManager::accounts[i].username << std::endl;
            }

            std::cout << std::endl;
        }
    } std::cout << CLR;

    std::cout << CLR;
}

