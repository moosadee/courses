#include "Account.h"

Account::Account( size_t id, std::string username /*= ""*/, size_t password_hash /*= 0*/ )
{
    this->id = id;
    this->username = username;
    this->password_hash = password_hash;
}

void Account::SetUsername( std::string username )
{
    this->username = username;
}

void Account::SetPasswordHash( size_t password_hash )
{
    this->password_hash = password_hash;
}

size_t Account::GetId() const
{
    return this->id;
}

std::string Account::GetUsername() const
{
    return this->username;
}

bool Account::CorrectPassword( size_t attempt_hash )
{
    return ( attempt_hash == this->password_hash );
}
