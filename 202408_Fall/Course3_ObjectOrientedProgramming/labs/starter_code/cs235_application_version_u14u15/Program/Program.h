#ifndef _PROGRAM
#define _PROGRAM

#include <vector>
#include <memory>

#include "../Filesystem/File.h"
#include "../Accounts/AccountManager.h"

class Program
{
public:
    Program();
    ~Program();

    void Setup();
    void Teardown();
    void Run();

    void Menu_Main();
    void Menu_CreateDocument();
    void Menu_ViewDocuments();

    void Menu_CreateAccount();
    void Menu_UpdateAccount();
    void Menu_DeleteAccount();
    void Menu_ViewAccounts();

private:
    void PressEnterToContinue();
    bool IsInvalidIndex( int index );
    int GetValidChoice( int min, int max );
    size_t HashPassword( std::string plaintext_password );
    void TextHeader( std::string text );

    const size_t DEFAULT_ACCOUNT_ID;
    size_t current_account_id;
    int screen_width;
    int screen_height;
    string save_path;

    std::vector<unique_ptr<File> > documents;
};

#endif
