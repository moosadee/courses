#include "Program.h"
#include "../Namespace_Utilities/ScreenDrawer.hpp"
#include "../Filesystem/FileTester.h"
#include "../Accounts/AccountManagerTester.h"
#include "../Accounts/AccountTester.h"
#include "../Exceptions/NotImplementedException.h"

#include <iostream>
#include <functional>

// ** See the comments marked "TODO" for places to update in the code! **

Program::Program()
    : DEFAULT_ACCOUNT_ID( 1000 )
{
    Setup();
    AccountManager::Setup( "../Data/accounts.txt" );
}

Program::~Program()
{
    Teardown();
    AccountManager::Teardown();
}

void Program::Setup()
{
    this->screen_width = 80;
    this->screen_height = 20;
    this->save_path = "../Data/";
    this->current_account_id = DEFAULT_ACCOUNT_ID;
    Utilities::ScreenDrawer::Setup( this->screen_width, this->screen_height );
}

void Program::Teardown()
{
    Utilities::ScreenDrawer::Teardown();
}

void Program::Run()
{
    this->Menu_Main();
}

void Program::Menu_Main()
{
    bool done = false;
    while ( !done )
    {
        Utilities::ScreenDrawer::DrawBackground();
        Utilities::ScreenDrawer::DrawWindow( "DOCUMATE", 2, 2, 76, 20-2 );
        Utilities::ScreenDrawer::Set( 50, 3, "DOC PATH: " + this->save_path, "yellow", "blue" );
        try
        {
            Utilities::ScreenDrawer::Set( 20, 3, "User [" + AccountManager::GetAccount( current_account_id ).GetUsername() + "] logged in", "green", "blue" );
        }
        catch ( ... )
        {
            Utilities::ScreenDrawer::Set( 20, 3, "AccountManager error", "red", "blue" );
        }

        Utilities::ScreenDrawer::Set( 6, 5,  "Documents", "white", "black" );
        Utilities::ScreenDrawer::Set( 7, 6,  "[11] Create document", "white", "black" );
        Utilities::ScreenDrawer::Set( 7, 7,  "[12] View documents", "white", "black" );

        Utilities::ScreenDrawer::Set( 6, 9,  "Accounts", "white", "black" );
        Utilities::ScreenDrawer::Set( 7, 10,  "[21] Create account", "white", "black" );
        Utilities::ScreenDrawer::Set( 7, 11,  "[22] Update accounts", "white", "black" );
        Utilities::ScreenDrawer::Set( 7, 12,  "[23] Delete accounts", "white", "black" );
        Utilities::ScreenDrawer::Set( 7, 13,  "[24] View accounts", "white", "black" );

        Utilities::ScreenDrawer::Set( 40, 5, "Tests", "white", "black" );
        Utilities::ScreenDrawer::Set( 41, 6,  "[91] File tester", "white", "black" );
        Utilities::ScreenDrawer::Set( 41, 7,  "[92] Account tester", "white", "black" );
        Utilities::ScreenDrawer::Set( 41, 8,  "[93] AccountManager tester", "white", "black" );

        Utilities::ScreenDrawer::Set( 6, 20-4, "[0] Exit", "white", "black" );

        Utilities::ScreenDrawer::Draw();

        int choice;
        std::cout << ">> ";
        std::cin >> choice;
        cin.ignore();

        switch( choice )
        {
            case 0: done = true; break;

            case 11: this->Menu_CreateDocument(); break;
            case 12: this->Menu_ViewDocuments();  break;
            case 21: this->Menu_CreateAccount();  break;
            case 22: this->Menu_UpdateAccount();  break;
            case 23: this->Menu_DeleteAccount();  break;
            case 24: this->Menu_ViewAccounts();   break;

        case 91:
        {
            FileTester tester;
            tester.Run();
            PressEnterToContinue();
        }
        break;
        case 92:
        {
            AccountTester tester;
            tester.Run();
            PressEnterToContinue();
        }
        break;
        case 93:
        {
            AccountManagerTester tester;
            tester.Run();
            PressEnterToContinue();
        }
        break;
        break;
        }
    }
}

void Program::PressEnterToContinue()
{
    string hit_enter;
    cout << endl << "HIT ENTER TO CONTINUE" << endl;
    getline( cin, hit_enter );
    getline( cin, hit_enter );
}

void Program::Menu_CreateDocument()
{
    Utilities::ScreenDrawer::DrawBackground();
    Utilities::ScreenDrawer::DrawWindow( "FILETYPE", 2, 4, 76, 24-6 );

    std::vector<std::string> filetype_exts = {
        "txt",
        "html",
        "csv"
    };
    std::vector<std::string> filetype_names = {
        " (Plaintext)",
        " (Webpage)",
        " (Spreadsheet)"
    };

    auto new_file = unique_ptr<File>(new File);

    for ( size_t i = 0; i < filetype_exts.size(); i++ )
    {
        Utilities::ScreenDrawer::Set( 6, 7+i, "[" + to_string( i+1 ) + "] " + filetype_exts[i] + filetype_names[i],  "white", "black" );
    }
    Utilities::ScreenDrawer::Set( 6, 24-8, "[0] Go back", "white", "black" );

    Utilities::ScreenDrawer::Draw();

    int choice = GetValidChoice(0, filetype_exts.size());

    if ( choice == 0 ) { return; }

    std::cout << "Enter filename: ";
    std::string name;
    std::cin.ignore();
    getline( std::cin, name );

    new_file->OpenFile( this->save_path, name, filetype_exts[choice-1] );

    std::cout << std::endl;
    std::cout << "EDITING " << new_file->GetFullname() << "..." << std::endl;
    std::cout << std::endl << "ENTER LINES OF TEXT. TO QUIT AND SAVE, ENTER :q" << std::endl << std::endl;
    int line_count = 0;
    while ( true )
    {
        std::cout << line_count++ << " >> ";
        std::string line;
        getline( std::cin, line );

        if ( line == ":q" ) { break; }

        new_file->Write( line );
    }

    std::cout << std::endl << new_file->GetFullname() << " created." << std::endl;
    this->documents.push_back( move(new_file) );

    PressEnterToContinue();
}

void Program::Menu_ViewDocuments()
{
    Utilities::ScreenDrawer::DrawBackground();
    Utilities::ScreenDrawer::DrawWindow( "VIEW DOCUMENT", 2, 4, 76, 24-6 );

    Utilities::ScreenDrawer::Set( 6, 7,  "Available documents", "white", "black" );
    for ( size_t i = 0; i < this->documents.size(); i++ )
    {
        Utilities::ScreenDrawer::Set( 7, 8+i,  "[" + to_string( i+1 ) + "] " + this->documents[i]->GetFullname(), "white", "black" );
    }
    Utilities::ScreenDrawer::Set( 6, 24-8, "[0] Go back", "white", "black" );

    Utilities::ScreenDrawer::Draw();

    int choice = GetValidChoice( 0, this->documents.size() );

    if ( choice == 0 ) { return; }

    if ( IsInvalidIndex( choice ) )
    {
        std::cout << "Invalid index!" << std::endl;
        PressEnterToContinue();
        return;
    }

    cin.ignore();

    int index = choice-1;

    std::cout << std::string( 80, '-' ) << std::endl;
    this->documents[index]->Display();
    std::cout << std::string( 80, '-' ) << std::endl;

    PressEnterToContinue();
}

void Program::Menu_CreateAccount()
{
    TextHeader( "Create account" );

    std::string username, password;
    std::cout << "Enter username: ";
    std::cin >> username;
    std::cout << "Enter password: ";
    std::cin >> password;

    size_t hashed_password = HashPassword( password );


    throw Exception::NotImplementedException( "Program::Menu_CreateAccount" ); // TODO: REMOVE ME!!

    // TODO: Call the AccountManager's CreateAccount function.


    AccountManager::SaveData();
    PressEnterToContinue();
}

void Program::Menu_UpdateAccount()
{
    TextHeader( "Update account" );

    AccountManager::DisplayAccountsTable();

    size_t id;
    std::cout << std::endl << "Update which account? #: ";
    std::cin >> id;

    throw Exception::NotImplementedException( "Program::Menu_UpdateAccount" ); // TODO: REMOVE ME!!

    // TODO: Implement the rest of the function.




    AccountManager::SaveData();
    PressEnterToContinue();
}

void Program::Menu_DeleteAccount()
{
    TextHeader( "Delete account" );

    AccountManager::DisplayAccountsTable();

    size_t id;
    std::cout << std::endl << "Delete which account? #: ";
    std::cin >> id;

    if ( id == DEFAULT_ACCOUNT_ID )
    {
        std::cout << std::endl << "!! Can't delete the default account !!" << std::endl;
        PressEnterToContinue();
        return;
    }



    throw Exception::NotImplementedException( "Program::Menu_DeleteAccount" ); // TODO: REMOVE ME!!

    // TODO: Implement the rest of the function


    AccountManager::SaveData();
    PressEnterToContinue();
}

void Program::Menu_ViewAccounts()
{
    Utilities::ScreenDrawer::DrawBackground();
    Utilities::ScreenDrawer::DrawWindow( "VIEW ACCOUNTS", 2, 4, 76, 24-6 );

    AccountManager::DisplayAccountsTable();

    PressEnterToContinue();
}

bool Program::IsInvalidIndex( int index )
{
    return ( index < 1 || index > this->documents.size() );
}

int Program::GetValidChoice( int min, int max )
{
    int choice;
    cout << ">> ";
    cin >> choice;

    while (choice < min || choice > max)
    {
        cout << "Must be between " << min << " and " << max << "! Try again!" << endl;
        cout << ">> ";
        cin >> choice;
    }

    return choice;
}

size_t Program::HashPassword( std::string plaintext_password )
{
    std::hash<std::string> hasher;
    return hasher( plaintext_password );
}

void Program::TextHeader( std::string text )
{
    text = "- " + text + " -";
    std::cout << text << std::string( 80-text.size(), '-' ) << std::endl << std::endl;
}

