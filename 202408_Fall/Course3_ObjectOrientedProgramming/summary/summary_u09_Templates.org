# -*- mode: org -*-

#+TITLE: Q&A Notes: Templates
#+AUTHOR: Rachel Wil Sha Singh

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/rworgmode.css" />
#+HTML_HEAD: <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script> <script>hljs.initHighlightingOnLoad();</script>

-----

1. What does utilizing templated parameters (“type T”) help us do?
1. Templated function declarations and definitions should go in WHAT type of C++ source file?
1. How do you declare and define a templated function? (Give example code)
1. How do you create a class that contains a templated member variable? (Give example code)
1. How do you create a function that uses two different template type placeholders? (Give example code)
