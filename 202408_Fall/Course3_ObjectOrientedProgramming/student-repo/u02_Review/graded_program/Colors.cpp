//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! DON'T MODIFY THIS FILE !!
//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!
#include "Colors.h"

#include <iostream>
using namespace std;

/* COLOR CODES:

| code value | execute            |
|------------|--------------------|
| ' '        | `cout << CLEAR;`   |
| 'K'        | `cout << BLACK;`   |
| 'R'        | `cout << RED;`     |
| 'G'        | `cout << GREEN;`   |
| 'Y'        | `cout << YELLOW;`  |
| 'B'        | `cout << BLUE;`    |
| 'M'        | `cout << MAGENTA;` |
| 'C'        | `cout << CYAN;`    |
| 'W'        | `cout << WHITE;`   |
*/

void DrawPixel( char code )
{
    // TODO: Based on the `code` passed in, output the corresponding COLOR code.



    cout << " ";
}
