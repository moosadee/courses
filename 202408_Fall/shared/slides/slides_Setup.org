# -*- mode: org -*-

First we're going to start with setting up accounts and software you need.
You should only need to do this once at the start of the semester unless you
want to install these tools on multiple computers.

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

* Create a GitLab account:

- *Create a GitLab account:* We're going to be using GitLab for code storage this semester. Go to the sign up page -
  https://gitlab.com/users/sign_up - and create an account. (This should all be free,
  even though GitLab provides professional level accounts, too!)

- *Find your profile:*
  - Once you've created your account, I need to know your account name so I can give you access to your code *repository*.
  - Your profile page should be something like =https://gitlab.com/rsingh13= (rsingh13 is my secondary account).
    You can also get to your profile by clicking on the circle picture for your account and selecting
    your username.

    #+ATTR_LATEX: :width 0.5\textwidth
    [[file:images/cs200-setup_gitlabprofile.png]]

- *Tell me your profile URL:*
  - Find the assignment on Canvas: *❗Setup: What is your GitLab username?*

    #+ATTR_LATEX: :width 0.5\textwidth
    [[file:images/cs200-setup_gitlabusernamesubmit.png]]



#+BEGIN_LATEX
\newpage
#+END_LATEX

* Installing the Git software:

- *Download the Git installer:*
  - WINDOWS: Go to https://git-scm.com/ and download and run the installer.
  - MAC: You will need to install the *xcode* software, which Git comes bundled with.
  - LINUX: Install via your favorite package manager. :)


While going through the setup if you see this screen (*"Choosing the default editor used by Git"*)
I would suggest setting it to *Notepad* - the default is Vim, and if you don't know Vim, I'm not
going to teach you! (You have to quit using =:q= :)

#+ATTR_LATEX: :width 0.7\textwidth
[[file:images/cs200-welcome_setup-git-install2.png]]





#+BEGIN_LATEX
\newpage
#+END_LATEX

* Configuring Git:
We need to configure Git for the first time and pull down the starter code for this course.
You should only need to do this part ONCE, unless you're changing computers.

- Go to *"View $\to$ Terminal"*. This will bring up a textbox at the bottom of the screen.
- You'll enter the following three commands to configure Git:
  [[file:images/cs200-welcome_setup-git-config.png]]
  - Set your email address - change "you@example.com" to your email address.

    =git config --global user.email "you@example.com"=

  - Set what your name shows up as - change "Your Name" to your name (keep the double quotes).

    =git config --global user.name "Your Name"=

  - Set the sync strategy (this just makes it easier):

   =git config --global pull.rebase false=

#+BEGIN_LATEX
\newpage
#+END_LATEX

* Installing the IDE:


#+BEGIN_LATEX
\newpage
#+END_LATEX


* Cloning your class repository:

The REPOSITORY is where your code will be stored. You and the instructor can both edit
the same code files and Git will merge them together. This can be handy if you're having
issues with an assignment, or the instructor may add comments during grading to help you
with correcting issues afterwards.

- You will find the URL to your REPOSITORY in the comments for the *"❗Setup: What is your GitLab username?"* assignment.
  Open up that URL - I would suggest *bookmarking it* since we'll be accessing it throughout the semester.

  #+ATTR_LATEX: :width 0.8\textwidth
  [[file:images/cs200-unit00-welcome_setup_gitlabnavB.png]]

- From your REPOSITORY page, click on the blue *"Code"* button and copy the "*Clone with HTTPS*" URL:

  #+ATTR_LATEX: :width 0.4\textwidth
  [[file:images/cs200-welcome_setup-giturl.png]]

- BACK IN VS CODE the front page should have an option to *"Clone Git Repository..."*. Click on this:

  #+ATTR_LATEX: :width 0.4\textwidth
  [[file:images/cs200-welcome_setup-vscodeclone.png]]

- A textbox will show up, paste your REPOSITORY URL into this space and hit ENTER:

  #+ATTR_LATEX: :width 0.8\textwidth
  [[file:images/cs200-welcome_setup-vscodecloneurl.png]]

- A FOLDER SELECT WINDOW will pop up. Select a location where you want to store your
  course files for the semester, such as under your *Documents*.

- VS Code will automatically download all of the files. After it's done the REPOSITORY
  and all the files will be on your computer. VS Code will help you sync code between
  your computer and the server (on GitLab).

- After cloning is done it might ask *"Would you like to open the cloned repository, or add it to the current workspace?"*.
  Go ahead and click *"Open"*:

  #+ATTR_LATEX: :width 1.0\textwidth
  [[file:images/cs200-welcome_setup-vscodeclonedone.png]]

- On the left-hand side of your screen you will see the REPOSITORY and all its internal folders and files.
  You can double-click on a FOLDER to open it up and look at code within.
  Go ahead and open the =u00_Setup= folder and open the =test.cpp= file by double-clicking on it.

  [[file:images/cs200-welcome_setup-vscodefolders.png]]

- VS Code might pop up a little message in the lower-left asking if you want to install
  *"C++ Extensions"* - go ahead and do this as well.



#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

THAT'S ALL THE SETUP! You won't have to do all of that again unless you change computers
during this course.


#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

The rest of the steps here will be more like what you do during
weekly labs.

#+BEGIN_LATEX
\newpage
#+END_LATEX

* Working on the Unit 00 Setup project:

- *OPEN PROJECT:* To begin working on the Unit 00 project, go to *"File $\to$ Open Folder..."* in VS Code.
  Select the folder =u00_Setup=.

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

  - VS Code might ask *"Would you like to open the repository?"* Click on *"Yes"* or *"Always"*.

    #+ATTR_LATEX: :width 0.7\textwidth
    [[file:images/cs200-welcome_setup-gitrepoparent.png]]

- *OPEN SOURCE FILE:* Double click on the =test.cpp= file to open it up. It has the following starter code:

#+BEGIN_HTML
\begin{lstlisting}[style=cpp]
#include <iostream>   // IMPORT INPUT AND OUTPUT
using namespace std;  // USING THE C++ STANDARD LIBRARY

int main() // PROGRAM STARTING POINT
{ // START OF CODE BLOCK

    // The following writes text out to the screen
    // when the program is run:

    cout << "Welcome to C++!" << endl;
    cout << "My name is... SOMEBODY" << endl;

    return 0; // PROGRAM ENDS
} // END OF CODE BLOCK
\end{lstlisting}
#+END_HTML


#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

- Change the test =SOMEBODY= to your own name.

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

- *BUILD PROGRAM:* Each time you make a change to a C++ source code file you will need
  to BUILD (aka COMPILE) your C++ source code into a runnable file. The COMPILER
  program (g++) does this for us.
  - In VS Code, go to *"View $\to$ Terminal"*.
  - Within the terminal, use the following command:

    =g++ test.cpp=

    This runs the *g++* program and tells it to build the *test.cpp* source code file.

  - If there are *no build errors* it will NOT SAY ANYTHING.

  - Use the =ls= (lower case L and S) command next, and it will show you what files are
    in your current directory. It will now have =test.cpp= /and/ a new file: =a.out= (Mac/Linux)
    or =a.exe= (Windows). This is the /runnable program/ created from your source code file.


#+BEGIN_HTML
\begin{lstlisting}[style=terminal]
$ ls
a.exe        test.cpp
\end{lstlisting}
#+END_HTML

- *RUN PROGRAM:* To run the program, use the command:

  =./a.exe= (Windows)

  or

  =./a.out= (Mac/Linux)

  You should see the greetings message with your name in it:



#+BEGIN_HTML
\begin{lstlisting}[style=terminal]
Welcome to C++!
My name is... R.W.
\end{lstlisting}
#+END_HTML


#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

If all of this goes well it means that you've set up VS Code and the g++ compiler correctly! Yay!

#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

* Sync code to GitLab server:

Second to last, we're going to SYNC OUR CODE to the GitLab server. Currently, the code
is only on your computer but not on the GitLab webpage.

- In VS Code, click on the source control button. This looks like a node that branches out into two:

  #+ATTR_LATEX: :width 0.4\textwidth
  [[file:images/cs200-welcome_setup-testprogramrepo.png]]

- It will show your changed files: test.cpp and a.exe (or a.out).

- Add a message in the *"Message"* box. The message should describe what you did,
  so that if you need to look at your change history later you can easily find where you
  changed something. I'd suggest adding a message like

  "Finished u00 setup."

- After typing in the message, click the *"Commit"* button. This SAVES your current changes.

- A dialog box /might/ pop up asking about staged commits. Click on *"Yes"* or *"Always"*:

  #+ATTR_LATEX: :width 0.8\textwidth
  [[file:images/cs200-welcome_setup-testprogramrepo2.png]]

- Next, the commit button turns into *"Sync Changes"*. This will actually send your code
  to the server and keep everything synced. Click on it.

  #+ATTR_LATEX: :width 0.4\textwidth
  [[file:images/cs200-welcome_setup-testprogramrepo3.png]]

- A box may pop up saying "This action will pull and push commits from and to "origin/main"."
  Click *"OK"* or *"OK, Don't Show Again"*.

- You might also see a popup in the lower-right saying,
  "Would you like Visual Studio Code to periodically run "git fetch"?"
  For this class, you can click *"Yes"*.

  #+ATTR_LATEX: :width 0.6\textwidth
  [[file:images/cs200-welcome_setup-gitfetch.png]]


#+BEGIN_LATEX
\newpage
#+END_LATEX

* Viewing changes on GitLab:

Back on your REPOSITORY WEBPAGE, you will see your commit message.
You can also go into your =u00_Setup= folder and click on the =test.cpp= file
to make sure your changes were saved properly.

[[file:images/cs200-welcome_setup-repoupdate.png]]

#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

* Turning in the assignment:

- From the REPOSITORY WEBPAGE, go to your =u00_Setup= folder and copy the URL from here:

  #+ATTR_LATEX: :width 0.6\textwidth
  [[file:images/cs200-welcome_setup-copyurl.png]]

- Back on the CANVAS WEBPAGE, go to the course assignment (*"Lab: Setting up your system & tools (U00.LAB.CS200.202406)"*).
  Click on *"Start Assignment"* to begin.

- Paste in the URL to your GitLab repository in this box here, then click *"Submit Assignment"*.

  #+ATTR_LATEX: :width 0.6\textwidth
  [[file:images/cs200-welcome_setup-canvassubmit.png]]



#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

NOW YOU'RE ALL DONE WITH SETUP!!

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

*Unit 01: Exploring computers* is more reading and concepts, then you will do more coding in
the *Unit 02: Variables, input, and output* unit. I will repeat the open project / steps to
build and run the program / syncing to the server / turning in on Canvas steps there.
