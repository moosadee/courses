# -*- mode: org -*-

JCCC requires us to take attendance during the first week of the semester. Students are required to attend class (if there is a scheduled class session) this first week. If there are scheduling conflicts during the first week of class, please reach out to the instructor to let them know.
JCCC auto-drops students marked as not in attendance during the first week of class, but students can be reinstated.
See https://www.jccc.edu/admissions/enrollment/reinstatement.html for more details.


#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX


*HyFlex classes:* The following three scenarios count as student attendance for my classes:
1. Attending class in person during the class times, or
2. Attending class remotely via Zoom during class times, or
3. Watching the recorded Zoom class afterwards


#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX


*Online classes:* Attendance is counted as completion of assignments for a given week.



#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX


*Attendance during the first week of class*

JCCC policy is for teachers to take attendance /at least/ the first week
of class of the semester. This attendance is posted, and if you
*are not counted as in-attendance* then you will be auto-dropped from
the course, though you can petition to be reinstated. See
the "Dropping & Reinstatement" page for more information:

https://www.jccc.edu/admissions/enrollment/reinstatement.html

*First week attendance* is counted by completion of the
*"Ethics and Academic Honesty"* assignment (on Canvas).
This assignment is required, but I can also count your
attendance as you showing up for the first day of class
/in person or remotely via Zoom/ for HyFlex classes.
(Online-only courses, you will need to do the Ethics and Academic Honesty assignment.)
