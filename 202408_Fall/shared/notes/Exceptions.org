# -*- mode: org -*-

1. When should throw be used?
1. When should try be used?
1. When should catch be used?
1. Write down example code of a function with a throw, then a call to that function with a wrapping try/catch.
1. Documentation URLs for the exception family: https://cplusplus.com/reference/stdexcept/ 
1. Write down example code for creating a class that inherits from an exception to create a custom exception type.
