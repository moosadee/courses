# -*- mode: org -*-


#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

- *Office hours* ::

My office hours for *Fall 2024* are:

- Wednesdays, 10:00 am - 12:30 pm via Zoom
- Wednesdays, 4:30 pm - 7:00 pm via Zoom

I am also on campus on Monday, Tuesday, and Thursday and we can schedule a one-on-one meeting time,
just email me via Canvas to get something scheduled.

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

- *Class communication* ::

- *Please use Canvas email, not the @jccc email, to contact the instructor!*
  I rely upon the Canvas email system for communication with students
  and only students. The direct @jccc email address also receives non-student
  communication such as automated campus updates, department emails, commitee emails,
  and so on. To ensure that I see your email in a timely manner, please email me via Canvas!
- *Instructor will respond within 1 business day*
  I do my best to respond to emails as quickly as possible, at most within 1 business day.
  Emails on Friday may not get a response until Monday. If I have not responded to your email
  within this time period it is possible I have not seen your email, please make sure to
  email me /through Canvas/ and not to my @jccc email.
- *Check course Announcements!*
  I use the Announcements board as a way to give additional course information, assignment
  corrections, and other things. You can set up Canvas to email these announcements to you
  automatically. Please don't ignore class Announcements!

