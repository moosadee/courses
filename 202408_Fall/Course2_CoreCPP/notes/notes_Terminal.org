# -*- mode: org -*-

* *Git*

- What is a commit message for?

#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

- The command to send your changes to the server is:

#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

* *Terminal*

- The command to list all files and folders in a directory is:

#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

- The command to navigate to a new folder is:

#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

- The command to navigate UP out of a folder is:

#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

- The command to build a single source file is:

#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

- The command to build multiple source files is:

#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

- The command to run a program is:

#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

