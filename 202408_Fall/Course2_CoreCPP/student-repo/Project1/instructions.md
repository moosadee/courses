# Project 1, CS 200 Summer 2024

## About
Choose **one program** to implement out of the list of the program ideas below. You will implement the program from scratch. Please save your project code in the **Project1** folder in your repository.

For this project you will need to use variables, cin, and cout.

## Starter code
Start by creating a new file in your Project1 folder. It could be named something like `main.cpp`.

The starting code for a C++ program is:

```
#include <iostream>
#include <string>
using namespace std;

int main()
{
    // Put your code here

    return 0;
}
```

## Options

1. **Calculator program**: Ask the user to enter two numbers. Afterwards, show the *sum*, *difference*, *product*, and *quotient* of those two variables.

2. **Recipe program**: Find a recipe with only a few ingredients. Write a program that displays the recipe, but first it should ask the user how many batches to make. Based on the batches, adjust the ingredient amounts (store the amounts in variables) and display the adjusted amounts to the user.

3. **House sqft**: Assume a house has a living room, kitchen, bedroom, and bathroom. Ask the user to enter the dimensions (width x length) of each room. Calculate and display the sqft of each room, and at the end display the total sqft of the house.


## Code requirements
- **Do not use future topics for this assignment; I am trying to verify your knowledge of the current topics.**
- Utilize variables, do basic math operation(s).
- Utilize `cin` and `cout` for input and output.


## Turn in
- Upload your project to your repository under the "Project1" folder.
- Upload a screenshot of your program running on Canvas.
