# Building and running the program:
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

From the terminal, use the g++ compiler to build:
`g++ *.cpp`

And then run it:
`./a.exe` (Windows)
`./a.out` (Linux/Mac)

# Example program output:
  BREAD RECIPE

  Ingredients
  * 4.5 Cups of Flour
  * 1 Tablespoons of Sugar
  * 2.25 Teaspoons of Instant Yeast
  * 1.66 Cups of Water
  * 2.5 Teaspoons of Table Salt

# Ingredient struct:
Above `main()`, declare a new struct called `Ingredient`. It should contain the following member variables:
- name, a string
- unit, a string
- amount, a float

# main:
Within `main()` I've written some code storing five ingredients' information in 3 separate variables per ingredient (so 15 total variables)!

Update the program to replace the three variables per ingredient...:
```
  string ingredient1_name{"Flour"};
  string ingredient1_unit{"Cups"};
  float  ingredient1_amount{4.5};
```

Into one struct per ingredient...:
```
  Ingredient ingredient1;
  ingredient1.name = "Flour";
  ingredient1.unit = "Cups";
  ingredient1.amount = 4.5;
```

You will also have to udpate the `cout` statements and the end of `main()`, which is displaying each ingredient's information. (e.g., `ingredient1_amount` becomes `ingredient1.amount`.)

