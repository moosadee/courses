# Building and running the program:
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

From the terminal, use the g++ compiler to build:
`g++ *.cpp`

And then run it:
`./a.exe` (Windows)
`./a.out` (Linux/Mac)


# Example program output:
  PET INFORMATION

  ---- PET 1 ----
  Name: Luna
  Age:  8
  Animal: Cat
  Breed: Black

  ---- PET 2 ----
  Name: Kabe
  Age:  10
  Animal: Cat
  Breed: Sandy

  Collected information:

  PET#   NAME                AGE    ANIMAL         BREED          
  --------------------------------------------------------------------------------
  1      Luna                8      Cat            Tuxedo          
  2      Kabe                10     Cat            Sandy          


# Pet struct (already written):
I've already declared the Pet struct above `main()`. The Pet structure contains four variables that each Pet will have:
- name, a string
- age, an integer
- animal, a string
- breed, a string

We can declare a pet variable like this: `Pet VARNAME;`

Then, we can access the internal variables like this:
`VARNAME.name = "PetName";`
`VARNAME.age = 10;`

For this lab we're utilizing this already-written struct.


# main:
Within `main()` the first Pet is already created. For `pet1` it asks the user to enter in the pet's name, age, animal, and breed.

Follow along and do the same for a second pet, `Pet pet2;`. Ask the same questions.

Beneath the `// ------- DISPLAY RESULTS -------` comment you don't have to modify anything; I've set up a table to be displayed to print out both pets' info.


# Reference
Struct declaration base:
```
struct STRUCTNAME
{
  // Declare variables in here
};
```
