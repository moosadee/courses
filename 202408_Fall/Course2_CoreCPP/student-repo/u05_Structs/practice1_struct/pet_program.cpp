// PROGRAM: Practice using struct
// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <iomanip>    // Used for setting column widths
#include <string>     // Library that contains `string` types
using namespace std;  // Using the C++ STanDard libraries

// - STRUCT DECLARATION -----------------------------------------------------//
// We are able to declare structs in the same file as main()
// but it must go *above* main() in order to use it within main().
// It is more standard to store a struct in its own .h file.
struct Pet
{
  string name{"notset"};
  int age{0};
  string animal{"notset"};
  string breed{"notset"};
}; // ; is required at the end of a struct declaration

// - PROGRAM CODE -----------------------------------------------------------//
int main()
{
  cout << "PET INFORMATION" << endl;
  // ------- GET USER INPUT -------
  cout << endl << "---- PET 1 ----" << endl;

  Pet pet1;

  // Ask the user to enter information...
  cout << "Name: ";             // Display prompt
  getline( cin, pet1.name );    // Get string input

  cout << "Age:  ";             // Display prompt
  cin >> pet1.age;              // Get int input
  cin.ignore();                 // Flush the input buffer

  cout << "Animal: ";           // Display prompt
  getline( cin, pet1.animal );  // Get string input

  cout << "Breed: ";            // Display prompt
  getline( cin, pet1.breed );   // Get string input



  cout << endl << "---- PET 2 ----" << endl;
  Pet pet2;

  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Ask the user to enter the name, age, animal, and breed for `pet2`.

  // -------------------------------------------------------------------------



  // ------- DISPLAY RESULTS -------

  cout << left; // Set left alignment
  cout << endl; // Add more spacing

  const int COL1 = 7;
  const int COL2 = 20;
  const int COL3 = 7;;
  const int COL4 = 15;
  const int COL5 = 15;

  cout << "Collected information:" << endl << endl;

  cout << setw( COL1 ) << "PET#" << setw( COL2 ) << "NAME" << setw( COL3 ) << "AGE" << setw( COL4 ) << "ANIMAL" << setw( COL5 ) << "BREED" << endl;
  cout << string( 80, '-' ) << endl;

  // Pet 1:
  cout << setw( COL1 ) << "1";
  cout << setw( COL2 ) << pet1.name;
  cout << setw( COL3 ) << pet1.age;
  cout << setw( COL4 ) << pet1.animal;
  cout << setw( COL5 ) << pet1.breed;
  cout << endl;


  // TODO: Display information for pet 2
  cout << setw( COL1 ) << "2";
  cout << setw( COL2 ) << pet2.name;
  cout << setw( COL3 ) << pet2.age;
  cout << setw( COL4 ) << pet2.animal;
  cout << setw( COL5 ) << pet2.breed;
  cout << endl;



 // Return 0 means quit program with no errors, in this context.
 return 0;
}
