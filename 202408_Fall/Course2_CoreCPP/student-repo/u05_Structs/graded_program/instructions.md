## Introduction

This program uses a struct to store a `Room`'s information and we're going to implement a function that specifies how our program will create a new room. We will also review some basic pointer usage.

# Building and running the program:
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

From the terminal, use the g++ compiler to build:
`g++ *.h *.cpp`

And then run it:
`./a.exe` (Windows)
`./a.out` (Linux/Mac)

## Example output

```
  1. Run AUTOMATED TESTS
  2. Run PROGRAM
  >> 2

  ################################################################################
  # ROOM INFO #
  #############

  bedroom1's address:        0x7ffc644bf560
  Address within CreateRoom: 0x7ffc644bf5c0
  Enter a width: 10
  Enter a length: 15
  Enter a name: garage room
  bedroom2's address:        0x7ffc644bf590
  Address within CreateRoom: 0x7ffc644bf5f0

  BEDROOM 1:
  - width:     10
  - length:    15
  - area:      150
  - perimeter: 50

  BEDROOM 2:
  - width:     10
  - length:    15
  - area:      150
  - perimeter: 50

  Your bedroom is garage room
  My bedroom is attic room
```

There are also automated tests, which should all pass:

```
1. Run AUTOMATED TESTS
2. Run PROGRAM
>> 1
[PASS]  TEST 1, CreateRoom(5, 7) = [width=5, length=7, area=35, perimeter=24]
[PASS]  TEST 2, CreateRoom(3, 11) = [width=3, length=11, area=33, perimeter=28]
```

## Room struct
The Room struct is declared within the "Room.h" file. The `Room` struct contains the following member variables:
- width, a float
- length, a float
- area, a float
- perimeter, a float
- name, a string


## CreateRoom function
Within "StructProgram.cpp" you'll see a `Room CreateRoom( float new_width, float new_length )` function.

This function has the following INPUT PARAMETERS:
- `string new_name`, the name for a new room
- `float new_width`, the width for a new room
- `float new_length`, the length for a new room

and it has one RETURN:
- `Room`, the Room object we create and set up within this function.

`Room my_room` has been declared at the beginning of the function and `return my_room` is at the end of the function. You'll be filling out the code in-between.

Set the `my_room` variable's internal `name` variable to the `new_name` passed in as an input paramter. Also assign the room's `length` and `width` in the same manner.

Afterwards, also calculate the area and perimeter, storing that data in `my_room.area` and `my_room.perimeter`.


## Main function

Within StructProgram.cpp you'll see a `void Main()` function.
You will be working here. First off, I've set up the first bedroom with some hard-coded information:

```
bedroom1 = CreateRoom( "attic room", 10, 15 );
```

Thanks to the CreateRoom function I can create everything in one step. Another way to use this function is to ask the user to enter information, store it in some variables, then pass those variables into the CreateRoom function call, which is what you'll be doing.

Within the function, do the following:

- DISPLAY a message "Enter a width" to the screen.
- GET KEYBOARD INPUT and store it in the `width` variable.
- DISPLAY a message "Enter a length" to the screen.
- GET KEYBOARD INPUT and store it in the `length` variable.
- DISPLAY a message "Enter a name" to the screen.
- Use `cin.ignore();` to flush the input buffer.
- Use `getline( cin, name );` to get a name from the keyboard, this will store it in the `name` variable.
- CALL the `CreateRoom` function, passing in `width` and `length` as its input arguments. Store the RETURN RESULT in the `new_room` variable.
- DISPLAY the room's width, length, area, and perimeter to the screen.



Before the end of the program, after displaying the BEDROOM 1 and BEDROOM 2 information (I already implemented), we're going to use a simple pointer to display each room's name.

First, create a new Room pointer and initialize it to nullptr: 
`Room* room_pointer{ nullptr };`

Then, assign `room_pointer` to the address of `bedroom2`. Remember that the address-of operator is `&`.

One way to access **member variables** within a struct pointer is like this:
`(*room_pointer).name`

Another way to display member variables within a struct pointer is like this:
`room_pointer->name`

Use both styles, first display `bedroom2`'s name via the `room_pointer`, then change the `room_pointer` to point to `bedroom1`'s address and display *its* name as well.


## Observations

Notice that the memory addresses of the Rooms differ while we're inside the CreateRoom function vs. inside of the Main function:

```
  bedroom1's address:        0x7ffc644bf560
  Address within CreateRoom: 0x7ffc644bf5c0
  
  [...]

  bedroom2's address:        0x7ffc644bf590
  Address within CreateRoom: 0x7ffc644bf5f0
```
