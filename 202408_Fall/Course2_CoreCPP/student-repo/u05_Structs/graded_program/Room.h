#ifndef _ROOM
#define _ROOM

#include <string>
using namespace std;

struct Room
{
  float width{0};
  float length{0};
  float area{0};
  float perimeter{0};
  string name{"notset"};
};

#endif
