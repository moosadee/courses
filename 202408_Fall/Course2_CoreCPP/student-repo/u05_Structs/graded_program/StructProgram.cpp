// Use `g++ *.cpp *.h` to build this program
#include "Room.h"

#include <iostream>
#include <string>
using namespace std;

/**
   @param   new_width      The new width we're assigning to the room
   @param   new_length     The new length we're assigning to the room
   @return  Room           The Room object we build
   Within this function, create a new Room variable.
   Assign its width and length to the `new_width` and `new_length` data passed in.  Also calculate the room's area and perimeter. At the end of the function return the room object.
 */
Room CreateRoom( string new_name, float new_width, float new_length )
{
  Room my_room;

  cout << "Address within CreateRoom: ";
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Print out my_room's memory address here
  cout << &my_room << endl;

  // TODO: Implement this function!


  // -------------------------------------------------------------------------
  // This function returns the room we just created
  return my_room;
}

void Program()
{
  float width, length;
  string name;
  Room bedroom1, bedroom2;


  // FIRST ROOM
  cout << "bedroom1's address:        " << &bedroom1 << endl;
  bedroom1 = CreateRoom( "attic room", 10, 15 );

  // SECOND ROOM
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Ask the user to enter width, store in width variable.
  
  // TODO: Ask the user to enter length, store in length variable.
  
  // TODO: Ask the user to enter name, store in name variable.
  
  // TODO: Display bedroom 2's address
  
  // TODO: Call the CreateRoom function, pass in name, width, and length as parameters. Store result in bedroom2 variable.

  // -------------------------------------------------------------------------

  

  cout << endl << "BEDROOM 1:" << endl;
  cout << "- width:     " << bedroom1.width <<  endl;
  cout << "- length:    " << bedroom1.length << endl;
  cout << "- area:      " << bedroom1.area << endl;
  cout << "- perimeter: " << bedroom1.perimeter << endl;

  cout << endl << "BEDROOM 2:" << endl;
  cout << "- width:     " << bedroom2.width <<  endl;
  cout << "- length:    " << bedroom2.length << endl;
  cout << "- area:      " << bedroom2.area << endl;
  cout << "- perimeter: " << bedroom2.perimeter << endl;
  cout << endl;


  Room* room_pointer{ nullptr };

  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Assign room_pointer to bedroom2's address.
  room_pointer = &bedroom2;
  // TODO: Display the name of the bedroom via the room_pointer.
  cout << "Your bedroom is " << (*room_pointer).name << endl;

  // TODO: Assign room_pointer to bedroom1's address.
  room_pointer = &bedroom1;
  // TODO: Display the name of the bedroom via the room_pointer.
  cout << "My bedroom is " << room_pointer->name << endl;
  // -------------------------------------------------------------------------
  cout << endl;
}



//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! DON'T MODIFY ANYTHING BELOW THIS POINT! !!
//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! I've written custom main code and an automated tester.
//   !! You only need to work with the functions above this.
enum class ProgramAction { TEST, PROGRAM };
void Tester();

int main( int argCount, char* args[] )
{
  string program_name = "ROOM INFO";
  ProgramAction action;
  if ( argCount > 1 && args[1] == "test" )
    {
      action = ProgramAction::TEST;
    }
  else
    {
      std::cout << "1. Run AUTOMATED TESTS" << std::endl;
      std::cout << "2. Run PROGRAM" << std::endl;
      int choice;
      std::cout << ">> ";
      std::cin >> choice;

      switch( choice )
        {
        case 1: action = ProgramAction::TEST; break;
        case 2: action = ProgramAction::PROGRAM; break;
        }
    }

  if ( action == ProgramAction::TEST )
    {
      Tester();
    }
  else
    {
      program_name = "# " + program_name + " #";
      cout << endl << string( 80, '#' ) << endl;
      cout << program_name << endl;
      cout << string( program_name.size(), '#' ) << endl << endl;

      Program();
    }

  return 0;
}

// - AUTOMATED TESTER -------------------------------------------------------//
void Tester()
{
  const string GRN = "\033[0;32m"; const string RED = "\033[0;31m"; const string BOLD = "\033[0;35m"; const string CLR = "\033[0m";

  // (Automated test):

  const int TOTAL_TESTS = 2;
  string   in0[TOTAL_TESTS]; // inputs 0
  float    in1[TOTAL_TESTS]; // inputs 1
  float    in2[TOTAL_TESTS]; // inputs 2
  Room     exo[TOTAL_TESTS]; // expected output
  Room     aco[TOTAL_TESTS]; // actual output

  // Setup test 1
  in0[0] = "bathroom";
  in1[0] = 5;
  in2[0] = 7;
  exo[0].width = 5;
  exo[0].length = 7;
  exo[0].area = 35;
  exo[0].perimeter = 24;
  exo[0].name = "bathroom";

  // Setup test 2
  in0[1] = "kitchen";
  in1[1] = 3;
  in2[1] = 11;
  exo[1].width = 3;
  exo[1].length = 11;
  exo[1].area = 33;
  exo[1].perimeter = 28;
  exo[1].name = "kitchen";

  // Run tests
  for ( int i = 0; i < TOTAL_TESTS; i++ )
  {
    aco[i] = CreateRoom( in0[i], in1[i], in2[i] );

    if (
        aco[i].name      == exo[i].name &&
        aco[i].width     == exo[i].width &&
        aco[i].length    == exo[i].length &&
        aco[i].area      == exo[i].area &&
        aco[i].perimeter == exo[i].perimeter
         )
    {
      // PASS
      cout << GRN << "[PASS] ";
      cout << " TEST " << i+1 << ", CreateRoom(" << in0[i] << ", " << in1[i] << ", " << in2[i] << ") = [";

      cout << "width=" << aco[i].width << ", ";
      cout << "length=" << aco[i].length << ", ";
      cout << "area=" << aco[i].area << ", ";
      cout << "perimeter=" << aco[i].perimeter;
      cout << "]" << endl;
    }
    else
    {
      // FAIL
      cout << RED << "[FAIL] ";
      cout << " TEST " << i+1 << ", CreateRoom(" << in0[i] << ", " << in1[i] << ", " << in2[i] << ")" << endl;
      cout << "   EXPECTED OUTPUT: [";
      cout << "name=" << exo[i].name << ", ";
      cout << "width=" << exo[i].width << ", ";
      cout << "length=" << exo[i].length << ", ";
      cout << "area=" << exo[i].area << ", ";
      cout << "perimeter=" << exo[i].perimeter;
      cout << "]" << endl;
      cout << "   ACTUAL OUTPUT:   [" ;
      cout << "name=" << aco[i].name << ", ";
      cout << "width=" << aco[i].width << ", ";
      cout << "length=" << aco[i].length << ", ";
      cout << "area=" << aco[i].area << ", ";
      cout << "perimeter=" << aco[i].perimeter;
      cout << "]" << endl;
    }
    cout << CLR;
  }
  cout << CLR;
}
