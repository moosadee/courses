# Building and running the program:
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

From the terminal, use the g++ compiler to build:
`g++ *.cpp`

And then run it:
`./a.exe` (Windows)
`./a.out` (Linux/Mac)


# Example program output:
    MY CLASSES v1

    Getting input:
    Enter class #0: CS 134
    Enter class #1: CS 200
    Enter class #2: CS 235
    Enter class #3: CS 250
    Enter class #4: ASL 120

    Resulting array:
    0 = CS 134
    1 = CS 200
    2 = CS 235
    3 = CS 250
    4 = ASL 120


# Instructions
1. Near the top of `main()`, declare an array of strings. Its size should be `ARRAY_SIZE`.

2. Within the for loop after "Getting input:", the message "Enter class #" is displayed. Afterwards, get the user's input from the keyboard and store it in your array, at position `[i]`.

3. After the text "Resulting array:", write a new for loop like the previous one. Within the loop, display each index `i` and each element at that position, `ARRAYNAME[i]`.


# Reference
C-style array declaration:
`TYPE ARRAYNAME[SIZE];`

Accessing an element at *index* i:
`cout << ARRAYNAME[i];`

- "Element of the array at position i" means use ARRAYNAME[i] to access a specific element
- `size_t` is short for `unsigned int`. Sizes of arrays cannot be negative, so using `unsigned` means the integer can be 0 to some MAX INT value.
