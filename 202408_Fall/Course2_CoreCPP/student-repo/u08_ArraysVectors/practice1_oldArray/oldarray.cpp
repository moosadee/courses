// PROGRAM: Practice using the C style array
#include <iostream>
#include <string>
using namespace std;

int main()
{
  cout << "MY CLASSES v1" << endl << endl;
  const size_t ARRAY_SIZE = 5;

  // - STUDENT CODE ----------------------------------------------------------
  // 1. TODO: Declare an array of strings, use `ARRAY_SIZE` as the size.
  
  // -------------------------------------------------------------------------



  cout << "Getting input:" << endl;
  for ( size_t i = 0; i < ARRAY_SIZE; i++ )
  {
    cout << "Enter class #" << i << ": ";

    // - STUDENT CODE ----------------------------------------------------------
    // 2. TODO: Get the user's input, store it in the array at position `i`.
    
    // -------------------------------------------------------------------------
  }


  cout << endl << "Resulting array:" << endl;
  // - STUDENT CODE ----------------------------------------------------------
  // 3. TODO: Write a for loop that starts at `i` = 0, 
  // loops while `i` is less than `ARRAY_SIZE`, 
  // and increments `i` by 1 each iteration.
  // Within the array, display the value of `i` and
  // the element of the array at position `i`.
  
  // -------------------------------------------------------------------------

  return 0;
}
