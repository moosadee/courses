// PROGRAM: Practice using the STL vector
#include <iostream>
#include <vector>
#include <string>
using namespace std;

int main()
{
  cout << "MY CLASSES v3" << endl << endl;

  string new_class;
  char again = 'Y';

  // - STUDENT CODE ----------------------------------------------------------
  // 1. TODO: Declare a vector of strings named my_classes.
  
  // -------------------------------------------------------------------------

  do 
  {
    cout << "Enter class #" << my_classes.size() << ": ";
    getline( cin, new_class );

    // - STUDENT CODE ----------------------------------------------------------
    // 2. TODO: Push the `new_class` variable into the `my_classes` vector.
    
    // -------------------------------------------------------------------------

    cout << "Enter another? (Y/N): ";
    cin >> again;
    again = toupper( again );
    cin.ignore();
    cout << endl;
  } while ( again == 'Y' );


  cout << endl << "Resulting array:" << endl;
  // - STUDENT CODE ----------------------------------------------------------
  // 3. TODO: Create a for loop that iterates over all the elements of `my_classes`.
  // Within the loop, display the index and the element.
  
  // -------------------------------------------------------------------------

  return 0;
}
