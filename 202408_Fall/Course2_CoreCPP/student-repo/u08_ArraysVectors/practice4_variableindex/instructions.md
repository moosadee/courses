# Building and running the program:
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

From the terminal, use the g++ compiler to build:
`g++ *.cpp`

And then run it:
`./a.exe` (Windows)
`./a.out` (Linux/Mac)


# Example program output:
    TACO PLACE

    MENU
    0. Bean Burrito ($1.99)
    1. Crunchy Taco ($1.79)
    2. Baja Blast ($1.29)

    What do you want to eat? 1

    You chose:

    Product: Crunchy Taco
    Cost: $1.79


# Instructions
1. Under the "Product:" text, display the `name` of the `product_list` element at index `index`.

2. Under the "Cost: $" text, display the `price` of the `product_list` element at index `index`.


# Reference
Accessing an array element's member variable:
`ARRAYNAME[INDEX].VARIABLE`

Displaying an array element's member variable:
`cout << ARRAYNAME[INDEX].VARIABLE;`
