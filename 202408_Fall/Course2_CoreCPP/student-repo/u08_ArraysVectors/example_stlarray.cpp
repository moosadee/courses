// PROGRAM: Practice using the STL array
#include <iostream>
#include <array>
#include <string>
using namespace std;

int main()
{
  array<string, 5> my_classes;

  cout << "Getting input:" << endl;

  for ( size_t i = 0; i < my_classes.size(); i++ )
  {
    cout << "Enter class #" << i << ": ";
    getline( cin, my_classes[i] );
  }

  cout << endl << "Resulting array:" << endl;
 for ( size_t i = 0; i < my_classes.size(); i++ )
  {
    cout << i << " = " << my_classes[i] << endl;
  }

  return 0;
}
