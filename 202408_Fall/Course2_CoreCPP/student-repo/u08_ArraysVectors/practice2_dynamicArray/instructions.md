# Building and running the program:
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

From the terminal, use the g++ compiler to build:
`g++ *.cpp`

And then run it:
`./a.exe` (Windows)
`./a.out` (Linux/Mac)


# Example program output:
    MY CLASSES v2

    How many classes do you have? 3

    Getting input:

    Enter class #0: CS 210

    Enter class #1: CS 200

    Enter class #2: ASL 120

    Resulting array:
    0 = CS 210
    1 = CS 200
    2 = ASL 120


# Instructions
1. After the user enters `total_classes`, create a dynamic array of strings. Use `total_classes` as the size.

2. After the "Getting input:" text, write a for loop that goes from `i=0` to the size of the array `total_classes`, incrementing by 1 each time. Within the loop, do the following:
    a. Display "Enter class #" and the value of `i`.
    b. Get the user's input from the keyboard and store it in the array element at position `i`.

3. After the "Resulting array" text, within this second for loop, display the value of `i` and the element at that position.

4. Before `return 0;`, make sure to free the array's memory.


# Reference
Create a dynamic array:
`TYPE* ARRAYNAME = new TYPE[SIZE];`

Free array's memory:
`delete [] ARRAYNAME;`

Iterate through an array, given some SIZE:
```
for ( size_t i = 0; i < SIZE; i++ )
{
    // index is i
    // element is ARRAYNAME[i]
}
```
