// PROGRAM: Practice creating a dynamic array using a pointer
#include <iostream>
#include <array>
#include <string>
using namespace std;

int main()
{
  cout << "MY CLASSES v2" << endl << endl;
  size_t total_classes;

  cout << "How many classes do you have? ";
  cin >> total_classes;
  cin.ignore();

  // - STUDENT CODE ----------------------------------------------------------
  // 1. TODO: Create a new dynamic array of strings, use `total_classes` as the size.
  
  // -------------------------------------------------------------------------

  
  cout << endl << "Getting input:" << endl;
  // - STUDENT CODE ----------------------------------------------------------
  // 2. TODO: Loop over all the indices of the array. Within the for loop,
  // display "Enter class #" and the value of `i`. Afterwards,
  // get the user's input and store it in the array at position `[i]`.
  
  // -------------------------------------------------------------------------

  cout << endl << "Resulting array:" << endl;

  for ( size_t i = 0; i < total_classes; i++ )
  {
    // - STUDENT CODE ----------------------------------------------------------
    // 3. TODO: Display the index `i` and the element `ARRAYNAME[i]`.
    
    // -------------------------------------------------------------------------
  }

  // - STUDENT CODE ----------------------------------------------------------
  // 4. TODO: Free the array's memory
  
  // -------------------------------------------------------------------------

  return 0;
}
