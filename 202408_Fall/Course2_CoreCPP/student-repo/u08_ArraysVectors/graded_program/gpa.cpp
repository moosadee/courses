// Use `g++ *.cpp` to build this program
// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
#include <vector>     // Library that contains `vector`
#include <iomanip>    // Library for formatting; `setprecision`
using namespace std;  // Using the C++ STanDard libraries

// - STUDENT CODE -----------------------------------------------------------//
float CalculateGpa( vector<float> course_grades )
{
  float total{ 0 };
  float average{ 0 };

  // - STUDENT CODE ----------------------------------------------------------
  // 1. TODO: Use a for loop to iterate over all the grades in the `course_grades` vector.
  // Within the loop, add each grade element to the `total`.


  // 2. TODO: Calculate the average (total divided by the amount of elements, use vector's size function.)

  // -------------------------------------------------------------------------
  
  // Returns the average
  return average;
}


void Program()
{
  cout << "GPA CALCULATOR" << endl << endl;

  vector<float> grades;
  float this_grade;
  float result_gpa;
  char choice;

  cout << "A = 4.0 \t B = 3.0 \t C = 2.0 \t D = 1.0 \t F = 0.0" << endl << endl;

  while ( choice != 'N' )
  {
    cout << "Enter a grade: ";
    cin >> this_grade;


    // - STUDENT CODE ----------------------------------------------------------
    // 3. TODO: Push `this_grade` into the `grades` vector.
    grades.push_back( this_grade );
    // -------------------------------------------------------------------------


    cout << "Enter another? (Y/N): ";
    cin >> choice;
    choice = toupper( choice );
    cout << endl;
  }

  // - STUDENT CODE ----------------------------------------------------------
  // 4. TODO: Call the CalculateGpa function, pass in the `grades` vector. Store the result in the `result_gpa` variable.
  result_gpa = CalculateGpa( grades );
  // -------------------------------------------------------------------------

  cout << "RESULT: " << result_gpa << endl;
}




//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! DON'T MODIFY ANYTHING BELOW THIS POINT! !!
//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! I've written custom main code and an automated tester.
//   !! You only need to work with the functions above this.
// - PROGRAM STARTER --------------------------------------------------------//
enum class ProgramAction { TEST, PROGRAM };
void Tester();
template <typename T>
void DisplayVector( vector<T> vec );
int main( int argCount, char* args[] )
{
  cout << fixed << setprecision( 2 );
  if ( argCount == 2 && string( args[1] ) == "test" )
  {
    // Run tests
    Tester();
  }
  else if ( argCount == 1 )
    {
      std::cout << "1. Run AUTOMATED TESTS" << std::endl;
      std::cout << "2. Run PROGRAMS" << std::endl;
      int choice;
      std::cout << ">> ";
      std::cin >> choice;

      if ( choice == 1 )
        {
          Tester();
        }
      else
        {
          Program();
        }
  }
  else
  {
    // Run program
    vector<float> grades;
    for ( int i = 1; i < argCount; i++ )
    {
      grades.push_back( stof( args[i] ) );
    }
    float result = CalculateGpa( grades );
    cout << "RESULT: " << result << endl;
  }

  return 0;
}


// - AUTOMATED TESTER -------------------------------------------------------//

void Tester()
{
  const string GRN = "\033[0;32m"; const string RED = "\033[0;31m"; const string BOLD = "\033[0;35m"; const string CLR = "\033[0m";
  // (Automated test):

  const int TOTAL_TESTS = 2;
  vector< vector<float> > in1; // inputs 1
  float  exo[TOTAL_TESTS]; // expected output
  float  aco[TOTAL_TESTS]; // actual output

  // Setup test 1
  in1.push_back ( { 4.0, 4.0, 3.0, 2.0 } ); // "AABC";
  exo[0] = 3.25;

  // Setup test 2
  in1.push_back( {4.0, 3.0, 2.0, 2.0, 1.0, 0.0 } ); // "ABCCCDF";
  exo[1] = 2.0;

  // Run tests
  for ( int i = 0; i < TOTAL_TESTS; i++ )
  {
    //vector<float> grades;
    //for ( size_t grade = 0; grade < in1[i].size(); grade++ )
    //{
    //  grades.push_back( in1[i][grade] );
    //}

    aco[i] = CalculateGpa( in1[i] );

    if ( aco[i] >= exo[i] - 0.1 && aco[i] <= exo[i] + 0.1 )
    {
      // PASS
      cout << GRN << "[PASS] ";
      cout << " TEST " << i+1 << ", StudentCode(";
      DisplayVector( in1[i] );
       //<< in1[i] <<
       cout << ") = " << aco[i] << endl;
    }
    else
    {
      // FAIL
      cout << RED << "[FAIL] ";
      cout << " TEST " << i+1 << ", StudentCode(";
      // << in1[i]
      DisplayVector( in1[i] );
      cout << ")" << endl;
      cout << "   EXPECTED OUTPUT: [" << exo[i] << "]" << endl;
      cout << "   ACTUAL OUTPUT:   [" << aco[i] << "]" << endl;
    }
  }
  cout << CLR;
}


template <typename T>
void DisplayVector( vector<T> vec )
{
  cout << "{ ";
  for ( size_t i = 0; i < vec.size(); i++ )
    {
      if ( i != 0 ) { cout << ", "; }
      cout << vec[i];
    }
  cout << " }";
}
