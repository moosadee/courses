# Building and running the program:
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

From the terminal, use the g++ compiler to build:
`g++ *.cpp`

And then run it:
`./a.exe` (Windows)
`./a.out` (Linux/Mac)


# Example output (program)
  1. Run AUTOMATED TESTS
  2. Run PROGRAMS
  >> 2
  GPA CALCULATOR

  A = 4.0          B = 3.0         C = 2.0         D = 1.0         F = 0.0
  Enter a grade: 4.0
  Enter another? (Y/N): y

  Enter a grade: 2.0
  Enter another? (Y/N): y

  Enter a grade: 1.0
  Enter another? (Y/N): n

  RESULT: 2.33


# Example output (passing tests)
  1. Run AUTOMATED TESTS
  2. Run PROGRAMS
  >> 1
  [PASS]  TEST 1, StudentCode({ 4.00, 4.00, 3.00, 2.00 }) = 3.25
  [PASS]  TEST 2, StudentCode({ 4.00, 3.00, 2.00, 2.00, 1.00, 0.00 }) = 2.00


# Instructions
You will be working in *gpa.cpp*.

## float CalculateGpa( vector<float> course_grades )
This function returns a **float**, which will be the calculated gpa. It takes in a vector of floats, representing grades, such as 4.0 for A, 3.0 for B, and so on.

1. After the `total` and `average` variables are declared, create a for loop to iterate over all of the `course_grades` elements. Add each element onto the `total`.

2. After the for loop, calculate the average. Store the result in the `average` variable. (Use the vector's `.size()` function to get the amount of elements.)


## void StuMain()
3. Within the while loop, the user enters `this_grade`. After that cin statement, push `this_grade` onto the `grades` vector.

4. After the while loop is done, call the `CalculateGpa` function. Pass in the `grades` vector, and store the result in the `result_gpa` variable.
