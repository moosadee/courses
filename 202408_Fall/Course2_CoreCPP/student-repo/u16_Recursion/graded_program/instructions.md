# Building and running the program:
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

From the terminal, use the g++ compiler to build:
`g++ *.cpp`

And then run it:
`./a.exe` (Windows)
`./a.out` (Linux/Mac)


# Example program output:
*Run program:*
  1. Run AUTOMATED TESTS
  2. Run PROGRAM
  >> 2

  ################################################################################
  # RECURSION #
  #############

  GetFirstUppercase, Iterative:
  * First upper-case in how are YOU?: 'Y'
  * First upper-case in What?: 'W'
  * First upper-case in where am I?: 'I'
  * First upper-case in no caps: ' '


  GetConsonants, Recursive:
  * First upper-case in how are YOU?: 'Y'
  * First upper-case in What?: 'W'
  * First upper-case in where am I?: 'I'
  * First upper-case in no caps: ' '


*Run tests:*
  1. Run AUTOMATED TESTS
  2. Run PROGRAM
  >> 1

  ---------------------------------------------------
  Test - GetFirstUppercase
  [PASS], GetFirstUppercase_Iter( "HELLO", 0 )
  [PASS], GetFirstUppercase_Iter( "heLLO", 0 )
  [PASS], GetFirstUppercase_Iter( "hello", 0 )
  [PASS], GetFirstUppercase_Rec( "HELLO", 0 )
  [PASS], GetFirstUppercase_Rec( "heLLO", 0 )
  [PASS], GetFirstUppercase_Rec( "hello", 0 )



# Instructions

For the graded assignment implement the iterative and recursion versions of the GetFirstUppercase function. Follow the instructions in the comments for hints.
