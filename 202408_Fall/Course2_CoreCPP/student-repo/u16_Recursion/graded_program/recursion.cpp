// Use `g++ *.cpp` to build this program
// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
#include <iomanip>    // Library for formatting; `setprecision`
#include <fstream>    // Library for using `ifstream` and `ofstream`
using namespace std;  // Using the C++ STanDard libraries

// - HELPER FUNCTIONS -----------------------------------------------------------//

//! Helper function to figure out if letter is upper-case
bool IsUppercase( char letter )
{
  return ( letter != ' ' && toupper( letter ) == letter );
}

// - STUDENT CODE -----------------------------------------------------------//

/**
@param  string  text    The text to look for capital letters in
@return char            The first upper-case character found, or ' ' if none found.

Iterate through each char in the string [text] and return the char if it is an upper-case letter.
If no upper-case letters are found, return a space character: ' '
*/
//! Returns the first uppercase letter found, or ' ' if none are found.
char GetFirstUppercase_Iter( string text )
{
  return 'x'; // TODO: TEMPORARY, REMOVE THIS LINE.
}

/**
@param  string  text    The text to look for capital letters in
@param  int     pos     The current position being investigated
@return char            The first upper-case character found, or ' ' if none found.

Recurse through each char in the string [text] and return the char if it is an upper-case letter.
If no upper-case letters are found, return a space character: ' '
*/
//! Returns the first uppercase letter found, or ' ' if none are found.
char GetFirstUppercase_Rec( string text, unsigned int pos /* = 0 */ )
{
  return 'x'; // TODO: TEMPORARY, REMOVE THIS LINE.
}



void Program()
{
  string text[] = { "how are YOU?", "What?", "where am I?", "no caps" };

  cout << "GetFirstUppercase, Iterative:" << endl;
  for ( int i = 0; i < 4; i++ )
    {
      cout << " * First upper-case in " << text[i] << ": '" << GetFirstUppercase_Iter( text[i] ) << "'" << endl;
    }

  cout << endl << endl;
  cout << "GetConsonants, Recursive:" << endl;
  for ( int i = 0; i < 4; i++ )
    {
      cout << " * First upper-case in " << text[i] << ": '" << GetFirstUppercase_Rec( text[i], 0 ) << "'" << endl;
    }

  cout << endl << endl;
}



//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! DON'T MODIFY ANYTHING BELOW THIS POINT! !!
//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! I've written custom main code and an automated tester.
//   !! You only need to work with the functions above this.
enum class ProgramAction { TEST, PROGRAM };
void Tester();
int main( int argCount, char* args[] )
{
  string program_name = "RECURSION";
  ProgramAction action;
  if ( argCount > 1 && string( args[1] ) == "test" )
    {
      action = ProgramAction::TEST;
    }
  else
    {
      std::cout << "1. Run AUTOMATED TESTS" << std::endl;
      std::cout << "2. Run PROGRAM" << std::endl;
      int choice;
      std::cout << ">> ";
      std::cin >> choice;

      switch( choice )
        {
        case 1: action = ProgramAction::TEST; break;
        case 2: action = ProgramAction::PROGRAM; break;
        }
    }

  if ( action == ProgramAction::TEST )
    {
      Tester();
    }
  else
    {
      program_name = "# " + program_name + " #";
      cout << endl << string( 80, '#' ) << endl;
      cout << program_name << endl;
      cout << string( program_name.size(), '#' ) << endl << endl;

      // PROGRAM
      Program();
    }

  return 0;
}

// - AUTOMATED TESTER -------------------------------------------------------//
void Tester()
{
  const string GRN = "\033[0;32m"; const string RED = "\033[0;31m"; const string BOLD = "\033[0;35m"; const string CLR = "\033[0m";

    cout << endl << "---------------------------------------------------" << endl;
    cout << "Test - GetFirstUppercase" << endl;
    char expectedOut, actualOut;

    { // Test 1
      expectedOut = 'H';
      actualOut = GetFirstUppercase_Iter( "HELLO" );

      if ( actualOut == expectedOut )     { cout << GRN <<" [PASS], GetFirstUppercase_Iter( \"HELLO\", 0 )" << endl; }
      else
        {
          cout << RED << "[FAIL] GetFirstUppercase_Iter( \"HELLO\", 0 )" << endl;
          cout << "   EXPECTED OUTPUT: '" << expectedOut << "'" << endl;
          cout << "   ACTUAL OUTPUT:   '" << actualOut << "'" << endl;
        }
      cout << CLR;
    }

    { // Test 2
      expectedOut = 'L';
      actualOut = GetFirstUppercase_Iter( "heLLO" );

      if ( actualOut == expectedOut )     { cout << GRN <<" [PASS], GetFirstUppercase_Iter( \"heLLO\", 0 )" << endl; }
      else
        {
          cout << RED << "[FAIL] GetFirstUppercase_Iter( \"heLLO\", 0 )" << endl;
          cout << "   EXPECTED OUTPUT: '" << expectedOut << "'" << endl;
          cout << "   ACTUAL OUTPUT:   '" << actualOut << "'" << endl;
        }
      cout << CLR;
    }

    { // Test 3
      expectedOut = ' ';
      actualOut = GetFirstUppercase_Iter( "hello" );

      if ( actualOut == expectedOut )     { cout << GRN <<" [PASS], GetFirstUppercase_Iter( \"hello\", 0 )" << endl; }
      else
        {
          cout << RED << "[FAIL] GetFirstUppercase_Iter( \"hello\", 0 )" << endl;
          cout << "   EXPECTED OUTPUT: '" << expectedOut << "'" << endl;
          cout << "   ACTUAL OUTPUT:   '" << actualOut << "'" << endl;
        }
      cout << CLR;
    }


    { // Test 3
      expectedOut = 'H';
      actualOut = GetFirstUppercase_Rec( "HELLO", 0 );

      if ( actualOut == expectedOut )     { cout << GRN <<" [PASS], GetFirstUppercase_Rec( \"HELLO\", 0 )" << endl; }
      else
        {
          cout << RED << "[FAIL] GetFirstUppercase_Rec( \"HELLO\", 0 )" << endl;
          cout << "   EXPECTED OUTPUT: '" << expectedOut << "'" << endl;
          cout << "   ACTUAL OUTPUT:   '" << actualOut << "'" << endl;
        }
      cout << CLR;
    }

    { // Test 4
      expectedOut = 'L';
      actualOut = GetFirstUppercase_Rec( "heLLO", 0 );

      if ( actualOut == expectedOut )     { cout << GRN <<" [PASS], GetFirstUppercase_Rec( \"heLLO\", 0 )" << endl; }
      else
        {
          cout << RED << "[FAIL] GetFirstUppercase_Rec( \"heLLO\", 0 )" << endl;
          cout << "   EXPECTED OUTPUT: '" << expectedOut << "'" << endl;
          cout << "   ACTUAL OUTPUT:   '" << actualOut << "'" << endl;
        }
      cout << CLR;
    }

    { // Test 5
      expectedOut = ' ';
      actualOut = GetFirstUppercase_Rec( "hello", 0 );

      if ( actualOut == expectedOut )     { cout << GRN <<" [PASS], GetFirstUppercase_Rec( \"hello\", 0 )" << endl; }
      else
        {
          cout << RED << "[FAIL] GetFirstUppercase_Rec( \"hello\", 0 )" << endl;
          cout << "   EXPECTED OUTPUT: '" << expectedOut << "'" << endl;
          cout << "   ACTUAL OUTPUT:   '" << actualOut << "'" << endl;
        }
      cout << CLR;
    }
}
