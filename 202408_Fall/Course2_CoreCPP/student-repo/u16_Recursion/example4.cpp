#include <iostream>
#include <vector>
using namespace std;

void Combinations_Iter( vector<char> data )
{
  for ( size_t i = 0; i < data.size()-1; i++ )
    {
      for ( size_t j = i+1; j < data.size(); j++ )
        {
          cout << "(" << i << "," << j << "): " << data[i] << "-" << data[j] << endl;
        }
    }
}

void Combinations_Rec( vector<char> data, int i = 0, int j = 1 );
void Combinations_Rec( vector<char> data, int i, int j )
{
  if ( i == data.size() - 1 ) { return; }

  cout << "(" << i << "," << j << "): " << data[i] << "-" << data[j] << endl;
  if ( j == data.size() - 1 )
    {
      Combinations_Rec( data, i+1, i+2 );
    }
  else
    {
      Combinations_Rec( data, i, j+1 );
    }

}

int main()
{
  vector<char> data = { 'C', 'A', 'T', 'S' };

  Combinations_Iter( data );

  cout << endl << endl;

  Combinations_Rec( data );

  return 0;
}
