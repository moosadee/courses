# Building and running the program:
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

From the terminal, use the g++ compiler to build:
`g++ *.cpp`

And then run it:
`./a.exe` (Windows)
`./a.out` (Linux/Mac)


# Example program output:
*Alphabet function:*
  Enter starting letter and ending letter: r z

  Alphabet, Iterative: rstuvwxyz

  Alphabet, Recursive: rstuvwxyz


*Factorial function:*
  Enter a value for n: 5

  Factorial, Iterative: 5! = 120

  Factorial, Recursive: 5! = 120


*Count consonants function:*
Enter a word (no spaces): whoareyou

GetConsonants, Iterative:
 * Consonants in whoareyou: 4

GetConsonants, Recursive:
 * Consonants in whoareyou: 4


*Run tests:*
  1. Run AUTOMATED TESTS
  2. Run PROGRAM
  >> 1

  ---------------------------------------------------
  ALPHABET TESTS 
  [PASS]  TEST 1a, Alphabet_Iter(a, g) = abcdefg
  [PASS]  TEST 1b, Alphabet_Rec(a, g) = abcdefg
  [PASS]  TEST 2a, Alphabet_Iter(l, p) = lmnop
  [PASS]  TEST 2b, Alphabet_Rec(l, p) = lmnop

  ---------------------------------------------------
  FACTORIAL TESTS 
  [PASS]  TEST 1a, Factorial_Iter(0) = 1
  [PASS]  TEST 1b, Factorial_Rec(0) = 1
  [PASS]  TEST 2a, Factorial_Iter(1) = 1
  [PASS]  TEST 2b, Factorial_Rec(1) = 1
  [PASS]  TEST 3a, Factorial_Iter(3) = 6
  [PASS]  TEST 3b, Factorial_Rec(3) = 6
  [PASS]  TEST 4a, Factorial_Iter(4) = 24
  [PASS]  TEST 4b, Factorial_Rec(4) = 24

  ---------------------------------------------------
  CONSONANT TESTS 
  [PASS]  TEST 1a, CountConsonants_Iter("aeiou") = 0
  [PASS]  TEST 1b, CountConsonants_Rec("aeiou") = 0
  [PASS]  TEST 2a, CountConsonants_Iter("jkl") = 3
  [PASS]  TEST 2b, CountConsonants_Rec("jkl") = 3
  [PASS]  TEST 3a, CountConsonants_Iter("hellothere") = 6
  [PASS]  TEST 3b, CountConsonants_Rec("hellothere") = 6




# Instructions

There are six total functions to implement:
- Alphabet_Iter
- Alphabet_Rec
- Factorial_Iter
- Factorial_Rec
- CountConsonants_Iter
- CountConsonants_Rec

There are also automated tests in this practice assignment so you can verify your work that way.


## Alphabet functions
`string Alphabet_Iter(char start, char end)`
and
`string Alphabet_Rec(char start, char end, string text )`

These functions should take a starting char and an ending char and generate a text string that uses all letters between `start` and `end`. For example:

Alphabet_Iter(a, g) = abcdefg




## Factorial functions
`int Factorial_Iter(int n)`
and
`int Factorial_Rec(int n)`

Both of these functions take in a value for `n` and the value of `n!`  is calculated, which is

n! = n * (n-1) * (n-2) * ... * 1

Also, 0! = 1, and 1! = 1.



## Count Consonant functions
`int CountConsonants_Iter(string text)`
and
`int CountConsonants_Rec(string text, unsigned int pos )`

Both of these functions take in a string and count how many consonants are present in the string. For example:

CountConsonants_Iter("hellothere") = 6


