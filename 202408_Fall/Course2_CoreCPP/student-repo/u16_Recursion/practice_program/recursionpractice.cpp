// Use `g++ *.cpp` to build this program
// - INCLUDES ---------------------------------------------------------------//
#include <iostream>  // Library that contains `cout` commands
#include <string>    // Library that contains `string` types
#include <iomanip>   // Library for formatting; `setprecision`
#include <fstream>   // Library for using `ifstream` and `ofstream`
using namespace std; // Using the C++ STanDard libraries

string Alphabet_Rec(char start, char end, string text = ""); // declaration
int CountConsonants_Rec(string text, unsigned int pos = 0);  // declaration

// - HELPER FUNCTIONS -----------------------------------------------------------//

//! Helper function to figure out if letter is upper-case
bool IsUppercase(char letter)
{
  return (letter != ' ' && toupper(letter) == letter);
}

// - STUDENT CODE -----------------------------------------------------------//
/***********************************************/
/** Function Set 1 ****************************/

/**
@param      char        start       The starting char (inclusive) to begin at
@param      char        end         The end char (inclusive) to run until
@return     string                  A string containing all the letters from start to end.
*/
//! Build a string that contains letters from start to end.
string Alphabet_Iter(char start, char end)
{
  return "NOT IMPLEMENTED"; // TODO: REMOVE THIS LINE
}

/**
@param      char        start       The starting char (inclusive) to begin at
@param      char        end         The end char (inclusive) to run until
@return     string                  A string containing all the letters from start to end.
*/
//! Build a string that contains letters from start to end.
string Alphabet_Rec(char start, char end, string text )
{
  return "NOT IMPLEMENTED"; // TODO: REMOVE THIS LINE
}

/***********************************************/
/** Function Set 2 ****************************/

/**
Factorial functions
@param      int     n       The value of n
@return     int             The value of n!

Calculate n! by multiplying n * (n-1) * (n-2) * ... * 3 * 2 * 1.
*/
//! Calculates n!
int Factorial_Iter(int n)
{
  int product = 1;
  if (n == 0 || n == 1)
  {
    return 1;
  }
  for (int i = n; i > 1; i--)
  {
    product *= i;
  }
  return product;
  return -1; // TODO: REMOVE THIS LINE
}

/**
Factorial functions
@param      int     n       The value of n
@return     int             The value of n!

Calculate n! by multiplying n * (n-1) * (n-2) * ... * 3 * 2 * 1.
*/
//! Calculates n!
int Factorial_Rec(int n)
{
  // Terminating case:
  // n is 0.
  if (n == 0)
  {
    return 1;
  }

  // Recursive case:
  // n is greater than 0.
  return n * Factorial_Rec(n - 1);

  return -1; // TODO: REMOVE THIS LINE
}

/***********************************************/
/** Function Set 3 ****************************/

//! Helper function to find whether something is a consonant or not.
bool IsConsonant(char letter)
{
  if (tolower(letter) == 'a' ||
      tolower(letter) == 'e' ||
      tolower(letter) == 'i' ||
      tolower(letter) == 'o' ||
      tolower(letter) == 'u')
  {
    return false;
  }

  return true;
}

/**
CountConsonants functions
@param  string  text        The text to count the consonants within
@return int                 The amount of consonants found

Iterate through each char in the string [text] and count up 1 if that letter is a consonant.
Return the amount of consonants found.
*/
//! Count the amount of consonants in a string and return the count.
int CountConsonants_Iter(string text)
{
  int counter = 0;
  for ( size_t i = 0; i < text.size(); i++ )
  {
    if ( IsConsonant( text[i] ) )
    {
      counter++;
    }
  }
  return counter;
  return -1; // TODO: REMOVE THIS LINE
}

/**
CountConsonants functions
@param  string  text        The text to count the consonants of
@param  int     pos         The current position being investigated
@return int                 The amount of consonants found

Recurse through each char in the string [text] and count up 1 if that letter is a consonant.
Return the amount of consonants found.
*/
//! Count the amount of consonants in a string and return the count.
int CountConsonants_Rec(string text, unsigned int pos )
{
  // Terminating case:
  // No more letters to look at.
  if ( pos >= text.size() ) { return 0; }

  // Recursive case:
  // Still more letters to inspect.
  int counter = 0;
  if ( IsConsonant( text[pos] ) ) { counter++; }
  return counter + CountConsonants_Rec( text, pos+1 );

  return -1; // TODO: REMOVE THIS LINE
}

void Program()
{
  cout << "1. Alphabet function" << endl;
  cout << "2. Factorial function" << endl;
  cout << "3. Count consonants function" << endl;
  cout << endl;

  int choice;
  cout << ">> ";
  cin >> choice;

  if (choice == 1)
  {
    char start;
    char end;

    cout << "Enter starting letter and ending letter: ";
    cin >> start >> end;
    cout << endl;

    cout << "Alphabet, Iterative: ";
    string result = Alphabet_Iter(start, end);
    cout << result << endl;

    cout << endl;
    cout << "Alphabet, Recursive: ";
    result = Alphabet_Rec(start, end, "");
    cout << result << endl;
  }

  else if (choice == 2)
  {
    int n;
    cout << "Enter a value for n: ";
    cin >> n;
    cout << endl;

    cout << "Factorial, Iterative: ";
    cout << n << "! = " << Factorial_Iter(n) << endl;

    cout << endl;
    cout << "Factorial, Recursive: ";
    cout << n << "! = " << Factorial_Rec(n) << endl;
    cout << endl;
  }

  else if (choice == 3)
  {
    string text;
    cout << "Enter a word (no spaces): ";
    cin >> text;
    cout << endl;

    cout << "GetConsonants, Iterative:" << endl;
    cout << " * Consonants in " << text << ": " << CountConsonants_Iter(text) << endl;

    cout << endl;
    cout << "GetConsonants, Recursive:" << endl;
    cout << " * Consonants in " << text << ": " << CountConsonants_Rec(text) << endl;

    cout << endl;
  }
}

//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! DON'T MODIFY ANYTHING BELOW THIS POINT! !!
//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! I've written custom main code and an automated tester.
//   !! You only need to work with the functions above this.
enum class ProgramAction
{
  TEST,
  PROGRAM
};
void Tester();
int main(int argCount, char *args[])
{
  string program_name = "RECURSION";
  ProgramAction action;
  if (argCount > 1 && string(args[1]) == "test")
  {
    action = ProgramAction::TEST;
  }
  else
  {
    std::cout << "1. Run AUTOMATED TESTS" << std::endl;
    std::cout << "2. Run PROGRAM" << std::endl;
    int choice;
    std::cout << ">> ";
    std::cin >> choice;

    switch (choice)
    {
    case 1:
      action = ProgramAction::TEST;
      break;
    case 2:
      action = ProgramAction::PROGRAM;
      break;
    }
  }

  if (action == ProgramAction::TEST)
  {
    Tester();
  }
  else
  {
    program_name = "# " + program_name + " #";
    cout << endl
         << string(80, '#') << endl;
    cout << program_name << endl;
    cout << string(program_name.size(), '#') << endl
         << endl;

    // PROGRAM
    Program();
  }

  return 0;
}

// - AUTOMATED TESTER -------------------------------------------------------//
void Tester()
{
  const string GRN = "\033[0;32m";
  const string RED = "\033[0;31m";
  const string BOLD = "\033[0;35m";
  const string CLR = "\033[0m";

  cout << endl
       << "---------------------------------------------------" << endl;
  cout << " ALPHABET TESTS " << endl;
  {
    const int TOTAL_TESTS = 2;
    char in1[TOTAL_TESTS];   // inputs 1
    char in2[TOTAL_TESTS];   // inputs 2
    string exo[TOTAL_TESTS]; // expected output
    string aco[TOTAL_TESTS]; // actual output

    in1[0] = 'a';
    in2[0] = 'g';
    exo[0] = "abcdefg";

    in1[1] = 'l';
    in2[1] = 'p';
    exo[1] = "lmnop";

    for (int i = 0; i < TOTAL_TESTS; i++)
    {
      aco[i] = Alphabet_Iter(in1[i], in2[i]);

      if (aco[i] == exo[i])
      {
        // PASS
        cout << GRN << "[PASS] ";
        cout << " TEST " << i + 1 << "a, Alphabet_Iter(" << in1[i] << ", " << in2[i] << ") = " << aco[i] << endl;
      }
      else
      {
        // FAIL
        cout << RED << "[FAIL] ";
        cout << " TEST " << i + 1 << "a, Alphabet_Iter(" << in1[i] << ", " << in2[i] << ") = " << aco[i] << endl;
        cout << "   EXPECTED OUTPUT: [" << exo[i] << "]" << endl;
        cout << "   ACTUAL OUTPUT:   [" << aco[i] << "]" << endl;
      }
      cout << CLR;

      aco[i] = Alphabet_Rec(in1[i], in2[i]);

      if (aco[i] == exo[i])
      {
        // PASS
        cout << GRN << "[PASS] ";
        cout << " TEST " << i + 1 << "b, Alphabet_Rec(" << in1[i] << ", " << in2[i] << ") = " << aco[i] << endl;
      }
      else
      {
        // FAIL
        cout << RED << "[FAIL] ";
        cout << " TEST " << i + 1 << "b, Alphabet_Rec(" << in1[i] << ", " << in2[i] << ") = " << aco[i] << endl;
        cout << "   EXPECTED OUTPUT: [" << exo[i] << "]" << endl;
        cout << "   ACTUAL OUTPUT:   [" << aco[i] << "]" << endl;
      }
      cout << CLR;
    }
  }

  cout << endl
       << "---------------------------------------------------" << endl;
  cout << " FACTORIAL TESTS " << endl;
  {
    const int TOTAL_TESTS = 4;
    int in1[TOTAL_TESTS]; // inputs 1
    int exo[TOTAL_TESTS]; // expected output
    int aco[TOTAL_TESTS]; // actual output

    in1[0] = 0;
    exo[0] = 1;

    in1[1] = 1;
    exo[1] = 1;

    in1[2] = 3;
    exo[2] = 6;

    in1[3] = 4;
    exo[3] = 24;

    for (int i = 0; i < TOTAL_TESTS; i++)
    {
      aco[i] = Factorial_Iter(in1[i]);

      if (aco[i] == exo[i])
      {
        // PASS
        cout << GRN << "[PASS] ";
        cout << " TEST " << i + 1 << "a, Factorial_Iter(" << in1[i] << ") = " << aco[i] << endl;
      }
      else
      {
        // FAIL
        cout << RED << "[FAIL] ";
        cout << " TEST " << i + 1 << "a, Factorial_Iter(" << in1[i] << ") = " << aco[i] << endl;
        cout << "   EXPECTED OUTPUT: [" << exo[i] << "]" << endl;
        cout << "   ACTUAL OUTPUT:   [" << aco[i] << "]" << endl;
      }
      cout << CLR;

      aco[i] = Factorial_Rec(in1[i]);

      if (aco[i] == exo[i])
      {
        // PASS
        cout << GRN << "[PASS] ";
        cout << " TEST " << i + 1 << "b, Factorial_Rec(" << in1[i] << ") = " << aco[i] << endl;
      }
      else
      {
        // FAIL
        cout << RED << "[FAIL] ";
        cout << " TEST " << i + 1 << "b, Factorial_Rec(" << in1[i] << ") = " << aco[i] << endl;
        cout << "   EXPECTED OUTPUT: [" << exo[i] << "]" << endl;
        cout << "   ACTUAL OUTPUT:   [" << aco[i] << "]" << endl;
      }
      cout << CLR;
    }
  }

  cout << endl
       << "---------------------------------------------------" << endl;
  cout << " CONSONANT TESTS " << endl;
  {
    const int TOTAL_TESTS = 3;
    string in1[TOTAL_TESTS]; // inputs 1
    int exo[TOTAL_TESTS];    // expected output
    int aco[TOTAL_TESTS];    // actual output

    in1[0] = "aeiou";
    exo[0] = 0;

    in1[1] = "jkl";
    exo[1] = 3;

    in1[2] = "hellothere";
    exo[2] = 6;

    for (int i = 0; i < TOTAL_TESTS; i++)
    {
      aco[i] = CountConsonants_Iter(in1[i]);

      if (aco[i] == exo[i])
      {
        // PASS
        cout << GRN << "[PASS] ";
        cout << " TEST " << i + 1 << "a, CountConsonants_Iter(\"" << in1[i] << "\") = " << aco[i] << endl;
      }
      else
      {
        // FAIL
        cout << RED << "[FAIL] ";
        cout << " TEST " << i + 1 << "a, CountConsonants_Iter(\"" << in1[i] << "\") = " << aco[i] << endl;
        cout << "   EXPECTED OUTPUT: [" << exo[i] << "]" << endl;
        cout << "   ACTUAL OUTPUT:   [" << aco[i] << "]" << endl;
      }
      cout << CLR;

      aco[i] = CountConsonants_Rec(in1[i]);

      if (aco[i] == exo[i])
      {
        // PASS
        cout << GRN << "[PASS] ";
        cout << " TEST " << i + 1 << "b, CountConsonants_Rec(\"" << in1[i] << "\") = " << aco[i] << endl;
      }
      else
      {
        // FAIL
        cout << RED << "[FAIL] ";
        cout << " TEST " << i + 1 << "b, CountConsonants_Rec(\"" << in1[i] << "\") = " << aco[i] << endl;
        cout << "   EXPECTED OUTPUT: [" << exo[i] << "]" << endl;
        cout << "   ACTUAL OUTPUT:   [" << aco[i] << "]" << endl;
      }
      cout << CLR;
    }
  }
}
