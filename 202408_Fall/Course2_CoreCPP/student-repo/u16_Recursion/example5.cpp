#include <iostream>
#include <vector>
using namespace std;

char FindMin_Iter( vector<char> data )
{
  int min_index = 0;
  for ( size_t i = 1; i < data.size(); i++ )
    {
      if ( data[i] < data[min_index] )
        {
          min_index = i;
        }
    }

  return data[min_index];
}

char FindMin_Rec( vector<char> data, int i = 0 );
char FindMin_Rec( vector<char> data, int i )
{
  if ( i == data.size()-1 ) { return data[i]; }

  char recmin = FindMin_Rec( data, i+1 );
  if ( data[i] < recmin )
    {
      return data[i];
    }
  else
    {
      return recmin;
    }
}

template <typename T>
void DisplayVector( vector<T> data )
{
  cout << "  ";
  for ( size_t i = 0; i < data.size(); i++ )
    {
      if ( i != 0 ) { cout << "  "; }
      cout << i;
    }
  cout << endl;
  cout << "{ ";
  for ( size_t i = 0; i < data.size(); i++ )
    {
      if ( i != 0 ) { cout << ", "; }
      cout << data[i];
    }
  cout << " }" << endl;
}

int main()
{
  vector<char> data = { 'C', 'I', 'L', 'A', 'N', 'T', 'R', 'O' };
  char result;

  cout << "Data:" << endl;
  DisplayVector( data );

  cout << endl << endl;

  result = FindMin_Iter( data );
  cout << "Result: " << result << endl;

  cout << endl << endl;

  result = FindMin_Rec( data );
  cout << "Result: " << result << endl;

  return 0;
}
