# You may need to use:
# chmod +x git-sync.sh
# Run with:
# ./git-sync.sh

echo "** AUTO SYNC FOR GIT! **"
echo "" 
echo "NOTE: During the MERGE PROCESS it may open up a text editor."
echo "      On that screen, type :q (ENTER) to exit it and continue."
echo "      If that doesn't pop up then it's fine, everything will update automatically."
echo ""

read -p "Enter commit message (description of what you updated): " message

# NOTE: Change this to student's repo path
cd Desktop/cs200-XYZ-repo

echo "* git status (here's the changed files):"
git status

echo "* git add"
git add .

echo "* git status"
git status

echo "git commit (back up the changes):"
git commit -m "Auto: $message"

echo "* git pull (check for changes from server):"
git pull

echo "* git push (send our changes to the server):"
git push
