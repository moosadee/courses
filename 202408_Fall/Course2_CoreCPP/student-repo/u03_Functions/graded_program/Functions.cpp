#include "Functions.h" // Include our other file

// FUNCTION DEFINITIONS

// FUNCTIONS THAT TAKE INPUT AND RETURN OUTPUT
//! This function should `return` the y value, given `x`, assume the function is "f(x) = 2x+3", but write in code instead.
float f( float x )
{
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Implement me!

  // -------------------------------------------------------------------------
  return 0; // TODO: REMOVE THIS LINE OF CODE!
}

//! This function should `return` the result of the area of a rectangle that's `width` x `length`.
float GetArea( float width, float length )
{
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Implement me!

  // -------------------------------------------------------------------------
  return 0; // TODO: REMOVE THIS LINE OF CODE!
}

//! This function should `return` the result of the volume of a cube that's `width` x `length` x `height`.
float GetVolume( float width, float length, float height )
{
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Implement me!

  // -------------------------------------------------------------------------
  return 0; // TODO: REMOVE THIS LINE OF CODE!
}

//! This function should `return` the result of the perimeter of a rectangle that's `width` x `length`.
float GetPerimeter( float width, float length )
{
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Implement me!

  // -------------------------------------------------------------------------
  return 0; // TODO: REMOVE THIS LINE OF CODE!
}

//! This function should `return` the result of the math operation to find what `percent`% of `original` is.
float GetPercentOf( float original, float percent )
{
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Implement me!

  // -------------------------------------------------------------------------
  return 0; // TODO: REMOVE THIS LINE OF CODE!
}


// FUNCTIONS WITH NO INPUT BUT RETURN OUTPUT
//! This function should `return` a string literal "" with your favorite food.
string GetFavoriteFood()
{
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Implement me!

  // -------------------------------------------------------------------------
  return "NOTHING"; // TODO: REMOVE THIS LINE OF CODE!
}

//! This function should `return` a string literal "" with your favorite movie.
string GetFavoriteMovie()
{
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Implement me!

  // -------------------------------------------------------------------------
  return "NOTHING"; // TODO: REMOVE THIS LINE OF CODE!
}

//! This function should `return` a string literal "" with your favorite animal.
string GetFavoriteAnimal()
{
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Implement me!

  // -------------------------------------------------------------------------
  return "NOTHING"; // TODO: REMOVE THIS LINE OF CODE!
}


//! This function should use `cout` to display the courses you're taking this semester.
void DisplayClasses()
{
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Implement me!

  // -------------------------------------------------------------------------
}
