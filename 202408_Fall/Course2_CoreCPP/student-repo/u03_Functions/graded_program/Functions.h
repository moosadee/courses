#ifndef _FUNCTIONS
#define _FUNCTIONS

#include <string>
#include <iostream>
using namespace std;

// FUNCTION DECLARATIONS

float f( float x );
float GetArea( float width, float length );
float GetVolume( float width, float length, float height );
float GetPerimeter( float width, float length );
float GetPercentOf( float original, float percent );

string GetFavoriteFood();
string GetFavoriteMovie();
string GetFavoriteAnimal();

void DisplayClasses();

#endif
