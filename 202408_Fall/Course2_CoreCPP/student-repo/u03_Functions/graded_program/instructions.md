# Graded: Functions program

## Introduction

Within "Functions.cpp" you'll implement the FUNCTION DEFINITIONS. Within "main.cpp" you will call the functions.

## Building and running the program

- Open **terminal**
- Build the program: `g++ *.h *.cpp`
- View files: `ls`
- Run program:
  - `./a.out` (Linux/Mac)
  - `.\a.exe` (Windows)

## Example output

```
--------------------------------------------------------------------------------
AUTOMATED TESTS

[PASS] f(x) test 1
[PASS] f(x) test 2
[PASS] GetArea test 1
[PASS] GetArea test 2
[PASS] GetVolume test 1
[PASS] GetVolume test 2
[PASS] GetPerimeter test 1
[PASS] GetPerimeter test 2
[PASS] GetFavoriteFood test
[PASS] GetFavoriteMovie test
[PASS] GetFavoriteAnimal test
DisplayClasses() must be tested manually!

AUTOMATED TESTS DONE!
--------------------------------------------------------------------------------

FUNCTION 1: y(x):
Enter a value of x: 5
Result: 13

FUNCTION 2: GetArea
Enter a width: 2
Enter a length: 3
Result: 6

FUNCTION 3: GetVolume
Enter a width: 4
Enter a length: 6
Enter a height: 3
Result: 72

FUNCTION 4: GetPerimeter
Enter a width: 2
Enter a length: 3
Result: 10

FUNCTION 5: GetPercentOf
Enter a number: 10
Enter a percentage: %60
Result: 0.6

FUNCTION 6: GetFavoriteFood
Favorite food: Bibim bap

FUNCTION 7: GetFavoriteMovie
Favorite movie: The Lost Skeleton of Cadavra

FUNCTION 8: GetFavoriteAnimal
Favorite animal: Moose

FUNCTION 9: DisplayClasses
The classes I'm taking this semester are... CS 200, CS 235, CS 250
```


## Instructions

### Functions.cpp:

Start with "Functions.cpp". You will implement these functions:

- float f( float x )
- float GetArea( float width, float length )
- float GetVolume( float width, float length, float height )
- float GetPerimeter( float width, float length )
- float GetPercentOf( float original, float percent )
- string GetFavoriteFood()
- string GetFavoriteMovie()
- string GetFavoriteAnimal()
- void DisplayClasses()

Each function has comments in the code to describe what each function should do.
For functions with a NON-VOID RETURN TYPE, make sure you have the `return` keyword returning data,
such as the result of a math operation.

NOTE: The functions with non-void return types have a placeholder return, like `return 0;`.
Remove these placeholder lines of code after you implement the function.


### main.cpp:

There are several regions marked throughout main.cpp where you will call each function and then
use `cout` to display the result (except the last function).

Follow along with the comments, making each function call as described,
storing the result in the variable specified,
then using a `cout` statement to display the result back to the screen for the user.