#include "Functions.h" // Include our other file

#include <iostream>
#include <string>
using namespace std;


void AutoTester(); // Don't modify this!
int main()
{
  AutoTester(); // Don't modify this!

  float x, y, width, length, height, original, percent, area, volume, perimeter, result;
  string fav_food, fav_movie, fav_animal;

  cout << endl << "FUNCTION 1: y(x):" << endl;
  cout << "Enter a value of x: ";
  cin >> x;
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Call the function, store its result in the `y` variable.

  // TODO: Use `cout` to display the `y` variable result.
  cout << "Result: " << endl;
  // -------------------------------------------------------------------------
  
  cout << endl << "FUNCTION 2: GetArea" << endl;
  cout << "Enter a width: ";
  cin >> width;
  cout << "Enter a length: ";
  cin >> length;
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Call the function, store its result in the `area` variable.

  // TODO: Use `cout` to display the `area` variable result.  
  cout << "Result: " << endl;
  // -------------------------------------------------------------------------
  
  cout << endl << "FUNCTION 3: GetVolume" << endl;
  cout << "Enter a width: ";
  cin >> width;
  cout << "Enter a length: ";
  cin >> length;
  cout << "Enter a height: ";
  cin >> height;
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Call the function, store its result in the `volume` variable.
  
  // TODO: Use `cout` to display the `volume` variable result.
  cout << "Result: " << endl;
  // -------------------------------------------------------------------------
  
  cout << endl << "FUNCTION 4: GetPerimeter" << endl;
  cout << "Enter a width: ";
  cin >> width;
  cout << "Enter a length: ";
  cin >> length;
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Call the function, store its result in the `perimeter` variable.
  
  // TODO: Use `cout` to display the `perimeter` variable result.
  cout << "Result: " << endl;
  // -------------------------------------------------------------------------
  
  cout << endl << "FUNCTION 5: GetPercentOf" << endl;
  cout << "Enter a number: ";
  cin >> original;
  cout << "Enter a percentage: %";
  cin >> percent;
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Call the function, store its result in the `result` variable.
  
  // TODO: Use `cout` to display the `result` variable result.
  cout << "Result: " << endl;
  // -------------------------------------------------------------------------

  cout << endl << "FUNCTION 6: GetFavoriteFood" << endl;
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Call the function, store its result in the `fav_food` variable.

  // TODO: Use `cout` to display the `fav_food` to the screen.
  cout << "Favorite food: " << endl;
  // -------------------------------------------------------------------------

  cout << endl << "FUNCTION 7: GetFavoriteMovie" << endl;
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Call the function, store its result in the `fav_movie` variable.

  // TODO: Use `cout` to display the `fav_movie` to the screen.
  cout << "Favorite movie: " << endl;
  // -------------------------------------------------------------------------

  cout << endl << "FUNCTION 8: GetFavoriteAnimal" << endl;
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Call the function, store its result in the `fav_animal` variable.

  // TODO: Use `cout` to display the `fav_animal` to the screen.
  cout << "Favorite animal: " << endl;
  // -------------------------------------------------------------------------

  cout << endl << "FUNCTION 9: DisplayClasses" << endl;
  cout << "The classes I'm taking this semester are..." << endl;
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Call the DisplayClasses function. It doesn't return any data, so it will just
  //       be the function name and ().
  
  // -------------------------------------------------------------------------

  
  return 0;
}





//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! DON'T MODIFY ANYTHING BELOW THIS POINT! !!
//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

void AutoTester()
{
  cout << endl << string( 80, '-' ) << endl;
  cout << "AUTOMATED TESTS" << endl << endl;
  const string GRN = "\033[0;32m"; const string RED = "\033[0;31m"; const string BOLD = "\033[0;35m"; const string CLR = "\033[0m";

  string test_name;
  
  { test_name = "f(x) test 1";
    float input = 2;
    float exp_out = 7;
    float act_out = f( input );

    if ( act_out == exp_out ) { cout << GRN << "[PASS] " << test_name << endl; }
    else
      {
	cout << RED << "[FAIL] " << test_name << endl;
	cout << "  Function call: f(" << input << ")" << endl;
	cout << "  Expected output:  " << exp_out << endl;
	cout << "  Actual output:    " << act_out << endl;
	cout << endl;
      }
    cout << CLR;
  }
  
  { test_name = "f(x) test 2";
    float input = 3;
    float exp_out = 9;
    float act_out = f( input );

    if ( act_out == exp_out ) { cout << GRN << "[PASS] " << test_name << endl; }
    else
      {
	cout << RED << "[FAIL] " << test_name << endl;
	cout << "  Function call: f(" << input << ")" << endl;
	cout << "  Expected output:  " << exp_out << endl;
	cout << "  Actual output:    " << act_out << endl;
	cout << endl;
      }
    cout << CLR;
  }
  
  { test_name = "GetArea test 1";
    float input_w = 3, input_l = 5;
    float exp_out = 15;
    float act_out = GetArea( input_w, input_l );

    if ( act_out == exp_out ) { cout << GRN << "[PASS] " << test_name << endl; }
    else
      {
	cout << RED << "[FAIL] " << test_name << endl;
	cout << "  Function call: GetArea(" << input_w << ", " << input_l << ")" << endl;
	cout << "  Expected output:  " << exp_out << endl;
	cout << "  Actual output:    " << act_out << endl;
	cout << endl;
      }
    cout << CLR;
  }
  
  { test_name = "GetArea test 2";
    float input_w = 7, input_l = 3;
    float exp_out = 21;
    float act_out = GetArea( input_w, input_l );

    if ( act_out == exp_out ) { cout << GRN << "[PASS] " << test_name << endl; }
    else
      {
	cout << RED << "[FAIL] " << test_name << endl;
	cout << "  Function call: GetArea(" << input_w << ", " << input_l << ")" << endl;
	cout << "  Expected output:  " << exp_out << endl;
	cout << "  Actual output:    " << act_out << endl;
	cout << endl;
      }
    cout << CLR;
  }
  
  { test_name = "GetVolume test 1";
    float input_w = 3, input_l = 5, input_h = 2;
    float exp_out = 30;
    float act_out = GetVolume( input_w, input_l, input_h );

    if ( act_out == exp_out ) { cout << GRN << "[PASS] " << test_name << endl; }
    else
      {
	cout << RED << "[FAIL] " << test_name << endl;
	cout << "  Function call: GetVolume(" << input_w << ", " << input_l << ", " << input_h << ")" << endl;
	cout << "  Expected output:  " << exp_out << endl;
	cout << "  Actual output:    " << act_out << endl;
	cout << endl;
      }
    cout << CLR;
  }
  
  { test_name = "GetVolume test 2";
    float input_w = 5, input_l = 7, input_h = 3;
    float exp_out = 105;
    float act_out = GetVolume( input_w, input_l, input_h );

    if ( act_out == exp_out ) { cout << GRN << "[PASS] " << test_name << endl; }
    else
      {
	cout << RED << "[FAIL] " << test_name << endl;
	cout << "  Function call: GetVolume(" << input_w << ", " << input_l << ", " << input_h << ")" << endl;
	cout << "  Expected output:  " << exp_out << endl;
	cout << "  Actual output:    " << act_out << endl;
	cout << endl;
      }
    cout << CLR;
  }
  
  { test_name = "GetPerimeter test 1";
    float input_w = 3, input_l = 5;
    float exp_out = 16;
    float act_out = GetPerimeter( input_w, input_l );

    if ( act_out == exp_out ) { cout << GRN << "[PASS] " << test_name << endl; }
    else
      {
	cout << RED << "[FAIL] " << test_name << endl;
	cout << "  Function call: Perimeter(" << input_w << ", " << input_l << ")" << endl;
	cout << "  Expected output:  " << exp_out << endl;
	cout << "  Actual output:    " << act_out << endl;
	cout << endl;
      }
    cout << CLR;
  }

  { test_name = "GetPerimeter test 2";
    float input_w = 5, input_l = 7;
    float exp_out = 24;
    float act_out = GetPerimeter( input_w, input_l );

    if ( act_out == exp_out ) { cout << GRN << "[PASS] " << test_name << endl; }
    else
      {
	cout << RED << "[FAIL] " << test_name << endl;
	cout << "  Function call: Perimeter(" << input_w << ", " << input_l << ")" << endl;
	cout << "  Expected output:  " << exp_out << endl;
	cout << "  Actual output:    " << act_out << endl;
	cout << endl;
      }
    cout << CLR;
  }
  
  { test_name = "GetFavoriteFood test";
    string act_out = GetFavoriteFood();

    if ( act_out != "" && act_out != "NOTHING" ) { cout << GRN << "[PASS] " << test_name << endl; }
    else
      {
	cout << RED << "[FAIL] " << test_name << endl;
	cout << "  Function call: GetFavoriteFood()" << endl;
	cout << "  Expected output:  Anything besides \"NOTHING\" or \"\"!" << endl;
	cout << "  Actual output:    " << act_out << endl;
	cout << endl;
      }
    cout << CLR;
  }
  
  { test_name = "GetFavoriteMovie test";
    string act_out = GetFavoriteMovie();

    if ( act_out != "" && act_out != "NOTHING" ) { cout << GRN << "[PASS] " << test_name << endl; }
    else
      {
	cout << RED << "[FAIL] " << test_name << endl;
	cout << "  Function call: GetFavoriteMovie()" << endl;
	cout << "  Expected output:  Anything besides \"NOTHING\" or \"\"!" << endl;
	cout << "  Actual output:    " << act_out << endl;
	cout << endl;
      }
    cout << CLR;
  }
  
  { test_name = "GetFavoriteAnimal test";
    string act_out = GetFavoriteAnimal();

    if ( act_out != "" && act_out != "NOTHING" ) { cout << GRN << "[PASS] " << test_name << endl; }
    else
      {
	cout << RED << "[FAIL] " << test_name << endl;
	cout << "  Function call: GetFavoriteAnimal()" << endl;
	cout << "  Expected output:  Anything besides \"NOTHING\" or \"\"!" << endl;
	cout << "  Actual output:    " << act_out << endl;
	cout << endl;
      }
    cout << CLR;
  }

  cout << "DisplayClasses() must be tested manually!" << endl;

  cout << endl << "AUTOMATED TESTS DONE!" << endl;
  cout << string( 80, '-' ) << endl;
}
