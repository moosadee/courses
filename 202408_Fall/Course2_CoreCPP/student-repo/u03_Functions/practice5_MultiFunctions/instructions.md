# Practice: Shopping cart

## Introduction

Within "Functions.cpp" you will implement several functions and then
put them together within `Main` as a small shopping cart program.

## Building and running the program

- Open **terminal**
- Build the program: `g++ *.h *.cpp`
- View files: `ls`
- Run program:
  - `./a.out` (Linux/Mac)
  - `.\a.exe` (Windows)

## Example output

```
################################################################################
# SHOPPING CART v1 #
####################

########################################
PURCHASE CALCULATOR

                    void DisplayTotal( float price )
                    price address: 0x7ffe6064607c

Total: $0.00

                    float GetNewPrice()
                    new_price address: 0x7ffe6064608c

Enter price of new item: $5.59

                    void DisplayTotal( float price )
                    price address: 0x7ffe6064607c

Total: $5.59

                    float GetNewPrice()
                    new_price address: 0x7ffe6064608c

Enter price of new item: $4.42

                    void DisplayTotal( float price )
                    price address: 0x7ffe6064607c

Total: $10.01

                    float GetNewPrice()
                    new_price address: 0x7ffe6064608c

Enter price of new item: $3.33

# RECEIPT #######################################

                    float GetTaxPercent()

                    float GetPricePlusTax( float original_price, float tax_percent )
                    original_price address: 0x7ffe6064607c
                    tax_percent    address: 0x7ffe60646078
                    tax_decimal    address: 0x7ffe60646088
                    new_price      address: 0x7ffe6064608c

* Transaction total:
                    void DisplayTotal( float price )
                    price address: 0x7ffe6064607c

Total: $13.34

* Tax rate:
                    float GetTaxPercent()

9.61%

* After tax:
                    void DisplayTotal( float price )
                    price address: 0x7ffe6064607c

Total: $14.62

                    float Main()
                    transaction_total      address: 0x7ffe606460e4
                    new_price              address: 0x7ffe606460e8
                    final_price            address: 0x7ffe606460ec

returned: 14.62
```

## float Main()

- CALL the `DisplayTotal` function, passing in the `transaction_total` as an input argument.
- CALL the `GetNewPrice` function, storing its RETURN RESULT in the `new_price` variable.
- CALCULATE the updated price: Add `new_price` onto the `transaction_total`, and store the result in `transaction_total`.

Do this twice more so that the user enters a total of 3 prices.

Afterwards:

- CALL the `GetPricePlusTax` function, passing in `transaction_total` and `GetTaxPercent()` as its input arguments. Store its RETURN RESULT in the `final_price` variable.
- DISPLAY the text "Transaction total:" to the screen.
- CALL the `DisplayTotal` function, passing in the `transaction_total` as its input argument.
- DISPLAY the text "Tax rate:", CALL the GetTaxPercent function to retrieve and display it.
- DISPLAY the text "After tax:"
- CALL the `DisplayTotal` function, passing in the `final_price` as its input argument.
- RETURN the `final_price` at the end of the function.


## Observations

While running the program, the main program output intended for the user is aligned to the left. Then, there is "debug information" indented further out. This debug information includes the name of a function and its variables' addresses. **Notice how even if variables have the same name, if they're declared within different functions they have different addresses - they are different variables.**
