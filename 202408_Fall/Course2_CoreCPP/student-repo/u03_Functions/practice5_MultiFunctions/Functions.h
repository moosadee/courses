#ifndef _FUNCTIONS
#define _FUNCTIONS

#include <string>
using namespace std;

// Function declarations:
void Tester();
void DisplayTotal( float );
float GetTaxPercent();
float GetPricePlusTax( float, float );
float GetNewPrice();
int GetChoice( int min, int max );
float Program();

#endif
