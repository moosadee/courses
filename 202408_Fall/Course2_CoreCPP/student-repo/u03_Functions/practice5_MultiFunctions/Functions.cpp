// Use `g++ *.cpp *.h` to build this program
// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
#include <iomanip>    // Library for formatting; `setprecision`
using namespace std;  // Using the C++ STanDard libraries

#include "Functions.h"

// -- FUNCTION DEFINITION GOES HERE -------------------------------------------//

/**
   @param  price
   @return NONE
   INPUTS data and stores in `price`. Within the function,
   DISPLAY the `price` to the screen, formatted as USD.
 */
void DisplayTotal( float price )
{
  cout << string( 20, ' ' ) << "void DisplayTotal( float price )" << endl;
  cout << string( 20, ' ' ) << "price address: " << &price << endl << endl;
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Implement me!

  // -------------------------------------------------------------------------
}

/**
   @param  NONE
   @return float    Returns the tax rate, as a percent
   RETURN the value 9.61 as the tax rate.
 */
float GetTaxPercent()
{
  cout << string( 20, ' ' ) << "float GetTaxPercent()" << endl << endl;
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Implement me!

  // -------------------------------------------------------------------------
}

/**
   @param   original_price     The original price of a product
   @param   tax_percent        The sales tax %
   @return  float              Returns the updated price, including tax
   CALCULATE AND STORE: Convert the tax rate to decimal by dividing it by 100.
   CALCULATE AND STORE: Calculate the tax price by multiplying the tax decimal by the original price.
   CALCULATE AND STORE: Calculate the new price by taking the original price plus the tax price.
   RETURN the new price.
 */
float GetPricePlusTax( float original_price, float tax_percent )
{
  float tax_decimal;
  float new_price;
  cout << string( 20, ' ' ) << "float GetPricePlusTax( float original_price, float tax_percent )" << endl;
  cout << string( 20, ' ' ) << "original_price address: " << &original_price << endl;
  cout << string( 20, ' ' ) << "tax_percent    address: " << &tax_percent << endl;
  cout << string( 20, ' ' ) << "tax_decimal    address: " << &tax_decimal << endl;
  cout << string( 20, ' ' ) << "new_price      address: " << &new_price << endl << endl;
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Implement me!

  return -1; // TODO: Remove me!
  // -------------------------------------------------------------------------
}

/**
   @param   NONE
   @return  float     Returns the price of a new item
   DISPLAY a message to the screen asking the user to enter the price of a new item.
   GET KEYBOARD INPUT AND STORE what the user enters.
   RETURN the user's input.
 */
float GetNewPrice()
{
  float new_price;
  cout << string( 20, ' ' ) << "float GetNewPrice()" << endl;
  cout << string( 20, ' ' ) << "new_price address: " << &new_price << endl << endl;
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Implement me!

  return -1; // TODO: Remove me!
  // -------------------------------------------------------------------------
}

/**
   @param   NONE
   @return  float    Returns the final price plus tax
   See requirements documentation for steps
 */
float Program()
{
  cout << string( 40, '#' ) << endl;
  cout << "PURCHASE CALCULATOR" << endl;
  float transaction_total = 0;
  float new_price = 0;
  float final_price = 0;

  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Implement me!
  // Iteration 1

  // Iteration 2

  // Iteration 3
  
  // -------------------------------------------------------------------------

  cout << endl << "# RECEIPT #######################################" << endl << endl;
  final_price = GetPricePlusTax( transaction_total, GetTaxPercent() );
  cout << "* Transaction total:" << endl;
  DisplayTotal( transaction_total );
  cout << "* Tax rate:" << endl << GetTaxPercent() << "%" << endl << endl;
  cout << "* After tax:" << endl;
  DisplayTotal( final_price );

  cout << string( 20, ' ' ) << "float Main()" << endl;
  cout << string( 20, ' ' ) << "transaction_total    address: " << &transaction_total << endl;
  cout << string( 20, ' ' ) << "new_price            address: " << &new_price << endl;
  cout << string( 20, ' ' ) << "final_price          address: " << &final_price << endl;
  cout << endl;

  return final_price;
}

//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! DON'T MODIFY ANYTHING BELOW THIS POINT! !!
//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! I've written custom main code and an automated tester.
//   !! You only need to work with the functions above this.
enum class ProgramAction { TEST, PROGRAM };
void Tester();

// - PROGRAM STARTER --------------------------------------------------------//
int main( int argCount, char* args[] )
{
  // This program has to be manually tested
  string program_name = "SHOPPING CART v1";
  cout << fixed << setprecision( 2 );

  ProgramAction action = ProgramAction::PROGRAM;
  
  if ( argCount > 1 && args[1] == "test" )
    {
      action = ProgramAction::TEST;
    }
  else
    {
      std::cout << "1. Run AUTOMATED TESTS" << std::endl;
      std::cout << "2. Run PROGRAM" << std::endl;
      int choice;
      std::cout << ">> ";
      std::cin >> choice;

      switch( choice )
        {
        case 1: action = ProgramAction::TEST; break;
        case 2: action = ProgramAction::PROGRAM; break;
        }
    }
  

  if ( action == ProgramAction::TEST )
    {
      Tester();
    }
  else
    {
      program_name = "# " + program_name + " #";
      cout << endl << string( 80, '#' ) << endl;
      cout <<  program_name << endl;
      cout << string( program_name.size(), '#' ) << endl << endl;
      float result = Program();
      cout << "returned: " << result << endl;
    }

  return 0;
}


// - AUTOMATED TESTER -------------------------------------------------------//
void Tester()
{
  const string GRN = "\033[0;32m"; const string RED = "\033[0;31m"; const string BOLD = "\033[0;35m"; const string CLR = "\033[0m";

  cout << endl;
  cout << BOLD << "DisplayTotal() must be tested manually" << endl;
  cout << BOLD << "GetNewPrice() must be tested manually" << endl;
  cout << BOLD << "Main() must be tested manually" << endl;
  cout << BOLD << endl << "Run the program manually to test these out." << endl;
  cout << endl;

  string test_name;

  { test_name = "GetTaxPercent() - Returns expected value";
    float expected_output = 9.61;
    float actual_output = GetTaxPercent();

    if ( actual_output == expected_output )
    {
      cout << GRN;
      cout << "[PASS] " << test_name << endl;
    }
    else
    {
      cout << RED;
      cout << "[FAIL] " << test_name << endl;
      cout << "   Expected output: " << expected_output << endl;
      cout << "   Actual output:   " << actual_output << endl;
      cout << "Check the return value of the function!" << endl;
    }
    cout << CLR;
  }

  { test_name = "GetPricePlusTax() - Pass in values, check result (#1)";
    float input_original_price = 10.00;
    float input_tax_percent = 5.0;
    float expected_output = 10.50;
    float actual_output = GetPricePlusTax( input_original_price, input_tax_percent );

    if ( actual_output < expected_output + 0.1 &&
         actual_output > expected_output - 0.1 ) //  Account for rounding issues
    {
      cout << GRN;
      cout << "[PASS] " << test_name << endl;
    }
    else
    {
      cout << RED;
      cout << "[FAIL] " << test_name << endl;
      cout << "   original_price:  " << expected_output << endl;
      cout << "   tax_percent:     " << expected_output << endl;
      cout << "   Expected output: " << expected_output << endl;
      cout << "   Actual output:   " << actual_output << endl;
      cout << "Check the return value of the function!" << endl;
    }
    cout << CLR;
  }

  { test_name = "GetPricePlusTax() - Pass in values, check result (#2)";
    float input_original_price = 3.00;
    float input_tax_percent = 7.5;
    float expected_output = 3.225;
    float actual_output = GetPricePlusTax( input_original_price, input_tax_percent );

    if ( actual_output < expected_output + 0.1 &&
         actual_output > expected_output - 0.1 ) //  Account for rounding issues
    {
      cout << GRN;
      cout << "[PASS] " << test_name << endl;
    }
    else
    {
      cout << RED;
      cout << "[FAIL] " << test_name << endl;
      cout << "   original_price:  " << expected_output << endl;
      cout << "   tax_percent:     " << expected_output << endl;
      cout << "   Expected output: " << expected_output << endl;
      cout << "   Actual output:   " << actual_output << endl;
      cout << "Check the return value of the function!" << endl;
    }
    cout << CLR;
  }

  cout << CLR;
}
