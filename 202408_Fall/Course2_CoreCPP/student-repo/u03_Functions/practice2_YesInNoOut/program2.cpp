// PROGRAM: Practice with function - YES input, NO output
#include <iostream>
#include <iomanip>
using namespace std;

// -- FUNCTION DEFINITION GOES HERE -------------------------------------------
/**
   DisplayFormattedUSD function
   YES input parameters / NO output return
   @param price    The price to display to the screen
   Set up cout to have fixed and setprecision(2) formatting.
   Then, use cout to display the `price` parameter.
*/
// - STUDENT CODE ----------------------------------------------------------
// TODO: Implement DisplayFormattedUSD function

// -------------------------------------------------------------------------

// -- MAIN PROGRAM FUNCTION ---------------------------------------------------
/**
   main function
   Program asks the user for an amount of money.
   Afterwards, call the DisplayFormattedUSD function to display
   the user's money in USD format.
*/
int main()
{
  float user_money;
  cout << "Enter an amount of money: ";
  cin >> user_money;

  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Call DisplayFormattedUSD
  
  // -------------------------------------------------------------------------
  
  

  return 0;
}
