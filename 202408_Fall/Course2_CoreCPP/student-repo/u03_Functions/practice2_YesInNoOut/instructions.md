# Build program:
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

g++ *.cpp

# Run program:
./a.out (For Linux/Mac)
.\a.exe (For Windows)


# Example program output:
------------------------------------------
Enter an amount of money: 5.3
$5.30
------------------------------------------

# Function:
Implement the DisplayTotal function definition below.
This function HAS input parameters so its parentheses ( ) will contain: `float price`
and it has NO output return so its return type will be `void`.

Within this function, set `cout << fixed << setprecision(2);`,
and then use cout to display the `price` passed in as a dollar amount.

# main:
Within `main()`, call the `DisplayTotal` function.
The function call will just be the function name and the `user_money`
variable passed in as an argument. The function call will have a semicolon ; at the end.

# Reference
Function definition, yes inputs, no outputs:
```
void FUNCTIONNAME( TYPE1 VARIABLE1 )
{
}
```
