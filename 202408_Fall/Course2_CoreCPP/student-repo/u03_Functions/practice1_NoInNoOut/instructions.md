# Build program:
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

g++ *.cpp

# Run program:
./a.out (For Linux/Mac)
.\a.exe (For Windows)


# Example program output:
    ------------------------------------------
    1. Add item          2. Checkout
    ------------------------------------------

 # Function:
 Implement the `DisplayMenu` function definition below.
 This function has NO input parameters so its parentheses ( ) will be empty, and it has NO output return so its return type will be `void`.

 Within this function, display the numbered menu like in the example output above.

 # main:
 Within `main()`, call the `DisplayMenu` function.
 The function call will just be the function name and empty parentheses,
 with a semicolon ; at the end.

 # Reference
 Function definition, no inputs, no outputs:
 ```
 void FUNCTIONNAME()
 {
 }
 ```
