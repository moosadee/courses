// PROGRAM: Practice with function - NO input, NO output
#include <iostream>
#include <iomanip>
using namespace std;

// -- FUNCTION DEFINITION GOES HERE -------------------------------------------
/**
   DisplayMenu function
   NO input parameters / NO output return
   This function displays a numbered menu to the screen.
*/
// - STUDENT CODE ----------------------------------------------------------
// TODO: Add DisplayMenu function

// -------------------------------------------------------------------------

// -- MAIN PROGRAM FUNCTION ---------------------------------------------------
/**
   main function
   Call the DisplayMenu function inside.
*/
int main()
{
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Call DisplayMenu function
  // -------------------------------------------------------------------------
  
  

  return 0;
}
