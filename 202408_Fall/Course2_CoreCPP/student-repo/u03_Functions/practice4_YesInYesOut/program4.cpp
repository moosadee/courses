// PROGRAM: Practice with function - YES input, YES output
#include <iostream>
#include <string>
using namespace std;

// -- FUNCTION DEFINITION GOES HERE -------------------------------------------
/**
   CalculatePricePlusTax function
   NO input parameters / YES output return
   @param   original_price    The original price of an item (before tax)
   @param   tax_percent       The tax rate, in % form
   @return  float             The new price after tax is applied
   Calculates the price plus the additional price to due tax and returns the value.
*/
// - STUDENT CODE ----------------------------------------------------------
// TODO: Define the CalculatePricePlusTax function here

  // -------------------------------------------------------------------------

// -- MAIN PROGRAM FUNCTION ---------------------------------------------------
int main()
{
  float price, tax, updated_price;

  cout << "Enter price: $";
  cin >> price;
  cout << "Enter tax: %";
  cin >> tax;

  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Call the GetPricePlusTax function,
  updated_price = CalculatePricePlusTax( price, tax );
  // -------------------------------------------------------------------------
  

  cout << "Updated price: $" << updated_price << endl;

  return 0;
}
