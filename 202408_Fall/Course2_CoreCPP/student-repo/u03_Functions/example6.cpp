#include <iostream>
using namespace std;

float f( float x )
{
  return x * x;
}

int main()
{
  for ( int x = -3; x <= 3; x++ )
    {
      cout << "f(" << x << ") \t = \t " << f(x) << endl;
    }
  
  return 0;
}
