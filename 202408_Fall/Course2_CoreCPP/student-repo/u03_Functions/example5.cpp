#include <iostream>
using namespace std;

float GetSum( float x, float y )
{
  return x + y;
}

float GetDifference( float x, float y )
{
  return x - y;
}

float GetProduct( float x, float y )
{
  return x * y;
}

float GetQuotient( float x, float y )
{
  return x / y;
}


int main()
{
  float num1, num2, sum, difference, product, quotient;

  cout << "Enter num1: ";
  cin >> num1;

  cout << "Enter num2: ";
  cin >> num2;

  cout << endl;
  cout << "SUM:  " << GetSum( num1, num2 ) << endl;
  cout << "DIFF: " << GetDifference( num1, num2 ) << endl;
  cout << "PROD: " << GetProduct( num1, num2 ) << endl;
  cout << "QUOT: " << GetQuotient( num1, num2 ) << endl;

  
  return 0;
}
