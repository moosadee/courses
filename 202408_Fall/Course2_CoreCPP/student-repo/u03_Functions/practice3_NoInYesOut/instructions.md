# Build program:
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

g++ *.cpp

# Run program:
./a.out (For Linux/Mac)
.\a.exe (For Windows)



# Example program output:
------------------------------------------
Tax rate is: 9.61
------------------------------------------

# Function:
Implement the `GetTaxPercent` function.
This function has NO input parameters so its parentheses ( ) will be empty
and it has a float output RETURN so its return type is `float`.

Within this function, return the number 9.61.

# main:
Within `main()`, set the `taxRate` variable to the result from calling the `GetTaxPercent` function.
Afterward the tax rate will be displayed to the screen.

# Reference
Function definition, no inputs, yes outputs:
```
RETURNTYPE FUNCTIONNAME()
{
}
```
