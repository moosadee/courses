// PROGRAM: Practice with function - NO input, YES output
#include <iostream>
#include <string>
using namespace std;

// -- FUNCTION DEFINITION GOES HERE -------------------------------------------
/**
   GetTaxPercent function
   NO input parameters / YES output return
   @return   The tax percent is returned.
   The tax rate is 9.61.
*/
// - STUDENT CODE ----------------------------------------------------------
// TODO: Define the GetTaxPercent function here

// -------------------------------------------------------------------------

// -- MAIN PROGRAM FUNCTION ---------------------------------------------------
int main()
{
  float taxRate;

  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Call the GetTaxPercent function
  taxRate = GetTaxPercent();
  // -------------------------------------------------------------------------
  

  cout << "Tax rate is: " << taxRate;

  return 0;
}
