# Building and running the program:
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

From the terminal, use the g++ compiler to build:
`g++ *.cpp`

And then run it:
`./a.exe` (Windows)
`./a.out` (Linux/Mac)


# Example program output:
 ------------------------------------------
 LINE #0, READ IN: THE FOX AND THE GRAPES
 LINE #1, READ IN:
 LINE #2, READ IN: A hungry Fox saw some fine bunches of Grapes hanging from a vine that was
 LINE #3, READ IN: trained along a high trellis, and did his best to reach them by jumping as high
 LINE #4, READ IN: as he could into the air. But it was all in vain, for they were just out of
 LINE #5, READ IN: reach: so he gave up trying, and walked away with an air of dignity and
 LINE #6, READ IN: unconcern, remarking, "I thought those Grapes were ripe, but I see now they are
 LINE #7, READ IN: quite sour."
 ------------------------------------------


 *Example input file:*
 ------------------------------------------
 THE FOX AND THE GRAPES

 A hungry Fox saw some fine bunches of Grapes hanging from a vine that was
 trained along a high trellis, and did his best to reach them by jumping as high
 as he could into the air. But it was all in vain, for they were just out of
 reach: so he gave up trying, and walked away with an air of dignity and
 unconcern, remarking, "I thought those Grapes were ripe, but I see now they are
 quite sour."
 ------------------------------------------




# Instructions
For this program, do the following:

1. Create an input file stream (`ifstream`) variable named `input`.
2. Use the `open` function to load "fable.txt".
3. Use a while loop to read each line: `while ( getline( input, line ) )`.    
   Within the loop, do the following:
  3a. Use a `cout` statement to display the contents of `counter` and `line` to the screen, and then an `endl`.
  3b. Increment `counter` by 1.

When you run the program it will load in every line from the input text file and display it on the screen.


# Reference
Using a loop to read every line of a file:
```
while ( getline( INFILEVAR, line ) )
{
  cout << line << endl;
}
```