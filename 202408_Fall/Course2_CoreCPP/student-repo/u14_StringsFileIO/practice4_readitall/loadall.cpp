// Use `g++ *.cpp` to build this program
// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
#include <fstream>    // Library that contains `ifstream` and `ofstream`
using namespace std;  // Using the C++ STanDard libraries

int main()
{
  string line;
  int counter = 0;

  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Create an input file stream named `input` and open "fable.txt".


  // TODO: Use a while loop to read all the lines of the file, load each line into `line`.
  // TODO: Within the loop display the line number (`counter`) and the `line` read in. Also increment `counter` by 1 each time.

  // -------------------------------------------------------------------------

  return 0;
}
