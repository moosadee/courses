# Building and running the program:
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

From the terminal, use the g++ compiler to build:
`g++ *.cpp`

And then run it:
`./a.exe` (Windows)
`./a.out` (Linux/Mac)


# Example program output:
 ------------------------------------------
 LINE #0, READ IN: We're
 LINE #1, READ IN: no
 LINE #2, READ IN: strangers
 ------------------------------------------


 *Example input file:*
 ------------------------------------------
 We're no strangers to love
 You know the rules and so do I (do I)
 A full commitment's what I'm thinking of
 You wouldn't get this from any other guy

 I just wanna tell you how I'm feeling
 Gotta make you understand
 ------------------------------------------




# Instructions
This program will use an input file stream (`ifstream`) and the input stream operator (`>>`) to read in the first 3 words from the input file "lyricsA.txt".

1. Create an input file stream (=ifstream=) variable named =input=.
2. Use the ifstream's `open` function (https://cplusplus.com/reference/fstream/ifstream/open/) to open the file "lyricsA.txt".
3. Use the `>>` operator to read 1 item from the =input= file to the =read= string variable.
4. Use `cout` to display the contents of the `counter` and `word` variables.
5. Do steps 3 and 4 two more times (same exact code).

Once you run the program it will show the first 3 words of the text file read in and displayed to the screen.


# Reference
Declaring an input file stream:
`ifstream INFILEVAR;`

Opening a file to read:
`INFILEVAR.open( "FILE.EXT" );`

Reading one word from a file to a variable:
`INFILEVAR >> variable;`