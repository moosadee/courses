// Use `g++ *.cpp` to build this program
// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
#include <fstream>    // Library that contains `ifstream` and `ofstream`
using namespace std;  // Using the C++ STanDard libraries

int main()
{
  string word;
  int counter = 0;

  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Create an input file stream variable named `input`

  // TODO: Open "lyricsA.txt" into the input file stream variable

  counter++;
  // TODO: Read once from `input`, storing it in the `word` variable.
  // TODO: Use cout to display the `counter` value and `word` read in.


  counter++;
  // TODO: Read once from `input`, storing it in the `word` variable.
  // TODO: Use cout to display the `counter` value and `word` read in.


  counter++;
  // TODO: Read once from `input`, storing it in the `word` variable.
  // TODO: Use cout to display the `counter` value and `word` read in.
  // -------------------------------------------------------------------------

  return 0;
}
