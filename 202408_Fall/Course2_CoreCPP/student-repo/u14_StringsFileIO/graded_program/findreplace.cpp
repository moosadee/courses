// Use `g++ *.cpp` to build this program
// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
#include <iomanip>    // Library for formatting; `setprecision`
#include <fstream>    // Library for using `ifstream` and `ofstream`
using namespace std;  // Using the C++ STanDard libraries

// - STUDENT CODE -----------------------------------------------------------//
/**
   @param    filename       The filename to open to modify
   @param    find_me        The string being searched for in the document
   @param    replace_text   The replacement string to be used to overwrite `find` text
 */
void ModifyFile( string filename, string find_me, string replace_text )
{
  string line;
  ifstream input( filename );
  ofstream output( "edit-" + filename );
  int found_index;

  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Read every line of the input file using getline, read into the `line` variable.
  // Within the while loop, do the following:
  //   a. Try to find `find_me` in the `line` string. Store result in the `found_index` variable.
  //   b. If the substring is found then replace the text at `found_index`, with the size `find_me.size()`, replacing it with the `replace_text`.
  //   c. Output the updated `line` to the `output` text file.

  // -------------------------------------------------------------------------

  cout << "Created modified file: edit-" << filename << "." << endl;
}

void Program()
{
  string filename, find, replace;
  cin.ignore();

  cout << "Enter name of file to modify: ";
  getline( cin, filename );

  cout << "Enter text to find: ";
  getline( cin, find );

  cout << "Enter text to replace it with: ";
  getline( cin, replace );

  ModifyFile( filename, find, replace );
}

//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! DON'T MODIFY ANYTHING BELOW THIS POINT! !!
//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! I've written custom main code and an automated tester.
//   !! You only need to work with the functions above this.
enum class ProgramAction { TEST, PROGRAM };
void Tester();
int main( int argCount, char* args[] )
{
  string program_name = "SEARCH AND REPLACE";
  ProgramAction action;
  if ( argCount > 1 && string( args[1] ) == "test" )
    {
      action = ProgramAction::TEST;
    }
  else
    {
      std::cout << "1. Run AUTOMATED TESTS" << std::endl;
      std::cout << "2. Run PROGRAM" << std::endl;
      int choice;
      std::cout << ">> ";
      std::cin >> choice;

      switch( choice )
        {
        case 1: action = ProgramAction::TEST; break;
        case 2: action = ProgramAction::PROGRAM; break;
        }
    }

  if ( action == ProgramAction::TEST )
    {
      Tester();
    }
  else
    {
      program_name = "# " + program_name + " #";
      cout << endl << string( 80, '#' ) << endl;
      cout << program_name << endl;
      cout << string( program_name.size(), '#' ) << endl << endl;

      // PROGRAM
      Program();
    }

  return 0;
}

// - AUTOMATED TESTER -------------------------------------------------------//
void Tester()
{
  const string GRN = "\033[0;32m"; const string RED = "\033[0;31m"; const string BOLD = "\033[0;35m"; const string CLR = "\033[0m";

  { // Test 1
  ofstream testfile( "test.txt" );
  testfile << "the quick brown fox jumps over the lazy dog." << endl;
  testfile << "the fox and the grapes." << endl;
  testfile << "the bear and the fox." << endl;
  testfile.close();

  ModifyFile( "test.txt", "fox", "mouse" );

  string expected_result[3] = {
    "the quick brown mouse jumps over the lazy dog.",
    "the mouse and the grapes.",
    "the bear and the mouse."
  };

  ifstream verifyfile( "edit-test.txt" );
  string actual_result[3];
  getline( verifyfile, actual_result[0] );
  getline( verifyfile, actual_result[1] );
  getline( verifyfile, actual_result[2] );

  bool all_match = true;
  for ( int i = 0; i < 3; i++ )
    {
      if ( actual_result[i] != expected_result[i] )
        {
          all_match = false;
        }
    }

  if ( all_match )
    {
      cout << GRN << "[PASS] ";
      cout << " TEST 1, ModifyFile( \"test.txt\", \"me\", \"you\" )" << endl;
    }
  else
    {
      cout << RED << "[FAIL] ";
      cout << " TEST 1, ModifyFile( \"test.txt\", \"me\", \"you\" )" << endl;
      cout << "   EXPECTED OUTPUT: [\"" << expected_result[0] << "\", \"" << expected_result[1] << "\", \"" << expected_result[2] << "\"]" << endl;
      cout << "   ACTUAL OUTPUT:   [\"" << actual_result[0] << "\", \""   << actual_result[1]   << "\", \"" << actual_result[2]   << "\"]" << endl;
    }

  cout << CLR;
  }


  { // Test 2
  ofstream testfile( "test.txt" );
  testfile << "nobody likes me" << endl;
  testfile << "everybody hates me" << endl;
  testfile << "guess I'll just eat worms" << endl;
  testfile.close();

  ModifyFile( "test.txt", "me", "you" );

  string expected_result[3] = {
    "nobody likes you",
    "everybody hates you",
    "guess I'll just eat worms"
  };

  ifstream verifyfile( "edit-test.txt" );
  string actual_result[3];
  getline( verifyfile, actual_result[0] );
  getline( verifyfile, actual_result[1] );
  getline( verifyfile, actual_result[2] );

  bool all_match = true;
  for ( int i = 0; i < 3; i++ )
    {
      if ( actual_result[i] != expected_result[i] )
        {
          all_match = false;
        }
    }

  if ( all_match )
    {
      cout << GRN << "[PASS] ";
      cout << " TEST 2, ModifyFile( \"test.txt\", \"me\", \"you\" )" << endl;
    }
  else
    {
      cout << RED << "[FAIL] ";
      cout << " TEST 2, ModifyFile( \"test.txt\", \"me\", \"you\" )" << endl;
      cout << "   EXPECTED OUTPUT: [\"" << expected_result[0] << "\", \"" << expected_result[1] << "\", \"" << expected_result[2] << "\"]" << endl;
      cout << "   ACTUAL OUTPUT:   [\"" << actual_result[0] << "\", \""   << actual_result[1]   << "\", \"" << actual_result[2]   << "\"]" << endl;
    }

  cout << CLR;
  }

}
