# Introduction

This program will allow the user to specify a text file, some text to find, and a second string of text to replace it with.

# Building and running the program
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

- Open **terminal**
- Build the program: `g++ *.cpp`
- View files: `ls`
- Run program:
  - `./a.out` (Linux/Mac)
  - `.\a.exe` (Windows)


# Example output
*Program output:*
  1. Run AUTOMATED TESTS
  2. Run PROGRAM
  >> 2

  ################################################################################
  # SEARCH AND REPLACE #
  ######################

  Enter name of file to modify: fable.txt
  Enter text to find: Fox
  Enter text to replace it with: Box
  Created modified file: edit-fable.txt.

*Tests passing:*
  1. Run AUTOMATED TESTS
  2. Run PROGRAM
  >> 1
  Created modified file: edit-test.txt.
  [PASS]  TEST 1, ModifyFile( "test.txt", "me", "you" )
  Created modified file: edit-test.txt.
  [PASS]  TEST 2, ModifyFile( "test.txt", "fox", "mouse" )


*Example input file:*
 ------------------------------------------
 THE FOX AND THE GRAPES

A hungry Fox saw some fine bunches of Grapes hanging from a vine that was 
trained along a high trellis, and did his best to reach them by jumping as high 
as he could into the air. But it was all in vain, for they were just out of 
reach: so he gave up trying, and walked away with an air of dignity and 
unconcern, remarking, "I thought those Grapes were ripe, but I see now they are 
quite sour."
 ------------------------------------------

*Example output file:*
 ------------------------------------------
 THE FOX AND THE GRAPES

A hungry Box saw some fine bunches of Grapes hanging from a vine that was 
trained along a high trellis, and did his best to reach them by jumping as high 
as he could into the air. But it was all in vain, for they were just out of 
reach: so he gave up trying, and walked away with an air of dignity and 
unconcern, remarking, "I thought those Grapes were ripe, but I see now they are 
quite sour."
 ------------------------------------------


# Updating ModifyFile

This function takes in three parameters:
1. `string filename`, the file to load in
2. `string find_me`, the text to search for in each line
3. `string replace_text`, the test to replace the found item with

After the `input` ifstream and `output` ofstream are created, create a while loop. Use getline to read each line of `input` into the `line` variable. Within the loop do the following:
  a. Try to find `find_me` in the `line` string. Store result in the       `found_index` variable.
  b. If the substring is found then replace the text at `found_index`, with the size `find_me.size()`, replacing it with the `replace_text`.
  c. Output the updated `line` to the `output` text file.

Make sure to check the output files generated to verify your work manually.