# Building and running the program:
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

From the terminal, use the g++ compiler to build:
`g++ *.cpp`

And then run it:
`./a.exe` (Windows)
`./a.out` (Linux/Mac)


# Example program output:
 ------------------------------------------
 = GRADE CRUNCHER =
 How much did you score on test 1 (out of 50?): 45
 How much did you score on test 2 (out of 25?): 20
 How much did you score on test 3 (out of 40?): 10
 Info saved to score.txt
 ------------------------------------------


 *Example input file:*
 ------------------------------------------
 45.00 out of 50.00 (90.00%)
 20.00 out of 25.00 (80.00%)
 10.00 out of 40.00 (25.00%)
 ------------------------------------------




# Instructions
 For this program the user will enter 3 test scores (already implemented).
 Next, ratios are calculated for each score, to give a grade percent for each one (already implemented).

 After this, we want to *create an output file* and write the test statistics out to it.

 Do the following:

 1. Create an output file stream (=ofstream=) variable named =output=.
 2. Use the ofstream's `open` function (https://cplusplus.com/reference/fstream/ofstream/open/) to open the file "score.txt".
 3. Set up text formatting to `fixed` and `setprecision(2)`.
 Instead of applying this to the `cout` operation, it should be applied to the `output` stream: `ofstream << fixed << setprecision(2);`

 4. For each of the scores, write the following to the `output` file.
 For example:
   [NUMBER]. [TESTSCORE] out of [TESTTOTAL] ([RATIO]%)
 Replace the items in [] with the appropriate variables.

 When the program is run the details will be calculated and written to an output file.
 Check the "score.txt" file on your computer after running the program to make sure its output looks like the example above.


# Reference
Declaring an output file stream:
`ofstream OUTFILEVAR;`

Opening a file to write:
`OUTFILEVAR.open( "FILE.EXT" );`

Writing out to a file:
`OUTFILEVAR << variable;`
`OUTFILEVAR << "text";`
`OUTFILEVAR << "text" << variable << endl;`