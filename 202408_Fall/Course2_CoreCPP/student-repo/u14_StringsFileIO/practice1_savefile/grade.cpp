// Use `g++ *.cpp` to build this program
// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
#include <fstream>    // Library that contains `ifstream` and `ofstream`
#include <iomanip>    // Library for formatting; `setprecision`
using namespace std;  // Using the C++ STanDard libraries

int main()
{
  float test1total = 50, test2total = 25, test3total = 40;
  float test1score, test2score, test3score;
  float ratio1, ratio2, ratio3;

  cout << "= GRADE CRUNCHER =" << endl;

  // Get user's test scores
  cout << "How much did you score on test 1 (out of " << test1total << "?): ";
  cin >> test1score;
  cout << "How much did you score on test 2 (out of " << test2total << "?): ";
  cin >> test2score;
  cout << "How much did you score on test 3 (out of " << test3total << "?): ";
  cin >> test3score;

  // Calculate score ratios
  ratio1 = test1score / test1total * 100;
  ratio2 = test2score / test2total * 100;
  ratio3 = test3score / test3total * 100;

  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Create an output file stream variable named `output`

  // TODO: Open "score.txt" with your `output` variable

  // output << fixed << setprecision( 2 ); // TODO: Uncomment me out!

  // TODO: Output `test1score`, `test1total`, and `ratio1` to the output file.
  // TODO: Output `test2score`, `test2total`, and `ratio2` to the output file.
  // TODO: Output `test3score`, `test3total`, and `ratio3` to the output file.

  // -------------------------------------------------------------------------
  cout << "Info saved to score.txt" << endl;

  return 0;
}
