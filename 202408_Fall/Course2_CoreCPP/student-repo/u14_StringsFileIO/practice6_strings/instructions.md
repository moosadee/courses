# Building and running the program:
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

From the terminal, use the g++ compiler to build:
`g++ *.cpp`

And then run it:
`./a.exe` (Windows)
`./a.out` (Linux/Mac)


# Example program output:
   TEXT:
   ------------------------------------------------------------------------
   the quick brown fox jumps over the lazy dog
   ------------------------------------------------------------------------

   - INFO --------
   1. Get string length
   2. Get letter #
   3. Find substring
   4. Compare strings
   5. String relations

   - MODIFY --------
   6. Combine strings
   7. Insert string
   8. Erase from string
   9. Replace in string

   0. QUIT

*Option 1:*
   The length of the string is... 43

*Option 2:*
   Enter an index: 4
   Letter #4 is... q

*Option 3:*
   Enter text to search for: jump
   String found at position... 20

*Option 4:*
   Enter a second string: the slow tortoise
   Comparison result... -2

*Option 5:*
   Enter a second string: the slow tortoise
   Comparison result... <

*Option 6:*
   Enter a second string: , whee!
   Text is now: the quick brown fox jumps over the lazy dog, whee!

*Option 7:*
   Enter a second string: slow
   Enter an index to insert the string: 4
   Text is now: the slowquick brown fox jumps over the lazy dog, whee!

*Option 8:*
   Enter an index to begin removing from: 8
   Enter # of characters to remove: 5
   Text is now: the slow brown fox jumps over the lazy dog, whee!

*Option 9:*
   Enter an index to begin removing from: 9    
   Enter # of characters to remove: 5
   Enter string to add in its place: purple
   Text is now: the slow purple fox jumps over the lazy dog, whee!


# Instructions
Within the set of if/else if statements in main() you'll utilize several functions from the string library. Utilize the documentation below for example code and explanations of how the functions work.


# Reference
1. size function: 
   https://cplusplus.com/reference/string/string/size/

2. Subscript operator: 
   https://cplusplus.com/reference/string/string/operator[]/

3. find function:
   https://cplusplus.com/reference/string/string/find/

4. compare function:
   https://cplusplus.com/reference/string/string/compare/

5. Relational operators:
   https://cplusplus.com/reference/string/string/operators/

6. Concatenation operator:
   https://cplusplus.com/reference/string/string/operator+/

7. insert function:
   https://cplusplus.com/reference/string/string/insert/

8. erase function:
   https://cplusplus.com/reference/string/string/erase/

9. replace function:
   https://cplusplus.com/reference/string/string/replace/