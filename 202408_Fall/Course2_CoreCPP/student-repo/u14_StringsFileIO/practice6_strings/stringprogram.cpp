// Use `g++ *.cpp` to build this program
// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
using namespace std;  // Using the C++ STanDard libraries

int main()
{
  bool program_running = true;
  string text = "the quick brown fox jumps over the lazy dog";

  while ( program_running )
  {
    cout << "TEXT:" << endl << string( 80, '-' ) << endl << "\033[3;31;40m";
    cout << text << endl;
    cout << "\033[0m" << string( 80, '-' ) << endl;

    cout << endl << "- INFO --------" << endl;
    cout << "1. Get string length" << endl;
    cout << "2. Get letter #" << endl;
    cout << "3. Find substring" << endl;
    cout << "4. Compare strings" << endl;
    cout << "5. String relations" << endl;

    cout << endl << "- MODIFY --------" << endl;
    cout << "6. Combine strings" << endl;
    cout << "7. Insert string" << endl;
    cout << "8. Erase from string" << endl;
    cout << "9. Replace in string" << endl;

    cout << endl << "0. QUIT" << endl << endl;
    cout << ">> ";
    int choice;
    cin >> choice;

    // - STUDENT CODE ----------------------------------------------------------
    if ( choice == 0 )
    {
      program_running = false;
    }
    else if ( choice == 1 ) // Get string length 
    {
      int length{0};

      // TODO: Use the size function


      cout << "The length of the string is... " << length << endl;
    }
    else if ( choice == 2 ) // Get letter #
    {
      int index;
      char letter_at_index = ' ';

      cout << "Enter an index: ";
      cin >> index;

      // TODO: Use the subscript operator


      cout << "Letter #" << index << " is... " << letter_at_index << endl;
    }
    else if ( choice == 3 ) // Find substring
    {
      string search_term;
      size_t found_index;

      cout << "Enter text to search for: ";
      cin.ignore();
      getline( cin, search_term );

      // TODO: Use the find function
      

      if ( found_index == string::npos )
      {
        cout << "String not found." << endl;
      }
      else
      {
        cout << "String found at position... " << found_index << endl;
      }
    }
    else if ( choice == 4 ) // Compare strings
    {
      string second_string;
      int compare_result{0};

      cout << "Enter a second string: ";
      cin.ignore();
      getline( cin, second_string );

      // TODO: Use the compare function
      

      cout << "Comparison result... " << compare_result << endl;
    }
    else if ( choice == 5 ) // String relations
    {
      string second_string;
      string compare_result{""};

      cout << "Enter a second string: ";
      cin.ignore();
      getline( cin, second_string );

      // TODO: Create an if/else if statement using <, >, and ==
      

      cout << "Comparison result... " << compare_result << endl;
    }
    else if ( choice == 6 ) // Combine strings
    {
      string second_string;

      cout << "Enter a second string: ";
      cin.ignore();
      getline( cin, second_string );

      // TODO: Use the concatenation operator +
      

      cout << "Text is now: " << text << endl;
    }
    else if ( choice == 7 ) // Insert string
    {
      string second_string;
      int index;

      cout << "Enter a second string: ";
      cin.ignore();
      getline( cin, second_string );

      cout << "Enter an index to insert the string: ";
      cin >> index;

      // TODO: Use the insert function
      

      cout << "Text is now: " << text << endl;
    }
    else if ( choice == 8 ) // Erase from string
    {
      int index, length;

      cout << "Enter an index to begin removing from: ";
      cin >> index;

      cout << "Enter # of characters to remove: ";
      cin >> length;

      // TODO: Use the erase function
      

      cout << "Text is now: " << text << endl;
    }
    else if ( choice == 9 ) // Replace in string
    {
      string second_string;
      int index, length;

      cout << "Enter an index to begin removing from: ";
      cin >> index;

      cout << "Enter # of characters to remove: ";
      cin >> length;

      cout << "Enter string to add in its place: ";
      cin.ignore();
      getline( cin, second_string );

      // TODO: Use the replace function
      

      cout << "Text is now: " << text << endl;
    }
    // -------------------------------------------------------------------------

    cout << endl << "PRESS ENTER TO CONTINUE." << endl;
    string a;
    getline( cin, a );
    getline( cin, a );
  }

  return 0;
}
