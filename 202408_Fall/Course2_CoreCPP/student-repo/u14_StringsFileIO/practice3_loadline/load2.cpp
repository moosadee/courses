// Use `g++ *.cpp` to build this program
// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
#include <fstream>    // Library that contains `ifstream` and `ofstream`
using namespace std;  // Using the C++ STanDard libraries

int main()
{
  string line;
  int counter = 0;

  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Create an input file stream variable named `input`
  ifstream input;

  // TODO: Open "lyricsA.txt" into the input file stream variable

  counter++;
  // TODO: Use getline to read one item from `input` to the `line` variable
  // TODO: Use cout to display the `counter` value and `line` read in.


  counter++;
  // TODO: Use getline to read one item from `input` to the `line` variable
  // TODO: Use cout to display the `counter` value and `line` read in.


  counter++;
  // TODO: Use getline to read one item from `input` to the `line` variable
  // TODO: Use cout to display the `counter` value and `line` read in.
  // -------------------------------------------------------------------------

  return 0;
}
