# Building and running the program:
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

From the terminal, use the g++ compiler to build:
`g++ *.cpp`

And then run it:
`./a.exe` (Windows)
`./a.out` (Linux/Mac)


# Example program output:
 ------------------------------------------
 LINE #0, READ IN: We're no strangers to love
 LINE #1, READ IN: You know the rules and so do I (do I)
 LINE #2, READ IN: A full commitment's what I'm thinking of
 ------------------------------------------


 *Example input file:*
 ------------------------------------------
 We're no strangers to love
 You know the rules and so do I (do I)
 A full commitment's what I'm thinking of
 You wouldn't get this from any other guy

 I just wanna tell you how I'm feeling
 Gotta make you understand
 ------------------------------------------




# Instructions
 For this program, copy what you have for practice 2 (`practice2_loadsingle/load1.cpp`).
 Instead of using the `>>` operator to read in from the `input` file, we'll use the `getline` function instead.

 Each time you have `input >> word`, replace that line instead with `getline( input, line );`.

 Once you run the program, it will show three lines of text read in.


# Reference
Declaring an input file stream:
`ifstream INFILEVAR;`

Opening a file to read:
`INFILEVAR.open( "FILE.EXT" );`

Reading one line from a file to a variable:
`getline( INFILEVAR, variable );`