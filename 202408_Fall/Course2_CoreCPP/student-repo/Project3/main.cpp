#include <iostream>
#include <vector>
#include <string>
using namespace std;

#include "Functions.h"

int main()
{
    // TODO: Create any arrays/vectors here that you need for your program

    SetupStarterItems( /* TODO: Need to pass your vector here! */);

    bool running = true;
    while ( running )
    {
        // MAIN MENU
        cout << "0. QUIT" << endl;
        cout << "---------------" << endl;
        cout << "1. Add" << endl;
        cout << "2. Edit" << endl;
        cout << "3. Display" << endl;
        cout << endl;
        int choice = GetValidInput( 1, 3 );

        if ( choice == 0 ) // Quit program
        {
            running = false;
        }

        else if ( choice == 1 )
        {
            Add( /* TODO: Need to pass your vector here! */ );
        }

        else if ( choice == 2 )
        {
            Edit( /* TODO: Need to pass your vector here! */ );
        }

        else if ( choice == 3 )
        {
            DisplayAll( /* TODO: Need to pass your vector here! */ );
        }
    }

    return 0;
}