// PROGRAM: Practice with accessing variable's memory address

#include <iostream>
#include <string>
using namespace std;

int main()
{
  bool bool1, bool2, bool3, bool4, bool5;
  int int1, int2, int3, int4, int5;

  cout << "int1's address is: " << &int1 << endl;
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Display the address of each int variable
  
  // -------------------------------------------------------------------------
  cout << endl;

  cout << "bool1's address is: " << &bool1 << endl;
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Display the address of each bool variable
  
  // -------------------------------------------------------------------------
  cout << endl;


  return 0;
}
