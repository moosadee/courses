# Building the program:
```
g++ *.cpp
```

# Running the program:
```
./a.out
```

# Example output:
```
int1's address is: 0x61ff04
int2's address is: 0x61ff00
int3's address is: 0x61fefc
int4's address is: 0x61fef8
int5's address is: 0x61fef4

bool1's address is: 0x61ff0f
bool2's address is: 0x61ff0e
bool3's address is: 0x61ff0d
bool4's address is: 0x61ff0c
bool5's address is: 0x61ff0b
```

# Information:
We use the `&` (address-of) operator to get the memory address where a variable is stored. For this program, we're going to investigate the addresses of the variables.