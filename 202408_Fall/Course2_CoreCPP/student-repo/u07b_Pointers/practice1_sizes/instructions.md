# Building the program:
```
g++ *.cpp
```

# Running the program:
```
./a.out
```

# Example output:
```
integer size:  4
float size:    4
double size:   8
bool size:     1
char size:     1
string size:   24
```

# Information:
We can use C++'s `sizeof( TYPE )` function to see how many bytes each data type takes up. Everything eventually boils down to binary numbers, and 1 byte = 8 bits (8 binary numbers).