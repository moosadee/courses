// PROGRAM: Practice with getting variable sizes

#include <iostream>
#include <string>
using namespace std;

int main()
{
  cout << "integer size:  " << sizeof( int ) << endl;
  
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Use the sizeof(...) function on int, float, double, bool, char, and string
  
  // -------------------------------------------------------------------------
  cout << endl;

  return 0;
}
