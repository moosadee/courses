# Building the program:
```
g++ *.cpp
```

# Running the program:
```
./a.out
```

# Example output:
```
studentA address: 0x61fef0, value: Luna
studentB address: 0x61fed8, value: Kabe
studentC address: 0x61fec0, value: Korra

ptrStudent is now pointing to: 0
ptrStudent is now pointing to address: 0x61fef0
ptrStudent is now pointing to address: 0x61fed8
ptrStudent is now pointing to address: 0x61fec0
```

# Information:
A pointer-variable is another type of variable... except its VALUE isn't an integer or character or float, it's a memory address!

To declare a pointer, we use the datatype of the thing it's going to /point to/, plus an asterisk to mark it as a pointer, then give it a name. The * can be attached to the data type or the variable name or neither, but I prefer the first way here:

```
int* integerPointer;
float * floatPointer;
string *stringPointer;
```

To assign a pointer-variable the /address/ of a different variable, we prefix the variable's name with the `&` address-of operator:

```
integerPointer = &someInteger;
```
