# Building the program:
```
g++ *.cpp
```

# Running the program:
```
./a.out
```

# Example output:
```
ORIGINAL TABLE
studentA address: 0x61fef0, value: Luna
studentB address: 0x61fed8, value: Kabe
studentC address: 0x61fec0, value: Korra

ptrStudent is pointing to address: 0

ptrStudent is now pointing to address: 0x61fef0
CURRENT VALUE: Luna
Enter a new name: Pixel

ptrStudent is now pointing to address: 0x61fed8
CURRENT VALUE: Kabe
Enter a new name: Daisy

ptrStudent is now pointing to address: 0x61fec0
CURRENT VALUE: Korra
Enter a new name: Buddy


UPDATED TABLE
studentA address: 0x61fef0, value: Pixel
studentB address: 0x61fed8, value: Daisy
studentC address: 0x61fec0, value: Buddy
```

# Information:

To assign a pointer-variable the /address/ of a different variable, we prefix the variable's name with the `&` address-of operator:

```
integerPointer = &someInteger;
```

You can then *de-reference* a pointer to access the value at the address it's pointing to. You can display values, overwrite values, and more - indirectly, through the pointer.

```
*integerPointer = 1000;
cout << "someInteger is now " << *integerPointer << endl;
```