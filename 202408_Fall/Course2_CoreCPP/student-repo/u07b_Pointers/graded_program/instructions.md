# Building the program:
```
g++ *.cpp
```

# Running the program:
```
./a.out
```

# Example output:
```
1. Run AUTOMATED TESTS
2. Run PROGRAMS
>> 1
2024-01-U09-P1-TEST; STUDENT: Your Name, Spring 2024
[PASS]  TEST 1, StudentCode(1) = Hikaru
[PASS]  TEST 2, StudentCode(2) = Umi
[PASS]  TEST 3, StudentCode(3) = Fuu
[PASS]  TEST 4, StudentCode(4) = UNKNOWN
```

```
1. Run AUTOMATED TESTS
2. Run PROGRAMS
>> 2
Enter ID of VIP student: 3
RESULT: Fuu
```

# Information:
A pointer-variable is another type of variable... except its VALUE isn't an integer or character or float, it's a memory address!

To declare a pointer, we use the datatype of the thing it's going to /point to/, plus an asterisk to mark it as a pointer, then give it a name. The * can be attached to the data type or the variable name or neither, but I prefer the first way here:

```
int* integerPointer;
float * floatPointer;
string *stringPointer;
```

To assign a pointer-variable the /address/ of a different variable, we prefix the variable's name with the `&` address-of operator:

```
integerPointer = &someInteger;
```
