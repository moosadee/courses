# Project 2, CS 200 Summer 2024

## About

Choose one program to implement out of the list of the program ideas below. You will implement the program from scratch. Please save your project code in the Project2 folder in your repository.

For this project you will need to use variables, cin, cout, functions, and structs.

Remember that you can refer to the "C++ quick reference" portion of the textbook for a quick lookup of how to declare/define functions and structs.


## Hints and tips
Building a multi-file project

g++ *.h *.cpp


### Starter code

In your Project2 folder I've added some starter files: main.cpp, Struct.h, Functions.h, Functions.cpp. You can rename "Struct.h" to something more descriptive.


### Header files

Your header files should all have file guard with unique labels:

```
#ifndef _LABEL
#define _LABEL

// Your struct and function declarations go here

#endif
```

## C++ Best Practices to adhere to

-    Struct Declarations should go in their own .h files. The name of the file should reflect the name of the struct. (e.g., "Room.h")
-    Function Declarations should go in a .h file ("Functions.h")
-    Function Definitions should go in a .cpp file ("Functions.cpp") - don't put the definitions in your main()'s file.


## User interface and code quality

See the "Code and UI style guide" in the reference part of the textbook for all the details.

The UI should be clear to a user who has never seen your program before. Keep in mind things like:

-    Telling a user what kind of data they're entering before getting their input.
-    Spacing out different sections of the program with additional cout << endl; s.

Code should be clean and readable:

-    Use consistent indentation. If you have trouble doing this manually, let VS Code do it automatically. Go to View > Command Palette, then type "Format Document". This command will automatically clean up the code.
-    Use descriptive names for functions, variables, and structs.
-    Use vertical spacing to separate sections of code, as needed.


## Code requirements
-   **Do not use future topics (arrays, loops, branching, classes) for this assignment; I am trying to verify your knowledge of the current topics.**
1.    At least one struct created to represent an object.
     -   The struct should have at least three member variables, they can be any data type.
2.    Use functions to reduce duplicate code:
     -   Getting similar input over and over again.
     -   Doing common calculations over and over again.
     -   Formatting menu output in a consistent manner.
     -   Displaying Struct information in a consistent manner (e.g., void DisplayThing( Thing input_thing );)
-        Setting up a Struct's information (e.g., Thing SetupThing( string a, int b, float c );)
3.    You should have at least one function that takes in input parameter(s) and returns output.
     -   E.g., this could be a function that takes in values for a new Struct object, and returns the built Struct afterwards.
     -   Could be used to take in some number values and do a computation, returning the result.



## Program options

1. Restaurant program: Create a program that utilizes a struct to represent an item off the menu, such as its name, price, and calorie count. Ask the user to enter how much of each item they want to order. Calculate the total price and total calories at the end.

Functions can be used to setup each item struct, take in an item struct and display its info, get user input on how many of each order to get, to calculate the total price/calories given x orders of y item.



## Example output:
```
--------------------------------------------------------------------------------
BURGERZ-N-FRIEZ
--------------------------------------------------------------------------------
MENU

$5.99 ... Hamburger (590 calories)
Order how many of these? 2

$5.69 ... Chicken Nuggets (470 calories)
Order how many of these? 1

$9.49 ... Apple Pecan Salad (540 calories)
Order how many of these? 1

--------------------------------------------------------------------------------
RECEIPT
* 2 x Hamburger @ 11.98
* 1 x Chicken Nuggets @ 5.69
* 1 x Apple Pecan Salad @ 9.49

TOTAL PRICE:    $27.16
TOTAL CALORIES: 2190

Thank you!
```


2. Movie Showtimes: Create a program that utilizes a struct to store a Movie Showtime. This may include something like the title, rating, showtime, and ticket price. Set up two movie showtimes with hard-coded values, then ask the user to enter information for the last movie (title/rating/showtime/ticket price). At the end, show all the movie information.

Functions can be used to setup each Movie struct, take in a Movie struct and display its info, get user input for each type of data.

Example output:

```
--------------------------------------------------------------------------------
A MOVIE CINEMA - DASHBOARD
--------------------------------------------------------------------------------
NEW MOVIE
* Title: Run Lola Run
* Rating (G, PG, PG13, R): R
* Showtime (00 - 23): 15
* Ticket price (x.xx): $ 7.50

--------------------------------------------------------------------------------
AVAILBLE MOVIES
Lord of the Rings (PG13), $15.00, START: 13
When Marnie Was There (PG), $15.00, START: 19
Run Lola Run (R), $5.00, START: 19
```


3. Custom: You can come up with your own small program, following the ideas above for inspiration. Make sure to follow the code requirements below. Also, don't design anything too big. Look at the programs above and try to stick to that format.


## Turn in

-    Upload your project to your repository in the appropriate folder ("Project2").
-    Upload a screenshot of your program running as part of this Canvas assignment.
