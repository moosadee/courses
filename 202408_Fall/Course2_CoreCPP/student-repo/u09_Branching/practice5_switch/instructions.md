# Building and running the program:
Open the folder for this assignment (either via terminal with the `cd` command, or in VS Code with File > Open Folder).

From the terminal, use the g++ compiler to build:
`g++ *.cpp`

And then run it:
`./a.exe` (Windows)
`./a.out` (Linux/Mac)


# Example program output:
*Run 1:*
    (e)speranto, (s)panish, (m)andarin, (h)indi
    Enter a language letter: e
    cat = kato

*Run 2:*
    (e)speranto, (s)panish, (m)andarin, (h)indi
    Enter a language letter: s
    cat = gato

*Run 3:*
    (e)speranto, (s)panish, (m)andarin, (h)indi
    Enter a language letter: m
    cat = mao

*Run 4:*
    (e)speranto, (s)panish, (m)andarin, (h)indi
    Enter a language letter: h
    cat = billee

*Run 5:*
    (e)speranto, (s)panish, (m)andarin, (h)indi
    Enter a language letter: x
    cat = ?


# Instructions
After the user has entered `language`, use a **switch statement**, investigating the value of `language`. Set the `translated` variable to one of the following values based on each case:

case            translated
'e'             "kato"
's'             "gato"
'm'             "mao"
'h'             "billee"
default         "?"


# Reference
A switch statement takes this form:
```
switch( VARIABLE )
{
    case VAL1:
    // Do thing A
    break;

    case VAL2:
    // Do thing B
    break;

    default:
    // Do thing C
}
```
