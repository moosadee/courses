// PROGRAM: Practice using switch statements

// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
using namespace std;  // Using the C++ STanDard libraries

// - PROGRAM CODE -----------------------------------------------------------//
int main()
{
  string english = "cat";
  string translated = "";
  char language;

  cout << "(e)speranto, (s)panish, (m)andarin, (h)indi" << endl;
  cout << "Enter a language letter: ";
  cin >> language;

  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Use a switch statement to look at the `language` chosen.
  // In case of 'e', set `translated` to "kato".
  // In case of 's', set `translated` to "gato".
  // In case of 'm', set `translated` to "mao".
  // In case of 'h', set `translated` to "billee".
  // For the default case, set `translated` to "?"

  // -------------------------------------------------------------------------

  // Display the `english` word and then `translated` word.`
  cout << english + " = " + translated << endl;



  return 0;
}
