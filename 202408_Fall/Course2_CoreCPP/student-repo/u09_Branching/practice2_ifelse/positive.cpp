// PROGRAM: Practice if/else statements

// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
using namespace std;  // Using the C++ STanDard libraries

// - PROGRAM CODE -----------------------------------------------------------//
int main()
{
  cout << "Positive? Negative? Zero?" << endl << endl;

  int number;
  cout << "Enter a number: ";
  cin >> number;

  // - STUDENT CODE ----------------------------------------------------------
  // TODO: If number is greater than 0, then display "Positive number". Otherwise, display "Negative or zero".

  // -------------------------------------------------------------------------



  cout << endl << "Goodbye." << endl;
  return 0;
}
