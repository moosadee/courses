# Building and running the program:
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

From the terminal, use the g++ compiler to build:
`g++ *.cpp`

And then run it:
`./a.exe` (Windows)
`./a.out` (Linux/Mac)


# Example program output:
*Run 1:*
    Positive? Negative? Zero?

    Enter a number: 10
    Positive number

    Goodbye.

*Run 2:*
    Positive? Negative? Zero?

    Enter a number: 0
    Negative or zero

    Goodbye.


# Instructions
After the user has entered a `number`, do the following:

1. If number is greater than 0, then do the following within the if statement's codeblock:
    1a. Display "Positive number". 
Otherwise, do the following within the else codeblock:
    1b. Display "Negative or zero".


# Reference
An if/else statement takes the form:
```
// A. Occurs always
cout << "Example output A" << endl;

if ( CONDITION )
{
    // B1. This code is executed when CONDITION is true
    cout << "Example output B-1" << endl;
}
else
{
    // B2. This code is executed when CONDITION is false.
    cout << "Example output B-2" << endl;
}

// C. Occurs always
cout << "Example output C" << endl;
```
