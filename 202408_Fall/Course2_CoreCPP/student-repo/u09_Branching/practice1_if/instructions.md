# Building and running the program:
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

From the terminal, use the g++ compiler to build:
`g++ *.cpp`

And then run it:
`./a.exe` (Windows)
`./a.out` (Linux/Mac)


# Example program output:
*Run 1:*
    Enter your current balance: $67
    Enter how much to withdraw: $48

    Remaining balance: $19.00

*Run 2:*
    Enter your current balance: $50
    Enter how much to withdraw: $100

    Remaining balance: $-50.00 (OVERDRAWN)


# Instructions
Add this after the user has entered `balance` and `withdraw`, and after `remaining` has been calculated.

1. Display "Remaining balance: $" and the `remaining` variable.

2. If `remaining` is less than 0, then, within its code-block:
    5a. Display "(OVERDRAWN)".



# Reference
An if statement takes the form:
```
// A. Occurs always
cout << "Example output A" << endl;

if ( CONDITION )
{
    // B. This code is executed when CONDITION is true
    cout << "Example output B" << endl;
}

// C. Occurs always
cout << "Example output C" << endl;
```
