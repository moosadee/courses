// PROGRAM: Practice if statements

// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
#include <iomanip>    // Library for formatting; `setprecision`
using namespace std;  // Using the C++ STanDard libraries

// - PROGRAM CODE -----------------------------------------------------------//
int main()
{
  cout << fixed << setprecision( 2 ); // Format floats like USD

  float balance, withdraw, remaining;

  cout << "Enter your current balance: $";
  cin >> balance;

  cout << "Enter how much to withdraw: $";
  cin >> withdraw;

  remaining = balance - withdraw;

  // - STUDENT CODE ----------------------------------------------------------
  // 1. TODO: Display "Remaining balance: $" and the `remaining` variable.

  // 2. TODO: If `remaining` is less than 0, then also display "(OVERDRAWN)".

  // -------------------------------------------------------------------------

  cout << endl;

  // Return 0 means quit program with no errors, in this context.
  return 0;
}
