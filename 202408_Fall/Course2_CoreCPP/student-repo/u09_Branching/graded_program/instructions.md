# Building and running the program:
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

From the terminal, use the g++ compiler to build:
`g++ *.cpp`

And then run it:
`./a.exe` (Windows)
`./a.out` (Linux/Mac)


# Example program output:
*Run program:*
    1. Run AUTOMATED TESTS
    2. Run PROGRAM
    >> 2
    Enter points possible: 10
    Enter your score: 9

    GRADE INFORMATION
    My score ......... 9.00
    Points possible ...10.00
    Grade % ...........90.00
    Letter grade ......A

    RESULT: A

*Run tests:*
    [PASS]  TEST 1, StudentCode(89.00, 90.00) = A

    GRADE INFORMATION
    My score ......... 64.00
    Points possible ...80.00
    Grade % ...........80.00
    Letter grade ......B

    [PASS]  TEST 2, StudentCode(80.00, 64.00) = B

    GRADE INFORMATION
    My score ......... 49.00
    Points possible ...70.00
    Grade % ...........70.00
    Letter grade ......C

    [PASS]  TEST 3, StudentCode(70.00, 49.00) = C

    GRADE INFORMATION
    My score ......... 36.00
    Points possible ...60.00
    Grade % ...........60.00
    Letter grade ......D

    [PASS]  TEST 4, StudentCode(60.00, 36.00) = D

    GRADE INFORMATION
    My score ......... 12.00
    Points possible ...59.00
    Grade % ...........20.34
    Letter grade ......F

    [PASS]  TEST 5, StudentCode(59.00, 12.00) = F


# Instructions
Within the `StudentCode` function, `grade_percent` has been calculated - this corresponds to a grade, from 0% to 100%.

Based on the value of `grade_percent`, set `letter_grade` to one of the following values:

grade_percent       letter_grade
89.5 and above      'A'
79.5 and above      'B'
69.5 and above      'C'
59.5 and above      'D'
everything else     'F'

Make sure that `return letter_grade` is at the end of the function.


# Reference
