# Building and running the program:
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

From the terminal, use the g++ compiler to build:
`g++ *.cpp`

And then run it:
`./a.exe` (Windows)
`./a.out` (Linux/Mac)


# Example program output:
*Run 1:*
    Is the printer on? (0) off, (1) on: 0
    Does the printer have paper? (0) no paper, (1) has paper: 0
    CANNOT PRINT!

*Run 2:*
    Is the printer on? (0) off, (1) on: 1
    Does the printer have paper? (0) no paper, (1) has paper: 0
    CANNOT PRINT!

*Run 3:*
    Is the printer on? (0) off, (1) on: 0
    Does the printer have paper? (0) no paper, (1) has paper: 1
    CANNOT PRINT!

*Run 4:*
    Is the printer on? (0) off, (1) on: 1
    Does the printer have paper? (0) no paper, (1) has paper: 1
    CAN PRINT!


# Instructions
After the user has entered `printer_on` and `has_paper`, do the following:

1a. Create an if statement whose condition is if `has_paper` is true AND `printer_on` is true. If both are true, then display the message "CAN PRINT!" within its codeblock.
1b. Otherwise, within the else block display "CANNOT PRINT!".


# Reference
And operator: &&
Or operator:  ||
Not operator: !
