// PROGRAM: Practice using logic operators

// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
using namespace std;  // Using the C++ STanDard libraries

// - PROGRAM CODE -----------------------------------------------------------//
int main()
{
  bool has_paper, printer_on;

  cout << "Is the printer on? (0) off, (1) on: ";
  cin >> printer_on;

  cout << "Does the printer have paper? (0) no paper, (1) has paper: ";
  cin >> has_paper;

  // - STUDENT CODE ----------------------------------------------------------
  // TODO: If has_paper is true and printer_on is true then display "CAN PRINT!"
  // Otherwise, display "CANNOT PRINT!".

  // -------------------------------------------------------------------------

  // Return 0 means quit program with no errors, in this context.
  return 0;
}
