#include <vector>
#include <string>
#include <iostream>
#include <iomanip>
using namespace std;

//! Converts the `text` string to all lower-case.
string ToLower(string text)
{
    string lower_string = "";
    for (size_t i = 0; i < text.size(); i++)
    {
        lower_string += tolower(text[i]);
    }
    return lower_string;
}

//! Returns true if `find_me` is found within the `full_string`.
bool StringContains(string full_string, string find_me)
{
    return (ToLower(full_string).find(ToLower(find_me)) != string::npos);
}

//! Prints out each element's index and value
void DisplayVector(const vector<string> data)
{
    cout << left;
    cout << setw(10) << "INDEX" << setw(40) << "VALUE" << endl;
    cout << string(50, '-') << endl;
    for (size_t i = 0; i < data.size(); i++)
    {
        cout << setw(10) << i << setw(40) << data[i] << endl;
    }
}

/**
@return     vector<string>                  Array of items that match
@param      vector<string>& original        Array of items to search through
@param      string findme                   Text to search for within the list
*/
vector<string> LinearSearch(const vector<string> &original, string findme)
{
    vector<string> matches;

    // - STUDENT CODE ----------------------------------------------------------
    // TODO: Implement linear search

    // -------------------------------------------------------------------------

    return matches;
}

int main()
{
    vector<string> data = {"cat", "dog", "rat", "mouse", "bat", "cow", "moose"};
    string searchTerm;

    cout << "Search for what text? ";
    getline(cin, searchTerm);

    vector<string> matches = LinearSearch(data, searchTerm);

    cout << endl
         << "RESULTS:" << endl;
    DisplayVector(matches);
}
