# Building and running the program:
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

From the terminal, use the g++ compiler to build:
`g++ *.cpp`

And then run it:
`./a.exe` (Windows)
`./a.out` (Linux/Mac)


# Example program output:
*Run 1:*
    Search for what text? at

    RESULTS:
    INDEX     VALUE                                   
    --------------------------------------------------
    0         cat                                     
    1         rat                                     
    2         bat                                     

*Run 2:*
    Search for what text? se

    RESULTS:
    INDEX     VALUE                                   
    --------------------------------------------------
    0         mouse                                   
    1         moose                                   


# Instructions
Implement the `LinearSearch` function using the textbook as reference.


# Reference
In the textbook, you can ignore the `template <typename T>` and `T` part of the code. This is how we make placeholders, which you will learn about in CS 235. In this course, all the data types we use will be known, so just use the data types relevant to the assignment.