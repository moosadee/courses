// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !! YOU DON'T NEED TO MODIFY THIS FILE !!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#ifndef _COURSE_H
#define _COURSE_H

#include <string>
using namespace std;

struct Course {
  string dept;
  string title;
  string hours;
  string description;
};

#endif
