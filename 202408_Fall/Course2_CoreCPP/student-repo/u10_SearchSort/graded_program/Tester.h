// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !! YOU DON'T NEED TO MODIFY THIS FILE !!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#ifndef _TESTER
#define _TESTER

#include "Course.h"

struct TestResults
{
  TestResults( string testing )
  {
    this->testing = testing;
    totalTests=0;
    totalPass=0;
    totalFail=0;
  }
  void AddTest()
  {
    totalTests++;
  }
  void AddPass()
  {
    totalPass++;
  }
  void AddFail()
  {
    totalFail++;
  }

  string testing;
  int totalTests;
  int totalPass;
  int totalFail;
};

void RunUnitTests();

#endif
