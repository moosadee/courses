// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !! YOU DON'T NEED TO MODIFY THIS FILE !!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#include "Tester.h"
#include "Functions.h"
#include "Course.h"

#include <vector>
#include <iostream>
#include <iomanip>
using namespace std;

void RunUnitTests()
{
    vector<TestResults> results;
    cout << left;

    {
        TestResults result( "Test - SearchByDepartment" );
        result.AddTest();

        cout << setw( 45 ) << "Test - SearchByDepartment... ";

        // Search by department test
        vector<Course> courseList =
        {
            { "AAA", "Intro to Potato Peeling", "3",
                   "How to peel potatoes and avoid peeling yourself" },
            { "AAA", "Identifying Sandwiches", "4", "Is a hotdog a sandwich?" },
            { "BBB", "Intro to Cheese Grating", "5",
                   "How to grate cheese and avoid grating yourself" },
            { "CCC", "Cupcakes of Doom", "6",
                   "Just because it's a small cake doesn't mean it's not loaded "
                   "with sugar" },
            { "CCC", "Cakes and You", "6",
                   "The evolution of Cakes over the history of humankind" }
        };

        vector<Course> expectedResult =
        {
            { "AAA", "Intro to Potato Peeling", "3",
                   "How to peel potatoes and avoid peeling yourself" },
            { "AAA", "Identifying Sandwiches", "4", "Is a hotdog a sandwich?" },
        };

        vector<Course> actualResult;
        string searchInput = "AAA";

        actualResult = SearchByDepartment(courseList, searchInput);

        // Check for matches
        bool allMatch = true;
        for (unsigned int i = 0; i < expectedResult.size(); i++)
        {
            if ( i >= actualResult.size() )
            {
                allMatch = false;
                cout << endl << "EXPECTED " << expectedResult.size() << " RESULTS, BUT GOT " << actualResult.size() << "!" << endl;
                break;
            }
            if (  expectedResult[i].title       != actualResult[i].title ||
                  expectedResult[i].description != actualResult[i].description ||
                  expectedResult[i].hours       != actualResult[i].hours ||
                  expectedResult[i].dept        != actualResult[i].dept
                  )
            {
                allMatch = false;
            }
        }

        if (allMatch)
        {
            result.AddPass();
            cout << "\033[0;32m TEST PASSES" << endl;
        }
        else
        {
            result.AddFail();
            cout << "\033[0;31m TEST FAILS" << endl;

            cout << "Test set of courses: " << endl;
            DisplayCourses(courseList);

            cout << endl << "Search term: \"" << searchInput << "\"" << endl;

            cout << endl << "EXPECTED RESULT:" << endl;
            DisplayCourses(expectedResult);

            cout << endl << "ACTUAL RESULT: " << endl;
            DisplayCourses(actualResult);
        }

        cout << "\033[0m";

        results.push_back( result );
    }

    {
        TestResults result( "Test - BubbleSortByTitle" );
        result.AddTest();

        cout << setw( 45 ) << "Test - BubbleSortByTitle... ";

        // Bubble sort / title test
        vector<Course> courseList =
        {
            {"asdf", "TitleB", "3", "Lorem ipsum"},
            {"zxcv", "TitleZ", "4", "Dolor sit amet"},
            {"qwer", "TitleA", "5", "Consectetur adipiscing"},
            {"rtyu", "TitleX", "6", "Elit sed do"}
        };

        vector<Course> expectedResult =
        {
            {"qwer", "TitleA", "5", "Consectetur adipiscing"},
            {"asdf", "TitleB", "3", "Lorem ipsum"},
            {"rtyu", "TitleX", "6", "Elit sed do"},
            {"zxcv", "TitleZ", "4", "Dolor sit amet"}
        };

        vector<Course> actualResult;

        actualResult = BubbleSortByTitle(courseList);

        // Check for matches
        bool allMatch = true;
        for (unsigned int i = 0; i < expectedResult.size(); i++)
        {
            if ( i >= actualResult.size() )
            {
                allMatch = false;
                cout << endl << "EXPECTED " << expectedResult.size() << " RESULTS, BUT GOT " << actualResult.size() << "!" << endl;
                break;
            }
            if (  expectedResult[i].title       != actualResult[i].title ||
                  expectedResult[i].description != actualResult[i].description ||
                  expectedResult[i].hours       != actualResult[i].hours ||
                  expectedResult[i].dept        != actualResult[i].dept
                  )
            {
                allMatch = false;
            }
        }

        if (allMatch)
        {
            result.AddPass();
            cout << "\033[0;32m TEST PASSES" << endl;
        }
        else
        {
            result.AddFail();
            cout << "\033[0;31m TEST FAILS" << endl;

            cout << "Course list before sort: " << endl;
            DisplayCourses(courseList);

            cout << endl << "EXPECTED RESULT:" << endl;
            DisplayCourses(expectedResult);

            cout << endl << "ACTUAL RESULT: " << endl;
            DisplayCourses(actualResult);
        }
        cout << "Done with test" << endl;

        cout << "\033[0m";

        results.push_back( result );
    } // End bubble sort test

    {
        TestResults result( "Test - SelectionSortByDepartment" );
        result.AddTest();

        cout << setw( 45 ) << "Test - SelectionSortByDepartment... ";

        // Selection sort / department test
        vector<Course> courseList =
        {
            {"rtyu", "TitleX", "6", "Elit sed do"},
            {"zxcv", "TitleZ", "4", "Dolor sit amet"},
            {"qwer", "TitleA", "5", "Consectetur adipiscing"},
            {"asdf", "TitleB", "3", "Lorem ipsum"},
        };

        vector<Course> expectedResult =
        {
            {"asdf", "TitleB", "3", "Lorem ipsum"},
            {"qwer", "TitleA", "5", "Consectetur adipiscing"},
            {"rtyu", "TitleX", "6", "Elit sed do"},
            {"zxcv", "TitleZ", "4", "Dolor sit amet"}
        };

        vector<Course> actualResult;

        actualResult = SelectionSortByDepartment(courseList);

        // Check for matches
        bool allMatch = true;
        for (unsigned int i = 0; i < expectedResult.size(); i++)
        {
            if ( i >= actualResult.size() )
            {
                allMatch = false;
                cout << endl << "EXPECTED " << expectedResult.size() << " RESULTS, BUT GOT " << actualResult.size() << "!" << endl;
                break;
            }
            if (  expectedResult[i].title       != actualResult[i].title ||
                  expectedResult[i].description != actualResult[i].description ||
                  expectedResult[i].hours       != actualResult[i].hours ||
                  expectedResult[i].dept        != actualResult[i].dept
                  )
            {
                allMatch = false;
            }
        }

        if (allMatch)
        {
            result.AddPass();
            cout << "\033[0;32m TEST PASSES" << endl;
        }
        else
        {
            result.AddFail();
            cout << "\033[0;31m TEST FAILS" << endl;

            cout << "Course list before sort: " << endl;
            DisplayCourses(courseList);

            cout << endl << "EXPECTED RESULT:" << endl;
            DisplayCourses(expectedResult);

            cout << endl << "ACTUAL RESULT: " << endl;
            DisplayCourses(actualResult);
        }

        cout << "\033[0m";

        results.push_back( result );
    } // End selection sort test

    {
        TestResults result( "Test - InsertionSortByHours" );
        result.AddTest();

        cout << setw( 45 ) << "Test - InsertionSortByHours... ";

        // Insertion sort /  test
        vector<Course> courseList =
        {
            {"rtyu", "TitleX", "6", "Elit sed do"},
            {"zxcv", "TitleZ", "4", "Dolor sit amet"},
            {"asdf", "TitleB", "3", "Lorem ipsum"},
            {"qwer", "TitleA", "5", "Consectetur adipiscing"},
        };

        vector<Course> expectedResult =
        {
            {"asdf", "TitleB", "3", "Lorem ipsum"},
            {"zxcv", "TitleZ", "4", "Dolor sit amet"},
            {"qwer", "TitleA", "5", "Consectetur adipiscing"},
            {"rtyu", "TitleX", "6", "Elit sed do"}
        };

        vector<Course> actualResult;

        actualResult = InsertionSortByHours(courseList);

        // Check for matches
        bool allMatch = true;
        for (unsigned int i = 0; i < expectedResult.size(); i++)
        {
            if ( i >= actualResult.size() )
            {
                allMatch = false;
                cout << endl << "EXPECTED " << expectedResult.size() << " RESULTS, BUT GOT " << actualResult.size() << "!" << endl;
                break;
            }
            if (  expectedResult[i].title       != actualResult[i].title ||
                  expectedResult[i].description != actualResult[i].description ||
                  expectedResult[i].hours       != actualResult[i].hours ||
                  expectedResult[i].dept        != actualResult[i].dept
                  )
            {
                allMatch = false;
            }
        }

        if (allMatch)
        {
            result.AddPass();
            cout << "\033[0;32m TEST PASSES" << endl;
        }
        else
        {
            result.AddFail();
            cout << "\033[0;31m TEST FAILS" << endl;

            cout << "Course list before sort: " << endl;
            DisplayCourses(courseList);

            cout << endl << "EXPECTED RESULT:" << endl;
            DisplayCourses(expectedResult);

            cout << endl << "ACTUAL RESULT: " << endl;
            DisplayCourses(actualResult);
        }

        cout << "\033[0m";

        results.push_back( result );
    } // End bubble sort test

    cout << endl << string(60, '*') << endl;
    cout << "ALL TEST RESULTS" << endl;

    cout << left << setw( 45 ) << "TEST" << setw( 10 ) << "FAILS" << setw( 10 ) << "PASS" << setw( 10 ) << "TOTAL" << endl;
    cout << string( 65, '-' ) << endl;
    for ( auto& result : results )
    {
        cout << left << setw( 45 ) << result.testing
            << setw( 1 ) << "\033[0;31m"    << setw( 10 ) << result.totalFail
            << setw( 1 ) << "\033[0;32m"    << setw( 10 ) << result.totalPass
            << setw( 1 ) << "\033[0m"       << setw( 10 ) << result.totalTests << endl;
    }
}
