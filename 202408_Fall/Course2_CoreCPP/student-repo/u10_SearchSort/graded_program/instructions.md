# Building and running the program:
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

From the terminal, use the g++ compiler to build:
`g++ *.h *.cpp`

And then run it:
`./a.exe` (Windows)
`./a.out` (Linux/Mac)


# Overview
This program has a lot of code in it but I've built the main program structure, you will just be implementing the Search and Sorting algorithms.


# Example program output:
    1264 total course(s) loaded

    --------------------------------------------------------------------------------
    MAIN MENU
    --------------------------------------------------------------------------------
    0. Exit program
    1. Run AUTOMATED TESTER

    Search
    > 2. Search for course by department

    Sort
    > 3. Perform Bubble Sort
    > 4. Perform Selection Sort
    > 5. Perform Insertion Sort

    View
    > 6. View a single course

    Choice: 

After searching or sorting it will display a list of the Course list that matches the operation.

You can also view full details of a single course with option #6.

Use option #1 to run the automated tests to make sure your search/sort algorithms have the correct logic.


# Instructions
Within **Functions.cpp** you will implement the following functions:

- SearchByDepartment
- BubbleSortByTitle
- InsertionSortByHours
- SelectionSortByDepartment

Each of these start with the first line of code (a vector of Course objects is created) and the last line of code (the vector is returned), you need to implement the code in-between.



# Reference
Utilize the textbook for the generic forms of the Search/Sorting algorithms you need. Remember that we're working with a struct:
```
struct Course {
  string dept;
  string title;
  string hours;
  string description;
};
```

So you'll need to do your comparisons based on `.dept`, `.title`, or `.hours`.
