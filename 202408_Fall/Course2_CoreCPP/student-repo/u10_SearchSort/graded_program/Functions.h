// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !! YOU DON'T NEED TO MODIFY THIS FILE !!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#ifndef _FUNCTIONS
#define _FUNCTIONS

#include "Course.h"
#include <string>
#include <vector>
using namespace std;

vector<Course> SearchByDepartment( const vector<Course>& original, string searchTerm );
vector<Course> BubbleSortByTitle( const vector<Course>& original );
vector<Course> InsertionSortByHours( const vector<Course>& original );
vector<Course> SelectionSortByDepartment( const vector<Course>& original );

void DisplayMenu();
int GetValidInput( int min, int max );
string ToLower( string text );
bool StringContains( string full_string, string find_me );
vector<Course> SetupCourses();
void DisplayCourses( const vector<Course>& courses );
void DisplayCourseDetails( const Course& course );
string Trim( string text, size_t length );

void Swap( Course& a, Course& b );

#endif
