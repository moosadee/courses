// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !! YOU DON'T NEED TO MODIFY THIS FILE !!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <iomanip>    // Library for formatting; `setprecision`
#include <string>     // Library that contains `string` types
#include <vector>     // Library that contains `vector`
using namespace std;  // Using the C++ STanDard libraries

#include "Course.h"
#include "Tester.h"
#include "Functions.h"

// - PROGRAM CODE -----------------------------------------------------------//
int main( int argCount, char* args[] )
{
  cout << left;
  if ( argCount == 2 && string( args[1] ) == "test" )
  {
    RunUnitTests();
    return 0;
  }

  vector<Course> courses = SetupCourses();

  cout << courses.size() << " total course(s) loaded" << endl;

  bool done = false;
  int choice;
  while ( !done )
  {
    DisplayMenu();

    choice = GetValidInput( 0, 6 );

    switch( choice )
    {
    case 0: // Exit
      done = true;
      break;

    case 1: // Tests
      RunUnitTests();
      break;

    case 2: // Search by description
    {
      string searchTerm = "";
      cout << "Enter search term: ";
      cin.ignore();
      getline( cin, searchTerm );

      vector<Course> matches = SearchByDepartment( courses, searchTerm );

      cout << endl << "-- MATCHES --" << endl << endl;
      DisplayCourses( matches );
    }
    break;

    case 3: // Bubble sort
    {
      vector<Course> sortedList = BubbleSortByTitle( courses );
      cout << endl << "-- SORTED --" << endl << endl;
      DisplayCourses( sortedList );
    }
    break;

    case 4: // Selection sort
    {
      vector<Course> sortedList = SelectionSortByDepartment( courses );
      cout << endl << "-- SORTED --" << endl << endl;
      DisplayCourses( sortedList );
    }
    break;

    case 5: // Insertion sort
    {
      vector<Course> sortedList = InsertionSortByHours( courses );
      cout << endl << "-- SORTED --" << endl << endl;
      DisplayCourses( sortedList );
    }
    break;

    case 6:
    {
      int id;
      cout << "Enter the course's ID: ";
      id = GetValidInput( 0, courses.size() - 1 );
      DisplayCourseDetails( courses[id] );
    }
    break;
    }
  }


// Return 0 means quit program with no errors, in this context.
  return 0;
}
