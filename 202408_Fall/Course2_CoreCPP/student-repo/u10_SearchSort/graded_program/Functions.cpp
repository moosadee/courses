#include "Functions.h"

#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

// Uses LINEAR SEARCH
vector<Course> SearchByDepartment( const vector<Course>& original, string searchTerm )
{
  vector<Course> matches;

  // - STUDENT CODE ----------------------------------------------------------
  // TODO: IMPLEMENT LINEAR SEARCH, SEARCH BY CODE

  // -------------------------------------------------------------------------

  return matches;
}

vector<Course> BubbleSortByTitle( const vector<Course>& original )
{
  vector<Course> sorted = original;

  // - STUDENT CODE ----------------------------------------------------------
  // TODO: IMPLEMENT BUBBLE SORT, SORT ON TITLE.

  // -------------------------------------------------------------------------

  return sorted;
}

vector<Course> InsertionSortByHours( const vector<Course>& original )
{
  vector<Course> sorted = original;

  // - STUDENT CODE ----------------------------------------------------------
  // TODO: IMPLEMENT INSERTION SORT, SORT ON HOURS.

  // -------------------------------------------------------------------------

  return sorted;
}

vector<Course> SelectionSortByDepartment( const vector<Course>& original )
{
  vector<Course> sorted = original;

  // - STUDENT CODE ----------------------------------------------------------
  // TODO: IMPLEMENT SELECTION SORT, SORT ON DEPARTMENT.

  // -------------------------------------------------------------------------

  return sorted;
}


// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !! YOU DON'T NEED TO MODIFY ANYTHING BELOW THIS POINT !!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

void DisplayMenu()
{
  cout << endl << string( 80, '-' ) << endl;
  cout << "MAIN MENU" << endl;
  cout << string( 80, '-' ) << endl;
  cout << "0. Exit program" << endl;
  cout << "1. Run AUTOMATED TESTER" << endl;
  cout << endl;
  cout << "Search" << endl;
  cout << "> 2. Search for course by department" << endl;
  cout << endl;
  cout << "Sort" << endl;
  cout << "> 3. Perform Bubble Sort" << endl;
  cout << "> 4. Perform Selection Sort" << endl;
  cout << "> 5. Perform Insertion Sort" << endl;
  cout << endl;
  cout << "View" << endl;
  cout << "> 6. View a single course" << endl;
  cout << endl;
}

int GetValidInput( int min, int max )
{
  int choice;
  cout << "Choice: ";
  cin >> choice;

  while ( choice < min || choice > max )
    {
      cout << endl << "INVALID SELECTION! Try again!" << endl;
      cin >> choice;
    }

  return choice;
}

string ToLower( string text )
{
  string lower_string = "";
  for ( size_t i = 0; i < text.size(); i++ )
    {
      lower_string += tolower( text[i] );
    }
  return lower_string;
}

bool StringContains( string full_string, string find_me )
{
  return ( ToLower( full_string ).find( ToLower( find_me ) ) != string::npos );
}

vector<Course> SetupCourses()
{
  vector<Course> courses;

  ifstream input( "courses.txt" );
  string buffer;
  Course newCourse;
  while ( getline( input, newCourse.dept ) )
  {
    getline( input, newCourse.title );
    getline( input, newCourse.hours );
    getline( input, newCourse.description );
    getline( input, buffer );
    courses.push_back( newCourse );
  }

  return courses;
}

string Trim( string text, size_t length )
{
  if ( text.size() >= length )
  {
    return text.substr( 0, length - 4 ) + "...";
  }
  else
  {
    return text;
  }
}

void DisplayCourses( const vector<Course>& courses )
{
  cout
    << setw( 5 ) << "#"
    << setw( 10 ) << "DEPT"
    << setw( 10 ) << "HRS"
    << setw( 20 ) << "TITLE"
    << setw( 35 ) << "DESC"
    << endl << string( 80, '-' ) << endl;
  for ( size_t i = 0; i < courses.size(); i++ )
    {
      cout << "Display course " << i << endl;
      cout
        << setw( 5  ) << i
        << setw( 10 ) << courses[i].dept
        << setw( 10 ) << courses[i].hours
        << setw( 20 ) << Trim( courses[i].title, 20 )
        << setw( 35 ) << Trim( courses[i].description, 35 )
        << endl;
    }
}

void DisplayCourseDetails( const Course& course )
{
  cout << string( 60, '-' ) << endl;
  cout << "DEPARTMENT: " << course.dept << endl;
  cout << "HOURS:      " << course.hours << endl;
  cout << "TITLE:      " << course.title << endl;
  cout << "DESCRIPTION" << endl << course.description << endl;
  cout << string( 60, '-' ) << endl;
}

void Swap( Course& a, Course& b )
{
  Course c = a;
  a = b;
  b = c;
}


