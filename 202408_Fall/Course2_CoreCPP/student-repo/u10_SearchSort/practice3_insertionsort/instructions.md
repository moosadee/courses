# Building and running the program:
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

From the terminal, use the g++ compiler to build:
`g++ *.cpp`

And then run it:
`./a.exe` (Windows)
`./a.out` (Linux/Mac)


# Example program output:
    ORIGINAL LIST:
    INDEX     VALUE                                   
    --------------------------------------------------
    0         cat                                     
    1         dog                                     
    2         rat                                     
    3         mouse                                   
    4         bat                                     
    5         cow                                     
    6         moose                                   

    RESULTS:
    INDEX     VALUE                                   
    --------------------------------------------------
    0         bat                                     
    1         cat                                     
    2         cow                                     
    3         dog                                     
    4         moose                                   
    5         mouse                                   
    6         rat                                   


# Instructions
Implement the `InsertionSort` function using the textbook as reference.


# Reference
In the textbook, you can ignore the `template <typename T>` and `T` part of the code. This is how we make placeholders, which you will learn about in CS 235. In this course, all the data types we use will be known, so just use the data types relevant to the assignment.
