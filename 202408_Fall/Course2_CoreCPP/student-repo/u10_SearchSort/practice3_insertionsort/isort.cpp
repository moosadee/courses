#include <vector>
#include <string>
#include <iostream>
#include <iomanip>
using namespace std;

//! Converts the `text` string to all lower-case.
string ToLower(string text)
{
    string lower_string = "";
    for (size_t i = 0; i < text.size(); i++)
    {
        lower_string += tolower(text[i]);
    }
    return lower_string;
}

//! Returns true if `find_me` is found within the `full_string`.
bool StringContains(string full_string, string find_me)
{
    return (ToLower(full_string).find(ToLower(find_me)) != string::npos);
}

//! Prints out each element's index and value
void DisplayVector(const vector<string> data)
{
    cout << left;
    cout << setw(10) << "INDEX" << setw(40) << "VALUE" << endl;
    cout << string(50, '-') << endl;
    for (size_t i = 0; i < data.size(); i++)
    {
        cout << setw(10) << i << setw(40) << data[i] << endl;
    }
}

//! Swaps values of two variables
void Swap( string& a, string& b )
{
  string c = a;
  a = b;
  b = c;
}

/**
@return     vector<string>                  Array of sorted items
@param      vector<string>& original        Array of items to sort
*/
vector<string> InsertionSort(const vector<string> &original)
{
    vector<string> sorted = original; // Created a copy to work with

    size_t i = 1;

    // - STUDENT CODE ----------------------------------------------------------
    // TODO: Implement insertion sort

    // -------------------------------------------------------------------------

    return sorted; // Return the sorted version of the vector
}

int main()
{
    vector<string> data = {"cat", "dog", "rat", "mouse", "bat", "cow", "moose"};

    cout << endl << "ORIGINAL LIST:" << endl;
    DisplayVector(data);


    vector<string> sorted = InsertionSort(data);

    cout << endl
         << "RESULTS:" << endl;
    DisplayVector(sorted);
}
