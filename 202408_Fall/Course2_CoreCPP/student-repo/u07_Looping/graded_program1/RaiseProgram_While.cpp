// Use `g++ *.cpp` to build this program
// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
#include <iomanip>    // Library for formatting; `setprecision`
using namespace std;  // Using the C++ STanDard libraries

// - STUDENT CODE -----------------------------------------------------------//
/**
   @param    starting_salary      The user's starting salary
   @param    raise_per_year       The user's raise per year, as a percent
   @param    years                How many years of pay raises to calculate
   @return   float                Returns the ending salary after `years` amount of years
 */
float CalculateRaise( float starting_salary, float raise_per_year, int years ) // WHILE LOOP VERSION
{
  cout << "Starting salary: $" << starting_salary << endl;

  float updated_salary = starting_salary;
  int current_year = 1;

  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Implement function

  // -------------------------------------------------------------------------

  cout << endl;
  return updated_salary;
}

void Program()
{
  cout << fixed << setprecision( 2 ); // Format for USD
  float starting_salary, raise_per_year, updated_salary;
  int years;

  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Implement function

  // -------------------------------------------------------------------------

  updated_salary = CalculateRaise( starting_salary, raise_per_year, years );
  cout << endl << "SALARY AFTER " << years << " YEARS: $" << updated_salary << endl;
}

//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! DON'T MODIFY ANYTHING BELOW THIS POINT! !!
//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! I've written custom main code and an automated tester.
//   !! You only need to work with the functions above this.
enum class ProgramAction { TEST, PROGRAM };
void Tester();
int main( int argCount, char* args[] )
{
  string program_name = "RAISE CALCULATOR";
  ProgramAction action;
  if ( argCount > 1 && string( args[1] ) == "test" )
    {
      action = ProgramAction::TEST;
    }
  else
    {
      std::cout << "1. Run AUTOMATED TESTS" << std::endl;
      std::cout << "2. Run PROGRAM" << std::endl;
      int choice;
      std::cout << ">> ";
      std::cin >> choice;

      switch( choice )
        {
        case 1: action = ProgramAction::TEST; break;
        case 2: action = ProgramAction::PROGRAM; break;
        }
    }

  if ( action == ProgramAction::TEST )
    {
      Tester();
    }
  else
    {
      program_name = "# " + program_name + " #";
      cout << endl << string( 80, '#' ) << endl;
      cout << program_name << endl;
      cout << string( program_name.size(), '#' ) << endl << endl;

      // PROGRAM
      Program();
    }

  return 0;
}

// - AUTOMATED TESTER -------------------------------------------------------//
void Tester()
{
  const string GRN = "\033[0;32m"; const string RED = "\033[0;31m"; const string BOLD = "\033[0;35m"; const string CLR = "\033[0m";

  // (Automated test):
  const int TOTAL_TESTS = 2;
  float  in1[TOTAL_TESTS]; // inputs 1
  float  in2[TOTAL_TESTS]; // inputs 2
  int    in3[TOTAL_TESTS]; // inputs 3
  float  exo[TOTAL_TESTS]; // expected output
  float  aco[TOTAL_TESTS]; // actual output

  // Setup test 1
  in1[0] = 60000;
  in2[0] = 5;
  in3[0] = 5;
  exo[0] = 76576.89;

  // Setup test 2
  in1[1] = 100000;
  in2[1] = 10;
  in3[1] = 3;
  exo[1] = 133100;

  // Run tests
  for ( int i = 0; i < TOTAL_TESTS; i++ )
  {
    cout << CLR;
    aco[i] = CalculateRaise( in1[i], in2[i], in3[i] );

    // Range-based check because of floats, woo.
    if ( aco[i] >= exo[i] - 1 && aco[i] <= exo[i] + 1 )
    {
      // PASS
      cout << GRN << "[PASS] ";
      cout << " TEST " << i+1 << ", CalculateRaise(" << in1[i] << ", " << in2[i] << ", " << in3[i] << ") = " << aco[i] << endl;
    }
    else
    {
      // FAIL
      cout << RED << "[FAIL] ";
      cout << " TEST " << i+1 << ", CalculateRaise(" << in1[i] << ", " << in2[i] << ", " << in3[i] << ")" << endl;
      cout << "   EXPECTED OUTPUT: [" << exo[i] << "]" << endl;
      cout << "   ACTUAL OUTPUT:   [" << aco[i] << "]" << endl;
    }
  }
  cout << CLR;
}
