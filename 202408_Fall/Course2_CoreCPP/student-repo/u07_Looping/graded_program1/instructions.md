# Introduction

This program will ask the user for their salary, their raise per year, and how many years to project. It will calculate the raise over the years and display all the data.

# Building and running the program
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

- Open **terminal**
- Build the program: `g++ *.cpp`
- View files: `ls`
- Run program:
  - `./a.out` (Linux/Mac)
  - `.\a.exe` (Windows)


# Example output

```
1. Run AUTOMATED TESTS
2. Run PROGRAM
>> 2

################################################################################
# RAISE CALCULATOR #
####################

Enter starting salary: 65000
Raise per year: %5
Enter how many years to project: 5
Starting salary: $65000.00
Year: 1, Salary: $68250.00
Year: 2, Salary: $71662.50
Year: 3, Salary: $75245.62
Year: 4, Salary: $79007.91
Year: 5, Salary: $82958.30

SALARY AFTER 5 YEARS: $82958.30

```

For graded programs, automated tests are also provided where you can verify your work.
Make sure all tests pass:

```
1. Run AUTOMATED TESTS
2. Run PROGRAM
>> 1
Starting salary: $60000
Year: 1, Salary: $63000
Year: 2, Salary: $66150
Year: 3, Salary: $69457.5
Year: 4, Salary: $72930.4
Year: 5, Salary: $76576.9
[PASS]  TEST 1, CalculateRaise(60000, 5, 5) = 76576.9

Starting salary: $100000
Year: 1, Salary: $110000
Year: 2, Salary: $121000
Year: 3, Salary: $133100
[PASS]  TEST 2, CalculateRaise(100000, 10, 3) = 133100
```

# Updating CalculateRaise

Within the "RaiseProgram.cpp" there is a `float CalculateRaise( float starting_salary, float raise_per_year, int years )` function you will be updating.

Within the function, calculate the updated salary doing the following:
1. ASSIGN `updated_salary` to the value from `starting_salary`.
2. WHILE `current_year` is less than or equal to `years`, do the following:
   2a. CALCULATE the amount of money for this raise: `updated_salary * raise_per_year/100`. STORE this in a variable.
   2b. ASSIGN a new value to `updated_salary`: The result of `updated_salary` plus the raise money.
   2c. DISPLAY the `current_year` and the `updated_salary` to the screen.
   2d. Increment the `current_year` by 1.
3. At the end of the function RETURN the `updated_salary`.


# Updating Main

Within the "RaiseProgram.cpp" there is a `void Main()` function you will be updating.

- DISPLAY a prompt "Enter starting salary:" to the screen.
- GET input and STORE it in the `starting_salary` variable.
- DISPLAY a prompt "Raise per year: %" to the screen.
- GET input and STORE it in the `raise_per_year` variable.
- DISPLAY a prompt "Enter how many years to project:" to the screen.
- GET input and STORE it in the `years` variable.
- CALL the CalculateRaise function, passing in `starting_salary`, `raise_per_year`, and `years` as arguments. STORE the RETURN OUTPUT in the `updated_salary` variable.
- DISPLAY "SALARY AFTER [YEARS] YEARS: $" (replacing [YEARS] with the `years` variable), then DISPLAY the `updated_salary`.
