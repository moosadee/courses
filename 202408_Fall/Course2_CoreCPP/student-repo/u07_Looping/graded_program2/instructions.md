# Building and running the program
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

- Open **terminal**
- Build the program: `g++ *.cpp`
- View files: `ls`
- Run program:
  - `./a.out` (Linux/Mac)
  - `.\a.exe` (Windows)


# Salary raise program

This program will be the same as graded_program1, but instead of using a **while loop** to do the calculation, use a **for loop** instead.

Starting: current year is 1
Condition: Loop while current year is less than or equal to `years`
Update: Add 1 to current year at the end of each iteration
