# Building and running the program:
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

From the terminal, use the g++ compiler to build:
`g++ *.cpp`

And then run it:
`./a.exe` (Windows)
`./a.out` (Linux/Mac)


# Example program output:
  Enter low number: 5
  Enter high number: 20
  Enter step: 2

  LOW TO HIGH
  5 7 9 11 13 15 17 19

  HIGH TO LOW
  20 18 16 14 12 10 8 6

  GOODBYE


# Instructions
Under the "LOW TO HIGH" label, create a while loop that counts from low to high:
1. ASSIGN `counter` the value from `low_number`.
2. while `counter` is less than or equal to the `high_number`...
  2a. DISPLAY `counter` and then a space.
  2b. INCREMENT `counter` by the `step` amount.

Under the "HIGH TO LOW" label, create a while loop that counts from high to low:
1. ASSIGN `counter` the value from `high_number`.
2. while `counter` is greater than or equal to the `low_number`...
  2a. DISPLAY `counter` and then a space.
  2b. DECREMENT `counter` by the `step` amount.


# Reference
A while loop takes this form:
```
while ( CONDITION )
{

}
```

Increment by some amount:
VARIABLE += VALUE;

Decrement by some amount:
VARIABLE -= VALUE;
