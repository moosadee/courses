// PROGRAM: Practice creating a program loop with while
// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
using namespace std;  // Using the C++ STanDard libraries

// - PROGRAM CODE -----------------------------------------------------------//
int main()
{
  int low_number;
  int high_number;
  int step;
  int counter;

  cout << "Enter low number: ";
  cin >> low_number;

  cout << "Enter high number: ";
  cin >> high_number;

  cout << "Enter step: ";
  cin >> step;

  cout << endl << "LOW TO HIGH" << endl;
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Implement low to high loop

  // -------------------------------------------------------------------------
  cout << endl;

  cout << endl << "HIGH TO LOW" << endl;
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Implement high to low loop

  // -------------------------------------------------------------------------
  cout << endl;

  cout << endl << "GOODBYE" << endl;

  // Return 0 means quit program with no errors, in this context.
  return 0;
}
