#include "ProductFamily.h"

// - GenericProduct ------------------------------------------------------------//

//! Constructor to quickly set up member variables
GenericProduct::GenericProduct( string new_name, float new_price )
{
  SetName( new_name );
  SetPrice( new_price );
}

//! Sets the member variable `name` to the value passed in as `new_name`.
void GenericProduct::SetName( string new_name )
{
  this->name = new_name;
}

//! Sets the member variable `price` to the value passed in as `new_price`.
void GenericProduct::SetPrice( float new_price )
{
  this->price = new_price;
}

//! Returns the value stored in the `name` member variable.
string GenericProduct::GetName() const
{
  return this->name;
}

//! Returns the value stored in the `price` member variable.
float GenericProduct::GetPrice() const
{
  return this->price;
}



// - Your class' functions --------------------------------------------------//
