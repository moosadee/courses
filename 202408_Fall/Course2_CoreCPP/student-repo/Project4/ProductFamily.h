#ifndef _PRODUCTFAMILY
#define _PRODUCTFAMILY

#include <string>
using namespace std;

//! A generic Product; inherit from this to make new types of Products
class GenericProduct
{
  public:
  // Constructor
  GenericProduct( string new_name, float new_price );

  // Getters and Setters
  void SetName( string new_name );
  void SetPrice( float new_price );

  string GetName() const;
  float GetPrice() const;

  protected: // Children inherit protected members
  string name;
  float price;
};

// TODO: Create two new Products that inherit from Product Base.
// e.g., Food Item (has calories), Book Item (has ISBN), Movie Item (has rating).

#endif
