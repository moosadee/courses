#ifndef _PROGRAM
#define _PROGRAM

#include "ProductFamily.h"

#include <vector>
using namespace std;

//! Class to store all the program's menus and data
class Program
{
  public:
  void Start();
  void SetupData();

  void Menu_Main();
  void Menu_ViewAllProducts();

  // - Generic product functions ----------------------------------------------//
  void Menu_GenericProducts_Add();
  void Menu_GenericProducts_Edit();
  void Display_GenericProducts();

  // - YOUR product functions -------------------------------------------------//
  // TODO: Add your function declarations here

  // - Helper functions -------------------------------------------------------//
  int GetValidInput( int min, int max );
  void ClearScreen() const;
  void DisplayHeader( string title ) const;
  void PressEnterToContinue() const;

  private:
  vector<GenericProduct> genericProducts;
  // TODO: Declare your vectors here
};

#endif
