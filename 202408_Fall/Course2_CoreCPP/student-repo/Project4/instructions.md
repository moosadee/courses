# Project 4: Classes and inheritance

## About

For this program I've implemented part of a program that has a GenericProduct class. This class has member variables name and price, which is the bare minimum for product data. You will create 2 new classes that inherit from GenericProduct to make more specific products - you can choose what kind of products to make. (More info later).



## Starter code

The starter code contains the following:

- main.cpp - Just starts the program
- Program.h and Program.cpp - The Program class is declared/defined here, it contains all the program's data, menu functions, and additional helper functions. You will be updating this file.
- ProductFamily.h and ProductFamily.cpp - The GenericProduct class is declared here, you will declare your child classes in the .h file and define their functions in the .cpp file.



## C++ Best Practices to adhere to

See the textbook section "Code and UI style guide" under the "Reference" section for more information.

- Give your arrays plural names, such as "courses" (not "course"), "products", "rooms".
- Singular variable should have singular names; "course", "new_course", "newRoom".
- Be consistent with naming conventions: All your functions should follow the same naming style, and all of your variables should share the same naming style.
  - Examples: CamelCaseFunctionName, lower_case_variable_name, lowerCaseVariableNameCamelCase
- Utilize the Auto-Format tool in VS Code to make sure the code stays clean and readable.
- Build and run everything with `g++ *.h *.cpp` - you shouldn't have entire programs in one source file.


## Code requirements

1. Within ProductFamily.h you should declare two new Product child classes. These classes should inherit from GenericProduct. (See the Program Options sections for suggestions on product ideas)
  - Declare functions such as Get and Set functions (and a Constructor might be useful!). Reference my GenericProduct for ideas.
  - Define your class' functions within ProductFamily.cpp.

2. Within Program.h you will see a vector<GenericProduct> genericProducts declared as a private member variable. You will also need to create two vectors for your two different product types. (In CS 235, we can use pointers and polymorphism to store all different product types in one vector, but we're not covering that here!)

3. Within Program.h you will see functions that handle the Generic Products:
   - void Menu_GenericProducts_Add();
   - void Menu_GenericProducts_Edit();
   - void Display_GenericProducts();
   - Add the same types of functions for each of your 2 classes.

4. Within Program.cpp, the following functions will need to be updated:
   - void Program::SetupData() - Set up some starting data for your 2 product vectors.
   - void Program::Menu_Main() - Add options for the "Add" and "Edit" submenus to the main menu display. Within the switch statement, detect the user's input and call the appropriate menu function.
   - void Program::Menu_ViewAllProducts() - After you have your Display_XyzProducts functions implemented, call those here.
   - Define your "Add", "Edit", and "Display" functions for your 2 classes in this file as well.



## Program options

When your new Products inherit from GenericProduct they will automatically inherit string name and float price. Your new classes will need to contain at least one additional piece of information each. Here are some examples:

- FoodProduct, adds int calories, bool is_vegan
- MovieProduct, adds string rating, int year
- BookProduct, adds string isbn, string author
- CarProduct, adds string make, string model

Or you can come up with other products to use. Remember that you need to have 2 new product types.



## Turn in
Upload your project to your repository in the appropriate folder ("Project4").
Upload a screenshot of your program running as part of this Canvas assignment.
