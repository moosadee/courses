#include "Program.h"

#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

//! Starting point of the Program
void Program::Start()
{
  cout << left << fixed << setprecision( 2 );
  SetupData();
  Menu_Main();
}

//! Used to set up program starter data
void Program::SetupData()
{
  // Setting up generic products. Note: I've created a CONSTRUCTOR in the GenericProduct function which allows me to initialize an item WITHIN my vector push.
  genericProducts.push_back( GenericProduct( "Gumball",     0.75 ) );
  genericProducts.push_back( GenericProduct( "Pack-o-gum",  0.80 ) );
  genericProducts.push_back( GenericProduct( "Spice drops", 0.50 ) );

  // Set up your products here.
}

//! The main menu
void Program::Menu_Main()
{
  bool menu_running = true;
  while ( menu_running )
  {
    ClearScreen();
    DisplayHeader( "MAIN MENU" );

    cout << "0. Exit" << endl;
    cout << endl;
    cout << "1. View all products" << endl;
    cout << endl;
    cout << "- Generic Products ------------" << endl;
    cout << "2. Add a new generic product" << endl;
    cout << "3. Edit an existing generic product" << endl;

    cout << endl;
    cout << "- OTHER1 Products -------------" << endl; // TODO: Make this label more descriptive
    // TODO: Add your menu options here

    cout << endl;
    cout << "- OTHER2 Products -------------" << endl; // TODO: Make this label more descriptive
    // TODO: Add your menu options here

    cout << endl;
    int choice = GetValidInput( 0, 3 ); // TODO: Make sure to update this when you add menu options!

    switch( choice )
    {
      case 0: menu_running = false;         break;
      case 1: Menu_ViewAllProducts();       break;
      case 2: Menu_GenericProducts_Add();   break;
      case 3: Menu_GenericProducts_Edit();  break;
      // TODO: Add your additional menu item calls here.
    }
  }
}

//! Submenu to show all products stored in the system
void Program::Menu_ViewAllProducts()
{
  ClearScreen();
  DisplayHeader( "VIEW ALL PRODUCTS" );

  // Table header, modify if you're adding columns
  // Default console width is 80 characters.
  cout
    << setw( 5 )  << "ID"
    << setw( 20 ) << "NAME"
    << setw( 10 ) << "PRICE"
    << setw( 10 ) << "OTHER1" // TODO: SUBCLASS 1 - Make this label more descriptive
    << setw( 10 ) << "OTHER2" // TODO: SUBCLASS 1 - Make this label more descriptive
    << setw( 10 ) << "OTHER3" // TODO: SUBCLASS 2 - Make this label more descriptive
    << setw( 10 ) << "OTHER4" // TODO: SUBCLASS 2 - Make this label more descriptive
    << endl;
  cout << string( 80, '-' ) << endl;

  // Display generic products
  Display_GenericProducts();

  PressEnterToContinue();
}



// - Generic product functions ----------------------------------------------//
//! Submenu to add a new Generic Product
void Program::Menu_GenericProducts_Add()
{
  ClearScreen();
  DisplayHeader( "ADD GENERIC PRODUCT" );

  string new_name;
  float new_price;

  cout << "Enter new name: ";
  cin.ignore();
  getline( cin, new_name );

  cout << "Enter new price: $";
  cin >> new_price;

  GenericProduct new_product( new_name, new_price ); // Setup via constructor
  genericProducts.push_back( new_product );

  cout << endl << "Item added." << endl;

  PressEnterToContinue();
}

//! Submenu to edit an existing Generic Products
void Program::Menu_GenericProducts_Edit()
{
  ClearScreen();
  DisplayHeader( "EDIT GENERIC PRODUCT" );

  Display_GenericProducts();
  cout << endl << "Edit which product?" << endl;
  int index = GetValidInput( 0, genericProducts.size()-1 );

  cout << endl << "FIELDS:" << endl;
  cout << "1. Name   (Currently " << genericProducts[index].GetName() << ")" << endl;
  cout << "2. Price  (Currently $" << genericProducts[index].GetPrice() << ")" << endl;
  cout << endl << "Update which field?" << endl;
  int edit = GetValidInput( 1, 2 );

  if ( edit == 1 )
  {
    string new_name;
    cout << "Enter new name: ";
    cin.ignore();
    getline( cin, new_name );

    genericProducts[index].SetName( new_name );

    cout << endl << "Name updated." << endl;
  }
  else if ( edit == 2 )
  {
    float new_price;
    cout << "Enter new price: $";
    cin >> new_price;

    genericProducts[index].SetPrice( new_price );
  }

  PressEnterToContinue();
}

//! Helper function to display Generic Products in row format
void Program::Display_GenericProducts()
{
  for ( size_t i = 0; i < genericProducts.size(); i++ )
  {
    cout
      << setw( 5 )  << i
      << setw( 20 ) << genericProducts[i].GetName()
      << setw( 10 ) << genericProducts[i].GetPrice()
      << setw( 10 ) << ""
      << setw( 10 ) << ""
      << endl;
  }
}



// - YOUR product functions -------------------------------------------------//






// - Helper functions -------------------------------------------------------//
//! Gets input from the user, makes sure it is between min-max and returns the result
int Program::GetValidInput( int min, int max )
{
  int input;
  cout << "(" << min << "-" << max <<"): ";
  cin >> input;
  while ( input < min || input > max )
  {
    cout << "INVALID SELECTION! Try again." << endl;
    cout << ">> ";
    cin >> input;
  }
  return input;
}

//! Draws a bunch of newlines to clear the screen
void Program::ClearScreen() const
{
  cout << string( 80, '\n' ); // clear screen
}

//! Formatted header to display to the screen
void Program::DisplayHeader( string title ) const
{
  cout << string( 80, '-' ) << endl
       << title << endl
       << string( 80, '-' ) << endl;
}

//! Workaround to get a "Press ENTER to continue" function working
void Program::PressEnterToContinue() const
{
  cout << endl;
  cout << endl << "Press a ENTER to continue" << endl;
  string a;
  getline( cin, a );
  getline( cin, a );
}

