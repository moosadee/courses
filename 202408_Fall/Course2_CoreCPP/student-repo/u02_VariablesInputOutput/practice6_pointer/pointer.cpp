// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <iomanip>    // Library for text formatting, has `setw`, `left`.`
#include <string>     // Library that contains `string` types
using namespace std;  // Using the C++ STanDard libraries

// - PROGRAM CODE -----------------------------------------------------------//
int main()
{
  cout << " SCHOOL SUPPLIES " << endl << endl;

  // Variable declarations
  string item1 = "Notebook";
  string item2 = "Pencil";
  string item3 = "Calculator";
  string item4 = "???";

  // Pointer variable declaration
  string * item_pointer = nullptr;

  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Point `item_pointer` to item 1, 2, or 3.
  

  // TODO: Display "I am going to buy", and then dereference the pointer.
  

  // TODO: Point `item_pointer` to item 4.
  

  // TODO: Dereference `item_pointer` to change item4's value to an item of your choice.
  
  // -------------------------------------------------------------------------

  cout << endl << "The available items are:" << endl;
  cout << "- " << item1 << endl;
  cout << "- " << item2 << endl;
  cout << "- " << item3 << endl;
  cout << "- " << item4 << endl;

  // Return 0 means quit program with no errors, in this context.
  cout << endl << " THE END " << endl;
  return 0;
}
