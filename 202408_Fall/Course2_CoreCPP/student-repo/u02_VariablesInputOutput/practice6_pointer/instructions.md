# Build program:
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

# Build program:
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! MAKE SURE TO SAVE YOUR FILE FIRST !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

g++ *.cpp

# Run program:
./a.out (For Linux/Mac)
.\a.exe (For Windows)

# Example program output
```
SCHOOL SUPPLIES

I am going to buy a Notebook

The available items are:
- Notebook
- Pencil
- Calculator
- Eraser

THE END
```

# Instructions
Under the TODO items --
- ASSIGN `item_pointer` the ADDRESS of one of the following items: `item1`, `item2`, or `item3`.
- DISPLAY "I am going to buy a" to the screen, and then dereference the pointer to display the value.
- ASSIGN `item_pointer` the address of `item4`.
- ASSIGN a school supply name to a dereferenced `item_pointer`.`

# Reference
Use `&VARIABLE` to get the address of VARIABLE.
Use `*POINTER` to derererence the pointer.
