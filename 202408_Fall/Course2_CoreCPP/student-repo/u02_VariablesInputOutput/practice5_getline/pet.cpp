// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <iomanip>    // Library for text formatting, has `setw`, `left`.`
#include <string>     // Library that contains `string` types
using namespace std;  // Using the C++ STanDard libraries

// - PROGRAM CODE -----------------------------------------------------------//
int main()
{
  cout << " PET PROGRAM " << endl << endl;

  // Variable declarations
  string your_name;
  string pet_name, pet_animal, pet_breed;
  int    pet_age;

  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Implement the rest of the program...

  // -------------------------------------------------------------------------
  // Return 0 means quit program with no errors, in this context.
  cout << endl << " THE END " << endl;
  return 0;
}
