# Graded: Pizza for guests program
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).


## Building and running the program

From within the **terminal**, you can build the program using the `g++` compiler:

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! MAKE SURE TO SAVE YOUR FILE FIRST !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

```
g++ *.cpp
```

This will generate an executable program. On **Windows*, it will be `a.exe` file.
On **Linux** and **Mac**, it will be `a.out` file. "a" is the default name,
and later we will see how to choose the name for our program, but the default is fine for now.

After the program has built, you can use

```
ls
```

to view the files in the folder. There should be an `a.out` or `a.exe`.

To run the program in Windows, use:

```
.\a.exe
```

To run the program in Linux/Mac, use:

```
./a.out
```


## Introduction

This program asks the user how many guests will be at their party and how many slices of pizza are available and will calculate how many slices of pizza each guest should get.

The BasicsProgram.cpp file contains a `void Main()`, which has already been implemented. This is where the user will be asked to enter the amount of guests and the amount of pizza slices. Then, the function `SlicesPerPerson` is called, and given that data. You'll implement this function.




--------------------------------------------------------------------------------
## Example output

```
1. Run AUTOMATED TESTS
2. Run PROGRAM
>> 2

################################################################################
# PIZZA FOR GUESTS #
####################

INSIDE Main FUNCTION
How many guests will be at your party?: 100
How many slices of pizza are available?: 450

INSIDE SlicesPerPerson FUNCTION
VARIABLE INFO:
pizza_slices        value: 450 	 address: 0x7ffd6978c9d8
guest_count         value: 100 	 address: 0x7ffd6978c9dc
slices_per_person   value: 4 	 address: 0x7ffd6978c9e4

INSIDE Main FUNCTION

Each guest gets 4 slices of pizza.

VARIABLE INFO:
pizza_slices        value: 450 	 address: 0x7ffd6978ca10
guest_count         value: 100 	 address: 0x7ffd6978ca0c
slices_per_person   value: 4 	 address: 0x7ffd6978ca14
```

For graded programs, automated tests are also provided where you can verify your work.
Make sure all tests pass:

```
[PASS]  TEST 1, PROGRAM(50, 100) = 2

(...)

[PASS]  TEST 2, PROGRAM(10, 75) = 7
```

--------------------------------------------------------------------------------
## Updating int SlicesPerPerson( int guest_count, int pizza_slices )

Within "BasicsProgram.cpp" we have a `SlicesPerPerson` function that you'll be working within.

This function has the following *input parameter variables*:

- `int guest_count`  The total amount of guests coming to the party
- `int pizza_slices` The total slices of pizza available at the party

This function will calculate the amount of slices per person,
and should display all of the information - total guests, slices of pizza, how much pizza each guest gets.

You can calculate the slices per person, you just divide pizza slices by the guest count.
Store the result in a variable.

Have the function return the amount of pizza slices per person at the end of the function.

--------------------------------------------------------------------------------
## Running the program - looking at addresses

When you run the program, the addresses of the variables will change each run. Also notice that there is text displayed showing when the we're "INSIDE Main FUNCTION" and "INSIDE SlicesPerPerson FUNCTION".

It is easy to overlook, but make sure to notice how the variables `pizza_slices`, `guest_count`, and `slices_per_person` have different ADDRESSES inside of Main and inside of SlicesPerPerson. These variables have the *same name*, but because they exist in *different functions*, they are totally different variables.

```
INSIDE Main FUNCTION
How many guests will be at your party?: 20
How many slices of pizza are available?: 500

INSIDE SlicesPerPerson FUNCTION
VARIABLE INFO:
pizza_slices        value: 500 	 address: 0x7ffcd5d95558
guest_count         value: 20 	 address: 0x7ffcd5d9555c
slices_per_person   value: 25 	 address: 0x7ffcd5d95564

INSIDE Main FUNCTION

Each guest gets 25 slices of pizza.

VARIABLE INFO:
pizza_slices        value: 500 	 address: 0x7ffcd5d95590
guest_count         value: 20 	 address: 0x7ffcd5d9558c
slices_per_person   value: 25 	 address: 0x7ffcd5d95594
```

We will look at functions more later, as well as talking about *variable scope*.
