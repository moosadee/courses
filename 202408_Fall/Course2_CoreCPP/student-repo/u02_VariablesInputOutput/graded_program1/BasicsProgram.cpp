// Use `g++ *.cpp` to build this program
// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
using namespace std;  // Using the C++ STanDard libraries

/**
   @param  guest_count   The amount of guests coming to the party
   @param  pizza_slices  The amount of pizza slices available
   @return int           Returns the amount of pizza slices each guest gets
*/
int SlicesPerPerson( int guest_count, int pizza_slices )
{
  cout << "INSIDE SlicesPerPerson FUNCTION" << endl;
  int slices_per_person;

  // - STUDENT CODE ----------------------------------------------------------
  // TODO: CALCULATE how many slices of pizza each guest gets
  
  // -------------------------------------------------------------------------

  // Display the variables' values and addresses
  cout << "VARIABLE INFO:" << endl;
  cout << "pizza_slices        value: " << pizza_slices << " \t address: " << &pizza_slices << endl;
  cout << "guest_count         value: " << guest_count  << " \t address: " << &guest_count << endl;
  cout << "slices_per_person   value: " << slices_per_person << " \t address: " << &slices_per_person << endl;
  
  // Return the amount of slices per person
  return slices_per_person;
}


void Program()
{
  cout << "INSIDE Main FUNCTION" << endl;
  // Declare variables
  int guest_count{0};
  int pizza_slices{0};
  int slices_per_person{0};

  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Ask the user to enter how many guests and slices of pizza there are.
  
  
  // TODO: Call the SlicesPerPerson function to figure out how many slices per person
  
  // -------------------------------------------------------------------------

  cout << endl << "INSIDE Main FUNCTION" << endl;
  cout << endl << "Each guest gets " << slices_per_person << " slices of pizza." << endl << endl;

  // Display variable information
  cout << "VARIABLE INFO:" << endl;
  cout << "pizza_slices        value: " << pizza_slices << " \t address: " << &pizza_slices << endl;
  cout << "guest_count         value: " << guest_count  << " \t address: " << &guest_count << endl;
  cout << "slices_per_person   value: " << slices_per_person << " \t address: " << &slices_per_person << endl;
}


//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! DON'T MODIFY ANYTHING BELOW THIS POINT! !!
//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! I've written custom main code and an automated tester.
//   !! You only need to work with the functions above this.
enum class ProgramAction { TEST, PROGRAM };
void Tester();


int main( int argCount, char* args[] )
{
  string program_name = "PIZZA FOR GUESTS";
  ProgramAction action;
  if ( argCount > 1 && args[1] == "test" )
    {
      action = ProgramAction::TEST;
    }
  else
    {
      std::cout << "1. Run AUTOMATED TESTS" << std::endl;
      std::cout << "2. Run PROGRAM" << std::endl;
      int choice;
      std::cout << ">> ";
      std::cin >> choice;

      switch( choice )
        {
        case 1: action = ProgramAction::TEST; break;
        case 2: action = ProgramAction::PROGRAM; break;
        }
    }

  if ( action == ProgramAction::TEST )
    {
      Tester();
    }
  else
    {
      program_name = "# " + program_name + " #";
      cout << endl << string( 80, '#' ) << endl;
      cout << program_name << endl;
      cout << string( program_name.size(), '#' ) << endl << endl;

      Program();
    }

  return 0;
}

// - AUTOMATED TESTER -------------------------------------------------------//
void Tester()
{
  const string GRN = "\033[0;32m"; const string RED = "\033[0;31m"; const string BOLD = "\033[0;35m"; const string CLR = "\033[0m";

  // (Automated test):
  const int TOTAL_TESTS = 2;
  int    in1[TOTAL_TESTS]; // inputs 1
  int    in2[TOTAL_TESTS]; // inputs 2
  int    exo[TOTAL_TESTS]; // expected output
  int    aco[TOTAL_TESTS]; // actual output

  // Setup test 1
  in1[0] = 50;
  in2[0] = 100;
  exo[0] = 2;

  // Setup test 2
  in1[1] = 10;
  in2[1] = 75;
  exo[1] = 7;

  // Run tests
  for ( int i = 0; i < TOTAL_TESTS; i++ )
  {
    aco[i] = SlicesPerPerson( in1[i], in2[i] );

    if ( aco[i] == exo[i] )
    {
      // PASS
      cout << GRN << "[PASS] ";
      cout << " TEST " << i+1 << ", SlicesPerPerson(" << in1[i] << ", " << in2[i] << ") = " << aco[i] << endl;
    }
    else
    {
      // FAIL
      cout << RED << "[FAIL] ";
      cout << " TEST " << i+1 << ", SlicesPerPerson(" << in1[i] << ", " << in2[i] << ")" << endl;
      cout << "   EXPECTED OUTPUT: [" << exo[i] << "]" << endl;
      cout << "   ACTUAL OUTPUT:   [" << aco[i] << "]" << endl;
    }
    cout << CLR;
  }
}
