# Build program:
Open the terminal (view > terminal) (View > Terminal) and make sure you're in the
"practice1_greeting" folder (use the cd command to navigate):
cd practice1_greeting

(Or you can go to Open > Folder in VS Code and open the specific folder you're working in.)

One in the folder, you can build the project.

# Build program:
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! MAKE SURE TO SAVE YOUR FILE FIRST !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

g++ *.cpp



# Run program:
./a.out (For Linux/Mac)
.\a.exe (For Windows)


# Example program output
```
Hello, I'm: NAME
My favorite color is: COLOR
This semester I hope to learn about... SOMETHING
```


# Instructions
Use cout statements to DISPLAY your name, favorite color, and something you want to learn about.


# Reference
cout statements look like this:
`cout << "TEXT";`
`cout << "TEXT" << endl;`

