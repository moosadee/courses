// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
using namespace std;  // Using the C++ STanDard libraries

// - PROGRAM CODE -----------------------------------------------------------//
int main()
{
  cout << " GREETINGS PROGRAM " << endl << endl;

  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Update the placeholder text below, NAME, COLOR, and SOMETHING, with your own text.

  // -------------------------------------------------------------------------

  // Return 0 means quit program with no errors, in this context.
  cout << endl << " THE END " << endl;
  return 0;
}

