# Build program:
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

# Build program:
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! MAKE SURE TO SAVE YOUR FILE FIRST !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

g++ *.cpp

# Run program:
./a.out (For Linux/Mac)
.\a.exe (For Windows)

# Example program output
```
ROOM PROGRAM

Enter a width: 5
Enter a length: 6
Perimeter: 22
Area: 30

THE END
```

# Instructions
- DECLARE float variables to hold a width and a length.
- DISPLAY a prompt to the user asking for them to enter a width. GET their input and STORE it in the width variable.
- DISPLAY a prompt to the user asking for them to enter a length. GET their input and STORE it in the length variable.
- DECLARE a float variable for the perimeter. CALCULATE the perimeter and store it in this variable.
- DECLARE a float variable for the area. CALCULATE the area and store it in this variable.
- DISPLAY the perimeter and area results. Make sure to label them so the user can tell which number is which.
