// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
using namespace std;  // Using the C++ STanDard libraries

// - PROGRAM CODE -----------------------------------------------------------//
int main()
{
  cout << " ROOM PROGRAM " << endl << endl;

  float width;
  float length;
  float perimeter;
  float area;
    
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Ask the user to enter a width and a length
  

  // TODO: Calculate the perimeter and area
  

  // TODO: Display results
  
  // -------------------------------------------------------------------------

  // Return 0 means quit program with no errors, in this context.
  cout << endl << " THE END " << endl;
  return 0;
}
