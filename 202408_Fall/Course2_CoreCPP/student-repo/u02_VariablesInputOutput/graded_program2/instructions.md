# Graded: Pizza for guests program
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

## Building and running the program

From within the **terminal**, you can build the program using the `g++` compiler:

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! MAKE SURE TO SAVE YOUR FILE FIRST !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

```
g++ *.h *.cpp
```

This will generate an executable program. On **Windows*, it will be `a.exe` file.
On **Linux** and **Mac**, it will be `a.out` file. "a" is the default name,
and later we will see how to choose the name for our program, but the default is fine for now.

After the program has built, you can use

```
ls
```

to view the files in the folder. There should be an `a.out` or `a.exe`.

To run the program in Windows, use:

```
.\a.exe
```

To run the program in Linux/Mac, use:

```
./a.out
```

## Example output

```
1. Run AUTOMATED TESTS
2. Run PROGRAM
>> 2

################################################################################
# GUEST SELECTOR #
##################

VARIABLE INFO:
guest1   value: Hikaru 	 address: 0x7fff7a54dcb0
guest2   value: Umi 	 address: 0x7fff7a54dcd0
guest3   value: Fuu 	 address: 0x7fff7a54dcf0
guest_pointer IS POINTING TO 0x7fff7a54dcd0

"Umi" selected.
```

For graded programs, automated tests are also provided where you can verify your work.
Make sure all tests pass:

```
[PASS]  TEST 1, ChooseGuest() = Umi
```



## Introduction

The PointerProgram.cpp there is a function `string ChooseGuest` that you will implement. Within this function, three string variables are declared with different guest names. After the "VARIABLE INFO" display table, a `guest_pointer` is declared. You will be working with this pointer.




## Updating ChooseGuest

To access the address of a variable, we use `&VARIABLE`. We can assign memory addresses to pointer variables like this:
`POINTER = &VARIABLE;`

To dereference a pointer, we use `*POINTER`. This gives us access to the *value* of the address it's pointing to, and we can either read the data or overwrite it with new data. `*POINTER` can be used like a normal variable.

Beneath the first TODO item, assign `guest_pointer` the address of `guest2`.

Beneath the second TODO item, return the dereferenced `guest_pointer`.
