// Use `g++ *.cpp` to build this program
// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
using namespace std;  // Using the C++ STanDard libraries

string ChooseGuest()
{
  // Declare variables
  string guest1 = "Hikaru";
  string guest2 = "Umi";
  string guest3 = "Fuu";

  // Display variable information
  cout << "VARIABLE INFO:" << endl;
  cout << "guest1   value: " << guest1 << " \t address: " << &guest1 << endl;
  cout << "guest2   value: " << guest2 << " \t address: " << &guest2 << endl;
  cout << "guest3   value: " << guest3 << " \t address: " << &guest3 << endl;

  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Declare pointer variable

  // TODO: Assign `guest_pointer` the address of `guest2`.

  // TODO: Return the dereferenced `guest_pointer`.

  return nullptr; // TODO: REMOVE THIS LINE!
  // -------------------------------------------------------------------------
}

void Program()
{
  string result = ChooseGuest();
  cout << endl << "\"" << result << "\" selected." << endl;
}

//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! DON'T MODIFY ANYTHING BELOW THIS POINT! !!
//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! I've written custom main code and an automated tester.
//   !! You only need to work with the functions above this.
enum class ProgramAction { TEST, PROGRAM };
void Tester();

int main( int argCount, char* args[] )
{
  string program_name = "GUEST SELECTOR";
  ProgramAction action;
  if ( argCount > 1 && args[1] == "test" )
    {
      action = ProgramAction::TEST;
    }
  else
    {
      std::cout << "1. Run AUTOMATED TESTS" << std::endl;
      std::cout << "2. Run PROGRAM" << std::endl;
      int choice;
      std::cout << ">> ";
      std::cin >> choice;

      switch( choice )
        {
        case 1: action = ProgramAction::TEST; break;
        case 2: action = ProgramAction::PROGRAM; break;
        }
    }

  if ( action == ProgramAction::TEST )
    {
      Tester();
    }
  else
    {
      program_name = "# " + program_name + " #";
      cout << endl << string( 80, '#' ) << endl;
      cout << program_name << endl;
      cout << string( program_name.size(), '#' ) << endl << endl;
      
      Program();
    }

  return 0;
}

// - AUTOMATED TESTER -------------------------------------------------------//
void Tester()
{
  const string GRN = "\033[0;32m"; const string RED = "\033[0;31m"; const string BOLD = "\033[0;35m"; const string CLR = "\033[0m";

  // (Automated test):
  const int TOTAL_TESTS = 1;
  //int    in1[TOTAL_TESTS]; // inputs 1
  //int    in2[TOTAL_TESTS]; // inputs 2
  string    exo[TOTAL_TESTS]; // expected output
  string    aco[TOTAL_TESTS]; // actual output

  // Setup test 1
  //in1[0] = 50;
  //in2[0] = 100;
  exo[0] = "Umi";

  // Run tests
  for ( int i = 0; i < TOTAL_TESTS; i++ )
  {
    aco[i] = ChooseGuest();

    if ( aco[i] == exo[i] )
    {
      // PASS
      cout << GRN << "[PASS] ";
      cout << " TEST " << i+1 << ", ChooseGuest() = " << aco[i] << endl;
    }
    else
    {
      // FAIL
      cout << RED << "[FAIL] ";
      cout << " TEST " << i+1 << ", ChooseGuest()" << endl;
      cout << "   EXPECTED OUTPUT: [" << exo[i] << "]" << endl;
      cout << "   ACTUAL OUTPUT:   [" << aco[i] << "]" << endl;
    }
    cout << CLR;
  }
}
