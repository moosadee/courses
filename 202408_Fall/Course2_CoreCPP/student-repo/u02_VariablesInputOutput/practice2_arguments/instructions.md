# Build program:
Open the terminal (view > terminal) (View > Terminal) and make sure you're in the
"practice2_arguments" folder (use the cd command to navigate).

(Or you can go to Open > Folder in VS Code and open the specific folder you're working in.)

Go up one directory:
cd ../

Go into the practice 2 folder:
cd practice2_arguments

# Build program:
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! MAKE SURE TO SAVE YOUR FILE FIRST !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

g++ *.cpp


# Run program:
./a.out (For Linux/Mac)
.\a.exe (For Windows)

# Example program output
```
Hello, I'm: NAME
My favorite color is: COLOR
This semester I hope to learn about... SOMETHING

The size of an integer is 4 bytes
The size of a float is    4 bytes
The size of a char is     1 bytes
The size of a bool is     1 bytes
The size of a string is   32 bytes

THE END
```

# Instructions
Take your code from greetings_v1 and add three string variables:
`name`, `color`, and `learn_about`.

Set these variables to your answers from before, like:
`string name = "NAME";`

Then, update your cout DISPLAY statements to use the variables instead of hard-coding your answers.

Afterwards, we're also going to use the sizeof() function to display the size of various data types. You can do it like this:
`cout << "The size of an integer is " << sizeof( int ) << " bytes" << endl;`
Do the same for float, char, bool, and string.

# Reference
You can string together text literals and variable values in one cout statement:
`cout << "LABEL: " << variablename << endl;`

You can get the size of a data type or a variable by using sizeof( THING ).
