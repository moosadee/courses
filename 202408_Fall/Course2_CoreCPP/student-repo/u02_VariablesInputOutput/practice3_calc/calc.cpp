// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
using namespace std;  // Using the C++ STanDard libraries

// - PROGRAM CODE -----------------------------------------------------------//
int main()
{
  cout << " CALCULATOR PROGRAM " << endl << endl;

  // Variable declarations
  float num1, num2;
  float sum, difference, product, quotient;
  
  // - STUDENT CODE ----------------------------------------------------------
  // TODO: Ask the user to enter values for num1 and num2
  
  // TODO: Calculate the sum, difference, product, and quotient
  
  // TODO: Display the results
  
  // -------------------------------------------------------------------------



  cout << "VARIABLE INFORMATION" << endl;
  cout << "num1       Value: " << num1       << "\t Address: " << &num1 << endl;
  cout << "num2       Value: " << num2       << "\t Address: " << &num2 << endl;
  cout << "sum        Value: " << sum        << "\t Address: " << &sum << endl;
  cout << "difference Value: " << difference << "\t Address: " << &difference << endl;
  cout << "product    Value: " << product    << "\t Address: " << &product << endl;
  cout << "quotient   Value: " << quotient   << "\t Address: " << &quotient << endl;

  // Return 0 means quit program with no errors, in this context.
  cout << endl << " THE END " << endl;
  return 0;
}
