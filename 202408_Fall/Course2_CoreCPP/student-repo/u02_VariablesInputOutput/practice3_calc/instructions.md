# Build program:
Open the folder for this assignment (either via terminal (view > terminal) with the `cd` command, or in VS Code with File > Open Folder).

# Build program:
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! MAKE SURE TO SAVE YOUR FILE FIRST !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

g++ *.cpp

# Run program:
./a.out (For Linux/Mac)
.\a.exe (For Windows)


# Example program output
```
CALCULATOR PROGRAM

Please enter num1: 2
Please enter num2: 4

ARITHMETIC:
SUM:        2 + 4 = 6
DIFFERENCE: 2 - 4 = -2
PRODUCT:    2 * 4 = 8
QUOTIENT:   2 / 4 = 0.5

VARIABLE INFORMATION
num1       Value: 2	 Address: 0x7fffc52f6ea0
num2       Value: 4	 Address: 0x7fffc52f6ea4
sum        Value: 6	 Address: 0x7fffc52f6ea8
difference Value: -2	 Address: 0x7fffc52f6eac
product    Value: 8	 Address: 0x7fffc52f6eb0
quotient   Value: 0.5	 Address: 0x7fffc52f6eb4

THE END
```

# Instructions
- DECLARE a couple of float variables.
- DISPLAY a message to the user to enter a first number. GET their input from the keyboard and STORE it in your first variable.
- DISPLAY a message to the user to enter a second number. GET their input from the keyboard and STORE it in your second variable.

- DECLARE float variables for sum, difference, product, and quotient.
- ASSIGN a value to your sum variable: first number + second number.
- ASSIGN a value to your difference variable: first number - second number.
- ASSIGN a value to your product variable: first number * second number.
- ASSIGN a value to your quotient variable: first number / second number.
- DISPLAY the sum, difference, product, and quotient results. Make sure to also label them clearly so the user can tell which number is which result.

- DISPLAY the header "VARIABLE INFORMATION".
- DISPLAY "num1", then the value of num1 (`num1`), and the address of num1 (`&num1`), like this: 
    `cout << "num1       Value: " << num1       << "\t Address: " << &num1 << endl;`
- Do the same for num2, sum, difference, product, and quotient variables.


# Reference
Variable declaration:
`DATATYPE VARIABLENAME;`

Input into a variable:
`cin >> VARIABLENAME;`

Assignment operator:
`VARIABLE = VALUE:`
`VARIABLE = ITEM1 + ITEM2;`

Address-of operator: & before a variable name
