#include <iostream>   // IMPORT INPUT AND OUTPUT
using namespace std;  // USING THE C++ STANDARD LIBRARY

int main() // PROGRAM STARTING POINT
{ // START OF CODE BLOCK
    
    // The following writes text out to the screen
    // when the program is run:
    
    cout << "Welcome to C++!" << endl;
    cout << "My name is... SOMEBODY" << endl;
    
    return 0; // PROGRAM ENDS
} // END OF CODE BLOCK
