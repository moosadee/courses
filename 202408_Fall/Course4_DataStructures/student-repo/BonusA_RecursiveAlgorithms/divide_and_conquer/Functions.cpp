#include "Functions.h"

#include <iostream>
using namespace std;

/** Non-recursive starting function
    - If the size of the vector is 0, then just return 0.
    - Otherwise, kick off the recursive function call, using [0, letters.size()) as the range.
      Note: 0 is included, letters.size() is not included, so pass letters.size()-1.
 */
char FindMax( vector<char> letters )
{
  // TODO: Implement function!
  return 0; // TODO: REMOVE ME!
}

/** Recursive FindMax function
    1. If `i` and `j` are the same then return the letter at position `i`.
    2. Calculate the `mid` point between `i` and `j`. Store in a variable `mid`.
    3. Create a `char maxLeft` variable, call the `FindMax_Recursive` function going from [i, mid] with the `letters` vector.
    4. Create a `char maxRight` variable, call the `FindMax_Recursive` function going from [mid+1, j] with the `letters` vector.
    5. If `maxLeft` is greater than `maxRight`, return `maxLeft`. Otherwise return `maxRight`.
 */
char FindMax_Recursive( size_t i, size_t j, vector<char> letters )
{
  // TODO: Implement function!
  return 0; // TODO: REMOVE ME!
}

void DisplayVector( vector<char> items )
{
  cout << "{ ";
  for ( size_t i = 0; i < items.size(); i++ )
  {
    if ( i != 0 ) { cout << ", "; }
    cout << items[i];
  }
  cout << " }";
}

//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! DON'T MODIFY ANYTHING BELOW !!
//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

void Tester()
{
  cout << "-- TESTER " << string( 61, '-' ) << endl;
  const std::string GRN = "\033[0;32m"; const std::string RED = "\033[0;31m"; const std::string BOLD = "\033[0;35m"; const std::string CLR = "\033[0m";
  std::string testName;

  { testName = "FindMax( { 'A', 'D', 'B', 'C' } ) should be 'D'.";
    vector<char> input = { 'A', 'D', 'B', 'C' };
    char expOut = 'D';
    char actOut = FindMax( input );

    if ( actOut == expOut ) { cout << GRN << "[PASS] " << testName << endl; }
    else
    {
      cout << RED << "[FAIL] " << testName << endl;
      cout << "EXPECTED: '" << expOut << "'" << endl;
      cout << "ACTUAL:   '" << actOut << "'" << endl;
      cout << endl;
    }

    std::cout << CLR;
  }

  { testName = "FindMax( { 'Z', 'Y', X', 'W' } ) should be 'Z'.";
    vector<char> input = { 'Z', 'Y', 'X', 'W' };
    char expOut = 'Z';
    char actOut = FindMax( input );

    if ( actOut == expOut ) { cout << GRN << "[PASS] " << testName << endl; }
    else
    {
      cout << RED << "[FAIL] " << testName << endl;
      cout << "EXPECTED: '" << expOut << "'" << endl;
      cout << "ACTUAL:   '" << actOut << "'" << endl;
      cout << endl;
    }

    std::cout << CLR;
  }

  cout << endl;
}



