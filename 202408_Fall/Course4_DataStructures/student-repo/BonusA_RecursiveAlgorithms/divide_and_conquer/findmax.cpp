#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
using namespace std;

#include "Functions.h"

int main()
{
  Tester();

  cout << "-- PROGRAM " << string( 60, '-' ) << endl;
  vector<char> letters;

  // Init random number generator
  srand( time( NULL ) );

  // Generate random numbers
  for ( int i = 0; i < 8; i++ )
  {
    char randval = rand() % 25 + 65;
    letters.push_back( randval );
  }

  cout << "Random letters = ";
  DisplayVector( letters );
  cout << endl << endl;

  char maxValue = FindMax( letters );
  cout << "Max value = '" << maxValue << "'" << endl;

  return 0;
}


