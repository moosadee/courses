#ifndef _FINDMAX
#define _FINDMAX

#include <vector>
using namespace std;

char FindMax( vector<char> letters );
char FindMax_Recursive( size_t i, size_t j, vector<char> letters );
void DisplayVector( vector<char> items );

void Tester();

#endif
