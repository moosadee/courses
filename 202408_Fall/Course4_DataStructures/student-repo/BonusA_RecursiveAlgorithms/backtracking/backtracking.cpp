#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
using namespace std;

#include "Functions.h"

int main()
{
  Tester();

  cout << "-- PROGRAM " << string( 60, '-' ) << endl;
  vector<char> letters;

  // Init random number generator
  srand( time( NULL ) );

  for ( int s = 2; s < 5; s++ )
  {
    cout << "  -- SET SIZE: " << s << " --" << endl;

    letters.clear();

    // Generate random numbers
    for ( int i = 0; i < s; i++ )
    {
      char randval = rand() % 5 + 65 + (i*5);
      letters.push_back( randval );
    }

    vector<bool> used( letters.size() );
    vector< vector<char> > results;
    vector<char> permutations;

    cout << "Random letters = ";
    DisplayVector( letters );
    cout << endl << endl;

    cout << "Find all subsets..." << endl;
    GetAllPermutations( results, letters, permutations, used );
    DisplayVectorOfVectors( results );

    cout << endl << endl;
  }


  return 0;
}

