#ifndef _FINDMAX
#define _FINDMAX

#include <vector>
using namespace std;

void GetAllPermutations( vector< vector<char> >& results, vector<char>& baseSet, vector<char>& permutation, vector<bool>& used );

void DisplayVector( vector<char> items );
void DisplayVectorOfVectors( vector< vector<char > > items );

void Tester();

#endif
