// Reference video: https://www.youtube.com/watch?v=Nabbpl7y4Lo

#include "Functions.h"

#include <iostream>
using namespace std;

/** GetAllPermutations
@param    results         Vector of all permutations
@param    baseSet         Vector of the starter set ("nums" in the example)
@param    permutation     Permutation being built
@param    used            Whether the element at the index of the original base set is accounted for in this permutation

      TERMINATING CASE:
      - If the permutation size matches the baseSet size, then add our current `permutation` to the `results` vector and return.

      CONTINUE:
      1. Loop from [0, baseSet.size()) with a for loop. Within the loop:
          1A. If `used` at position `i` is false, then:
              1Aa: // MAKE A CHOICE
              1Ab. Set `used` at position `i` to true.
              1Ac. Add the `baseSet` element at position `i` to the `permutation`.
              1Ad. Recurse, pass the same arguments in

              1Ae: // UNDO THE CHOICE
              1Af. Set `used` at position `i` to false.
              1Ag. Pop the back-most item from the `permutation`.
 */
void GetAllPermutations( vector< vector<char> >& results, vector<char>& baseSet, vector<char>& permutation, vector<bool>& used )
{
  // TODO: IMPLEMENT ME!
}

//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! DON'T MODIFY ANYTHING BELOW !!
//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

void DisplayVector( vector<char> items )
{
  cout << "{ ";
  for ( size_t i = 0; i < items.size(); i++ )
  {
    if ( i != 0 ) { cout << ", "; }
    cout << items[i];
  }
  cout << " }";
}

void DisplayVectorOfVectors( vector< vector<char > > items )
{
  cout << "{ " << endl;
  for ( size_t out = 0; out < items.size(); out++ )
  {
    cout << "\t";
    DisplayVector( items[out] );
    if ( out != items.size()-1 ) { cout << ", "; }
    if ( (out+1) % 3 == 0 )
    {
      cout << endl;
    }
  }
  cout << " }";
}

void Tester()
{
  cout << "-- TESTER " << string( 61, '-' ) << endl;
  const std::string GRN = "\033[0;32m"; const std::string RED = "\033[0;31m"; const std::string BOLD = "\033[0;35m"; const std::string CLR = "\033[0m";
  std::string testName;

  { testName = "GetAllPermutations( { 'A' } )";
    vector<char> input = { 'A' };
    vector< vector<char > > expOut = { { 'A' } };
    vector< vector<char > > actOut;

    vector<bool> used( input.size() );
    vector< vector<char> > results;
    vector<char> permutations;

    GetAllPermutations( actOut, input, permutations, used );

    bool allElementsAccountedFor = true;
    for ( size_t e = 0; e < expOut.size(); e++ )
    {
      bool elementFound = false;
      for ( size_t a = 0; a < actOut.size(); a++ )
      {
        if ( a == e ) { elementFound = true; break; }
      }

      if ( elementFound == false )
      {
        allElementsAccountedFor = false;
        break;
      }
    }

    if ( allElementsAccountedFor ) { cout << GRN << "[PASS] " << testName << endl; }
    else
    {
      cout << RED << "[FAIL] " << testName << endl;
      cout << "EXPECTED: ";
      DisplayVectorOfVectors( expOut );
      cout << endl;
      cout << "ACTUAL:   ";
      DisplayVectorOfVectors( actOut );
      cout << endl;
      cout << endl;
    }

    std::cout << CLR;
  }

  { testName = "GetAllPermutations( { 'K', 'O' } )";
    vector<char> input = { 'K', 'O' };
    vector< vector<char > > expOut = { { 'K', 'O' }, { 'O', 'K' } };;
    vector< vector<char > > actOut;

    vector<bool> used( input.size() );
    vector< vector<char> > results;
    vector<char> permutations;

    GetAllPermutations( actOut, input, permutations, used );

    bool allElementsAccountedFor = true;
    for ( size_t e = 0; e < expOut.size(); e++ )
    {
      bool elementFound = false;
      for ( size_t a = 0; a < actOut.size(); a++ )
      {
        if ( a == e ) { elementFound = true; break; }
      }

      if ( elementFound == false )
      {
        allElementsAccountedFor = false;
        break;
      }
    }

    if ( allElementsAccountedFor ) { cout << GRN << "[PASS] " << testName << endl; }
    else
    {
      cout << RED << "[FAIL] " << testName << endl;
      cout << "EXPECTED: ";
      DisplayVectorOfVectors( expOut );
      cout << endl;
      cout << "ACTUAL:   ";
      DisplayVectorOfVectors( actOut );
      cout << endl;
      cout << endl;
    }

    std::cout << CLR;
  }

  { testName = "GetAllPermutations( { 'X', 'Y', 'Z' } )";
    vector<char> input = { 'X', 'Y', 'Z' };
    vector< vector<char> > expOut = { { 'X', 'Y', 'Z' }, { 'X', 'Z', 'Y' }, { 'Z', 'X', 'Y' }, { 'Z', 'Y', 'X' }, { 'Y', 'X', 'Z' }, { 'Y', 'Z', 'X' } };
    vector< vector<char> > actOut;

    vector<bool> used( input.size() );
    vector< vector<char> > results;
    vector<char> permutations;

    GetAllPermutations( actOut, input, permutations, used );

    bool allElementsAccountedFor = true;
    for ( size_t e = 0; e < expOut.size(); e++ )
    {
      bool elementFound = false;
      for ( size_t a = 0; a < actOut.size(); a++ )
      {
        if ( a == e ) { elementFound = true; break; }
      }

      if ( elementFound == false )
      {
        allElementsAccountedFor = false;
        break;
      }
    }

    if ( allElementsAccountedFor ) { cout << GRN << "[PASS] " << testName << endl; }
    else
    {
      cout << RED << "[FAIL] " << testName << endl;
      cout << "EXPECTED: ";
      DisplayVectorOfVectors( expOut );
      cout << endl;
      cout << "ACTUAL:   ";
      DisplayVectorOfVectors( actOut );
      cout << endl;
      cout << endl;
    }

    std::cout << CLR;
  }

  cout << endl;
}


