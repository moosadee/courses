#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <queue>
#include <stack>
using namespace std;

struct Node
{
		Node() { leftChild = rightChild = nullptr; }
		Node( int newValue ) {
				leftChild = rightChild = nullptr;
				value = newValue;
		}
		~Node() {
				if ( leftChild != nullptr ) { delete leftChild; }
				if ( rightChild != nullptr ) { delete rightChild; }
		}
		Node* leftChild;
		Node* rightChild;
		int value;
};

/**
	 1. If the current node has a left child and a right child, then:
	 1a. If the left child's value is greater than the right child's value, then:
	 return the current node's value PLUS recursive call to GreedyGetLargest with the left child.
	 1b. If the right child's value is greater than the left child's value, then:
	 return the current node's value PLUS recursive call to GreedyGetLargest with the right child.
   2. If the current node has only one child, then:
	 return the current node's value PLUS recursive call to GreedyGetLargest with that child.
	 3. If the current node has NO children, then:
	 return the current node's value.
*/
int GreedyGetLargest( Node* current )
{
}

int main()
{
		{
				cout << endl << "Tree 1:" << endl;
				// Build a tree:
				Node* root = new Node(0);
				root->leftChild = new Node( 2 );
				root->rightChild = new Node( 3 );
				root->leftChild->leftChild = new Node( 6 );
				root->leftChild->rightChild = new Node( 3 );
				root->rightChild->leftChild = new Node( 2 );
				root->rightChild->rightChild = new Node( 1 );

				int greedyResult = GreedyGetLargest( root );
				cout << "Result: " << greedyResult << endl;

				delete root;
		}

		{
				cout << endl << "Tree 2:" << endl;
				// Build a tree:
				Node* root = new Node(0);
				root->leftChild = new Node( 4 );
				root->rightChild = new Node( 2 );
				root->leftChild->leftChild = new Node( 2 );
				root->leftChild->rightChild = new Node( 1 );
				root->leftChild->leftChild->leftChild = new Node( 2 );
				root->rightChild->leftChild = new Node( 4 );
				root->rightChild->rightChild = new Node( 3 );
				root->rightChild->leftChild->leftChild = new Node( 5 );
				root->rightChild->leftChild->rightChild = new Node( 4 );
				root->rightChild->rightChild->rightChild = new Node( 2 );

				int greedyResult = GreedyGetLargest( root );
				cout << "Result: " << greedyResult << endl;

				delete root;
		}

		return 0;
};
