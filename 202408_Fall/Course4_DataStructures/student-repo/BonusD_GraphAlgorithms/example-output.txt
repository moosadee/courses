

GRAPH 1:
* DFS.......	0	1	2	3	
* BFS.......	0	1	2	3	
* TOPSORT...	0	1	2	3	

GRAPH 2:
* DFS.......	0	1	3	5	2	4	
* BFS.......	0	1	2	3	4	5	
* TOPSORT...	0	1	3	5	2	4	

GRAPH 3:
* DFS.......	0	1	8	7	6	5	3	2	4	10	11	9	
* BFS.......	0	1	9	8	7	6	10	11	5	3	2	4	
* TOPSORT...	12	0	1	8	9	7	10	11	6	5	3	4	2	

GRAPH 4:
* DFS.......	0	7	3	2	12	8	1	10	9	4	6	5	11	
* BFS.......	0	7	9	11	3	6	8	10	2	4	5	1	12	
* TOPSORT...	0	7	11	6	5	3	4	2	12	8	1	10	9	

GRAPH 5:
* TOPSORT...	K	C	A	D	F	B	E	H	G	J	I	

GRAPH 6:
* TOPSORT...	5	4	2	3	1	0	

GRAPH 7:
* TOPSORT...	6	0	1	5	4	2	3	

