#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <queue>
#include <stack>
using namespace std;

#define A 0
#define B 1
#define C 2
#define D 3
#define E 4
#define F 5
#define G 6
#define H 7
#define I 8
#define J 9
#define K 10
#define L 11
#define M 12

struct Node
{
		void Setup( string newData, vector<size_t> adjacent )
		{
				adj = adjacent;
				data = newData;
	  }

		vector<size_t> adj;
		string data;
};

/*
 * The first index represents a node,
 the internal indices represent adjacent nodes.
*/
struct Graph
{
		Graph() {}

		Graph( size_t size )
		{
				nodes.resize( size );
	  }

		/*
			Depth First Search - Recursive!
		  1. set `visited` at the current `vertex` index to true
		  2. display the current nodes[vertex] data
		  3. iterate over all the adjacent nodes in `nodes[vertex].adj`, within this loop:
		     3a. if that adjacent node index hasn't been visited yet, then
			 	     recurse back into DepthFirstSearch, passing in the visited list and this adjacent node index.
		 */
		void DepthFirstSearch( vector<bool>& visited, int vertex )
		{
		}

		/*
			Breadth First Search - Iterative!
 		  1. Create a queue of to-visit indices named `indexQueue`.
			2. Create a vector of booleans named `visited` with the same size as `nodes`. Initialize all items to false.
			3. Set `visited[vertex]` to true.
			4. Push `vertex` to the `indexQueue`.
			5. While the `indexQueue` is NOT empty, do the following:
			   5a. Store the queue's front item in a variable (e.g., `current`).
				 5b. Pop the front-most item out of the queue.
				 5c. Display the `nodes[current]` data.
				 5d. Iterate over all of `node[current]` 's adjacent indices. Within this loop...:
				     5d1. If this adjacent index has NOT been visited, then:
						      Set `visited` at this adjacent index to true.
									Push this adjacent index to the `indexQueue`.
		 */
		void BreadthFirstSearch( int vertex )
		{
	  }

		/*
		 Topological Sort
		 1. Create a stack named `vertexStack` that stores vertex indices.
		 2. Create a vector of booleans named `visited`, with the size of `nodes.size()`, all initialized to false.
		 3. For each index from 0 to `nodes.size()`, do the following:
		    3a. If `visited` at this index is false, then call the `TopologicalSort_Recursive` function, passing in the index, visited vector, and `vertexStack` stack.

		 4. Afterwards we're going to output the found path. While the `vertexStack` is not empty, do the following:
		    4a. Grab the `vertexStack` top, this is an index.
				4b. Display the data of `nodes` at that index.
				4c. Pop the top item off `vertexStack`.
		 */
		void TopologicalSort()
		{
	  }

		/*
		 Topological Sort helper (recursive)
		 1. Set `visited` at the given `vertex` to true.
		 2. For each adjacent index in `nodes[vertex].adj`, do the following:
		    2a. If `visited` at this adjacent index is false, then:
				    Call `TopologicalSort_Recursive` with this index, and the same visited vector and vertexStack.
		 3. Afterwards, push the `vertex` into the `vertexStack`.
		 */
		void TopologicalSort_Recursive( size_t vertex, vector<bool>& visited, stack<size_t>& vertexStack )
		{
	  }

		vector< Node > nodes;
};

int main()
{
		{
				cout << endl << endl << "GRAPH 1:" << endl;
				Graph graph( 4 ); // Vertices 0, 1, 2, and 3.
				vector< bool > visited( graph.nodes.size(), false );

				graph.nodes[0].Setup(  "0", { 1 } );
				graph.nodes[1].Setup(  "1", { 0, 2, 3 } );
				graph.nodes[2].Setup(  "2", { 1, 3 } );
				graph.nodes[3].Setup(  "3", { 2, 3 } );

				cout << "* DFS.......\t";
				graph.DepthFirstSearch( visited, 0 );
				cout << endl << "* BFS.......\t";
				graph.BreadthFirstSearch( 0 );
				cout << endl << "* TOPSORT...\t";
				graph.TopologicalSort();
		}

		{
				cout << endl << endl <<  "GRAPH 2:" << endl;
				Graph graph( 6 );
				vector< bool > visited( graph.nodes.size(), false );

				graph.nodes[0].Setup(  "0", { 1, 2, 3, 4 } );
				graph.nodes[1].Setup(  "1", { 0, 3 } );
				graph.nodes[2].Setup(  "2", { 0, 4, 5  } );
				graph.nodes[3].Setup(  "3", { 0, 1, 5 } );
				graph.nodes[4].Setup(  "4", { 0, 2 } );
				graph.nodes[5].Setup(  "5", { 2, 3 } );

				cout << "* DFS.......\t";
				graph.DepthFirstSearch( visited, 0 );
				cout << endl << "* BFS.......\t";
				graph.BreadthFirstSearch( 0 );
				cout << endl << "* TOPSORT...\t";
				graph.TopologicalSort();
		}

		{
				cout << endl << endl <<  "GRAPH 3:" << endl;
				Graph graph( 13 );
				vector< bool > visited( graph.nodes.size(), false );

				graph.nodes[0].Setup(  "0",  { 1, 9 } );
				graph.nodes[1].Setup(  "1",  { 0, 8 } );
				graph.nodes[2].Setup(  "2",  { 3 } );
				graph.nodes[3].Setup(  "3",  { 2, 4, 5, 7 } );
				graph.nodes[4].Setup(  "4",  { 3 } );
				graph.nodes[5].Setup(  "5",  { 3, 6 } );
				graph.nodes[6].Setup(  "6",  { 5, 7 } );
				graph.nodes[7].Setup(  "7",  { 6, 8, 10, 11 } );
				graph.nodes[8].Setup(  "8",  {1, 7, 9 } );
				graph.nodes[9].Setup(  "9",  { 0, 8 } );
				graph.nodes[10].Setup( "10", { 7, 11 } );
				graph.nodes[11].Setup( "11", { 10, 7 } );
				graph.nodes[12].Setup( "12", {} );

				cout << "* DFS.......\t";
				graph.DepthFirstSearch( visited, 0 );
				cout << endl << "* BFS.......\t";
				graph.BreadthFirstSearch( 0 );
				cout << endl << "* TOPSORT...\t";
				graph.TopologicalSort();
		}

		{
				cout << endl << endl <<  "GRAPH 4:" << endl;
				Graph graph( 13 );
				vector< bool > visited( graph.nodes.size(), false );

				graph.nodes[0].Setup(  "0",  { 7, 9, 11 } );
				graph.nodes[1].Setup(  "1",  { 8, 10 } );
				graph.nodes[2].Setup(  "2",  { 3, 12 } );
				graph.nodes[3].Setup(  "3",  { 2, 4, 7 } );
				graph.nodes[4].Setup(  "4",  { 3 } );
				graph.nodes[5].Setup(  "5",  { 6 } );
				graph.nodes[6].Setup(  "6",  { 5, 7 } );
				graph.nodes[7].Setup(  "7",  { 0, 3, 6, 11 } );
				graph.nodes[8].Setup(  "8",  { 1, 9, 12 } );
				graph.nodes[9].Setup(  "9",  { 0, 8, 10 } );
				graph.nodes[10].Setup( "10", { 1, 9 } );
				graph.nodes[11].Setup( "11", { 0, 7 } );
				graph.nodes[12].Setup( "12", { 2, 8 } );

				cout << "* DFS.......\t";
				graph.DepthFirstSearch( visited, 0 );
				cout << endl << "* BFS.......\t";
				graph.BreadthFirstSearch( 0 );
				cout << endl << "* TOPSORT...\t";
				graph.TopologicalSort();
		}

		{
				cout << endl << endl << "GRAPH 5:" << endl;
				Graph graph( 11 );
				vector< bool > visited( graph.nodes.size(), false );

				graph.nodes[A].Setup( "A", { B, D } );
				graph.nodes[B].Setup( "B", { E } );
				graph.nodes[C].Setup( "C", { F } );
				graph.nodes[D].Setup( "D", { E, F } );
				graph.nodes[E].Setup( "E", { G, H } );
				graph.nodes[F].Setup( "F", { G, I } );
				graph.nodes[G].Setup( "G", { I, J } );
				graph.nodes[H].Setup( "H", { J } );
				graph.nodes[I].Setup( "I", {} );
				graph.nodes[J].Setup( "J", {} );
				graph.nodes[K].Setup( "K", {} );

				cout << "* TOPSORT...\t";
				graph.TopologicalSort();
		}

		{
				cout << endl << endl << "GRAPH 6:" << endl;
				Graph graph( 6 );
				vector< bool > visited( graph.nodes.size(), false );

				graph.nodes[0].Setup( "0", {} );
				graph.nodes[1].Setup( "1", {} );
				graph.nodes[2].Setup( "2", { 3 } );
				graph.nodes[3].Setup( "3", { 1 } );
				graph.nodes[4].Setup( "4", { 0, 1 } );
				graph.nodes[5].Setup( "5", { 2, 0 } );

				cout << "* TOPSORT...\t";
				graph.TopologicalSort();
		}

		{
				cout << endl << endl << "GRAPH 7:" << endl;
				Graph graph( 7 );
				vector< bool > visited( graph.nodes.size(), false );

				graph.nodes[0].Setup( "0", { 1, 2 } );
				graph.nodes[1].Setup( "1", { 2, 5 } );
				graph.nodes[2].Setup( "2", { 3 } );
				graph.nodes[3].Setup( "3", { } );
				graph.nodes[4].Setup( "4", { } );
				graph.nodes[5].Setup( "5", { 3, 4 } );
				graph.nodes[6].Setup( "6", { 1, 5 } );

				cout << "* TOPSORT...\t";
				graph.TopologicalSort();
		}


		cout << endl << endl;

		return 0;
};
