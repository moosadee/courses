PROJECT EXAMPLE
468 record(s) loaded

REPORT: TOTAL PARKS PER REGION

* Central: 71 park(s)
* North: 91 park(s)
* South: 54 park(s)
* UNSPECIFIED: 252 park(s)

REPORT: PARK AREA BREAKDOWN

* 0: 267 park(s)
* 1: 15 park(s)
* 2: 32 park(s)
* 5: 38 park(s)
* 10: 66 park(s)
* 25: 22 park(s)
* 50: 9 park(s)
* 75: 4 park(s)
* 100: 8 park(s)
* 250: 3 park(s)
* 750: 2 park(s)
* 1000: 1 park(s)
* 1500: 1 park(s)
