#include <iostream>
#include <vector>
#include <string>
#include <map>
using namespace std;

#include "CsvParser.h"

struct ReportedIssue
{
	string currentStatus;
	string openDateTime;
	string resolvedDate;
	int daysOpen;
	string reportSource;
	string issueType;
	string issueSubType;
	string departmentWorkGroup;
};

void LoadRecords(string filename, vector<ReportedIssue>& issues)
{
	CsvDocument doc;
	doc = CsvParser::Parse(filename);

	ReportedIssue newIssue;
	int counter = 0;
	for (size_t r = 0; r < doc.rows.size(); r++)
	{
		for (size_t c = 0; c < doc.rows[r].size(); c++)
		{
			if (doc.header[c] == "Current Status") { newIssue.currentStatus = doc.rows[r][c]; }
			else if (doc.header[c] == "Open Date-Time") { newIssue.openDateTime = doc.rows[r][c]; }
			else if (doc.header[c] == "Resolved Date") { newIssue.resolvedDate = doc.rows[r][c]; }
			else if (doc.header[c] == "Report Source") { newIssue.reportSource = doc.rows[r][c]; }
			else if (doc.header[c] == "Issue Type") { newIssue.issueType = doc.rows[r][c]; }
			else if (doc.header[c] == "Issue Sub-Type") { newIssue.issueSubType = doc.rows[r][c]; }
			else if (doc.header[c] == "Department Work Group") { newIssue.departmentWorkGroup = doc.rows[r][c]; }
			else if (doc.header[c] == "Days Open") { newIssue.daysOpen = stoi( doc.rows[r][c] ); }
		}
		issues.push_back(newIssue);
	}
}

void Report1(const vector<ReportedIssue>& issues)
{
	vector<size_t> newIssueIndices;
	for (size_t i = 0; i < issues.size(); i++)
	{
		if (issues[i].currentStatus == "new")
		{
			newIssueIndices.push_back(i);
		}
	}

	cout << newIssueIndices.size() << " new issues found:" << endl;
	for (auto& index : newIssueIndices)
	{
		cout << "* " << issues[index].openDateTime << ", " 
			//<< issues[index].daysOpen << ", " 
			<< issues[index].issueType << ", "
			<< issues[index].issueSubType << ", "
			<< issues[index].reportSource
			<< endl;
	}
}

void Report2(const vector<ReportedIssue>& issues)
{

}


int main()
{
	vector<ReportedIssue> issues;
	LoadRecords("data2.csv", issues);

	Report1(issues);
	Report2(issues);

	return 0;
}