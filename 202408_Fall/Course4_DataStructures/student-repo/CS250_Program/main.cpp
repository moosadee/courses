#include <string>

#include "Program_Base/Program.h"

enum class ProgramAction { TEST, PROGRAM };
void Tester();
int main( int argCount, char* args[] )
{
    std::string program_name = "DYNAMIC ARRAY";
    ProgramAction action = ProgramAction::PROGRAM;

    // RUN THE PROGRAM OR RUN THE TESTS?
    if ( argCount > 1 && std::string( args[1] ) == "test" )
    {
        action = ProgramAction::TEST;
    }
    else if ( argCount > 1 && std::string( args[1] ) == "run" )
    {
        action = ProgramAction::PROGRAM;
    }
    else
    {
        action = ProgramAction::PROGRAM;
    }

    // Execute action
    if ( action == ProgramAction::TEST )
    {
        Program program;
        program.RunAllTests();
    }
    else
    {
        Program program;
        program.Run();
    }

    return 0;
}

