#ifndef _PRIORITY_QUEUE
#define _PRIORITY_QUEUE

#include "../../Exceptions/NotImplementedException.h"
#include "../../Exceptions/StructureEmptyException.h"

#include <iostream>
#include <vector>
using namespace std;

template <typename T>
struct PQNode
{
    T data;
    int priority;
};

// ---------------------------------------------------------------------------- ARRAY BASED PRIORITY QUEUE

//! Priority Queue implemented on an array
template <typename T>
class BasicPriorityQueue
{
    public:
    void Push( int priority, T data );
    void Pop();
    T& Peek();
    void Display() const;

    private:
    vector< PQNode<T> > m_array;

    void ShiftRight( size_t index );
    void ShiftLeft( size_t index );

    friend class PriorityQueueTester;
};

/**
Copy the ShiftRight functionality from the FixedArray or DynamicArray.
Note: I didn't write the tests to check for exceptions here.
*/
template <typename T>
void BasicPriorityQueue<T>::ShiftRight( size_t index )
{
    throw Exception::NotImplementedException( "PriorityQueue::ShiftRight" ); // Erase this once you work on this function
}

/**
Copy the ShiftLeft functionality from the FixedArray or DynamicArray.
Note: I didn't write the tests to check for exceptions here.
*/
template <typename T>
void BasicPriorityQueue<T>::ShiftLeft( size_t index )
{
    throw Exception::NotImplementedException( "PriorityQueue::ShiftLeft" ); // Erase this once you work on this function
}

/**
@param      priority        The priority level of this item
@param      data            The data to be stored

1. Create a PQNode<T> object, set its `data` and `priority` member variables using the parameters passed in.
2. Create an integer named `insertIndex`, initialize it to the size of the array.
3. Create a for loop to iterate over all the indices of the array. Within the for loop:
    3a. If the priority of the item at `m_array[i]` is less than the `newNode.priority`, then set `insertIndex` to this index and break.
4. Resize the array, adding 1 additional space: `m_array.resize( m_array.size() + 1 );`
5. In the array at the `insertIndex`, copy the `newNode` over.
*/
template <typename T>
void BasicPriorityQueue<T>::Push( int priority, T data )
{
    throw Exception::NotImplementedException( "PriorityQueue::Push" ); // Erase this once you work on this function
}

/**
ERROR CHECK: If the array size is 0, then throw a `Exception::StructureEmptyException`.

1. Call ShiftLeft with index 0, this overwrites the top-priority item.
2. Call the resize function on the array, shrinking it by 1.
*/
template <typename T>
void BasicPriorityQueue<T>::Pop()
{
    throw Exception::NotImplementedException( "PriorityQueue::Pop" ); // Erase this once you work on this function
}

/**
ERROR CHECK: If the array size is 0, then throw a `Exception::StructureEmptyException`.

1. Return the data of the item at position [0] in the array.
*/
template <typename T>
T& BasicPriorityQueue<T>::Peek()
{
    throw Exception::NotImplementedException( "PriorityQueue::Peek" ); // Erase this once you work on this function
}

template <typename T>
void BasicPriorityQueue<T>::Display() const
{
    cout << string( 40, '-' ) << endl;
    cout << "Array size: " << m_array.size() << endl;
    for ( size_t i = 0; i < m_array.size(); i++ )
    {
        cout << i << ". " << m_array[i].data << " (P: " << m_array[i].priority << ")" << endl;
    }
}


// ---------------------------------------------------------------------------- HEAP BASED PRIORITY QUEUE

template <typename T>
class HeapPriorityQueue
{
    public:
    void Push( int priority, T data );
    void Pop();
    T& Peek();
    void Display() const;

    private:
    void Heapify( size_t index );

    //! The underlying data structure
    vector< PQNode<T> > m_array;

    friend class PriorityQueueTester;
};


/**
1. Create an integer variable `left`, which should be 2 * index + 1.
2. Create an integer variable `right`, which should be 2 * index + 2.
3. Create an integer variable `largest`, initialize it to the `index`.
4. If `left` is a valid index and the priority of `m_array[left]` is greater than `m_array[largest]`'s priority,
    4a. then set `largest` to the `left` index.
5. If `right` is a valid index and the priority of `m_array[right]` is greater than `m_array[largest]`'s priority,
    5a. then set `largest` to the `right` index.
6. If `largest` is not `index`, then:
    6a. Swap `m_array[largest]` and `m_array[index]`.
    6b. Call `Heapify` using `largest` as the argument.
*/
template <typename T>
void HeapPriorityQueue<T>::Heapify( size_t index )
{
    throw Exception::NotImplementedException( "Heap::Heapify" ); // Erase this once you work on this function
}

/**
@param      priority        The priority level of this item
@param      data            The data to be stored

1. Create a PQNode<T> variable. Set its data and priority.
2. Push the newNode into the `m_array` vector.
3. Create a variable `i`, set it to `m_array.size() - 1`.
* NOTE: The parent node location is (i-1)/2.
4. While `i` is not zero AND the priority of the parent node is less than the priority of node `i`, do:
    5a. swap the array item at `i` and at the parent location.
    5b. Set `i` to the parent location index.
*/
template <typename T>
void HeapPriorityQueue<T>::Push( int priority, T data )
{
    throw Exception::NotImplementedException( "Heap::Push" ); // Erase this once you work on this function
}

/**
ERROR CHECK: If the array size is 0, then throw a `Exception::StructureEmptyException`.

1. If the array size is 1, then call `pop_back` on the array and then return.
2. Otherwise, set `m_array[0]` to the back-most item of the array.
3. Call `pop_back` on the array.
4. Call `Heapify` on index 0.
*/
template <typename T>
void HeapPriorityQueue<T>::Pop()
{
    throw Exception::NotImplementedException( "Heap::Pop" ); // Erase this once you work on this function
}

/**
ERROR CHECK: If the array size is 0, then throw a `Exception::StructureEmptyException`.

1. Return the data of the array item at index 0.
*/
template <typename T>
T& HeapPriorityQueue<T>::Peek()
{
    throw Exception::NotImplementedException( "Heap::Peek" ); // Erase this once you work on this function
}

template <typename T>
void HeapPriorityQueue<T>::Display() const
{
    cout << string( 40, '-' ) << endl;
    cout << "Array size: " << m_array.size() << endl;
    for ( size_t i = 0; i < m_array.size(); i++ )
    {
        cout << i << ". " << m_array[i].data << " (P: " << m_array[i].priority << ")" << endl;
    }
}

#endif
