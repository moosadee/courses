#include "PriorityQueueTester.h"
#include "../../Utilities/Style.h"
#include "../../Exceptions/NotImplementedException.h"
#include "PriorityQueue.h"

void PriorityQueueTester::RunAll()
{
    DisplayHeader( "PriorityQueueTester - RunAll", 0 );
    Test_BasicPriorityQueue_Push();
    Test_BasicPriorityQueue_Pop();
    Test_BasicPriorityQueue_Peek();

    Test_HeapPriorityQueue_Heapify();
    Test_HeapPriorityQueue_Push();
    Test_HeapPriorityQueue_Pop();
    Test_HeapPriorityQueue_Peek();
}

// ---------------------------------------------------------------------------- ARRAY BASED PRIORITY QUEUE
void PriorityQueueTester::Test_BasicPriorityQueue_Push()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "BasicPriorityQueue::Push";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        BasicPriorityQueue<string> pq;
        try                                                    { pq.Push( 1, "A" ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Add 1 item" );
        BasicPriorityQueue<string> pq;
        pq.Push( 1, "breakfast" );
        totalTests++; testsPassed += Test_DoTheyMatch( size_t( 1 ), pq.m_array.size(), testName, "pq.m_array.size()" );
        if ( pq.m_array.size() == 0 ) { cout << YELLOW << "pq.m_array.size() IS 0; LEAVING TESTS EARLY." << CLEAR << endl; return; }
        totalTests++; testsPassed += Test_DoTheyMatch( string( "breakfast" ), pq.m_array[ 0 ].data, testName, "pq.m_array[ 0 ]" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Add 3 items" );
        BasicPriorityQueue<string> pq;
        pq.Push( 3, "breakfast" );
        pq.Push( 2, "lunch" );
        pq.Push( 1, "dinner" );
        totalTests++; testsPassed += Test_DoTheyMatch( size_t( 3 ), pq.m_array.size(), testName, "pq.m_array.size()" );
        if ( pq.m_array.size() == 0 ) { cout << YELLOW << "pq.m_array.size() IS 0; LEAVING TESTS EARLY." << CLEAR << endl; return; }
        totalTests++; testsPassed += Test_DoTheyMatch( string( "breakfast" ), pq.m_array[ 0 ].data, testName, "pq.m_array.GetAt[ 0 ]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "lunch" ),     pq.m_array[ 1 ].data, testName, "pq.m_array.GetAt[ 1 ]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "dinner" ),    pq.m_array[ 2 ].data, testName, "pq.m_array.GetAt[ 2 ]" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Add item with bigger priority each time" );
        BasicPriorityQueue<string> pq;
        pq.Push( 10, "breakfast" );
        pq.Push( 30, "lunch" );
        pq.Push( 20, "dinner" );
        pq.Push( 50, "brunch" );
        pq.Push( 40, "snack" );
        totalTests++; testsPassed += Test_DoTheyMatch( size_t( 5 ), pq.m_array.size(), testName, "pq.m_array.size()" );
        if ( pq.m_array.size() == 0 ) { cout << YELLOW << "pq.m_array.size() IS 0; LEAVING TESTS EARLY." << CLEAR << endl; return; }
        totalTests++; testsPassed += Test_DoTheyMatch( string( "brunch" ),     pq.m_array[ 0 ].data, testName, "pq.m_array[ 0 ]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "snack" ),      pq.m_array[ 1 ].data, testName, "pq.m_array[ 1 ]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "lunch" ),      pq.m_array[ 2 ].data, testName, "pq.m_array[ 2 ]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "dinner" ),     pq.m_array[ 3 ].data, testName, "pq.m_array[ 3 ]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "breakfast" ),  pq.m_array[ 4 ].data, testName, "pq.m_array[ 4 ]" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void PriorityQueueTester::Test_BasicPriorityQueue_Pop()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "BasicPriorityQueue::Pop";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        BasicPriorityQueue<string> pq;
        try                                                    { pq.Pop(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Remove top item, check bubble up" );
        BasicPriorityQueue<string> pq;
        pq.m_array.resize( 5 );
        pq.m_array[0].data = "snack";      pq.m_array[0].priority = 50;
        pq.m_array[1].data = "brunch";     pq.m_array[1].priority = 40;
        pq.m_array[2].data = "lunch";      pq.m_array[2].priority = 30;
        pq.m_array[3].data = "breakfast";  pq.m_array[3].priority = 20;
        pq.m_array[4].data = "dinner";     pq.m_array[4].priority = 10;

        pq.Pop();

        totalTests++; testsPassed += Test_DoTheyMatch( string( "brunch" ),     pq.m_array[ 0 ].data, testName, "pq.m_array[ 0 ]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "lunch" ),      pq.m_array[ 1 ].data, testName, "pq.m_array[ 1 ]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "breakfast" ),  pq.m_array[ 2 ].data, testName, "pq.m_array[ 2 ]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "dinner" ),     pq.m_array[ 3 ].data, testName, "pq.m_array[ 3 ]" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void PriorityQueueTester::Test_BasicPriorityQueue_Peek()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "BasicPriorityQueue::Peek";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        BasicPriorityQueue<string> pq;
        try                                                    { pq.Peek(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Peek at top item" );
        BasicPriorityQueue<string> pq;
        pq.m_array.resize( 3 );
        pq.m_array[0].data = "breakfast"; pq.m_array[0].priority = 3;
        pq.m_array[1].data = "lunch";     pq.m_array[1].priority = 2;
        pq.m_array[2].data = "dinner";    pq.m_array[2].priority = 1;

        string expOut = "breakfast";
        string actOut = pq.Peek();

        totalTests++; testsPassed += Test_DoTheyMatch( expOut, actOut, testName, "pq.Peek()" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Peek at top item" );
        BasicPriorityQueue<string> pq;
        pq.m_array.resize( 5 );
        pq.m_array[0].data = "snack";      pq.m_array[0].priority = 50;
        pq.m_array[1].data = "brunch";     pq.m_array[1].priority = 40;
        pq.m_array[2].data = "lunch";      pq.m_array[2].priority = 20;
        pq.m_array[3].data = "breakfast";  pq.m_array[3].priority = 10;

        string expOut = "snack";
        string actOut = pq.Peek();

        totalTests++; testsPassed += Test_DoTheyMatch( expOut, actOut, testName, "pq.Peek()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}


// ---------------------------------------------------------------------------- HEAP BASED PRIORITY QUEUE
void PriorityQueueTester::Test_HeapPriorityQueue_Heapify()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "HeapPriorityQueue::Heapify";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        HeapPriorityQueue<string> heapy;
        try                                                    { heapy.Heapify( 0 ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        /*
        Tree form:
                            A(1)                INDEX       0   1
                          /                     PRIORITY    1   3
                         B(3)                   VALUE       A   B

        HeapPriorityQueueified:
                            B(3)                INDEX       0   1
                          /                     PRIORITY    3   1
                         A(1)                   VALUE       B   A
        */
        string testName = Test_Begin( "Array: A(1), B(3) --> HeapPriorityQueue: B(3), A(1)" );
        HeapPriorityQueue<string> heapy;
        heapy.m_array.resize( 2 );
        heapy.m_array[0].data = "A";  heapy.m_array[0].priority = 1;
        heapy.m_array[1].data = "B";  heapy.m_array[1].priority = 3;
        heapy.Heapify( 0 );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "B" ),  heapy.m_array[ 0 ].data, testName, "heapy.m_array[ 0 ]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "A" ),  heapy.m_array[ 1 ].data, testName, "heapy.m_array[ 1 ]" );
        Test_End();
    }

    {
        /*
        Tree form:
                            A(1)                INDEX       0   1   2
                          /     \               PRIORITY    1   3   5
                         B(3)    C(5)           VALUE       A   B   C

        HeapPriorityQueueified:
                            C(5)                INDEX       0   1   2
                          /     \               PRIORITY    5   3   1
                         B(3)    A(1)           VALUE       C   B   A
        */
        string testName = Test_Begin( "Array: 1 3 5 --> HeapPriorityQueue: 5 3 1" );
        HeapPriorityQueue<string> heapy;
        heapy.m_array.resize( 3 );
        heapy.m_array[0].data = "A";  heapy.m_array[0].priority = 1;
        heapy.m_array[1].data = "B";  heapy.m_array[1].priority = 3;
        heapy.m_array[2].data = "C";  heapy.m_array[2].priority = 5;
        heapy.Heapify( 0 );

        for ( size_t i = 0; i < heapy.m_array.size() - 2; i++ )
        {
            totalTests++;
            size_t left = 2 * i + 1;
            size_t right = 2 * i + 2;

            // Node must be larger than its children
            if ( left < heapy.m_array.size() && heapy.m_array[i].priority < heapy.m_array[left].priority )
            {
                Test_FailDisplayExpectedActual( testName, "Node >= left child",
                    string( to_string( heapy.m_array[i].priority ) + ">=" + to_string( heapy.m_array[left].priority ) ),
                    string( to_string( heapy.m_array[i].priority ) + "<" + to_string( heapy.m_array[left].priority ) )
                    );
            }
            else if ( right < heapy.m_array.size() &&heapy.m_array[i].priority < heapy.m_array[right].priority )
            {
                Test_FailDisplayExpectedActual( testName, "Node >= right child",
                    string( to_string( heapy.m_array[i].priority ) + ">=" + to_string( heapy.m_array[right].priority ) ),
                    string( to_string( heapy.m_array[i].priority ) + "<" + to_string( heapy.m_array[right].priority ) )
                    );
            }
            else
            {
                testsPassed++; Test_PassDisplay( testName, "Node >= left and right child" );
            }
        }

        Test_End();
    }

    {
        /*
        HeapPriorityQueue:
                            Z(9)                INDEX       0   1   2   3   4
                          /     \               PRIORITY    9   8   7   6   5
                         Y(8)    X(7)           VALUE       Z   Y   X   W   V
                       /     \
                    W(6)     V(5)

        HeapPriorityQueue after pop:
                            V(5)                INDEX       0   1   2   3
                          /     \               PRIORITY    5   8   7   6
                         Y(8)    X(7)           VALUE       V   Y   X   W
                       /
                    W(6)
        */
        string testName = Test_Begin( "5 8 7 6 --> 8 7 6 5" );
        HeapPriorityQueue<string> heapy;
        heapy.m_array.resize( 4 );
        heapy.m_array[0].data = "V";  heapy.m_array[0].priority = 5;
        heapy.m_array[1].data = "Y";  heapy.m_array[1].priority = 8;
        heapy.m_array[2].data = "X";  heapy.m_array[2].priority = 7;
        heapy.m_array[3].data = "W";  heapy.m_array[3].priority = 6;

        heapy.Heapify( 0 );

        for ( size_t i = 0; i < heapy.m_array.size() - 2; i++ )
        {
            totalTests++;
            size_t left = 2 * i + 1;
            size_t right = 2 * i + 2;

            // Node must be larger than its children
            if ( left < heapy.m_array.size() && heapy.m_array[i].priority < heapy.m_array[left].priority )
            {
                Test_FailDisplayExpectedActual( testName, "Node >= left child",
                    string( to_string( heapy.m_array[i].priority ) + ">=" + to_string( heapy.m_array[left].priority ) ),
                    string( to_string( heapy.m_array[i].priority ) + "<" + to_string( heapy.m_array[left].priority ) )
                    );
            }
            else if ( right < heapy.m_array.size() &&heapy.m_array[i].priority < heapy.m_array[right].priority )
            {
                Test_FailDisplayExpectedActual( testName, "Node >= right child",
                    string( to_string( heapy.m_array[i].priority ) + ">=" + to_string( heapy.m_array[right].priority ) ),
                    string( to_string( heapy.m_array[i].priority ) + "<" + to_string( heapy.m_array[right].priority ) )
                    );
            }
            else
            {
                testsPassed++; Test_PassDisplay( testName, "Node >= left and right child" );
            }
        }

        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void PriorityQueueTester::Test_HeapPriorityQueue_Push()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "HeapPriorityQueue::Push";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        HeapPriorityQueue<string> heapy;
        try                                                    { heapy.Push( 1, "A" ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Add 1 item" );
        HeapPriorityQueue<string> heapy;
        heapy.Push( 1, "breakfast" );
        totalTests++; testsPassed += Test_DoTheyMatch( size_t( 1 ), heapy.m_array.size(), testName, "heapy.m_array.size()" );
        if ( heapy.m_array.size() == 0 ) { cout << YELLOW << "heapy.m_array.size() IS 0; LEAVING TESTS EARLY." << CLEAR << endl; return; }
        totalTests++; testsPassed += Test_DoTheyMatch( string( "breakfast" ), heapy.m_array[ 0 ].data, testName, "heapy.m_array[ 0 ]" );
        Test_End();
    }

    {
        /*
                    3, breakfast
                    /          \
                2, lunch     1, dinner
        */
        string testName = Test_Begin( "Add 3 items" );
        HeapPriorityQueue<string> heapy;
        heapy.Push( 3, "breakfast" );      // Expected index 0
        heapy.Push( 2, "lunch" );          // i's left child is at [2 * i + 1]
        heapy.Push( 1, "dinner" );         // i's right child is at [2 * i + 2]
        totalTests++; testsPassed += Test_DoTheyMatch( size_t( 3 ), heapy.m_array.size(), testName, "heapy.m_array.size()" );
        if ( heapy.m_array.size() == 0 ) { cout << YELLOW << "heapy.m_array.size() IS 0; LEAVING TESTS EARLY." << CLEAR << endl; return; }
        totalTests++; testsPassed += Test_DoTheyMatch( string( "breakfast" ), heapy.m_array[ 0 ].data, testName, "heapy.m_array.GetAt[ 0 ]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "lunch" ),     heapy.m_array[ 1 ].data, testName, "heapy.m_array.GetAt[ 1 ]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "dinner" ),    heapy.m_array[ 2 ].data, testName, "heapy.m_array.GetAt[ 2 ]" );
        Test_End();
    }

    {
        /*
                           50, snack                          index 0
                          /         \                        /      \
                   40, brunch       20, lunch          index 1      index 2
                  /          \                          /    \
            10, breakfast    30, dinner           index 3   index 4
        */
        string testName = Test_Begin( "Add item with bigger priority each time" );
        HeapPriorityQueue<string> heapy;
        heapy.Push( 10, "breakfast" );
        heapy.Push( 20, "lunch" );
        heapy.Push( 30, "dinner" );
        heapy.Push( 40, "brunch" );
        heapy.Push( 50, "snack" );
        totalTests++; testsPassed += Test_DoTheyMatch( size_t( 5 ), heapy.m_array.size(), testName, "heapy.m_array.size()" );
        if ( heapy.m_array.size() == 0 ) { cout << YELLOW << "heapy.m_array.size() IS 0; LEAVING TESTS EARLY." << CLEAR << endl; return; }
        totalTests++; testsPassed += Test_DoTheyMatch( string( "breakfast" ),  heapy.m_array[ 3 ].data, testName, "heapy.m_array[ 3 ]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "lunch" ),      heapy.m_array[ 2 ].data, testName, "heapy.m_array[ 2 ]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "dinner" ),     heapy.m_array[ 4 ].data, testName, "heapy.m_array[ 4 ]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "brunch" ),     heapy.m_array[ 1 ].data, testName, "heapy.m_array[ 1 ]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "snack" ),      heapy.m_array[ 0 ].data, testName, "heapy.m_array[ 0 ]" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void PriorityQueueTester::Test_HeapPriorityQueue_Pop()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "HeapPriorityQueue::Pop";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        HeapPriorityQueue<string> heapy;
        try                                                    { heapy.Pop(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        /*
                           50, snack                          index 0
                          /         \                        /      \
                   40, brunch       20, lunch          index 1      index 2
                  /          \                          /    \
            10, breakfast    30, dinner           index 3   index 4
        */
        string testName = Test_Begin( "Remove top item, check bubble up" );
        HeapPriorityQueue<string> heapy;
        heapy.m_array.resize( 5 );
        heapy.m_array[0].data = "snack";      heapy.m_array[0].priority = 50;
        heapy.m_array[1].data = "brunch";     heapy.m_array[1].priority = 40;
        heapy.m_array[2].data = "lunch";      heapy.m_array[2].priority = 20;
        heapy.m_array[3].data = "breakfast";  heapy.m_array[3].priority = 10;
        heapy.m_array[4].data = "dinner";     heapy.m_array[4].priority = 30;

        heapy.Pop();

        /*
                          40, brunch                          index 0
                          /         \                        /      \
                   30, dinner       20, lunch          index 1      index 2
                  /                                    /
            10, breakfast                         index 3
        */

        totalTests++; testsPassed += Test_DoTheyMatch( string( "breakfast" ),  heapy.m_array[ 3 ].data, testName, "heapy.m_array[ 3 ]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "lunch" ),      heapy.m_array[ 2 ].data, testName, "heapy.m_array[ 2 ]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "dinner" ),     heapy.m_array[ 1 ].data, testName, "heapy.m_array[ 1 ]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "brunch" ),     heapy.m_array[ 0 ].data, testName, "heapy.m_array[ 0 ]" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void PriorityQueueTester::Test_HeapPriorityQueue_Peek()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "HeapPriorityQueue::Peek";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        HeapPriorityQueue<string> heapy;
        try                                                    { heapy.Peek(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        /*
                    3, breakfast
                    /          \
                2, lunch     1, dinner
        */
        string testName = Test_Begin( "Peek at top item" );
        HeapPriorityQueue<string> heapy;
        heapy.m_array.resize( 3 );
        heapy.m_array[0].data = "breakfast"; heapy.m_array[0].priority = 3;
        heapy.m_array[1].data = "lunch";     heapy.m_array[1].priority = 2;
        heapy.m_array[2].data = "dinner";    heapy.m_array[2].priority = 1;

        string expOut = "breakfast";
        string actOut = heapy.Peek();

        totalTests++; testsPassed += Test_DoTheyMatch( expOut, actOut, testName, "heapy.Peek()" );
        Test_End();
    }

    {
        /*
                           50, snack                          index 0
                          /         \                        /      \
                   40, brunch       20, lunch          index 1      index 2
                  /          \                          /    \
            10, breakfast    30, dinner           index 3   index 4
        */
        string testName = Test_Begin( "Peek at top item" );
        HeapPriorityQueue<string> heapy;
        heapy.m_array.resize( 5 );
        heapy.m_array[0].data = "snack";      heapy.m_array[0].priority = 50;
        heapy.m_array[1].data = "brunch";     heapy.m_array[1].priority = 40;
        heapy.m_array[2].data = "lunch";      heapy.m_array[2].priority = 20;
        heapy.m_array[3].data = "breakfast";  heapy.m_array[3].priority = 10;

        string expOut = "snack";
        string actOut = heapy.Peek();

        totalTests++; testsPassed += Test_DoTheyMatch( expOut, actOut, testName, "heapy.Peek()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}



