#ifndef _PRIORITY_QUEUE_TESTER
#define _PRIORITY_QUEUE_TESTER

class PriorityQueueTester
{
public:
    void RunAll();
    void Test_BasicPriorityQueue_Push();
    void Test_BasicPriorityQueue_Pop();
    void Test_BasicPriorityQueue_Peek();

    void Test_HeapPriorityQueue_Heapify();
    void Test_HeapPriorityQueue_Push();
    void Test_HeapPriorityQueue_Pop();
    void Test_HeapPriorityQueue_Peek();
};

#endif
