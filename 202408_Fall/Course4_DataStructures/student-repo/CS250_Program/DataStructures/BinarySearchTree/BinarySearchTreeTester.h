#ifndef _BINARY_SEARCH_TREE_TESTER
#define _BINARY_SEARCH_TREE_TESTER

class BinarySearchTreeTester
{
public:
    void RunAll();
    void Test_BinarySearchTree_NodeConstructor();
    void Test_BinarySearchTree_Constructor();
    void Test_BinarySearchTree_Push();
    void Test_BinarySearchTree_Contains();
    void Test_BinarySearchTree_FindNode();
    void Test_BinarySearchTree_GetInOrder();
    void Test_BinarySearchTree_GetPreOrder();
    void Test_BinarySearchTree_GetPostOrder();
    void Test_BinarySearchTree_GetMinKey();
    void Test_BinarySearchTree_GetMaxKey();
    void Test_BinarySearchTree_GetCount();
    void Test_BinarySearchTree_GetHeight();
    void Test_BinarySearchTree_PopNode();
    void Test_BinarySearchTree_PopRoot();
    void Test_BinarySearchTree_GetSuccessor();
    void Test_BinarySearchTree_ShiftNodes();
};

#endif
