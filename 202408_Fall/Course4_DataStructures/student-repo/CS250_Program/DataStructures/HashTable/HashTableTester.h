#ifndef _HASH_TABLE_TESTER
#define _HASH_TABLE_TESTER

#include "HashTable.h"

class HashTableTester
{
public:
    void RunAll();
    void Test_HashTable_Constructor();
    void Test_HashTable_SetCollisionMethod();
    void Test_HashTable_Size();
    void Test_HashTable_Hash1();
    void Test_HashTable_Hash2();
    void Test_HashTable_LinearProbe();
    void Test_HashTable_QuadraticProbe();
    void Test_HashTable_Push();
    void Test_HashTable_Get();
};

#endif
