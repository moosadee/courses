#include "HashTableTester.h"
#include "HashTable.h"
#include "../../Utilities/Style.h"

void HashTableTester::RunAll()
{
    DisplayHeader( "HashTableTester - RunAll", 0 );
    Test_HashTable_Constructor();
    Test_HashTable_SetCollisionMethod();
    Test_HashTable_Size();
    Test_HashTable_Hash1();
    Test_HashTable_Hash2();
    Test_HashTable_LinearProbe();
    Test_HashTable_QuadraticProbe();
    Test_HashTable_Push();
    Test_HashTable_Get();
}

void HashTableTester::Test_HashTable_Constructor()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;

    {
        string testName = Test_Begin( "HashTable<int> tbl;" );
        HashTable<int> tbl;
        totalTests++; testsPassed += Test_DoTheyMatch( 5, tbl.m_table.ArraySize(), testName, "tbl.m_table.ArraySize()" );
        Test_End();
    }

    {
        string testName = Test_Begin( "HashTable<int> tbl(11);" );
        HashTable<int> tbl( 11 );
        totalTests++; testsPassed += Test_DoTheyMatch( 11, tbl.m_table.ArraySize(), testName, "tbl.m_table.ArraySize()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void HashTableTester::Test_HashTable_SetCollisionMethod()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "HashTable::SetCollisionMethod";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        HashTable<string> tbl;
        try                                                    { tbl.SetCollisionMethod( CollisionMethod::LINEAR ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "SetCollisionMethod( LINEAR )" );
        HashTable<int> tbl;
        tbl.SetCollisionMethod( CollisionMethod::LINEAR );
        totalTests++; testsPassed += Test_DoTheyMatch( static_cast<int>( CollisionMethod::LINEAR ), static_cast<int>( tbl.m_method ), testName, "tbl.m_method" );
        Test_End();
    }

    {
        string testName = Test_Begin( "SetCollisionMethod( QUADRATIC )" );
        HashTable<int> tbl;
        tbl.SetCollisionMethod( CollisionMethod::QUADRATIC );
        totalTests++; testsPassed += Test_DoTheyMatch( static_cast<int>( CollisionMethod::QUADRATIC ), static_cast<int>( tbl.m_method ), testName, "tbl.m_method" );
        Test_End();
    }

    {
        string testName = Test_Begin( "SetCollisionMethod( DOUBLE_HASH )" );
        HashTable<int> tbl;
        tbl.SetCollisionMethod( CollisionMethod::DOUBLE_HASH );
        totalTests++; testsPassed += Test_DoTheyMatch( static_cast<int>( CollisionMethod::DOUBLE_HASH ), static_cast<int>( tbl.m_method ), testName, "tbl.m_method" );
        Test_End();
    }

    cout << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void HashTableTester::Test_HashTable_Size()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "HashTable::Size";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        HashTable<string> tbl;
        try                                                    { tbl.Size(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Set m_table size, check Size()" );
        HashTable<int> tbl;
        tbl.m_table.m_itemCount = 5;
        tbl.m_table.m_arraySize = 11; // Size() should reflect m_itemCount, NOT m_arraySize
        totalTests++; testsPassed += Test_DoTheyMatch( 5, tbl.Size(), testName, "tbl.Size()" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Set m_table size, check Size()" );
        HashTable<int> tbl;
        tbl.m_table.m_itemCount = 7;
        tbl.m_table.m_arraySize = 13; // Size() should reflect m_itemCount, NOT m_arraySize
        totalTests++; testsPassed += Test_DoTheyMatch( 7, tbl.Size(), testName, "tbl.Size()" );
        Test_End();
    }

    cout << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void HashTableTester::Test_HashTable_Hash1()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "HashTable::Hash1";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        HashTable<string> tbl;
        try                                                    { tbl.Hash1( 10 ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Check result of Hash1 - should be x % arrsize" );
        HashTable<int> tbl;
        totalTests++; testsPassed += Test_DoTheyMatch( 3, tbl.Hash1( 3 ), testName, "tbl.Hash1( 3 )" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Check result of Hash1 - should be x % arrsize" );
        HashTable<int> tbl;
        totalTests++; testsPassed += Test_DoTheyMatch( 2, tbl.Hash1( 7 ), testName, "tbl.Hash1( 7 )" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Check result of Hash1 - should be x % arrsize" );
        HashTable<int> tbl( 11 );
        totalTests++; testsPassed += Test_DoTheyMatch( 1, tbl.Hash1( 23 ), testName, "tbl.Hash1( 23 )" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Check result of Hash1 - should be x % arrsize" );
        HashTable<int> tbl( 11 );
        totalTests++; testsPassed += Test_DoTheyMatch( 2, tbl.Hash1( 35 ), testName, "tbl.Hash1( 35 )" );
        Test_End();
    }

    cout << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void HashTableTester::Test_HashTable_Hash2()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "HashTable::Hash2";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        HashTable<string> tbl;
        try                                                    { tbl.Hash2( 1, 2 ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Check result of Hash2 - expecting Hash1(x) + ( collisions * (3 - x  % 3) )" );
        HashTable<int> tbl;
        totalTests++; testsPassed += Test_DoTheyMatch( 0, tbl.Hash2( 0, 0 ), testName, "tbl.Hash2( 0, 0 )" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Check result of Hash2 - expecting Hash1(x) + ( collisions * (3 - x  % 3) )" );
        HashTable<int> tbl;
        totalTests++; testsPassed += Test_DoTheyMatch( 6, tbl.Hash2( 0, 2 ), testName, "tbl.Hash2( 0, 2 )" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Check result of Hash2 - expecting Hash1(x) + ( collisions * (3 - x  % 3) )" );
        HashTable<int> tbl;
        totalTests++; testsPassed += Test_DoTheyMatch( 2, tbl.Hash2( 2, 0 ), testName, "tbl.Hash2( 2, 0 )" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Check result of Hash2 - expecting Hash1(x) + ( collisions * (3 - x  % 3) )" );
        HashTable<int> tbl;
        totalTests++; testsPassed += Test_DoTheyMatch( 12, tbl.Hash2( 3, 3 ), testName, "tbl.Hash2( 3, 3 )" );
        Test_End();
    }

    cout << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void HashTableTester::Test_HashTable_LinearProbe()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "HashTable::LinearProbe";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        HashTable<string> tbl;
        try                                                    { tbl.LinearProbe( 1, 10 ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Check LinearProbe result" );
        HashTable<int> tbl;
        totalTests++; testsPassed += Test_DoTheyMatch( 1, tbl.LinearProbe( 0, 1 ), testName, "LinearProbe( 0, 1 )" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Check LinearProbe result" );
        HashTable<int> tbl;
        totalTests++; testsPassed += Test_DoTheyMatch( 2, tbl.LinearProbe( 0, 2 ), testName, "LinearProbe( 0, 2 )" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Check LinearProbe result" );
        HashTable<int> tbl;
        totalTests++; testsPassed += Test_DoTheyMatch( 5, tbl.LinearProbe( 3, 2 ), testName, "LinearProbe( 3, 2 )" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void HashTableTester::Test_HashTable_QuadraticProbe()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "HashTable::QuadraticProbe";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        HashTable<string> tbl;
        try                                                    { tbl.QuadraticProbe( 1, 10 ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Check QuadraticProbe result" );
        HashTable<int> tbl;
        totalTests++; testsPassed += Test_DoTheyMatch( 1, tbl.QuadraticProbe( 0, 1 ), testName, "QuadraticProbe( 0, 1 )" );
        Test_End();
    } cout << CLEAR;

    {
        string testName = Test_Begin( "Check QuadraticProbe result" );
        HashTable<int> tbl;
        totalTests++; testsPassed += Test_DoTheyMatch( 4, tbl.QuadraticProbe( 0, 2 ), testName, "QuadraticProbe( 0, 2 )" );
        Test_End();
    } cout << CLEAR;

    {
        string testName = Test_Begin( "Check QuadraticProbe result" );
        HashTable<int> tbl;
        totalTests++; testsPassed += Test_DoTheyMatch( 9, tbl.QuadraticProbe( 0, 3 ), testName, "QuadraticProbe( 0, 3 )" );
        Test_End();
    } cout << CLEAR;

    {
        string testName = Test_Begin( "Check QuadraticProbe result" );
        HashTable<int> tbl;
        totalTests++; testsPassed += Test_DoTheyMatch( 4, tbl.QuadraticProbe( 3, 1 ), testName, "QuadraticProbe( 3, 1 )" );
        Test_End();
    } cout << CLEAR;

    {
        string testName = Test_Begin( "Check QuadraticProbe result" );
        HashTable<int> tbl;
        totalTests++; testsPassed += Test_DoTheyMatch( 7, tbl.QuadraticProbe( 3, 2 ), testName, "QuadraticProbe( 3, 2 )" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void HashTableTester::Test_HashTable_Push()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "HashTable::Push";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ LinearProbe implemented" ); totalTests++;
        HashTable<string> tbl;
        try                                                    { tbl.LinearProbe( 1, 10 ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Check if PREREQ QuadraticProbe implemented" ); totalTests++;
        HashTable<string> tbl;
        try                                                    { tbl.QuadraticProbe( 1, 10 ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Check if PREREQ Hash2 implemented" ); totalTests++;
        HashTable<string> tbl;
        try                                                    { tbl.Hash2( 1, 2 ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Check if PREREQ SetCollisionMethod implemented" ); totalTests++;
        HashTable<string> tbl;
        try                                                    { tbl.SetCollisionMethod( CollisionMethod::DOUBLE_HASH ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        HashTable<string> tbl;
        try                                                    { tbl.Push( 1, "b" ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Push(0,1); Push(1,2); Push(5,3); Push(6,4); LINEAR" );
        HashTable<int> tbl; // size 5
        tbl.SetCollisionMethod( CollisionMethod::LINEAR );
        tbl.Push( 0, 1 );
        tbl.Push( 1, 2 );
        tbl.Push( 5, 3 ); // Should have collision
        tbl.Push( 6, 4 ); // Should have collision

        int results[4] = { -1, -1, -1, -1 };
        for ( int i = 0; i < 4; i++ )
        {
            try
            {
                results[i] = tbl.m_table.GetAt( i ).data;
            }
            catch( ... )
            {
                cout << YELLOW << "NOTHING FOUND AT INDEX " << i << "! Do you have a logic error in your Push function?" << endl;
            }
        }

        totalTests++; testsPassed += Test_DoTheyMatch( 4, tbl.m_table.Size(), testName, "tbl.m_table.Size()" );
        totalTests++; testsPassed += Test_DoTheyMatch( 1, results[0], testName, "tbl.m_table.GetAt( 0 )" );
        totalTests++; testsPassed += Test_DoTheyMatch( 2, results[1], testName, "tbl.m_table.GetAt( 1 )" );
        totalTests++; testsPassed += Test_DoTheyMatch( 3, results[2], testName, "tbl.m_table.GetAt( 2 )" );
        totalTests++; testsPassed += Test_DoTheyMatch( 4, results[3], testName, "tbl.m_table.GetAt( 3 )" );
        Test_End();
    } cout << CLEAR;

    {
        string testName = Test_Begin( "Check for exception thrown when structure is full" );
        HashTable<int> tbl; // size 5
        tbl.SetCollisionMethod( CollisionMethod::LINEAR );
        tbl.Push( 10, 1 );
        tbl.Push( 11, 2 );
        tbl.Push( 12, 3 );
        tbl.Push( 13, 4 );
        tbl.Push( 14, 5 );

        string exceptionType = "";
        string expectedException = "StructureFullException";

        try
        {
            tbl.Push( 15, 6 );
        }
        catch( const Exception::StructureFullException& ex )
        {
            exceptionType = "StructureFullException";
        }
        catch( ... )
        {
            exceptionType = "Unknown exception";
        }

        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionType, testName, "Exception?" );
        Test_End();
    } cout << CLEAR;

    {
        string testName = Test_Begin( "Push(0,1); Push(1,2); Push(5,3); Push(6,4); QUADRATIC" );
        HashTable<int> tbl; // size 5
        tbl.SetCollisionMethod( CollisionMethod::QUADRATIC );
        tbl.Push( 0, 1 );
        tbl.Push( 1, 2 );
        tbl.Push( 5, 3 ); // Should have collision; 0 -> 1 -> 4
        tbl.Push( 6, 4 ); // Should have collision; 1 -> 2

        totalTests++; testsPassed += Test_DoTheyMatch( 4, tbl.m_table.Size(), testName, "tbl.m_table.Size()" );

        int positions[4] = { 0, 1, 4, 2 };
        int expected[4] = { 1, 2, 3, 4 };
        int results[4] = { -1, -1, -1, -1 };
        for ( int i = 0; i < 4; i++ )
        {
            try
            {
                results[i] = tbl.m_table.GetAt( positions[i] ).data;
                totalTests++; testsPassed += Test_DoTheyMatch( expected[i], results[i], testName, "tbl.m_table.GetAt( " + ToString( positions[i] ) + " )" );
            }
            catch( ... )
            {
                cout << YELLOW << "NOTHING FOUND AT INDEX " << i << "! Do you have a logic error in your Push function?" << endl;
            }
        }

        Test_End();
    } cout << CLEAR;

    {
        string testName = Test_Begin( "Push(0,1); Push(1,2); Push(5,3); Push(6,4); DOUBLE_HASH" );
        HashTable<int> tbl; // size 5
        tbl.SetCollisionMethod( CollisionMethod::DOUBLE_HASH );
        tbl.Push( 0, 1 );
        tbl.Push( 1, 2 );
        tbl.Push( 5, 3 ); // Should have collision; 0 -> 0 + 1 * ( 3 - 5 mod 3 ) = 1; 0 + 2 * ( 3 - 5 mod 3 )
        tbl.Push( 6, 4 ); // Should have collision; 1 -> 1 + 1 * ( 3 - 6 mod 3 ) = 4

        totalTests++; testsPassed += Test_DoTheyMatch( 4, tbl.m_table.Size(), testName, "tbl.m_table.Size()" );

        int positions[4] = { 0, 1, 2, 4 };
        int expected[4] = { 1, 2, 3, 4 };
        int results[4] = { -1, -1, -1, -1 };
        for ( int i = 0; i < 4; i++ )
        {
            try
            {
                results[i] = tbl.m_table.GetAt( positions[i] ).data;
                totalTests++; testsPassed += Test_DoTheyMatch( expected[i], results[i], testName, "tbl.m_table.GetAt( " + ToString( positions[i] ) + " )" );
            }
            catch( ... )
            {
                cout << YELLOW << "NOTHING FOUND AT INDEX " << i << "! Do you have a logic error in your Push function?" << endl;
            }
        }

        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void HashTableTester::Test_HashTable_Get()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "HashTable::Get";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ LinearProbe implemented" ); totalTests++;
        HashTable<string> tbl;
        try                                                    { tbl.LinearProbe( 1, 10 ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Check if PREREQ QuadraticProbe implemented" ); totalTests++;
        HashTable<string> tbl;
        try                                                    { tbl.QuadraticProbe( 1, 10 ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Check if PREREQ Hash2 implemented" ); totalTests++;
        HashTable<string> tbl;
        try                                                    { tbl.Hash2( 1, 2 ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Check if PREREQ SetCollisionMethod implemented" ); totalTests++;
        HashTable<string> tbl;
        try                                                    { tbl.SetCollisionMethod( CollisionMethod::DOUBLE_HASH ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        HashTable<string> tbl;
        try                                                    { tbl.Get( 1 ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Get LINEAR" );
        HashTable<int> tbl; // size 5
        tbl.SetCollisionMethod( CollisionMethod::LINEAR );
        tbl.m_table.m_array[0].used = true;
        tbl.m_table.m_array[0].data.key = 5;
        tbl.m_table.m_array[0].data.data = 1;
        tbl.m_table.m_array[1].used = true;
        tbl.m_table.m_array[1].data.key = 6;
        tbl.m_table.m_array[1].data.data = 2;
        tbl.m_table.m_array[2].used = true;
        tbl.m_table.m_array[2].data.key = 0;
        tbl.m_table.m_array[2].data.data = 3;
        tbl.m_table.m_itemCount = 3;

        int positions[] = { 5, 6, 0 };
        int expected[] = { 1, 2, 3 };
        int results[] = { -1, -1, -1 };
        for ( int i = 0; i < 3; i++ )
        {
            try
            {
                results[i] = tbl.Get( positions[i] );
                testsPassed += Test_DoTheyMatch( expected[i], results[i], testName, "tbl.Get( " + ToString( positions[i] ) + " )" );
            }
            catch( ... )
            {
                cout << YELLOW << "NOTHING FOUND AT INDEX " << i << "! Do you have a logic error in your Push function?" << endl;
            }
        }

        totalTests++; testsPassed += Test_DoTheyMatch( 1, tbl.Get( 5 ), testName, "tbl.Get( 5 )" );
        totalTests++; testsPassed += Test_DoTheyMatch( 2, tbl.Get( 6 ), testName, "tbl.Get( 6 )" );
        totalTests++; testsPassed += Test_DoTheyMatch( 3, tbl.Get( 0 ), testName, "tbl.Get( 0 )" );
        Test_End();
    } cout << CLEAR;

    {
        string testName = Test_Begin( "Get QUADRATIC" );
        HashTable<int> tbl; // size 5
        tbl.SetCollisionMethod( CollisionMethod::QUADRATIC );
        tbl.m_table.m_array[0].used = true;
        tbl.m_table.m_array[0].data.key = 5;
        tbl.m_table.m_array[0].data.data = 1;
        tbl.m_table.m_array[1].used = true;
        tbl.m_table.m_array[1].data.key = 6;
        tbl.m_table.m_array[1].data.data = 2;
        tbl.m_table.m_array[4].used = true;
        tbl.m_table.m_array[4].data.key = 0;
        tbl.m_table.m_array[4].data.data = 3;
        tbl.m_table.m_itemCount = 3;

        int positions[] = { 5, 6, 0 };
        int expected[] = { 1, 2, 3 };
        int results[] = { -1, -1, -1 };
        for ( int i = 0; i < 3; i++ )
        {
            try
            {
                results[i] = tbl.Get( positions[i] );
                testsPassed += Test_DoTheyMatch( expected[i], results[i], testName, "tbl.Get( " + ToString( positions[i] ) + " )" );
            }
            catch( ... )
            {
                cout << YELLOW << "NOTHING FOUND AT INDEX " << i << "! Do you have a logic error in your Push function?" << endl;
            }
        }

        totalTests++; testsPassed += Test_DoTheyMatch( 1, tbl.Get( 5 ), testName, "tbl.Get( 5 )" );
        totalTests++; testsPassed += Test_DoTheyMatch( 2, tbl.Get( 6 ), testName, "tbl.Get( 6 )" );
        totalTests++; testsPassed += Test_DoTheyMatch( 3, tbl.Get( 0 ), testName, "tbl.Get( 0 )" );

        Test_End();
    } cout << CLEAR;

    {
        string testName = Test_Begin( "Get DOUBLE_HASH" );
        HashTable<int> tbl; // size 5
        tbl.SetCollisionMethod( CollisionMethod::DOUBLE_HASH );
        tbl.m_table.m_array[0].used = true;
        tbl.m_table.m_array[0].data.key = 0;
        tbl.m_table.m_array[0].data.data = 1;
        tbl.m_table.m_array[1].used = true;
        tbl.m_table.m_array[1].data.key = 1;
        tbl.m_table.m_array[1].data.data = 2;
        tbl.m_table.m_array[4].used = true;
        tbl.m_table.m_array[4].data.key = 6;
        tbl.m_table.m_array[4].data.data = 3;
        tbl.m_table.m_itemCount = 3;

        int positions[] = { 0, 1, 6 };
        int expected[] = { 1, 2, 3 };
        int results[] = { -1, -1, -1 };
        for ( int i = 0; i < 3; i++ )
        {
            try
            {
                results[i] = tbl.Get( positions[i] );
                testsPassed += Test_DoTheyMatch( expected[i], results[i], testName, "tbl.Get( " + ToString( positions[i] ) + " )" );
            }
            catch( ... )
            {
                cout << YELLOW << "NOTHING FOUND AT INDEX " << i << "! Do you have a logic error in your Push function?" << endl;
            }
        }

        totalTests++; testsPassed += Test_DoTheyMatch( 1, tbl.Get( 0 ), testName, "tbl.Get( 5 )" );
        totalTests++; testsPassed += Test_DoTheyMatch( 2, tbl.Get( 1 ), testName, "tbl.Get( 6 )" );
        totalTests++; testsPassed += Test_DoTheyMatch( 3, tbl.Get( 6 ), testName, "tbl.Get( 0 )" );

        Test_End();
    } cout << CLEAR;

    {
        string testName = Test_Begin( "Check for exception thrown when structure is empty" );
        HashTable<int> tbl; // size 5
        tbl.m_table.m_itemCount = 0;

        string exceptionType = "";
        string expectedException = "StructureEmptyException";

        try
        {
            tbl.Get( 1 );
        }
        catch( const Exception::StructureEmptyException& ex )
        {
            exceptionType = "StructureEmptyException";
        }
        catch( ... )
        {
            exceptionType = "Unknown exception";
        }

        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionType, testName, "Exception?" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

