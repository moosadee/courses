#ifndef _FIXED_ARRAY_TESTER
#define _FIXED_ARRAY_TESTER

#include "FixedArray.h"

class FixedArrayTester
{
public:
  void RunAll();
  void Test_FixedArray_Constructor();
  void Test_FixedArray_ShiftLeft();
  void Test_FixedArray_ShiftRight();
  void Test_FixedArray_PopBack();
  void Test_FixedArray_PopFront();
  void Test_FixedArray_PopAt();
  void Test_FixedArray_GetBack();
  void Test_FixedArray_GetFront();
  void Test_FixedArray_GetAt();
  void Test_FixedArray_IsFull();
  void Test_FixedArray_IsEmpty();
  void Test_FixedArray_PushBack();
  void Test_FixedArray_PushFront();
  void Test_FixedArray_PushAt();
  void Test_FixedArray_Search();

};
#endif

