#ifndef _SMART_FIXED_ARRAY_HPP
#define _SMART_FIXED_ARRAY_HPP

// C++ Library includes
#include <iostream>

// Project includes
#include "../../Exceptions/ItemNotFoundException.h"
#include "../../Exceptions/NotImplementedException.h"
#include "../../Exceptions/StructureFullException.h"
#include "../../Exceptions/StructureEmptyException.h"
#include "../../Exceptions/InvalidIndexException.h"
#include "../../Utilities/Strings.h"

template <typename T>
//! A data structure that wraps a fixed array
class FixedArray
{
public:
    FixedArray();
    ~FixedArray();

    //! Insert an item to the END of the array.
    void PushBack( T newItem );
    //! Insert an item to the BEGINNING of the array.
    void PushFront( T newItem );
    //! Insert an item at some index in the array.
    void PushAt( size_t index, T newItem );

    //! Remove the LAST item in the array.
    void PopBack();
    //! Remove the FRONT item in the array. Shift everything to the left.
    void PopFront();
    //! Remove an item in the middle of the array. Close up the gap.
    void PopAt( size_t index );

    //! Get the LAST item in the array.
    T& GetBack();
    //! Get the FIRST item in the array.
    T& GetFront();
    //! Get an item in the array at some index.
    T& GetAt( size_t index );

    //! Search for an item by its value, return the index of its position.
    size_t Search( T item ) const;

    //! Returns the amount of items currently stored in the array.
    size_t Size() const;

    //! Check if the array is currently empty.
    bool IsEmpty() const;

    //! Check if the array is currently full.
    bool IsFull() const;

    //! Clear the data.
    void Clear();

private:
    //! The pointer used for the dynamic array
    T m_array[100];

    //! The current size of the array
    const size_t ARRAY_SIZE;

    //! The current amount of items inserted into the array
    size_t m_itemCount;

    /* Private member methods */
    //! Move all items past the given index to the left.
    void ShiftLeft( size_t index );

    //! Move all items past the given index to the right.
    void ShiftRight( size_t index );

    friend class FixedArrayTester;
};

template <typename T>
FixedArray<T>::FixedArray()
    : ARRAY_SIZE( 100 )
{
    Clear();
    m_itemCount = 0;
}

template <typename T>
FixedArray<T>::~FixedArray()
{
    Clear();
}

template <typename T>
void FixedArray<T>::Clear()
{
    m_itemCount = 0;
}

template <typename T>
size_t FixedArray<T>::Size() const
{
    throw Exception::NotImplementedException( "FixedArray<T>::Size" ); // Erase this once you work on this function
}

template <typename T>
bool FixedArray<T>::IsFull() const
{
    throw Exception::NotImplementedException( "FixedArray<T>::IsFull" ); // Erase this once you work on this function
}

template <typename T>
bool FixedArray<T>::IsEmpty() const
{
    throw Exception::NotImplementedException( "FixedArray<T>::IsEmpty" ); // Erase this once you work on this function
}

template <typename T>
void FixedArray<T>::ShiftLeft( size_t index )
{
    throw Exception::NotImplementedException( "FixedArray<T>::ShiftLeft" ); // Erase this once you work on this function
}

template <typename T>
void FixedArray<T>::ShiftRight( size_t index )
{
    throw Exception::NotImplementedException( "FixedArray<T>::ShiftRight" ); // Erase this once you work on this function
}

template <typename T>
void FixedArray<T>::PushBack( T newItem )
{
    throw Exception::NotImplementedException( "FixedArray<T>::PushBack" ); // Erase this once you work on this function
}

template <typename T>
void FixedArray<T>::PushFront( T newItem )
{
    throw Exception::NotImplementedException( "FixedArray<T>::PushFront" ); // Erase this once you work on this function
}

template <typename T>
void FixedArray<T>::PushAt( size_t index, T newItem )
{
    throw Exception::NotImplementedException( "FixedArray<T>::PushAt" ); // Erase this once you work on this function
}

template <typename T>
void FixedArray<T>::PopBack()
{
    throw Exception::NotImplementedException( "FixedArray<T>::PopBack" ); // Erase this once you work on this function
}

template <typename T>
void FixedArray<T>::PopFront()
{
    throw Exception::NotImplementedException( "FixedArray<T>::PopFront" ); // Erase this once you work on this function
}

template <typename T>
void FixedArray<T>::PopAt( size_t index )
{
    throw Exception::NotImplementedException( "FixedArray<T>::PopAt" ); // Erase this once you work on this function
}

template <typename T>
T& FixedArray<T>::GetBack()
{
    throw Exception::NotImplementedException( "FixedArray<T>::GetBack" ); // Erase this once you work on this function
}

template <typename T>
T& FixedArray<T>::GetFront()
{
    throw Exception::NotImplementedException( "FixedArray<T>::GetFront" ); // Erase this once you work on this function
}

template <typename T>
T& FixedArray<T>::GetAt( size_t index )
{
    throw Exception::NotImplementedException( "FixedArray<T>::GetAt" ); // Erase this once you work on this function
}

template <typename T>
size_t FixedArray<T>::Search( T item ) const
{
    throw Exception::NotImplementedException( "FixedArray<T>::Search" ); // Erase this once you work on this function
}

#endif
