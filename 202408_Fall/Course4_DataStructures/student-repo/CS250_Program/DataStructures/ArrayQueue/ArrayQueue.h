#ifndef ARRAY_QUEUE_HPP
#define ARRAY_QUEUE_HPP

// Project includes
#include "../DynamicArray/DynamicArray.h"
#include "../../Exceptions/NotImplementedException.h"
#include "../../Exceptions/StructureFullException.h"
#include "../../Exceptions/InvalidIndexException.h"
#include "../../Exceptions/NullptrException.h"

template <typename T>
//! A first-in-first-out (FIFO) queue structure built on top of an array
class ArrayQueue
{
    public:
    //! Push a new item into the back of the queue
    void Push(const T& newData );
    //! Remove the item at the front of the queue
    void Pop();
    //! Access the data at the front of the queue
    T& Front();
    //! Get the amount of items in the queue
    int Size();
    //! Return whether the queue is empty
    bool IsEmpty();

    private:
    DynamicArray<T> m_vector;

    friend class ArrayQueueTester;
};

template <typename T>
void ArrayQueue<T>::Push( const T& newData )
{
    throw Exception::NotImplementedException( "ArrayQueue::Push is not implemented" );
}

template <typename T>
void ArrayQueue<T>::Pop()
{
    throw Exception::NotImplementedException( "ArrayQueue::Pop is not implemented" );
}

template <typename T>
T& ArrayQueue<T>::Front()
{
    throw Exception::NotImplementedException( "ArrayQueue::Front is not implemented" );
}

template <typename T>
int ArrayQueue<T>::Size()
{
    throw Exception::NotImplementedException( "ArrayQueue::Size is not implemented" );
}

template <typename T>
bool ArrayQueue<T>::IsEmpty()
{
    throw Exception::NotImplementedException( "ArrayQueue::IsEmpty is not implemented" );
}

#endif
