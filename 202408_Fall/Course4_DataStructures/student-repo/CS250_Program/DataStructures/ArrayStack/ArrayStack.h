#ifndef ARRAY_STACK_HPP
#define ARRAY_STACK_HPP

#include "../DynamicArray/DynamicArray.h"
#include "../../Exceptions/NotImplementedException.h"
#include "../../Exceptions/StructureFullException.h"
#include "../../Exceptions/InvalidIndexException.h"
#include "../../Exceptions/NullptrException.h"

template <typename T>
//! A last-in-first-out (LIFO) stack structure built on top of an array
class ArrayStack
{
    public:
    //! Push a new item into the back of the queue
    void Push(const T& newData );
    //! Remove the item at the front of the queue
    void Pop();
    //! Access the data at the front of the queue
    T& Top();
    //! Get the amount of items in the queue
    int Size();
    //! Return whether the queue is empty
    bool IsEmpty();

    private:
    DynamicArray<T> m_vector;

    friend class ArrayStackTester;
};

template <typename T>
void ArrayStack<T>::Push( const T& newData )
{
    throw Exception::NotImplementedException( "ArrayStack::Push is not implemented" );
}

template <typename T>
void ArrayStack<T>::Pop()
{
    throw Exception::NotImplementedException( "ArrayStack::Pop is not implemented" );
}

template <typename T>
T& ArrayStack<T>::Top()
{
    throw Exception::NotImplementedException( "ArrayStack::Front is not implemented" );
}

template <typename T>
int ArrayStack<T>::Size()
{
    throw Exception::NotImplementedException( "ArrayStack::Size is not implemented" );
}

template <typename T>
bool ArrayStack<T>::IsEmpty()
{
    throw Exception::NotImplementedException( "ArrayStack::IsEmpty is not implemented" );
}

#endif
