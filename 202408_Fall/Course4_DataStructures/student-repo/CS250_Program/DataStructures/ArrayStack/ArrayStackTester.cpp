#include "ArrayStackTester.h"
#include "ArrayStack.h"
#include "../../Utilities/Style.h"

void ArrayStackTester::RunAll()
{
    DisplayHeader( "ArrayStackTester - RunAll", 0 );
    Test_ArrayStack_Top();
    Test_ArrayStack_IsEmpty();
    Test_ArrayStack_Pop();
    Test_ArrayStack_Push();
    Test_ArrayStack_Size();
}

void ArrayStackTester::Test_ArrayStack_Push()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "ArrayStack::Push";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ IsEmpty implemented" ); totalTests++;
        ArrayStack<string> testStructure;
        try                                                    { testStructure.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Check if PREREQ Size implemented" ); totalTests++;
        ArrayStack<string> testStructure;
        try                                                    { testStructure.Size(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        ArrayStack<string> testStructure;
        try                                                    { testStructure.Push( "A" ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Push 1 item" );
        ArrayStack<string> testStructure;
        testStructure.Push( "cheese" );
        string exp1 = "not nullptr", exp2 = "not nullptr";
        totalTests++; testsPassed += Test_DoTheyMatch( 1,                  testStructure.Size(), testName, "testStructure.Size()" );
        if ( testStructure.m_vector.m_itemCount == 0 ) { cout << YELLOW << "testStructure.m_vector.m_itemCount IS 0; LEAVING TESTS EARLY." << CLEAR << endl; return; }
        totalTests++; testsPassed += Test_DoTheyMatch( string( "cheese" ), testStructure.m_vector.GetAt( 0 ), testName, "testStructure.m_vector.GetAt( 0 )" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Push 2 items, check order" );
        ArrayStack<string> testStructure;
        testStructure.Push( "A" );
        testStructure.Push( "B" );
        totalTests++; testsPassed += Test_DoTheyMatch( 2, testStructure.Size(), testName, "testStructure.Size()" );
        if ( testStructure.m_vector.m_itemCount == 0 ) { cout << YELLOW << "testStructure.m_vector.m_itemCount IS 0; LEAVING TESTS EARLY." << CLEAR << endl; return; }
        totalTests++; testsPassed += Test_DoTheyMatch( string( "A" ), testStructure.m_vector.GetAt( 0 ), testName, "testStructure.m_vector.GetAt( 0 )" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "B" ), testStructure.m_vector.GetAt( 1 ), testName, "testStructure.m_vector.GetAt( 1 )" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void ArrayStackTester::Test_ArrayStack_Pop()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "ArrayStack::Pop";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ IsEmpty implemented" ); totalTests++;
        ArrayStack<string> testStructure;
        try                                                    { testStructure.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Check if PREREQ Size implemented" ); totalTests++;
        ArrayStack<string> testStructure;
        try                                                    { testStructure.Size(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        ArrayStack<string> testStructure;
        try                                                    { testStructure.Pop(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Pop from top of list" );
        ArrayStack<string> testStructure;
        testStructure.m_vector.PushBack( "A" );
        testStructure.m_vector.PushBack( "B" );
        testStructure.m_vector.PushBack( "C" );

        testStructure.Pop();

        totalTests++; testsPassed += Test_DoTheyMatch( 2,             testStructure.Size(), testName, "testStructure.Size()" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "A" ), testStructure.m_vector.GetFront(), testName, "testStructure.m_vector.GetFront()" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "B" ), testStructure.m_vector.GetBack(),  testName, "testStructure.m_vector.GetBack()" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Can't pop from empty list" );
        ArrayStack<string> testStructure;
        string exceptionThrown = "";
        string expectedException = "Exception::StructureEmptyException";
        try                                                      { testStructure.Pop(); }
        catch ( const Exception::StructureEmptyException& ex )   { exceptionThrown = "Exception::StructureEmptyException"; }
        catch( ... )                                             { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "list.Pop()" );

        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void ArrayStackTester::Test_ArrayStack_Top()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "ArrayStack::Top";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ IsEmpty implemented" ); totalTests++;
        ArrayStack<string> testStructure;
        try                                                    { testStructure.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        ArrayStack<string> testStructure;
        try                                                    { testStructure.Top(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Get Top" );
        ArrayStack<string> testStructure;
        testStructure.m_vector.PushBack( "A" );
        testStructure.m_vector.PushBack( "B" );
        testStructure.m_vector.PushBack( "C" );

        string expOut = "C";
        string actOut = testStructure.Top();

        totalTests++; testsPassed += Test_DoTheyMatch( expOut, actOut, testName, "testStructure.Top()" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Can't get (Top) from empty list" );
        ArrayStack<string> testStructure;
        string exceptionThrown = "";
        string expectedException = "Exception::StructureEmptyException";
        try                                                      { testStructure.Top(); }
        catch ( const Exception::StructureEmptyException& ex )   { exceptionThrown = "Exception::StructureEmptyException"; }
        catch( ... )                                             { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "list.Top()" );

        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void ArrayStackTester::Test_ArrayStack_Size()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "ArrayStack::Size";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        ArrayStack<string> testStructure;
        try                                                    { testStructure.Size(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Size of empty structure?" );
        ArrayStack<string> testStructure;
        totalTests++; testsPassed += Test_DoTheyMatch( 0, testStructure.Size(), testName, "testStructure.Size()" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Push 2 items, check Size" );
        ArrayStack<string> testStructure;
        testStructure.m_vector.PushBack( "A" );
        testStructure.m_vector.PushBack( "B" );
        totalTests++; testsPassed += Test_DoTheyMatch( 2, testStructure.Size(), testName, "testStructure.Size()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void ArrayStackTester::Test_ArrayStack_IsEmpty()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "ArrayStack::IsEmpty";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        ArrayStack<string> testStructure;
        try                                                    { testStructure.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "IsEmpty() when structure is empty" );
        ArrayStack<string> testStructure;
        totalTests++; testsPassed += Test_DoTheyMatch( true, testStructure.IsEmpty(), testName, "testStructure.IsEmpty()" );
        Test_End();
    }

    {
        string testName = Test_Begin( "IsEmpty() when structure not empty" );
        ArrayStack<string> testStructure;
        testStructure.m_vector.PushBack( "A" );
        testStructure.m_vector.PushBack( "B" );
        totalTests++; testsPassed += Test_DoTheyMatch( false, testStructure.IsEmpty(), testName, "testStructure.IsEmpty()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

