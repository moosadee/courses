#include "LinkedStackTester.h"
#include "LinkedStack.h"
#include "../../Utilities/Style.h"

void LinkedStackTester::RunAll()
{
    DisplayHeader( "LinkedStackTester - RunAll", 0 );
    Test_LinkedStack_Top();
    Test_LinkedStack_IsEmpty();
    Test_LinkedStack_Pop();
    Test_LinkedStack_Push();
    Test_LinkedStack_Size();
}

void LinkedStackTester::Test_LinkedStack_Push()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "LinkedStack::Push";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ IsEmpty implemented" );
        LinkedStack<string> testStructure;
        try                                                    { testStructure.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Check if PREREQ Size implemented" ); totalTests++;
        LinkedStack<string> testStructure;
        try                                                    { testStructure.Size(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        LinkedStack<string> testStructure;
        try                                                    { testStructure.Push( "A" ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Push 1 item" );
        LinkedStack<string> testStructure;
        testStructure.Push( "cheese" );
        string exp1 = "not nullptr", exp2 = "not nullptr";
        totalTests++; testsPassed += Test_DoTheyMatch( 1,                  testStructure.m_list.m_itemCount, testName, "testStructure.m_list.m_itemCount" );
        if ( testStructure.m_list.m_itemCount == 0 ) { cout << YELLOW << "testStructure.m_list.m_itemCount IS 0; LEAVING TESTS EARLY." << CLEAR << endl; return; }
        totalTests++; testsPassed += Test_DoTheyMatch( string( "cheese" ), testStructure.m_list.GetAt( 0 ), testName, "testStructure.m_list.GetAt( 0 )" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Push 2 items, check order" );
        LinkedStack<string> testStructure;
        testStructure.Push( "A" );
        testStructure.Push( "B" );
        totalTests++; testsPassed += Test_DoTheyMatch( 2, testStructure.m_list.m_itemCount, testName, "testStructure.m_list.m_itemCount" );
        if ( testStructure.m_list.m_itemCount == 0 ) { cout << YELLOW << "testStructure.m_list.m_itemCount IS 0; LEAVING TESTS EARLY." << CLEAR << endl; return; }
        totalTests++; testsPassed += Test_DoTheyMatch( string( "A" ), testStructure.m_list.GetAt( 0 ), testName, "testStructure.m_list.GetAt( 0 )" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "B" ), testStructure.m_list.GetAt( 1 ), testName, "testStructure.m_list.GetAt( 1 )" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void LinkedStackTester::Test_LinkedStack_Pop()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "LinkedStack::Pop";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ IsEmpty implemented" ); totalTests++;
        LinkedStack<string> testStructure;
        try                                                    { testStructure.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Check if PREREQ Size implemented" ); totalTests++;
        LinkedStack<string> testStructure;
        try                                                    { testStructure.Size(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        LinkedStack<string> testStructure;
        try                                                    { testStructure.Pop(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Pop from Top of list" );
        LinkedStack<string> testStructure;
        testStructure.m_list.PushBack( "A" );
        testStructure.m_list.PushBack( "B" );
        testStructure.m_list.PushBack( "C" );

        testStructure.Pop();

        totalTests++; testsPassed += Test_DoTheyMatch( 2,             testStructure.Size(), testName, "testStructure.Size()" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "A" ), testStructure.m_list.m_ptrFirst->m_data, testName, "testStructure.m_list.m_ptrFirst->m_data" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "B" ), testStructure.m_list.m_ptrLast->m_data,  testName, "testStructure.m_list.m_ptrLast->m_data" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Can't pop from empty list" );
        LinkedStack<string> testStructure;
        string exceptionThrown = "";
        string expectedException = "Exception::StructureEmptyException";
        try                                                      { testStructure.Pop(); }
        catch ( const Exception::StructureEmptyException& ex )   { exceptionThrown = "Exception::StructureEmptyException"; }
        catch( ... )                                             { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "list.Pop()" );

        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void LinkedStackTester::Test_LinkedStack_Top()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "LinkedStack::Top";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ IsEmpty implemented" ); totalTests++;
        LinkedStack<string> testStructure;
        try                                                    { testStructure.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        LinkedStack<string> testStructure;
        try                                                    { testStructure.Top(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Get Top" );
        LinkedStack<string> testStructure;
        testStructure.m_list.PushBack( "A" );
        testStructure.m_list.PushBack( "B" );
        testStructure.m_list.PushBack( "C" );

        string expOut = "C";
        string actOut = testStructure.Top();

        totalTests++; testsPassed += Test_DoTheyMatch( expOut, actOut, testName, "testStructure.Top()" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Can't get (Top) from empty list" );
        LinkedStack<string> testStructure;
        string exceptionThrown = "";
        string expectedException = "Exception::StructureEmptyException";
        try                                                      { testStructure.Top(); }
        catch ( const Exception::StructureEmptyException& ex )   { exceptionThrown = "Exception::StructureEmptyException"; }
        catch( ... )                                             { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "list.Top()" );

        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void LinkedStackTester::Test_LinkedStack_Size()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "LinkedStack::Size";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        LinkedStack<string> testStructure;
        try                                                    { testStructure.Size(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "Size of empty structure?" );
        LinkedStack<string> testStructure;
        totalTests++; testsPassed += Test_DoTheyMatch( 0, testStructure.Size(), testName, "testStructure.Size()" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Push 2 items, check Size" );
        LinkedStack<string> testStructure;
        testStructure.m_list.PushBack( "A" );
        testStructure.m_list.PushBack( "B" );
        totalTests++; testsPassed += Test_DoTheyMatch( 2, testStructure.Size(), testName, "testStructure.Size()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void LinkedStackTester::Test_LinkedStack_IsEmpty()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "LinkedStack::IsEmpty";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Is " + functionName + " implemented?" ); totalTests++;
        LinkedStack<string> testStructure;
        try                                                    { testStructure.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" ); }
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = Test_Begin( "IsEmpty() when structure is empty" );
        LinkedStack<string> testStructure;
        totalTests++; testsPassed += Test_DoTheyMatch( true, testStructure.IsEmpty(), testName, "testStructure.IsEmpty()" );
        Test_End();
    }

    {
        string testName = Test_Begin( "IsEmpty() when structure not empty" );
        LinkedStack<string> testStructure;
        testStructure.m_list.PushBack( "A" );
        testStructure.m_list.PushBack( "B" );
        totalTests++; testsPassed += Test_DoTheyMatch( false, testStructure.IsEmpty(), testName, "testStructure.IsEmpty()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

