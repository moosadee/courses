#ifndef _LINKED_STACK_TESTER
#define _LINKED_STACK_TESTER

class LinkedStackTester
{
public:
    void RunAll();
    void Test_LinkedStack_Push();
    void Test_LinkedStack_Pop();
    void Test_LinkedStack_Top();
    void Test_LinkedStack_Size();
    void Test_LinkedStack_IsEmpty();
};

#endif
