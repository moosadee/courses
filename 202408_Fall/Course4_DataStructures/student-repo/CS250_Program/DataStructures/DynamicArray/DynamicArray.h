#ifndef DYNAMIC_ARRAY
#define DYNAMIC_ARRAY

// C++ Library includes
#include <iostream>
#include <memory>
using namespace std;

// Project includes
#include "../../Exceptions/NotImplementedException.h"
#include "../../Exceptions/StructureFullException.h"
#include "../../Exceptions/StructureEmptyException.h"
#include "../../Exceptions/InvalidIndexException.h"
#include "../../Exceptions/ItemNotFoundException.h"
#include "../../Exceptions/NullptrException.h"

template <typename T>
//! A data structure that wraps a dynamic array
class DynamicArray
{
public:
    /* Public member methods */
    //! Sets up the DynamicArray.
    DynamicArray();
    //! Cleans up the DynamicArray.
    ~DynamicArray();

    //! Insert an item to the END of the array.
    void PushBack( T newItem );
    //! Insert an item to the BEGINNING of the array.
    void PushFront( T newItem );
    //! Insert an item at some index in the array.
    void PushAt( size_t index, T newItem );

    //! Remove the LAST item in the array.
    void PopBack();
    //! Remove the FRONT item in the array. Shift everything to the left.
    void PopFront();
    //! Remove an item in the middle of the array. Close up the gap.
    void PopAt( int index );

    //! Get the LAST item in the array.
    T& GetBack();
    //! Get the FIRST item in the array.
    T& GetFront();
    //! Get an item in the array at some index.
    T& GetAt( int index );

    //! Search for an item by its value, return the index of its position.
    int Search( T item ) const;

    //! Returns the amount of items currently stored in the array.
    int Size() const;

    //! Check if the array is currently empty.
    bool IsEmpty() const;

    //! Deallocates memory for the array and resets the member variables.
    void Clear();

private:
    /* Private member variables */
    //! The pointer used for the dynamic array
    T* m_array;

    //! The current size of the array
    int m_arraySize;

    //! The current amount of items inserted into the array
    int m_itemCount;

    /* Private member methods */
    //! Move all items past the given index to the left.
    void ShiftLeft( int index );
    //! Move all items past the given index to the right.
    void ShiftRight( int index );

    //! Allocate memory for the dynamic array.
    void AllocateMemory( int size );
    //! Resize the dynamic array.
    void Resize( int newSize );

    //! Check if the array is currently full.
    bool IsFull() const;

    friend class DynamicArrayTester;
    friend class ArrayQueueTester;
    friend class ArrayStackTester;
};

template <typename T>
DynamicArray<T>::DynamicArray()
{
    m_array = nullptr;
    Clear();
}

template <typename T>
DynamicArray<T>::~DynamicArray()
{
    Clear();
}

template <typename T>
void DynamicArray<T>::Clear()
{
    if ( m_array != nullptr )
    {
        delete[] m_array;
        m_array = nullptr;
    }
    m_arraySize = 0;
    m_itemCount = 0;
}

template <typename T>
int DynamicArray<T>::Size() const
{
    return m_itemCount;
}

template <typename T>
bool DynamicArray<T>::IsFull() const
{
    return ( m_itemCount == m_arraySize );
}

template <typename T>
bool DynamicArray<T>::IsEmpty() const
{
    throw Exception::NotImplementedException( "DynamicArray<T>::IsEmpty" ); // Erase this once you work on this function
}

template <typename T>
void DynamicArray<T>::AllocateMemory( int size )
{
    throw Exception::NotImplementedException( "DynamicArray<T>::AllocateMemory" ); // Erase this once you work on this function
}

template <typename T>
void DynamicArray<T>::Resize( int newSize )
{
    throw Exception::NotImplementedException( "DynamicArray<T>::Resize" ); // Erase this once you work on this function
}

template <typename T>
void DynamicArray<T>::ShiftLeft( int index )
{
    throw Exception::NotImplementedException( "DynamicArray<T>::ShiftLeft" ); // Erase this once you work on this function
}

template <typename T>
void DynamicArray<T>::ShiftRight( int index )
{
    throw Exception::NotImplementedException( "DynamicArray<T>::ShiftRight" ); // Erase this once you work on this function
}

template <typename T>
void DynamicArray<T>::PushBack( T newItem )
{
    throw Exception::NotImplementedException( "DynamicArray<T>::PushBack" ); // Erase this once you work on this function
}

template <typename T>
void DynamicArray<T>::PushFront( T newItem )
{
    throw Exception::NotImplementedException( "DynamicArray<T>::PushFront" ); // Erase this once you work on this function
}

template <typename T>
void DynamicArray<T>::PushAt( size_t index, T newItem )
{
    throw Exception::NotImplementedException( "DynamicArray<T>::PushAt" ); // Erase this once you work on this function
}

template <typename T>
void DynamicArray<T>::PopBack()
{
    throw Exception::NotImplementedException( "DynamicArray<T>::PopBack" ); // Erase this once you work on this function
}

template <typename T>
void DynamicArray<T>::PopFront()
{
    throw Exception::NotImplementedException( "DynamicArray<T>::PopFront" ); // Erase this once you work on this function
}

template <typename T>
void DynamicArray<T>::PopAt( int index )
{
    throw Exception::NotImplementedException( "DynamicArray<T>::PopAt" ); // Erase this once you work on this function
}

template <typename T>
T& DynamicArray<T>::GetBack()
{
    throw Exception::NotImplementedException( "DynamicArray<T>::GetBack" ); // Erase this once you work on this function
}

template <typename T>
T& DynamicArray<T>::GetFront()
{
    throw Exception::NotImplementedException( "DynamicArray<T>::GetFront" ); // Erase this once you work on this function
}

template <typename T>
T& DynamicArray<T>::GetAt( int index )
{
    throw Exception::NotImplementedException( "DynamicArray<T>::GetAt" ); // Erase this once you work on this function
}

template <typename T>
int DynamicArray<T>::Search( T item ) const
{
    throw Exception::NotImplementedException( "DynamicArray<T>::Search" ); // Erase this once you work on this function
}

#endif
