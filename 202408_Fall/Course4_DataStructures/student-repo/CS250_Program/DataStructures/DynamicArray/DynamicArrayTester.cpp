#include "DynamicArrayTester.h"
#include "../../Utilities/Style.h"

void DynamicArrayTester::RunAll()
{
    DisplayHeader( "DynamicArrayTester - RunAll", 0 );
    Test_DynamicArray_Constructor();
    Test_DynamicArray_ShiftLeft();
    Test_DynamicArray_ShiftRight();
    Test_DynamicArray_PopBack();
    Test_DynamicArray_PopFront();
    Test_DynamicArray_PopAt();
    Test_DynamicArray_GetBack();
    Test_DynamicArray_GetFront();
    Test_DynamicArray_GetAt();
    Test_DynamicArray_IsFull();
    Test_DynamicArray_IsEmpty();
    Test_DynamicArray_PushBack();
    Test_DynamicArray_PushFront();
    Test_DynamicArray_PushAt();
    Test_DynamicArray_Search();
    Test_DynamicArray_AllocateMemory();
    Test_DynamicArray_Resize();
}

void DynamicArrayTester::Test_DynamicArray_Constructor()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "DynamicArray Constructor";

    {
        string testName = "DynamicArray obj; check values after constructor";
        DynamicArray<int> obj;
        totalTests++; testsPassed += Test_DoTheyMatch( int( 0 ),      obj.m_arraySize, testName, "obj.m_arraySize" );
        totalTests++; testsPassed += Test_DoTheyMatch( int( 0 ),      obj.m_itemCount, testName, "obj.m_itemCount" );
        string expOut = "nullptr";
        string actOut = ( obj.m_array == nullptr ) ? "nullptr" : "not nullptr";
        totalTests++; testsPassed += Test_DoTheyMatch( expOut, actOut, testName, "obj.m_array" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void DynamicArrayTester::Test_DynamicArray_ShiftLeft()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "DynamicArray::ShiftLeft";
    bool prereqImplemented = true;
    {
        string testName = "Check " + functionName + " is implemented"; totalTests++;
        DynamicArray<char> obj;
        string exceptionMessage = "";
        try                                                    { obj.m_itemCount = 5; obj.ShiftLeft( 2 ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Populate array, call ShiftLeft, check positions";
        DynamicArray<std::string> arr;
        arr.m_array = new string[ 10 ];
        arr.m_arraySize = 10;
        arr.m_itemCount = 4;
        arr.m_array[0] = "a"; arr.m_array[1] = "b"; arr.m_array[2] = "c"; arr.m_array[3] = "d";
        arr.ShiftLeft( 1 );

        totalTests++; testsPassed += Test_DoTheyMatch( 4,             arr.m_itemCount, testName, "testList.m_itemCount" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "a" ), arr.m_array[0],  testName, "arr.m_array[0]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "c" ), arr.m_array[1],  testName, "arr.m_array[1]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "d" ), arr.m_array[2],  testName, "arr.m_array[2]" );

        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void DynamicArrayTester::Test_DynamicArray_ShiftRight()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "DynamicArray::ShiftRight";
    bool prereqImplemented = true;
    {
        string testName = "Check " + functionName + " is implemented"; totalTests++;
        DynamicArray<char> obj;
        string exceptionMessage = "";
        try                                                    { obj.m_itemCount = 5; obj.ShiftRight( 2 ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Populate array, call ShiftRight, check positions";
        DynamicArray<std::string> arr;
        arr.m_array = new string[ 10 ];
        arr.m_arraySize = 10;
        arr.m_itemCount = 4;
        arr.m_array[0] = "a"; arr.m_array[1] = "b"; arr.m_array[2] = "c"; arr.m_array[3] = "d";
        arr.ShiftRight( 1 );

        totalTests++; testsPassed += Test_DoTheyMatch( 4,             arr.m_itemCount, testName, "testList.m_itemCount" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "a" ), arr.m_array[0],  testName, "arr.m_array[0]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "b" ), arr.m_array[2],  testName, "arr.m_array[2]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "c" ), arr.m_array[3],  testName, "arr.m_array[3]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "d" ), arr.m_array[4],  testName, "arr.m_array[4]" );

        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void DynamicArrayTester::Test_DynamicArray_PopBack()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "DynamicArray::PopBack";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ IsEmpty implemented" ); totalTests++;
        DynamicArray<char> obj;
        try                                                    { obj.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Check " + functionName + " is implemented"; totalTests++;
        DynamicArray<char> obj;
        string exceptionMessage = "";
        try                                                    { obj.PopBack(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Set up array, use PopBack, check";
        DynamicArray<std::string> arr;
        arr.m_array = new string[10];
        arr.m_arraySize = 10;
        arr.m_itemCount = 3;
        arr.m_array[0] = "a"; arr.m_array[1] = "b"; arr.m_array[2] = "c";

        arr.PopBack();

        totalTests++; testsPassed += Test_DoTheyMatch( int( 2 ),      arr.m_itemCount, testName, "arr.m_itemCount" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "a" ), arr.m_array[0],  testName, "arr.m_array[0]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "b" ), arr.m_array[1],  testName, "arr.m_array[1]" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Can't pop from empty list" );
        DynamicArray<std::string> arr; arr.m_itemCount = 0;
        string exceptionThrown = "";
        string expectedException = "Exception::StructureEmptyException";
        try                                                      { arr.PopBack(); }
        catch ( const Exception::StructureEmptyException& ex )   { exceptionThrown = "Exception::StructureEmptyException"; }
        catch( ... )                                             { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "list.GetFront()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void DynamicArrayTester::Test_DynamicArray_PopFront()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "DynamicArray::PopFront";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ IsEmpty implemented" ); totalTests++;
        DynamicArray<char> obj;
        try                                                    { obj.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Check " + functionName + " is implemented"; totalTests++;
        DynamicArray<char> obj;
        string exceptionMessage = "";
        try                                                    { obj.PopFront(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Set up array, use PopFront, check";
        DynamicArray<std::string> arr;
        arr.m_array = new string[10];
        arr.m_arraySize = 10;
        arr.m_itemCount = 3;
        arr.m_array[0] = "a"; arr.m_array[1] = "b"; arr.m_array[2] = "c";

        arr.PopFront();

        totalTests++; testsPassed += Test_DoTheyMatch( int( 2 ),      arr.m_itemCount, testName, "arr.m_itemCount" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "b" ), arr.m_array[0],  testName, "arr.m_array[0]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "c" ), arr.m_array[1],  testName, "arr.m_array[1]" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Can't pop from empty list" );
        DynamicArray<std::string> arr; arr.m_itemCount = 0;
        string exceptionThrown = "";
        string expectedException = "Exception::StructureEmptyException";
        try                                                      { arr.PopFront(); }
        catch ( const Exception::StructureEmptyException& ex )   { exceptionThrown = "Exception::StructureEmptyException"; }
        catch( ... )                                             { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "list.GetFront()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void DynamicArrayTester::Test_DynamicArray_PopAt()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "DynamicArray::PopAt";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ IsEmpty implemented" ); totalTests++;
        DynamicArray<char> obj;
        try                                                    { obj.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Check " + functionName + " is implemented"; totalTests++;
        DynamicArray<char> obj;
        string exceptionMessage = "";
        try                                                    { obj.PopAt( 1 ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Set up array, use PopAt( 1 ), check";
        DynamicArray<std::string> arr;
        arr.m_array = new string[10];
        arr.m_arraySize = 10;
        arr.m_itemCount = 3;
        arr.m_array[0] = "a"; arr.m_array[1] = "b"; arr.m_array[2] = "c";

        arr.PopAt( 1 );

        totalTests++; testsPassed += Test_DoTheyMatch( int( 2 ),      arr.m_itemCount, testName, "arr.m_itemCount" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "a" ), arr.m_array[0],  testName, "arr.m_array[0]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "c" ), arr.m_array[1],  testName, "arr.m_array[1]" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Can't pop from empty list" );
        DynamicArray<std::string> arr; arr.m_itemCount = 0;
        string exceptionThrown = "";
        string expectedException = "Exception::StructureEmptyException";
        try                                                      { arr.PopAt( 1 ); }
        catch ( const Exception::StructureEmptyException& ex )   { exceptionThrown = "Exception::StructureEmptyException"; }
        catch( ... )                                             { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "arr.PopAt( 1 )" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Negative index marked invalid?" );
        DynamicArray<std::string> arr; arr.m_itemCount = 5;
        string exceptionThrown = "";
        string expectedException = "Exception::InvalidIndexException";
        try                                                    { arr.PopAt( -5 ); }
        catch ( const Exception::InvalidIndexException& ex )   { exceptionThrown = "Exception::InvalidIndexException"; }
        catch( ... )                                           { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "arr.PopAt( -5 )" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Out-of-bounds index marked invalid?" );
        DynamicArray<std::string> arr; arr.m_itemCount = 5;
        string exceptionThrown = "";
        string expectedException = "Exception::InvalidIndexException";
        try                                                    { arr.PopAt( 6 ); }
        catch ( const Exception::InvalidIndexException& ex )   { exceptionThrown = "Exception::InvalidIndexException"; }
        catch( ... )                                           { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "arr.PopAt( 6 )" );
        Test_End();
    }


    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void DynamicArrayTester::Test_DynamicArray_GetBack()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "DynamicArray::GetBack";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ IsEmpty implemented" ); totalTests++;
        DynamicArray<char> obj;
        try                                                    { obj.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Check " + functionName + " is implemented"; totalTests++;
        DynamicArray<char> obj;
        string exceptionMessage = "";
        try                                                    { obj.GetBack(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Set up array, use GetBack(), check";
        DynamicArray<std::string> arr;
        arr.m_array = new string[10];
        arr.m_arraySize = 10;
        arr.m_itemCount = 3;
        arr.m_array[0] = "a"; arr.m_array[1] = "b"; arr.m_array[2] = "c";
        totalTests++; testsPassed += Test_DoTheyMatch( string( "c" ), arr.GetBack(),  testName, "arr.GetBack()" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Can't get from empty list" );
        DynamicArray<std::string> arr; arr.m_itemCount = 0;
        string exceptionThrown = "";
        string expectedException = "Exception::StructureEmptyException";
        try                                                      { arr.GetBack(); }
        catch ( const Exception::StructureEmptyException& ex )   { exceptionThrown = "Exception::StructureEmptyException"; }
        catch( ... )                                             { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "arr.GetBack()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void DynamicArrayTester::Test_DynamicArray_GetFront()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "DynamicArray::GetFront";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ IsEmpty implemented" ); totalTests++;
        DynamicArray<char> obj;
        try                                                    { obj.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Check " + functionName + " is implemented"; totalTests++;
        DynamicArray<char> obj;
        string exceptionMessage = "";
        try                                                    { obj.GetFront(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Set up array, use GetFront(), check";
        DynamicArray<std::string> arr;
        arr.m_array = new string[10];
        arr.m_arraySize = 10;
        arr.m_itemCount = 3;
        arr.m_array[0] = "a"; arr.m_array[1] = "b"; arr.m_array[2] = "c";
        totalTests++; testsPassed += Test_DoTheyMatch( string( "a" ), arr.GetFront(),  testName, "arr.GetFront()" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Can't get from empty list" );
        DynamicArray<std::string> arr; arr.m_itemCount = 0;
        string exceptionThrown = "";
        string expectedException = "Exception::StructureEmptyException";
        try                                                      { arr.GetFront(); }
        catch ( const Exception::StructureEmptyException& ex )   { exceptionThrown = "Exception::StructureEmptyException"; }
        catch( ... )                                             { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "arr.GetBack()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void DynamicArrayTester::Test_DynamicArray_GetAt()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "DynamicArray::GetAt";
    bool prereqImplemented = true;
    {
        string testName = Test_Begin( "Check if PREREQ IsEmpty implemented" ); totalTests++;
        DynamicArray<char> obj;
        try                                                    { obj.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Check " + functionName + " is implemented"; totalTests++;
        DynamicArray<char> obj;
        string exceptionMessage = "";
        try                                                    { obj.GetAt( 1 ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Set up array, use GetAt( 1 ), check";
        DynamicArray<std::string> arr;
        arr.m_array = new string[10];
        arr.m_arraySize = 10;
        arr.m_itemCount = 3;
        arr.m_array[0] = "a"; arr.m_array[1] = "b"; arr.m_array[2] = "c";
        totalTests++; testsPassed += Test_DoTheyMatch( string( "b" ), arr.GetAt( 1 ),  testName, "arr.GetAt( 1 )" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Can't get from empty list" );
        DynamicArray<std::string> arr; arr.m_itemCount = 0;
        string exceptionThrown = "";
        string expectedException = "Exception::StructureEmptyException";
        try                                                      { arr.GetAt( 1 ); }
        catch ( const Exception::StructureEmptyException& ex )   { exceptionThrown = "Exception::StructureEmptyException"; }
        catch( ... )                                             { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "arr.GetAt( 1 )" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Negative index marked invalid?" );
        DynamicArray<std::string> arr; arr.m_itemCount = 5;
        string exceptionThrown = "";
        string expectedException = "Exception::InvalidIndexException";
        try                                                    { arr.GetAt( -5 ); }
        catch ( const Exception::InvalidIndexException& ex )   { exceptionThrown = "Exception::InvalidIndexException"; }
        catch( ... )                                           { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "arr.GetAt( -5 )" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Out-of-bounds index marked invalid?" );
        DynamicArray<std::string> arr; arr.m_itemCount = 5;
        string exceptionThrown = "";
        string expectedException = "Exception::InvalidIndexException";
        try                                                    { arr.GetAt( 6 ); }
        catch ( const Exception::InvalidIndexException& ex )   { exceptionThrown = "Exception::InvalidIndexException"; }
        catch( ... )                                           { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "arr.GetAt( 6 )" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void DynamicArrayTester::Test_DynamicArray_IsEmpty()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "DynamicArray::IsEmpty";
    bool prereqImplemented = true;
    {
        string testName = "Check " + functionName + " is implemented"; totalTests++;
        DynamicArray<char> obj;
        string exceptionMessage = "";
        try                                                    { obj.IsEmpty(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Check IsEmpty returns true if array is empty.";
        DynamicArray<int> arr;
        arr.m_array = new int[10]; arr.m_arraySize = 10;
        arr.m_itemCount = 0;
        totalTests++; testsPassed += Test_DoTheyMatch( true, arr.IsEmpty(), testName, "arr.IsEmpty()" );
        Test_End();
    }

    {
        string testName = "Check IsEmpty returns true if array is NOT empty.";
        DynamicArray<int> arr;
        arr.m_array = new int[10]; arr.m_arraySize = 10;
        arr.m_itemCount = 5;
        totalTests++; testsPassed += Test_DoTheyMatch( false, arr.IsEmpty(), testName, "arr.IsEmpty()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void DynamicArrayTester::Test_DynamicArray_IsFull()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "DynamicArray::IsFull";
    bool prereqImplemented = true;
    {
        string testName = "Check " + functionName + " is implemented"; totalTests++;
        DynamicArray<char> obj;
        string exceptionMessage = "";
        try                                                    { obj.IsFull(); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Check IsFull returns true if array is full.";
        DynamicArray<int> arr;
        arr.m_array = new int[10]; arr.m_arraySize = 10;
        arr.m_itemCount = arr.m_arraySize;
        totalTests++; testsPassed += Test_DoTheyMatch( true, arr.IsFull(), testName, "arr.IsFull()" );
        Test_End();
    }

    {
        string testName = "Check IsFull returns true if array is NOT full.";
        DynamicArray<int> arr;
        arr.m_array = new int[10]; arr.m_arraySize = 10;
        arr.m_itemCount = arr.m_arraySize - 1;
        totalTests++; testsPassed += Test_DoTheyMatch( false, arr.IsFull(), testName, "arr.IsFull()" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void DynamicArrayTester::Test_DynamicArray_PushBack()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "DynamicArray::PushBack";
    bool prereqImplemented = true;
    {
        string testName = "Check " + functionName + " is implemented"; totalTests++;
        DynamicArray<char> obj;
        string exceptionMessage = "";
        try                                                    { obj.PushBack( 'A' ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "PushBack('A')";
        DynamicArray<char> arr;
        arr.m_array = new char[10]; arr.m_arraySize = 10;

        string exceptionThrown = "";
        try             { arr.PushBack( 'A' ); }
        catch ( ... )   { exceptionThrown = "Unknown exception"; }

        totalTests++; testsPassed += Test_DoTheyMatch( string( "" ), exceptionThrown, testName, "arr.PushBack( 'A' )" );
        totalTests++; testsPassed += Test_DoTheyMatch( 'A',          arr.m_array[0],  testName, "arr.m_array[0]" );
        totalTests++; testsPassed += Test_DoTheyMatch( 1,            arr.m_itemCount, testName, "arr.m_itemCounts]" );

        Test_End();
    }

    {
        string testName = "PushBack('A') PushBack('B')";
        DynamicArray<char> arr;
        arr.m_array = new char[10]; arr.m_arraySize = 10;

        string exceptionThrown = "";
        try             { arr.PushBack( 'A' ); arr.PushBack( 'B' ); }
        catch ( ... )   { exceptionThrown = "Unknown exception"; }

        totalTests++; testsPassed += Test_DoTheyMatch( string( "" ), exceptionThrown, testName, "arr.PushBack( 'A' )" );
        totalTests++; testsPassed += Test_DoTheyMatch( 'A',          arr.m_array[0],  testName, "arr.m_array[0]" );
        totalTests++; testsPassed += Test_DoTheyMatch( 'B',          arr.m_array[1],  testName, "arr.m_array[1]" );
        totalTests++; testsPassed += Test_DoTheyMatch( 2,            arr.m_itemCount, testName, "arr.m_itemCounts]" );

        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void DynamicArrayTester::Test_DynamicArray_PushFront()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "DynamicArray::PushFront";
    bool prereqImplemented = true;
    {
        string testName = "Check " + functionName + " is implemented"; totalTests++;
        DynamicArray<char> obj;
        string exceptionMessage = "";
        try                                                    { obj.PushFront( 'A' ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "PushFront('A')";
        DynamicArray<char> arr;
        arr.m_array = new char[10]; arr.m_arraySize = 10;

        string exceptionThrown = "";
        try             { arr.PushFront( 'A' ); }
        catch ( ... )   { exceptionThrown = "Unknown exception"; }

        totalTests++; testsPassed += Test_DoTheyMatch( string( "" ), exceptionThrown, testName, "arr.PushBack( 'A' )" );
        totalTests++; testsPassed += Test_DoTheyMatch( 'A',          arr.m_array[0],  testName, "arr.m_array[0]" );
        totalTests++; testsPassed += Test_DoTheyMatch( 1,            arr.m_itemCount, testName, "arr.m_itemCounts]" );

        Test_End();
    }

    {
        string testName = "PushFront('A') PushFront('B')";
        DynamicArray<char> arr;
        arr.m_array = new char[10]; arr.m_arraySize = 10;

        string exceptionThrown = "";
        try             { arr.PushFront( 'A' ); arr.PushFront( 'B' ); }
        catch ( ... )   { exceptionThrown = "Unknown exception"; }

        totalTests++; testsPassed += Test_DoTheyMatch( string( "" ), exceptionThrown, testName, "arr.PushBack( 'A' )" );
        totalTests++; testsPassed += Test_DoTheyMatch( 'B',          arr.m_array[0],  testName, "arr.m_array[0]" );
        totalTests++; testsPassed += Test_DoTheyMatch( 'A',          arr.m_array[1],  testName, "arr.m_array[1]" );
        totalTests++; testsPassed += Test_DoTheyMatch( 2,            arr.m_itemCount, testName, "arr.m_itemCounts]" );

        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void DynamicArrayTester::Test_DynamicArray_PushAt()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "DynamicArray::PushAt";
    bool prereqImplemented = true;
    {
        string testName = "Check " + functionName + " is implemented"; totalTests++;
        DynamicArray<char> obj;
        string exceptionMessage = "";
        try                                                    { obj.PushAt( 1, 'A' ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "PushAt( 1, 'Z' )";
        DynamicArray<char> arr;
        arr.m_array = new char[10]; arr.m_arraySize = 10;
        arr.m_itemCount = 3;
        arr.m_array[0] = 'A'; arr.m_array[1] = 'B'; arr.m_array[2] = 'C';

        string exceptionThrown = "";
        try             { arr.PushAt( 1, 'Z' ); }
        catch ( ... )   { exceptionThrown = "Unknown exception"; }

        totalTests++; testsPassed += Test_DoTheyMatch( string( "" ), exceptionThrown, testName, "arr.PushAt( 1, 'Z' )" );
        totalTests++; testsPassed += Test_DoTheyMatch( 'A',          arr.m_array[0],  testName, "arr.m_array[0]" );
        totalTests++; testsPassed += Test_DoTheyMatch( 'Z',          arr.m_array[1],  testName, "arr.m_array[1]" );
        totalTests++; testsPassed += Test_DoTheyMatch( 'B',          arr.m_array[2],  testName, "arr.m_array[2]" );
        totalTests++; testsPassed += Test_DoTheyMatch( 'C',          arr.m_array[3],  testName, "arr.m_array[3]" );
        totalTests++; testsPassed += Test_DoTheyMatch( 4,            arr.m_itemCount, testName, "arr.m_itemCounts]" );

        Test_End();
    }

    {
        string testName = Test_Begin( "Negative index marked invalid?" );
        DynamicArray<char> arr; arr.m_itemCount = 5;
        string exceptionThrown = "";
        string expectedException = "Exception::InvalidIndexException";
        try                                                    { arr.PushAt( -5, 'A' ); }
        catch ( const Exception::InvalidIndexException& ex )   { exceptionThrown = "Exception::InvalidIndexException"; }
        catch( ... )                                           { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "arr.PushAt( -5, 'A' )" );
        Test_End();
    }

    {
        string testName = Test_Begin( "Out-of-bounds index marked invalid?" );
        DynamicArray<char> arr; arr.m_itemCount = 5;
        string exceptionThrown = "";
        string expectedException = "Exception::InvalidIndexException";
        try                                                    { arr.PushAt( 6, 'A' ); }
        catch ( const Exception::InvalidIndexException& ex )   { exceptionThrown = "Exception::InvalidIndexException"; }
        catch( ... )                                           { exceptionThrown = "Unknown exception"; }
        totalTests++; testsPassed += Test_DoTheyMatch( expectedException, exceptionThrown, testName, "arr.PushAt( 6, 'A' )" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void DynamicArrayTester::Test_DynamicArray_Search()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "DynamicArray::Search";
    bool prereqImplemented = true;
    {
        string testName = "Check " + functionName + " is implemented"; totalTests++;
        DynamicArray<char> obj;
        string exceptionMessage = "";
        try                                                    { obj.Search( 'A' ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Search for item in array";
        DynamicArray<char> arr;
        arr.m_array = new char[10]; arr.m_arraySize = 10;
        arr.m_itemCount = 3;
        arr.m_array[0] = 'C'; arr.m_array[1] = 'A'; arr.m_array[2] = 'T';

        string exceptionThrown = "";
        int result = -1;
        try             { result = arr.Search( 'A' ); }
        catch ( ... )   { exceptionThrown = "Unknown exception"; }

        totalTests++; testsPassed += Test_DoTheyMatch( 1, result, testName, "arr.Search( 'A' )" );

        Test_End();
    }

    {
        string testName = "Search for item in NOT array";
        DynamicArray<char> arr;
        arr.m_array = new char[10]; arr.m_arraySize = 10;
        arr.m_itemCount = 3;
        arr.m_array[0] = 'C'; arr.m_array[1] = 'A'; arr.m_array[2] = 'T';

        string exceptionThrown = "";
        int result = -1;
        try                                                     { result = arr.Search( 'Z' ); }
        catch ( const Exception::ItemNotFoundException& ex )    { exceptionThrown = "ItemNotFoundException"; }
        catch ( ... )                                           { exceptionThrown = "Unknown exception"; }

        totalTests++; testsPassed += Test_DoTheyMatch( string( "ItemNotFoundException" ), exceptionThrown, testName, "arr.Search( 'Z' )" );

        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void DynamicArrayTester::Test_DynamicArray_AllocateMemory()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "DynamicArray::AllocateMemory";
    bool prereqImplemented = true;
    {
        string testName = "Check " + functionName + " is implemented"; totalTests++;
        DynamicArray<char> obj;
        string exceptionMessage = "";
        try                                                    { obj.AllocateMemory( 5 ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Check member variables after AllocateMemory";
        DynamicArray<char> arr; arr.AllocateMemory( 10 );

        string expOut = "not nullptr";
        string actOut = ( arr.m_array == nullptr ) ? "nullptr" : "not nullptr";
        totalTests++; testsPassed += Test_DoTheyMatch( expOut, actOut, testName, "arr.m_array" );
        totalTests++; testsPassed += Test_DoTheyMatch( 0,      arr.m_itemCount,  testName, "arr.m_itemCount" );
        totalTests++; testsPassed += Test_DoTheyMatch( 10,     arr.m_arraySize,  testName, "arr.m_arraySize" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}

void DynamicArrayTester::Test_DynamicArray_Resize()
{
    DisplayHeader( __func__, 1 );
    int testsPassed = 0, totalTests = 0;
    string functionName = "DynamicArray::Resize";
    bool prereqImplemented = true;
    {
        string testName = "Check " + functionName + " is implemented"; totalTests++;
        DynamicArray<char> obj;
        string exceptionMessage = "";
        try                                                    { obj.Resize( 5 ); }
        catch ( const Exception::NotImplementedException& ex ) { prereqImplemented = false; Test_FailDisplayExpectedActual( testName, "NotImplementedException thrown?", false, true ); }
        catch( ... )                                           { /* Ignore other exception types */ }
        if ( prereqImplemented )                               { testsPassed++; Test_PassDisplay( testName, "NotImplementedException NOT THROWN" );}
        Test_End();
    } if ( prereqImplemented == false ) { cout << YELLOW << "PREREQ NOT IMPLEMENTED; LEAVING TESTS EARLY." << CLEAR << endl; return; }

    {
        string testName = "Check new array info after Resize";
        DynamicArray<string> arr; arr.m_array = new string[3]; arr.m_itemCount = 3;
        arr.m_array[0] = "a"; arr.m_array[1] = "b"; arr.m_array[2] = "c";

        string* originalAddress = arr.m_array;
        arr.Resize( 6 );

        string actOut = ( arr.m_array == nullptr ) ? "nullptr" : "not nullptr";
        totalTests++; testsPassed += Test_DoTheyMatch( string( "not nullptr" ),          actOut, testName, "arr.m_array" );

        actOut = ( arr.m_array == originalAddress ) ? "original address" : "not original address";
        totalTests++; testsPassed += Test_DoTheyMatch( string( "not original address" ), actOut, testName, "arr.m_array" );

        totalTests++; testsPassed += Test_DoTheyMatch( 3,             arr.m_itemCount,  testName, "arr.m_itemCount" );
        totalTests++; testsPassed += Test_DoTheyMatch( 6,             arr.m_arraySize,  testName, "arr.m_arraySize" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "a" ), arr.m_array[0],   testName, "arr.m_array[0]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "b" ), arr.m_array[1],   testName, "arr.m_array[1]" );
        totalTests++; testsPassed += Test_DoTheyMatch( string( "c" ), arr.m_array[2],   testName, "arr.m_array[2]" );
        Test_End();
    }

    cout << CLEAR << endl << testsPassed << " tests passed out of " << totalTests << endl;
}



