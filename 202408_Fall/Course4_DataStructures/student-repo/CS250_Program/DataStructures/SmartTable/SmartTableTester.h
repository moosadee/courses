#ifndef _SMART_TABLE_TESTER
#define _SMART_TABLE_TESTER

class SmartTableTester
{
public:
    void RunAll();
    void Test_SmartTable_ShiftLeft();
    void Test_SmartTable_ShiftRight();
    void Test_SmartTable_RemoveBack();
    void Test_SmartTable_RemoveFront();
    void Test_SmartTable_RemoveAt();
    void Test_SmartTable_GetBack();
    void Test_SmartTable_GetFront();
    void Test_SmartTable_GetAt();
    void Test_SmartTable_AllocateMemory();
    void Test_SmartTable_Resize();
    void Test_SmartTable_IsFull();
    void Test_SmartTable_PushBack();
    void Test_SmartTable_PushFront();
    void Test_SmartTable_PushAt();
    void Test_SmartTable_Search();
};

#endif
