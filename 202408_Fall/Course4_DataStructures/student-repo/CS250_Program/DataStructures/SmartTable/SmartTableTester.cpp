#include "SmartTableTester.h"
#include "../../Utilities/Style.h"

void SmartTableTester::RunAll()
{
    DisplayHeader( "SmartTableTester - RunAll", 0 );
}

void SmartTableTester::Test_SmartTable_ShiftLeft()
{
}

void SmartTableTester::Test_SmartTable_ShiftRight()
{
}

void SmartTableTester::Test_SmartTable_RemoveBack()
{
}

void SmartTableTester::Test_SmartTable_RemoveFront()
{
}

void SmartTableTester::Test_SmartTable_RemoveAt()
{
}

void SmartTableTester::Test_SmartTable_GetBack()
{
}

void SmartTableTester::Test_SmartTable_GetFront()
{
}

void SmartTableTester::Test_SmartTable_GetAt()
{
}

void SmartTableTester::Test_SmartTable_AllocateMemory()
{
}

void SmartTableTester::Test_SmartTable_Resize()
{
}

void SmartTableTester::Test_SmartTable_IsFull()
{
}

void SmartTableTester::Test_SmartTable_PushBack()
{
}

void SmartTableTester::Test_SmartTable_PushFront()
{
}

void SmartTableTester::Test_SmartTable_PushAt()
{
}

void SmartTableTester::Test_SmartTable_Search()
{
}







