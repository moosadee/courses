#ifndef _STRINGS
#define _STRINGS

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

bool Contains( std::string haystack, std::string needle, bool caseSensitive /*= true*/ );
std::string PartialString( const std::string& original, int maxLength );

template <typename T>
std::string ToString( const T& value )
{
  std::stringstream ss;
  ss << value;
  return ss.str();
}

#endif
