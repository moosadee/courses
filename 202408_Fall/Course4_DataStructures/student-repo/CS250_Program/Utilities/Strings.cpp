#include "Strings.h"

bool Contains( std::string haystack, std::string needle, bool caseSensitive /*= true*/ )
{
    std::string a = haystack;
    std::string b = needle;

    if ( !caseSensitive )
    {
        for ( unsigned int i = 0; i < a.size(); i++ )
        {
            a[i] = tolower( a[i] );
        }

        for ( unsigned int i = 0; i < b.size(); i++ )
        {
            b[i] = tolower( b[i] );
        }
    }

    return ( a.find( b ) != std::string::npos );
}

std::string PartialString( const std::string& original, int maxLength )
{
    std::string adjust = original;
    if ( original.size() > maxLength )
    {
        adjust = original.substr( 0, maxLength - 3 );
        adjust += "...";
    }
    return adjust;
}
