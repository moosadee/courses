#ifndef _STUDENT_PROGRAM
#define _STUDENT_PROGRAM

#include <string>
#include <vector>
using namespace std;

// TODO: Put your struct here!
struct Record
{
    string STRINGFIELD;  // TODO: Remove this placeholder!
    int INTFIELD;          // TODO: Remove this placeholder!
};

class StudentProgram
{
    public:
    StudentProgram();
    ~StudentProgram();

    vector<Record> LoadRecords( string filename );

    void ViewAll();
    void Report1();
    void Report2();
    void Search();
    void Sort();

    void Run();

    private:
    vector<Record> records;
};

#endif
