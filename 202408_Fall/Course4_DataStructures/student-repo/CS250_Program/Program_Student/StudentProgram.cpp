#include "StudentProgram.h"

#include "../Utilities/Style.h"
#include "../Utilities/Strings.h"
#include "../Utilities/CsvParser.h"

#include <iostream>
#include <iomanip>
#include <map>
using namespace std;

StudentProgram::StudentProgram()
{
    //records = LoadRecords( "../Data/KansasCityMissouriParksandBoulevardsMap.csv" );
}

StudentProgram::~StudentProgram()
{
}

vector<Record> StudentProgram::LoadRecords( string filename )
{
    // TODO: Add your loading functionality here!
    vector<Record> data;
    return data;
}

void StudentProgram::ViewAll()
{
    DisplayHeader( "ALL RECORDS", 2 );
    cout << left << fixed << setprecision( 2 );
    cout << "  "
        << setw( 5 ) << "IDX"
        << setw( 20 ) << "INTFIELD"
        << setw( 20 ) << "STRINGFIELD"
        << endl << "  " << string( 75, '-' ) << endl;
    for ( size_t i = 0; i < records.size(); i++ )
    {
        cout << "  "
            << setw( 5 ) << i
            << setw( 20 ) << records[i].INTFIELD
            << setw( 20 ) << PartialString( records[i].STRINGFIELD, 18 )
            << endl;
    }
}

void StudentProgram::Report1()
{
    DisplayHeader( "REPORT 1", 2 );
    // TODO: Add your report 1 here
}

void StudentProgram::Report2()
{
    DisplayHeader( "REPORT 2", 2 );
    // TODO: Add your report 2 here
}

void StudentProgram::Search()
{
    DisplayHeader( "SEARCH", 2 );
    cout << "  Search by what field?" << endl;
    string choice = GetChoice( { "STRINGFIELD", "INTFIELD" }, 2 ); // TODO: Add fields you want searchable

    string search_term;
    cout << "  Enter partial or full search term: ";
    cin.ignore();
    getline( cin, search_term );

    vector<Record> matches;

    for ( auto& rec : records )
    {
        if ( choice == "STRINGFIELD" && Contains( rec.STRINGFIELD, search_term, false ) )
        {
            matches.push_back( rec );
        }
        else if ( choice == "INTFIELD" && Contains( ToString( rec.INTFIELD ), search_term, false ) )
        {
            matches.push_back( rec );
        }
    }

    DisplayHeader( "RESULTS", 3 );
    for ( size_t i = 0; i < matches.size(); i++ )
    {
        cout << "   Match #" << i+1 << ":" << endl;
        cout << "   * STRINGFIELD:  " << matches[i].STRINGFIELD << endl;
        cout << "   * INTFIELD:     " << matches[i].INTFIELD << endl;
        cout << endl;
    }
}

void StudentProgram::Sort()
{
    DisplayHeader( "SORT", 0 );
    cout << "  Sort on what field?" << endl;
    string choice = GetChoice( { "STRINGFIELD", "INTFIELD" }, 2 ); // TODO: Add fields you want sortable

    // BubbleSort
    cout << "  Now sorting..." << endl;
    for ( size_t i = 0; i < records.size() - 1; i++ )
    {
        for ( size_t j = 0; j < records.size() - i - 1; j++ )
        {
            if ( choice == "STRINGFIELD" && records[j].STRINGFIELD > records[j+1].STRINGFIELD )
            {
                swap( records[j], records[j+1] );
            }
            else if ( choice == "INTFIELD" && records[j].INTFIELD > records[j+1].INTFIELD )
            {
                swap( records[j], records[j+1] );
            }
        }
    }

    cout << "  Sorting complete." << endl;
}

void StudentProgram::Run()
{
    bool menu_running = true;
    while ( menu_running )
    {
        DisplayHeader( "PROJECT", 1 );
        string choice = GetChoice( { "QUIT", "VIEW ALL", "REPORT 1", "REPORT 2", "SEARCH", "SORT" }, 1 );
        if ( choice == "QUIT" )
        {
            menu_running = false;
        }
        else if ( choice == "VIEW ALL" )
        {
            ViewAll();
        }
        else if ( choice == "REPORT 1" )
        {
            Report1();
        }
        else if ( choice == "REPORT 2" )
        {
            Report2();
        }
        else if ( choice == "SEARCH" )
        {
            Search();
        }
        else if ( choice == "SORT" )
        {
            Sort();
        }
    }
}

