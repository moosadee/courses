#ifndef _PROGRAM_BASE
#define _PROGRAM_BASE

class Program
{
public:
    Program();
    ~Program();

    void Run();

    void Setup();
    void Teardown();

    void SaveData();
    void LoadData();

    void Menu_Main();
    void Menu_Tests();

    void RunAllTests();

private:
};

#endif
