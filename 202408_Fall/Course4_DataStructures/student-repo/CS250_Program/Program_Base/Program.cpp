#include "Program.h"
#include "../Utilities/Style.h"
#include "../Program_Student/StudentProgram.h"

#include "../Algorithms/Search/SearchTester.h"
#include "../Algorithms/Sort/SortTester.h"
#include "../DataStructures/ArrayQueue/ArrayQueueTester.h"
#include "../DataStructures/ArrayStack/ArrayStackTester.h"
#include "../DataStructures/BalancedSearchTree/BalancedSearchTreeTester.h"
#include "../DataStructures/BinarySearchTree/BinarySearchTreeTester.h"
#include "../DataStructures/DynamicArray/DynamicArrayTester.h"
#include "../DataStructures/FixedArray/FixedArrayTester.h"
#include "../DataStructures/HashTable/HashTableTester.h"
#include "../DataStructures/LinkedList/LinkedListTester.h"
#include "../DataStructures/LinkedQueue/LinkedQueueTester.h"
#include "../DataStructures/LinkedStack/LinkedStackTester.h"
#include "../DataStructures/PriorityQueue/PriorityQueueTester.h"
#include "../DataStructures/SmartTable/SmartTableTester.h"

Program::Program()
{
    Setup();
}

Program::~Program()
{
    Teardown();
}

void Program::Setup()
{
    LoadData();
}

void Program::Teardown()
{
    SaveData();
}

void Program::LoadData()
{
}

void Program::SaveData()
{
}

void Program::Run()
{
    Menu_Main();
}

void Program::Menu_Main()
{
    bool menu_running = true;
    while ( menu_running )
    {
        DisplayHeader( "MAIN MENU", 0 );
        string choice = GetChoice( { "QUIT", "TESTS", "PROJECT", "C" }, 0 );

        if ( choice == "QUIT" )
        {
            menu_running = false;
        }
        else if ( choice == "TESTS" )
        {
            Menu_Tests();
        }
        else if ( choice == "PROJECT" )
        {
            StudentProgram program;
            program.Run();
        }
    }
}

void Program::Menu_Tests()
{
    bool menu_running = true;
    while ( menu_running )
    {
        DisplayHeader( "TESTS", 1 );
        string choice = GetChoice( { "BACK", "RUN ALL", "Data Structures", "Searching", "Sorting", "BONUS" }, 1 );
        if ( choice == "BACK" )
        {
            return;
        }

        if ( choice == "RUN ALL" )
        {
            RunAllTests();
        }
        else if ( choice == "Data Structures" )
        {
            DisplayHeader( "TESTS - Data Structures", 2 );
            choice = GetChoice( { "BACK",
                                  "FixedArray",
                                  "DynamicArray",
                                  "HashTable",
                                  "LinkedList",
                                  "LinkedStack",
                                  "ArrayStack",
                                  "LinkedQueue",
                                  "ArrayQueue",
                                  "BinarySearchTree",
                                }, 2 );
            if ( choice == "BACK" ) { continue; }
            else if ( choice == "FixedArray" )      { FixedArrayTester         faTest;    faTest.RunAll(); }
            else if ( choice == "DynamicArray")     { DynamicArrayTester       daTest;    daTest.RunAll(); }
            else if ( choice == "HashTable")        { HashTableTester          htTest;    htTest.RunAll(); }
            else if ( choice == "LinkedList")       { LinkedListTester         llTest;    llTest.RunAll(); }
            else if ( choice == "LinkedQueue")      { LinkedQueueTester        lqTest;    lqTest.RunAll(); }
            else if ( choice == "ArrayQueue")       { ArrayQueueTester         aqTest;    aqTest.RunAll(); }
            else if ( choice == "LinkedStack")      { LinkedStackTester        lsTest;    lsTest.RunAll(); }
            else if ( choice == "ArrayStack")       { ArrayStackTester         asTest;    asTest.RunAll(); }
            else if ( choice == "BinarySearchTree") { BinarySearchTreeTester   bstTest;   bstTest.RunAll(); }
        }
        else if ( choice == "Searching" )
        {
            DisplayHeader( "TESTS - Searching", 2 );
            SearchTester seTest;
            seTest.RunAll();
        }
        else if ( choice == "Sorting" )
        {
            DisplayHeader( "TESTS - Sorting", 2 );
            SortTester soTest;
            soTest.RunAll();
        }
        else if ( choice == "BONUS" )
        {
            DisplayHeader( "TESTS - BONUS", 2 );
            choice = GetChoice( { "BACK",
                                  "BalancedSearchTree",
                                  "PriorityQueue"
                                }, 2 );
            if ( choice == "BACK" ) { continue; }
            else if ( choice == "BalancedSearchTree" )  { BalancedSearchTreeTester bltTest;   bltTest.RunAll(); }
            else if ( choice == "PriorityQueue" )       { PriorityQueueTester      pqTest;    pqTest.RunAll();  }
        }
    }
}

void Program::RunAllTests()
{
    // Searching algorithms
    SearchTester             seTest;    seTest.RunAll();
    // Sorting algorithms
    SortTester               soTest;    soTest.RunAll();
    // Main data structures
    FixedArrayTester         faTest;    faTest.RunAll();
    DynamicArrayTester       daTest;    daTest.RunAll();
    HashTableTester          htTest;    htTest.RunAll();
    LinkedListTester         llTest;    llTest.RunAll();
    LinkedQueueTester        lqTest;    lqTest.RunAll();
    ArrayQueueTester         aqTest;    aqTest.RunAll();
    LinkedStackTester        lsTest;    lsTest.RunAll();
    ArrayStackTester         asTest;    asTest.RunAll();
    BinarySearchTreeTester   bstTest;   bstTest.RunAll();
    SmartTableTester         stTest;    stTest.RunAll();
    // Bonus data structures
    BalancedSearchTreeTester bltTest;   bltTest.RunAll();
    PriorityQueueTester      pqTest;    pqTest.RunAll();
}
