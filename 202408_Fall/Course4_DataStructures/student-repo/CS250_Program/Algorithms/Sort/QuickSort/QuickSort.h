#ifndef _QUICKSORT
#define _QUICKSORT

template <typename T>
size_t PartitionEm( vector<T>& arr, int low, int high )
{
    int pivot_value = arr[high];
    int i = low - 1;
    // TODO: Implement me!
    return i+1;
}

template <typename T>
void QuickSort_Recursive( vector<T>& arr, int low, int high )
{
    // TODO: Implement me!
}

/**
@param      arr         The array to sort
*/
template <typename T>
void QuickSort( vector<T>& arr )
{
    QuickSort_Recursive( arr, 0, arr.size() - 1 );
}

#endif
