#ifndef _RADIXSORT
#define _RADIXSORT

#include <algorithm>
using namespace std;

// Adapted from https://www.geeksforgeeks.org/radix-sort/

void CountSort( vector<int>& arr, int n, int exp )
{
    // TODO: Implement me!
}

int GetMax( vector<int>& arr )
{
    int max_value = arr[0];
    for ( size_t i = 1; i < arr.size(); i++ )
    {
        if ( arr[i] > max_value )
        {
            max_value = arr[i];
        }
    }
    return max_value;
}

/**
@param      arr         The array to sort
*/
void RadixSort( vector<int>& arr )
{
    // TODO: Implement me!
}

#endif
