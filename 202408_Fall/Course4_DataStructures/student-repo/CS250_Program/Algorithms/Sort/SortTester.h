#ifndef _SORT_TESTER
#define _SORT_TESTER

class SortTester
{
public:
    void RunAll();
    void Test_BubbleSort();
    void Test_HeapSort();
    void Test_InsertionSort();
    void Test_MergeSort();
    void Test_QuickSort();
    void Test_RadixSort();
    void Test_SelectionSort();
};

#endif
