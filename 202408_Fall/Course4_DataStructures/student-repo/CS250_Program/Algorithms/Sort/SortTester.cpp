#include "SortTester.h"
#include "BubbleSort/BubbleSort.h"
#include "HeapSort/HeapSort.h"
#include "InsertionSort/InsertionSort.h"
#include "MergeSort/MergeSort.h"
#include "QuickSort/QuickSort.h"
#include "RadixSort/RadixSort.h"
#include "SelectionSort/SelectionSort.h"
#include "../../Utilities/Style.h"

void SortTester::RunAll()
{
    DisplayHeader( "SortTester - RunAll", 0 );
    Test_BubbleSort();
    Test_InsertionSort();
    Test_SelectionSort();
    Test_MergeSort();
    Test_QuickSort();
    Test_HeapSort();
    Test_RadixSort();
}

void SortTester::Test_BubbleSort()
{
    DisplayHeader( __func__, 1 );
    string testName = "";

    { testName = "Check if sort for { 'z', 'o', 'r', 'd', 's' } was correct";
        vector<char> arr = { 'z', 'o', 'r', 'd', 's' };
        vector<char> expectOut = { 'd', 'o', 'r', 's', 'z' };
        vector<char> actualOut = arr;
        BubbleSort( actualOut );

        bool allMatch = true;
        if ( actualOut.size() != expectOut.size() )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* ORIGINAL arr.size(): " << arr.size() << endl;
            cout << "* EXPECTED arr.size(): " << expectOut.size() << endl;
            cout << "* ACTUAL   arr.size(): " << actualOut.size() << endl;
            cout << endl;
        }

        for ( size_t i = 0; i < actualOut.size(); i++ )
        {
            if ( actualOut[i] != expectOut[i] )
            {
                allMatch = false;
            }
        }

        if ( allMatch == false )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* ORIGINAL arr: ";
            DisplayVector( arr, true );
            cout << endl;
            cout << "* EXPECTED arr: ";
            DisplayVector( expectOut, true );
            cout << endl;
            cout << "* ACTUAL   arr: ";
            DisplayVector( actualOut, true );
            cout << endl;
            cout << endl;
        }
        else  { cout << GREEN << "[PASS] " << testName << endl; }
        cout << CLEAR;
    }

    { testName = "Check if sort for { 3, 2, 1 } was correct";
        vector<int> arr = { 3, 2, 1 };
        vector<int> expectOut = { 1, 2, 3 };
        vector<int> actualOut = arr;
        BubbleSort( actualOut );

        bool allMatch = true;
        if ( actualOut.size() != expectOut.size() )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* ORIGINAL arr.size(): " << arr.size() << endl;
            cout << "* EXPECTED arr.size(): " << expectOut.size() << endl;
            cout << "* ACTUAL   arr.size(): " << actualOut.size() << endl;
            cout << endl;
        }

        for ( size_t i = 0; i < actualOut.size(); i++ )
        {
            if ( actualOut[i] != expectOut[i] )
            {
                allMatch = false;
            }
        }

        if ( allMatch == false )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* ORIGINAL arr: ";
            DisplayVector( arr, true );
            cout << endl;
            cout << "* EXPECTED arr: ";
            DisplayVector( expectOut, true );
            cout << endl;
            cout << "* ACTUAL   arr: ";
            DisplayVector( actualOut, true );
            cout << endl;
            cout << endl;
        }
        else  { cout << GREEN << "[PASS] " << testName << endl; }
        cout << CLEAR;
    }
}

void SortTester::Test_InsertionSort()
{
    DisplayHeader( __func__, 1 );
    string testName = "";

    { testName = "Check if sort for { 'z', 'o', 'r', 'd', 's' } was correct";
        vector<char> arr = { 'z', 'o', 'r', 'd', 's' };
        vector<char> expectOut = { 'd', 'o', 'r', 's', 'z' };
        vector<char> actualOut = arr;
        InsertionSort( actualOut );

        bool allMatch = true;
        if ( actualOut.size() != expectOut.size() )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* EXPECTED arr.size(): " << expectOut.size() << endl;
            cout << "* ACTUAL   arr.size(): " << actualOut.size() << endl;
            cout << endl;
        }

        for ( size_t i = 0; i < actualOut.size(); i++ )
        {
            if ( actualOut[i] != expectOut[i] )
            {
                allMatch = false;
            }
        }

        if ( allMatch == false )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* EXPECTED arr: ";
            DisplayVector( expectOut, true );
            cout << endl;
            cout << "* ACTUAL   arr: ";
            DisplayVector( actualOut, true );
            cout << endl;
            cout << endl;
        }
        else  { cout << GREEN << "[PASS] " << testName << endl; }
        cout << CLEAR;
    }

    { testName = "Check if sort for { 3, 2, 1 } was correct";
        vector<int> arr = { 3, 2, 1 };
        vector<int> expectOut = { 1, 2, 3 };
        vector<int> actualOut = arr;
        InsertionSort( actualOut );

        bool allMatch = true;
        if ( actualOut.size() != expectOut.size() )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* EXPECTED arr.size(): " << expectOut.size() << endl;
            cout << "* ACTUAL   arr.size(): " << actualOut.size() << endl;
            cout << endl;
        }

        for ( size_t i = 0; i < actualOut.size(); i++ )
        {
            if ( actualOut[i] != expectOut[i] )
            {
                allMatch = false;
            }
        }

        if ( allMatch == false )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* EXPECTED arr: ";
            DisplayVector( expectOut, true );
            cout << endl;
            cout << "* ACTUAL   arr: ";
            DisplayVector( actualOut, true );
            cout << endl;
            cout << endl;
        }
        else  { cout << GREEN << "[PASS] " << testName << endl; }
        cout << CLEAR;
    }
}

void SortTester::Test_SelectionSort()
{
    DisplayHeader( __func__, 1 );
    string testName = "";

    { testName = "Check if sort for { 'z', 'o', 'r', 'd', 's' } was correct";
        vector<char> arr = { 'z', 'o', 'r', 'd', 's' };
        vector<char> expectOut = { 'd', 'o', 'r', 's', 'z' };
        vector<char> actualOut = arr;
        SelectionSort( actualOut );

        bool allMatch = true;
        if ( actualOut.size() != expectOut.size() )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* EXPECTED arr.size(): " << expectOut.size() << endl;
            cout << "* ACTUAL   arr.size(): " << actualOut.size() << endl;
            cout << endl;
        }

        for ( size_t i = 0; i < actualOut.size(); i++ )
        {
            if ( actualOut[i] != expectOut[i] )
            {
                allMatch = false;
            }
        }

        if ( allMatch == false )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* EXPECTED arr: ";
            DisplayVector( expectOut, true );
            cout << endl;
            cout << "* ACTUAL   arr: ";
            DisplayVector( actualOut, true );
            cout << endl;
            cout << endl;
        }
        else  { cout << GREEN << "[PASS] " << testName << endl; }
        cout << CLEAR;
    }

    { testName = "Check if sort for { 3, 2, 1 } was correct";
        vector<int> arr = { 3, 2, 1 };
        vector<int> expectOut = { 1, 2, 3 };
        vector<int> actualOut = arr;
        SelectionSort( actualOut );

        bool allMatch = true;
        if ( actualOut.size() != expectOut.size() )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* EXPECTED arr.size(): " << expectOut.size() << endl;
            cout << "* ACTUAL   arr.size(): " << actualOut.size() << endl;
            cout << endl;
        }

        for ( size_t i = 0; i < actualOut.size(); i++ )
        {
            if ( actualOut[i] != expectOut[i] )
            {
                allMatch = false;
            }
        }

        if ( allMatch == false )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* EXPECTED arr: ";
            DisplayVector( expectOut, true );
            cout << endl;
            cout << "* ACTUAL   arr: ";
            DisplayVector( actualOut, true );
            cout << endl;
            cout << endl;
        }
        else  { cout << GREEN << "[PASS] " << testName << endl; }
        cout << CLEAR;
    }
}

void SortTester::Test_MergeSort()
{
    DisplayHeader( __func__, 1 );
    string testName = "";

    { testName = "Check if sort for { 'z', 'o', 'r', 'd', 's' } was correct";
        vector<char> arr = { 'z', 'o', 'r', 'd', 's' };
        vector<char> expectOut = { 'd', 'o', 'r', 's', 'z' };
        vector<char> actualOut = arr;
        MergeSort( actualOut );

        bool allMatch = true;
        if ( actualOut.size() != expectOut.size() )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* EXPECTED arr.size(): " << expectOut.size() << endl;
            cout << "* ACTUAL   arr.size(): " << actualOut.size() << endl;
            cout << endl;
        }

        for ( size_t i = 0; i < actualOut.size(); i++ )
        {
            if ( actualOut[i] != expectOut[i] )
            {
                allMatch = false;
            }
        }

        if ( allMatch == false )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* EXPECTED arr: ";
            DisplayVector( expectOut, true );
            cout << endl;
            cout << "* ACTUAL   arr: ";
            DisplayVector( actualOut, true );
            cout << endl;
            cout << endl;
        }
        else  { cout << GREEN << "[PASS] " << testName << endl; }
        cout << CLEAR;
    }

    { testName = "Check if sort for { 3, 2, 1 } was correct";
        vector<int> arr = { 3, 2, 1 };
        vector<int> expectOut = { 1, 2, 3 };
        vector<int> actualOut = arr;
        MergeSort( actualOut );

        bool allMatch = true;
        if ( actualOut.size() != expectOut.size() )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* EXPECTED arr.size(): " << expectOut.size() << endl;
            cout << "* ACTUAL   arr.size(): " << actualOut.size() << endl;
            cout << endl;
        }

        for ( size_t i = 0; i < actualOut.size(); i++ )
        {
            if ( actualOut[i] != expectOut[i] )
            {
                allMatch = false;
            }
        }

        if ( allMatch == false )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* EXPECTED arr: ";
            DisplayVector( expectOut, true );
            cout << endl;
            cout << "* ACTUAL   arr: ";
            DisplayVector( actualOut, true );
            cout << endl;
            cout << endl;
        }
        else  { cout << GREEN << "[PASS] " << testName << endl; }
        cout << CLEAR;
    }
}

void SortTester::Test_HeapSort()
{
    DisplayHeader( __func__, 1 );
    string testName = "";

    { testName = "Check if sort for { 'z', 'o', 'r', 'd', 's' } was correct";
        vector<char> arr = { 'z', 'o', 'r', 'd', 's' };
        vector<char> expectOut = { 'd', 'o', 'r', 's', 'z' };
        vector<char> actualOut = arr;
        HeapSort( actualOut );

        bool allMatch = true;
        if ( actualOut.size() != expectOut.size() )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* EXPECTED arr.size(): " << expectOut.size() << endl;
            cout << "* ACTUAL   arr.size(): " << actualOut.size() << endl;
            cout << endl;
        }

        for ( size_t i = 0; i < actualOut.size(); i++ )
        {
            if ( actualOut[i] != expectOut[i] )
            {
                allMatch = false;
            }
        }

        if ( allMatch == false )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* EXPECTED arr: ";
            DisplayVector( expectOut, true );
            cout << endl;
            cout << "* ACTUAL   arr: ";
            DisplayVector( actualOut, true );
            cout << endl;
            cout << endl;
        }
        else  { cout << GREEN << "[PASS] " << testName << endl; }
        cout << CLEAR;
    }

    { testName = "Check if sort for { 3, 2, 1 } was correct";
        vector<int> arr = { 3, 2, 1 };
        vector<int> expectOut = { 1, 2, 3 };
        vector<int> actualOut = arr;
        HeapSort( actualOut );

        bool allMatch = true;
        if ( actualOut.size() != expectOut.size() )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* EXPECTED arr.size(): " << expectOut.size() << endl;
            cout << "* ACTUAL   arr.size(): " << actualOut.size() << endl;
            cout << endl;
        }

        for ( size_t i = 0; i < actualOut.size(); i++ )
        {
            if ( actualOut[i] != expectOut[i] )
            {
                allMatch = false;
            }
        }

        if ( allMatch == false )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* EXPECTED arr: ";
            DisplayVector( expectOut, true );
            cout << endl;
            cout << "* ACTUAL   arr: ";
            DisplayVector( actualOut, true );
            cout << endl;
            cout << endl;
        }
        else  { cout << GREEN << "[PASS] " << testName << endl; }
        cout << CLEAR;
    }
}

void SortTester::Test_QuickSort()
{
    DisplayHeader( __func__, 1 );
    string testName = "";

    { testName = "Check if sort for { 'z', 'o', 'r', 'd', 's' } was correct";
        vector<char> arr = { 'z', 'o', 'r', 'd', 's' };
        vector<char> expectOut = { 'd', 'o', 'r', 's', 'z' };
        vector<char> actualOut = arr;
        QuickSort( actualOut );

        bool allMatch = true;
        if ( actualOut.size() != expectOut.size() )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* EXPECTED arr.size(): " << expectOut.size() << endl;
            cout << "* ACTUAL   arr.size(): " << actualOut.size() << endl;
            cout << endl;
        }

        for ( size_t i = 0; i < actualOut.size(); i++ )
        {
            if ( actualOut[i] != expectOut[i] )
            {
                allMatch = false;
            }
        }

        if ( allMatch == false )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* EXPECTED arr: ";
            DisplayVector( expectOut, true );
            cout << endl;
            cout << "* ACTUAL   arr: ";
            DisplayVector( actualOut, true );
            cout << endl;
            cout << endl;
        }
        else  { cout << GREEN << "[PASS] " << testName << endl; }
        cout << CLEAR;
    }

    { testName = "Check if sort for { 3, 2, 1 } was correct";
        vector<int> arr = { 3, 2, 1 };
        vector<int> expectOut = { 1, 2, 3 };
        vector<int> actualOut = arr;
        QuickSort( actualOut );

        bool allMatch = true;
        if ( actualOut.size() != expectOut.size() )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* EXPECTED arr.size(): " << expectOut.size() << endl;
            cout << "* ACTUAL   arr.size(): " << actualOut.size() << endl;
            cout << endl;
        }

        for ( size_t i = 0; i < actualOut.size(); i++ )
        {
            if ( actualOut[i] != expectOut[i] )
            {
                allMatch = false;
            }
        }

        if ( allMatch == false )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* EXPECTED arr: ";
            DisplayVector( expectOut, true );
            cout << endl;
            cout << "* ACTUAL   arr: ";
            DisplayVector( actualOut, true );
            cout << endl;
            cout << endl;
        }
        else  { cout << GREEN << "[PASS] " << testName << endl; }
        cout << CLEAR;
    }
}

void SortTester::Test_RadixSort()
{
    DisplayHeader( __func__, 1 );
    string testName = "";

    { testName = "Check if sort for { 3, 2, 1 } was correct";
        vector<int> arr = { 3, 2, 1 };
        vector<int> expectOut = { 1, 2, 3 };
        vector<int> actualOut = arr;
        RadixSort( actualOut );

        bool allMatch = true;
        if ( actualOut.size() != expectOut.size() )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* EXPECTED arr.size(): " << expectOut.size() << endl;
            cout << "* ACTUAL   arr.size(): " << actualOut.size() << endl;
            cout << endl;
        }

        for ( size_t i = 0; i < actualOut.size(); i++ )
        {
            if ( actualOut[i] != expectOut[i] )
            {
                allMatch = false;
            }
        }

        if ( allMatch == false )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* EXPECTED arr: ";
            DisplayVector( expectOut, true );
            cout << endl;
            cout << "* ACTUAL   arr: ";
            DisplayVector( actualOut, true );
            cout << endl;
            cout << endl;
        }
        else  { cout << GREEN << "[PASS] " << testName << endl; }
        cout << CLEAR;
    }

    { testName = "Check if sort for { 456, 234, 17, 3443, 12323, 112, 1123, 634 } was correct";
        vector<int> arr         = { 456, 234, 17, 3443, 12323, 112, 1123, 634 };
        vector<int> expectOut   = { 17, 112, 234, 456, 634, 1123, 3443, 12323 };
        vector<int> actualOut = arr;
        RadixSort( actualOut );

        bool allMatch = true;
        if ( actualOut.size() != expectOut.size() )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* EXPECTED arr.size(): " << expectOut.size() << endl;
            cout << "* ACTUAL   arr.size(): " << actualOut.size() << endl;
            cout << endl;
        }

        for ( size_t i = 0; i < actualOut.size(); i++ )
        {
            if ( actualOut[i] != expectOut[i] )
            {
                allMatch = false;
            }
        }

        if ( allMatch == false )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* EXPECTED arr: ";
            DisplayVector( expectOut, true );
            cout << endl;
            cout << "* ACTUAL   arr: ";
            DisplayVector( actualOut, true );
            cout << endl;
            cout << endl;
        }
        else  { cout << GREEN << "[PASS] " << testName << endl; }
        cout << CLEAR;
    }
}

