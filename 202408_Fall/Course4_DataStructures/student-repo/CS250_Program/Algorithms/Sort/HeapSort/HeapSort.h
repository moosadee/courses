#ifndef _HEAPSORT
#define _HEAPSORT

// https://en.wikipedia.org/wiki/Heapsort

int LeftChildIndex( int index )
{
    return 2 * index + 1;
}

int RightChildIndex( int index )
{
    return 2 * index + 2;
}

int ParentIndex( int index )
{
    return int( ( index - 1 ) / 2 );
}

template <typename T>
void SiftDown( vector<T>& arr, int root, int end )
{
    // TODO: Implement me!
}

template <typename T>
void Heapify( vector<T>& arr, size_t size )
{
    // TODO: Implement me!
}

/**
@param      arr         The array to sort
*/
template <typename T>
void HeapSort( vector<T>& arr )
{
    // TODO: Implement me!
}

#endif
