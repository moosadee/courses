#ifndef _LINEARSEARCH
#define _LINEARSEARCH

#include <string>
#include <vector>
using namespace std;

/**
@return     int         index of found item, or -1 if not found
@param      arr         vector of elements to search through
@param      find_me     item we're searching for in the arr
*/
template <typename T>
int LinearSearchSingle( const vector<T>& arr, T find_me )
{
    // TODO: Implement me!
    return -1;
}

/**
@return     vector<int>     vector of indices where the item is found
@param      arr             vector of elements to search through
@param      find_me         item we're searching for in the arr
*/
vector<int> LinearSearchAll( const vector<string>& arr, string find_me )
{
    vector<int> found_indices;
    // TODO: Implement me!
    return found_indices;
}

#endif
