#ifndef _BINARYSEARCH
#define _BINARYSEARCH

// https://en.wikipedia.org/wiki/Binary_search_algorithm

/**
@return     int         index of found item, or -1 if not found
@param      arr         vector of elements to search through
@param      find_me     item we're searching for in the arr
*/
template <typename T>
int BinarySearch( const vector<T>& arr, T find_me )
{
    // TODO: Implement me!

    return -1;
}

#endif
