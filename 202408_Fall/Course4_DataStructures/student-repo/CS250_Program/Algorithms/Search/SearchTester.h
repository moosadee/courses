#ifndef _SEARCH_TESTER
#define _SEARCH_TESTER

class SearchTester
{
public:
    void RunAll();
    void Test_LinearSearch();
    void Test_BinarySearch();
};

#endif
