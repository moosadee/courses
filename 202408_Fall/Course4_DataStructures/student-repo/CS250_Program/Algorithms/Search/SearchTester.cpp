#include "SearchTester.h"
#include "LinearSearch/LinearSearch.h"
#include "BinarySearch/BinarySearch.h"
#include "../../Utilities/Style.h"

#include <iostream>
using namespace std;

void SearchTester::RunAll()
{
    DisplayHeader( "SearchTester - RunAll", 0 );
    Test_LinearSearch();
    Test_BinarySearch();
}

void SearchTester::Test_LinearSearch()
{
    DisplayHeader( __func__, 1 );
    string testName = "";

    { testName = "LinearSearchSingle( { 10, 20, 30, 40 }, 40 ) should find item";
        vector<int> arr = { 10, 20, 30, 40 };
        int expectOut = 3;
        int actualOut = LinearSearchSingle( arr, 40 );

        if ( actualOut != expectOut )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* arr: ";
            DisplayVector( arr, true );
            cout << endl;
            cout << "* EXPECTED found index: " << expectOut << endl;
            cout << "* ACTUAL   found index: " << actualOut << endl;
            cout << endl;
        }
        else  { cout << GREEN << "[PASS] " << testName << endl; }
        cout << CLEAR;
    }

    { testName = "LinearSearchSingle( { 10, 20, 30, 40 }, 100 ) shouldn't find item";
        vector<int> arr = { 10, 20, 30, 40 };
        int expectOut = -1;
        int actualOut = LinearSearchSingle( arr, 100 );

        if ( actualOut != expectOut )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* arr: ";
            DisplayVector( arr, true );
            cout << endl;
            cout << "* EXPECTED found index: " << expectOut << endl;
            cout << "* ACTUAL   found index: " << actualOut << endl;
            cout << endl;
        }
        else  { cout << GREEN << "[PASS] " << testName << endl; }
        cout << CLEAR;
    }

    { testName = "LinearSearchAll( { \"cat\", \"cathode\", \"tv\", \"cat5e\" }, \"cat\" )";
        vector<string> arr = { "cat", "cathode", "tv", "cat5e" };
        vector<int> expectOut = { 0, 1, 3 };
        vector<int> actualOut = LinearSearchAll( arr, "cat" );

        if ( actualOut != expectOut )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* arr: ";
            DisplayVector( arr, true );
            cout << endl;
            cout << "* EXPECTED indices: ";
            DisplayVector( expectOut, false );
            cout << endl;
            cout << "* ACTUAL   indices: ";
            DisplayVector( actualOut, false );
            cout << endl;
            cout << endl;
        }
        else  { cout << GREEN << "[PASS] " << testName << endl; }
        cout << CLEAR;
    }

    { testName = "LinearSearchAll( { \"cat\", \"cathode\", \"tv\", \"cat5e\" }, \"dog\" )";
        vector<string> arr = { "cat", "cathode", "tv", "cat5e" };
        vector<int> expectOut = {};
        vector<int> actualOut = LinearSearchAll( arr, "dog" );

        if ( actualOut != expectOut )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* arr: ";
            DisplayVector( arr, true );
            cout << endl;
            cout << "* EXPECTED indices: ";
            DisplayVector( expectOut, false );
            cout << endl;
            cout << "* ACTUAL   indices: ";
            DisplayVector( actualOut, false );
            cout << endl;
            cout << endl;
        }
        else  { cout << GREEN << "[PASS] " << testName << endl; }
        cout << CLEAR;
    }
}

void SearchTester::Test_BinarySearch()
{
    DisplayHeader( __func__, 1 );
    string testName = "";

    { testName = "BinarySearch( { 10, 20, 30, 40 }, 40 ) should find item";
        vector<int> arr = { 10, 20, 30, 40 };
        int expectOut = 3;
        int actualOut = BinarySearch( arr, 40 );

        if ( actualOut != expectOut )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* arr: ";
            DisplayVector( arr, true );
            cout << endl;
            cout << "* EXPECTED found index: " << expectOut << endl;
            cout << "* ACTUAL   found index: " << actualOut << endl;
            cout << endl;
        }
        else  { cout << GREEN << "[PASS] " << testName << endl; }
        cout << CLEAR;
    }

    { testName = "BinarySearch( { 10, 20, 30, 40 }, 100 ) shouldn't find item";
        vector<int> arr = { 10, 20, 30, 40 };
        int expectOut = -1;
        int actualOut = BinarySearch( arr, 100 );

        if ( actualOut != expectOut )
        {
            cout << RED << "[FAIL] " << testName << endl;
            cout << "* arr: ";
            DisplayVector( arr, true );
            cout << endl;
            cout << "* EXPECTED found index: " << expectOut << endl;
            cout << "* ACTUAL   found index: " << actualOut << endl;
            cout << endl;
        }
        else  { cout << GREEN << "[PASS] " << testName << endl; }
        cout << CLEAR;
    }
}
