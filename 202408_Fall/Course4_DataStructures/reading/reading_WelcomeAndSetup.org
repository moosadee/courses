# -*- mode: org -*-

* *Welcome!*

It feels weird to start a collection of notes (or a "textbook")
without some sort of welcome, though at the same time I know that
people are probably /not/ going to read the introduction
(Unless I put some cute art and interesting footnotes, maybe.)

I think that I will welcome you to my notes by addressing anxiety.

*Belonging*

Unfortunately there is a lot of bias in STEM fields and over
decades there has been a narrative that computer science is /for/
a certain type of person - antisocial, nerdy, people who started coding
when they were 10 years old.

Because of this, a lot of people who don't fit this description can
be hesitant to get into computers or programming because they don't
see people like themselves in media portrayals. Or perhaps previous
professors or peers have acted like you're not a /real programmer/
if you didn't start programming as a child

#+ATTR_HTML: :class hint
#+NAME: content
#+BEGIN_HTML
If you want to learn about coding, then you belong here.

There are no prerequisites.
#+END_HTML

I will say from my own experience, I know developers who fell in
love with programming by accident as an adult after having to take
a computer class for a /different degree/. I know developers who
are into all sorts of sports, or into photography, or into fashion.
There is no specific "type" of programmer. You can be any religion,
any gender, any color, from any country, and be a programmer.

#+CAPTION: Don't be like Gatekeeper Baby. You can begin coding at any age!
[[file:images/u00_WelcomeAndSetup_gatekeepingbaby.png]]

*Challenge*

Programming can be hard sometimes. There are many aspects of learning
to write software (or websites, apps, games, etc.) and you *will*
get better at it with practice and with time. But I completely understand
the feeling of hitting your head against a wall wondering /why won't this work?!/
and even wondering /am I cut out for this?!/ - Yes, you are.

[[file:images/u00_WelcomeAndSetup_justwork.png]]

I will tell you right now, I /have/ cried over programming assignments,
over work, over software. I have taken my laptop with me to a family
holiday celebration because I /couldn't figure out this program/
and I /had to/ get it done!!

All developers struggle. Software is a really unique field. It's very
intangible, and there are lots of programming languages, and all sorts
of tools, and various techniques. Nobody knows everything, and there's
always more to learn.


Just because something is /hard/ doesn't mean that it is /impossible/.


It's completely natural to hit roadblocks. To have to step away from
your program and come back to it later with a clear head. It's natural
to be confused. It's natural to /not know/.

But some skills you will learn to make this process smoother are
how to plan out your programs, test and verify your programs,
how to phrase what you don't know as a question, how to ask for help.
These are all skills that you will build up over time.
Even if it feels like you're not making progress, I promise that you are,
and hopefully at the end of our class you can look back to the start of it
and realize how much you've learned and grown.


*Learning*

First and foremost, I am here to help you *learn*.

My teaching style is influenced on all my experiences throughout
my learning career, my software engineer career, and my teaching career.

I have personally met teachers who have tried to /scare me away/
from computers, I've had teachers who really encouraged me,
I've had teachers who barely cared, I've had teachers who made
class really fun and engaging.

I've worked professionally in software and web development, and
independently making apps and video games. I know what it's like
to apply for jobs and work with teams of people and experience
a software's development throughout its lifecycle.

And as a teacher I'm always trying to improve my classes -
making the learning resources easily available and accessible,
making assignments help build up your knowledge of the topics,
trying to give feedback to help you design and write good programs.

As a teacher, I am not here to trick you with silly questions
or decide whether you're a /real programmer/ or not;
I am here to help guide you to learn about programming,
learn about design, learn about testing, and learn how to teach yourself.

#+ATTR_HTML: :width 400px
#+ATTR_LATEX: :width 200px
#+ATTR_ORG: :width 100px
[[file:images/u00_WelcomeAndSetup_buildingblocks.png]]


*Roadmap*

[[file:images/u00_WelcomeAndSetup_roadmap.png]]

When you're just starting out, it can be hard to know what all
you're going to be learning about. I have certainly read course
descriptions and just thought to myself "I have no idea what
any of that meant, but it's required for my degree, so I guess
I'm taking it!"

Here's kind of my mental map of how the courses I teach work:

- CS 200: Concepts of Programming with C++ :: You're learning the language. Think of it like actually
  learning a human language; I'm teaching you words and
  the grammar, and at first you're just parroting what I say,
  but with practice you'll be able to build your own sentences.

- CS 235 Object-Oriented Programming with C++ :: You're learning more about software development practices, design, testing, as well as more advanced object oriented concepts.

- CS 250 Basic Data Structures with C++ :: You're learning about data, how to store data, how to assess how efficient algorithms are. Data data data.

In addition to learning about the language itself, I also try to
sprinkle in other things I've learned from experience that I think
you should know as a software developer (or someone who codes for
whatever reason), like

- How do you validate that what you wrote /actually works?/ (Spoilers: How to write tests, both manual and automated.)
- What tools can you use to make your programming life easier? (And are used in the professional world?)
- How do you design a solution given just some requirements?
- How do you network in the tech field?
- How do you find jobs?
- What are some issues facing tech fields today?

Something to keep in mind is that, if you're studying *Computer Science*
as a degree (e.g., my Bachelor's degree is in Computer Science),
technically that field is about /"how do computers work?"/,
not about /"how do I write software good?"/
but I still find these topics important to go over.

[[file:images/u00_WelcomeAndSetup_search.png]]

That's all I can really think of to write here. If you have any questions, let me know. Maybe I'll add on here.


#+BEGIN_LATEX
\vspace{0.2cm}
#+END_LATEX

---------------------------------------------

* What is this weird webpage/"book"?
In the past I've had all my course content available on the web on separate webpages.
However, maintaining the HTML, CSS, and JS for this over time is cumbersome. Throughout 2023 I've been
adapting my course content to /emacs orgmode/ documents, which allows me to export the course
content to HTML and PDF files. I have a few goals with this:

1. All course information is in one singular place
2. At the end of the semester, you can download the entire course's "stuff" in a single PDF file
   for easy referencing later on
3. Hopefully throughout this semester I'll get everything "moved over" to orgmode,
   and in Summer/Fall 2024 I can have a physical textbook printed for the courses,
   which students can use and take notes in so as to have most of their course
   stuff in one place as well
4. It's important to me to make sure that you have access to the course content
   even once you're not my student anymore - this resource is available publicly
   online, whether you're in the course or not. You can always reference it later.

I know a lot of text can be intimidating at first, but hopefully it will be less
intimidating as the semester goes and we learn our way around this page.


#+BEGIN_LATEX
\vspace{0.2cm}
#+END_LATEX

---------------------------------------------

* What are "data structures"?

[[file:../../images/comics/hit-computer.png]]

A *data structure* is an object (a class, a struct - some
type of *structured* thing) that holds *data*.
In particular, the entire job of a data structure object is
to store data and provide an interface for *adding, removing,*
and *accessing* that data.

/*"Don't all the classes we write hold data? How is this different from other objects I've defined?"*/

In the past, you may have written classes to represent objects,
like perhaps a book. A book could have member variables like
its title, isbn, and year published, and some methods that
help us interface with a book...


#+ATTR_HTML: :class uml
| Book              |          |
|-------------------+----------|
| =- title=         | : string |
| =- isbn=          | : string |
| =- year=          | : int    |
|-------------------+----------|
| =+ Setup( ... )=  | : void   |
| =+ Update( ... )= | : void   |
| =+ Display()=     | : void   |


However - this is not a data structure. We /could/, however, use a data structure
to store a /list/ of books:

#+ATTR_HTML: :class uml
| BookArray                               |            |
|-----------------------------------------+------------|
| =- bookArray=                           | : =Book[]= |
| =- arraySize=                           | : int      |
| =- totalBooks=                          | : int      |
|-----------------------------------------+------------|
| =+ AddBook( newBook: Book )=            | : void     |
| =+ Update( index: int, newBook: Book )= | : void     |
| =+ RemoveBook( index: int )=            | : void     |
| =+ GetBook( index: int )=               | : Book&    |
| =+DisplayAll()=                         | : void     |
| =+GetSize()=                            | : int      |

A data structure will store a series of some sort of data.
Often, it will use an *array* or a *linked list*
as a base structure and functionality will be built on top.

Ideally, the *user* doesn't care about /how/ the data
structure works, they just care that they can *add, remove,*
and *access* data they want to store.

Ideally, when we are writing a data structure, it should be:

- Generic, so you could store /any data type/ in
- Reusable, so that the data structure can be used in many different programs.
- Robust, offering exception handling to prevent the program from crashing when something goes wrong with it.
- Encapsulated, handling the inner-workings of dealing with the data, without the user (or other programmers working outside the data structure) having to write special code to perform certain operations.

The way I try to conceptualize the work I'm doing on a data structure
is to pretend that I'm a developer that is going to create and sell a
C++ library of data structures that /other developers at other companies/
can use in their own, completely separate, software projects. If I'm
selling my data structures package to other businesses, my code should
be dependable, stable, efficient, and relatively easy to use.

[[file:images/c4_u00_data-structures-ad.png]]

#+BEGIN_LATEX
\vspace{0.2cm}
#+END_LATEX

---------------------------------------------

* What is "algorithm analysis"?

[[file:images/c4_u00_complexity.png]]

Algorithm Analysis is the process of figuring out how
*efficient* a function is and how it scales over time,
given more and more data to operate on.

This is another important part of dealing with data structures,
as different structures will offer different trade offs when
it comes to the efficiency of *data access* functions,
*data searching* functions, *data adding* functions,
and *data removal* functions.

Every operation takes a little bit of processing time, and as you iterate over data, that
processing time is multiplied by the amount of times you go through a loop.

*Example: Scalability*

Let's say we have a sorting algorithm that, for every $n$ records,
it takes $n^2$ program units to find an object.
If we had 100 records, then it would take $100^2 = 10,000$ time-units to
sort the set of data.

What the time-units are could vary - an older
machine might take longer to execute one instruction, and a newer
computer might process an instruction much more quickly, but we think of
algorithm complexity in this sort of generic form.

[[file:images/c4_u00_search-sort-ad.png]]


*Example: Efficiency of an Array-based structure*

|        0 |          1 |          2 |      3 |          4 |
| "kansas" | "missouri" | "arkansas" | "ohio" | "oklahoma" |

In an array-based structure, we have a series of *elements* in a row,
and each element is accessible via its *index* (its position in the array).
Arrays allow for random-access, so *accessing* element #2 is instant:

#+BEGIN_SRC cpp :class cpp
  cout << arr[2];
#+END_SRC

No matter how many elements there are ($n$), we can access the element at
index 2 without doing any looping. We state that this is $O(1)$ ("Big-O of 1")
time complexity for an *access* operation on an *array.

However, if we were *searching* through the unsorted array for an item, we would
have to start at the beginning and look at each item, one at a time, until
we either found what we're looking for, or hit the end of the array:

#+BEGIN_SRC cpp :class cpp
  for ( int i = 0; i < ARR_SIZE; i++ )
  {
      if ( arr[i] == searchTerm )
      {
          return i; // found at this position
      }
  }
  return -1;      // not found
#+END_SRC

For *search*, the /worst-case scenario/ is having to
look at /all elements of the array/ to ensure what we're
looking for isn't there. Given $n$ items in the array, we have to
iterate through the loop $n$ times. This would end up being
$O(n)$ ("Big-O of $n$") time complexity for a *search*
operation on an *array*.

We can build our data structures on top of an array, but there
is also a type of structure called a *linked* structure,
which offers its own pros and cons to go with it. We will learn
more about algorithm analysis and types of structures later on.

#+BEGIN_LATEX
\vspace{0.2cm}
#+END_LATEX

---------------------------------------------

* Why is there a whole class dedicated to "data structures"?

Of course, plenty of data structures have already been written
and are available out there for you to use. C++ even has the
*Standard Template Library* full of structures
already built and optimized!

*/"So why are we learning to write data structures if they've already been written for us?"/*

While you generally /won't/ be rolling your own linked list
for projects or on the job, it /is/ important to know how
the inner-workings of these structures operate. Knowing how each structure
works, its tradeoffs in efficiency, and how it structures its data
will help you *choose* what structures to use when faced with
a *design decision* for your software.

[[file:images/c4_u00_which-structure.png]]

---------------------------------------------

* Review questions

Answer these questions in your notes, then check your work by completing the
related Concept Intro assignment on Canvas.

#+ATTR_HTML: :class action-item
#+NAME: content
#+BEGIN_HTML
*Review questions:*
1. Why should a data structure be generic?
2. What does it mean for a data structure to be robust?
3. Most data structures have what kind of core functionality?
#+END_HTML
