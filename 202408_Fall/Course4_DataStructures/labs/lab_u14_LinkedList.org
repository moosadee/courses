# -*- mode: org -*-

#+ATTR_HTML: :class action-item
#+NAME: content
#+BEGIN_HTML


[[file:../../images/comics/pixel-goals.png]] *Goals:*
- Implement a Linked List
- Implement a Stack
- Implement a Queue
- Look at the data structures in use in a program


[[file:../../images/comics/pixel-turnin.png]] *Turn-in:*
- You'll commit your code to your repository, create a *merge request*, and submit the merge request URL in Canvas. (Instructions in document.)


[[file:../../images/comics/pixel-practice.png]] *Practice programs and graded program:*
- This assignment contains several "practice" program folders,
  and a "graded" program folder. Only the "graded" program is required.
- The practice programs are there to help you practice with the topics before
  tackling the larger graded program. If you feel confident in the topic already,
  you may go ahead and work on the graded program. If you feel like you need
  additional practice first, that's what the practice assignments are for.
- The *graded program* assignment contains unit tests to help verify your code.
- You can score up to 100% turning in just the *graded program* assignment.
- You can turn in the *practice* assignments for *extra credit points*.


[[file:../../images/comics/pixel-fight.png]] *Stuck on the assignment? Not sure what to do next?*
- Continue scrolling down in the documentation and see if you're missing anything.
- Try skimming through the entire assignment before getting started,
  to get a high-level overview of what we're going to be doing.


#+END_HTML

------------------------------------------------
* *Setup: Starter code and new branch*

For this assignment, I've already added the code to your repository.

1. Pull the starter code:
   1. Check out the =main= branch: =git checkout main=
   2. Pull latest: =git pull=
2. Create a new branch to start working from: =git checkout -b BRANCHNAME=

The folder =cs250_program= is added:

#+begin_src artist

=^o.o^=

#+end_src


------------------------------------------------
* *About the program*

[[file:images/lab_u14_LinkedList_program.png]]

The main menu of the program contains several options:
- *[1] Shopazon Store*
- *[2] Grillezy Sandwiches*
- *[11] Smart dynamic array tests*
- *[12] Linked list tests*
- *[13] Stack tests*
- *[14] Queue tests*

Options 1 and 2 are programs that utilize the data structures you'll be implementing.
You don't need to implement the programs themselves, but you can look at their
usage within the code.

As you work on the data structures, run the automated tests to check your work.
These automated tests generate *output .html files* with detailed information about
the tests. Make sure to check these for information on why a test fails.

[[file:images/lab_u14_LinkedList_tests.png]]

The test file will be generated in your project directory, either *=Project_VisualStudio2022=*,
*=Project_CodeBlocks=*, or *=Project_Makefile=*.

[[file:images/lab_u14_LinkedList_testsfolder.png]]


For this program the *SmartDynamicArray* is already provided and you will implement the *LinkedList*.
There is also two variants of the Stack and the Queue: *ArrayStack*, *LinkedStack*, *ArrayQueue*, *LinkedQueue*.
The array versions are already done, and you'll implement the linked versions on top of the LinkedList.
More information later.

------------------------------------------------
* *Implementing the data structures*

| Data structure                      | Path                                                   |
|-------------------------------------+--------------------------------------------------------|
| Smart Dynamic Array (already done)  | =DataStructures/SmartDynamicArray/SmartDynamicArray.h= |
| Linked List                         | =DataStructures/LinkedList/LinkedList.h=               |
| Queue (Array version, already done) | =DataStructures/Queue/ArrayQueue.h=                    |
| Queue (Linked version)              | =DataStructures/Queue/LinkedQueue.h=                   |
| Stack (Array version, already done) | =DataStructures/Stack/ArrayStack.h=                    |
| Stack (Linked version)              | =DataStructures/Stack/LinkedStack.h=                   |

Refer to the textbook, slides, and class video for the implementation specifics.

------------------------------------------------
* *Looking at the programs*

We will also be looking at the implementations of the *Shopazon Store* and *Grillezy Sandwiches* during class,
so watch the archived lecture video if you miss it.


------------------------------------------------
* *Turning in the assignment*

*Screenshot:* Before finishing up, run the automated tests and take a screenshot
of all of your tests passing. Save the file somewhere as a .png.

*Back up your code:* Open Git Bash in the base directory of your repository folder.

1. Use =git status= to view all the changed items (in red).
2. Use =git add .= to add all current changed items.
3. Use =git commit -m "INFO"= to commit your changes, change "INFO" to a more descriptive message.
4. Use =git push -u origin BRANCHNAME= to push your changes to the server.
5. On GitLab.com, go to your repository webpage. There should be a button to
   *Create a new Merge Request*. You can leave the default settings and create the merge request.
6. Copy the URL of your merge request.

*Turn in on Canvas:*
1. Find the assignment on Canvas and click *"Start assignment"*.
2. Select *Upload file* and *upload* your screenshot.
3. *Paste your GitLab URL* into the Comments box.
4. Click *"Submit assignment"* to finish.
