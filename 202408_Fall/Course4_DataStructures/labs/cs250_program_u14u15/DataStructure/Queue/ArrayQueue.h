#ifndef ARRAY_QUEUE_HPP
#define ARRAY_QUEUE_HPP

// Project includes
#include "../SmartDynamicArray/SmartDynamicArray.h"
#include "../../Namespace_Utilities/Logger.h"
#include "../../Exceptions/NotImplementedException.h"
#include "../../Exceptions/StructureFullException.h"
#include "../../Exceptions/InvalidIndexException.h"
#include "../../Exceptions/NullptrException.h"

namespace DataStructure
{

template <typename T>
//! A first-in-first-out (FIFO) queue structure built on top of an array
class ArrayQueue
{
public:
    //! Push a new item into the back of the queue
    void Push(const T& newData );
    //! Remove the item at the front of the queue
    void Pop();
    //! Access the data at the front of the queue
    T& Front();
    //! Get the amount of items in the queue
    int Size();
    //! Return whether the queue is empty
    bool IsEmpty();

private:
    SmartDynamicArray<T> m_vector;

    friend class QueueTester;
};

template <typename T>
void ArrayQueue<T>::Push( const T& newData )
{
    m_vector.PushBack( newData );
}

template <typename T>
void ArrayQueue<T>::Pop()
{
    m_vector.PopFront();
}

template <typename T>
T& ArrayQueue<T>::Front()
{
    return m_vector.GetFront();
}

template <typename T>
int ArrayQueue<T>::Size()
{
    return m_vector.Size();
}

template <typename T>
bool ArrayQueue<T>::IsEmpty()
{
    return m_vector.IsEmpty();
}

} // End of namespace

#endif
