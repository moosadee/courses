#ifndef _NULLPTR_EXCEPTION
#define _NULLPTR_EXCEPTION

// C++ Library includes
#include <stdexcept>
#include <string>

namespace Exception
{

//! EXCEPTION for when a nullptr is detected when it is not expected
class NullptrException : public std::runtime_error
{
    public:
    NullptrException( std::string functionName, std::string message )
        : std::runtime_error( "[" + functionName + "] " + message ) { ; }
};

} // End of namespace

#endif
