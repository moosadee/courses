#include "Program.h"
#include "../Namespace_Utilities/ScreenDrawer.hpp"
#include "../DataStructure/LinkedList/LinkedListTester.h"
#include "../DataStructure/Queue/QueueTester.h"
#include "../DataStructure/SmartDynamicArray/SmartDynamicArrayTester.h"
#include "../DataStructure/Stack/StackTester.h"
#include "../Program_Shopazon/ShopazonProgram.h"
#include "../Program_Grillezy/GrillezyProgram.h"

#include <iostream>

Program::Program()
{
    Setup();
}

Program::~Program()
{
    Teardown();
}

void Program::Setup()
{
    this->screen_width = 80;
    this->screen_height = 20;
    this->save_path = "../Data/";
    Utilities::ScreenDrawer::Setup( this->screen_width, this->screen_height );
}

void Program::Teardown()
{
    Utilities::ScreenDrawer::Teardown();
}

void Program::Run()
{
    this->Menu_Main();
}

void Program::Menu_Main()
{
    bool done = false;
    while ( !done )
    {
        Utilities::ScreenDrawer::DrawBackground();
        Utilities::ScreenDrawer::DrawWindow( "DATA STRUCTURES PROGRAM", 2, 1, 76, 20-2 );
        Utilities::ScreenDrawer::Set( 40, 2, "STORAGE PATH: " + this->save_path, "yellow", "blue" );

        Utilities::ScreenDrawer::Set( 6, 4,  "Programs", "white", "black" );
        Utilities::ScreenDrawer::Set( 7, 5,  "[1] Shopazon Store", "white", "black" );
        Utilities::ScreenDrawer::Set( 7, 6,  "[2] Grillezy Sandwiches", "white", "black" );


        Utilities::ScreenDrawer::Set( 40, 4, "Tests", "white", "black" );
        Utilities::ScreenDrawer::Set( 41, 5,  "[11] Smart dynamic array tests", "white", "black" );
        Utilities::ScreenDrawer::Set( 41, 6,  "[12] Linked list tests", "white", "black" );
        Utilities::ScreenDrawer::Set( 41, 7,  "[13] Stack tests", "white", "black" );
        Utilities::ScreenDrawer::Set( 41, 8,  "[14] Queue tests", "white", "black" );

        Utilities::ScreenDrawer::Set( 6, 20-4, "[0] Exit", "white", "black" );

        Utilities::ScreenDrawer::Draw();

        int choice;
        std::cout << ">> ";
        std::cin >> choice;
        std::cin.ignore();

        switch( choice )
        {
        case 0:
            done = true;
            break;

        case 1:
        {
            Shopazon::ShopazonProgram program;
            program.SetDataPath( "../Data/" );
            program.Run();
        }
        break;
        case 2:
        {
            Grillezy::GrillezyProgram program;
            program.SetDataPath( "../Data/" );
            program.Run();
        }
        break;

        case 11:
        {
            DataStructure::SmartDynamicArrayTester tester;
            tester.Start();
        }
        break;
        case 12:
        {
            DataStructure::LinkedListTester tester;
            tester.Start();
        }
        break;
        case 13:
        {
            DataStructure::StackTester tester;
            tester.Start();
        }
        break;
        case 14:
        {
            DataStructure::QueueTester tester;
            tester.Start();
        }
        break;
        }

        PressEnterToContinue();
    }
}

void Program::PressEnterToContinue()
{
    std::string hit_enter;
    std::cout << std::endl << "HIT ENTER TO CONTINUE" << std::endl;
    getline( std::cin, hit_enter );
}

int Program::GetValidChoice( int min, int max )
{
    int choice;
    std::cout << ">> ";
    std::cin >> choice;

    while (choice < min || choice > max)
    {
        std::cout << "Must be between " << min << " and " << max << "! Try again!" << std::endl;
        std::cout << ">> ";
        std::cin >> choice;
    }

    return choice;
}
