#ifndef _PROGRAM
#define _PROGRAM

#include <vector>
#include <memory>
#include <string>

class Program
{
public:
    Program();
    ~Program();

    void Setup();
    void Teardown();
    void Run();

    void Menu_Main();
    void Menu_CreateDocument();
    void Menu_ViewDocuments();
private:
    void PressEnterToContinue();
    int GetValidChoice( int min, int max );

    int screen_width;
    int screen_height;
    std::string save_path;
};

int main();

#endif
