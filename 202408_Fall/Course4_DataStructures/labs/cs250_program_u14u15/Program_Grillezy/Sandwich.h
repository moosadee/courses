#ifndef _GRILLEZY_SANDWICH
#define _GRILLEZY_SANDWICH

#include <stack>
#include <string>
#include <ostream>

namespace Grillezy
{

class Sandwich
{
public:
    void Clear();
    void Add( std::string topping );
    void Remove();

private:
    std::stack<std::string> m_toppings;

    friend std::ostream& operator<<( std::ostream& out, const Sandwich& me );
};

}

#endif
