#ifndef _LOGIFY_PROGRAM
#define _LOGIFY_PROGRAM

#include "../DataStructure/HashTable/HashTable.h"
#include "DataGen.h"
#include "Employee.h"

#include <string>
#include <fstream>

namespace Logify
{

class LogifyProgram
{
public:
	LogifyProgram();
	void Run();
	void SetDataPath(std::string path);

private:
	void Setup();
	void Cleanup();

	std::string m_dataPath;
	std::ofstream m_log;
	DataGen m_dataGen;

	DataStructure::HashTable<Employee> m_employees;
};

}

#endif
