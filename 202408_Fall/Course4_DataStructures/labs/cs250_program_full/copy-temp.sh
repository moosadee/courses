cp /home/rachelwil/TEACHING/courses/202401_Spring/Course4_DataStructures/labs/cs250_program_full/DataStructure/BinarySearchTree ./DataStructure/ -r
cp /home/rachelwil/TEACHING/courses/202401_Spring/Course4_DataStructures/labs/cs250_program_full/DataStructure/SmartTable ./DataStructure/ -r
cp /home/rachelwil/TEACHING/courses/202401_Spring/Course4_DataStructures/labs/cs250_program_full/DataStructure/HashTable ./DataStructure/ -r
cp /home/rachelwil/TEACHING/courses/202401_Spring/Course4_DataStructures/labs/cs250_program_full/Program_Base ./ -r
cp /home/rachelwil/TEACHING/courses/202401_Spring/Course4_DataStructures/labs/cs250_program_full/Program_CallWave ./ -r
cp /home/rachelwil/TEACHING/courses/202401_Spring/Course4_DataStructures/labs/cs250_program_full/Program_Logify ./ -r
cp /home/rachelwil/TEACHING/courses/202401_Spring/Course4_DataStructures/labs/cs250_program_full/Program_Shopazon ./ -r
cp /home/rachelwil/TEACHING/courses/202401_Spring/Course4_DataStructures/labs/cs250_program_full/Program_Grillezy ./ -r
cp /home/rachelwil/TEACHING/courses/202401_Spring/Course4_DataStructures/labs/cs250_program_full/Project_CodeBlocks ./ -r
cp /home/rachelwil/TEACHING/courses/202401_Spring/Course4_DataStructures/labs/cs250_program_full/Project_Makefile ./ -r
cp /home/rachelwil/TEACHING/courses/202401_Spring/Course4_DataStructures/labs/cs250_program_full/Project_VisualStudio2022 ./ -r
cp /home/rachelwil/TEACHING/courses/202401_Spring/Course4_DataStructures/labs/cs250_program_full/Data ./ -r
rm Project_CodeBlocks/*.html
rm Project_CodeBlocks/*Exe
rm Project_CodeBlocks/LinkedList_DataStructure.*
git add *.h *.cpp
git add Program_Base/
git add Program_Grillezy/
git add Program_Shopazon/
git add Program_Logify/
git add Program_CallWave/
git add Project_*/
git add Data/
git add DataStructure/
#codeblocks Project_CodeBlocks/*.cbp
git status
git commit -m "Remaining data structures - Binary Search Tree and Hash Table"
git push
rm copy-temp.sh
