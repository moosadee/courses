
--------------------------------------------------------------------------------
void Program::ReadMessages()
Added [ADD, bun] to message queue.
Added [ADD, cheese] to message queue.
Added [ADD, mushroom] to message queue.
Added [REMOVE, ] to message queue.
Added [ADD, mayo] to message queue.
Added [ADD, chicken] to message queue.
Added [ADD, bun] to message queue.
Added [FINISH, ] to message queue.
Added [ADD, biscuit] to message queue.
Added [ADD, asparagus] to message queue.
Added [REMOVE, ] to message queue.
Added [ADD, gravy] to message queue.
Added [ADD, sausage] to message queue.
Added [ADD, biscuit] to message queue.
Added [ADD, biscuit] to message queue.
Added [REMOVE, ] to message queue.
Added [FINISH, ] to message queue.
Added [ADD, bread] to message queue.
Added [ADD, cheese] to message queue.
Added [ADD, mayo] to message queue.
Added [ADD, egg] to message queue.
Added [ADD, cheese] to message queue.
Added [ADD, cheese] to message queue.
Added [ADD, cheese] to message queue.
Added [REMOVE, ] to message queue.
Added [REMOVE, ] to message queue.
Added [ADD, bread] to message queue.
Added [FINISH, ] to message queue.
Message queue size is now: 28

--------------------------------------------------------------------------------
void Program::ProcessMessages()
Process message [ADD, bun].
Add "bun" to the sandwich.
Process message [ADD, cheese].
Add "cheese" to the sandwich.
Process message [ADD, mushroom].
Add "mushroom" to the sandwich.
Process message [REMOVE, ].
Remove last added item from the sandwich.
Process message [ADD, mayo].
Add "mayo" to the sandwich.
Process message [ADD, chicken].
Add "chicken" to the sandwich.
Process message [ADD, bun].
Add "bun" to the sandwich.
Process message [FINISH, ].
Sandwich is finished, add to results list.
Process message [ADD, biscuit].
Add "biscuit" to the sandwich.
Process message [ADD, asparagus].
Add "asparagus" to the sandwich.
Process message [REMOVE, ].
Remove last added item from the sandwich.
Process message [ADD, gravy].
Add "gravy" to the sandwich.
Process message [ADD, sausage].
Add "sausage" to the sandwich.
Process message [ADD, biscuit].
Add "biscuit" to the sandwich.
Process message [ADD, biscuit].
Add "biscuit" to the sandwich.
Process message [REMOVE, ].
Remove last added item from the sandwich.
Process message [FINISH, ].
Sandwich is finished, add to results list.
Process message [ADD, bread].
Add "bread" to the sandwich.
Process message [ADD, cheese].
Add "cheese" to the sandwich.
Process message [ADD, mayo].
Add "mayo" to the sandwich.
Process message [ADD, egg].
Add "egg" to the sandwich.
Process message [ADD, cheese].
Add "cheese" to the sandwich.
Process message [ADD, cheese].
Add "cheese" to the sandwich.
Process message [ADD, cheese].
Add "cheese" to the sandwich.
Process message [REMOVE, ].
Remove last added item from the sandwich.
Process message [REMOVE, ].
Remove last added item from the sandwich.
Process message [ADD, bread].
Add "bread" to the sandwich.
Process message [FINISH, ].
Sandwich is finished, add to results list.

--------------------------------------------------------------------------------
void Program::SaveResults()
Writing out sandwich:
bun
cheese
mayo
chicken
bun

Writing out sandwich:
biscuit
gravy
sausage
biscuit

Writing out sandwich:
bread
cheese
mayo
egg
cheese
bread


Results written to: ../Data/results-output.txt
