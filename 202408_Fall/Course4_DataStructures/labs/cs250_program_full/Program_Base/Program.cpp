#include "Program.h"
#include "../Namespace_Utilities/ScreenDrawer.hpp"
#include "../DataStructure/LinkedList/LinkedListTester.h"
#include "../DataStructure/Queue/QueueTester.h"
#include "../DataStructure/SmartDynamicArray/SmartDynamicArrayTester.h"
#include "../DataStructure/BinarySearchTree/BinarySearchTreeTester.h"
#include "../DataStructure/HashTable/HashTableTester.h"
#include "../DataStructure/Stack/StackTester.h"
#include "../Program_Shopazon/ShopazonProgram.h"
#include "../Program_Grillezy/GrillezyProgram.h"
#include "../Program_CallWave/CallWaveProgram.h"
#include "../Program_Logify/LogifyProgram.h"

#include <iostream>

Program::Program()
{
    Setup();
}

Program::~Program()
{
    Teardown();
}

void Program::Setup()
{
    this->screen_width = 80;
    this->screen_height = 20;
    this->save_path = "../Data/";
    Utilities::ScreenDrawer::Setup( this->screen_width, this->screen_height );
}

void Program::Teardown()
{
    Utilities::ScreenDrawer::Teardown();
}

void Program::Run()
{
    this->Menu_Main();
}

void Program::Menu_Main()
{
    bool done = false;
    while ( !done )
    {
        Utilities::ScreenDrawer::DrawBackground();
        Utilities::ScreenDrawer::DrawWindow( "DATA STRUCTURES PROGRAM", 2, 1, 76, 20-2 );
        Utilities::ScreenDrawer::Set( 40, 2, "STORAGE PATH: " + this->save_path, "yellow", "blue" );

        Utilities::ScreenDrawer::Set( 6, 4,  "Programs", "white", "black" );
        Utilities::ScreenDrawer::Set( 7, 5,  "[1] Shopazon Store", "white", "black" );
        Utilities::ScreenDrawer::Set( 7, 6,  "[2] Grillezy Sandwiches", "white", "black" );
        Utilities::ScreenDrawer::Set( 7, 7,  "[3] CallWave Phone Support", "white", "black" );
        Utilities::ScreenDrawer::Set( 7, 8,  "[4] Logify Employee Login", "white", "black" );


        Utilities::ScreenDrawer::Set( 40, 4,   "Tests", "white", "black" );
        Utilities::ScreenDrawer::Set( 41, 5,   "[10] All tests", "white", "black" );
        Utilities::ScreenDrawer::Set( 41, 6,   "[11] Smart dynamic array tests", "white", "black" );
        Utilities::ScreenDrawer::Set( 41, 7,   "[12] Linked list tests", "white", "black" );
        Utilities::ScreenDrawer::Set( 41, 8,   "[13] Stack tests", "white", "black" );
        Utilities::ScreenDrawer::Set( 41, 9,   "[14] Queue tests", "white", "black" );
        Utilities::ScreenDrawer::Set( 41, 10,  "[15] BinarySearchTree tests", "white", "black" );
        Utilities::ScreenDrawer::Set( 41, 11,  "[16] HashTable tests", "white", "black" );

        Utilities::ScreenDrawer::Set( 6, 20-4, "[0] Exit", "white", "black" );

        Utilities::ScreenDrawer::Draw();

        int choice;
        std::cout << ">> ";
        std::cin >> choice;
        std::cin.ignore();

        switch( choice )
        {
            case 0:
                done = true;
                break;

            case 1:
            {
                Shopazon::Program program;
                program.SetDataPath( "../Data/" );
                program.Run();
            }
            break;

            case 2:
            {
                Grillezy::Program program;
                program.SetDataPath( "../Data/" );
                program.Run();
            }
            break;

            case 3:
            {
                Callwave::CallwaveProgram program;
                program.SetDataPath( "../Data/" );
                program.Run();
            }
            break;

            case 4:
            {
                Logify::LogifyProgram program;
                program.SetDataPath( "../Data/" );
                program.Run();
            }
            break;

            case 10:
            {
                DataStructure::SmartDynamicArrayTester sdatester;
                sdatester.Start();

                DataStructure::LinkedListTester lltester;
                lltester.Start();

                DataStructure::StackTester stester;
                stester.Start();

                DataStructure::QueueTester qtester;
                qtester.Start();

                DataStructure::BinarySearchTreeTester bsttester;
                bsttester.Start();

                DataStructure::HashTableTester httester;
                httester.Start();
            }
            break;

            case 11:
            {
                DataStructure::SmartDynamicArrayTester tester;
                tester.Start();
            }
            break;

            case 12:
            {
                DataStructure::LinkedListTester tester;
                tester.Start();
            }
            break;

            case 13:
            {
                DataStructure::StackTester tester;
                tester.Start();
            }
            break;

            case 14:
            {
                DataStructure::QueueTester tester;
                tester.Start();
            }
            break;

            case 15:
            {
                DataStructure::BinarySearchTreeTester tester;
                tester.Start();
            }
            break;

            case 16:
            {
                DataStructure::HashTableTester tester;
                tester.Start();
            }
            break;
        }

        PressEnterToContinue();
    }
}

void Program::PressEnterToContinue()
{
    std::string hit_enter;
    std::cout << std::endl << "HIT ENTER TO CONTINUE" << std::endl;
    getline( std::cin, hit_enter );
}

int Program::GetValidChoice( int min, int max )
{
    int choice;
    std::cout << ">> ";
    std::cin >> choice;

    while (choice < min || choice > max)
    {
        std::cout << "Must be between " << min << " and " << max << "! Try again!" << std::endl;
        std::cout << ">> ";
        std::cin >> choice;
    }

    return choice;
}
