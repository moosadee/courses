#include "CallWaveProgram.h"

#include "../Namespace_Utilities/Menu.h"

#include <iostream>
#include <iomanip>
#include <fstream>

namespace Callwave
{

void CallwaveProgram::SetDataPath(std::string path)
{
	m_dataPath = path;
	std::cout << "Program data should be located at \"" << m_dataPath << "\"..." << std::endl;
}

void CallwaveProgram::Setup()
{
	SetupLog();
	m_hour = 8;
	m_minute = 0;
	m_timePass = 5; // pass minutes 5 at a time
	std::cout << std::fixed;
	m_log << std::fixed;
	m_generator.Setup(m_dataPath + "names.txt");
}

void CallwaveProgram::Cleanup()
{
	CleanupLog();

	// Free memory
	for (auto& call : m_callList)
	{
		if (call != nullptr)
		{
			delete call;
			call = nullptr;
		}
	}
}

void CallwaveProgram::Run()
{
	Setup();

	// While we're during business hours
	while (m_hour < (12 + 5))
	{
		if (m_callPriorityQueue.GetCount() == 0)
		{
			ReceiveCall();
		}
		else
		{
			int action = rand() % 10;
			if (action < 2)
			{
				ResolveCall();
			}
			else if (action < 4)
			{
				RequeueCall();
			}
			else
			{
				ReceiveCall();
			}
		}


		// Time passes...
		m_minute += m_timePass;
		if (m_minute >= 60) {
			m_hour++;
			m_minute = 0;
		}
		for (auto& call : m_callList)
		{
			call->Wait(m_timePass);
			cout << "[" << m_hour << ":" << setw(2) << setfill('0') << m_minute << "]: (STATUS) - Customer: "
				<< call->customerName << " (SCORE: " << call->customerValue
				<< ", MINUTES ON CALL: " << call->minutesOnCall << ") IS STILL WAITING..." << endl;
		}
	}

	std::cout << std::endl << "WORK DAY IS OVER!" << std::endl;

	SaveSummary();

	Cleanup();
}

void CallwaveProgram::ReceiveCall()
{
	Call* newCall = new Call;
	int customerValue = rand() % 1000 + 5;
	while (m_callPriorityQueue.Contains(customerValue))
	{
		customerValue = rand() % 1000 + 5;
	}
	newCall->Setup(m_generator.GetRandomName(), customerValue);

	m_log << "<tr class='receive'><td>" << m_hour << ":" << setw(2) << setfill('0') << m_minute << "</td>";
	m_log << "<td>" << m_callPriorityQueue.GetCount() << " calls in queue</td>";
	m_log << "<td>RECEIVED</td>";
	m_log << "<td>" << newCall->minutesOnCall << "</td>";
	m_log << "<td>" << newCall->customerValue << "</td>";
	m_log << "<td>" << newCall->customerName << "</td>";
	m_log << "<td></td>";
	m_log << "</tr>" << endl;

	m_callList.push_back(newCall);
	m_callPriorityQueue.Push(newCall->customerValue, newCall);
}

void CallwaveProgram::ResolveCall()
{
	if (m_callPriorityQueue.GetCount() > 0)
	{
		Call* ptrCall = m_callPriorityQueue.GetRoot();

		m_log << "<tr class='resolve'><td>" << m_hour << ":" << setw(2) << setfill('0') << m_minute << "</td>";
		m_log << "<td>" << m_callPriorityQueue.GetCount() << " calls in queue</td>";
		m_log << "<td>RESOLVED</td>";
		m_log << "<td>" << ptrCall->minutesOnCall << "</td>";
		m_log << "<td>" << ptrCall->customerValue << "</td>";
		m_log << "<td>" << ptrCall->customerName << "</td>";
		m_log << "<td></td>";
		m_log << "</tr>" << endl;

		ptrCall->resolved = true;
		m_callPriorityQueue.PopRoot();
	}
}

void CallwaveProgram::RequeueCall()
{
	if (m_callPriorityQueue.GetCount() > 0)
	{
		Call* ptrCall = m_callPriorityQueue.GetRoot();

		m_log << "<tr class='requeue'><td>" << m_hour << ":" << setw(2) << setfill('0') << m_minute << "</td>";
		m_log << "<td>" << m_callPriorityQueue.GetCount() << " calls in queue</td>";
		m_log << "<td>REQUEUE</td>";
		m_log << "<td>" << ptrCall->minutesOnCall << "</td>";
		m_log << "<td>" << ptrCall->customerValue << "</td>";
		m_log << "<td>" << ptrCall->customerName << "</td>";
		m_log << "<td> Unable to resolve; finding new Customer Service Representative and re - queuing client.</td>";
		m_log << "</tr>" << endl;

		int updatedScore = ptrCall->customerValue - 1;
		while (m_callPriorityQueue.Contains(updatedScore))
		{
			updatedScore--;
		}

		m_callPriorityQueue.Push(updatedScore, ptrCall);
		m_callPriorityQueue.PopRoot();
	}
}

void CallwaveProgram::SaveSummary()
{
	m_log << "</table><hr>" << endl;

	m_log << "<h2>CALL SUMMARY</h2>" << endl;

	m_log << "<table>" << endl;
	m_log << "<tr><th>ID</th><th>DONE?</th><th>MINUTES ON CALL</th><th>VALUE SCORE</th><th>CUSTOMER NAME</th></tr>" << endl;

	for (size_t i = 0; i < m_callList.size(); i++)
	{
		string resolved = (m_callList[i]->resolved) ? "Y" : "N";

		m_log << "<tr>" << endl;
		m_log << "<td>" << i << "</td>";
		m_log << "<td>" << resolved << "</td>";
		m_log << "<td>" << m_callList[i]->minutesOnCall << "</td>";
		m_log << "<td>" << m_callList[i]->customerValue << "</td>";
		m_log << "<td>" << m_callList[i]->customerName << "</td>";
		m_log << "</tr>" << endl;
	}

	m_log << "</table>" << endl;
}

void CallwaveProgram::SetupLog()
{
	m_log.open("results.html");

	m_log << "<head><title>Call Queue Results</title>" << endl;
	m_log << "<style type='text/css'>" << endl;
	m_log << ".requeue { background: #fff06e;}" << endl;
	m_log << ".receive { background: #ffa96e; }" << endl;
	m_log << ".resolve { background: #77ff6e; }" << endl;
	m_log << ".status {}" << endl;
	m_log << "table { background: #ffffff; width: 100%; font-size: 10pt; }" << std::endl;
	m_log << "table tr { font-size: 12pt; border: solid 1px #000066; }" << std::endl;
	m_log << "table tr th { background: #fff; padding: 10px; border: solid 1px #000033; }" << std::endl;
	m_log << "table tr td { padding: 10px; border: solid 1px #000033; }" << std::endl;
	m_log << "</style>" << endl;
	m_log << "</head><body>" << endl;
	m_log << "<h1>Customr Call Queue System</h1>" << endl;

	m_log << "<table>" << endl;
	m_log << "<tr><th>TIME</th><th>STATUS</th><th>EVENT</th><th>MINUTES ON CALL</th><th>VALUE SCORE</th><th>CUSTOMER NAME</th><th>NOTES</th></tr>" << endl;
}

void CallwaveProgram::CleanupLog()
{
	m_log << "</body>" << endl;
	m_log.close();

	std::cout << std::endl << "VIEW FILE results.html FOR FULL OUTPUT" << std::endl;
}

}
