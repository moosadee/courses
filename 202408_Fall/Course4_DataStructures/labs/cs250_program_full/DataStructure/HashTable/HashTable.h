#ifndef _HASH_TABLE_TESTER
#define _HASH_TABLE_TESTER

#include <iomanip>

// Project includes
#include "HashItem.h"
#include "../SmartTable/SmartTable.h"
#include "../../Exceptions/StructureFullException.h"

namespace DataStructure
{

//! An enumeration to define what kind of collision method the hash table is using.
enum class CollisionMethod
{
    LINEAR,
    QUADRATIC,
    DOUBLE_HASH
};

template <typename T>
//! A key/value based data structure
class HashTable
{
public:
    HashTable( int size );
    HashTable();
    ~HashTable();

    //! Set the collision method the hash table uses
    void SetCollisionMethod( CollisionMethod method );

    //! Add a new item to the hash table with an integer key and any data type for the data
    void Push(size_t key, T data );

    //! Retrieve data from the table with a given key, throws exception if not found
    T& Get( int key );

    //! Returns the amount of items stored in the hash table
    int Size();

private:
    //! Primary hash to convert an integer key into an array index
    int Hash1( int key );

    //! Collision method that steps forward by 1 each time
    int LinearProbe( int originalIndex, int collisionCount );

    //! Collision method that steps forward by c^2 each time
    int QuadraticProbe( int originalIndex, int collisionCount );

    //! Collision method that uses a second hash to figure step size
    int Hash2( int key, int collisionCount );

    //! Hash table's current collision method
    CollisionMethod m_method;

    //! Internal array that may contain gaps
    SmartTable<HashItem<T>> m_table;

    friend class HashTableTester;
};

/* -----------------------------------------------------------------------------------------*/
/* ------------------------------------------------------------------------------ HashTable */
/**
- Set a default collision method
- Allocate memory for the base table, use a default value since none was specified
*/
template <typename T>
HashTable<T>::HashTable()
{
    m_method = CollisionMethod::LINEAR;
    m_table.AllocateMemory( 5 );
}

/* -----------------------------------------------------------------------------------------*/
/* ------------------------------------------------------------------------------ HashTable */
/**
- Set a default collision method
- Allocate memory in the table given the size passed in
*/
template <typename T>
HashTable<T>::HashTable( int size )
{
    m_method = CollisionMethod::LINEAR;
    m_table.AllocateMemory( size );
}

/* -----------------------------------------------------------------------------------------*/
/* ----------------------------------------------------------------------------- ~HashTable */
template <typename T>
HashTable<T>::~HashTable()
{
    // Nothing to do
}

template <typename T>
void HashTable<T>::SetCollisionMethod( CollisionMethod method )
{
    m_method = method;
}

template <typename T>
void HashTable<T>::Push(size_t key, T data)
{
    throw Exception::NotImplementedException( "HashTable<T>::Push" ); // Erase this once you work on this function
}

template <typename T>
T& HashTable<T>::Get( int key )
{
    throw Exception::NotImplementedException( "HashTable<T>::Get" ); // Erase this once you work on this function
}

template <typename T>
int HashTable<T>::Size()
{
    return m_table.Size();
}

template <typename T>
int HashTable<T>::Hash1( int key )
{
    throw Exception::NotImplementedException( "HashTable<T>::Hash1" ); // Erase this once you work on this function
}

template <typename T>
int HashTable<T>::LinearProbe( int originalIndex, int collisionCount )
{
    throw Exception::NotImplementedException( "HashTable<T>::LinearProbe" ); // Erase this once you work on this function
}

template <typename T>
int HashTable<T>::QuadraticProbe( int originalIndex, int collisionCount)
{
    throw Exception::NotImplementedException( "HashTable<T>::QuadraticProbe" ); // Erase this once you work on this function
}

template <typename T>
int HashTable<T>::Hash2( int key, int collisionCount )
{
    throw Exception::NotImplementedException( "HashTable<T>::Hash2" ); // Erase this once you work on this function
}

} // End namespace

#endif
