#ifndef _BINARY_SEARCH_TREE_HPP
#define _BINARY_SEARCH_TREE_HPP

// Project includes
#include "BinarySearchTreeNode.h"

#include <sstream>
#include <stdexcept>
using namespace std;

namespace DataStructure
{

template <typename TK, typename TD>
//! A template binary search tree class that takes a KEY and a DATA
class BinarySearchTree
{
private:
    //! A pointer to the root node of the tree; TK = data type of the key, TD = data type of the data.
    BinarySearchTreeNode<TK, TD>* m_ptrRoot;

    //! The amount of nodes in the tree
    int m_nodeCount;

    //! The tester is our friend
    friend class BinarySearchTreeTester;

public:
    //! BinarySearchTree Constructor
    BinarySearchTree();
    //! BinarySearchTree Destructor
    ~BinarySearchTree();

    // Basic functionality
    //! Function that begins the recursive push function. Add new item to tree.
    void Push( const TK& newKey, const TD& newData );
    //! Function that begins the recursive contains function. Check if key is contained within the tree.
    bool Contains( const TK& key );
    //! Function that begins the recursive find node function. Get data relayed to some key stored in the tree.
    TD& GetData( const TK& key );

    //! Retrieves the item at the root
    TD& GetRoot();
    //! Removes the root item and reorganizes the tree
    void PopRoot();
    //! Shifts nodes for deletion operation
    void ShiftNodes(BinarySearchTreeNode<TK, TD>* node1, BinarySearchTreeNode<TK, TD>* node2);

    // Traversal functions
    //! Function that begins the recursive in-order traversal.
    std::string GetInOrder();
    //! Function that begins the recursive pre-order traversal.
    std::string GetPreOrder();
    //! Function that begins the recursive post-order traversal.
    std::string GetPostOrder();

    // Additional functionality
    //! Function that begins the recursive get min key, returns key with smallest value.
    TK& GetMinKey();
    //! Function that begins the recursive get max key, returns key with largest value.
    TK& GetMaxKey();
    //! Function that beginst he recursive get height, returns the height of the tree.
    int GetHeight();
    //! Get amount of nodes in tree.
    int GetCount();

    //    void Delete( const TK& key );

private:
    //! Function that begins the recursive find node process based on node's key.
    BinarySearchTreeNode<TK, TD>* FindNode( const TK& key );

    // Recursive traversal functions
    //! Recursive in-order traversal function
    void RecursiveGetInOrder  ( BinarySearchTreeNode<TK, TD>* ptrCurrent, std::stringstream& stream );
    //! Recursive pre-order traversal function
    void RecursiveGetPreOrder ( BinarySearchTreeNode<TK, TD>* ptrCurrent, std::stringstream& stream );
    //! Recursive post-order traversal function
    void RecursiveGetPostOrder( BinarySearchTreeNode<TK, TD>* ptrCurrent, std::stringstream& stream );

    // Recursive additional functionality
    //! Recurses through nodes in tree looking for node with given key.
    BinarySearchTreeNode<TK, TD>* RecursiveFindNode( const TK& key, BinarySearchTreeNode<TK, TD>* ptrCurrent );
    //! Recurses through tree looking for the node with the maximum key value.
    BinarySearchTreeNode<TK, TD>* RecursiveGetMax( BinarySearchTreeNode<TK, TD>* ptrCurrent );
    //! Recurses through tree looking for the node with the minimum key value.
    BinarySearchTreeNode<TK, TD>* RecursiveGetMin( BinarySearchTreeNode<TK, TD>* ptrCurrent );
    //! Recurses through the tree comparing heights of left and right sub-trees to get the overall height.
    int RecursiveGetHeight( BinarySearchTreeNode<TK, TD>* ptrCurrent );
    //! Recurses through the tree searching for a node with a matching key.
    bool RecursiveContains( const TK& key, BinarySearchTreeNode<TK, TD>* ptrCurrent );
    //! Recurses through the tree searching for a position to insert the new data based on its key.
    void RecursivePush( const TK& newKey, const TD& newData, BinarySearchTreeNode<TK, TD>* ptrCurrent );

    //! Removes a node - the root of its own subtree
    void PopNode(BinarySearchTreeNode<TK, TD>* ptrCurrent);

    //! Gets the node with the smallest key that is greater than ptrCurrent's key.
    BinarySearchTreeNode<TK, TD>* GetSuccessor(BinarySearchTreeNode<TK, TD>* ptrCurrent);
};

template <typename TK, typename TD>
BinarySearchTree<TK,TD>::BinarySearchTree()     // done
{
    m_ptrRoot = nullptr;
    m_nodeCount = 0;
}

template <typename TK, typename TD>
BinarySearchTree<TK,TD>::~BinarySearchTree()     // done
{
    if ( m_ptrRoot != nullptr )
    {
        delete m_ptrRoot;
    }
}

template <typename TK, typename TD>
int BinarySearchTree<TK,TD>::GetCount()         // done
{
    return m_nodeCount;
}

template <typename TK, typename TD>
void BinarySearchTree<TK,TD>::Push( const TK& newKey, const TD& newData )
{
    throw Exception::NotImplementedException( "BinarySearchTree<TK,TD>::Push" ); // Erase this once you work on this function
}

template <typename TK, typename TD>
void BinarySearchTree<TK,TD>::RecursivePush( const TK& newKey, const TD& newData, BinarySearchTreeNode<TK, TD>* ptrCurrent )
{
    throw Exception::NotImplementedException( "BinarySearchTree<TK,TD>::RecursivePush" ); // Erase this once you work on this function
}

template <typename TK, typename TD>
bool BinarySearchTree<TK,TD>::Contains( const TK& key )
{
    return RecursiveContains( key, m_ptrRoot );
}

template <typename TK, typename TD>
bool BinarySearchTree<TK,TD>::RecursiveContains( const TK& key, BinarySearchTreeNode<TK, TD>* ptrCurrent )
{
    throw Exception::NotImplementedException( "BinarySearchTree<TK,TD>::RecursiveContains" ); // Erase this once you work on this function
}

template <typename TK, typename TD>
std::string BinarySearchTree<TK,TD>::GetInOrder()
{
    throw Exception::NotImplementedException( "BinarySearchTree<TK,TD>::GetInOrder" ); // Erase this once you work on this function

    std::stringstream stream;
    RecursiveGetInOrder( m_ptrRoot, stream );
    return stream.str();
}

template <typename TK, typename TD>
void BinarySearchTree<TK,TD>::RecursiveGetInOrder( BinarySearchTreeNode<TK, TD>* ptrCurrent, std::stringstream& stream )
{
    throw Exception::NotImplementedException( "BinarySearchTree<TK,TD>::BinarySearchTreeNode" ); // Erase this once you work on this function
}

template <typename TK, typename TD>
std::string BinarySearchTree<TK,TD>::GetPreOrder()
{
    throw Exception::NotImplementedException( "BinarySearchTree<TK,TD>::GetPreOrder" ); // Erase this once you work on this function
}

template <typename TK, typename TD>
void BinarySearchTree<TK,TD>::RecursiveGetPreOrder( BinarySearchTreeNode<TK, TD>* ptrCurrent, std::stringstream& stream )
{
    throw Exception::NotImplementedException( "BinarySearchTree<TK,TD>::RecursiveGetPreOrder" ); // Erase this once you work on this function
}

template <typename TK, typename TD>
std::string BinarySearchTree<TK,TD>::GetPostOrder()
{
    throw Exception::NotImplementedException( "BinarySearchTree<TK,TD>::GetPostOrder" ); // Erase this once you work on this function
}

template <typename TK, typename TD>
void BinarySearchTree<TK,TD>::RecursiveGetPostOrder( BinarySearchTreeNode<TK, TD>* ptrCurrent, std::stringstream& stream )
{
    throw Exception::NotImplementedException( "BinarySearchTree<TK,TD>::RecursiveGetPostOrder" ); // Erase this once you work on this function
}

template <typename TK, typename TD>
TK& BinarySearchTree<TK,TD>::GetMaxKey()
{
    throw Exception::NotImplementedException( "BinarySearchTree<TK,TD>::GetMaxKey" ); // Erase this once you work on this function
}

template <typename TK, typename TD>
BinarySearchTreeNode<TK, TD>* BinarySearchTree<TK,TD>::RecursiveGetMax( BinarySearchTreeNode<TK, TD>* ptrCurrent )
{
    throw Exception::NotImplementedException( "BinarySearchTree<TK,TD>::RecursiveGetMax" ); // Erase this once you work on this function
}

template <typename TK, typename TD>
TK& BinarySearchTree<TK,TD>::GetMinKey()
{
    throw Exception::NotImplementedException( "BinarySearchTree<TK,TD>::GetMinKey" ); // Erase this once you work on this function
}

template <typename TK, typename TD>
BinarySearchTreeNode<TK, TD>* BinarySearchTree<TK,TD>::RecursiveGetMin( BinarySearchTreeNode<TK, TD>* ptrCurrent )
{
    throw Exception::NotImplementedException( "BinarySearchTree<TK,TD>::RecursiveGetMin" ); // Erase this once you work on this function
}

template <typename TK, typename TD>
TD& BinarySearchTree<TK,TD>::GetData( const TK& key )
{
    BinarySearchTreeNode<TK, TD>* node = FindNode( key );
    if ( node == nullptr ) { throw runtime_error( "Key not found!" ); }
    return node->data;
}

template <typename TK, typename TD>
BinarySearchTreeNode<TK, TD>* BinarySearchTree<TK,TD>::FindNode( const TK& key )
{
    return RecursiveFindNode( key, m_ptrRoot );
}

template <typename TK, typename TD>
BinarySearchTreeNode<TK, TD>* BinarySearchTree<TK,TD>::RecursiveFindNode( const TK& key, BinarySearchTreeNode<TK, TD>* ptrCurrent )
{
    throw Exception::NotImplementedException( "BinarySearchTree<TK,TD>::RecursiveFindNode" ); // Erase this once you work on this function
}

template <typename TK, typename TD>
int BinarySearchTree<TK,TD>::GetHeight()
{
    if ( m_ptrRoot == nullptr ) { return 0; }
    return RecursiveGetHeight( m_ptrRoot );
}

template <typename TK, typename TD>
int BinarySearchTree<TK,TD>::RecursiveGetHeight( BinarySearchTreeNode<TK, TD>* ptrCurrent )
{
    throw Exception::NotImplementedException( "BinarySearchTree<TK,TD>::RecursiveGetHeight" ); // Erase this once you work on this function
}

template <typename TK, typename TD>
TD& BinarySearchTree<TK, TD>::GetRoot()
{
    if (m_ptrRoot == nullptr) { throw runtime_error("Tree is empty!"); }
    return m_ptrRoot->data;
}

template <typename TK, typename TD>
void BinarySearchTree<TK, TD>::PopRoot() // done
{
    PopNode(m_ptrRoot);
}

// !! This stuff is done !!

// https://en.wikipedia.org/wiki/Binary_search_tree#Deletion
template <typename TK, typename TD>
void BinarySearchTree<TK, TD>::PopNode(BinarySearchTreeNode<TK, TD>* ptrCurrent) // done
{
    if (m_ptrRoot->ptrLeft == nullptr)
    {
        ShiftNodes(ptrCurrent, ptrCurrent->ptrRight);
    }
    else if (m_ptrRoot->ptrRight == nullptr)
    {
        ShiftNodes(ptrCurrent, ptrCurrent->ptrLeft);
    }
    else
    {
        BinarySearchTreeNode<TK, TD>* ptrSuccessor = GetSuccessor(ptrCurrent);
        if (ptrSuccessor->ptrParent != ptrCurrent)
        {
            ShiftNodes(ptrSuccessor, ptrSuccessor->ptrRight);
            ptrSuccessor->ptrRight = ptrCurrent->ptrRight;
            ptrSuccessor->ptrRight->ptrParent = ptrSuccessor;
        }

        ShiftNodes(ptrCurrent, ptrSuccessor);
        ptrSuccessor->ptrLeft = ptrCurrent->ptrLeft;
        ptrSuccessor->ptrLeft->ptrParent = ptrSuccessor;
    }

    m_nodeCount--;
}

// https://en.wikipedia.org/wiki/Binary_search_tree#Deletion
template <typename TK, typename TD>
void BinarySearchTree<TK, TD>::ShiftNodes(BinarySearchTreeNode<TK, TD>* node1, BinarySearchTreeNode<TK, TD>* node2) // done
{
    if (node1->ptrParent == nullptr)
    {
        m_ptrRoot = node2;
    }
    else if (node1 == node1->ptrParent->ptrLeft)
    {
        node1->ptrParent->ptrLeft = node2;
    }
    else
    {
        node1->ptrParent->ptrRight = node2;
    }

    if (node2 != nullptr)
    {
        node2->ptrParent = node1->ptrParent;
    }
}

// https://en.wikipedia.org/wiki/Binary_search_tree#Successor_and_predecessor
template <typename TK, typename TD>
BinarySearchTreeNode<TK, TD>* BinarySearchTree<TK, TD>::GetSuccessor(BinarySearchTreeNode<TK, TD>* ptrCurrent) // done
{
    if (ptrCurrent->ptrRight != nullptr)
    {
        return RecursiveGetMin(ptrCurrent->ptrRight);
    }

    BinarySearchTreeNode<TK, TD>* ptrPrevious = ptrCurrent->ptrParent;
    while (ptrPrevious != nullptr && ptrCurrent == ptrPrevious->ptrRight)
    {
        ptrCurrent = ptrPrevious;
        ptrPrevious = ptrCurrent->ptrParent;
    }

    return ptrPrevious;
}

} // End namespace

#endif
