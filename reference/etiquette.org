* Classroom Etiquette

** Classroom focus:

Classroom co-working/lab time is for students to work on homework and ask questions.
Students will need to be able to focus in order to properly get through assignments.
Please be mindful of others and adhere to the following:

- Do not sing in class, unless we happen to all break out in song because it's a "musical episode" of class.
- Do not talk to yourself while working through problems. (Muttering is probably fine, but don't use your normal speaking volume.)
- Do not eat with your mouth open. Some people have misophonia.
- Feel free to bring and wear headphones during class to focus.
