# -*- mode: org -*-
* First time setup: Cloning your repository

- After you tell the instructor your GitLab name they will give you access to a personal repository for your classwork.
- [[file:images/vscode-gitlab-clonehttps.png]]
  - From your repository GitLab webpage, click the blue "Code" button, then copy the "HTTPS" link. (If you know about SSH keys, go for it.)
- [[file:images/vscode-clonegitrepo.png]]
  - When you first start VS Code, there will be an option to *"Clone Git Repository"*. Click on this, and the top textbar will wait for a URL.
- [[file:images/vscode-clonegitrepo2.png]]
  - Paste your repo URL in this box and hit ENTER.
- [[file:images/vscode-clonegitrepo3.png]]
  - It will ask you where you want to store the repository folder on your hard drive. Find a location that you won't forget. :)
- [[file:images/vscode-git-cloning.png]]
  - The clone process will pull the files from the server to your computer.
- [[file:images/vscode-git-cloning-username.png]]
  - VS Code might ask for your Username in the top bar, or Windows might pop up a dialog box to have you sign in. Sign in with your GitLab account and password.
- [[file:images/vscode-repoopen.png]]
  - Once cloning is done, you can open the FOLDER for your repo and see any files within it, such as lab starter code.


** Configuring Git

You can run these commands either in VS Code or in Git Bash, but you'll need to enter a few commands to get everything set up.

#+BEGIN_SRC
git config --global user.name "YOURNAME"
#+END_SRC

#+BEGIN_SRC
git config --global user.email "YOUREMAIL"
#+END_SRC

#+BEGIN_SRC
git config --global pull.rebase false
#+END_SRC

--------------------------------------------------------------------------------

* Making changes and backing them up

- [[file:images/vscode-source-addchange.png]]
  - After you've made a chance to one or more files, it will show up in the list of Changes under the Source Control button.
  - Next to a file you want to backup, press the "+" button. It will then be categorized under "Staged Changes".
- [[file:images/vscode-source-commit.png]]
  - Add a description of your changes in the textbox above the *"Commit"* button. Once you're done, click *"Commit"*.
- [[file:images/vscode-source-sync.png]]
  - To send your changes to the server for backup, click on the circular arrow icon at the bottom of VS Code. This will send your changes to the server and pull any changes (e.g., from the instructor) to your computer.
- [[file:images/vscode-gitfetch.png]]
  - It might ask if you want to periodically run "git fetch". This command pulls changes from the server. You can select "Yes".


** Verify that your changes were saved

- After committing your changes, make sure the up-to-date version shows up on GitLab.
- [[file:images/gitlab-confirmchanges1.png]]
  - Go to your GitLab webpage. Make sure your commit message shows up here.
- [[file:images/gitlab-confirmchanges2.png]]
  - Also, navigate to the *file* that you updated from this GitLab web view.
  - Make sure this is the *latest version* of your file.
