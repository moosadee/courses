# -*- mode: org -*-

#+TITLE: Code Cookbook
#+AUTHOR: Rachel Wil Sha Singh

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/rworgmode.css" />
#+HTML_HEAD: <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script> <script>hljs.initHighlightingOnLoad();</script>

-----

* Looping

** Make sure int input is valid

#+BEGIN_SRC cpp :class cpp
  int GetIntInput( int minval, int maxval )
  {
    // Ask the user to enter a choice between minval and maxval
    int choice;
    cout << "Enter # between " << minval << "-" << maxval << ": ";
    cin >> choice;

    // Have the user re-enter their selection while their selection is invalid
    while ( choice < minval || choice > maxval )
    {
      cout << "Invalid selection! Try again: ";
      cin >> choice;
    }

    // Input is finally valid; return it back to the caller
    return choice;
  }
#+END_SRC


** Basic program loop

#+BEGIN_SRC cpp :class cpp
  bool running = true;
  while ( running )
    {
      // Main menu
      cout << "MAIN MENU" << endl;
      cout << "0. Quit" << endl;
      cout << "1. Do thing A" << endl;
      cout << "2. Do thing B" << endl;

      // Get user's selection
      int choice = GetIntInput( 0, 2 );

      // Perform some operation based on their input
      switch( choice )
        {
        case 0:
          {
            // The loop will end and program will stop.
            running = false;
          }
          break;

        case 1:
          {
            // Do thing A
          }
          break;

        case 2:
          {
            // Do thing B
          }
          break;
        }
    }

  cout << endl << "Goodbye." << endl;
#+END_SRC


** Display all items in a vector

#+BEGIN_SRC cpp :class cpp
  // Example vector object
  vector<TYPE> my_vector;

  // Display a list of each element's INDEX and VALUE.
  for ( size_t i = 0; i < my_vector.size(); i++ )
  {
    cout << i << ". " << my_vector[i] << endl;
  }
#+END_SRC








-----
