# -*- mode: org -*-

#+TITLE: Pixel Aliens activity - Rachel Wil ("R.W.") 🏳️‍⚧️ Courses
#+AUTHOR: Rachel Wil Sha Singh
#+OPTIONS: toc:nil

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../../style/purple-style.css" />
#+HTML_HEAD:  <link rel="icon" type="image/png" href="http://rachel.likespizza.com/web-assets/images/favicon-moose.png">

[[https://moosadee.gitlab.io/courses/][<< Back to homepage]]

---------------------------------------------------------

- [[file:game/index.html][Interactive game]]
- [[file:activity-minizine-printable.png][Printable workbook]]

---------------------------------------------------------
